KAIST 게임제작동아리 하제
Project MMO

기획서
https://drive.google.com/open?id=1dE_-N3ZECKGPHeA--fLXZmbRYY6MCr-RuNCHYduUv-E



개발 환경 ---------------------------------------

서버, 클라이언트 IDE: VS2013
서버, 클라이언트 언어: C# .NET Framework 4.5

그래픽 라이브러리: OpenTK

RDBMS: MariaDB
RDBMS Connector: MySQL connector ./net

NoSQL: Redis
NoSQL Connector: 미정



빌드 방법 ---------------------------------------

MMO.sln 솔루션을 열어서 컴파일.
로컬 서버는 별도의 세팅 없이도 동작한다.

