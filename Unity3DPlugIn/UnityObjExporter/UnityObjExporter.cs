﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class UnityObjExporter : ScriptableWizard 
{
	public string exportName;
	public string exportFolder;

    public bool exportTexture = true;
    public bool ApplyTranslate;
    public bool ApplyRotate;
    public bool ApplyScale;
    public bool IsExportMaterial = true;
	
	[MenuItem ("File/Export/Wavefront .OBJ")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard("Export OBJ", typeof(UnityObjExporter), "Export");
	}

	void OnInspectorUpdate() 
	{
		Repaint();
	}

	void OnWizardCreate()
	{
		string selectedName;
		bool isScene = false;

		if(Selection.activeGameObject == null)
		{
			isScene = true;
			selectedName = "Scene";
		}
		else
		{
			selectedName = Selection.activeGameObject.name;
		}

		string expFile = EditorUtility.SaveFilePanel("Export OBJ", "", selectedName + ".obj", "obj");

		if(expFile.Length > 0)
		{
			string fileName = System.IO.Path.GetFileNameWithoutExtension(expFile);
			exportName = fileName;
			exportFolder = System.IO.Path.GetDirectoryName (expFile);
			ExportModel(isScene);
		}
	}

	void ExportMaterial()
	{
		//Export Reference Material for .Obj

	}

	void ExportModel(bool isScene)
	{
		//TODO : Export SkinnedMeshRender's Mesh to .Obj

		StringBuilder sb = new StringBuilder();
        StringBuilder sbMat = new StringBuilder();
		sb.AppendLine("g default");

		Object[] obj;
		//Get all GameObject in current Scene and Export.
		if(isScene)
		{
			obj = GameObject.FindObjectsOfType(typeof(GameObject));
		}
		else
		{
			obj = Selection.gameObjects;
		}

        Dictionary<Mesh, Renderer> meshDic = new Dictionary<Mesh, Renderer>();

		if(obj.Length > 0)
		{
			foreach(GameObject o in obj)
			{
				if(isScene)
				{
					MeshFilter tempFilter = o.GetComponent<MeshFilter>();
					
					if(tempFilter != null )
					{
                        meshDic.Add(tempFilter.sharedMesh, tempFilter.gameObject.GetComponent<MeshRenderer>());
					}
					
					SkinnedMeshRenderer renderer = o.GetComponent<SkinnedMeshRenderer>();
					
					if(renderer != null)
					{
                        meshDic.Add(renderer.sharedMesh, renderer);
					}
				}
				else
				{
					MeshFilter[] meshFilter = (MeshFilter[])o.GetComponentsInChildren<MeshFilter>();
					SkinnedMeshRenderer[] skinnedMeshFilter = (SkinnedMeshRenderer[])o.GetComponentsInChildren<SkinnedMeshRenderer>();

					foreach(MeshFilter f in meshFilter)
					{
                        MeshRenderer renderer = f.gameObject.GetComponent<MeshRenderer>();

                        meshDic.Add(f.sharedMesh, renderer);
					}

					foreach(SkinnedMeshRenderer f in skinnedMeshFilter)
					{
                        meshDic.Add(f.sharedMesh, f);
					}
				}

                if (meshDic.Count > 0)
                {
					sb.AppendLine("mtllib ./" + exportName + ".mtl");
                    ExportMesh(sb, sbMat, meshDic);
                }
            }
        }
	}

    void ExportMesh(StringBuilder sb, StringBuilder sbMat, Dictionary<Mesh, Renderer> meshList)
    {
		int cFaceIndex = 1;

        foreach (KeyValuePair<Mesh, Renderer> pair in meshList)
		{
			//TODO : Read Mesh Data here
			//Mesh mesh = f.mesh;
			Mesh sharedMesh = pair.Key;
			int wVerts = 0;

			//vector
			foreach(Vector3 vec in sharedMesh.vertices)
			{
				sb.AppendLine("v " + vec.x + " " + vec.y + " " + vec.z);
				wVerts++;
			}
					
			//normal
			foreach(Vector3 norm in sharedMesh.normals)
			{
				sb.AppendLine("vn " + norm.x+ " " + norm.y + " " + norm.z);
			}
					
			//texture uv
			foreach(Vector2 vec in sharedMesh.uv)
			{
                // OpenGL has uv convention of considering lower left as the origin
				sb.AppendLine("vt " + vec.x + " " + (1 - vec.y));
			}

			for(int i = 0; i < sharedMesh.subMeshCount; i++)
			{
                if (IsExportMaterial)
                {
                    if(i <= pair.Value.sharedMaterials.Length - 1)
                    {
                        Material material = pair.Value.sharedMaterials[i];
                        string materialName = GenerateMaterialNew(ref sbMat, ref material);

                        sb.AppendLine("usemtl " + materialName);
						sb.AppendLine("usemap " + materialName);
                    }
                }
                        
				int[] indices = sharedMesh.GetTriangles(i);

				for(int k = 0; k < indices.Length; k += 3)
				{
					string face3 = ConstructFaceString(indices[k+2] + cFaceIndex, indices[k+2] + cFaceIndex, indices[k+2] + cFaceIndex);
					string face2 = ConstructFaceString(indices[k+1] + cFaceIndex, indices[k+1] + cFaceIndex, indices[k+1] + cFaceIndex);
					string face1 = ConstructFaceString(indices[k] + cFaceIndex, indices[k] + cFaceIndex, indices[k] + cFaceIndex);
							
					sb.AppendLine ("f " + face3 + " " + face2 + " " + face1);
				}
			}

			cFaceIndex += wVerts;
		}

        System.IO.File.WriteAllText (exportFolder + "/" + exportName + ".obj", sb.ToString());
        System.IO.File.WriteAllText(exportFolder + "/" + exportName + ".mtl", sbMat.ToString());
	}

	string ConstructFaceString(int i1, int i2, int i3)
	{
		return "" + i1 + "/" + i2 + "/" + i3;
	}

    List<string> notReadableTextures = new List<string>();
    List<string> matNameCache = new List<string>();

    string GenerateMaterialNew(ref System.Text.StringBuilder sbR, ref Material m)
    {
        string matName = m.name;
        if (m.HasProperty("_MainTex"))
        {
            Texture2D mT = m.GetTexture("_MainTex") as Texture2D;
            if (mT != null)
            {
                matName = m.GetTexture("_MainTex").name;
            }
            else
            {
                Debug.Log("Could not generate material from texture name (" + matName + "). No texture assigned");
            }
        }

        bool instance = matName.Contains("Instance");

        if (instance)
        {
            matName += "_(" + m.GetInstanceID() + ")";
        }

        if (matNameCache.Contains(matName) == false)
        {
            matNameCache.Add(matName);
            sbR.AppendLine("newmtl " + matName);

            bool hasColor = m.HasProperty("_Color");

            if (hasColor)
            {
                Color matColor = m.color;
                sbR.AppendLine("Kd " + matColor.r + " " + matColor.g + " " + matColor.b);
                float alpha = Mathf.Lerp(0, 1, matColor.a);

				sbR.AppendLine("d " + alpha);
			}
			
			bool hasSpecular = m.HasProperty("_SpecColor");

            if (hasSpecular)
            {
                Color specColor = m.GetColor("_SpecColor");
                sbR.AppendLine("Ks " + specColor.r + " " + specColor.g + " " + specColor.b);
            }

            bool hasTexture = m.HasProperty("_MainTex");

            if (hasTexture)
            {
                Texture2D mainTex = m.GetTexture("_MainTex") as Texture2D;

                if (mainTex != null)
                {
                    Vector2 mainTexScale = m.GetTextureScale("_MainTex");
                    if (mainTex.wrapMode == TextureWrapMode.Clamp)
                    {
                        sbR.AppendLine("-clamp on");
                    }

                    sbR.AppendLine("s " + mainTexScale.x + " " + mainTexScale.y);
                    sbR.AppendLine("map_Kd " + mainTex.name + ".png");

                    try
                    {
                        if (exportTexture)
                        {
                            Texture2D nT = new Texture2D(mainTex.width, mainTex.height, TextureFormat.ARGB32, false);
                            Color[] pxls = mainTex.GetPixels();
                            nT.SetPixels(pxls);

                            byte[] pngEx = nT.EncodeToPNG();

							string fileName = exportFolder + "/" + mainTex.name + ".png";
							if(!System.IO.File.Exists(fileName))
							{
								System.IO.File.WriteAllBytes(fileName, pngEx);
							}
                        }
                    }
                    catch (System.Exception ex)
                    {
						Debug.LogException(ex);
                    }
                }
            }
            sbR.AppendLine();
        }
        return matName;
    }

}

#endif
