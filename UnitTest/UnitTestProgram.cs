﻿using System;
using System.Reflection;

namespace HAJE.MMO
{
    public class UnitTestProgram
    {
        static int Main(string[] args)
        {
            // 하드코딩 주의
            AppDomain.CurrentDomain.Load(new AssemblyName("Client, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"));
            AppDomain.CurrentDomain.Load(new AssemblyName("Server, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"));

            try
            {
                LogDispatcher ld = new LogDispatcher("UnitTest", 10);
                ld.ConsoleOutputEnable = true;

                UnitTest.Run(ld);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return 1;
            }

            return 0;
        }
    }
}
