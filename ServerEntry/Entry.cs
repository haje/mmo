﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;

namespace HAJE.MMO.Server.Entry
{
    class Entry
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                System.Console.WriteLine("Sending msg using npcs");
                NamedPipeClientStream npcs = new NamedPipeClientStream("SlackBot");
                npcs.Connect();

                StreamWriter sw = new StreamWriter(npcs);
                sw.WriteLine("ServerException");
                sw.WriteLine(e.ExceptionObject.ToString());
                sw.Flush();
                npcs.WaitForPipeDrain();
                System.Console.WriteLine("sended");

                Process.GetCurrentProcess().Kill();
            };

            Configuration.Load();

            if (Configuration.Instance.UnitTestEnabled)
            {
                var unitTestLog = new LogDispatcher("unittest", 16);
                unitTestLog.ConsoleOutputEnable = true;
                unitTestLog.DebugOutputEnable = true;
                UnitTest.Run(unitTestLog);
            }

            var console = new Server.Console.ServerConsoleFactory().Create();
            ServerContext.Instance.RootPrompt = console;

            var config = Configuration.Instance;
            switch (config.ServerMode)
            {
                case ServerMode.Console:
                    break;
                case ServerMode.LocalServer:
                    console.ExecuteCommand("start master");
                    console.ExecuteCommand("start login");
                    console.ExecuteCommand("start achieve");
                    console.ExecuteCommand("start field");
                    console.ExecuteCommand("start auth");
                    break;
                case ServerMode.DevServer:
                    console.ExecuteCommand("start master");
                    console.ExecuteCommand("start login");
                    console.ExecuteCommand("start achieve");
                    console.ExecuteCommand("start field");
                    //console.Child.ExecuteCommand("createfield 1 0");
                    //console.Child.ExecuteCommand("log");
                    break;
                case ServerMode.LiveMaster:
                    console.ExecuteCommand("start master");
                    break;
                case ServerMode.LiveLogin:
                    console.ExecuteCommand("start login");
                    break;
                default:
                    System.Console.WriteLine("아직 구현되지 않은 모드입니다.");
                    break;
            }

            console.Run();
        }
    }
}
