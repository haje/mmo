﻿using HAJE.MMO.Client.UI.Tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client
{
    public static class GlobalDebugCommand
    {
        public static void Initialize()
        {
            var cmds = GameSystem.CommandList;
            cmds.AddCommand("test", "테스트 모드로 들어갑니다.", Test, "animation", "field", "particle", "ui");
            cmds.AddCommand("preview", "미리보기 모드로 들어갑니다.", Preview, "terrain", "charmesh");
            cmds.AddCommand("dout", "디버그 출력을 켜고 끕니다.", DOut, "fps", "watch", "log");
            cmds.AddCommand("help", "전체 명령어 목록을 출력합니다.", Help);
            cmds.AddCommand("minimize", "디버그 콘솔을 최소화합니다.", Minimize, "on", "off");
            cmds.AddCommand("dock", "디버그 콘솔의 위치를 변경합니다.", Dock, "topleft", "topright", "bottomleft", "bottomright");
            cmds.AddCommand("close", "디버그 콘솔을 닫습니다.", Close);
            cmds.AddCommand("exit", "게임을 종료합니다.", Exit);
        }

        static void Test(DebugConsole console, string[] args)
        {
            string param = "";
            if (args.Length == 2)
                param = args[1];

            if (param == "animation")
            {
                GameSystem.SetGameLoop(new GameLoop.AnimationTest());
            }
            else if (param == "particle")
            {
                GameSystem.SetGameLoop(new GameLoop.ParticleTest());
            }
            else if (param == "ui")
            {
                GameSystem.SetGameLoop(new GameLoop.UITest());
            }
            else
            {
                console.Write("test animation | test particle | test ui");
            }
        }

        static void Preview(DebugConsole console, string[] args)
        {
            string param = "";
            int index = 0;
            if (args.Length > 1)
                param = args[1];

            if (param == "terrain" && args.Length > 2 && int.TryParse(args[2], out index))
            {
                GameSystem.SetGameLoop(new GameLoop.TerrainPreview(index));
            }
            else if (param == "charamesh")
            {
                GameSystem.SetGameLoop(new GameLoop.AnimatedMeshPreview());
            }
            else
            {
                console.Write("preview terrain [index] | preview charamesh");
            }
        }

        static void DOut(DebugConsole console, string[] args)
        {
            if (args.Length == 2)
            {
                string param = args[1].ToLower();
                if (param == "fps")
                {
                    GameSystem.DebugOutput.FpsToggle();
                }
                else if (param == "watch")
                {
                    GameSystem.DebugOutput.WatchToggle();
                }
                else if (param == "log")
                {
                    GameSystem.DebugOutput.LogToggle();
                }
                else
                {
                    console.Write("dout | dout fps | dout watch | dout log");
                }
            }
            else
            {
                GameSystem.DebugOutput.DOutToggle();
            }
        }

        static void Help(DebugConsole console, string[] args)
        {
            if (GameSystem.RunningGameLoop != null)
            {
                console.Write("{0}의 명령어 목록", GameSystem.RunningGameLoop.DebugName);
                GameSystem.RunningGameLoop.CommandList.PrintCommandList(console);
                console.Write("");
            }

            console.Write("전역 명령어 목록");
            GameSystem.CommandList.PrintCommandList(console);
        }

        static void Minimize(DebugConsole console, string[] args)
        {
            string param = "";
            if (args.Length == 1)
            {
                console.ToggleMinimize();
            }
            if (args.Length == 2)
                param = args[1].ToLower();

            if (param == "on")
            {
                console.MinimizeOn();
            }
            else if (param == "off")
            {
                console.MinimizeOff();
            }
            else
            {
                console.Write("minimize on | minimize off");
            }
        }

        static void Dock(DebugConsole console, string[] args)
        {
            string param = "";
            if (args.Length == 2)
                param = args[1].ToLower();

            if (param == "topleft")
            {
                console.DockTopLeft();
            }
            else if (param == "topright")
            {
                console.DockTopRight();
            }
            else if (param == "bottomleft")
            {
                console.DockBottomLeft();
            }
            else if (param == "bottomright")
            {
                console.DockBottomRight();
            }
            else
            {
                console.Write("dock topleft | dock topright | dock bottomleft | dock bottomright");
            }
        }

        static void Close(DebugConsole console, string[] args)
        {
            console.ConsoleLayer.Visible = false;
        }

        static void Exit(DebugConsole console, string[] args)
        {
            GameSystem.Exit();
        }
    }
}
