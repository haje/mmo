#version 330 core

uniform sampler2D DiffuseTexture;
uniform bool IsTextureLoaded;

in vec4 projPosition;
in vec4 viewNormal;
in vec2 fragUV;
in vec4 fragColor;

out vec4 UnshadedOut;
out vec4 NormalOut;
out vec4 DepthOut;

void main()
{
    if (IsTextureLoaded)
    {
        UnshadedOut = vec4(texture(DiffuseTexture, fragUV).rgb * fragColor.rgb, 1.0);
    }
    else
    {
        UnshadedOut = vec4(fragColor.rgb, 1.0);
    }
    NormalOut = vec4(normalize(viewNormal).xyz, 1.0);
    DepthOut = vec4(vec3(projPosition.z / projPosition.w), 1.0);
}