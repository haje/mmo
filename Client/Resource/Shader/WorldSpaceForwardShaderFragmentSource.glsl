#version 330 core

in vec4 fragColor;
in vec3 eyePosition;
in vec3 eyeNormal;

out vec4 colorOut;

void main()
{
    vec4 lightPos = vec4(0, 9999, 0, 1);   /* Pseudo-directional light */
    vec3 L = normalize(vec3(lightPos) - eyePosition);

    float diff = max(dot(eyeNormal, L), 0.0);

    vec3 diffColor = diff * vec3(fragColor);
    vec4 ambientColor = fragColor * 0.4;

    colorOut = max(vec4(diffColor, fragColor.a), ambientColor);
}