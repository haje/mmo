#version 330 core

uniform float frequency;
uniform float minHeight;
uniform float maxHeight;

in vec4 fragColor;
in vec3 modelPosition;
in vec3 eyePosition;

out vec4 colorOut;

float pi = 3.1415926;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
    float intensity = pow(max(1 - abs(sin(modelPosition.x * frequency * pi)), 1 - abs(sin(modelPosition.z * frequency * pi))), 3);
    intensity = step(0.8, intensity) * clamp(-1/eyePosition.z * 5, 0.8, 1) ;
    vec3 heightColor = hsv2rgb(vec3(smoothstep(minHeight, maxHeight, modelPosition.y), 1, 1));

    colorOut = vec4(heightColor * intensity, 1.0);
}