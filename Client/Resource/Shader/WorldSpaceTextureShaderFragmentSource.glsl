#version 330 core

uniform sampler2D Tex;

in vec4 fColor;
in vec2 fUV;

out vec4 colorOut;

void main()
{
    colorOut = texture(Tex, fUV) * fColor;
}