#version 330 core

uniform sampler2D Tex;
uniform vec2 TextureSize;

in vec4 fColor;
in vec2 fUV;

out vec4 colorOut;

float blurKernel[25] = float[](
    1,  4,  7,  4, 1,
    4, 16, 26, 16, 4,
    7, 26, 41, 26, 7,
    4, 16, 26, 16, 4,
    1,  4,  7,  4, 1
);

vec2 uvOffset[25] = vec2[](
    vec2(-2, 2), vec2(-1, 2), vec2( 0, 2), vec2( 1, 2), vec2( 2, 2),
    vec2(-2, 1), vec2(-1, 1), vec2( 0, 1), vec2( 1, 1), vec2( 2, 1),
    vec2(-2, 0), vec2(-1, 0), vec2( 0, 0), vec2( 1, 0), vec2( 2, 0),
    vec2(-2,-1), vec2(-1,-1), vec2( 0,-1), vec2( 1,-1), vec2( 2,-1),
    vec2(-2,-2), vec2(-1,-2), vec2( 0,-2), vec2( 1,-2), vec2( 2,-2)
);

void main()
{
    colorOut = vec4(0.0);
    vec2 invTextureSize = new vec2(1.0/TextureSize.x, 1.0/TextureSize.y);
    for (int i = 0; i < 25; ++i)
    {
        colorOut += blurKernel[i]/273.0 * texture(Tex, fUV + invTextureSize * uvOffset[i]) * fColor;
    }
}