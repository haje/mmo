#version 330 core
                
uniform mat3 trans;
uniform vec2 viewPort;

layout (location = 0) in vec2 positionIn;
layout (location = 1) in vec4 colorIn;
layout (location = 2) in vec2 textureUVIn;
                
out vec4 color;
out vec2 UV;
                
void main()
{
    color = colorIn;
    UV = textureUVIn;
    vec3 transformed = trans * vec3(positionIn, 1);
    transformed.x = transformed.x / viewPort.x * 2 - 1.0f;
    transformed.y = transformed.y / viewPort.y * 2 - 1.0f;
    transformed.z = 1;
    gl_Position = vec4(transformed, 1);
}