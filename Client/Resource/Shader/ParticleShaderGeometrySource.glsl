#version 330 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in vec4 gViewPosition[];
in vec4 gColor[];
in float gRotation[];
in vec2 gUVmin[];
in vec2 gUVmax[];
in vec4 gBoundingBox[];

out vec4 fColor;
out vec2 fUV;
out vec4 fViewPosition;

//AB
//CD
//in order of C->A->D->B
//ref. https://en.wikipedia.org/wiki/Triangle_strip

void main()
{
    fViewPosition = gViewPosition[0];
    fColor = gColor[0];
    vec4 halfBB = gBoundingBox[0] / 2.0;
    float pi = 3.1415926535;
    
    //Point C
    //gl_Position = gl_in[0].gl_Position + vec4(-1, -1, 1, 0) * halfBB;
    gl_Position = gl_in[0].gl_Position + vec4(cos(gRotation[0] + 1.25 * pi), sin(gRotation[0] + 1.25*pi), 1, 0) * halfBB;
    fUV = gUVmin[0];
    EmitVertex();

    //Point A
    //gl_Position = gl_in[0].gl_Position + vec4(-1,  1, 1, 0) * halfBB;
    gl_Position = gl_in[0].gl_Position + vec4(cos(gRotation[0] + 0.75 * pi), sin(gRotation[0] + 0.75*pi), 1, 0) * halfBB;
    fUV = vec2(gUVmin[0].x, gUVmax[0].y);
    EmitVertex();

    //Point D
    //gl_Position = gl_in[0].gl_Position + vec4( 1, -1, 1, 0) * halfBB;
    gl_Position = gl_in[0].gl_Position + vec4(cos(gRotation[0] + 1.75 * pi), sin(gRotation[0] + 1.75*pi), 1, 0) * halfBB;
    fUV = vec2(gUVmax[0].x, gUVmin[0].y);
    EmitVertex();

    //Point B
    //gl_Position = gl_in[0].gl_Position + vec4( 1,  1, 1, 0) * halfBB;
    gl_Position = gl_in[0].gl_Position + vec4(cos(gRotation[0] + 0.25 * pi), sin(gRotation[0] + 0.25*pi), 1, 0) * halfBB;
    fUV = gUVmax[0];
    EmitVertex();

    EndPrimitive();
}