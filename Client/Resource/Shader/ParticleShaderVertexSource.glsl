#version 330 core

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;

layout (location = 0) in vec3 position;
layout (location = 2) in vec4 color;
layout (location = 5) in float rotation;
layout (location = 6) in vec2 UVmin;
layout (location = 7) in vec2 UVmax;
layout (location = 8) in vec2 boundingBox;

out vec4 gViewPosition;
out vec4 gColor;
out float gRotation;
out vec2 gUVmin;
out vec2 gUVmax;
out vec4 gBoundingBox;

void main()
{
    gl_Position = ProjectionMatrix * ViewMatrix * vec4(position, 1.0);
    gViewPosition = ViewMatrix * vec4(position, 1.0);
    gColor = color;
    gRotation = rotation;
    gUVmax = UVmax;
    gUVmin = UVmin;
    //gBoundingBox = (ProjectionMatrix * ViewMatrix * vec4(boundingBox, 0.0, 0.0)).xy;
    
    // Shader temporarily changed for bounding box; this code can contain some bugs.
    // Original code was:
    //       gBoundingBox = ProjectionMatrix * vec4(vec2(1, 1), 0.0, 0.0);
    // - KeV
    gBoundingBox = ProjectionMatrix * vec4(boundingBox, 0.0, 0.0);
}