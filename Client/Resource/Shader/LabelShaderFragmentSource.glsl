#version 330 core

uniform sampler2D tex;
uniform bool isTexture;

in vec4 fragColor;
in vec2 texCoord;

out vec4 colorOut;

void main()
{
    if (isTexture)
    {
        vec4 textureColor = texture(tex,texCoord);
        colorOut = vec4(fragColor.x, fragColor.y, fragColor.z, textureColor.x);
    }
    else
    {
        colorOut = fragColor;
    }
}