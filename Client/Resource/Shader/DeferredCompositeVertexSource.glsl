#version 330 core

layout (location = 0) in vec3 position;

out vec4 pos;
out vec2 texCoord;

void main()
{
    pos = vec4(position, 1.0);
    gl_Position = pos;
    texCoord = pos.xy / pos.w * 0.5 + 0.5;
}