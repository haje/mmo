#version 330 core

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform vec4 ColorTint;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv;

out vec4 fColor;
out vec2 fUV;
out vec4 fViewPosition;

void main()
{
    fViewPosition   = ViewMatrix * ModelMatrix * vec4(position, 1.0);
    gl_Position     = ProjectionMatrix * fViewPosition;
    fColor          = color * ColorTint;
    fUV             = uv;
}