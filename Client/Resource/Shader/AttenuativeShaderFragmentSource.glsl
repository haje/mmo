#version 330 core

uniform sampler2D Tex;
uniform bool IsTextureLoaded;
uniform float CharacterDistance;
uniform float FogSightRange;
uniform float CharacterSyncRange;

in vec4 fColor;
in vec2 fUV;
in vec4 fViewPosition;

out vec4 colorAccum;
out vec4 revealage;

float fogThreshold = 0.1;
float fogEventHorizon = (FogSightRange + CharacterSyncRange) / 2.0;
float density = sqrt(log(1 / fogThreshold)) / FogSightRange;

/* weight function */
float w(in float z, in float alpha)
{
    float depth_weight = 100/(1e-5+pow(abs(z)/10.0, 3.0)+pow(abs(z)/200.0, 6.0));
    return pow(alpha+0.01, 4.0) + max(1e-2, min(3e3, depth_weight));
}

float distToFog(float dist)
{
    float d3dfog_exp2 = 0.0;
    if (dist > FogSightRange)
    {
        d3dfog_exp2 = smoothstep(1 / fogEventHorizon, 1 / FogSightRange, 1 / dist) * fogThreshold;
    }
    else
    {
        d3dfog_exp2 = exp(-pow(dist * density, 2));
    }
    return d3dfog_exp2;
}

void main()
{
    vec4 colorOut;
    if (IsTextureLoaded)
    {
        colorOut = texture2D(Tex, fUV) * fColor;
    }
    else
    {
        colorOut = fColor;
    }
    colorOut.a *= clamp(distToFog(length(fViewPosition)), 0, 1);
    colorOut.a *= 1 - clamp((CharacterDistance - length(fViewPosition)) / CharacterDistance, 0, 1);
    float weight = w(fViewPosition.z, colorOut.a);
    colorAccum = vec4(colorOut.rgb * colorOut.a * weight, colorOut.a);
    revealage.r = colorOut.a * weight;
}