#version 150 core

uniform sampler2D tex;
uniform vec4 borderColor;
uniform vec4 fillColor;

in vec4 color;
in vec2 UV;

out vec4 colorOut;

void main()
{
    if(UV.x == 0 && UV.y == 0)
        colorOut = fillColor;
    else
        colorOut = borderColor;
}