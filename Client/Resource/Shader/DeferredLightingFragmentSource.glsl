#version 330 core

uniform bool IsShadowEnabled;

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform vec3 LightColor;
uniform float LightRadius;

uniform sampler2D DepthBuffer;
uniform sampler2D NormalBuffer;
uniform sampler2D ShadowTexture;

in vec3 viewLightPos;
in vec4 pos;
in vec2 texCoord;

out vec4 colorOut;

// Code from https://imdoingitwrong.wordpress.com/2011/01/31/light-attenuation/
vec3 DirectIllumination(vec3 posVec, vec3 normalVec, vec3 lightCenter, float lightRadius, vec3 lightColor, float cutoff)
{
    // calculate normalized light vector and distance to sphere light surface
    vec3 lightVec = lightCenter - posVec;
    float lightDist = length(lightVec);
    float distToSphereLightSurface = max(lightDist - lightRadius, 0);
    lightVec = normalize(lightVec);
     
    // calculate basic attenuation
    float denom = distToSphereLightSurface / lightRadius + 1;
    float attenuation = 1 / (denom * denom);
    
    // scale and bias attenuation such that:
    //   attenuation == 0 at extent of max influence
    //   attenuation == 1 when d == 0
    attenuation = (attenuation - cutoff) / (1 - cutoff);
    attenuation = max(attenuation, 0);
    
    float cosineTerm = max(dot(lightVec, normalVec), 0);
    return lightColor * cosineTerm * attenuation;
}

void main()
{
    vec3 normal = texture2D(NormalBuffer, texCoord).rgb;
    float depth = texture2D(DepthBuffer, texCoord).r;
    float visibility;
    if (IsShadowEnabled)
        visibility = texture2D(ShadowTexture, texCoord).r;
    else
        visibility = 1.0;

    vec4 unnormalizedVP = (inverse(ProjectionMatrix) * vec4(pos.xy, depth, 1.0));
    vec3 viewPosition = unnormalizedVP.xyz / unnormalizedVP.w;

    colorOut = vec4(DirectIllumination(viewPosition, normal, viewLightPos, LightRadius, LightColor, 0.0001) * visibility, 1.0);
}