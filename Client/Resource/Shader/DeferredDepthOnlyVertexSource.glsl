#version 330 core

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;

layout (location = 0) in vec3 position;

out vec4 projPosition;

void main()
{
    projPosition = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(position, 1.0);
    gl_Position = projPosition;
}