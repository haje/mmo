#version 330 core

uniform sampler2D accumTexture;
uniform sampler2D revealageTexture;

in vec2 texCoord;

out vec4 colorOut;

void main()
{
    vec4 accum = texture(accumTexture, texCoord, 0);
    float r = accum.a;
    accum.a = texture(revealageTexture, texCoord, 0).r;
    colorOut = vec4(accum.rgb / clamp(accum.a, 1e-4, 5e4), r);
}