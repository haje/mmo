#version 330 core

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform vec4 ColorTint;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 textureUV;

out vec4 projPosition;
out vec4 viewNormal;
out vec2 fragUV;
out vec4 fragColor;

void main()
{
    projPosition = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(position, 1.0);
    gl_Position = projPosition;
    viewNormal = ViewMatrix * ModelMatrix * vec4(normal, 0.0);
    fragUV = textureUV;
    fragColor = color * ColorTint;
}