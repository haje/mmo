#version 330 core

uniform samplerCube Cubemap;

in vec3 viewDirection;

out vec4 colorOut;

void main()
{
    colorOut = texture(Cubemap, viewDirection);
}