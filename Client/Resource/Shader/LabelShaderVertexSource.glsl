#version 330 core
                
uniform mat3 transfMat;
uniform vec2 viewport;
uniform vec4 color;

layout (location = 0) in vec2 positionIn;
layout (location = 1) in vec2 textureUVIn;
                
out vec4 fragColor;
out vec2 texCoord;
                
void main()
{
    fragColor = color;
    texCoord = textureUVIn;
    vec3 transformed = transfMat * vec3(positionIn, 1);
    transformed.x = transformed.x / viewport.x * 2 - 1.0f;
    transformed.y = transformed.y / viewport.y * 2 - 1.0f;
    transformed.z = 1;
    gl_Position = vec4(transformed, 1);
}