#version 330 core

uniform sampler2D Tex;
uniform bool IsTextureLoaded;

in vec4 fColor;
in vec2 fUV;
in vec4 fViewPosition;

out vec4 colorOut;

void main()
{
    if (IsTextureLoaded)
    {
        colorOut = texture2D(Tex, fUV) * fColor;
    }
    else
    {
        colorOut = fColor;
    }
}