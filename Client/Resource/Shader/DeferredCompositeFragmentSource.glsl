#version 330 core

uniform sampler2D ColorBuffer;
uniform sampler2D LightingBuffer;

/* Fog specific uniforms */
uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform bool FogActivated;
uniform vec4 FogColor;
uniform float FogSightRange;
uniform float CharacterSyncRange;
uniform sampler2D DepthBuffer;
uniform sampler2D PerlinNoise;
uniform vec3 CameraPosition;
uniform float CameraFarP;

uniform int screenWidth;
uniform int screenHeight;
uniform int crosshairSize;
uniform int isCrosshair;

in vec4 pos;
in vec2 texCoord;

out vec4 colorOut;

float fogHeight = 15.0;
float fogHeightTurbulence = 10.0;
float noiseSamplingRate = 1 / 100.0;
float fogThreshold = 0.1;
float fogEventHorizon = (FogSightRange + CharacterSyncRange) / 2.0;
float density = sqrt(log(1 / fogThreshold)) / FogSightRange;
float parallax = 0.3;

float distToFog(float dist)
{
    float d3dfog_exp2 = 0.0;
    if (dist > FogSightRange)
    {
        d3dfog_exp2 = smoothstep(1 / fogEventHorizon, 1 / FogSightRange, 1 / dist) * fogThreshold;
    }
    else
    {
        d3dfog_exp2 = exp(-pow(dist * density, 2));
    }
    return d3dfog_exp2;
}

float distToFog_far(float dist)
{
    float d3dfog_exp2 = exp(-pow(dist * density, 2));
    d3dfog_exp2 = smoothstep(fogThreshold, 1.0, d3dfog_exp2);
    return d3dfog_exp2;
}

void main()
{
    vec4 color = vec4(texture2D(LightingBuffer, texCoord).xyz * 2.0 * 
        texture2D(ColorBuffer, texCoord).xyz, texture2D(ColorBuffer, texCoord).a);

    if (FogActivated)
    {
        float depth = texture2D(DepthBuffer, texCoord).r;
        vec4 unnormalizedVP = (inverse(ProjectionMatrix) * vec4(pos.xy, depth, 1.0));
        vec3 viewPosition = unnormalizedVP.xyz / unnormalizedVP.w;
        vec4 unnormalizedWP = inverse(ViewMatrix) * vec4(viewPosition, 1.0);
        vec3 worldPosition = unnormalizedWP.xyz / unnormalizedWP.w;
        vec3 deltaFromCam = worldPosition - CameraPosition;
        vec3 fogParticipation = deltaFromCam / deltaFromCam.y * fogHeight;

        float fogHeightDelta = texture(PerlinNoise, (fogParticipation.xz + CameraPosition.xz * parallax) * noiseSamplingRate).r * fogHeightTurbulence;
        fogHeight += fogHeightDelta;
        fogParticipation = deltaFromCam / deltaFromCam.y * fogHeight;

        float fogNoise = clamp(texture(PerlinNoise, CameraPosition.xz * noiseSamplingRate / 2.5).r + 0.6, 0, 1);

        float antiFogRate = 0.0;
        if (length(deltaFromCam) < length(fogParticipation))
        {
            antiFogRate = clamp(distToFog(length(deltaFromCam)), 0, 1);
        }
        else
        {
            antiFogRate = clamp(distToFog_far(length(fogParticipation)), 0, 1);
        }

        colorOut = mix
                (
                    vec4(FogColor.rgb, 1.0),
                    color,
                    antiFogRate
                );
    }
    else
    {
        colorOut = color;
    }

	if(isCrosshair==1)
	{
		if(texCoord.x * screenWidth > screenWidth / 2 - 2 && texCoord.x * screenWidth < screenWidth / 2 + 2
		&& texCoord.y * screenHeight > screenHeight / 2 - crosshairSize && texCoord.y * screenHeight < screenHeight / 2 + crosshairSize)
			colorOut = vec4(1,0,0,1);

		if(texCoord.x * screenWidth > screenWidth / 2 - crosshairSize && texCoord.x * screenWidth < screenWidth / 2 + crosshairSize
		&& texCoord.y * screenHeight > screenHeight / 2 - 2 && texCoord.y * screenHeight < screenHeight / 2 + 2)
			colorOut = vec4(1,0,0,1);
	}
}