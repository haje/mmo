#version 330 core

uniform sampler2D tex;
uniform bool isTexture;

in vec4 fragColor;
in vec2 texCoord;

out vec4 colorOut;

void main()
{
    if (isTexture)
    {
        colorOut = texture(tex,texCoord) * fragColor;
    }
    else
    {
        colorOut = fragColor;
    }
}