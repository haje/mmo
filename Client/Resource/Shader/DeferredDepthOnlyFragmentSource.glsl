#version 330 core

in vec4 projPosition;

out vec4 DepthOut;

void main()
{
    DepthOut = vec4(vec3(projPosition.z), 1.0);
}