#version 330 core

uniform mat4 ViewMatrix;
uniform vec3 LightPosition;

layout (location = 0) in vec3 position;

out vec3 viewLightPos;
out vec4 pos;
out vec2 texCoord;

void main()
{
    pos = vec4(position, 1.0);
    gl_Position = pos;
    texCoord = (position.xy + 1.0) / 2.0;
    viewLightPos = (ViewMatrix * vec4(LightPosition, 1.0)).xyz;
}