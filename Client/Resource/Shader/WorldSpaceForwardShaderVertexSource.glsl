#version 330 core

uniform mat4 ProjectionMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;

out vec4 fragColor;
out vec3 eyePosition;
out vec3 eyeNormal;

void main()
{
    gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(position, 1.0);
    fragColor   = color;
    eyePosition = (ViewMatrix * ModelMatrix * vec4(position, 1.0)).xyz;
    eyeNormal   = (ViewMatrix * ModelMatrix * vec4(normal, 0.0)).xyz;
}