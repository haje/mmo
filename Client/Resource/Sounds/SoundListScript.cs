﻿using System;
using System.Collections.Generic;
using HAJE.MMO.Client.Sounds;

namespace HAJE.MMO.Client.Resource.Sounds
{
    public class Sound : SoundInfoList
    {
        public void CreatePathList()
        {
            //SoundInfo BGMSound = new SoundInfo();
            //BGMSound.Path = "Background/Background.ogg";
            //BGMSound.Type = SoundType.BGM;
            //this.InfoList.Add(BGMSound);

            SoundInfo CharaWalkSound = new SoundInfo();
            CharaWalkSound.Path = "Effects/snow1.wav";
            CharaWalkSound.BufferID = SoundBufferID.Walk;
            this.InfoList.Add(CharaWalkSound);

            SoundInfo CharaJumpSound = new SoundInfo();
            CharaJumpSound.Path = "Effects/land4.wav";
            CharaJumpSound.BufferID = SoundBufferID.Jump;
            this.InfoList.Add(CharaJumpSound);
        }
    }
}
