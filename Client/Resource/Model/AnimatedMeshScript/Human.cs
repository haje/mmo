﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Renderer;
using OpenTK;
using System;

namespace HAJE.MMO.Client.Animation.AnimatedMeshScript
{
    public class Human : AnimatedMeshInfoList
    {
        public void CreateMeshList()
        {
            this.Name = "Human";

            AnimatedMeshInformation noseMesh = new AnimatedMeshInformation();
            noseMesh.BoneType = BoneType.Head;
            noseMesh.Name = BoneType.Head.ToString();
            noseMesh.MeshType = MeshType.SquarePyramid;
            noseMesh.Width = 0.189f;
            noseMesh.Length = 0.185f;
            noseMesh.StartOffset = 0.299f;

            AnimatedMeshInformation headMesh = new AnimatedMeshInformation();
            headMesh.BoneType = BoneType.Head;
            headMesh.Name = BoneType.Head.ToString();
            headMesh.MeshType = MeshType.Cube;
            headMesh.Width = 0.249f;
            headMesh.Length = 0.245f;

            AnimatedMeshInformation upperBodyMesh = new AnimatedMeshInformation();
            upperBodyMesh.BoneType = BoneType.Spine;
            upperBodyMesh.Name = BoneType.Spine.ToString();
            upperBodyMesh.MeshType = MeshType.Cube;
            upperBodyMesh.Width = 0.409f;
            upperBodyMesh.Length = 0.299f;

            AnimatedMeshInformation lowerBodyMesh = new AnimatedMeshInformation();
            lowerBodyMesh.BoneType = BoneType.Spine;
            lowerBodyMesh.Name = BoneType.Spine.ToString();
            lowerBodyMesh.MeshType = MeshType.SquarePyramid;
            lowerBodyMesh.Width = 0.409f;
            lowerBodyMesh.Length = 0.299f;
            lowerBodyMesh.StartOffset = -0.995f;

            AnimatedMeshInformation rightArmMesh = new AnimatedMeshInformation();
            rightArmMesh.BoneType = BoneType.RightArm;
            rightArmMesh.Name = BoneType.RightArm.ToString();
            rightArmMesh.MeshType = MeshType.Cube;
            rightArmMesh.Width = 0.121f;
            rightArmMesh.Length = 0.121f;
            rightArmMesh.StartOffset = 0.012f;

            AnimatedMeshInformation leftArmMesh = new AnimatedMeshInformation();
            leftArmMesh.BoneType = BoneType.LeftArm;
            leftArmMesh.Name = BoneType.LeftArm.ToString();
            leftArmMesh.MeshType = MeshType.Cube;
            leftArmMesh.Width = 0.121f;
            leftArmMesh.Length = 0.121f;
            leftArmMesh.StartOffset = 0.012f;

            AnimatedMeshInformation rightLegMesh = new AnimatedMeshInformation();
            rightLegMesh.BoneType = BoneType.RightLeg;
            rightLegMesh.Name = BoneType.RightLeg.ToString();
            rightLegMesh.MeshType = MeshType.Cube;
            rightLegMesh.Width = 0.196f;
            rightLegMesh.Length = 0.196f;

            AnimatedMeshInformation leftLegMesh = new AnimatedMeshInformation();
            leftLegMesh.BoneType = BoneType.LeftLeg;
            leftLegMesh.Name = BoneType.LeftLeg.ToString();
            leftLegMesh.MeshType = MeshType.Cube;
            leftLegMesh.Width = 0.196f;
            leftLegMesh.Length = 0.196f;

            //this.MeshInfoList.Add(noseMesh);
            this.MeshInfoList.Add(headMesh);
            this.MeshInfoList.Add(upperBodyMesh);
            //this.MeshInfoList.Add(lowerBodyMesh);
            this.MeshInfoList.Add(rightArmMesh);
            this.MeshInfoList.Add(leftArmMesh);
            this.MeshInfoList.Add(rightLegMesh);
            this.MeshInfoList.Add(leftLegMesh);
        }

        public void CreateAttachmentPoint()
        {
            AnimatedAttachmentInformation TestOrigin = new AnimatedAttachmentInformation();
            TestOrigin.BoneType = BoneType.LeftArm;
            TestOrigin.AP = AttachmentPoint.MagicWand;
            TestOrigin.DirectionVector = new Vector3(0, 0, 0);
            TestOrigin.Length = 0.5f;

            this.APList.Add(TestOrigin);
        }
    }
}
