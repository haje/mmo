﻿using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GamePlay
{
    public class MoveInput
    {
        Tuple<Key, MoveDirection>[] keyMap = new Tuple<Key, MoveDirection>[]
        {
            new Tuple<Key, MoveDirection>(Key.W, MoveDirection.Forward),
            new Tuple<Key, MoveDirection>(Key.A, MoveDirection.Left),
            new Tuple<Key, MoveDirection>(Key.D, MoveDirection.Right),
            new Tuple<Key, MoveDirection>(Key.S, MoveDirection.Backward),
        };

        public void Update(Second deltaTime, Input.KeyboardInput keyboard, PlayerCharacter character)
        {
            if (keyboard.IsDown(Key.Space) && State != MoveState.Jump)
            {
                character.PlayJumpingSound();
                State = MoveState.Jump;
                ElapsedJumpTime = Second.Zero;
            }

            #region jump
            if (State == MoveState.Jump)
            {
                ElapsedJumpTime += deltaTime;
                if (ElapsedJumpTime > MoveInput.JumpTime)
                {
                    ElapsedJumpTime = Second.Zero;
                    State = MoveState.Stand;
                }
            }
            #endregion
            #region walk/run
            if (State == MoveState.Stand || State == MoveState.Walk || State == MoveState.Run)
            {
                if (keyboard.InputHandled)
                {
                    inputStack.Clear();
                }
                else
                {
                    foreach (var km in keyMap)
                    {
                        var key = km.Item1;
                        var dir = km.Item2;
                        if (keyboard.IsPressing(key))
                        {
                            if (!inputStack.Contains(dir))
                                inputStack.Add(dir);
                        }
                        else
                        {
                            if (inputStack.Contains(dir))
                                inputStack.Remove(dir);
                        }
                    }
                }

                if (inputStack.Count == 0)
                {
                    Direction = MoveDirection.None;
                    State = MoveState.Stand;
                    character.StopWalkingSound();
                }
                else
                {
                    Direction = MoveDirection.None;
                    #region InputStack to Direction
                    foreach (MoveDirection md in inputStack)
                    {
                        if (Direction == MoveDirection.None)
                            Direction = md;
                        else if (Direction == MoveDirection.Forward)
                        {
                            if (md == MoveDirection.Left)
                                Direction = MoveDirection.ForwardLeft;
                            else if (md == MoveDirection.Right)
                                Direction = MoveDirection.ForwardRight;
                        }
                        else if (Direction == MoveDirection.Backward)
                        {
                            if (md == MoveDirection.Left)
                                Direction = MoveDirection.BackwardLeft;
                            else if (md == MoveDirection.Right)
                                Direction = MoveDirection.BackwardRight;
                        }
                        else if (Direction == MoveDirection.Left)
                        {
                            if (md == MoveDirection.Forward)
                                Direction = MoveDirection.ForwardLeft;
                            else if (md == MoveDirection.Backward)
                                Direction = MoveDirection.BackwardLeft;
                        }
                        else if (Direction == MoveDirection.Right)
                        {
                            if (md == MoveDirection.Forward)
                                Direction = MoveDirection.ForwardRight;
                            else if (md == MoveDirection.Backward)
                                Direction = MoveDirection.BackwardRight;
                        }
                    }
                    #endregion

                    character.PlayWalkingSound();

                    if (State == MoveState.Stand)
                    {
                        State = MoveState.Run;
                    }
                }
            }
            #endregion
        }

        public Second ElapsedJumpTime
        {
            get;
            private set;
        }
        public static Second JumpTime = (Second)1;

        public MoveState State { get; private set; }
        public MoveDirection Direction { get; private set; }

        List<MoveDirection> inputStack = new List<MoveDirection>();
    }
    
    public enum MoveDirection
    {
        None,
        Forward,
        ForwardLeft,
        ForwardRight,
        Backward,
        BackwardLeft,
        BackwardRight,
        Left,
        Right,
    }

    public enum MoveState
    {
        Stand,
        Walk,
        Run,
        Jump
    }
}
