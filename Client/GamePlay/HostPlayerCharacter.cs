﻿using HAJE.MMO.Client.Network.Field;
using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Sounds;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Spell;
using OpenTK;
using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GamePlay
{
    public class HostPlayerCharacter : PlayerCharacter
    {
        public HostPlayerCharacter(int characterId, PositionInfo positionInfo, MeshProvider meshProvider)
            : base(characterId, positionInfo, meshProvider)
        {
            spellList.Clear();
            spellEnableList.Clear();
            spellList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));

            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneSlowMissile));
            spellBookList.Add(SpellInfo.GetInfo(SpellId.ArcaneExplosion));
            isAlive = true;

            for (int i = 0; i < spellList.Count; i++)
                spellEnableList.Add(true);
        }

        public override void SetPositionInfo(PositionInfo info)
        {
            serverPosition.CopyFrom(info);
        }

        public override void Update(Second deltaTime, FieldOrganizer field)
        {
            base.Update(deltaTime, field);
            
            //Sound Listener update
            Vector3 posVec = new Vector3(Position.X, 0, Position.Y);
            AL.Listener(ALListener3f.Position, ref posVec);
            ALHelper.Check();
        }

        protected Vector3 ToFrontRightWorldYBasis(Vector3 inVector)
        {
            Vector3 viewDir = clientPosition.Direction.Normalized();
            Vector3 worldY = new Vector3(0, 1, 0);
            Vector3 rightDir = Vector3.Cross(viewDir, worldY).Normalized();
            Vector3 frontDir = Vector3.Cross(worldY, rightDir).Normalized();
            return inVector.X * frontDir + inVector.Y * rightDir + inVector.Z * worldY;
        }

        public void ChangeDirection(Vector2 cursorDelta)
        {
            if (cursorDelta.LengthSquared == 0) return;

            var dir = Direction;
            Vector2 cursorDeltaClamped = Vector2.Clamp(cursorDelta, Vector2.One * (float)(Degree)(-90.0f), Vector2.One * (float)(Degree)(90.0f));
            var yRot = cursorDeltaClamped.Y;
            var zRot = -cursorDeltaClamped.X;
            var rotMatY = Matrix4.CreateRotationY(yRot);
            var rotMatZ = Matrix4.CreateRotationZ(zRot);
            var rotMat = Matrix4.Mult(rotMatZ, rotMatY);

            Vector3 rotAxis;
            float rotAngle;
            rotMat.ExtractRotation().ToAxisAngle(out rotAxis, out rotAngle);
            rotAxis = ToFrontRightWorldYBasis(rotAxis).Normalized();
            Matrix4 newRotMat = Matrix4.CreateFromAxisAngle(rotAxis, rotAngle);
            Vector3.Transform(ref dir, ref newRotMat, out dir);

            clientPosition.Direction = dir;

            var sDir = serverPosition.Direction;
            var cDir = clientPosition.Direction;
            if (sDir != cDir)
            {
                syncReserved = true;
                timeToNextSync = (Second)Math.Min(timeToNextSync, 0.5f);
            }
        }

        protected override void InterpolatePositionInfo(Second deltaTime)
        {
            Interpolate(ref clientPosition.Position, serverPosition.Position, 1.5f, 2.0f, deltaTime);
            // 호스트의 경우는 전적으로 클라이언트의 속력과 방향벡터를 따른다.
        }

        public void SetMoveState(MoveState state, MoveDirection direction)
        {
            if (state == MoveState.Run)
            {
                corn.Run();
                clientPosition.MotionState = MotionState.Run;
            }
            else if (state == MoveState.Stand)
            {
                corn.Stand();
                clientPosition.MotionState = MotionState.Stop;
            }
            else if (state == MoveState.Jump)
            {
                corn.Jump();
                isJumping = true;
                clientPosition.MotionState = MotionState.Jump;
            }
            else if (state == MoveState.Walk)
            {
                corn.Walk();
                clientPosition.MotionState = MotionState.Walk;
            }

            Vector3 headingDir = corn.HeadingDirection;
            Vector2 dir = new Vector2(headingDir.X, headingDir.Z);
            #region MoveDirection to dir
            dir.Normalize();
            if (direction == MoveDirection.ForwardLeft)
                dir = (-Radian.Pi / 4).Rotate(dir);
            else if (direction == MoveDirection.ForwardRight)
                dir = (Radian.Pi / 4).Rotate(dir);
            else if (direction == MoveDirection.Backward)
                dir = -dir;
            else if (direction == MoveDirection.BackwardLeft)
                dir = -(Radian.Pi / 4).Rotate(dir);
            else if (direction == MoveDirection.BackwardRight)
                dir = -(-Radian.Pi / 4).Rotate(dir);
            else if (direction == MoveDirection.Right)
                dir = (Radian.Pi / 2).Rotate(dir);
            else if (direction == MoveDirection.Left)
                dir = -(Radian.Pi / 2).Rotate(dir);
            #endregion

            if (state == MoveState.Run)
                clientPosition.Velocity = dir * GamePlayConstant.RunSpeed;
            else if (state == MoveState.Walk)
                clientPosition.Velocity = dir * GamePlayConstant.WalkSpeed;
            else if (state == MoveState.Stand)
                clientPosition.Velocity = Vector2.Zero;
            
            if (clientPosition.MotionState != serverPosition.MotionState)
            {
                syncReserved = true;
                timeToNextSync = (Second)0;
            }
            else
            {
                var sVel = serverPosition.Velocity;
                var cVel = clientPosition.Velocity;
                if (sVel != cVel)
                {
                    syncReserved = true;
                    timeToNextSync = (Second)Math.Min(timeToNextSync, 0.25f);
                }
            }
        }

        public void SendActionRequest(Network.Field.FieldContext network, Second deltaTime)
        {
            if (syncReserved)
            {
                if (timeToNextSync <= Second.Zero)
                { 
                    syncReserved = false;
                    timeToNextSync = (Second)1.0f;
                    network.SendMoveStateSync(CharacterId, clientPosition);
                }
                else
                {
                    timeToNextSync -= deltaTime;
                }
            }
        }

        public void EquipSpell(int index, SpellInfo spell)
        {
            spellList[index] = spell;
        }

        protected override void ForceSyncPosition()
        {
            syncReserved = true;
            timeToNextSync = Second.Zero;
        }

        bool syncReserved = false;
        Second timeToNextSync = (Second)0;

        public int SelectedSpellIndex
        {
            get
            {
                return selectedSpellIndex;
            }
            set
            { 
                selectedSpellIndex = value;
                selectedSpellIndex = Math.Max(0, selectedSpellIndex);
                selectedSpellIndex = Math.Min(spellList.Count, selectedSpellIndex);
            }
        }

        public SpellInfo SelectedSpell
        {
            get
            {
                return spellList[selectedSpellIndex];
            }
        }

        public IReadOnlyList<Spell.SpellInfo> ActiveSpellList
        {
            get
            {
                return spellList;
            }
        }

        public IReadOnlyList<Spell.SpellInfo> SpellBookList
        {
            get
            {
                return spellBookList;
            }
        }

        public bool SpellEnable
        {
            get
            {
                return IsSpellEnable(SelectedSpell);
            }
        }

        public bool IsSpellEnable(SpellInfo spell)
        {
            var mpCost = spell.ManaCost;
            if (ManaPoint < mpCost) return false;

            var cpCost = spell.ComboPointCost;
            if (ComboPoint(spell.ElementType) < cpCost) return false;

            return true;
        }

        public bool IsSpellEnable(int index)
        {
            return IsSpellEnable(spellList[index]);
        }

        public bool CheckSpellEnableState()
        {
            bool changed = false;
            for (int i = 0; i < SpellCount; i++)
            {
                bool nowEnable = IsSpellEnable(i);
                if (nowEnable != spellEnableList[i])
                { 
                    spellEnableList[i] = nowEnable;
                    changed = true;
                }
            }

            return changed;
        }

        public int SpellCount
        {
            get
            {
                return spellEnableList.Count;
            }
        }

        public IReadOnlyList<bool> SpellEnableList
        {
            get
            {
                return spellEnableList;
            }
        }

        public void BeginCast()
        {
            BeginCast(SelectedSpell);
        }

        public bool IsAlive
        {
            get
            {
                return isAlive;
            }
            set
            {
                isAlive = value;
            }
        }

        List<SpellInfo> spellList = new List<Spell.SpellInfo>(2);
        List<SpellInfo> spellBookList = new List<Spell.SpellInfo>();
        List<bool> spellEnableList = new List<bool>(2);
        int selectedSpellIndex = 0;
        bool isAlive;
    }
}
