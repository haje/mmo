﻿using HAJE.MMO.Client.Animation;
using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.EffectSystem;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Client.Sounds;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GamePlay
{
    public abstract class PlayerCharacter
    {
        public PlayerCharacter(int characterId, PositionInfo positionInfo, MeshProvider meshProvider)
        {
            this.CharacterId = characterId;
            InitializePositionInfo(positionInfo);
            this.provider = meshProvider;
            corn = new Corn(meshProvider);
            pickingTarget = new Rendering.Picking.BoundingCylinder(
                corn.WorldSpaceEntity,
                0.5f,
                2.0f,
                Rendering.Picking.PickableType.Player,
                this);
            clientPosition.Direction = corn.HeadingDirection;

            playerWorldLabel = new WorldPositionLabel("Player", @"Resource/font/notosans-64b.fnt", 30, Vector3.Zero);
            playerWorldLabel.Color = OpenTK.Graphics.Color4.White;
            NameDescriptor = Rendering.WorldPositionLabelDescriptor.FromParticleSystem(playerWorldLabel, TextureBlendTypes.Attenuative);

            foreach (var e in SpellInfo.ElementTypes)
                comboPoints.Add(e, 0);

            isJumping = false;
        }

        public void InitializePositionInfo(PositionInfo info)
        {
            clientPosition.CopyFrom(info);
            serverPosition.CopyFrom(info);
        }

        public abstract void SetPositionInfo(PositionInfo info);

        public Vector2 WhenCaughtIn( Vector2 nowposition, Doodad doodad )
        {
            Vector2 unitVector = Vector2.Normalize( nowposition - doodad.CollisionPosition );
            Vector2 finalPosition = nowposition;
            float movingPos = 0.0f;
            movingPos += doodad.Radius;
            finalPosition += unitVector * movingPos;
            return finalPosition;
        }

        public virtual void Update(Second deltaTime, FieldOrganizer field)
        {
            corn.Update(deltaTime);

            clientPosition.Position += clientPosition.Velocity * deltaTime;
            serverPosition.Position += serverPosition.Velocity * deltaTime;

            clientPosition.AdjustInFieldPosition(field.GetField(FieldId).connection);
            serverPosition.AdjustInFieldPosition(field.GetField(serverPosition.FieldId).connection);

            Doodad CollideDoodad = field.GetField(FieldId).CheckCollision(clientPosition.Position, 0.0f);

            const int CollisionCheckLoop = 5;
            int CollisionCheck = CollisionCheckLoop;

            /*
            const int Boundary = 3;
            if ( clientPosition.Position.X < Boundary 
                || GamePlayConstant.DefualtFieldSize -Boundary < clientPosition.Position.X 
                || clientPosition.Position.Y < Boundary 
                || GamePlayConstant.DefualtFieldSize - Boundary < clientPosition.Position.Y )
            {
                clientPosition.Position -= clientPosition.Velocitiy * deltaTime;
                clientPosition.Velocitiy = Vector2.Zero;
                ForceSyncPosition();
            }
            */

            if (CollideDoodad != null)
            {
                clientPosition.Position -= clientPosition.Velocity * deltaTime;

                while ( CollisionCheck < 0 )
                {
                    CollisionCheck--;
                    CollideDoodad = field.GetField(FieldId).CheckCollision(clientPosition.Position, 0.0f);
                    if (CollideDoodad != null)
                    {
                        clientPosition.Position -= clientPosition.Velocity * deltaTime;
                    }
                    else
                    {
                        break;
                    }
                    if (CollisionCheck == 0)
                    {
                        clientPosition.Position = WhenCaughtIn(clientPosition.Position, CollideDoodad);
                    }
                }
                clientPosition.Velocity = Vector2.Zero;
                ForceSyncPosition();
            }

            if (isJumping)
                elapsedJumpTime += deltaTime;
            if (elapsedJumpTime > MoveInput.JumpTime)
                isJumping = false;

            InterpolatePositionInfo(deltaTime);

            if (IsCasting)
            {
                if (castingTime < CastingSpell.CastingTime 
                    && castingTime + deltaTime >= CastingSpell.CastingTime)
                {
                    EffectFactory.CreateEndCast(-1, corn.GetAP(AttachmentPoint.MagicWand));
                }
                castingTime += deltaTime;
                castingTime = (Second)Math.Min(castingTime, CastingSpell.CastingTime);
            }

            playerWorldLabel.Position = corn.Position + Vector3.UnitY * 1.5f;

            PlayerSoundBufferUpdate();
        }

        protected virtual void ForceSyncPosition()
        {
        }

        protected abstract void InterpolatePositionInfo(Second deltaTime);

        protected void Interpolate(ref Vector2 client, Vector2 server, float min, float k, Second deltaTime)
        {
            var diff = (client - server).Length;
            var delta = (min + diff * k) * deltaTime;
            if (diff < delta)
            {
                client = server;
            }
            else
            {
                var clientToServer = server - client;
                clientToServer.Normalize();
                client += clientToServer * delta;
            }
        }

        protected void Interpolate(ref Vector3 client, Vector3 server, float min, float k, Second deltaTime)
        {
            var diff = (client - server).Length;
            var delta = (min + diff * k) * deltaTime;
            if (diff < delta)
            {
                client = server;
            }
            else
            {
                var clientToServer = server - client;
                clientToServer.Normalize();
                client += clientToServer * delta;
            }
        }

        public void Draw(DeferredRenderer renderer, FieldOrganizer fields)
        {
            var pos = clientPosition.Position;
            var terrainLevel = fields.GetTerrainLevel(FieldId, pos + clientPosition.RelativeOfs);

            corn.Position = new Vector3(pos.X , terrainLevel + 0.75f + jumpHeight, pos.Y );
            corn.HeadingDirection = clientPosition.Direction;

            if (clientPosition.Velocity.LengthSquared > 0)
                corn.MovingDirection = Vector2.Normalize(clientPosition.Velocity);
            
            if (pickingTarget is Rendering.Picking.DynamicPickable)
            {
                var bsPickingTarget = pickingTarget as Rendering.Picking.DynamicPickable;
                bsPickingTarget.InvalidateCollisionObject();
            }

            corn.Draw(renderer);
            renderer.Render(NameDescriptor);
        }

        public void ReloadCharaMesh()
        {
            corn.ReloadCornMesh(provider);
        }

        public readonly int CharacterId;

        public int FieldId
        {
            get
            {
                return clientPosition.FieldId;
            }
        }

        public Vector2 Position
        {
            get
            {
                return clientPosition.Position;
            }
        }

        public Vector2 RelativePosition
        {
            get
            {
                return clientPosition.Position + clientPosition.RelativeOfs;
            }
        }

        public Vector3 Direction
        {
            get
            {
                return clientPosition.Direction;
            }
        }
        
        public Vector3 WandPosition
        {
            get
            {
                return corn.GetAP(AttachmentPoint.MagicWand).Position;
            }
        }

        public PositionInfo PositionInfo
        {
            get
            {
                return clientPosition;
            }
        }

        Effect castEffect;
        public virtual void BeginCast(SpellInfo spell)
        {
            if (IsCasting) return;
            castingTime = Second.Zero;
            CastingSpell = spell;
            corn.AimUp = true;
            
            castEffect = EffectFactory.CreateCastLoop(-1, corn.GetAP(AttachmentPoint.MagicWand));
        }

        public virtual void FinishCast()
        {
            if (!IsCasting) return;
            EndCast();
        }

        public virtual void CancelCast()
        {
            if (!IsCasting) return;
            EndCast();
        }

        protected void EndCast()
        {
            castingTime = Second.Zero;
            CastingSpell = null;
            corn.AimUp = false;

            if (castEffect != null)
                castEffect.Kill();
            castEffect = null;
        }

        public bool IsCasting
        {
            get
            {
                return CastingSpell != null;
            }
        }

        MeshProvider provider;

        protected bool _isJumping;
        protected bool isJumping
        {
            get
            {
                return _isJumping;
            }
            set
            {
                if (value == _isJumping)
                    return;

                _isJumping = value;
                elapsedJumpTime = (Second)0;
            }
        }
        private float elapsedJumpTime;
        private float maxJumpHeight = 3;
        private float jumpHeight
        {
            get
            {
                return Math.Max(0, elapsedJumpTime * (MoveInput.JumpTime - elapsedJumpTime) * maxJumpHeight);
            }
        }
        protected WorldPositionLabel playerWorldLabel;
        public void PlayerWorldLabelTextSet(string text)
        {
            playerWorldLabel.Label.Text = text;
        }
        
        public readonly Rendering.Picking.Pickable pickingTarget;
        protected PositionInfo clientPosition = new PositionInfo();
        protected PositionInfo serverPosition = new PositionInfo();
        protected Corn corn;
        Rendering.WorldPositionLabelDescriptor NameDescriptor;

        public SpellInfo CastingSpell { get; private set; }

        protected Second castingTime = Second.Zero;

        public float CastingTimeRatio
        {
            get
            {
                return castingTime / CastingSpell.CastingTime;
            }
        }

        public WorldSpaceEntity WorldSpaceEntity
        {
            get
            {
                return corn.WorldSpaceEntity;
            }
        }

        public int ComboPoint(Spell.ElementType elementType)
        {
            return comboPoints[elementType];
        }

        public void StatusSync(StatusType type, int value)
        {
            switch(type)
            {
                case StatusType.HitPoint:
                    hitPoint = value;
                    break;
                case StatusType.ManaPoint:
                    manaPoint = value;
                    break;
                default:
                    throw new NotImplementedException("알 수 없는 캐릭터 상태 타입: " + type);
            }
        }

        public void ComboPointSync(ElementType type, int value)
        {
            comboPoints[type] = value;
            //TODO: 임시 구현. 일단 Arcane 숫자만 동기화
            while (comboPoints[ElementType.Arcane] > comboPointEffect.Count)
            {
                var e = EffectFactory.CreateComboPoint(-1, corn.WorldSpaceEntity);
                comboPointEffect.Add(e);
            }
            while (comboPoints[ElementType.Arcane] < comboPointEffect.Count)
            {
                var last = comboPointEffect.Count - 1;
                var e = comboPointEffect[last];
                comboPointEffect.RemoveAt(last);
                e.Kill();
            }
        }

        Dictionary<Spell.ElementType, int> comboPoints = new Dictionary<Spell.ElementType, int>();
        int maxHitPoint = GamePlayConstant.MaxHitPoint;
        int hitPoint = GamePlayConstant.MaxHitPoint;
        int maxManaPoint = GamePlayConstant.MaxManaPoint;
        int manaPoint = GamePlayConstant.MaxManaPoint;

        public int MaxHitPoint
        {
            get
            {
                return maxHitPoint;
            }
        }

        public int HitPoint
        {
            get
            {
                return hitPoint;
            }
        }

        public int MaxManaPoint
        {
            get
            {
                return maxManaPoint;
            }
        }

        public int ManaPoint
        {
            get
            {
                return manaPoint;
            }
        }

        List<Effect> comboPointEffect = new List<Effect>();

        public void Dead()
        {
            if (IsCasting)
                CancelCast();
            foreach (var e in comboPointEffect)
                e.Kill();
            comboPointEffect.Clear();
        }

        //TODO : Player Character에서 발생할 sound의 버퍼 리스트.
        List<SoundBuffer> soundBufferList;
        SoundBuffer walkSoundBuffer;
        SoundBuffer jumpSoundBuffer;
        public void PlayWalkingSound()
        {
            //SoundManager를 통해 Source를 하나 할당받는다. 이 함수가 호출되었을 때 그 함수가 끝나있다면
            //sound를 플레이해준다.
            //이렇게 할당받은 녀석들은 자동으로 지우면 안된다.

            if(walkSoundBuffer == null)
            {
                walkSoundBuffer = GameSystem.SoundManager.GetSoundBuffer(SoundBufferID.Walk, Position);
                walkSoundBuffer.Play();
            }
            
            if(!walkSoundBuffer.IsPlaying)
            {
                GameSystem.SoundManager.RewindPlay(walkSoundBuffer);
            }
        }

        public void StopWalkingSound()
        {
            if(walkSoundBuffer != null)
            {
                walkSoundBuffer.Stop();
            }
        }

        public void PlayJumpingSound()
        {
            if (jumpSoundBuffer == null)
            {
                jumpSoundBuffer = GameSystem.SoundManager.GetSoundBuffer(SoundBufferID.Jump, Position);
                jumpSoundBuffer.Play();
            }

            if (!jumpSoundBuffer.IsPlaying)
            {
                GameSystem.SoundManager.RewindPlay(jumpSoundBuffer);
            }
        }

        public void PlayerSoundBufferUpdate()
        {
            //캐릭터가 이동중이라면 계속 세팅을 변경해주어야 한다.
            if(walkSoundBuffer != null)
            {
                walkSoundBuffer.SetPosition(Position);
            }
            
            if(jumpSoundBuffer != null)
            {
                jumpSoundBuffer.SetPosition(Position);
            }
        }
    }
}
