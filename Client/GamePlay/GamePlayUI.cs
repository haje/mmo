﻿using HAJE.MMO.Client.UI.Scenes;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.GamePlay
{
    public class GamePlayUI
    {
        public GamePlayUI(PlayContext gamePlay)
        {
            scene = new MainGameUIScene(gamePlay);
            this.gamePlay = gamePlay;
        }

        public void Show()
        {
            GameSystem.UISystem.ReplaceScene(scene);
        }

        public void SetSpellEnabled(IReadOnlyList<bool> enableList)
        {
            var spellList = scene.SpellBar.SpellList;
            Debug.Assert(spellList.Count == enableList.Count);
            for (int i = 0; i < enableList.Count; i++)
                spellList[i].Enabled = enableList[i];
        }

        int selectedSpellIndex = 0;
        public int SelectedSpellIndex
        {
            get
            {
                return selectedSpellIndex;
            }
            set
            {
                value = Math.Max(0, value);
                value = Math.Min(value, gamePlay.HostingCharacter.ActiveSpellList.Count);
                selectedSpellIndex = value;
                scene.SpellBar.SetSelectedIndex(value);
            }
        }

        int hp, hpMax, mp, mpMax;
        public void SetStatusBar(int hp, int hpMax, int mp, int mpMax)
        {
            if (this.hpMax != hpMax)
            {
                this.hpMax = hpMax;
                scene.HPBar.SetMaxResource(hpMax);
            }
            if (this.hp != hp)
            {
                this.hp = hp;
                scene.HPBar.SetCurrentResource(hp);
            }
            if (this.mpMax != mpMax)
            {
                this.mpMax = mpMax;
                scene.MPBar.SetMaxResource(mpMax);
            }
            if (this.mp != mp)
            {
                this.mp = mp;
                scene.MPBar.SetCurrentResource(mp);
            }
        }

        public void StartCast()
        {
            scene.CastingBar.ResetCastBar();
            scene.CastingBar.CastSpellImage.UpdatePath(scene.SpellBar.SpellList[selectedSpellIndex].Spell.IconPath);
            scene.CastingBar.SetCastTime(scene.SpellBar.SpellList[selectedSpellIndex].Spell.CastingTime);
        }

        public bool MousePressingCloseTime()
        {
            return scene.CastingBar.OnCloseTime;
        }

        public void CastCancel()
        {
            scene.CastingBar.Interrupt = true;
        }

        public bool SetSpellInventoryVisible()
        {
            if(scene.SpellInventory.Visible)
            {
                scene.SpellInventory.Visible = false;
                return false;
            }
                
            scene.SpellInventory.Visible = true;
            return true;
        }

        public void DeleteInvetorySpell(int row, int col)
        {
            scene.SpellInventory.SpellDelete(row, col);
        }

        public void SyncSpellCount(Spell.SpellId spellId, int value)
        {
            foreach (var ssf in scene.SpellBar.SpellList)
            {
                if (ssf.Spell.Id == spellId)
                    ssf.SpellIcon.SetSpellCount(value);
            }
            foreach (var gs in scene.SpellInventory.AllSpellSingList)
            {
                if (gs.Spell.Id == spellId)
                    gs.SetSpellCount(value);
            }
        }

        PlayContext gamePlay;
        MainGameUIScene scene;
    }
}
