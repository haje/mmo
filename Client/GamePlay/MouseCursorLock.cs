﻿using OpenTK;
using OpenTK.Input;
using System.Windows.Forms;

namespace HAJE.MMO.Client.GamePlay
{
    public class MouseCursorLock
    {
        public MouseCursorLock()
        {
            Enabled = true;
        }

        public Vector2 GetMovedDistance(bool isRevert)
        {
            var form = GameSystem.MainForm;
            if (!form.Focused || !enabled) return Vector2.Zero;

            if (!initialized)
            {
                center = new Vector2(GameSystem.MainForm.X + GameSystem.MainForm.Width / 2, GameSystem.MainForm.Y + GameSystem.MainForm.Height / 2);
                Mouse.SetPosition(center.X, center.Y);
                var state = Mouse.GetCursorState();
                centerMousePosition = new Vector2(state.X, state.Y);

                initialized = true;
                return Vector2.Zero;
            }
            else
            {
                center = new Vector2(GameSystem.MainForm.X + GameSystem.MainForm.Width / 2, GameSystem.MainForm.Y + GameSystem.MainForm.Height / 2);
                var state = Mouse.GetCursorState();
                var curPos = new Vector2(state.X, state.Y);
                var distance = curPos - center;

                Mouse.SetPosition(center.X, center.Y);
                state = Mouse.GetCursorState();
                centerMousePosition = new Vector2(state.X, state.Y);

                // 마우스 좌표계가 UI 좌표계와 y축이 반대
                // 출력 벡터를 UI 좌표계(화면상 위쪽이 큰 y값)와 맞추기 위해 뒤집는다.

                if(isRevert)
                {
                    distance.Y = -distance.Y;
                }

                return distance * 0.001f;
            }
        }

        Vector2 center;
        Vector2 centerMousePosition;
        bool initialized = false;

        bool enabled = false;
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (value)
                {
                    initialized = false;
                    Mouse.SetPosition(center.X, center.Y);
                    Cursor.Hide();
                }
                else
                    Cursor.Show();
                enabled = value;
            }
        }
    }
}
