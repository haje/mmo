﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Terrain;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GamePlay
{
    public class Field : IDisposable
    {
        public Field(int id, int type, Vector2 fieldPosition, MeshProvider meshProvider, FieldOrganizer fieldOrganizer)
        {
            for (int xidx = 0; xidx < cullingGridGranularity; ++xidx)
            {
                for (int yidx = 0; yidx < cullingGridGranularity; ++yidx)
                {
                    doodadMeshDescriptors[xidx, yidx] = new List<MeshDescriptor>();
                }
            }

            this.parentFieldOrganizer = fieldOrganizer;
            this.connection = new Dictionary<FieldConnectDirection, int>();
            this.Id = id;
            this.Type = type;
            this.FieldPosition = fieldPosition;
            this.parentPicker = null;

            Terrain = TerrainMeshDescriptor.FromIndex(type, new Vector3(fieldPosition.X, 0, fieldPosition.Y));

            var doodadList = FieldDescriptionFile.ReadDoodadFromFile(type);
            foreach (var d in doodadList)
            {
                var key = new Point()
                {
                    X = (int)d.Position.X,
                    Y = (int)d.Position.Y
                };
                if (!doodads.ContainsKey(key))
                {
                    var doodad = new Doodad(d, fieldPosition, this, meshProvider, DoodadMeshInformationProvider.Instance);
                    doodads.Add(key, doodad);
                    int gridX = (int)(d.Position.X * cullingGridGranularity / GamePlayConstant.DefaultFieldSize);
                    int gridY = (int)(d.Position.Y * cullingGridGranularity / GamePlayConstant.DefaultFieldSize);
                    doodadMeshDescriptors[gridX, gridY].Add(doodad.MeshDescriptor);
                }
            }
        }

        public void AddPickingTargetsTo(Rendering.Picking.Picker picker)
        {
            parentPicker = picker;
            parentPicker.AddTarget(Terrain.PickingTarget);
            foreach (var d in doodads.Values)
                parentPicker.AddTarget(d.PickingTarget);
        }

        public void SetConnection(int[] connection)
        {
            bool isHeightmapUpdateRequired = false;
            for (FieldConnectDirection direction = 0; (int)direction < connection.Length; direction++)
            {
                var connectedFieldId = connection[(int)direction];
                if (connectedFieldId >= 0)
                {
                    if (this.connection.ContainsKey(direction))
                    {
                        this.connection[direction] = connectedFieldId;
                    }
                    else
                    {
                        this.connection.Add(direction, connectedFieldId);
                    }
                    if (direction == FieldConnectDirection.Right ||
                        direction == FieldConnectDirection.UpperLeft ||
                        direction == FieldConnectDirection.UpperRight)
                    {
                        isHeightmapUpdateRequired = true;
                    }
                    if (direction == FieldConnectDirection.Left ||
                        direction == FieldConnectDirection.LowerLeft ||
                        direction == FieldConnectDirection.LowerRight)
                    {
                        var thatField = parentFieldOrganizer.GetField(connectedFieldId);
                        if (thatField != null)
                        {
                            thatField.updateHeightmap();
                        }
                    }
                }
            }
            if (isHeightmapUpdateRequired)
            {
                updateHeightmap();
            }
        }

        protected void updateHeightmap()
        {
            var prevHeightmap = Terrain.Heightmap;
            var prevHeightmapData = prevHeightmap.HeightmapRawData;
            int zDim = prevHeightmapData.GetLength(0);
            int xDim = prevHeightmapData.GetLength(1);
            float[,] newHeightmapData = new float[GamePlayConstant.DefaultFieldSize + 1, GamePlayConstant.DefaultFieldSize + 1];
            for (int zIndex = 0; zIndex < zDim; ++zIndex)
            {
                for (int xIndex = 0; xIndex < xDim; ++xIndex)
                {
                    // in-place copy
                    newHeightmapData[zIndex, xIndex] = prevHeightmapData[zIndex, xIndex];
                }
            }

            // update(augment) heightmap
            for (int index = 0; index < GamePlayConstant.DefaultFieldSize; ++index)
            {
                newHeightmapData[index, GamePlayConstant.DefaultFieldSize] = 0;
                newHeightmapData[GamePlayConstant.DefaultFieldSize, index] = 0;
            }
            newHeightmapData[GamePlayConstant.DefaultFieldSize, GamePlayConstant.DefaultFieldSize] = 0;

            if (connection.ContainsKey(FieldConnectDirection.Right))
            {
                var rightField = parentFieldOrganizer.GetField(connection[FieldConnectDirection.Right]);
                if (rightField != null)
                {
                    var rightFieldHeightmapRawData = rightField.Terrain.Heightmap.HeightmapRawData;
                    for (int zIndex = 0; zIndex < GamePlayConstant.DefaultFieldSize; ++zIndex)
                    {
                        newHeightmapData[zIndex, GamePlayConstant.DefaultFieldSize] = rightFieldHeightmapRawData[zIndex, 0];
                    }
                }
            }

            if (connection.ContainsKey(FieldConnectDirection.UpperLeft))
            {
                var upperLeftField = parentFieldOrganizer.GetField(connection[FieldConnectDirection.UpperLeft]);
                if (upperLeftField != null)
                {
                    var xOffset = (int)(FieldPosition.X - upperLeftField.FieldPosition.X);
                    var xLength = GamePlayConstant.DefaultFieldSize - xOffset;
                    var upperLeftFieldHeightmapRawData = upperLeftField.Terrain.Heightmap.HeightmapRawData;
                    for (int xIndex = 0; xIndex < xLength; ++xIndex)
                    {
                        newHeightmapData[GamePlayConstant.DefaultFieldSize, xIndex] = upperLeftFieldHeightmapRawData[0, xIndex + xOffset];
                    }
                }
            }

            if (connection.ContainsKey(FieldConnectDirection.UpperRight))
            {
                var upperRightField = parentFieldOrganizer.GetField(connection[FieldConnectDirection.UpperRight]);
                if (upperRightField != null)
                {
                    var xOffset = (int)(upperRightField.FieldPosition.X - FieldPosition.X);
                    var upperRightFieldHeightmapRawData = upperRightField.Terrain.Heightmap.HeightmapRawData;
                    for (int xIndex = xOffset; xIndex < GamePlayConstant.DefaultFieldSize + 1; ++xIndex)
                    {
                        newHeightmapData[GamePlayConstant.DefaultFieldSize, xIndex] = upperRightFieldHeightmapRawData[0, xIndex - xOffset];
                    }
                }
            }

            Heightmap newHeightmap = new Heightmap(newHeightmapData, prevHeightmap.GridSize, prevHeightmap.MinHeight, prevHeightmap.MaxHeight);
            if (parentPicker != null)
            {
                parentPicker.RemoveTarget(Terrain.PickingTarget);
            }
            Dispose();
            Terrain = TerrainMeshDescriptor.FromHeightmap(newHeightmap, new Vector3(FieldPosition.X, 0, FieldPosition.Y));
            if (parentPicker != null)
            {
                parentPicker.AddTarget(Terrain.PickingTarget);
            }
        }

        public int GetConnectedFieldId(FieldConnectDirection direction)
        {
            if (direction == FieldConnectDirection.MAX)
                return -1;

            return connection[direction];
        }

        public void UpdateCulledGrid(Vector2 cameraPos)
        {
            var relativeCameraPos = cameraPos - FieldPosition;
            int cameraGridX = (int)(relativeCameraPos.X * cullingGridGranularity / GamePlayConstant.DefaultFieldSize);
            int cameraGridY = (int)(relativeCameraPos.Y * cullingGridGranularity / GamePlayConstant.DefaultFieldSize);
            if (cameraGridX == prevCameraGrid.X && cameraGridY == prevCameraGrid.Y)
            {
                return;
            }
            gridsToDisplay.Clear();
            for (int xidx = 0; xidx < cullingGridGranularity; ++xidx)
            {
                for (int yidx = 0; yidx < cullingGridGranularity; ++yidx)
                {
                    float deltaX = cameraGridX - xidx;
                    float deltaY = cameraGridY - yidx;
                    float length = (float)Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
                    if (length < GamePlayConstant.CharacterSyncRange * cullingGridGranularity / GamePlayConstant.DefaultFieldSize)
                    {
                        gridsToDisplay.Add(new Point(xidx, yidx));
                    }
                }
            }
            prevCameraGrid.X = cameraGridX;
            prevCameraGrid.Y = cameraGridY;
        }

        public void Draw(DeferredRenderer renderer)
        {
            renderer.Render(Terrain);
            foreach (var grid in gridsToDisplay)
            {
                if (doodadMeshDescriptors[grid.X, grid.Y].Count > 0)
                    renderer.Render(doodadMeshDescriptors[grid.X, grid.Y]);
            }
        }

        public float GetTerrainLevel(Vector2 position)
        {
            return Terrain.GetHeight(position);
        }

        public Doodad CheckCollision(Vector2 position, float radius )
        {
            foreach (KeyValuePair<Point, Doodad> key in doodads)
            {
                if ((key.Value.CollisionPosition - position).Length < radius + key.Value.Radius)
                {
                    return key.Value;
                }
            }
            
            return null;
        }

        public void Dispose()
        {
            if (Terrain != null) Terrain.Dispose(); Terrain = null;
        }

        struct Point
        {
            public int X;
            public int Y;
            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

        public readonly int Id;
        public readonly int Type;
        public readonly Vector2 FieldPosition;
        public Dictionary<FieldConnectDirection, int> connection;
        public TerrainMeshDescriptor Terrain
        {
            get;
            protected set;
        }
        public static readonly int cullingGridGranularity = 25;
        Point prevCameraGrid = new Point(0, 0);
        
        Dictionary<Point, Doodad> doodads = new Dictionary<Point, Doodad>();
        List<MeshDescriptor>[,] doodadMeshDescriptors = new List<MeshDescriptor>[cullingGridGranularity, cullingGridGranularity];
        List<Point> gridsToDisplay = new List<Point>();
        FieldOrganizer parentFieldOrganizer;
        Rendering.Picking.Picker parentPicker;
    }
}
