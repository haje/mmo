﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Picking;
using HAJE.MMO.Client.Rendering.Renderer;
using OpenTK;
using System.Collections.Generic;
using System;

namespace HAJE.MMO.Client.GamePlay
{
    public class Presentation : IPresentation
    {
        DeferredPointLight cameraLight;
        DeferredPointLight[] cameraSubLights;
        public void DrawCameraLight(Vector3 cameraPosition, DeferredRenderer renderer)
        {
            const float y = 15.0f;
            const float intensity = 0.045f;
            const float radius = 45.0f;
            if (cameraLight == null)
            {
                cameraLight = new DeferredPointLight(Vector3.One, new Vector3(0.85f, 0.91f, 0.97f) * intensity, radius);
            }

            cameraPosition.Y += y;
            cameraLight.Position = cameraPosition;
            renderer.Render(cameraLight);
        }

        DeferredPointLight[] characterLights;
        public void DrawCharacterLight(Vector2 characterPosition, Vector3 direction, float terrainLevel, DeferredRenderer renderer)
        {
            const float y = 0.5f;
            const float intensity = 0.06f;
            const float radius = 10.0f;
            const int count = 4;
            if (characterLights == null)
            {
                characterLights = new DeferredPointLight[count];
                for (int i = 0; i < count; i++)
                    characterLights[i] = new DeferredPointLight(Vector3.One, new Vector3(0.82f, 0.86f, 0.87f) * intensity, radius);
            }
            for (int i = 0; i < count; i++)
            {
                const float r = 15.0f;
                var pos = new Vector3(characterPosition.X, terrainLevel + y, characterPosition.Y);
                var theta = (Radian)(i / (float)count * Radian.Pi * 2.0f / 3.0f) + Radian.Pi * 2.0f / 3.0f;
                var v = theta.Rotate(new Vector2(direction.X, direction.Z));
                pos.X += v.X * r;
                pos.Z += v.Y * r;
                characterLights[i].Position = pos;
            }
            renderer.Render(characterLights);
        }

        DeferredPointLight moonLight;
        public void DrawMoonLight(Vector3 cameraPosition, DeferredRenderer renderer)
        {
            const float y = 500.0f;
            const float intensity = 0.1f;
            const float radius = 1000.0f;
            if (moonLight == null)
            {
                moonLight = new DeferredPointLight(Vector3.One, new Vector3(0.89f, 0.95f, 0.96f) * intensity, radius, true);
            }
            cameraPosition.Y += y;
            moonLight.Position = cameraPosition;
            renderer.Render(moonLight);
        }

        public void SetCharacterCamera(Vector2 characterPosition, Vector3 characterLookingAt, float terrainLevel, Camera camera, Picker picker)
        {
            const float cameraY = 1.7f;
            Vector3 charPos = new Vector3(characterPosition.X, terrainLevel + cameraY, characterPosition.Y);
            Vector3 dir = characterLookingAt;
            Vector2 dir2D = new Vector2(dir.X, dir.Z);
            Vector2 right2D = (Radian.Pi / 2).Rotate(dir2D);
            Vector3 right = new Vector3(right2D.X, 0, right2D.Y);

            const float cameraMaxDistance = 5.25f;
            const float cameraTerrainOffset = 0.01f;
            const float cameraRightOffset = 1.3f;
            const float viewDistance = 50.0f;
            HashSet<Pickable> exclList = new HashSet<Pickable>();
            HashSet<PickableType> allowedTypeList = new HashSet<PickableType>();
            allowedTypeList.Add(PickableType.Terrain);
            Vector3 rayOrig = charPos + right * cameraRightOffset;
            var result = picker.Pick(rayOrig, -dir, exclList, allowedTypeList);
            float cameraDistance = Math.Min(cameraMaxDistance, (result.CollidePosition - rayOrig).Length - cameraTerrainOffset);
            Vector3 camPos = rayOrig - dir * cameraDistance;
            Vector3 viewTarget = charPos + dir * viewDistance;
            camera.Position = camPos;
            camera.LookingAtDirection = Vector3.Normalize(viewTarget - camera.Position);
            camera.zAlpha = cameraDistance;
        }
    }
}
