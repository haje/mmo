﻿using HAJE.MMO.Client.Rendering;
using OpenTK;

namespace HAJE.MMO.Client.GamePlay
{
    public class Doodad
    {
        public Doodad(FieldDoodad doodad, Vector2 fieldPosition, Field field, MeshProvider meshProvider, DoodadMeshInformationProvider doodadMeshInfoProvider)
        {
            MeshDescriptor = DoodadManager.CreateDoodadMeshDescriptor(
                doodad,
                field.Terrain,
                meshProvider,
                doodadMeshInfoProvider
            );
            MeshDescriptor.Position += new Vector3(fieldPosition.X, 0, fieldPosition.Y);
            PickingTarget = new Rendering.Picking.MeshPickingTarget(
                MeshDescriptor,
                Rendering.Picking.PickableType.Doodad,
                this
                );
            Name = doodad.Type;
            CollisionPosition = MeshDescriptor.Position.Xz;
            Radius = doodad.Radius;
            Field = field;
            Id = doodad.Id;
        }
        public readonly Vector2 CollisionPosition;
        public readonly int Id;
        public readonly float Radius;
        public readonly string Name;
        public readonly MeshDescriptor MeshDescriptor;
        public readonly Rendering.Picking.Pickable PickingTarget;
        public readonly Field Field;
    }
}
