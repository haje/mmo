﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.EffectSystem;
using HAJE.MMO.Client.Rendering.Picking;
using HAJE.MMO.Client.Rendering.Renderer;
using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GamePlay
{
    public class CursorParticle
    {
        public CursorParticle()
        {
            effectSystem = new EffectSystem();
            ppcDesc = ParticlePointCloudDescriptor.FromEffectSystem(effectSystem, TextureBlendTypes.Additive);
        }

        public void Update(Second deltaTime)
        {
            effectSystem.Update(deltaTime);
        }

        public void Draw(
            Camera camera,
            Picker picker,
            HostPlayerCharacter hostingPlayer,
            DeferredRenderer renderer)
        {
            excludeList.Clear();
            excludeList.Add(hostingPlayer.pickingTarget);
            var pickResult = picker.Pick(
                camera.Position + camera.zAlpha * camera.LookingAtDirection,
                camera.LookingAtDirection,
                excludeList,
                Picker.AllPickableTypes
            );

            if (pickResult.IsPicked)
            {
                if (effect == null)
                {
                    effect = EffectFactory.CreateCursorParticle(-1, effectSystem, Vector3.Zero);
                }
                effect.Position = new Vector3(pickResult.CollidePosition);
                GameSystem.DebugOutput.SetWatch("Type", pickResult.Type);
                GameSystem.DebugOutput.SetWatch("isPicked", "true");
                GameSystem.DebugOutput.SetWatch("Position", pickResult.CollidePosition.ToString());
            }
            else
            {
                if (effect != null)
                { 
                    effect.Kill();
                    effect = null;
                }
                GameSystem.DebugOutput.SetWatch("Type", "None");
                GameSystem.DebugOutput.SetWatch("isPicked", "false");
                GameSystem.DebugOutput.SetWatch("Position", pickResult.CollidePosition.ToString());
            }
            renderer.Render(ppcDesc);
        }


        HashSet<Pickable> excludeList = new HashSet<Pickable>();
        EffectSystem effectSystem;
        Effect effect;
        ParticlePointCloudDescriptor ppcDesc;
    }
}
