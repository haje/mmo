﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Picking;
using HAJE.MMO.Client.Rendering.Renderer;
using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GamePlay
{
    public interface IPresentation
    {
        void DrawCameraLight(Vector3 cameraPosition, DeferredRenderer renderer);
        void DrawCharacterLight(Vector2 characterPosition, Vector3 direction, float terrainLevel, DeferredRenderer renderer);
        void DrawMoonLight(Vector3 cameraPosition, DeferredRenderer renderer);
        void SetCharacterCamera(Vector2 characterPosition, Vector3 characterLookingAt, float terrainLevel, Camera camera, Picker picker);
    }
}
