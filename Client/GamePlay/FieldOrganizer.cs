﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Network.Packet;
using OpenTK;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.GamePlay
{
    public class FieldOrganizer
    {
        public FieldOrganizer(Network.Field.FieldContext network, MeshProvider meshProvider, Rendering.Picking.Picker picker)
        {
            this.network = network;
            this.meshProvider = meshProvider;
            this.picker = picker;
        }

        public void EnsureField(int fieldId)
        {
            if (IsFieldLoaded(fieldId) || IsFieldLoading(fieldId) || fieldId < 0)
                return;

            network.SendFieldRequest(fieldId);
            requestedIdSet.Add(fieldId);
        }

        public void SetFieldInfo(int fieldId, int terrainType, int[] connection)
        {
            if (fields.ContainsKey(fieldId))
            {
                var field = fields[fieldId];
                Debug.Assert(field.Type == terrainType);
                field.SetConnection(connection);
            }
            else
            {
                var field = new Field(fieldId, terrainType, GetFieldPosition(fieldId), meshProvider, this);
                field.AddPickingTargetsTo(picker);
                fields.Add(fieldId, field);
                field.SetConnection(connection);
                requestedIdSet.Remove(fieldId);
                //TODO : @stimo 임시로 서버에 있는 필드는 모두 로드하도록 한다.  
                foreach (var neighbor in connection)
                    EnsureField(neighbor);
            }
        }

        Vector2 GetFieldPosition(int fieldId)
        {
            foreach (var f in fields.Values)
            {
                foreach (var dir in f.connection.Keys)
                {
                    var size = GamePlayConstant.DefaultFieldSize;
                    if (fieldId == f.GetConnectedFieldId(dir))
                    {
                        switch (dir)
                        {
                            case FieldConnectDirection.Left:
                                return f.FieldPosition + new Vector2(-size, 0);
                            case FieldConnectDirection.LowerLeft:
                                return f.FieldPosition + new Vector2(-size / 2, -size);
                            case FieldConnectDirection.LowerRight:
                                return f.FieldPosition + new Vector2(size / 2, -size);
                            case FieldConnectDirection.Right:
                                return f.FieldPosition + new Vector2(size, 0);
                            case FieldConnectDirection.UpperLeft:
                                return f.FieldPosition + new Vector2(-size / 2, size);
                            case FieldConnectDirection.UpperRight:
                                return f.FieldPosition + new Vector2(size / 2, size);
                            default:
                                Debug.Assert(false);
                                break;
                        }
                    }
                }
            }
            return Vector2.Zero;
        }


        public float GetTerrainLevel(int fieldId, Vector2 position)
        {
            if (!fields.ContainsKey(fieldId))
                return 0;

            var field = fields[fieldId];
            return field.GetTerrainLevel(position);
        }

        public void UpdateCull(Vector2 cameraPos)
        {
            foreach (var f in fields.Values)
            {
                f.UpdateCulledGrid(cameraPos);
            }
        }

        public void Draw(DeferredRenderer renderer)
        {
            foreach (var f in fields.Values)
            {
                f.Draw(renderer);
            }
        }

        public TerrainMeshDescriptor GetTerrain(int fieldId)
        {
            if (fields.ContainsKey(fieldId))
            {
                return fields[fieldId].Terrain;
            }
            else
            {
                return null;
            }
        }

        public Field GetField(int fieldId )
        {
            if ( fields.ContainsKey(fieldId))
            {
                return fields[fieldId];
            }
            else
            {
                return null;
            }
        }

        public bool IsFieldLoaded(int fieldId)
        {
            return fields.ContainsKey(fieldId);
        }

        public bool IsFieldLoading(int fieldId)
        {
            return requestedIdSet.Contains(fieldId);
        }

        public int LoadingFieldCount
        {
            get
            {
                return requestedIdSet.Count;
            }
        }

        public void Dispose()
        {
            foreach (var f in fields.Values)
                f.Dispose();
            fields.Clear();
        }

        Network.Field.FieldContext network;
        SortedSet<int> requestedIdSet = new SortedSet<int>();
        Dictionary<int, Field> fields = new Dictionary<int, Field>();
        MeshProvider meshProvider;
        Rendering.Picking.Picker picker;
    }
}
