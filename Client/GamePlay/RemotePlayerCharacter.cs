﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Network.Packet;
using System;

namespace HAJE.MMO.Client.GamePlay
{
    public class RemotePlayerCharacter : PlayerCharacter
    {
        public RemotePlayerCharacter(int characterId, PositionInfo positionInfo, MeshProvider meshProvider)
            : base(characterId, positionInfo, meshProvider)
        {
        }

        public override void Update(Second deltaTime, FieldOrganizer field)
        {
            base.Update(deltaTime, field);

            PlayRemotePlayerSound(clientPosition.MotionState);
        }

        public override void SetPositionInfo(PositionInfo info)
        {
            if (info.MotionState != serverPosition.MotionState)
            {
                if (info.MotionState == MotionState.Stop)
                {
                    corn.Stand();
                    StopWalkingSound();
                }
                else if (info.MotionState == MotionState.Run)
                {
                    corn.Run();
                }
                else if (info.MotionState == MotionState.Jump)
                {
                    corn.Jump();
                    isJumping = true;
                }
                else if (info.MotionState == MotionState.Walk)
                {
                    corn.Walk();
                }
            }

            serverPosition.CopyFrom(info);
            clientPosition.MotionState = info.MotionState;
        }

        public void PlayRemotePlayerSound(MotionState state)
        {
            if (state == MotionState.Run)
            {
                PlayWalkingSound();
            }
            else if (state == MotionState.Jump)
            {
                PlayJumpingSound();
            }
        }

        protected override void InterpolatePositionInfo(Second deltaTime)
        {
            Interpolate(ref clientPosition.Position, serverPosition.Position, 1.5f, 2.0f, deltaTime);
            Interpolate(ref clientPosition.Velocity, serverPosition.Velocity, 3.0f, 4.0f, deltaTime);
            Interpolate(ref clientPosition.Direction, serverPosition.Direction, 0.5f, 0.5f, deltaTime);
            clientPosition.Direction.Normalize();
        }
    }
}
