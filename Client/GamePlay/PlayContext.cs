﻿using HAJE.MMO.Client.Rendering.EffectSystem;
using HAJE.MMO.Client.Rendering.Picking;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.GamePlay
{
    public class PlayContext : IDisposable
    {
        public PlayContext(int hostingCharacterId, Network.Field.FieldContext network)
        {
            this.network = network;
            this.hostingCharacterId = hostingCharacterId;
            this.IsLoadFinished = false;
            this.meshProvider = new Rendering.MeshProvider();
            //this.picker = new Rendering.Picking.BruteForceRayPicker();
            this.picker = new Rendering.Picking.BulletSharpRayPicker(GamePlayConstant.CharacterSyncRange);
            this.field = new FieldOrganizer(network, meshProvider, picker);
            this.log = GameSystem.Log;
            log.Write("PlayContext Created. hostingCharacterId={0}", hostingCharacterId);
        }

        public bool EnsureCharacterExists(int characterId)
        {
            //캐릭터가 없으면 dictionary에 캐릭터 넣을 공간을 마련하고 false 반환
            if (players.ContainsKey(characterId))
            {
                return true;
            }
            else
            {
                if (requestedPlayers.Contains(characterId))
                    return false;

                requestedPlayers.Add(characterId);
                network.SendCharacterRequest(characterId);
                return false;
            }
        }

        public void EncountPlayerCharacter(int characterId, string characterName, PositionInfo positionInfo)
        {
            log.Write("EncountPlayerCharacter characterId={0}", characterId);
            PlayerCharacter player;
            if (players.ContainsKey(characterId))
                return;

            if (hostingCharacter == null && characterId == hostingCharacterId)
            {
                hostingCharacter = new HostPlayerCharacter(characterId, positionInfo, meshProvider);
                player = hostingCharacter;
            }
            else
            {
                player = new RemotePlayerCharacter(characterId, positionInfo, meshProvider);
            }
            player.PlayerWorldLabelTextSet(characterName);
            field.EnsureField(player.FieldId);
            
            players.Add(characterId, player);
            if (requestedPlayers.Contains(characterId))
                requestedPlayers.Remove(characterId);
            picker.AddTarget(player.pickingTarget);
        }

        public void SetGamePlayUI(GamePlayUI ui)
        {
            this.ui = ui;
        }

        public void SetFieldInfo(int fieldId, int terrainType, int[] connection)
        {
            log.Write("SetFieldInfo fieldId={0}", fieldId);
            field.SetFieldInfo(fieldId, terrainType, connection);
            if (IsLoadFinished == false)
            {
                if (field.LoadingFieldCount == 0)
                {
                    IsLoadFinished = true;
                }
            }
        }

        public void SetPlayerPositionInfo(int characterId, PositionInfo positionInfo)
        {
            if (EnsureCharacterExists(characterId))
            {
                var player = players[characterId];

                Field positionedField = field.GetField(positionInfo.FieldId);
                if (positionedField == null) //필드 초기화가 되지 않았으면 정보를 받지 않는다.
                    return;
                Vector2 relativeOfs = field.GetField(positionInfo.FieldId).FieldPosition;
                positionInfo.Position = positionInfo.Position + relativeOfs;
                positionInfo.RelativeOfs = -relativeOfs;
                player.SetPositionInfo(positionInfo);
            }
        }

        public void StatusSync(int characterId, StatusType type, int value)
        {
            if (!EnsureCharacterExists(characterId)) return;

            var character = players[characterId];
            character.StatusSync(type, value);
        }

        public void ComboPointSync(int characterId, ElementType type, int value)
        {
            if (!EnsureCharacterExists(characterId)) return;

            var character = players[characterId];
            character.ComboPointSync(type, value);
        }

        public void SyncSpellCount(SpellId spellId, int value)
        {
            if (hostingCharacter == null) return;
            ui.SyncSpellCount(spellId, value);
        }

        public void KillPlayer(int characterId)
        {
            if (EnsureCharacterExists(characterId))
            {
                if(characterId == hostingCharacter.CharacterId)
                {
                    hostingCharacter.IsAlive = false;
                    UI.Scenes.DeadScene deadScene = new UI.Scenes.DeadScene();
                    GameSystem.UISystem.ReplaceScene(deadScene);
                }
                var player = players[characterId];
                player.Dead();
                players.Remove(characterId);
                picker.RemoveTarget(player.pickingTarget);
            }
        }

        public void BeginCast(int characterId, SpellInfo info)
        {
            if (EnsureCharacterExists(characterId))
            {
                var player = players[characterId];
                player.BeginCast(info);
            }
        }

        public void CancelCast(int characterId)
        {
            if (EnsureCharacterExists(characterId))
            {
                var player = players[characterId];
                player.CancelCast();
            }
        }

        public void FinishCast(int characterId)
        {
            if (EnsureCharacterExists(characterId))
            {
                var player = players[characterId];
                player.FinishCast();
            }
        }
        
        public void CreateEffect(int effectId, EffectType effectType, TargetType targetType,
            PositionInField targetPosition, int targetId, AttachmentPoint attachmentPoint)
        {
            if (effectType == EffectType.DropElement)
            {
                EffectFactory.CreateElementPoint(effectId, targetPosition.Position);
            }
            else if (effectType == EffectType.ArcaneMissile)
            {
                int fieldId = -1;
                Vector3 createPos = Vector3.Zero;
                if (targetType == TargetType.Player)
                {
                    if (!EnsureCharacterExists(targetId)) return;
                    var player = players[targetId];
                    fieldId = player.FieldId;
                    createPos = player.WandPosition;
                }
                else if (targetType == TargetType.Air)
                {
                    fieldId = targetPosition.FieldId;
                    createPos = targetPosition.Position;
                }
                else
                    return;

                var terrain = field.GetTerrain(fieldId);
                if (terrain == null) return;
                var arcaneMissile = SpellInfo.GetInfo(SpellId.ArcaneMissile);
                //EffectFactory.CreateArcaneMissile(effectId, terrain, createPos);
                //EffectFactory.CreateIceArrow(effectId, terrain, createPos);
                EffectFactory.CreatePyroblast(effectId, terrain, createPos);
            }
            else if (effectType == EffectType.ArcaneHit)
            {
                var position = targetPosition.Position;
                if (targetType == TargetType.Player)
                {
                    if (!EnsureCharacterExists(targetId)) return;
                    var player = players[targetId];
                    position = player.WandPosition; // TODO: AttachmentPoint
                }
                EffectFactory.CreateHitEffect(effectId, position);
            }
            else if (effectType == EffectType.ArcaneExplosion)
            {
                //EffectFactory.CreateArcaneExplosion(effectId, targetPosition.Position);
                EffectFactory.CreateFrostNova(effectId, targetPosition.Position);
            }
        }

        public void MoveEffect(int effectId, TargetType targetType,
            PositionInField targetPosition, int targetId, AttachmentPoint attachmentPoint, Second duration)
        {
            var effect = EffectFactory.FindEffect(effectId);
            if (effect == null) return;

            KeyFrameContainer<Vector3> poskf = new KeyFrameContainer<Vector3>();
            poskf.AddKeyFrame(Second.Zero, effect.Position, InterpolationMethod.Linear);
            switch (targetType) // TODO: 아직 코드 개판
            {
                case TargetType.Air:
                case TargetType.Ground:
                case TargetType.Doodad:
                    poskf.AddKeyFrame(duration, targetPosition.Position, InterpolationMethod.Linear);
                    break;
                case TargetType.Player:
                    if (EnsureCharacterExists(targetId))
                    {
                        var player = players[targetId];
                        var pos = player.WandPosition;
                        var vec2 = player.PositionInfo.Velocity;
                        var vec3 = new Vector3(vec2.X, 0, vec2.Y);
                        pos += vec3 * duration;
                        poskf.AddKeyFrame(duration, pos, InterpolationMethod.Linear);
                        // TODO: AttachmentPoint
                    }
                    break;
                default:
                    break;
            }
            effect.Move(poskf);
        }

        public void RemoveEffect(int effectId)
        {
            EffectFactory.RemoveEffect(effectId);
        }

        public void SendBeginCast()
        {
            var spell = hostingCharacter.CastingSpell;
            Debug.Assert(spell != null);

            var id = hostingCharacter.CharacterId;
            var pos = hostingCharacter.PositionInfo;
            network.SendBeginCast(id, pos, spell.Id);
        }

        public void SendFinishCast(Rendering.Camera camera)
        {
            var spell = hostingCharacter.CastingSpell;
            Debug.Assert(spell != null);

            var id = hostingCharacter.CharacterId;
            if (spell.AimType == AimType.Object)
            {
                pickExcludeSet.Clear();
                pickExcludeSet.Add(hostingCharacter.pickingTarget);
                pickTarget.Clear();
                pickTarget.Add(PickableType.Doodad);
                pickTarget.Add(PickableType.Player);

                var pickResult = picker.Pick(camera.Position + camera.zAlpha * camera.LookingAtDirection, camera.LookingAtDirection, pickExcludeSet, pickTarget);
                if (!pickResult.IsPicked)
                {
                    SendEndCast(spell);
                }
                else if (pickResult.Type == PickableType.Doodad)
                {
                    var target = pickResult.Owner as Doodad;
                    var targetPos = pickResult.CollidePosition;
                    var distance = (hostingCharacter.WandPosition - targetPos).Length;
                    if (distance <= spell.Range)
                        network.SendEndCastToDoodad(id, spell.Id, target.Id, target.Field.Id, targetPos);
                    else
                        SendEndCast(spell);
                }
                else if (pickResult.Type == PickableType.Player)
                {
                    var target = pickResult.Owner as PlayerCharacter;
                    var distance = (target.Position - hostingCharacter.Position).Length;
                    if (distance <= spell.Range)
                        network.SendEndCastToPlayer(id, spell.Id, target.CharacterId);
                    else
                        SendEndCast(spell);
                }
            }
            else if (spell.AimType == AimType.Ground)
            {
                pickExcludeSet.Clear();
                pickTarget.Clear();
                pickTarget.Add(PickableType.Terrain);
                var pickResult = picker.Pick(camera.Position, camera.LookingAtDirection, pickExcludeSet, pickTarget);
                if (!pickResult.IsPicked)
                {
                    CancelCast(hostingCharacter.CharacterId);
                }
                else if (pickResult.Type == PickableType.Terrain)
                {
                    var pos3 = pickResult.CollidePosition;
                    var pos2 = new Vector2(pos3.X, pos3.Z);
                    if ((hostingCharacter.Position - pos2).Length <= spell.Range)
                    {
                        network.SendEndCastToGround(id, spell.Id, hostingCharacter.FieldId, pos3);
                    }
                    else // Range 벗어나면 취소
                    {
                        CancelCast(hostingCharacter.CharacterId);
                    }
                }
            }
            else if (spell.AimType == AimType.Direction)
            {
                SendEndCast(spell);
            }
        }

        public void SendCancelCast()
        {
            var spell = hostingCharacter.CastingSpell;
            Debug.Assert(spell != null);

            var id = hostingCharacter.CharacterId;
            var pos = hostingCharacter.PositionInfo;
            network.SendCancelCast(id, pos, spell.Id);
        }

        public void SendEndCast(SpellInfo spell)
        {
            var dir = hostingCharacter.Direction;
            var dir2 = new Vector3(dir.X, dir.Y, dir.Z);
            dir2.Normalize();

            var from = hostingCharacter.WandPosition;
            var to = from + dir2 * spell.Range;
            var fieldId = hostingCharacter.FieldId;
            //TODO: field가 다를 수 있다

            network.SendEndCast(hostingCharacter.CharacterId, spell.Id, fieldId, to);
        }

        public void Update(Second deltaTime)
        {
            foreach (var p in players.Values)
                p.Update(deltaTime, field);

            hostingCharacter.SendActionRequest(network, deltaTime);
            if (picker is BulletSharpRayPicker)
            {
                (picker as BulletSharpRayPicker).UpdateCollisionObjects();
            }
            scheduler.Update(deltaTime);
        }

        public void UpdateCull(Vector2 cameraPos)
        {
            field.UpdateCull(cameraPos);
        }

        public void Draw(DeferredRenderer renderer)
        {
            field.Draw(renderer);
            foreach (var p in players.Values)
                p.Draw(renderer, field);
        }

        public HostPlayerCharacter HostingCharacter
        {
            get
            {
                return hostingCharacter;
            }
        }

        public Rendering.Picking.Picker Picker
        {
            get
            {
                return picker;
            }
        }

        public float GetTerrainLevel(int fieldId, Vector2 position)
        {
            return field.GetTerrainLevel(fieldId, position);
        }

        public void Dispose()
        {
            if (network != null)
                network.Dispose();

            if (picker != null && picker is IDisposable)
            {
                var disposablePicker = picker as IDisposable;
                disposablePicker.Dispose();
                picker = null;
            }

            if (field != null)
                field.Dispose();
            field = null;

            if (meshProvider != null)
                meshProvider.Dispose();
            meshProvider = null;
        }

        Network.Field.FieldContext network;
        HostPlayerCharacter hostingCharacter;
        Dictionary<int, PlayerCharacter> players = new Dictionary<int, PlayerCharacter>();
        List<int> requestedPlayers = new List<int>();
        FieldOrganizer field;
        Rendering.MeshProvider meshProvider;
        Rendering.Picking.Picker picker;
        readonly int hostingCharacterId;
        GamePlayUI ui;

        public bool IsLoadFinished { get; private set; }

        HashSet<Pickable> pickExcludeSet = new HashSet<Pickable>();
        HashSet<PickableType> pickTarget = new HashSet<PickableType>();
        LogDispatcher log;

        Scheduler scheduler = new Scheduler("playContext");
    }
}
