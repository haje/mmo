﻿namespace HAJE.MMO.Client
{
    [UnitTestAttribute]
    public static class PacketRWTest
    {
        public static bool RunUnitTest()
        {
            var writer = new HAJE.MMO.Network.PacketWriter();
            var reader = new HAJE.MMO.Network.PacketReader();

            int x = 0;

            if (x == 1)
            {
                int i = 1234567;
                writer.Write(i);
                byte[] buffer = writer.Buffer;
                reader.SetBuffer(buffer, buffer.Length);
                int result = reader.ReadInt32();
                string s = "result: " + result;
                UnitTest.Log.Write(s);
            }

            if (x == 2)
            {
                float f = 1234.567f;
                writer.Write(f);
                byte[] buffer = writer.Buffer;
                reader.SetBuffer(buffer, buffer.Length);
                float result = reader.ReadFloat();
                //string hexOutput = String.Format("{0:X}", result);
                string s = "result: " + result;
                UnitTest.Log.Write(s);
            }

            if (x == 3)
            {
                double d = 1234.567d;
                writer.Write(d);
                byte[] buffer = writer.Buffer;
                reader.SetBuffer(buffer, buffer.Length);
                double result = reader.ReadDouble();
                //string hexOutput = String.Format("{0:X}", result);
                string s = "result: " + result;
                UnitTest.Log.Write(s);

            }

            if (x == 4)
            {
                writer.Write("A quick brown fox jumps over the lazy dog");
                byte[] buffer = writer.Buffer;
                reader.SetBuffer(buffer, buffer.Length);
                string result = reader.ReadString();
                UnitTest.Log.Write(result);
            }

            // TODO : 상황에 맞게 true/false를 리턴하기.
            return true;
        }

    }

}
