﻿using OpenTK;

namespace HAJE.MMO.Client.Animation
{
    public class Pose
    {
        Quaternion?[] rotations;

        public Pose(Quaternion?[] rotations)
        {
            this.rotations = rotations;
        }

        public Pose()
        {
            rotations = new Quaternion?[Bone.BoneTypes.Count];
        }

        public void Initialize(AnimatedEntity entity)
        {
            for (int i = 0; i < Bone.BoneTypes.Count; i++)
            {
                BoneType bone = (BoneType)i;
                rotations[i] = entity.GetBone(bone).Rotation;
            }
        }

        public Quaternion? this[int index]
        {
            get
            {
                return rotations[index];
            }
            set
            {
                rotations[index] = value;
            }
        }

        public Quaternion? this[BoneType boneType]
        {
            get
            {
                return rotations[(int)boneType];
            }
            set
            {
                rotations[(int)boneType] = value;
            }
        }

        public static Pose FromWorldDirection(Vector3?[] worldDirections, AnimatedEntity targetEntity)
        {
            Quaternion[] oldPose = new Quaternion[Bone.BoneTypes.Count];
            Quaternion?[] newPose = new Quaternion?[Bone.BoneTypes.Count];
            targetEntity.EnsureTransform();
            for (int i = 0; i < Bone.BoneTypes.Count; i++)
            {
                BoneType bone = (BoneType)i;
                oldPose[i] = targetEntity[bone].Rotation;

                if (worldDirections[i] != null)
                {
                    targetEntity[bone].DirectionInWorld = Vector3.Normalize(worldDirections[i].Value);
                    newPose[i] = targetEntity[bone].Rotation;
                }
            }

            for (int i = 0; i < Bone.BoneTypes.Count; i++)
            {
                BoneType bone = (BoneType)i;
                targetEntity[bone].Rotation = oldPose[i];
            }

            return new Pose(newPose);
        }

        public static Pose HandForward(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0.2f);
            dirs[(int)BoneType.LeftArm] = new Vector3(0, 0, 1);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose Stand(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, 0);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, 0);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0);
            dirs[(int)BoneType.LeftArm] = new Vector3(0, -1, 0.1f);
            dirs[(int)BoneType.RightArm] = new Vector3(0, -1, 0.1f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose WalkL(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.08f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, 0.5f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, -0.5f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0);
            dirs[(int)BoneType.LeftArm] = new Vector3(0, -1, -0.3f);
            dirs[(int)BoneType.RightArm] = new Vector3(0, -1, 0.5f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose WalkR(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.08f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, -0.5f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, 0.5f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, -0);
            dirs[(int)BoneType.LeftArm] = new Vector3(0, -1, 0.3f);
            dirs[(int)BoneType.RightArm] = new Vector3(0, -1, -0.5f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose WalkN(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, 0);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, 0);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0);
            dirs[(int)BoneType.LeftArm] = new Vector3(0, -1, 0.1f);
            dirs[(int)BoneType.RightArm] = new Vector3(0, -1, 0.1f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose RunL(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0.1f);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.1f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0f);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0f);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, 0.9f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, -1.3f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, -0f);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0f);
            dirs[(int)BoneType.LeftArm] = new Vector3(-0.2f, -1, -1.0f);
            dirs[(int)BoneType.RightArm] = new Vector3(-0.1f, -1, 0.9f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose RunR(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0.1f);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.1f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0f);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0f);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, -1.3f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, 0.9f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0f);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, -0f);
            dirs[(int)BoneType.LeftArm] = new Vector3(0.1f, -1, 0.9f);
            dirs[(int)BoneType.RightArm] = new Vector3(0.2f, -1, -1.0f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose RunN(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0.0f);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.2f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, 0);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, 0);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0);
            dirs[(int)BoneType.LeftArm] = new Vector3(-0.1f, -1, 0.1f);
            dirs[(int)BoneType.RightArm] = new Vector3(0.1f, -1, 0.1f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose RunLeftL(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0.1f, 1, -0.1f);
            dirs[(int)BoneType.Spine] = new Vector3(0.15f, 1, 0.1f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0.8f);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, -0.8f);
            dirs[(int)BoneType.LeftLeg] = new Vector3(-1, -1, 0.0f);
            dirs[(int)BoneType.RightLeg] = new Vector3(1, -1, -0.0f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0.3f);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, -0.3f);
            dirs[(int)BoneType.LeftArm] = new Vector3(0.5f, -1, 0.2f);
            dirs[(int)BoneType.RightArm] = new Vector3(-0.3f, -1, -0.5f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose RunLeftR(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0.1f, 1, -0.1f);
            dirs[(int)BoneType.Spine] = new Vector3(0.15f, 1, 0.1f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0.8f);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, -0.8f);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0.5f, -1, 0.1f);
            dirs[(int)BoneType.RightLeg] = new Vector3(-0.6f, -1, -0.1f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0.9f);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, -0.9f);
            dirs[(int)BoneType.LeftArm] = new Vector3(-0.9f, -1, 0f);
            dirs[(int)BoneType.RightArm] = new Vector3(0.5f, -1, 0.2f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose RunLeftN(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0.1f, 1, -0.1f);
            dirs[(int)BoneType.Spine] = new Vector3(0.1f, 1, 0.1f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0.8f);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, -0.8f);
            dirs[(int)BoneType.LeftLeg] = new Vector3(-0.1f, -1, 0.1f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0.2f, -1, -0.1f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0.6f);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, -0.6f);
            dirs[(int)BoneType.LeftArm] = new Vector3(-0.2f, -1, 0.2f);
            dirs[(int)BoneType.RightArm] = new Vector3(0.1f, -1, -0.3f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose JumpRdy(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, 0.2f);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.3f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -1.5f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -1.5f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(0, -1, 0.5f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0, -1, 0.5f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0);
            dirs[(int)BoneType.LeftArm] = new Vector3(-0.3f, -1, -0.8f);
            dirs[(int)BoneType.RightArm] = new Vector3(0.3f, -1, -0.8f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        public static Pose JumpAir(AnimatedEntity entity)
        {
            Vector3?[] dirs = new Vector3?[Bone.BoneTypes.Count];
            dirs[(int)BoneType.Head] = new Vector3(0, 1, -0.1f);
            dirs[(int)BoneType.Spine] = new Vector3(0, 1, 0.1f);
            dirs[(int)BoneType.LeftPelvis] = new Vector3(-1, -0f, 0);
            dirs[(int)BoneType.RightPelvis] = new Vector3(1, -0f, 0);
            dirs[(int)BoneType.LeftLeg] = new Vector3(-0.1f, -1, -0.2f);
            dirs[(int)BoneType.RightLeg] = new Vector3(0.1f, -1, 0.1f);
            dirs[(int)BoneType.LeftShoulder] = new Vector3(-1, 0, 0);
            dirs[(int)BoneType.RightShoulder] = new Vector3(1, 0, 0);
            dirs[(int)BoneType.LeftArm] = new Vector3(-1, -1, 0.2f);
            dirs[(int)BoneType.RightArm] = new Vector3(1, -1, 0.2f);
            return Pose.FromWorldDirection(dirs, entity);
        }

        

        

    }
}
