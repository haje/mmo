﻿using OpenTK;

namespace HAJE.MMO.Client.Animation
{
    public static class AnimationFactory
    {
        public static Animation CreateWalk(AnimatedEntity entity)
        {
            var animation = new Animation(entity);
            var standPose = Pose.Stand(entity);
            var walkL = Pose.WalkL(entity);
            var walkR = Pose.WalkR(entity);
            var walkN = Pose.WalkN(entity);
            animation.AddPose(walkN, (Second)0.2f);
            animation.AddPose(walkL, (Second)0.3f);
            animation.AddPose(walkN, (Second)0.2f);
            animation.AddPose(walkR, (Second)0.3f);
            return animation;
        }

        public static Animation CreateRun(AnimatedEntity entity)
        {
            var animation = new Animation(entity);
            var standPose = Pose.Stand(entity);
            var runL = Pose.RunL(entity);
            var runR = Pose.RunR(entity);
            var runN = Pose.RunN(entity);
            animation.AddPose(runN, (Second)0.15f);
            animation.AddPose(runL, (Second)0.20f);
            animation.AddPose(runN, (Second)0.15f);
            animation.AddPose(runR, (Second)0.20f);
            return animation;
        }

        public static Animation CreateRunLeft(AnimatedEntity entity)
        {
            var animation = new Animation(entity);
            var standPose = Pose.Stand(entity);
            var runL = Pose.RunLeftL(entity);
            var runR = Pose.RunLeftR(entity);
            var runN = Pose.RunLeftN(entity);
            animation.AddPose(runN, (Second)0.15f);
            animation.AddPose(runL, (Second)0.20f);
            animation.AddPose(runN, (Second)0.15f);
            animation.AddPose(runR, (Second)0.20f);
            return animation;
        }

        public static Animation CreateJump(AnimatedEntity entity)
        {
            var animation = new Animation(entity);
            var standPose = Pose.Stand(entity);
            var jumpReady = Pose.JumpRdy(entity);
            var jumpAir = Pose.JumpAir(entity);
            animation.AddPose(jumpReady, (Second)0.3f);
            animation.AddPose(jumpAir, (Second)0.4f);
            animation.AddPose(jumpReady, (Second)0.2f);
            animation.AddPose(standPose, (Second)0.1f);

            return animation;
        }
    }
}
