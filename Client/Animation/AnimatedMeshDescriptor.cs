﻿using OpenTK;
using System.Collections.Generic;
using System.Xml.Serialization;
using HAJE.MMO.Client.Rendering;

namespace HAJE.MMO.Client.Animation
{
    public class AnimatedMeshInformation
    {
        public string Name = "";
        public BoneType BoneType = BoneType.Root;
        public MeshType MeshType = MeshType.TrianglePyramid;
        public float Width = 0.1f;
        public float Length = 0.1f;
        public bool IsOpposite = false;
        public Vector3 DirectionVector = new Vector3(1, 0, 0);
        public float StartOffset = 0.0f;
        public float Height = 0.0f;
    }

    public class AnimatedMeshDescriptor : MeshDescriptor
    {
        public AnimatedMeshDescriptor(BoneType type, Mesh mesh)
            :base(Vector3.Zero, Vector3.UnitX, Vector3.UnitY, mesh)
        {
            this.type = type;
            this.info = new AnimatedMeshInformation();
            this.info.BoneType = type;
        }

        public AnimatedMeshDescriptor(AnimatedMeshInformation info, Mesh mesh)
            :base(Vector3.Zero, Vector3.UnitX, Vector3.UnitY, mesh)
        {
            this.info = info;
            this.type = info.BoneType;
        }

        private BoneType type;
        public BoneType Type
        {
            get
            {
                return type;
            }
        }

        private AnimatedMeshInformation info;
        public AnimatedMeshInformation Info
        {
            get
            {
                return info;
            }
        }

        private Matrix4 modelMatrix = Matrix4.Identity;
        public override Matrix4 ModelMatrix
        {
            get
            {
                return modelMatrix;
            }
        }

        public virtual void UpdateModelMatrix(Bone bone, Matrix4 ownerMatrix)
        {
            //offset만큼 translation
            Vector3 transVec;
            
            if(info.IsOpposite)
            {
                transVec = new Vector3(bone.Length - info.StartOffset, 0, 0);
            }
            else
            {
                transVec = new Vector3(info.StartOffset, 0, 0);
            }

            //if MeshType is cube
            if (this.info.MeshType == MeshType.Cube)
            {
                if(info.IsOpposite)
                {
                    transVec.X = transVec.X - (info.Height * 0.5f);
                }
                else
                {
                    transVec.X = transVec.X + info.Height * 0.5f;
                }
            }

            Matrix4 transMat = Matrix4.CreateTranslation(transVec);

            //Rotation Matrix
            Vector3 rotateVec = info.DirectionVector;
            if (rotateVec == Vector3.Zero)
            {
                rotateVec = bone.DirectionInWorld;
            }

            if(info.IsOpposite)
            {
                rotateVec *= -1;
            }

            Matrix4 rotateMat;
            var axis = Vector3.Cross(Vector3.UnitX, rotateVec).Normalized();
            if (float.IsNaN(axis.Length))   // rotateVec is the same as Vector3.UnitX
            {
                rotateMat = Matrix4.Identity;
            }
            else
            {
                var angle = Vector3.CalculateAngle(Vector3.UnitX, rotateVec);
                rotateMat = Matrix4.CreateFromAxisAngle(axis, angle);
            }

            if (info.Height <= 0.0f)
            {
                //Height이 지정되지 않았다면 bone.Length를 사용한다.
                info.Height = bone.Length;
            }

            //Scale Matrix
            Vector3 scaleVec = new Vector3(info.Height, info.Width, info.Length);
            Matrix4 scaleMat = Matrix4.CreateScale(scaleVec);

            Matrix4 transformMat = scaleMat * rotateMat * transMat;

            modelMatrix = transformMat * bone.ThisToModelTransform * ownerMatrix;
        }
    }
}

