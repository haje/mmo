﻿using OpenTK;
using System.Diagnostics;

namespace HAJE.MMO.Client.Animation
{
    public class PoseBlender
    {
        public PoseBlender(AnimatedEntity owner)
        {
            this.owner = owner;
            this.ownerPose = new Pose();
        }

        public void ApplyPose(Pose pose, Second length)
        {
            ownerPose.Initialize(owner);
            foreach (BoneType b in Bone.BoneTypes)
            {
                if (pose[b] == null)
                    ownerPose[b] = null;
            }
            ApplyPose(ownerPose, pose, length);
        }

        public void ApplyPose(Pose from, Pose to, Second length)
        {
            const float minLength = 0.005f;
            if (length <= (Second)minLength) length = (Second)minLength;
            this.from = from;
            this.to = to;
            foreach (BoneType b in Bone.BoneTypes)
            {
                Debug.Assert(from[b].HasValue == to[b].HasValue);
            }
            this.length = length;
            this.time = (Second)0;
        }

        public void ClearPose()
        {
            from = to = null;
        }

        public void Update(Second deltaTime)
        {
            if (from == null || to == null) return;

            if (time < length)
            {
                time += deltaTime;
                if (time > length)
                    time = length;
            }

            float blend = time / length;
            owner.BlendPose(from, to, blend);
        }

        readonly AnimatedEntity owner;
        Pose ownerPose;
        Pose from;
        Pose to;
        Second length;
        Second time;
    }
}
