﻿using OpenTK;

namespace HAJE.MMO.Client.Animation
{
    public class BoneDescriptor
    {
        public BoneType Type;
        public float Length;
        public Vector3 DefaultDirection;

        public BoneDescriptor(BoneType type)
        {
            this.Type = type;
        }
    }
}
