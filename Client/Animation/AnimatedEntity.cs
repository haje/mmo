﻿using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Animation
{
    [UnitTestAttribute]
    public class AnimatedEntity : Rendering.WorldSpaceEntity
    {
        Bone[] bones;

        public AnimatedEntity(AnimatedEntityDescriptor desc)
            : base(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY)
        {
            var boneTypes = Bone.BoneTypes;
            bones = new Bone[boneTypes.Count];
            bones[(int)BoneType.Root] = Bone.CreateRoot();
            for (int i = 0; i < boneTypes.Count; i++)
            {
                if (i == (int)BoneType.Root)
                    continue;
                bones[i] = new Bone(boneTypes[i], desc[i].Length);
            }

            GetBone(BoneType.Root).AddChild(GetBone(BoneType.Spine));
            GetBone(BoneType.Spine).AddChild(GetBone(BoneType.Head));
            GetBone(BoneType.Root).AddChild(GetBone(BoneType.RightPelvis));
            GetBone(BoneType.RightPelvis).AddChild(GetBone(BoneType.RightLeg));
            GetBone(BoneType.Root).AddChild(GetBone(BoneType.LeftPelvis));
            GetBone(BoneType.LeftPelvis).AddChild(GetBone(BoneType.LeftLeg));
            GetBone(BoneType.Spine).AddChild(GetBone(BoneType.RightShoulder));
            GetBone(BoneType.RightShoulder).AddChild(GetBone(BoneType.RightArm));
            GetBone(BoneType.Spine).AddChild(GetBone(BoneType.LeftShoulder));
            GetBone(BoneType.LeftShoulder).AddChild(GetBone(BoneType.LeftArm));

            for (int i = 0; i < boneTypes.Count; i++)
            {
                bones[i].DirectionInWorld = desc[i].DefaultDirection;
            }
        }

        public Bone GetBone(BoneType type)
        {
            return bones[(int)type];
        }

        public Bone this[BoneType type]
        {
            get
            {
                return GetBone(type);
            }
        }

        public void BlendPose(Pose from, Pose to, float blend)
        {
            foreach (BoneType b in Bone.BoneTypes)
            {
                if (from[b] != null)
                {
                    var rotation = Quaternion.Slerp(from[b].Value, to[b].Value, blend);
                    GetBone(b).Rotation = rotation;
                }
            }
        }

        public void EnsureTransform()
        {
            EnsureTransform(GetBone(BoneType.Root));
        }
        
        void EnsureTransform(Bone bone)
        {
            bone.EnsureTransform();

            foreach (var c in bone.Children)
            {
                EnsureTransform(c);
            }
        }

        public static bool RunUnitTest()
        {
            UnitTest.Begin("AnimatedEntity");
            var animation = new AnimatedEntity(AnimatedEntityDescriptor.Human);
            animation.EnsureTransform();

            foreach (var b in Bone.BoneTypes)
            {
                var transform = animation[b].ThisToModelTransform;
                var boneFrom = Vector3.Transform(Vector3.Zero, transform);
                var boneTo = Vector3.Transform(new Vector3(animation[b].Length, 0, 0), transform);
                var boneDirection = animation[b].DirectionInWorld;
                UnitTest.Log.Write("{0} : ({1:F2}, {2:F2}, {3:F2}) -> ({4:F2}, {5:F2}, {6:F2}) - Dir:({7:F2}, {8:F2}, {9:F2})",
                    b.ToString(),
                    boneFrom.X, boneFrom.Y, boneFrom.Z,
                    boneTo.X, boneTo.Y, boneTo.Z,
                    boneDirection.X, boneDirection.Y, boneDirection.Z);
            }

            UnitTest.End();

            // TODO : 상황에 맞게 true/false를 리턴하기.
            return true;
        }
    }
}
