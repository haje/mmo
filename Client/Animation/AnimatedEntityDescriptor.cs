﻿using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Animation
{
    public class AnimatedEntityDescriptor
    {
        public AnimatedEntityDescriptor()
        {
            bones = new List<BoneDescriptor>(Bone.BoneTypes.Count);
            for (int i = 0; i < Bone.BoneTypes.Count; i++)
                bones.Add(new BoneDescriptor((BoneType)i));

            bones[(int)BoneType.Root].Type = BoneType.Root;
            bones[(int)BoneType.Root].DefaultDirection = Vector3.UnitX;
            bones[(int)BoneType.Root].Length = 0;
        }

        List<BoneDescriptor> bones;
        public BoneDescriptor this[int index]
        {
            get
            {
                return bones[index];
            }
        }

        public BoneDescriptor this[BoneType type]
        {
            get
            {
                return bones[(int)type];
            }
        }

        static AnimatedEntityDescriptor human;
        public static AnimatedEntityDescriptor Human
        {
            get
            {
                if (human == null)
                {
                    human = new AnimatedEntityDescriptor();

                    var root = human[BoneType.Root];
                    root.Length = 0.0f;
                    root.DefaultDirection = new Vector3(1, 0, 0);

                    var spine = human[BoneType.Spine];
                    spine.Length = 0.666f;
                    spine.DefaultDirection = new Vector3(0, 1, 0);

                    var head = human[BoneType.Head];
                    head.Length = 0.299f;
                    head.DefaultDirection = new Vector3(0, 1, 0);

                    var rplv = human[BoneType.RightPelvis];
                    var lplv = human[BoneType.LeftPelvis];
                    rplv.Length = 0.109f;
                    lplv.Length = 0.109f;
                    rplv.DefaultDirection = new Vector3(1, 0, 0);
                    lplv.DefaultDirection = new Vector3(-1, 0, 0);

                    var rleg = human[BoneType.RightLeg];
                    var lleg = human[BoneType.LeftLeg];
                    rleg.Length = 1.036f;
                    lleg.Length = 1.036f;
                    rleg.DefaultDirection = new Vector3(0, -1, 0);
                    lleg.DefaultDirection = new Vector3(0, -1, 0);

                    var rsld = human[BoneType.RightShoulder];
                    var lsld = human[BoneType.LeftShoulder];
                    rsld.Length = 0.224f;
                    lsld.Length = 0.224f;
                    rsld.DefaultDirection = new Vector3(1, 0, 0);
                    lsld.DefaultDirection = new Vector3(-1, 0, 0);

                    var rarm = human[BoneType.RightArm];
                    var larm = human[BoneType.LeftArm];
                    rarm.Length = 0.925f;
                    larm.Length = 0.925f;
                    rarm.DefaultDirection = new Vector3(0, -1, 0);
                    larm.DefaultDirection = new Vector3(0, -1, 0);
                }

                return human;
            }
        }
    }
}
