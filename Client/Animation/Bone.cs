﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.Animation
{
    public enum BoneType
    {
        Root = 0,
        Spine,
        Head,
        RightPelvis,
        RightLeg,
        LeftPelvis,
        LeftLeg,
        RightShoulder,
        RightArm,
        LeftShoulder,
        LeftArm,
    }

    public class Bone
    {
        public readonly BoneType BoneType;
        public readonly float Length;

        public static Bone CreateRoot()
        {
            return new Bone(BoneType.Root, 0);
        }

        public Bone(BoneType type, float length)
        {
            this.BoneType = type;
            this.Length = length;
        }

        List<Bone> children = new List<Bone>();

        Bone parent;

        public void AddChild(Bone bone)
        {
            Debug.Assert((int)BoneType < (int)bone.BoneType,
                "BoneType은 항상 위상 순서대로 정렬되어있어야 합니다.");
            bone.parent = this;
            children.Add(bone);
        }

        public IReadOnlyList<Bone> Children
        {
            get
            {
                return children.AsReadOnly();
            }
        }

        Quaternion rotation = Quaternion.Identity;
        public Vector3 DirectionInWorld
        {
            get
            {
                ForceEnsureTransform();
                var fromInLocal = Vector3.Zero;
                var toInLocal = new Vector3(Length, 0, 0);
                var fromInModel = Vector3.Transform(fromInLocal, thisToModelTransform);
                var toInModel = Vector3.Transform(toInLocal, thisToModelTransform);
                return Vector3.Normalize(toInModel - fromInModel);
            }
            set
            {
                Vector3 originInModel = Vector3.Zero;
                Vector3 targetInModel = value;

                if(targetInModel == -Vector3.UnitX)
                {
                    targetInModel.Y = -0.1f;
                }

                var modelToParent = Matrix4.Identity;
 
                if(parent != null)
                {
                    parent.ForceEnsureTransform();

                    modelToParent = Matrix4.Invert(parent.thisToModelTransform);
                }

                var originInParent = Vector3.Transform(originInModel, modelToParent);
                var directionInParent = Vector3.Transform(targetInModel, modelToParent);

                var unitX = Vector3.UnitX;
                var direction = Vector3.Normalize(directionInParent - originInParent);

                if(direction.Y == 0 && direction.Z == 0)
                {
                    direction = Vector3.UnitY;
                }

                var axis = Vector3.Cross(unitX, direction);
                float angle = Vector3.CalculateAngle(unitX, direction);

                rotation = Quaternion.FromAxisAngle(axis, angle);

                MarkTransformNeedUpdate();
            }
        }

        public Quaternion Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                MarkTransformNeedUpdate();
            }
        }

        private void MarkTransformNeedUpdate()
        {
            thisToParentTransformNeedUpdate = true;
            thisToModelTransformNeedUpdate = true;
        }

        private void CalculateTransform()
        {
            var translation = Matrix4.Identity;
            if (parent != null)
                translation = Matrix4.CreateTranslation(parent.Length, 0, 0);
            var rotation = Matrix4.CreateFromQuaternion(this.rotation);

            thisToParentTransform = rotation * translation;
        }

        void ForceEnsureTransform()
        {
            if (this.parent != null)
                parent.ForceEnsureTransform();

            EnsureTransform();
        }

        public void EnsureTransform()
        {
            if (thisToParentTransformNeedUpdate)
            {
                CalculateTransform();
                thisToParentTransformNeedUpdate = false;
                thisToModelTransformNeedUpdate = true;
            }

            if (thisToModelTransformNeedUpdate)
            {
                if (parent != null)
                    thisToModelTransform = thisToParentTransform * parent.thisToModelTransform;
                else
                    thisToModelTransform = thisToParentTransform;

                foreach (Bone b in children)
                    b.OnParentTransformUpdated();

                thisToModelTransformNeedUpdate = false;
            }
        }

        private void OnParentTransformUpdated()
        {
            thisToModelTransformNeedUpdate = true;
        }

        bool thisToParentTransformNeedUpdate = false;
        bool thisToModelTransformNeedUpdate = false;
        Matrix4 thisToParentTransform = Matrix4.Identity;
        Matrix4 thisToModelTransform = Matrix4.Identity;

        public Matrix4 ThisToModelTransform
        {
            get
            {
                return thisToModelTransform;
            }
        }

        static List<BoneType> boneTypes;
        public static IReadOnlyList<BoneType> BoneTypes
        {
            get
            {
                return boneTypes.AsReadOnly();
            }
        }

        public override string ToString()
        {
            return BoneType.ToString() + ":" + DirectionInWorld.ToString();
        }

        static Bone()
        {
            var bones = Enum.GetValues(typeof(BoneType));
            boneTypes = new List<BoneType>();
            foreach (var b in bones)
                boneTypes.Add((BoneType)b);
        }
    }
}
