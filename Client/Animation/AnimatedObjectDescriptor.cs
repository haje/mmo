﻿using OpenTK;
using System.Collections.Generic;
using HAJE.MMO.Client.Rendering;

namespace HAJE.MMO.Client.Animation
{
    public class AnimatedObjectDescriptor : WorldSpaceEntity
    {
        AnimatedEntity entity;
        public AnimatedEntity AnimatedEntity
        {
            get
            {
                return entity;
            }
        }

        public string Name = "";

        AnimatedMeshDescriptor[] objDescArray;
        Dictionary<AttachmentPoint, AnimatedAttachmentDescriptor> apDic;
        PoseBlender poseBlender;
        Animation animation;

        public AnimatedObjectDescriptor(Vector3 position, Vector3 lookingAtDirection, 
                                        AnimatedEntity entity, AnimatedMeshDescriptor[] descArray)
            :base(position, lookingAtDirection, Vector3.UnitY)
        {
            this.entity = entity;
            this.objDescArray = descArray;
            this.poseBlender = new PoseBlender(entity);
            this.apDic = new Dictionary<AttachmentPoint, AnimatedAttachmentDescriptor>();
        }

        public AnimatedObjectDescriptor(Vector3 position, Vector3 lookingAtDirection,
                                AnimatedEntity entity)
            : base(position, lookingAtDirection, Vector3.UnitY)
        {
            this.entity = entity;
            this.poseBlender = new PoseBlender(entity);
            this.apDic = new Dictionary<AttachmentPoint, AnimatedAttachmentDescriptor>();
        }

        public void ApplyPose(Pose to, Second length)
        {
            poseBlender.ApplyPose(to, length);
        }

        public void ApplyPose(Pose from, Pose to, Second length)
        {
            poseBlender.ApplyPose(from, to, length);
        }

        public void ApplyPose(Pose pose)
        {
            poseBlender.ApplyPose(pose, (Second)0);
            poseBlender.Update((Second)1.0f);
            poseBlender.ClearPose();
        }

        public void ApplyAndHoldPose(Pose pose)
        {
            poseBlender.ApplyPose(pose, (Second)0);
        }

        public void RunAnimation(Animation animation)
        {
            this.animation = animation;
        }

        public void Update(Second deltaTime)
        {
            entity.EnsureTransform();
            foreach (AnimatedAttachmentDescriptor aad in apDic.Values)
                aad.UpdateModelMatrix(entity.GetBone(aad.BoneType), this.ModelMatrix);

            if (animation != null)
                animation.Update(deltaTime);

            if (poseBlender != null)
                poseBlender.Update(deltaTime);
        }

        public AnimatedMeshDescriptor[] GetMeshDescriptor()
        {
            entity.EnsureTransform();

            foreach(AnimatedMeshDescriptor m in objDescArray)
            {
                var bone = entity.GetBone(m.Type);
                m.UpdateModelMatrix(bone, this.ModelMatrix);
            }

            return objDescArray;
        }

        public AnimatedMeshDescriptor GetSpecifiedMeshDescriptor(BoneType type)
        {
            foreach(AnimatedMeshDescriptor d in objDescArray)
            {
                if(d.Type == type)
                {
                    return d;
                }
            }

            return null;
        }

        public WorldSpaceEntity GetAP(AttachmentPoint point)
        {
            if(apDic.ContainsKey(point))
            {
                //bone의 상대적 위치로 계산하기 때문에, AP를 얻기 전에 반드시 transform을 최신화
                //해야 한다.
                entity.EnsureTransform();

                AnimatedAttachmentDescriptor desc = apDic[point];
                var attachBone = entity.GetBone(desc.BoneType);
                desc.UpdateModelMatrix(attachBone, this.ModelMatrix);

                return desc;
            }
            else
            {
                throw new System.Exception("해당 Attachment 타입이 존재하지 않습니다.");
            }
        }

        public void ReloadMeshInfo(MeshProvider provider)
        {
            AnimatedMeshInformationProvider.Instance.ReadMeshScript();

            LoadInfoFromScript(provider);
        }

        public void LoadInfoFromScript(MeshProvider provider)
        {
            LoadMeshInfoFromScriptData(provider);
            LoadAPInfoFromScriptData();
        }

        private void LoadMeshInfoFromScriptData(MeshProvider provider)
        {
            if(Name.Length != 0)
            {
                List<AnimatedMeshDescriptor> meshDesc = new List<AnimatedMeshDescriptor>();
                IMeshInfo list = AnimatedMeshInformationProvider.Instance[this.Name];
                
                foreach(AnimatedMeshInformation m in list.MeshInfoList)
                {
                    AnimatedMeshDescriptor desc = new AnimatedMeshDescriptor(m, provider.CreateMeshByType(m.MeshType));
                    meshDesc.Add(desc);   
                }

                this.objDescArray = meshDesc.ToArray();
            }
        }

        private void LoadAPInfoFromScriptData()
        {
            if(Name.Length != 0)
            {
                IMeshInfo list = AnimatedMeshInformationProvider.Instance[this.Name];

                foreach(AnimatedAttachmentInformation info in list.APList)
                {
                    AnimatedAttachmentDescriptor newDesc = new AnimatedAttachmentDescriptor(info);
                    apDic.Add(info.AP, newDesc);
                }
            }
        }
        
        public static AnimatedObjectDescriptor CreateHuman(MeshProvider provider)
        {
            AnimatedEntity human = new AnimatedEntity(AnimatedEntityDescriptor.Human);
            AnimatedObjectDescriptor desc = new AnimatedObjectDescriptor(Vector3.Zero, Vector3.UnitZ, human);

            desc.Name = "Human";
            desc.LoadInfoFromScript(provider);

            return desc;
        }
    }
}
