﻿using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.Animation
{
    public class Animation
    {
        public Animation(AnimatedEntity owner)
        {
            this.owner = owner;
        }

        public void AddPose(Pose pose, Second length)
        {
            var from = Length;
            var frame = new Frame();
            frame.Pose = pose;
            frame.From = from;
            frame.To = from + length;
            frameList.Add(frame);
        }

        public void Update(Second deltaTime)
        {
            var length = Length;
            Debug.Assert(length > 0);

            if (time < length)
            {
                time += deltaTime;
                if (Loop)
                {
                    while (time > length)
                    {
                        time -= length;
                    }
                }
                else
                {
                    time = length;
                }
            }

            int index = 0;
            Frame frame = frameList[index];
            while (frame.To < time)
            {
                frame = frameList[++index];
            }
            if (++index >= frameList.Count)
            {
                if (Loop)
                    index = 0;
                else
                    index = frameList.Count - 1;
            }
            Pose from = frame.Pose;
            Pose to = frameList[index].Pose;
            float blend = (time - frame.From) / (frame.To - frame.From);
            Debug.Assert(0 <= blend && blend <= 1);
            owner.BlendPose(from, to, blend);
        }

        public Second Length
        {
            get
            {
                Second length = (Second)0;
                if (frameList.Count > 0)
                {
                    var last = frameList[frameList.Count - 1];
                    length = last.To;
                }
                return length;
            }
        }

        public class Frame
        {
            public Pose Pose;
            public Second From;
            public Second To;
        }
        List<Frame> frameList = new List<Frame>();
        AnimatedEntity owner;

        public bool Loop = true;
        Second time;
    }
}
