﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Animation
{
    public class AnimatedAttachmentInformation
    {
        public BoneType BoneType = BoneType.Root;
        public AttachmentPoint AP = AttachmentPoint.Origin;
        public Vector3 DirectionVector = new Vector3(1, 0, 0);
        public float Length = 0.0f;
    }

    public class AnimatedAttachmentDescriptor : WorldSpaceEntity
    {
        AnimatedAttachmentInformation info;

        public BoneType BoneType
        {
            get
            {
                return info.BoneType;
            }
        }

        public AnimatedAttachmentDescriptor(AnimatedAttachmentInformation info)
            :base(Vector3.Zero, Vector3.UnitX, Vector3.UnitY)
        {
            this.info = info;
        }

        private Matrix4 modelMatrix = Matrix4.Identity;
        public override Matrix4 ModelMatrix
        {
            get
            {
                return modelMatrix;
            }
        }

        public virtual void UpdateModelMatrix(Bone bone, Matrix4 ownerMatrix)
        {
            Vector3 transVec = new Vector3(info.Length, 0, 0);

            Matrix4 transMat = Matrix4.CreateTranslation(transVec);

            Vector3 rotateVec = info.DirectionVector;

            Matrix4 rotateMat;

            var axis = Vector3.Cross(Vector3.UnitX, rotateVec).Normalized();

            if (float.IsNaN(axis.Length))   // rotateVec is the same as Vector3.UnitX
            {
                rotateMat = Matrix4.Identity;
            }
            else
            {
                var angle = Vector3.CalculateAngle(Vector3.UnitX, rotateVec);
                rotateMat = Matrix4.CreateFromAxisAngle(axis, angle);
            }

            Matrix4 transforMat = rotateMat * transMat;

            modelMatrix = transforMat * bone.ThisToModelTransform * ownerMatrix;

            Position = modelMatrix.ExtractTranslation();
        }
    }
}
