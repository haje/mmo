﻿using System.Collections.Generic;
using System.Linq;

namespace HAJE.MMO.Client.Animation
{
    public interface IMeshInfo
    {
        string Name { get; set; }
        List<AnimatedMeshInformation> MeshInfoList { get; set; }
        List<AnimatedAttachmentInformation> APList { get; set; }

        void CreateMeshList();
        void CreateAttachmentPoint();
        void CopyTo(IMeshInfo info);
    }

    public class AnimatedMeshInfoList : IMeshInfo
    {
        public string Name { get; set; }
        public List<AnimatedMeshInformation> MeshInfoList { get; set; }
        public List<AnimatedAttachmentInformation> APList { get; set; }

        public AnimatedMeshInfoList()
        {
            this.Name = "";
            this.MeshInfoList = new List<AnimatedMeshInformation>();
            this.APList = new List<AnimatedAttachmentInformation>();
        }

        public virtual void CreateMeshList()
        {
            
        }

        public virtual void CreateAttachmentPoint()
        {

        }

        public virtual void CopyTo(IMeshInfo info)
        {
            this.Name = info.Name;
            this.MeshInfoList = info.MeshInfoList.ToList();
            this.APList = info.APList.ToList();
        }
    }

    public class AnimatedMeshInformationProvider
    {
        const string path = "Resource/Model/AnimatedMeshScript/";
        Dictionary<string, IMeshInfo> infoDic = new Dictionary<string, IMeshInfo>();

        private static AnimatedMeshInformationProvider instance;
        public static AnimatedMeshInformationProvider Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new AnimatedMeshInformationProvider();

                    instance.ReadMeshScript();
                }

                return instance;
            }
        }

        public IMeshInfo this[string name]
        {
            get
            {
                return infoDic[name];
            }
        }

        IMeshInfo newInfoList;

        public void ReadMeshScript()
        {
            infoDic.Clear();

            string[] fileNames = System.IO.Directory.GetFiles(path, "*.cs");

            foreach(string s in fileNames)
            {
                LoadAnimatedMeshScript(s, ref newInfoList);
                newInfoList.CreateMeshList();
                newInfoList.CreateAttachmentPoint();

                AnimatedMeshInfoList copyList = new AnimatedMeshInfoList();
                copyList.CopyTo(newInfoList);
                
                infoDic.Add(newInfoList.Name, copyList);
            }
        }

        public void LoadAnimatedMeshScript<T>(string path, ref T script) where T : class
        {
            try
            {
                CSScriptLibrary.CSScript.Evaluator.Reset();
                var loaded = CSScriptLibrary.CSScript.Evaluator.LoadFile<T>(path);
                script = loaded;
            }
            catch (System.Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
                CSScriptLibrary.CSScript.Evaluator.Reset();
            }
        }
    }
}
