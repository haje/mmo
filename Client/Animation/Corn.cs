﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Animation
{
    public class Corn
    {
        public Corn(MeshProvider meshProvider)
        {
            animatedObject = AnimatedObjectDescriptor.CreateHuman(meshProvider);
            jump = AnimationFactory.CreateJump(animatedObject.AnimatedEntity);
            walk = AnimationFactory.CreateWalk(animatedObject.AnimatedEntity);
            run = AnimationFactory.CreateRun(animatedObject.AnimatedEntity);
            runLeft = AnimationFactory.CreateRunLeft(animatedObject.AnimatedEntity);
            aimUp = Pose.HandForward(animatedObject.AnimatedEntity);
            stand = Pose.Stand(animatedObject.AnimatedEntity);
            headingDirection = animatedObject.LookingAtDirection;
            movingDirection = Vector2.UnitX;
            State = AnimationState.Stand;
        }

        public void Update(Second deltaTime)
        {
            animatedObject.Update(deltaTime);
        }

        public void Draw(Rendering.Renderer.IMeshRenderable renderer)
        {
            renderer.Render(animatedObject.GetMeshDescriptor());
        }

        public void Stand()
        {
            if (State == AnimationState.Stand) return;

            animatedObject.RunAnimation(null);
            animatedObject.ApplyPose(stand);
            if (aimUpState)
                animatedObject.ApplyAndHoldPose(aimUp);

            State = AnimationState.Stand;
        }

        public void Walk()
        {
            if (State == AnimationState.Walk) return;

            animatedObject.RunAnimation(walk);
            State = AnimationState.Walk;
        }

        public void Run()
        {
            if (State == AnimationState.Run) return;

            animatedObject.RunAnimation(run);
            State = AnimationState.Run;
        }
        
        public void RunLeft()
        {
            if (State == AnimationState.RunLeft) return;

            animatedObject.RunAnimation(runLeft);
            State = AnimationState.RunLeft;
        }

        public void Jump()
        {
            if (State == AnimationState.Jump) return;

            animatedObject.RunAnimation(jump);
            State = AnimationState.Jump;
        }

        public WorldSpaceEntity GetAP(AttachmentPoint point)
        {
            return animatedObject.GetAP(point);
        }

        public void ReloadCornMesh(MeshProvider provider)
        {
            animatedObject.ReloadMeshInfo(provider);
        }

        bool aimUpState = false;
        public bool AimUp
        {
            get
            {
                return aimUpState;
            }
            set
            {
                if (aimUpState == value)
                    return;

                if (value)
                {
                    animatedObject.ApplyPose(aimUp, (Second)0.5f);
                }
                else
                {
                    animatedObject.ApplyPose(stand);
                }
                aimUpState = value;
            }
        }

        public Vector3 Position
        {
            get
            {
                return animatedObject.Position;
            }
            set
            {
                animatedObject.Position = value;
            }
        }

        Vector2 movingDirection;
        public Vector2 MovingDirection
        {
            get
            {
                return movingDirection;
            }
            set
            {
                Vector2.Normalize(ref value, out movingDirection);
                ApplyDirectionVectors();
            }
        }

        Vector3 headingDirection;
        public Vector3 HeadingDirection
        {
            get
            {
                return headingDirection;
            }
            set
            {
                Vector3.Normalize(ref value, out headingDirection);
                if (State == AnimationState.Stand)
                {
                    Vector2 moveDir = new Vector2(headingDirection.X, headingDirection.Z);
                    moveDir.Normalize();
                    MovingDirection = moveDir;
                }
                else
                {
                    ApplyDirectionVectors();
                }
            }
        }

        void ApplyDirectionVectors()
        {
            var dir = new Vector3(movingDirection.X, 0, movingDirection.Y);
            if (AimUp)
            {
                dir = new Vector3(headingDirection.X, 0, headingDirection.Z);
            }
            animatedObject.LookingAtDirection = dir;
        }

        public WorldSpaceEntity WorldSpaceEntity
        {
            get
            {
                return animatedObject;
            }
        }

        AnimatedObjectDescriptor animatedObject;
        Animation jump;
        Animation run;
        Animation runLeft;
        Animation walk;
        Pose aimUp;
        Pose stand;
        public AnimationState State { get; private set; }
    }

    public enum AnimationState
    {
        Stand,
        Jump,
        Run,
        RunLeft,
        Walk
    }
}
