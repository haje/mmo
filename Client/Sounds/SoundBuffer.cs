﻿using System;
using System.IO;
using OpenTK;
using OpenTK.Audio.OpenAL;
using NVorbis;

namespace HAJE.MMO.Client.Sounds
{
    public static class ALHelper
    {
        public static void Check()
        {
            ALError error;
            if ((error = AL.GetError()) != ALError.NoError)
                throw new InvalidOperationException(AL.GetErrorString(error));
        }
    }
    
    public class SoundBuffer
    {
        //TODO : 플레이할 sound의 정보를 가지고 있는 클래스
        //각 SoundData에는 플레이 할 하나의 sound가 매칭된다.
        public int BufferID { get; private set; }
        public int SourceID { get; private set; }
        public ALSourceState State { get; private set; }
        public ALFormat Format { get; private set; }

        public bool IsAllocated { get; private set; }
        public bool Looping { get; private set; }
        public Vector2 Position { get; private set; }
        public float Pitch { get; private set; }
        public float Gain { get; private set; }
        public bool IsPlaying
        {
            get
            {
                ALSourceState state = AL.GetSourceState(SourceID);

                if (state == ALSourceState.Stopped || state == ALSourceState.Paused || state == ALSourceState.Initial)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public SoundBuffer(int bufferID)
        {
            SourceID = AL.GenSource();
            BufferID = bufferID;
            Gain = 1.0f;
            Pitch = 1.0f;
            Looping = false;
            IsAllocated = false;
        }

        public SoundBuffer(int bufferID, Vector2 position)
        {
            SourceID = AL.GenSource();
            BufferID = bufferID;
            Position = position;
            Gain = 1.0f;
            Pitch = 1.0f;
            Looping = false;
            IsAllocated = false;
        }

        public void Play()
        {
            Vector3 posVec = new Vector3(Position.X, 0, Position.Y);
            AL.Source(SourceID, ALSource3f.Position, ref posVec);
            AL.Source(SourceID, ALSourcef.Gain, Gain);
            AL.Source(SourceID, ALSourcef.Pitch, Pitch);
            AL.Source(SourceID, ALSourceb.Looping, Looping);
            ALHelper.Check();

            AL.SourceQueueBuffer(SourceID, BufferID);
            ALHelper.Check();

            AL.SourcePlay(SourceID);
            ALHelper.Check();
        }

        public void Stop()
        {
            AL.SourceStop(SourceID);
        }

        public void Pause()
        {
            AL.SourcePause(SourceID);
        }

        public void SetPosition(Vector2 position)
        {
            Position = position;
        }

        public void SetGain(float gain)
        {
            Gain = gain;
        }

        public void SetAllocated(bool isAllocated)
        {
            IsAllocated = isAllocated;
        }
    }

    public class SoundDataLoader
    {
        //TODO : sound를 로드하고 SoundData에 맞게 세팅해준다.
        public static void LoadSoundFile(int[] buffer, SoundInfo info)
        {
            ALFormat format;
            int channels, bps, rate;

            string ext = Path.GetExtension(info.Path);

            switch (ext)
            {
                case ".ogg":
                    var oggSoundData = LoadOgg(info.Path, out channels, out bps, out rate);
                    format = GetSoundFormat(channels, bps);
                    AL.BufferData((int)info.BufferID, format, oggSoundData, oggSoundData.Length, rate);
                    ALHelper.Check();
                    break;
                case ".wav":
                    var wavSoundData = LoadWav(new FileStream(info.Path, FileMode.Open), out channels, out bps, out rate);
                    format = GetSoundFormat(channels, bps);
                    AL.BufferData((int)info.BufferID, format, wavSoundData, wavSoundData.Length, rate);
                    ALHelper.Check();
                    break;
                default:
                    throw new Exception("This sound is not supported.");
            }

            ALHelper.Check();
        }

        private static void CastBuffer(float[] incoming, short[] outgoing, int length)
        {
            for (int i = 0; i < length; i++)
            {
                var temp = (int)(32767f * incoming[i]);
                if (temp > short.MaxValue) temp = short.MaxValue;
                else if (temp < short.MinValue) temp = short.MinValue;
                outgoing[i] = (short)temp;
            }
        }

        private static short[] LoadOgg(string path, out int channels, out int bps, out int rate)
        {
            //TODO : sample read 시 일정 크기 이상의 BGM으로 판명된다면 스트리밍을 시도해야 한다.
            //일단 지금은 처리하지 않고 넘어간다.
            using (VorbisReader reader = new VorbisReader(path))
            {
                channels = reader.Channels;
                bps = 16;
                rate = reader.SampleRate;

                float[] newData = new float[reader.TotalSamples];
                int k = reader.ReadSamples(newData, 0, newData.Length);

                short[] castBuffer = new short[reader.TotalSamples];
                CastBuffer(newData, castBuffer, newData.Length);

                return castBuffer;
            }
        }

        private static byte[] LoadWav(Stream stream, out int channels, out int bps, out int rate)
        {
            if (stream == null)
                throw new ArgumentException("No stream!", "stream");

            using (BinaryReader reader = new BinaryReader(stream))
            {
                // RIFF header
                string signature = new string(reader.ReadChars(4));
                if (signature != "RIFF")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                int riff_chunck_size = reader.ReadInt32();

                string format = new string(reader.ReadChars(4));
                if (format != "WAVE")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                // WAVE header
                string format_signature = new string(reader.ReadChars(4));
                if (format_signature != "fmt ")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int format_chunk_size = reader.ReadInt32();
                int audio_format = reader.ReadInt16();
                int num_channels = reader.ReadInt16();
                int sample_rate = reader.ReadInt32();
                int byte_rate = reader.ReadInt32();
                int block_align = reader.ReadInt16();
                int bits_per_sample = reader.ReadInt16();

                string data_signature = new string(reader.ReadChars(4));
                if (data_signature != "data")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int data_chunk_size = reader.ReadInt32();

                channels = num_channels;
                bps = bits_per_sample;
                rate = sample_rate;

                //http://gamedev.stackexchange.com/questions/71571/how-do-i-prevent-clicking-at-the-end-of-each-sound-play-in-openal
                //return reader.ReadBytes((int)reader.BaseStream.Length);
                return reader.ReadBytes(data_chunk_size);
            }
        }

        private static ALFormat GetSoundFormat(int channels, int bps)
        {
            switch (channels)
            {
                case 1:
                    return bps == 8 ? ALFormat.Mono8 : ALFormat.Mono16;
                case 2:
                    return bps == 8 ? ALFormat.Stereo8 : ALFormat.Stereo16;
                default:
                    throw new NotSupportedException("The specified sound format is not supported.");
            }
        }
    }
}
