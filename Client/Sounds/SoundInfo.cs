﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client.Sounds
{
    public enum SoundBufferID
    {
        //BGM
        BGM = 0,

        //Character
        Jump,
        Walk,
        Hit,

        //Skill Effect
        Fire1,
        Fire2,
        Fire3,
    }

    public class SoundInfo
    {
        public string Path;
        public string Name;
        public SoundBufferID BufferID;

        public void CopyTo(SoundInfo target)
        {
            target.Path = Path;
            target.Name = Name;
            target.BufferID = BufferID;
        }
    }
}
