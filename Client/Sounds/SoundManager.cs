﻿using OpenTK;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;
using System.Linq;


namespace HAJE.MMO.Client.Sounds
{
    public class SoundManager
    {
        AudioContext AC;
        int[] bufferData;

        BufferManager bufferManager;

        public SoundManager()
        {
            bufferManager = new BufferManager();

            AC = new AudioContext();
            CreateBufferDataFromScript();
        }

        public void Update(float deltaTime)
        {
            bufferManager.Update();
        }

        public SoundBuffer Play(SoundBufferID id, Vector2 position)
        {
            SoundBuffer sb = bufferManager.GetNewBuffer(id, position);
            ALHelper.Check();

            sb.Play();
            ALHelper.Check();

            return sb;
        }

        public void RewindPlay(SoundBuffer sb)
        {
            AL.SourceUnqueueBuffer(sb.SourceID);
            ALHelper.Check();

            AL.SourceRewind(sb.SourceID);
            ALHelper.Check();

            sb.Play();
            ALHelper.Check();
        }

        public void Stop()
        {

        }

        public void Pause()
        {

        }

        public void Mute(bool isMute)
        {
            bufferManager.Mute(isMute);
        }

        public SoundBuffer GetSoundBuffer(SoundBufferID id, Vector2 position)
        {
            SoundBuffer sb = bufferManager.GetNewBuffer(id, position);

            return sb;
        }

        public void CreateBufferDataFromScript()
        {
            bufferData = AL.GenBuffers(Enum.GetValues(typeof(SoundBufferID)).Length);

            //Sound Path를 바탕으로 ALBuffer에 Data들을 로딩한다.
            foreach (SoundInfo info in SoundInfomationProvider.Instance.SoundInfoList)
            {
                SoundDataLoader.LoadSoundFile(bufferData, info);
            }
        }
    }

    public class BufferManager
    {
        List<SoundBuffer> bufferList;

        bool isMute;

        public BufferManager()
        {
            bufferList = new List<SoundBuffer>();
        }

        public SoundBuffer GetNewBuffer(SoundBufferID id, Vector2 pos)
        {
            SoundBuffer sb = new SoundBuffer((int)id, pos);
            bufferList.Add(sb);
            sb.SetAllocated(true);

            return sb;
        }

        public void Update()
        {
            //TODO : 매 프레임 업데이트를 돌려서 sound 재생이 끝난 SoundBuffer를 제거한다.
            //재사용할 수 있지만 이건 3D 사운드 구현이 끝나고 나서 추가로 구현해준다.

            if (isMute)
            {
                foreach (SoundBuffer sb in bufferList)
                {
                    sb.SetGain(0.0f);
                }
            }
            else
            {
                foreach (SoundBuffer sb in bufferList)
                {
                    sb.SetGain(1.0f);
                }
            }

            RemoveStopBuffer();
        }

        public void Mute(bool isMute)
        {
            this.isMute = isMute;
        }

        public void RemoveStopBuffer()
        {
            bufferList.RemoveAll(a => (!a.IsPlaying && !a.IsAllocated));
        }
    }
}
