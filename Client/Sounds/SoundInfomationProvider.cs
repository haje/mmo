﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HAJE.MMO.Client.Sounds
{
    public interface ISoundInfoList
    {
        List<SoundInfo> InfoList { get; set; }

        void CreatePathList();
        void CopyTo(ISoundInfoList info);
    }

    //TODO : SoundInfo는 하나의 사운드 정보를 담고 있어야 한다.
    //Path나 이름, Type 같은 General한 정보.
    public class SoundInfoList : ISoundInfoList
    {
        public List<SoundInfo> InfoList { get; set; }

        public SoundInfoList()
        {
            InfoList = new List<SoundInfo>();
        }

        public virtual void CreatePathList()
        {

        }

        public virtual void CopyTo(ISoundInfoList info)
        {
            this.InfoList = info.InfoList.ToList();
        }
    }

    public class SoundInfomationProvider
    {
        const string path = "Resource/Sounds/";

        public List<SoundInfo> SoundInfoList { get; private set; }

        private static SoundInfomationProvider instance;
        public static SoundInfomationProvider Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new SoundInfomationProvider();

                    instance.ReadSoundScript();
                }

                return instance;
            }
        }

        public SoundInfomationProvider()
        {
            SoundInfoList = new List<SoundInfo>();
        }

        ISoundInfoList list;
        public void ReadSoundScript()
        {
            SoundInfoList.Clear();

            string[] fileNames = System.IO.Directory.GetFiles(path, "*.cs");

            foreach (string s in fileNames)
            {
                LoadSoundScript(s, ref list);
                list.CreatePathList();

                for(int i = 0; i < list.InfoList.Count; i++)
                {
                    //releative path로 변경
                    list.InfoList[i].Path = path + list.InfoList[i].Path;

                    SoundInfo newInfo = new SoundInfo();
                    list.InfoList[i].CopyTo(newInfo);

                    SoundInfoList.Add(newInfo);
                }
            }
        }

        public void LoadSoundScript<T>(string path, ref T script) where T : class
        {
            try
            {
                CSScriptLibrary.CSScript.Evaluator.Reset();
                var loaded = CSScriptLibrary.CSScript.Evaluator.LoadFile<T>(path);
                script = loaded;
            }
            catch (System.Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
                CSScriptLibrary.CSScript.Evaluator.Reset();
            }
        }
    }
}
