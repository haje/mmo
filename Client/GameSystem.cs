﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI;
using HAJE.MMO.Client.Sounds;
using OpenTK;
using System;
using System.Diagnostics;
using System.IO;

namespace HAJE.MMO.Client
{
    public static class GameSystem
    {
        const int clientLogBuffer = 10;

        public static Vector2 Viewport
        {
            get
            {
                return new Vector2(MainForm.Width, MainForm.Height);
            }
        }

        public static MainForm MainForm
        {
            get;
            private set;
        }

        public static TextureCache TextureCache
        {
            get;
            private set;
        }

        public static FontCache FontCache
        {
            get;
            private set;
        }

        public static Profiler Profiler
        {
            get;
            private set;
        }

        private static GameTime renderTime;

        public static DebugOutput DebugOutput
        {
            get;
            private set;
        }

        public static LogDispatcher Log
        {
            get;
            private set;
        }

        public static UISystem UISystem
        {
            get;
            private set;
        }

        public static Input.InputState InputState
        {
            get;
            private set;
        }

        public static CommandList CommandList
        {
            get;
            private set;
        }

        public static SoundManager SoundManager
        {
            get;
            private set;
        }

        static Network.NetworkContext networkContext;
        public static Network.NetworkContext NetworkContext
        {
            get
            {
                if (networkContext == null)
                {
                    networkContext = new Network.NetworkContext(Log);
                    networkContext.ErrorReceived += (content, errorType) =>
                    {
                        UISystem.ShowError(content, errorType);
                    };
                }
                return networkContext;
            }
        }

        public static GameLoopBase RunningGameLoop { get; private set; }

        public static void Initialize(bool startGame)
        {
            InitializeCurrentDirectory();
            MainForm = new MainForm();
            TextureCache = new TextureCache();
            FontCache = new FontCache();
            Profiler = new Profiler();
            renderTime = new GameTime();
            InputState = new Input.InputState();
            Log = new LogDispatcher("Client", clientLogBuffer);
            Log.DebugOutputEnable = true;
            DebugOutput = new DebugOutput(Log);
            UISystem = new UISystem();
            SoundManager = new SoundManager();
            CommandList = new Client.CommandList();
            GlobalDebugCommand.Initialize();

            if (Configuration.Instance.UnitTestEnabled)
                UnitTest.Run(Log);
            
            if (startGame == true)
                MainForm.Run();
        }

        static void InitializeCurrentDirectory()
        {
            // Resource 폴더를 일일이 복사하지 않더라도 실행 경로를 수정. 테스트 가능하도록
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Client" + "\\Resource"))
            {
                if (Debugger.IsAttached || Directory.Exists(Environment.CurrentDirectory + "\\..\\..\\Resource"))
                {
                    Environment.CurrentDirectory = "../../";
                }
            }
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                throw new FileNotFoundException("리소스 파일의 경로를 찾지 못했습니다.\n현재 경로: " + Environment.CurrentDirectory + "\n");
            }
        }

        public static void SetGameLoop(GameLoopBase gameLoop)
        {
            var oldLoop = RunningGameLoop;
            if (oldLoop != null)
            {
                oldLoop.Stop();
                oldLoop.Dispose();
                GameSystem.UISystem.ReplaceScene(null);
            }
            gameLoop.Run();
            RunningGameLoop = gameLoop;
        }

        public static void Update(Second deltaTime)
        {
            if (networkContext != null)
            {
                networkContext.PacketDispatcher.Dispatch();
            }
            InputState.FetchState(MainForm);
            Profiler.UpdateFps(renderTime);
            Profiler.UpdateProbeProfiles(renderTime);
            UISystem.Update(deltaTime);
            RunningGameLoop.Update(deltaTime, InputState);
        }

        public static void Render()
        {
            RunningGameLoop.RenderFrame();
            renderTime.Refresh();
            UISystem.Render();
        }

        public static void Exit()
        {
            if (RunningGameLoop != null)
            {
                RunningGameLoop.Dispose();
                RunningGameLoop = null;
            }

            if (MainForm != null)
            {
                MainForm.Exit();
            }
            Environment.Exit(0);
        }

        public static void Unload()
        {
            if (RunningGameLoop != null)
            {
                RunningGameLoop.Dispose();
                RunningGameLoop = null;
            }

            if (MainForm != null)
            {
                MainForm.Dispose();
                MainForm = null;
            }

            if (TextureCache != null)
            {
                TextureCache.Dispose();
                TextureCache = null;
            }

            if (UISystem != null)
            {
                UISystem.Dispose();
                UISystem = null;
            }

            if (networkContext != null)
            {
                networkContext.Dispose();
                networkContext = null;
            }
        }

        //TEMP
        public static string NewCharacterName = "Unknown";
    }
}
