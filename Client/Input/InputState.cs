﻿using OpenTK.Input;

namespace HAJE.MMO.Client.Input
{
    public class InputState
    {
        public void FetchState(MainForm form)
        {
            Keyboard.FetchState(form);
            Mouse.FetchState(form);
        }

        public KeyboardInput Keyboard = new KeyboardInput();
        public MouseInput Mouse = new MouseInput();
    }
}
