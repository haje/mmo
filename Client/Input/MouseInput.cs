﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;
using OpenTK.Input;
using System.Drawing;

namespace HAJE.MMO.Client.Input
{
    public class MouseInput
    {
        public void FetchState(MainForm form)
        {
            prevState = nowState;
            nowState = Mouse.GetState();
            inputHandled = false;
            isFormFocused = form.Focused;

            if (isFormFocused)
            {
                var cursor = Mouse.GetCursorState();
                var screenPoint = new Point(cursor.X, cursor.Y);
                var clientPoint = form.PointToClient(screenPoint);
                clientPoint.Y = form.Height - clientPoint.Y;
                position = new Vector2(clientPoint.X, clientPoint.Y);
            }

            var viewport = GameSystem.Viewport;
            if (position.X < 0) position.X = 0;
            if (position.X > viewport.X) position.X = viewport.X;
            if (position.Y < 0) position.Y = 0;
            if (position.Y > viewport.Y) position.Y = viewport.Y;
        }

        Vector2 position;
        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        public bool IsPressing(MouseButton button)
        {
            return nowState[button] && isFormFocused;
        }

        public bool IsDown(MouseButton button)
        {
            return !prevState[button]
                && nowState[button]
                && isFormFocused;
        }

        public bool IsUp(MouseButton button)
        {
            return prevState[button]
                && (!nowState[button] || !isFormFocused);
        }

        /// <summary>
        /// 모든 노드는 입력을 처리하기 전에 InputHandled를 체크해야 한다.
        /// 이 메서드를 호출하면 지금 처리중인 입력이 처리되었음 상태로 만들어
        /// 다른 노드들이 입력 중복 처리를 피할 수 있도록 한다.
        /// </summary>
        public void MarkAsHandled()
        {
            inputHandled = true;
        }

        public bool InputHandled
        {
            get
            {
                return inputHandled;
            }
        }

        /// <summary>
        /// Track이 되면 다음 입력을 제일 먼저 받을 수 있게 된다.
        /// </summary>
        public void MarkAsTracked(Node node)
        {
            if (OnMarkAsTracked != null)
                OnMarkAsTracked(node);
        }

        event NodeHandler OnMarkAsTracked = delegate { };

        public void SetTrackedHandler(NodeHandler handler)
        {
            OnMarkAsTracked = handler;
        }


        bool inputHandled;
        bool isFormFocused;
        MouseState nowState;
        MouseState prevState;
    }
}
