﻿using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Input
{
    public class KeyboardInput
    {
        public KeyboardInput()
        {
            var keys = Enum.GetValues(typeof(Key));
            var keySet = new SortedSet<Key>();
            foreach (var k in keys)
            {
                keySet.Add((Key)k);
            }
            this.keys = new List<Key>(keySet);

            this.keyToCharacter = new Dictionary<Key, char>();
            this.shiftKeyToCharacter = new Dictionary<Key, char>();
            #region key -> char map construction

            AddCharMap(Key.Space, ' ');
            for (Key key = Key.A; key <= Key.Z; key++)
            {
                AddCharMap(key,
                    (char)(key - (int)Key.A + 'a'),
                    (char)(key - (int)Key.A + 'A')
                );
            }
            for (Key key = Key.Keypad0; key <= Key.Keypad9; key++)
            {
                AddCharMap(key,(char)(key - (int)Key.Keypad0 + '0'));
            }
            AddCharMap(Key.Number0, '0', ')');
            AddCharMap(Key.Number1, '1', '!');
            AddCharMap(Key.Number2, '2', '@');
            AddCharMap(Key.Number3, '3', '#');
            AddCharMap(Key.Number4, '4', '$');
            AddCharMap(Key.Number5, '5', '%');
            AddCharMap(Key.Number6, '6', '^');
            AddCharMap(Key.Number7, '7', '&');
            AddCharMap(Key.Number8, '8', '*');
            AddCharMap(Key.Number9, '9', '(');
            AddCharMap(Key.KeypadDivide, '/');
            AddCharMap(Key.KeypadAdd, '+');
            AddCharMap(Key.KeypadDecimal, '.');
            AddCharMap(Key.KeypadMultiply, '*');
            AddCharMap(Key.KeypadMinus, '-');
            AddCharMap(Key.Grave, '`', '~');
            AddCharMap(Key.Minus, '-', '_');
            AddCharMap(Key.Plus, '=', '+');
            AddCharMap(Key.BackSlash, '\\', '|');
            AddCharMap(Key.BracketLeft, '[', '{');
            AddCharMap(Key.BracketRight, ']', '}');
            AddCharMap(Key.Semicolon, ';', ':');
            AddCharMap(Key.Quote, '\'', '\"');
            AddCharMap(Key.Comma, ',', '<');
            AddCharMap(Key.Period, '.', '>');
            AddCharMap(Key.Slash, '/', '?');

            #endregion
        }

        private void AddCharMap(Key key, char ch)
        {
            AddCharMap(key, ch, ch);
        }

        private void AddCharMap(Key key, char ch, char shiftChar)
        {
            keyToCharacter.Add(key, ch);
            shiftKeyToCharacter.Add(key, shiftChar);
        }

        public void FetchState(MainForm form)
        {
            prevState = nowState;
            nowState = Keyboard.GetState();
            inputHandled = false;
            isFormFocused = form.Focused;
        }

        public bool IsPressing(Key key)
        {
            return nowState[key] && isFormFocused;
        }

        public bool IsDown(Key key)
        {
            return !prevState[key]
                && nowState[key]
                && isFormFocused;
        }

        public bool IsUp(Key key)
        {
            return prevState[key]
                && (!nowState[key] || !isFormFocused);
        }

        public void MarkAsHandled()
        {
            inputHandled = true;
        }

        public bool InputHandled
        {
            get
            {
                return inputHandled;
            }
        }

        public IReadOnlyList<Key> Keys
        {
            get
            {
                return keys.AsReadOnly();
            }
        }

        public IReadOnlyDictionary<Key, char> KeyToCharacter
        {
            get
            {
                return keyToCharacter;
            }
        }

        public IReadOnlyDictionary<Key, char> ShiftKeyToCharacter
        {
            get
            {
                return shiftKeyToCharacter;
            }
        }

        Dictionary<Key, char> keyToCharacter;
        Dictionary<Key, char> shiftKeyToCharacter;
        List<Key> keys;
        bool inputHandled;
        bool isFormFocused;
        KeyboardState nowState;
        KeyboardState prevState;
    }
}
