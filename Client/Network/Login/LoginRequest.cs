﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Client.Network.Login
{
    public delegate void LoginHandler(LoginResponse response);

    public class LoginRequest
    {
        public LoginRequest(ClientBase client, PacketDispatcher dispatcher)
        {
            this.client = client;
            this.dispatcher = dispatcher;
            this.dispatcher.SetReceiveHandler(
                PacketType.LoginResponse,
                OnLoginResponse
            ); 
        }

        public void SendSuper(string name)
        {
            if (name.Length > 10)
                name = name.Remove(10);

            GameSystem.NewCharacterName = name;
            superRequest.Name = name;
            client.SendAsync(superRequest);
        }

        public void Send(string email, string password)
        {
            request.Email = email;
            request.Password = password;
            client.SendAsync(request);
        }

        public void Abort()
        {
            if (client != null)
            {
                client = null;
                dispatcher.SetReceiveHandler(
                    PacketType.LoginResponse,
                    null
                );
            }
        }

        void OnLoginResponse(PacketReader reader)
        {
            Abort();
            response.Deserialize(reader);
            if (response.ResponseCode == LoginResponseCode.Success)
            {
                OnLoginSuccess(response);
            }
            else
            {
                OnLoginFailed(response);
            }
        }

        public event LoginHandler OnLoginSuccess = delegate { };
        public event LoginHandler OnLoginFailed = delegate { };

        ClientBase client;
        PacketDispatcher dispatcher;
        MMO.Network.Packet.SuperLoginRequest superRequest = new MMO.Network.Packet.SuperLoginRequest();
        MMO.Network.Packet.LoginRequest request = new MMO.Network.Packet.LoginRequest();
        MMO.Network.Packet.LoginResponse response = new MMO.Network.Packet.LoginResponse();
    }
}
