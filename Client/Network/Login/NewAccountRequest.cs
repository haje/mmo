﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Client.Network.Login
{
    public delegate void NewAccountHandler(NewAccountResponseCode responseCode);

    public class NewAccountRequest
    {
        public NewAccountRequest(ClientBase client, PacketDispatcher dispatcher)
        {
            this.client = client;
            this.dispatcher = dispatcher;
            this.dispatcher.SetReceiveHandler(
                PacketType.NewAccountResponse,
                OnNewAccountResponse
            );
        }

        public void Send(string email, string password, string userName)
        {
            request.UserName = userName;
            request.Email = email;
            request.Password = password;
            client.SendAsync(request);
        }

        public event NewAccountHandler OnNewAccountSuccess = delegate { };
        public event NewAccountHandler OnNewAccountFailed = delegate { };

        public void Abort()
        {
            if (client != null)
            {
                client = null;
                dispatcher.SetReceiveHandler(
                    PacketType.NewAccountResponse,
                    null
                );
            }
        }

        void OnNewAccountResponse(PacketReader reader)
        {
            Abort();
            response.Deserialize(reader);
            if (response.ResponseCode == NewAccountResponseCode.Success)
            {
                OnNewAccountSuccess(NewAccountResponseCode.Success);
            }
            else
            {
                OnNewAccountFailed(response.ResponseCode);
            }
        }

        ClientBase client;
        PacketDispatcher dispatcher;
        MMO.Network.Packet.NewAccountRequest request = new MMO.Network.Packet.NewAccountRequest();
        MMO.Network.Packet.NewAccountResponse response = new MMO.Network.Packet.NewAccountResponse();
    }
}
