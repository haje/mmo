﻿using HAJE.MMO.Network.Controller;
using System;
using System.Windows.Forms;

namespace HAJE.MMO.Client.Network.Login
{
    public class LoginContext : IDisposable
    {
        public LoginContext(NetworkContext owner)
        {
            this.ownerCtx = owner;

            var config = Configuration.Instance;
            if (config.ConnectionTarget == ConnectionTarget.Local)
            {
                var server = owner.LocalServerContext.LoginServer;
                client = new SslStreamClient("LoginContext", "localHost", server.Port, "CARoot");
                owner.PacketDispatcher.AttachToClient(client);
                client.ConnectServer();
                if (!client.IsConnected)
                {
                    owner.WriteLog("LoginContext: client 접속에 실패했습니다.");
                    throw new InvalidOperationException("로컬 서버로의 보안 접속 생성에 실패했습니다. 인증서 설치 여부를 확인하세요.");
                }
            }
            else
            {
                client = new SslStreamClient("LoginContext", config.IP, config.LoginPort, "CARoot");
                owner.PacketDispatcher.AttachToClient(client);
                client.ConnectServer();
                if (!client.IsConnected)
                {
                    owner.WriteLog("LoginContext: client 접속에 실패했습니다.");
                    throw new InvalidOperationException("The server has no response.\nCheck the Internet connection.");
                }
                // 개발 서버나 실섭에 접속
            }

            if (client.IsConnected)
            {
                var report = new MMO.Network.Packet.BuildStampReport();
                report.buildStamp = BuildInfo.BuildStamp;
                client.SendAsync(report);
            }
        }

        public LoginRequest CreateLoginRequest()
        {
            return new LoginRequest(client, ownerCtx.PacketDispatcher);
        }

        public NewAccountRequest NewAccountRequest()
        {
            return new NewAccountRequest(client, ownerCtx.PacketDispatcher);
        }

        public void Dispose()
        {
            if (client != null)
            {
                //TODO: @server client connection을 종료
                client = null;
            }
        }

        ClientBase client;
        NetworkContext ownerCtx;
    }
}
