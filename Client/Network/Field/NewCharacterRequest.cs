﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Client.Network.Field
{
    public delegate void NewCharacterHandler(int characterId);

    public class NewCharacterRequest
    {
        public NewCharacterRequest(ClientBase client, PacketDispatcher dispatcher)
        {
            this.client = client;
            this.dispatcher = dispatcher;
            this.dispatcher.SetReceiveHandler(
                PacketType.NewCharacterResponse,
                OnNewCharacterResponse
            );
        }

        public void Send(string characterName)
        {
            if (characterName.Length > 10)
                characterName = characterName.Remove(10);

            request.CharacterName = characterName;
            client.SendAsync(request);
        }

        private void OnNewCharacterResponse(PacketReader reader)
        {
            response.Deserialize(reader);
            OnNewCharacter(response.CharacterId);
        }

        public event NewCharacterHandler OnNewCharacter = delegate { };

        ClientBase client;
        PacketDispatcher dispatcher;
        MMO.Network.Packet.NewCharacterRequest request = new MMO.Network.Packet.NewCharacterRequest();
        MMO.Network.Packet.NewCharacterRepsonse response = new MMO.Network.Packet.NewCharacterRepsonse();
    }
}
