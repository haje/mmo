﻿using System;
using System.Windows.Forms;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Network;
using HAJE.MMO.Spell;
using OpenTK;

namespace HAJE.MMO.Client.Network.Field
{
    public class FieldContext : IDisposable
    {
        public FieldContext(NetworkContext owner, string ip, int port)
        {
            this.ownerCtx = owner;

            client = new SslStreamClient("FieldContext", ip, port, "CARoot");
            owner.PacketDispatcher.AttachToClient(client);
            client.ConnectServer();
            if (!client.IsConnected)
            {
                owner.WriteLog("LoginContext: client 접속에 실패했습니다.");
                MessageBox.Show("로컬 서버로의 보안 접속 생성에 실패했습니다. 인증서 설치 여부를 확인하세요.");
            }
            
            respondCharacterPacket = new RespondCharacter(TargetType.Player,1, "Unknown", encountCharacterPositionInfo);

            moveSync = new MoveSync(moveSyncPositionInfo);
        }

        public void Dispose()
        {
            if (client != null)
            {
                client.Dispose();
                client = null;
            }
        }

        public void SendSuperCommand(string cmd)
        {
            client.SendAsync(new SuperCommandRequest(cmd));
        }

        public void SendAuthToken(int id, string token)
        {
            authTokenPacket.Id = id;
            authTokenPacket.Token = token;
            client.SendAsync(authTokenPacket);
        }

        public void SetPlayContext(GamePlay.PlayContext playContext)
        {
            this.gamePlay = playContext;
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.SuperCommandResponse, OnSuperCommandResponse);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.RespondCharacter, OnRespondCharacter);
            //ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.RequestCharacter, OnRequestCharacter);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.DeadCharacter, OnDeadCharacter);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.FieldResponse, OnFieldResponse);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.StatusSync, OnStatusSync);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.MoveSync, OnMoveSync);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.BeginCastSync, OnBeginCastSync);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.CancelCastSync, OnCancelCastSync);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.EndCastSync, OnEndCastSync);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.CreateEffect, OnCreateEffect);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.MoveEffect, OnMoveEffect);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.RemoveEffect, OnRemoveEffect);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.EquipSpell, OnEquipSpell);
            ownerCtx.PacketDispatcher.SetReceiveHandler(PacketType.DropItem, OnDropItem);
        }

        public void SendFieldRequest(int fieldId)
        {
            client.SendAsync(new FieldRequest(fieldId));
        }

        public void SendCharacterRequest(int characterId)
        {
            requestCharacterPacket.FieldId = 1; //명세에는 적혀 있지만 줄 수 있는 방법이 없다.
                                                //필요한 것인가?
            requestCharacterPacket.CharacterId = characterId;
            client.SendAsync(requestCharacterPacket);
        }

        public void SendMoveStateSync
            (int charId, 
            PositionInfo info)
        {
            var packet = new MoveSync(info);
            packet.CharacterId = charId;
            client.SendAsync(packet);
        }

        public void SendBeginCast(int charId, PositionInfo info, SpellId id)
        {
            var packet = new BeginCastSync();
            packet.CharacterId = charId;
            packet.SpellId = id;
            client.SendAsync(packet);
            GameSystem.Log.Write("SendBeginCast id = {0}", id);
        }

        public void SendCancelCast(int charId, PositionInfo info, SpellId id)
        {
            var packet = new CancelCastSync();
            packet.CharacterId = charId;
            client.SendAsync(packet);
        }

        public void SendEndCast(int charId, SpellId spellId, int fieldId, Vector3 position)
        {
            var packet = new EndCastSync(charId, spellId, TargetType.Air);
            packet.TargetFieldId = fieldId;
            packet.TargetPosition = position;
            client.SendAsync(packet);
        }

        public void SendEndCastToPlayer(int charId, SpellId spellId, int targetId)
        {
            var packet = new EndCastSync(charId, spellId, TargetType.Player);
            packet.TargetId = targetId;
            client.SendAsync(packet);
        }

        public void SendEndCastToGround(int charId, SpellId spellId, int fieldId, Vector3 position)
        {
            var packet = new EndCastSync(charId, spellId, TargetType.Ground);
            packet.TargetFieldId = fieldId;
            packet.TargetPosition = position;
            client.SendAsync(packet);
        }

        public void SendEndCastToDoodad(int charId, SpellId spellId, int doodadId, int fieldId, Vector3 position)
        {
            var packet = new EndCastSync(charId, spellId, TargetType.Doodad);
            packet.TargetId = doodadId;
            packet.TargetFieldId = fieldId;
            packet.TargetPosition = position;
            client.SendAsync(packet);
        }

        public void SendPurchaseSpell(SpellId spellId)
        {
            purchaseSpell.CharacterId = gamePlay.HostingCharacter.CharacterId;
            purchaseSpell.SpellId = spellId;
            client.SendAsync(purchaseSpell);
        }

        public void SendEquipSpell(int index, SpellId spellId)
        {
            equipSpell.CharacterId = gamePlay.HostingCharacter.CharacterId;
            equipSpell.Index = index;
            equipSpell.SpellId = spellId;
            client.SendAsync(equipSpell);
        }

        void OnSuperCommandResponse(PacketReader reader)
        {
            superCommandResponse.Deserialize(reader);
            GameSystem.UISystem.Console.Write(superCommandResponse.Result);
        }

        /*
        void OnRequestCharacter(PacketReader reader)
        {
            respondCharacterPacket.Deserialize(reader);
            gamePlay.EncountPlayerCharacter(
                respondCharacterPacket.CharacterId,
                respondCharacterPacket.CharacterName,
                encountCharacterPositionInfo
            );
        }
        */
        void OnRespondCharacter(PacketReader reader)
        {
            respondCharacterPacket.Deserialize(reader);
            gamePlay.EncountPlayerCharacter(
                respondCharacterPacket.CharacterId,
                respondCharacterPacket.CharacterName,
                encountCharacterPositionInfo
            );
        }

        void OnDeadCharacter(PacketReader reader)
        {
            deadCharacter.Deserialize(reader);
            gamePlay.KillPlayer(deadCharacter.CharacterId);
        }

        void OnFieldResponse(PacketReader reader)
        {
            fieldResponsePacket.Deserialize(reader);
            gamePlay.SetFieldInfo(
                fieldResponsePacket.FieldId,
                fieldResponsePacket.FieldType,
                fieldResponsePacket.NeighborFieldId
            );
        }
        
        void OnStatusSync(PacketReader reader)
        {
            statusSync.Deserialize(reader);

            if (statusSync.StatusType == StatusType.ComboPoint)
            {
                gamePlay.ComboPointSync(
                    statusSync.CharacterId,
                    statusSync.ElementType,
                    statusSync.Value);
            }
            else if (statusSync.StatusType == StatusType.ElementPoint)
            {
                //TODO: update ui
            }
            else if (statusSync.StatusType == StatusType.SpellCount)
            {
                gamePlay.SyncSpellCount(statusSync.SpellId, statusSync.Value);
            }
            else
            {
                gamePlay.StatusSync(
                    statusSync.CharacterId,
                    statusSync.StatusType,
                    statusSync.Value);
            }
        }

        void OnMoveSync(PacketReader reader)
        {
            moveSync.Deserialize(reader);
            gamePlay.SetPlayerPositionInfo(moveSync.CharacterId, moveSyncPositionInfo);            
        }

        void OnBeginCastSync(PacketReader reader)
        {
            beginCastSync.Deserialize(reader);
            gamePlay.BeginCast(beginCastSync.CharacterId, 
                SpellInfo.GetInfo(beginCastSync.SpellId));
        }

        void OnCancelCastSync(PacketReader reader)
        {
            cancelCastSync.Deserialize(reader);
            gamePlay.CancelCast(cancelCastSync.CharacterId);
        }

        void OnEndCastSync(PacketReader reader)
        {
            endCastSync.Deserialize(reader);
            var charId = endCastSync.CharacterId;
            gamePlay.FinishCast(charId);
        }

        void OnCreateEffect(PacketReader reader)
        {
            createEffect.Deserialize(reader);
            gamePlay.CreateEffect(createEffect.Id, createEffect.EffectType, createEffect.TargetType,
                createEffect.TargetPosition, createEffect.TargetId, createEffect.AttachmentPoint);
        }

        void OnMoveEffect(PacketReader reader)
        {
            moveEffect.Deserialize(reader);
            gamePlay.MoveEffect(moveEffect.Id, moveEffect.TargetType, 
                moveEffect.TargetPosition, moveEffect.TargetId, moveEffect.AttachmentPoint, moveEffect.Duration);
        }

        void OnRemoveEffect(PacketReader reader)
        {
            removeEffect.Deserialize(reader);
            gamePlay.RemoveEffect(removeEffect.Id);
        }

        void OnEquipSpell(PacketReader reader)
        {
            equipSpell.Deserialize(reader);
            //TODO: update character equip spell
        }

        void OnDropItem(PacketReader reader)
        {
            dropItem.Deserialize(reader);
            //TODO: update drop item effect
        }

        public NewCharacterRequest NewCharacterRequest()
        {
            return new NewCharacterRequest(client, ownerCtx.PacketDispatcher);
        }

        ClientBase client;
        NetworkContext ownerCtx;
        GamePlay.PlayContext gamePlay;
        AuthWithToken authTokenPacket = new AuthWithToken();
        SuperCommandResponse superCommandResponse = new SuperCommandResponse();
        PositionInfo encountCharacterPositionInfo = new PositionInfo();
        DeadCharacter deadCharacter = new DeadCharacter();
        RequestCharacter requestCharacterPacket = new RequestCharacter();
        RespondCharacter respondCharacterPacket = new RespondCharacter();
        FieldResponse fieldResponsePacket = new FieldResponse();
        StatusSync statusSync = new StatusSync();
        PositionInfo moveSyncPositionInfo = new PositionInfo();
        MoveSync moveSync;
        BeginCastSync beginCastSync = new BeginCastSync();
        CancelCastSync cancelCastSync = new CancelCastSync();
        EndCastSync endCastSync = new EndCastSync();
        CreateEffect createEffect = new CreateEffect();
        MoveEffect moveEffect = new MoveEffect();
        RemoveEffect removeEffect = new RemoveEffect();
        PurchaseSpell purchaseSpell = new PurchaseSpell();
        EquipSpell equipSpell = new EquipSpell();
        DropItem dropItem = new DropItem();
    }
}
