﻿using System;
using HAJE.MMO.Client.Network.Login;
using HAJE.MMO.Client.Network.Field;
using HAJE.MMO.Network.Controller;

namespace HAJE.MMO.Client.Network
{
    public class NetworkContext : IDisposable
    {
        public NetworkContext(LogDispatcher clientLog)
        {
            log = clientLog;
            config = Configuration.Instance;

            PacketDispatcher = new PacketDispatcher();
            PacketDispatcher.SetReceiveHandler(MMO.Network.Packet.PacketType.ErrorReport, OnError);

            if (config.ConnectionTarget == ConnectionTarget.Local)
            {
                SetupLocalServers();
            }
        }

        public event ErrorHandler ErrorReceived = delegate { };

        void OnError(MMO.Network.PacketReader reader)
        {
            var errorReport = new MMO.Network.Packet.ErrorReport();
            errorReport.Deserialize(reader);
            ErrorReceived(errorReport.Content, errorReport.ErrorType);
        }

        void SetupLocalServers()
        {
            WriteLog("LocalServer 켜는 중");
            var ctx = Server.ServerContext.Instance;
            ctx.ErrorLog.OnWriteLog += OnWriteLog;
            ctx.ErrorLog.OwnerName = "Server Error";
            LocalServerContext = ctx;

            Server.Configuration.LoadLocalServerConfig();

            var console = new Server.Console.ServerConsoleFactory().Create();
            ctx.RootPrompt = console;
            console.ExecuteCommand("start master");
            console.ExecuteCommand("start login");
            console.ExecuteCommand("start achieve");
            console.ExecuteCommand("start field");

            WriteLog("LocalServer 부팅 완료");
        }

        public void WriteLog(string format, params object[] args)
        {
            log.Write("Network: " + string.Format(format, args));
        }

        void OnWriteLog(string logText, LogDispatcher dispatcher)
        {
            log.Write("{0}: {1}", dispatcher.OwnerName, logText);
        }

        public Server.ServerContext LocalServerContext;

        LoginContext loginContext;
        public LoginContext GetLoginContext()
        {
            if (loginContext == null)
                loginContext = new LoginContext(this);
            return loginContext;
        }
        public void CloseLoginContext()
        {
            if (loginContext != null)
            {
                loginContext.Dispose();
                loginContext = null;
            }
        }

        FieldContext fieldContext;
        public FieldContext CreateFieldContext(string ip, int port)
        {
            if (fieldContext == null)
                fieldContext = new FieldContext(this, ip, port);
            return fieldContext;
        }
        public FieldContext GetFieldContext()
        {
            return fieldContext;
        }
        public void CloseFieldContext()
        {
            if (fieldContext != null)
            {
                fieldContext.Dispose();
                fieldContext = null;
            }
        }

        public readonly PacketDispatcher PacketDispatcher;

        public void Dispose()
        {
            if (LocalServerContext != null)
            {
                LocalServerContext.Dispose();
                LocalServerContext = null;
            }
            if (loginContext != null)
            {
                loginContext.Dispose();
                loginContext = null;
            }
        }

        LogDispatcher log;
        Configuration config;
    }

    public delegate void ErrorHandler(string errorDescription, ErrorType errorType);
}
