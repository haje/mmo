﻿using HAJE.MMO.Client.UI.Tool;
using System.Collections.Generic;
using System;

namespace HAJE.MMO.Client
{
    public class CommandList
    {
        public void AddCommand(string command, string description, CommandHandler handler, params string[] parameters)
        {
            cmds.Add(command, new CommandElement(command, parameters, description, handler));
        }

        public bool Execute(DebugConsole console, string[] args)
        {
            if (cmds.ContainsKey(args[0]))
            {
                cmds[args[0]].Handler(console, args);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void PrintCommandList(DebugConsole console)
        {
            foreach (var e in cmds.Values)
            {
                console.Write("{0}: {1}", e.Command, e.Description);
            }
        }

        public void PrintParamList(string command, DebugConsole console)
        {
            if (cmds.ContainsKey(command))
            {
                foreach (var param in cmds[command].Parameters)
                {
                    console.Write("{0} {1} | ", command, param);
                }
            }
        }

        public string AutoCompleteCommand(string command)
        {
            string[] words = command.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            int match = 0;
            string matchedCommand = command;
            if (words.Length > 2 || words.Length == 0)
            {
                return command;
            }
            else if (words.Length == 1)
            {
                foreach (var key in cmds.Keys)
                {
                    if (key.StartsWith(words[0], System.StringComparison.CurrentCultureIgnoreCase))
                    {
                        matchedCommand = key;
                        match++;
                    }
                }
                if (match == 1) return matchedCommand;
            }
            else if (words.Length == 2)
            {
                if (cmds.ContainsKey(words[0]))
                {
                    foreach (var param in cmds[words[0]].Parameters)
                    {
                        if (param.StartsWith(words[1], System.StringComparison.CurrentCultureIgnoreCase))
                        {
                            matchedCommand = words[0] + " " + param;
                            match++;
                        }
                    }
                    if (match == 1) return matchedCommand;
                }
            }
            return command;
        }

        public class CommandElement
        {
            public CommandElement(string command, string[] parameters, string description, CommandHandler handler)
            {
                this.Command = command;
                this.Parameters = parameters;
                this.Description = description;
                this.Handler = handler;
            }
            
            public readonly string Command;
            public readonly string[] Parameters;
            public readonly string Description;
            public readonly CommandHandler Handler;
        }

        Dictionary<string, CommandElement> cmds = new Dictionary<string, CommandElement>();
    }

    public delegate void CommandHandler(DebugConsole console, string[] args);
}
