﻿using System;
using System.Drawing.Text;
using System.Text;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.MMO.Client
{
    public class FontCache : IDisposable
    {
        public PrivateFontCollection fontCollection;
        private List<string> fontKeyList;

        public FontCache()
        {
            fontCollection = new PrivateFontCollection();
            fontKeyList = new List<string>();
        }

        public Font GetFont(string fontPath, int size)
        {
            if (size > 0)
            {
                foreach (string c in fontKeyList)
                    if (c == fontPath)
                        return new System.Drawing.Font(fontCollection.Families[fontKeyList.IndexOf(c)], size, System.Drawing.FontStyle.Bold);

                AddFont(fontPath);
                return new System.Drawing.Font(fontCollection.Families[fontKeyList.Count - 1], size, System.Drawing.FontStyle.Bold);
            }
            else
            {
                throw new Exception("폰트의 사이즈는 0 이하가 될 수 없습니다.");
            }            
        }

        public void Dispose()
        {
            fontCollection.Dispose();
            fontKeyList.Clear();
        }

        public void AddFont(String fontname)
        {
            fontCollection.AddFontFile(fontname);
            fontKeyList.Add(fontname);
        }
    }
}
