﻿using System.Xml.Serialization;

namespace HAJE.MMO.Client.Rendering
{
    public class CubemapDescriptor
    {
        [XmlArray("Faces")]
        public CubemapFaceDescriptor[] Faces;
    }
    
    [XmlRoot("Face")]
    public class CubemapFaceDescriptor
    {
        public string Path;
        [XmlAttribute]
        public CubemapFace Face;
    }
}
