﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public interface ISpaceEntity
    {
        Matrix4 ModelMatrix
        {
            get;
        }
    }
}
