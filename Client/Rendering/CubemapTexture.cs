﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml.Serialization;

namespace HAJE.MMO.Client.Rendering
{
    public enum CubemapFace
    {
        [XmlEnum(Name = "PositiveX")]
        PositiveX = TextureTarget.TextureCubeMapPositiveX,
        [XmlEnum(Name = "PositiveY")]
        PositiveY = TextureTarget.TextureCubeMapPositiveY,
        [XmlEnum(Name = "PositiveZ")]
        PositiveZ = TextureTarget.TextureCubeMapPositiveZ,
        [XmlEnum(Name = "NegativeX")]
        NegativeX = TextureTarget.TextureCubeMapNegativeX,
        [XmlEnum(Name = "NegativeY")]
        NegativeY = TextureTarget.TextureCubeMapNegativeY,
        [XmlEnum(Name = "NegativeZ")]
        NegativeZ = TextureTarget.TextureCubeMapNegativeZ,
    }

    public class CubemapTexture : IDisposable
    {
        protected CubemapTexture(int handle)
        {
            texture = handle;
        }

        public static CubemapTexture CreateDefaultCubemap()
        {
            TextReader reader = new StreamReader("Resource/Image/SkyBox/skyrender.xml");
            XmlSerializer serializer = new XmlSerializer(typeof(CubemapDescriptor));
            var cdescRead = (CubemapDescriptor)serializer.Deserialize(reader);
            return CreateFromDescriptor(cdescRead);
        }

        public static CubemapTexture CreateFromDescriptor(CubemapDescriptor descriptor)
        {
            var texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.TextureCubeMap, texture);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            foreach (var face in descriptor.Faces)
            {
                var target = (TextureTarget)face.Face;
                var bitmap = new Bitmap(face.Path);

                BitmapData bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                GL.TexImage2D(target, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
                    OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);

                bitmap.UnlockBits(bitmapData);
            }

            GL.BindTexture(TextureTarget.TextureCubeMap, 0);

            GLHelper.CheckGLError();

            return new CubemapTexture(texture);
        }

        public void Dispose()
        {
            if (texture != 0) GL.DeleteTexture(texture); texture = 0;
        }

        public int TextureHandle
        {
            get
            {
                return texture;
            }
        }

        #region privates

        int texture;

        #endregion
    }
}
