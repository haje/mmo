﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class Particle : ComponentContainer
    {
        public Radian Rotation;
        public Vector2 UVMin;
        public Vector2 UVMax;
        public Color4 Color;
        public Vector2 BoundingBox;

        #region .ctor/reset
        public Particle() 
            : base()
        {
        }
        #endregion
    }
}
