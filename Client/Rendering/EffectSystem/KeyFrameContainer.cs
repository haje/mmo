﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class KeyFrameContainer<T> where T : struct
    {
        private List<KeyFrame<T>> keyFrameList;
        public KeyFrameContainer()
        {
            keyFrameList = new List<KeyFrame<T>>();
        }

        public void AddKeyFrame(Second sec, T value, InterpolationMethod method)
        {
            keyFrameList.Add(new KeyFrame<T>(sec, value, method));
            keyFrameList.Sort();
        }
        public T GetValue(Second sec)
        {
            int index = 0;
            foreach (var keyFrame in keyFrameList)
            {
                if (keyFrame.Time > sec)
                {
                    index--;
                    break;
                }

                index++;
            }

            if (index == -1)
                return keyFrameList[0].Value;
            if (index == keyFrameList.Count)
                return keyFrameList[keyFrameList.Count - 1].Value;

            var curr = keyFrameList[index];
            var next = keyFrameList[index + 1];
            return GetInterpolatedValue(curr.Value, next.Value, (sec - curr.Time) / (next.Time - curr.Time)).Value;
        }
        public T GetDelta(Second from, Second to)
        {
            if (TypeHelper<T>.ToType() == typeof(float))
            {
                float fromValue = (GetValue(from) as Nullable<float>).Value;
                float toValue = (GetValue(to) as Nullable<float>).Value;

                return ((toValue - fromValue) as Nullable<T>).Value;
            }
            if (TypeHelper<T>.ToType() == typeof(Radian))
            {
                Radian fromValue = (GetValue(from) as Nullable<Radian>).Value;
                Radian toValue = (GetValue(to) as Nullable<Radian>).Value;

                return ((toValue - fromValue) as Nullable<T>).Value;
            }
            if (TypeHelper<T>.ToType() == typeof(Vector3))
            {
                Vector3 fromValue = (GetValue(from) as Nullable<Vector3>).Value;
                Vector3 toValue = (GetValue(to) as Nullable<Vector3>).Value;

                return ((toValue - fromValue) as Nullable<T>).Value;
            }
            if (TypeHelper<T>.ToType() == typeof(Vector2))
            {
                Vector2 fromValue = (GetValue(from) as Nullable<Vector2>).Value;
                Vector2 toValue = (GetValue(to) as Nullable<Vector2>).Value;

                return ((toValue - fromValue) as Nullable<T>).Value;
            }
            if (TypeHelper<T>.ToType() == typeof(Color4))
            {
                Color4 fromValue = (GetValue(from) as Nullable<Color4>).Value;
                Color4 toValue = (GetValue(to) as Nullable<Color4>).Value;

                return (new Color4(fromValue.R - toValue.R, 
                    fromValue.G - toValue.G, 
                    fromValue.B - toValue.B, 
                    fromValue.A - toValue.A) as Nullable<T>).Value;
            }

            throw new NotImplementedException();
        }

        // 많은 캐스팅.
        private Nullable<T> GetInterpolatedValue(T lhs, T rhs, float weight)
        {
            if (TypeHelper<T>.ToType() == typeof(float))
            {
                float lhsValue = (lhs as Nullable<float>).Value;
                float rhsValue = (rhs as Nullable<float>).Value;

                return (lhsValue * (1 - weight) + rhsValue * weight) as Nullable<T>;
            }
            if (TypeHelper<T>.ToType() == typeof(Radian))
            {
                Radian lhsValue = (lhs as Nullable<Radian>).Value;
                Radian rhsValue = (rhs as Nullable<Radian>).Value;

                return (lhsValue * (1 - weight) + rhsValue * weight) as Nullable<T>;
            }
            if (TypeHelper<T>.ToType() == typeof(Vector2))
            {
                Vector3 lhsValue = (lhs as Nullable<Vector3>).Value;
                Vector3 rhsValue = (rhs as Nullable<Vector3>).Value;

                return (lhsValue * (1 - weight) + rhsValue * weight) as Nullable<T>;
            }
            if (TypeHelper<T>.ToType() == typeof(Vector3))
            {
                Vector3 lhsValue = (lhs as Nullable<Vector3>).Value;
                Vector3 rhsValue = (rhs as Nullable<Vector3>).Value;

                return (lhsValue * (1 - weight) + rhsValue * weight) as Nullable<T>;
            }
            if (TypeHelper<T>.ToType() == typeof(Color4))
            {
                Color4 lhsValue = (lhs as Nullable<Color4>).Value;
                Color4 rhsValue = (rhs as Nullable<Color4>).Value;

                return new Color4((lhsValue.R * (1 - weight) + rhsValue.R * weight),
                    (lhsValue.G * (1 - weight) + rhsValue.G * weight),
                    (lhsValue.B * (1 - weight) + rhsValue.B * weight),
                    (lhsValue.A * (1 - weight) + rhsValue.A * weight)) as Nullable<T>;
            }
            
            throw new NotImplementedException();
        }
    }
}
