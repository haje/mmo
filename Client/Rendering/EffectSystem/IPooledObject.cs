﻿namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public interface IPooledObject
    {
        bool IsAlive
        {
            get;
        }
        
        void Reset();
    }
}