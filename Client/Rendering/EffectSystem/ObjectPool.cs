﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using HAJE.MMO.Client.UI.Tool;
using System.Text.RegularExpressions;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public static class ObjectPool
    {
        static ObjectPool()
        {
            isWatchOn = false;
        }

        private static Boolean isWatchOn;
        public static Boolean IsWatchOn
        {
            get
            {
                return isWatchOn;
            }
            private set
            {
                if (isWatchOn == value)
                    return;

                if (value == false)
                    GameSystem.DebugOutput.RemoveWatch(WatchCategoryString);
                isWatchOn = value;
            }
        }
        public const String WatchCategoryString = "ObjectPool@EffectSystem";

        public static String FullNameToSimpleName(String name)
        {
            Regex r = new Regex(".+? EffectSystem. (.+)", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
            return r.Match(name).Groups[1].ToString();
        }
        public static void UpdateWatch<T>(List<T> aliveList, Dictionary<Type, Queue<T>> garbage)
        {
            if (IsWatchOn == true)
            {
                GameSystem.DebugOutput.SetWatch(
                    String.Format("Alive[{0}]", FullNameToSimpleName(typeof(T).ToString())), 
                    WatchCategoryString,
                    aliveList.Count);
                foreach (Type t in garbage.Keys)
                    GameSystem.DebugOutput.SetWatch(
                        String.Format("Dead[{0}]", FullNameToSimpleName(t.ToString())), 
                        WatchCategoryString,
                        garbage[t].Count);
            }
        }

        public static void TurnOnOffHandler(DebugConsole console, String[] args)
        {
            IsWatchOn ^= true;
        }
    }
    public class ObjectPool<T> where T : class, IPooledObject
    {
        private List<T> aliveList;
        public ReadOnlyCollection<T> AliveList
        {
            get
            {
                return aliveList.AsReadOnly();
            }
        }
        private List<T> copiedList;
        private Dictionary<Type, Queue<T>> garbage;
        private Dictionary<Type, ConstructorInfo> ctorList;

        public ObjectPool()
        {
            aliveList = new List<T>();
            copiedList = new List<T>();
            garbage = new Dictionary<Type, Queue<T>>();
            ctorList = new Dictionary<Type, ConstructorInfo>();
        }

        public T Create()
        {
            return Create<T>();
        }
        public Child Create<Child>() where Child : class, T
        {
            Child elem = null;
            if (garbage.ContainsKey(typeof(Child)) == false)
                garbage.Add(typeof(Child), new Queue<T>());

            if (garbage[typeof(Child)].Count != 0)
                elem = garbage[typeof(Child)].Dequeue() as Child;
            else
            {
                Type childType = typeof(Child);
                if (ctorList.ContainsKey(childType) == false)
                    ctorList.Add(childType, childType.GetConstructor(new Type[] { }));

                elem = ctorList[childType].Invoke(new Object[] { }) as Child;
            }

            (elem as IPooledObject).Reset();
            aliveList.Add(elem as T);
            return elem;
        }

        private void Swap(ref List<T> lhs, ref List<T> rhs)
        {
            List<T> temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
        public void Update()
        {
            Swap(ref aliveList, ref copiedList);
            aliveList.Clear();
            foreach (T elem in copiedList)
            {
                if ((elem as IPooledObject).IsAlive == true)
                    aliveList.Add(elem);
                else
                    garbage[elem.GetType()].Enqueue(elem);
            }

            ObjectPool.UpdateWatch<T>(aliveList, garbage);
        }
    }
}
