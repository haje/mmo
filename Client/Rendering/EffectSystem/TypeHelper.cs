﻿using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public static class TypeHelper<T>
    {
        private static Type componentType;
        static TypeHelper()
        {
            componentType = typeof(T);
        }

        public static Type ToType()
        {
            return componentType;
        }
    }
}
