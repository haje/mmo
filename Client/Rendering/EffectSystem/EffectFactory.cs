﻿using HAJE.MMO.Client.Rendering.EffectSystem.Impl;
using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public static class EffectFactory
    {
        #region .ctor
        private static EffectSystem effectSystem;
        public static void Initialize(EffectSystem effectSystem)
        {
            EffectFactory.effectSystem = effectSystem;
        }
        #endregion

        #region Combo Point
        public static Effect CreateComboPoint(int id, WorldSpaceEntity target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target.Position);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateComboPointEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Element Point
        public static Effect CreateElementPoint(int id, Vector3 position)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, position);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateElementPointEmitter(effectSystem, position));
            return eff;
        }
        #endregion
        #region Cast
        public static Effect CreateCastLoop(int id, WorldSpaceEntity target)
        {
            var eff = effectSystem.CreateEffect<TracerEffect>(id, target.Position);
            eff.Initialize(target);
            eff.AddEmitterList(ParticleEmitterFactory.CreateCastLoopEmitter(effectSystem, target));
            return eff;
        }
        public static Effect CreateEndCast(int id, WorldSpaceEntity target)
        {
            var eff = effectSystem.CreateEffect<TracerEffect>(id, target.Position);
            eff.Initialize(target);
            eff.AddEmitterList(ParticleEmitterFactory.CreateEndCastEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Hit
        public static Effect CreateHitEffect(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateHitEffectEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Cursor
        public static Effect CreateCursorParticle(int id, EffectSystem particleSystem, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateCursorParticle(particleSystem, target));
            return eff;
        }
        #endregion
        #region Frostwave
        public static Effect CreateFrostWave(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneExplosionEmitter(effectSystem, target));

            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(new Vector3(target.X, target.Y + 2.0f, target.Z), Vector3.UnitX, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/IceShard2.obj"), TextureBlendTypes.Additive, new Color4(0.7f, 0.7f, 1f, 0.75f));


            EffectLight l = effectSystem.CreateLight(new Vector3(target.X, target.Y + 2f, target.Z), new Vector3(0.7f, 0.7f, 1f), 0.5f);


            eff.AddLightList(new EffectLight[] { l });
            //var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            //comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)2.3f);
            //md.AddComponent(comp1);

            md.MeshDescriptor.ScaleFactor = effectSystem.Random.NextFloat(2f, 6f) * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            return eff;
        }
        #endregion
        #region Freeze
        public static Effect CreateFreeze(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneExplosionEmitter(effectSystem, target));

            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(new Vector3(target.X, target.Y + 2.0f, target.Z), Vector3.UnitX, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/IceShard2.obj"), TextureBlendTypes.Additive, new Color4(0.7f, 0.7f, 1f, 0.75f));


            EffectLight l = effectSystem.CreateLight(new Vector3(target.X, target.Y + 2f, target.Z), new Vector3(0.7f, 0.7f, 1f), 0.5f);


            eff.AddLightList(new EffectLight[] { l });
            //var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            //comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)2.3f);
            //md.AddComponent(comp1);

            md.MeshDescriptor.ScaleFactor = effectSystem.Random.NextFloat(2f, 6f) * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            return eff;
        }
        #endregion
        #region FrostNova
        public static Effect CreateFrostNova(int id, Vector3 target)
        {
            float cRadius;
            float cDelay, cDuration; //no slow effect if duration is 0
            

            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFrostNovaEmitter(effectSystem, new Vector3(target.X, target.Y + 0.5f, target.Z)));
           
            {
                for (int i = 0; i < 5; i++) {
                    EffectMeshDescriptor sWave = effectSystem.CreateEffectMeshDescriptor(new Vector3(target.X, target.Y, target.Z), Vector3.UnitX, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/Shockwave_1.obj"), TextureBlendTypes.Additive, new Color4(0.7f, 0.8f, 1f, 1f));
                    var rotMat3 = Matrix4.CreateFromAxisAngle(Vector3.UnitX, (Radian)(Degree)(-90f));
                    sWave.MeshDescriptor.RotateLocal(ref rotMat3);
                    //sWave.MeshDescriptor.ScaleFactor = Vector3.One * 2.2f;
                    sWave.MeshDescriptor.ColorTint = new Color4(0f, 0f, 0f, 0f);

                    var rot = Matrix4.CreateFromAxisAngle(Vector3.UnitY, (Radian)effectSystem.Random.NextDegree((Degree)0, (Degree)360));
                    sWave.MeshDescriptor.RotateLocal(ref rot);

                    var lt = effectSystem.CreateComponent<CommonComponent.LifeTime>();
                    lt.Initialize((Second)1.1f);
                    sWave.AddComponent(lt);

                    var scale1 = effectSystem.CreateComponent<MeshComponent.ScaleOverTime>();
                    scale1.Initialize(Vector3.One * 0.2f, Vector3.One * 0.8f, (Second)0.2f, (Second)0.55f);
                    var scale2 = effectSystem.CreateComponent<MeshComponent.ScaleOverTime>();
                    scale2.Initialize(Vector3.One * 0.8f, Vector3.One * 3.5f, Second.Zero, (Second)0.8f);

                    var seq = effectSystem.CreateComponent<CommonComponent.Sequence>();
                    seq.Initialize();
                    seq.AddComponent(scale1);
                    seq.AddComponent(scale2);

                    sWave.AddComponent(seq);

                    
                    var color1 = effectSystem.CreateComponent<MeshComponent.MeshColorOverTime>();
                    color1.Initialize(new Color4(0f, 0f, 0f, 0f), new Color4(85, 85, 255, 200), (Second)0.2f, (Second)0.65f);
                    var color2 = effectSystem.CreateComponent<MeshComponent.MeshColorOverTime>();
                    color2.Initialize(new Color4(85, 85, 255, 200), new Color4(0f, 0f, 0f, 0f), Second.Zero, (Second)0.25f);

                    var seq2 = effectSystem.CreateComponent<CommonComponent.Sequence>();
                    seq2.Initialize();
                    seq2.AddComponent(color1);
                    seq2.AddComponent(color2);
                    sWave.AddComponent(seq2);
                    eff.AddMeshList(new EffectMeshDescriptor[] { sWave });
             }
            }

            EffectLight l = effectSystem.CreateLight(new Vector3(target.X, target.Y + 2f, target.Z), new Vector3(0.7f, 0.7f, 1f), 0.5f);

            var light_lt = effectSystem.CreateComponent<CommonComponent.LifeTime>();
            light_lt.Initialize((Second)0.8f);
            l.AddComponent(light_lt);

            eff.AddLightList(new EffectLight[] { l });
            return eff;
        }
        #endregion
        #region Icicles
        public static Effect CreateIcicle(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneExplosionEmitter(effectSystem, target));

            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(new Vector3(target.X, target.Y + 2.0f, target.Z), Vector3.UnitX, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/IceShard2.obj"), TextureBlendTypes.Additive, new Color4(0.7f, 0.7f, 1f, 0.75f));
            //var rotMat = Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)(Degree)90f);
            //md.MeshDescriptor.RotateLocal(ref rotMat);


            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)(Degree)90));
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)effectSystem.Random.NextDegree()));
            //Matrix4 aaa = Matrix4.CreateFromAxisAngle(md.MeshDescriptor.InitLookingAtDirection,effectSystem.Random.NextFloat(0,350f));
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, aaa);
            //md.MeshDescriptor.LookingAtDirection = Vector3.UnitY+ new Vector3(effectSystem.Random.NextFloat(-1f,1f),effectSystem.Random.NextFloat(-0.3f,0.3f),effectSystem.Random.NextFloat(-0.3f,0.3f));
            //md.MeshDescriptor.ScaleFactor = effectSystem.Random.GetUnitVector();

            var comp1 = effectSystem.CreateComponent<MeshComponent.MeshRotationOverTime>();
            md.AddComponent(comp1);
            comp1.Initialize(Vector3.One,0f);

            EffectLight l = effectSystem.CreateLight(new Vector3(target.X, target.Y + 2f, target.Z), new Vector3(0.7f, 0.7f, 1f), 0.5f);


            eff.AddLightList(new EffectLight[] { l });
            //var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            //comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)2.3f);
            //md.AddComponent(comp1);

            md.MeshDescriptor.ScaleFactor = effectSystem.Random.NextFloat(2f, 6f) * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            return eff;
        }
        #endregion

        #region Arcane Explosion
        public static Effect CreateArcaneExplosion(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneExplosionEmitter(effectSystem, target));

            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(new Vector3(target.X, target.Y +2.0f, target.Z),Vector3.UnitX, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/IceShard2.obj"), TextureBlendTypes.Additive, new Color4(0.7f, 0.7f, 1f, 0.75f));
            //var rotMat = Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)(Degree)90f);
            //md.MeshDescriptor.RotateLocal(ref rotMat);

            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)(Degree)90));
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)effectSystem.Random.NextDegree()));
            //Matrix4 aaa = Matrix4.CreateFromAxisAngle(md.MeshDescriptor.InitLookingAtDirection,effectSystem.Random.NextFloat(0,350f));
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, aaa);
            //md.MeshDescriptor.LookingAtDirection = Vector3.UnitY+ new Vector3(effectSystem.Random.NextFloat(-1f,1f),effectSystem.Random.NextFloat(-0.3f,0.3f),effectSystem.Random.NextFloat(-0.3f,0.3f));
            //md.MeshDescriptor.ScaleFactor = effectSystem.Random.GetUnitVector();

            //var comp1 = effectSystem.CreateComponent<MeshComponent.MeshRotationOverTime>();
            //md.AddComponent(comp1);
            //comp1.Initialize(Vector3.One);

            EffectLight l = effectSystem.CreateLight(new Vector3(target.X, target.Y + 2f, target.Z), new Vector3(0.7f, 0.7f, 1f), 0.5f);
            

            eff.AddLightList(new EffectLight[] { l });
            //var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            //comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)2.3f);
            //md.AddComponent(comp1);

            md.MeshDescriptor.ScaleFactor = effectSystem.Random.NextFloat(2f,6f) * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            return eff;
        }
        #endregion
        #region Pyroblast
        public static Effect CreatePyroblast(int id, TerrainMeshDescriptor terrain, Vector3 at)
        {

            var eff = effectSystem.CreateEffect<StandardEffect>(id, at);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreatePyroblastEmitter(effectSystem, terrain, at));
            eff.AddEmitterList(ParticleEmitterFactory.CreatePyroblastEmitter_Attenuative(effectSystem, terrain, at));

            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(at, Vector3.UnitZ, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/Fireball_Core.obj"), TextureBlendTypes.Attenuative);
            

            md.AddComponent(effectSystem.CreateComponent<MeshComponent.MeshRotateToTangent>());
            md.MeshDescriptor.ScaleFactor = 2.5f * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            EffectLight l = effectSystem.CreateLight(at, new Vector3(1f, 0.2f, 0.2f), 1.5f);


            eff.AddLightList(new EffectLight[] { l });

            return eff;
        }
        #endregion
        #region Ice Arrow
        public static Effect CreateIceArrow(int id, TerrainMeshDescriptor terrain, Vector3 at)
        {
            
            var eff = effectSystem.CreateEffect<StandardEffect>(id, at);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateIceArrowEmitter(effectSystem, terrain, at));

            // Temporary code for test
            //EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(at, Vector3.One, (new MeshProvider()).Cube);
            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(at, Vector3.UnitZ, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/IceShardNew.obj"), TextureBlendTypes.Additive);
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection,Matrix4.LookAt(eff.position,at,Vector3.UnitX));
            //md.MeshDescriptor.
            //md.MeshDescriptor.LookingAtDirection = Vector3.UnitZ;
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitY, (Radian)(Degree)90));
            //md.MeshDescriptor.LookingAtDirection = Vector3.Transform(md.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)(Degree)90));
            //var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            //comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)2.3f);
            //md.AddComponent(comp1);

            md.AddComponent(effectSystem.CreateComponent<MeshComponent.MeshRotateToTangent>());
            md.MeshDescriptor.ScaleFactor = 2.5f * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            EffectLight l = effectSystem.CreateLight(at, new Vector3(0.6f, 0.6f, 1f), 3);


            eff.AddLightList(new EffectLight[] { l });

            return eff;

        }
        #endregion
        #region Arcane Missile
        public static Effect CreateArcaneMissile(int id, TerrainMeshDescriptor terrain, Vector3 at)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, at);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneMissileEmitter(effectSystem, terrain, at));

            // Temporary code for test
            //EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(at, Vector3.One, (new MeshProvider()).Cube);
            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(at, Vector3.One, Vector3.UnitY, (new MeshProvider()).GetMesh("Resource/Model/IceShardNew.obj"), TextureBlendTypes.Additive);
            
            //var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            //comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)2.3f);
            //md.AddComponent(comp1);

            md.MeshDescriptor.ScaleFactor = 2.5f * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            EffectLight l = effectSystem.CreateLight(at, new Vector3(1, 0, 1), 3);
            var light1 = effectSystem.CreateComponent<LightComponent.LinearIntensityChanger>();
            light1.Initialize(new Vector3(1, 0, 1), new Vector3(0, 1, 0), Second.Zero, (Second)0.3f);
            var light2 = effectSystem.CreateComponent<LightComponent.LinearIntensityChanger>();
            light2.Initialize(new Vector3(0, 1, 0), new Vector3(1, 0, 1), Second.Zero, (Second)0.3f);

            var seq = effectSystem.CreateComponent<CommonComponent.Sequence>();
            seq.Initialize();
            seq.AddComponent(light1);
            seq.AddComponent(light2);

            var il = effectSystem.CreateComponent<CommonComponent.InfiniteLoop>();
            il.Initialize(seq);

            l.AddComponent(il);

            eff.AddLightList(new EffectLight[] { l });

            return eff;
        }
        #endregion

        #region Fireball
        public static Effect CreateFireball(int id, TerrainMeshDescriptor terrain, Vector3 from, Vector3 to)
        {
            Second lifeTime = (Second)(from - to).Length / 40f;
            var eff = effectSystem.CreateEffect<StandardEffect>(id, from);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFireBallEmitter(effectSystem, terrain, from, to, lifeTime));
            return eff;
        }
        public static Effect CreateFireball(int id, TerrainMeshDescriptor terrain, Vector3 from, WorldSpaceEntity to, Vector3 offset)
        {
            Second lifeTime = (Second)(from - to.Position).Length / 40f;
            var eff = effectSystem.CreateEffect<StandardEffect>(id, from);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFireBallEmitter(effectSystem, terrain, from, to, offset, lifeTime));
            return eff;
        }
        #endregion
        #region Flame Strike
        public static Effect CreateFlameStrike(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFlameStrikeEmitter(effectSystem, target));
            return eff;
        }
        #endregion

        #region Find/Remove
        public static Effect FindEffect(int id)
        {
            return effectSystem.FindEffect(id);
        }
        public static void RemoveEffect(int id)
        {
            effectSystem.RemoveEffect(id);
        }
        #endregion
    }
}
