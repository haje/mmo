﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region HitEffect
        public static ParticleEmitter[] CreateHitEffectEmitter(EffectSystem system, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[5];
            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();


                //todo : add components
                emitter.Position = position;
                // // emitter.IsActivated = true;
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.02f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.21f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)0.2f, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[2];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(236, 120, 255, 255);

                        Color4 eColor = Color4.Blue;
                        for (int iter = 0; iter < 2; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 2.1f, ParticleType.Ring1, (Second)0.5f);



                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//GlowingOrb
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;



                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.1f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 15, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[50];
                        //Color4 sColor = new Color4(230,0,255,255);
                        Color4 sColor = Color4.MediumPurple;
                        sColor.A = 0.8f;
                        for (int iter = 0; iter < 50; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, Vector2.One * 1.5f, ParticleType.Glow1, (Second)1f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[1] = emitter;

            }
            {//Sparkle
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;


                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.4f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 15, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[50];
                        Color4 sColor = new Color4(230, 0, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 50; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, Vector2.One, ParticleType.Circle2, (Second)1f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[2] = emitter;

            }
            {//HorizontalLine
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.5f);
                emitter.AddComponent(lifeTime);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[1];
                        Color4 sColor = new Color4(230, 0, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 0.6f;
                        for (int iter = 0; iter < 1; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateBasicParticle(s, e, t, null, sColor, Vector2.One * 2f, ParticleType.Side, (Second)0.2f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[3] = emitter;

            }
            {//GlowingOrb_center
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;


                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.1f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[2];
                        Color4 sColor = Color4.Purple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 2; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            //Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, eColor, Vector2.One * 3, ParticleType.Glow1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 2.5f, ParticleType.Glow1, (Second)0.8f);

                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox * 0.6f, p.BoundingBox, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);

                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[4] = emitter;

            }

            return emitters;
        }
        #endregion
    }
}
