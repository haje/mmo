﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleFactory
    {
        public static Particle CreateLoopingParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
            WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
            ParticleType particleType, Second lifeTime)
        {
            return CreateLoopingParticle(system, emitter, timeRemain, target, color, boundingBox, particleType, lifeTime, TextureBlendTypes.Additive);
        }

        public static Particle CreateLoopingParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
            WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
            ParticleType particleType, Second lifeTime,TextureBlendTypes bType)
        {
            Particle particle;
            if (bType == TextureBlendTypes.Attenuative)
            {
                particle = system.CreateAttenuativeParticle();
            }
            else
            {
                particle = system.CreateAdditiveParticle();
            }
            
            particle.BoundingBox = boundingBox;
            particle.Color = color;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));


            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(Vector3.Zero, system.Random.GetUnitVector3() * system.Random.NextFloat(0.5f, 0.9f) * 0.2f, Second.Zero, lTime.TimeRemain);
            particle.AddComponent(linMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, Color4.Transparent, InterpolationMethod.Linear);
            colorkf.AddKeyFrame((Second)0.2f, particle.Color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, new Color4(color.R, color.G, color.B, 0), InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(Color4.Transparent, particle.Color, Second.Zero, (Second)0.2f , ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);
            
            var colorFade2 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            //colorFade2.Initialize(color, Color4.Transparent, Second.Zero, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            colorFade2.Initialize(particle.Color, new Color4(color.R,color.G,color.B,0), Second.Zero, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade2);
            */
            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;

        }
        public static Particle CreateLoopingParticle_Local(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
           WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
           ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox;
            particle.Color = color;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));

            particle.BoundingBox *= system.Random.NextFloat(0.8f, 1.2f);

            var eTracer = system.CreateComponent<CommonComponent.EmitterTracer>();
            eTracer.Initialize(emitter);
            particle.AddComponent(eTracer);

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(Vector3.Zero, system.Random.GetUnitVector3() * system.Random.NextFloat(0.5f, 0.9f) * 0.3f, Second.Zero, lTime.TimeRemain);
            particle.AddComponent(linMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, Color4.Transparent, InterpolationMethod.Linear);
            colorkf.AddKeyFrame((Second)0.1f, particle.Color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, new Color4(color.R, color.G, color.B, 0), InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(Color4.Transparent, particle.Color, Second.Zero, (Second)0.2f , ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);
            
            var colorFade2 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            //colorFade2.Initialize(color, Color4.Transparent, Second.Zero, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            colorFade2.Initialize(particle.Color, new Color4(color.R,color.G,color.B,0), Second.Zero, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade2);
            */

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;

        }

        public static Particle CreateLoopingParticle_Pulsar(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
           WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
           ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox * system.Random.NextFloat(0.3f, 2f);
            particle.Color = color;
            //particle.Rotation = system.Random.NextRadian();
            particle.Rotation = Radian.Zero;
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));


            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(Vector3.Zero, system.Random.GetUnitVector3() * system.Random.NextFloat(0.5f, 0.9f) * 0.2f, Second.Zero, lTime.TimeRemain);
            particle.AddComponent(linMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, Color4.Transparent, InterpolationMethod.Linear);
            colorkf.AddKeyFrame((Second)0.2f, particle.Color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, new Color4(color.R, color.G, color.B, 0), InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(Color4.Transparent, particle.Color, Second.Zero, (Second)0.2f , ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);
            
            var colorFade2 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade2.Initialize(particle.Color, new Color4(color.R, color.G, color.B, 0), Second.Zero, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade2);
            */

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;

        }
    }
}
