﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region Fireball
        public static ParticleEmitter[] CreateFireBallEmitter(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 start, Vector3 end, Second lTime)
        {
            ParticleEmitter emitter = system.CreateParticleEmitter();
            emitter.Position = start;
            // emitter.IsActivated = true;

            var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
            lifeTime.Initialize(lTime);
            emitter.AddComponent(lifeTime);
            
            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
            emitter.AddComponent(linMove);

            var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
            terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
            emitter.AddComponent(terComp);

            var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
            pGen.Initialize((Second)1f/120, Second.Zero,
                (s, e, t) =>
                {
                    Particle[] ps = new Particle[1];
                    for (int iter = 0; iter < 1; iter++)
                    {
                        Color4 startColor = Color4.DarkRed;
                        startColor.A = 0.5f;
                        Color4 endColor = Color4.Yellow;
                        endColor.A = 0.1f;

                        Particle p = ParticleFactory.CreateStandParticle(s, e, t, 
                            null, startColor, endColor, Vector2.One * 0.25f, 
                            system.Random.GetRandomParticleType(ParticleCategory.Glow), (Second)0.2f);
                        ps[iter] = p;
                    }

                    return ps;
                },
                system, emitter);
            emitter.AddComponent(pGen);

            return new ParticleEmitter[] { emitter };
        }
        public static ParticleEmitter[] CreateFireBallEmitter(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 start, WorldSpaceEntity end, Vector3 offset, Second lTime)
        {
            ParticleEmitter emitter = system.CreateParticleEmitter();
            emitter.Position = start;
            // emitter.IsActivated = true;

            var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
            lifeTime.Initialize(lTime);
            emitter.AddComponent(lifeTime);

            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(start, end.Position + offset, Second.Zero, lifeTime.TimeRemain);
            emitter.AddComponent(linMove);

            /*var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
            objTracer.Initialize(end);
            emitter.AddComponent(objTracer);*/

            var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
            pGen.Initialize((Second)1f / 120, Second.Zero,
                (s, e, t) =>
                {
                    Particle[] ps = new Particle[1];
                    for (int iter = 0; iter < 1; iter++)
                    {
                        Color4 startColor = Color4.DarkRed;
                        startColor.A = 0.5f;
                        Color4 endColor = Color4.Yellow;
                        endColor.A = 0.1f;

                        Particle p = ParticleFactory.CreateStandParticle(s, e, t,
                            null, startColor, endColor, Vector2.One * 0.25f,
                            system.Random.GetRandomParticleType(ParticleCategory.Glow), (Second)0.2f);
                        ps[iter] = p;
                    }

                    return ps;
                },
                system, emitter);
            emitter.AddComponent(pGen);

            return new ParticleEmitter[] { emitter };
        }
        #endregion
    }
}