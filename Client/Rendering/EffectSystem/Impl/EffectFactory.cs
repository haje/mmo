﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public static class EffectFactory
    {
        #region .ctor
        private static EffectSystem effectSystem;
        public static void Initialize(EffectSystem effectSystem)
        {
            EffectFactory.effectSystem = effectSystem;
        }
        #endregion

        #region Combo Point
        public static Effect CreateComboPoint(int id, WorldSpaceEntity target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target.Position);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateComboPointEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Cast
        public static Effect CreateCastLoop(int id, WorldSpaceEntity target)
        {
            var eff = effectSystem.CreateEffect<TracerEffect>(id, target.Position);
            eff.Initialize(target);
            eff.AddEmitterList(ParticleEmitterFactory.CreateCastLoopEmitter(effectSystem, target));
            return eff;
        }
        public static Effect CreateEndCast(int id, WorldSpaceEntity target)
        {
            var eff = effectSystem.CreateEffect<TracerEffect>(id, target.Position);
            eff.Initialize(target);
            eff.AddEmitterList(ParticleEmitterFactory.CreateEndCastEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Hit
        public static Effect CreateHitEffect(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateHitEffectEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Cursor
        public static Effect CreateCursorParticle(int id, EffectSystem particleSystem, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateCursorParticle(particleSystem, target));
            return eff;
        }
        #endregion

        #region Arcane Explosion
        public static Effect CreateArcaneExplosion(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneExplosionEmitter(effectSystem, target));
            return eff;
        }
        #endregion
        #region Arcane Missile
        public static Effect CreateArcaneMissile(int id, TerrainMeshDescriptor terrain, Vector3 at)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, at);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateArcaneMissileEmitter(effectSystem, terrain, at));

            // Temporary code for test
            EffectMeshDescriptor md = effectSystem.CreateEffectMeshDescriptor(at, Vector3.One, (new MeshProvider()).Cube);
            var comp1 = effectSystem.CreateComponent<CommonComponent.LinearMoving>();
            comp1.Initialize(at, at + Vector3.UnitY * 10, Second.Zero, (Second)0.3f);
            md.AddComponent(comp1);

            md.MeshDescriptor.ScaleFactor = 1.5f * Vector3.One;
            eff.AddMeshList(new EffectMeshDescriptor[] { md });

            EffectLight l = effectSystem.CreateLight(at, new Vector3(1, 0, 1), 3);
            var light1 = effectSystem.CreateComponent<LightComponent.LinearIntensityChanger>();
            light1.Initialize(new Vector3(1, 0, 1), new Vector3(0, 1, 0), Second.Zero, (Second)0.3f);
            var light2 = effectSystem.CreateComponent<LightComponent.LinearIntensityChanger>();
            light2.Initialize(new Vector3(0, 1, 0), new Vector3(1, 0, 1), Second.Zero, (Second)0.3f);

            var seq = effectSystem.CreateComponent<CommonComponent.Sequence>();
            seq.Initialize();
            seq.AddComponent(light1);
            seq.AddComponent(light2);

            var il = effectSystem.CreateComponent<CommonComponent.InfiniteLoop>();
            il.Initialize(seq);

            l.AddComponent(il);

            eff.AddLightList(new EffectLight[] { l });

            return eff;
        }
        #endregion

        #region Fireball
        public static Effect CreateFireball(int id, TerrainMeshDescriptor terrain, Vector3 from, Vector3 to)
        {
            Second lifeTime = (Second)(from - to).Length / 40f;
            var eff = effectSystem.CreateEffect<StandardEffect>(id, from);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFireBallEmitter(effectSystem, terrain, from, to, lifeTime));
            return eff;
        }
        public static Effect CreateFireball(int id, TerrainMeshDescriptor terrain, Vector3 from, WorldSpaceEntity to, Vector3 offset)
        {
            Second lifeTime = (Second)(from - to.Position).Length / 40f;
            var eff = effectSystem.CreateEffect<StandardEffect>(id, from);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFireBallEmitter(effectSystem, terrain, from, to, offset, lifeTime));
            return eff;
        }
        #endregion
        #region Flame Strike
        public static Effect CreateFlameStrike(int id, Vector3 target)
        {
            var eff = effectSystem.CreateEffect<StandardEffect>(id, target);
            eff.Initialize();
            eff.AddEmitterList(ParticleEmitterFactory.CreateFlameStrikeEmitter(effectSystem, target));
            return eff;
        }
        #endregion

        #region Find/Remove
        public static Effect FindEffect(int id)
        {
            return effectSystem.FindEffect(id);
        }
        public static void RemoveEffect(int id)
        {
            effectSystem.RemoveEffect(id);
        }
        #endregion
    }
}
