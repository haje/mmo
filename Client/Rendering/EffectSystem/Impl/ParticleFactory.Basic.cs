﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleFactory
    {
        public static Particle CreateBasicParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
           WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
           ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox*system.Random.NextFloat(0.5f,2f);
            particle.Color = Color4.Transparent;
            particle.Rotation = Radian.Zero;
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));
            

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, Color4.Transparent, InterpolationMethod.Linear);
            colorkf.AddKeyFrame((Second)0.1f, color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, Color4.Transparent, InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(Color4.Transparent, color, Second.Zero, (Second)0.1f, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);

            var colorFade2 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade2.Initialize(color, Color4.Transparent, (Second)0.1f, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade2);
            */

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
    }
}
