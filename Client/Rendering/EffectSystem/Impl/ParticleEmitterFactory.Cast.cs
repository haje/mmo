﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region CastingLoop
        public static ParticleEmitter[] CreateCastLoopEmitter(EffectSystem system, WorldSpaceEntity target)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[2];
            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = target.Position;
                // // emitter.IsActivated = true;

                /*var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                objTracer.Initialize(target);
                emitter.AddComponent(objTracer);*/

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 12, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[8];
                        //Color4 sColor = Color4.MediumPurple;
                        //Color4 sColor = new Color4(236, 120, 255, 250);

                        Color4 sColor = Color4.Purple;
                        sColor.G = 0.03f;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 8; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 0.3f, ParticleType.Ring5, (Second)0.5f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);



                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Glow
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = target.Position;
                // // emitter.IsActivated = true;

                /*var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                objTracer.Initialize(target);
                emitter.AddComponent(objTracer);*/

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 12, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[6];

                        Color4 sColor = new Color4(236, 70, 255, 255);
                        //Color4 sColor = Color4.Orange;

                        sColor.A = 0.5f;
                        for (int iter = 0; iter < 6; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle_Pulsar(s, e, t, null, sColor, Vector2.One * 0.25f, ParticleType.Glow1, (Second)0.5f);
                            //Particle p = ParticleFactory.CreateBasicParticle(s, e, t, null, sColor, Vector2.One * 2f, ParticleType.Side, (Second)0.2f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var emitterTracer = system.CreateComponent<CommonComponent.EmitterTracer>();
                            emitterTracer.Initialize(emitter);
                            p.AddComponent(emitterTracer);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }


            return emitters;

        }
        #endregion        
        #region EndCast
        public static ParticleEmitter[] CreateEndCastEmitter(EffectSystem system, WorldSpaceEntity target)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[2];



            {//HorizontalLine
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = target.Position;
                // // emitter.IsActivated = true;

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.5f);
                emitter.AddComponent(lifeTime);

                /*var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                objTracer.Initialize(target);
                emitter.AddComponent(objTracer);*/

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[2];
                        Color4 sColor = new Color4(230, 35, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 0.6f;
                        for (int iter = 0; iter < 2; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateBasicParticle(s, e, t, null, sColor, Vector2.One * 1f, ParticleType.Side, (Second)0.2f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[0] = emitter;

            }
            {//Glow

                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = target.Position;
                // // emitter.IsActivated = true;

                /*var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                objTracer.Initialize(target);
                emitter.AddComponent(objTracer);*/

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.7f);
                emitter.AddComponent(lifeTime);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 24, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[4];

                        Color4 sColor = new Color4(236, 15, 255, 255);
                        //Color4 sColor = Color4.Orange;

                        sColor.A = 0.5f;
                        for (int iter = 0; iter < 4; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle_Pulsar(s, e, t, null, sColor, Vector2.One * 0.6f, ParticleType.Glow1, (Second)0.4f);
                            //Particle p = ParticleFactory.CreateBasicParticle(s, e, t, null, sColor, Vector2.One * 2f, ParticleType.Side, (Second)0.2f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
                            linMove.Initialize(Vector3.Zero, new Vector3(0f, 1f, 0f), Second.Zero, (Second)0.5f);
                            p.AddComponent(linMove);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }

            return emitters;

        }
        #endregion
    }
}