﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region ArcaneMissile
        public static ParticleEmitter[] CreateArcaneMissileEmitter(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[2];

            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 120, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[12];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(236, 120, 255, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 12; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 0.6f, ParticleType.Ring5, (Second)0.1f, TextureBlendTypes.Additive);

                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);



                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Dust
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 120, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[3];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(236, 7, 255, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 3; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 2.4f, ParticleType.Glow1, (Second)0.3f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }


            return emitters;
        }
        public static ParticleEmitter[] CreateArcaneMissileEmitter(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 start, Vector3 offset, Second lTime)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[2];

            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = start;
                // emitter.IsActivated = true;

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end.Position + offset, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);

                var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                objTracer.Initialize(end);
                emitter.AddComponent(objTracer);*/

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 120, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[12];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(236, 120, 255, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 12; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 0.6f, ParticleType.Ring5, (Second)0.1f,TextureBlendTypes.Additive);

                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);



                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Dust
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = start;
                // emitter.IsActivated = true;

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end.Position + offset, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);

                var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                objTracer.Initialize(end);
                emitter.AddComponent(objTracer);*/

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 120, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[3];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(236, 7, 255, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 3; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 2.4f, ParticleType.Glow1, (Second)0.3f, TextureBlendTypes.Additive);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }


            return emitters;
        }
        #endregion
    }
}