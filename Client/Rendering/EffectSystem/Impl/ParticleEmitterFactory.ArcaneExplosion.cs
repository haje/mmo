﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region ArcaneExplosion
        public static ParticleEmitter[] CreateArcaneExplosionEmitter(EffectSystem system, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[6];
            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();


                //todo : add components
                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.72f);
                emitter.AddComponent(delay);*/

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.21f + 0.72f);
                emitter.AddComponent(lifeTime);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)0.2f, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[3];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(236, 45, 255, 255);

                        Color4 eColor = Color4.Blue;
                        for (int iter = 0; iter < 3; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);



                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter,
                    (Second)0.72f,
                    Second.Infinity);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//GlowingOrb
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.7f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.2f + 0.7f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 5, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[50];
                        Color4 sColor = new Color4(230, 45, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 0.8f;
                        for (int iter = 0; iter < 50; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, Vector2.One * 3f, ParticleType.Glow1, (Second)1f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter,
                    (Second)0.7f,
                    Second.Infinity);
                emitter.AddComponent(pGen);
                emitters[1] = emitter;

            }
            {//Sparkle
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.7f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.4f + 0.7f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 15, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[50];
                        Color4 sColor = new Color4(230, 0, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 50; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, Vector2.One, ParticleType.Circle2, (Second)1f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter,
                    (Second)0.7f,
                    Second.Infinity);
                emitter.AddComponent(pGen);
                emitters[2] = emitter;

            }
            {//Energy Gathering
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.4f);
                emitter.AddComponent(lifeTime);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[50];
                        Color4 sColor = new Color4(230, 0, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 50; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateGatheringParticle(s, e, t, null, sColor, Vector2.One, ParticleType.Circle2, (Second)0.7f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[3] = emitter;

            }
            {//HorizontalLine
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.5f);
                emitter.AddComponent(lifeTime);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[1];
                        Color4 sColor = new Color4(230, 0, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 0.6f;
                        for (int iter = 0; iter < 1; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateBasicParticle(s, e, t, null, sColor, Vector2.One * 2f, ParticleType.Side, (Second)0.2f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[4] = emitter;

            }
            {//GlowingOrb_center
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;


                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.1f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[2];
                        Color4 sColor = Color4.Purple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 2; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            //Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, eColor, Vector2.One * 3, ParticleType.Glow1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 2.5f, ParticleType.Glow1, (Second)0.8f, TextureBlendTypes.Additive);

                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox * 0.6f, p.BoundingBox, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);

                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[5] = emitter;

            }

            return emitters;

        }
        #endregion
    }
}