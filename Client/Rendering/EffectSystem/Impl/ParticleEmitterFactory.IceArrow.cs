﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        public static ParticleEmitter[] CreateIceArrowEmitter(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[3];
            
            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;


                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 20, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[2];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(2, 5, 25, 200);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 2; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 0.9f, ParticleType.Ring1, (Second)0.3f);

                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);



                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Dust
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 60, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[3];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(4, 9, 25, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 3; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 1.2f, ParticleType.Glow1, (Second)0.6f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }
            {//Sparkle
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.7f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.4f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 30, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[10];
                        Color4 sColor = new Color4(35, 35, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 10; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            //Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, Vector2.One, ParticleType.Circle2, (Second)0.4f);
                            Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor,Color4.Transparent, Vector2.One*0.5f, ParticleType.Circle2, (Second)0.5f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One, ParticleType.Circle2, (Second)0.7f);
                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter);
                emitter.AddComponent(pGen);
                emitters[2] = emitter;

            }


            return emitters;
        }
    }
}
