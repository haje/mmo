﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleFactory
    {
        public static Particle CreateGatheringParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
           WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
           ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox;
            particle.Color = Color4.Transparent;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));
            particle.Position += system.Random.GetUnitVector3() * system.Random.NextFloat(0.2f, 1.2f) * 4.3f;

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(particle.Position, emitter.Position, Second.Zero, lTime.TimeRemain);
            particle.AddComponent(linMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, Color4.Transparent, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, color, InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(Color4.Transparent, color, Second.Zero, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);
            */


            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
    }
}
