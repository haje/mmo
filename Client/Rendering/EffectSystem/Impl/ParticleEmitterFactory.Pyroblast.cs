﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        public static ParticleEmitter[] CreatePyroblastEmitter_Attenuative(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 position)
        {

            ParticleEmitter[] emitters = new ParticleEmitter[2];
            {//Frag
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 24, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[5];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(0, 0, 0, 220);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 5; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateStationaryParticle_Fading(s, e, t, null, sColor, Vector2.One * 0.25f, ParticleType.Fragment2, (Second)0.7f,TextureBlendTypes.Attenuative,(Second)0.7f);
                           
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.7f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Smoke
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);



                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 36, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[1];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(0, 0, 0, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 1; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 1.5f, ParticleType.Fire2, (Second)1.2f,TextureBlendTypes.Attenuative);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.4f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }
            return emitters;
        }
        public static ParticleEmitter[] CreatePyroblastEmitter(EffectSystem system, TerrainMeshDescriptor terrain, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[3];

            
            {//Dust
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);

                

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 36, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[1];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(255, 15, 5, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 1; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle_Local(s, e, t, null, sColor, Vector2.One * 1.4f, ParticleType.Fire1, (Second)0.6f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.3f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Dust
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);



                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 36, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[1];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(255, 15, 5, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 1; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 1.5f, ParticleType.Fire1, (Second)0.6f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.4f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }
            {//Frag
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;

                /*var lifeTime = system.CreateEmitterComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize(lTime);
                emitter.AddComponent(lifeTime);*/

                /*var linMove = system.CreateEmitterComponent<EmitterComponent.LinearMoving>();
                linMove.Initialize(start, end, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);*/

                var terComp = system.CreateComponent<CommonComponent.TerrainCompensator>();
                terComp.Initialize(terrain, 0.7f, Second.Zero, Second.Infinity);
                emitter.AddComponent(terComp);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 36, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[5];
                        //Color4 sColor = Color4.MediumPurple;
                        Color4 sColor = new Color4(255, 145, 0, 250);

                        //Color4 sColor = Color4.Purple;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 5; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateStationaryParticle_Fading(s, e, t, null, sColor, Vector2.One * 0.25f, ParticleType.Fragment2, (Second)1.1f, TextureBlendTypes.Additive,(Second)1.1f);

                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
                            sizeFade.Initialize(p.BoundingBox, Vector2.Zero, Second.Zero, (Second)0.4f);
                            p.AddComponent(sizeFade);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[2] = emitter;
            }
            


            return emitters;
        }
    }
}
