﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleFactory
    {
        public static Particle CreateStandParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
            WorldSpaceEntity target, Color4 startColor, Color4 endColor, Vector2 boundingBox,
            ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox;
            particle.Color = startColor;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
            linMove.Initialize(Vector3.Zero, system.Random.GetUnitVector3(), Second.Zero, lTime.TimeRemain);
            particle.AddComponent(linMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, particle.Color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain * 0.8f, endColor, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, new Color4(endColor.R, endColor.G, endColor.B, 0), InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(particle.Color, endColor, Second.Zero, lTime.TimeRemain * 0.8f, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);

            var colorFade2 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade2.Initialize(endColor, new Color4(endColor.R, endColor.G, endColor.B, 0), lTime.TimeRemain * 0.8f, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade2);*/

            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
            sizeFade.Initialize(particle.BoundingBox, Vector2.Zero, Second.Zero, lTime.TimeRemain);
            particle.AddComponent(sizeFade);

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
    }
}
