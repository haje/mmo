﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region ComboPoint
        public static ParticleEmitter[] CreateComboPointEmitter(EffectSystem system, WorldSpaceEntity target)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[2];
            Vector3 rv = system.Random.GetUnitVector3();
            Vector3 rv2 = system.Random.GetUnitVector3();
            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();

                //                Vector3 rv = system.Random.GetUnitVector();
                rv.Y = 1;

                emitter.Position = target.Position + Vector3.UnitY + 1 * Vector3.Normalize(Vector3.Cross(rv, rv2));
                // emitter.IsActivated = true;

                var spMoving = system.CreateComponent<CommonComponent.SphericalMoving>();
                spMoving.Initialize(target, Vector3.UnitY, rv, emitter.Position, (Radian)2, Second.Zero, Second.Infinity);
                emitter.AddComponent(spMoving);

                //emitter.Position = target.Position;
                //System.Console.WriteLine(emitter.Position);
                //emitter.IsActivated = true; = true;

                //var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                //objTracer.Initialize(target);
                //emitter.AddComponent(objTracer);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 12, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[8];
                        //Color4 sColor = Color4.MediumPurple;
                        //Color4 sColor = new Color4(236, 120, 255, 250);

                        Color4 sColor = Color4.Purple;
                        sColor.G = 0.03f;
                        //Color4 sColor = Color4.Red;

                        for (int iter = 0; iter < 8; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 0.1f, ParticleType.Star1, (Second)0.5f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var emitterTracer = system.CreateComponent<CommonComponent.EmitterTracer>();
                            emitterTracer.Initialize(emitter);
                            p.AddComponent(emitterTracer);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[0] = emitter;
            }
            {//Glow

                //Vector3 rv = system.Random.GetUnitVector();
                rv.Y = 1;

                ParticleEmitter emitter = system.CreateParticleEmitter();
                emitter.Position = target.Position + Vector3.UnitY + 1 * Vector3.Normalize(Vector3.Cross(rv, rv2));
                // emitter.IsActivated = true;

                var spMoving = system.CreateComponent<CommonComponent.SphericalMoving>();
                spMoving.Initialize(target, Vector3.UnitY, rv, emitter.Position, (Radian)2, Second.Zero, Second.Infinity);
                emitter.AddComponent(spMoving);


                //ParticleEmitter emitter = system.CreateParticleEmitter();

                //emitter.Position = target.Position;
                //// emitter.IsActivated = true;

                // var objTracer = system.CreateEmitterComponent<EmitterComponent.ObjectTracer>();
                //objTracer.Initialize(target);
                //emitter.AddComponent(objTracer);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 24, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[4];

                        Color4 sColor = new Color4(236, 15, 255, 255);
                        //Color4 sColor = Color4.Orange;

                        sColor.A = 0.5f;
                        for (int iter = 0; iter < 4; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle_Pulsar(s, e, t, null, sColor, Vector2.One * 0.15f, ParticleType.Glow1, (Second)0.3f);
                            //Particle p = ParticleFactory.CreateBasicParticle(s, e, t, null, sColor, Vector2.One * 2f, ParticleType.Side, (Second)0.2f);
                            //Particle p = ParticleFactory.CreateStationaryParticle(s, e, t, null, sColor, Vector2.One * 4.2f, ParticleType.Ring1, (Second)1.2f);
                            var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
                            linMove.Initialize(Vector3.Zero, new Vector3(0f, 0.4f, 0f), Second.Zero, (Second)0.5f);
                            p.AddComponent(linMove);


                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);

                emitters[1] = emitter;
            }


            return emitters;
        }
        #endregion
    }
}