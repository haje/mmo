﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region ArcaneExplosion
        public static ParticleEmitter[] CreateFrostNovaEmitter(EffectSystem system, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[2];
           
            {//GlowingOrb
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.7f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.2f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 5, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[100];
                        Color4 sColor = new Color4(35, 55, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 0.8f;
                        for (int iter = 0; iter < 100; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateScatteringParticle_Horizontal(s, e, t, null, sColor, Vector2.One * 3f, ParticleType.Glow1, (Second)1f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter,Second.Zero,Second.Infinity);
                emitter.AddComponent(pGen);
                emitters[0] = emitter;

            }
            {//Sparkle
                ParticleEmitter emitter = system.CreateParticleEmitter();

                emitter.Position = position;
                // emitter.IsActivated = true;
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)0.7f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.4f + 0.3f);
                emitter.AddComponent(lifeTime);


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 15, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[50];
                        Color4 sColor = new Color4(45, 45, 255, 255);
                        //Color4 sColor = Color4.MediumPurple;
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 50; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateScatteringParticle(s, e, t, null, sColor, Vector2.One*0.4f, ParticleType.Glow1, (Second)1f);


                            ps[iter] = p;
                        }
                        return ps;
                    },
                    system,
                    emitter,(Second)0.4f,Second.Infinity);
                emitter.AddComponent(pGen);
                emitters[1] = emitter;

            }


            return emitters;
        }
        #endregion
    }
}