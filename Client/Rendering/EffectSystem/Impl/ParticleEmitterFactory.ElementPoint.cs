﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region ElementPoint
        public static ParticleEmitter[] CreateElementPointEmitter(EffectSystem system, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[1];
            Vector3 rv = system.Random.GetUnitVector3();
            Vector3 rv2 = system.Random.GetUnitVector3();
            {//Ring
                ParticleEmitter emitter = system.CreateParticleEmitter();
                emitter.Position = position + rv;

                var spMoving = system.CreateComponent<CommonComponent.SphericalMoving>();
                spMoving.Initialize(position, rv2, emitter.Position, (Radian)3, Second.Zero, Second.Infinity);
                emitter.AddComponent(spMoving);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 12, Second.Zero,
                    (EmitterComponent.Generator)((s, e, t) =>
                    {
                        Particle[] ps = new Particle[8];
                        Color4 sColor = Color4.Green;

                        for (int iter = 0; iter < 8; iter++)
                        {
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One * 0.1f, ParticleType.Star1, (Second)0.5f);
                            var emitterTracer = system.CreateComponent<CommonComponent.EmitterTracer>();
                            emitterTracer.Initialize(emitter);
                            p.AddComponent(emitterTracer);
                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);

                emitter.AddComponent(pGen);
                emitters[0] = emitter;
            }
            
            return emitters;
        }
        #endregion
    }
}
