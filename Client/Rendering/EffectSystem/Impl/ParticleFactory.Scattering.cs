﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleFactory
    {
        public static Particle CreateScatteringParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
               WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
               ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox;
            particle.Color = color;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));
            particle.Position += system.Random.GetUnitVector3() * system.Random.NextFloat(0.2f, 1.2f) * 1.3f;
            particle.BoundingBox *= system.Random.NextFloat(0.4f, 0.9f);

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var accMove = system.CreateComponent<CommonComponent.AccelMoving>();
            accMove.Initialize(system.Random.GetUnitVector3(), Second.Zero, lTime.TimeRemain, 0.035f, 3f);
            particle.AddComponent(accMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, particle.Color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, new Color4(particle.Color.R, particle.Color.G, particle.Color.B, 0), InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(particle.Color, new Color4(particle.Color.R, particle.Color.G, particle.Color.B, 0), (Second)0.5f, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);
            */

            //var sizeFade = system.CreateParticleComponent<ParticleComponent.LinearSizeChanger>();
            //sizeFade.Initialize(particle.BoundingBox * 0.6f, particle.BoundingBox*0.8f, Second.Zero, (Second)0.3f);
            //particle.AddComponent(sizeFade);

            var sizeFade2 = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
            sizeFade2.Initialize(particle.BoundingBox, particle.BoundingBox * 0.6f, (Second)0.3f, lTime.TimeRemain);
            particle.AddComponent(sizeFade2);

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
        public static Particle CreateScatteringParticle_Horizontal(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
               WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
               ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox;
            particle.Color = color;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));
            //particle.Position += system.Random.GetUnitVector3() * system.Random.NextFloat(0.2f, 1.2f) * 3.3f;
            particle.BoundingBox *= system.Random.NextFloat(0.4f, 0.9f);

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var accMove = system.CreateComponent<CommonComponent.AccelMoving>();
            Vector2 moveTarget = system.Random.GetUnitVector2();

            accMove.Initialize(new Vector3(moveTarget.X, 0f, moveTarget.Y), Second.Zero, lTime.TimeRemain, 0.02f, system.Random.NextFloat(6f, 8f));
            
            particle.AddComponent(accMove);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame(Second.Zero, new Color4(particle.Color.R, particle.Color.G, particle.Color.B, 0), InterpolationMethod.Linear);
            colorkf.AddKeyFrame((Second)0.1f, particle.Color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, new Color4(particle.Color.R, particle.Color.G, particle.Color.B, 0), InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            /*
            var colorFade1 = system.CreateParticleComponent<ParticleComponent.ColorFade>();
            colorFade1.Initialize(particle.Color, new Color4(particle.Color.R, particle.Color.G, particle.Color.B, 0), (Second)0.5f, lTime.TimeRemain, ParticleComponent.ColorFadeMethod.LinearRGB);
            particle.AddComponent(colorFade1);
            */

            //var sizeFade = system.CreateParticleComponent<ParticleComponent.LinearSizeChanger>();
            //sizeFade.Initialize(particle.BoundingBox * 0.6f, particle.BoundingBox*0.8f, Second.Zero, (Second)0.3f);
            //particle.AddComponent(sizeFade);

            var sizeFade2 = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
            sizeFade2.Initialize(particle.BoundingBox, particle.BoundingBox * 0.6f, (Second)0.3f, lTime.TimeRemain);
            particle.AddComponent(sizeFade2);

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
    }
}
