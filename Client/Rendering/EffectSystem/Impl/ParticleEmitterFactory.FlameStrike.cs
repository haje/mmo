﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region FlameStrike
        public static ParticleEmitter[] CreateFlameStrikeEmitter(EffectSystem system, Vector3 position)
        {
            ParticleEmitter[] emitters = new ParticleEmitter[6];

            for (Radian theta = Radian.Zero; theta < Radian.Pi * 2; theta += Radian.Pi / 3)
            {
                ParticleEmitter emitter = system.CreateParticleEmitter();
                emitter.Position = position + 3 * Vector3.UnitX * Radian.Cos(theta) + 3 * Vector3.UnitZ * Radian.Sin(theta);
                /*
                var delay = system.CreateComponent<EmitterComponent.DelayedStart>();
                delay.Initialize((Second)1f);
                emitter.AddComponent(delay);
                */
                var lifeTime = system.CreateComponent<CommonComponent.LifeTime>();
                lifeTime.Initialize((Second)0.25f);
                emitter.AddComponent(lifeTime);

                var linMove = system.CreateComponent<CommonComponent.LinearMoving>();
                linMove.Initialize(position, position + Vector3.UnitY * 10, Second.Zero, lifeTime.TimeRemain);
                emitter.AddComponent(linMove);

                var cirMove = system.CreateComponent<CommonComponent.ConicMoving>();
                cirMove.Initialize(position, Vector3.UnitY, emitter.Position, (Radian)20, Second.Zero, Second.Infinity);
                emitter.AddComponent(cirMove);

                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 120, Second.Zero,
                    (s, e, t) =>
                    {
                        Particle[] ps = new Particle[1];
                        Color4 startColor = Color4.DarkRed;
                        startColor.A = 0.5f;
                        Color4 endColor = Color4.Yellow;
                        endColor.A = 0.5f;

                        for (int iter = 0; iter < 1; iter++)
                        {
                            var ecbArray = e.GetComponent<CommonComponent.LifeTime>();

                            Particle p = ParticleFactory.CreateStandParticle(s, e, t,
                                null, startColor, endColor,
                                Vector2.One, system.Random.GetRandomParticleType(ParticleCategory.Glow),
                                0.3f + 5 * ecbArray[0].TimeRemain);
                            ps[iter] = p;
                        }

                        return ps;
                    },
                    system, emitter);
                emitter.AddComponent(pGen);
            }

            return emitters;
        }
        #endregion
    }
}