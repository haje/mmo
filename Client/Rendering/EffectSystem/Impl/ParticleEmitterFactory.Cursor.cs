﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleEmitterFactory
    {
        #region CursorParticle
        public static ParticleEmitter[] CreateCursorParticle(EffectSystem system, Vector3 position)
        {
            ParticleEmitter emitter = system.CreateParticleEmitter();
            {//GlowingOrb

                emitter.Position = position;
                // emitter.IsActivated = true;


                var pGen = system.CreateComponent<EmitterComponent.ParticleGenerator>();
                pGen.Initialize((Second)1f / 12, Second.Zero,
(EmitterComponent.Generator)                    ((s, e, t) =>
                    {
                        Particle[] ps = new Particle[2];
                        //Color4 sColor = Color4.OrangeRed;
                        //Color4 sColor = Color4.Magenta;
                        Color4 sColor = new Color4(230, 15, 230, 255);
                        sColor.A = 1.0f;
                        for (int iter = 0; iter < 2; iter++)
                        {
                            //Particle p = ParticleFactory.CreateStandParticle(s, e, t, null, sColor, eColor, Vector2.One, ParticleType.Ring1, (Second)1f);
                            Particle p = ParticleFactory.CreateLoopingParticle(s, e, t, null, sColor, Vector2.One, ParticleType.Circle2, (Second)1.0f);
                            var emitterTracer = system.CreateComponent<CommonComponent.EmitterTracer>();
                            emitterTracer.Initialize(emitter);
                            p.AddComponent(emitterTracer);

                            ps[iter] = p;
                        }
                        return ps;
                    }),
                    system,
                    emitter);
                emitter.AddComponent(pGen);
            }

            return new ParticleEmitter[] { emitter };
        }
        #endregion
    }
}