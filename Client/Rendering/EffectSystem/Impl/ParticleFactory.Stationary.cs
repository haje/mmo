﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.EffectSystem.Impl
{
    public static partial class ParticleFactory
    {
        public static Particle CreateStationaryParticle(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
            WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
            ParticleType particleType, Second lifeTime)
        {
            var particle = system.CreateAdditiveParticle();
            particle.BoundingBox = boundingBox;
            particle.Color = color;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            var sizeFade = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
            sizeFade.Initialize(Vector2.Zero, particle.BoundingBox * 0.5f, Second.Zero, (Second)0.1f);
            particle.AddComponent(sizeFade);

            var sizeFade2 = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
            sizeFade2.Initialize(particle.BoundingBox * 0.5f, particle.BoundingBox * 1.0f, (Second)0.1f, (Second)0.4f);
            particle.AddComponent(sizeFade2);

            var sizeFade3 = system.CreateComponent<ParticleComponent.LinearSizeChanger>();
            sizeFade3.Initialize(particle.BoundingBox * 1.0f, particle.BoundingBox * 1.1f, (Second)0.4f, (Second)1);
            particle.AddComponent(sizeFade3);

            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame((Second)0.7f, color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame((Second)0.9f, new Color4(color.R, color.G, color.B, color.A * 0.8f), InterpolationMethod.Linear);
            colorkf.AddKeyFrame(lTime.TimeRemain, Color4.Transparent, InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
        public static Particle CreateStationaryParticle_Fading(EffectSystem system, ParticleEmitter emitter, Second timeRemain,
            WorldSpaceEntity target, Color4 color, Vector2 boundingBox,
            ParticleType particleType, Second lifeTime,TextureBlendTypes bType,Second FadeTime)
        {
            Particle particle;
            if(bType == TextureBlendTypes.Attenuative)
            {
                particle = system.CreateAttenuativeParticle();
            }
            else {
                particle = system.CreateAdditiveParticle();
            }
            
            particle.BoundingBox = boundingBox;
            particle.BoundingBox *= system.Random.NextFloat(0.3f, 1.2f);
            particle.Color = color;
            particle.Rotation = system.Random.NextRadian();
            particle.UVMin = ParticleUVHelper.GetUVMin(particleType);
            particle.UVMax = ParticleUVHelper.GetUVMax(particleType);
            particle.Position = emitter.GetInterpolatedPosition((Second)(-timeRemain));
            particle.Position += system.Random.GetUnitVector3() * system.Random.NextFloat(0.6f, 0.8f);

            var lTime = system.CreateComponent<CommonComponent.LifeTime>();
            lTime.Initialize(lifeTime);
            particle.AddComponent(lTime);

            
            var colorkf = new KeyFrameContainer<Color4>();
            colorkf.AddKeyFrame((Second)0.05f, color, InterpolationMethod.Linear);
            colorkf.AddKeyFrame(FadeTime, Color4.Transparent, InterpolationMethod.Linear);

            var colorFade = system.CreateComponent<ParticleComponent.ColorOverTime>();
            colorFade.Initialize(colorkf, false);
            particle.AddComponent(colorFade);
            

            if (target != null)
            {
                var objTracer = system.CreateComponent<CommonComponent.ObjectTracer>();
                objTracer.Initialize(target);
                particle.AddComponent(objTracer);
            }

            return particle;
        }
    }
}
