﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;
using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class EffectLight : ComponentContainer
    {
        public DeferredPointLight Light
        {
            get;
            protected set;
        }
        public override Vector3 Position
        {
            get
            {
                return Light.Position;
            }
            set
            {
                if (Light.Position != value)
                    Light.Position = value;
            }
        }

        #region .ctor
        public EffectLight()
            : base()
        {
            Light = new DeferredPointLight(Vector3.Zero, Vector3.One, 1);
        }
        #endregion
    }
}
