﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;

namespace HAJE.MMO.Client.Rendering.EffectSystem.EmitterComponent
{
    public abstract class EmitterComponentBase : ComponentBase
    {
        protected ParticleEmitter emitter
        {
            get
            {
                return target as ParticleEmitter;
            }
            set
            {
                if (target != value)
                    target = value;
            }
        }
    }
}
