﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;

namespace HAJE.MMO.Client.Rendering.EffectSystem.EmitterComponent
{
    public delegate Particle[] Generator(EffectSystem system, ParticleEmitter emitter, Second timeRemain);

    public class ParticleGenerator : EmitterComponentBase
    {
        public Second TimeGap
        {
            get;
            private set;
        }
        public Second TimeError
        {
            get;
            private set;
        }

        private Second timeRemain;
        private Generator gen;
        private EffectSystem system;

        public void Initialize(Second timeGap, Second timeError, Generator gen, EffectSystem system, ParticleEmitter emitter)
        {
            Initialize(timeGap, timeError, gen, system, emitter, Second.Zero, Second.Infinity);
        }
        public void Initialize(Second timeGap, Second timeError, Generator gen, EffectSystem system, ParticleEmitter emitter, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;

            this.gen = gen;
            this.TimeGap = timeGap;
            this.TimeError = timeError;
            this.emitter = emitter;
            this.system = system;

            this.timeRemain = timeGap + system.Random.NextSecond(-TimeError, TimeError);
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            timeRemain -= deltaTime;
            while (timeRemain <= 0)
            {
                Particle[] ps = gen(system, emitter, (Second) (-timeRemain));
                foreach (Particle p in ps)
                    foreach (ComponentBase pcb in p.Components)
                        pcb.Update((Second) (-timeRemain));

                timeRemain += TimeGap + system.Random.NextSecond(-TimeError, TimeError);
            }
        }
    }
}
