﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class EffectSystem : IDisposable
    {
        public ReadOnlyCollection<Effect> EffectList
        {
            get
            {
                return effectPool.AliveList;
            }
        }
        private ObjectPool<Effect> effectPool;
        private ObjectPool<Particle> additiveParticlePool;
        private ObjectPool<Particle> attenuativeParticlePool;
        private ObjectPool<ParticleEmitter> emitterPool;
        private ObjectPool<EffectMeshDescriptor> meshPool;
        private ObjectPool<EffectLight> lightPool;

        private ObjectPool<ComponentBase> componentPool;

        public Texture Texture
        {
            get;
            private set;
        }
        public readonly Random Random;

        public delegate void GetActiveParticles(ReadOnlyCollection<Particle> visibleParticles);
        public event GetActiveParticles AdditiveParticlesUpdated;
        public event GetActiveParticles AttenuativeParticlesUpdated;

        public EffectSystem()
        {
            try
            {
                Texture = Texture.CreateFromFile(@"Resource\ParticleSystem\tex.png");
            }
            catch (ArgumentException e)
            {
                GameSystem.Log.Write(e);
                throw e;
            }

            effectPool = new ObjectPool<Effect>();
            additiveParticlePool = new ObjectPool<Particle>();
            attenuativeParticlePool = new ObjectPool<Particle>();
            emitterPool = new ObjectPool<ParticleEmitter>();
            meshPool = new ObjectPool<EffectMeshDescriptor>();
            lightPool = new ObjectPool<EffectLight>();

            componentPool = new ObjectPool<ComponentBase>();

            Random = new Random();
            ctorDictionary = new Dictionary<Type, ConstructorInfo>();
        }

        #region Create / Find / Remove Effect
        private Dictionary<Type, ConstructorInfo> ctorDictionary;
        public T CreateEffect<T>(int id, Vector3 initPos) where T : Effect
        {
            Effect eff = effectPool.Create<T>();
            eff.Reset(id, initPos);
            return eff as T;
        }
        public Effect FindEffect(int id)
        {
            foreach (Effect eff in effectPool.AliveList)
                if (eff.Id == id)
                    return eff;

            return null;
        }
        public void RemoveEffect(int id)
        {
            FindEffect(id).Kill();
        }
        #endregion
        #region Create Particle / Emitter / Mesh / Light / Component
        public Particle CreateAdditiveParticle()
        {
            return additiveParticlePool.Create();
        }
        public Particle CreateAttenuativeParticle()
        {
            return attenuativeParticlePool.Create();
        }
        public ParticleEmitter CreateParticleEmitter()
        {
            return emitterPool.Create();
        }
        public EffectMeshDescriptor CreateEffectMeshDescriptor(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, Mesh mesh, TextureBlendTypes blendType = TextureBlendTypes.Opaque, Color4? colorTint = null)
        {
            EffectMeshDescriptor emd = meshPool.Create();
            emd.Initialize(position, lookingAtDirection, upVector, mesh, blendType, colorTint);
            return emd;            
        }
        public EffectLight CreateLight()
        {
            return lightPool.Create();
        }
        public EffectLight CreateLight(Vector3 position, Vector3 intensity, float radius)
        {
            EffectLight efl = lightPool.Create();
            efl.Position = position;
            efl.Light.Intensity = intensity;
            efl.Light.Radius = radius;

            return efl;
        }
        public T CreateComponent<T>() where T : ComponentBase, new()
        {
            ComponentBase cb = componentPool.Create<T>();
            return cb as T;
        }
        #endregion
        #region Update
        private void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
        public void Update(Second deltaTime)
        {
            effectPool.Update();
            additiveParticlePool.Update();
            attenuativeParticlePool.Update();
            emitterPool.Update();
            componentPool.Update();

            if (AdditiveParticlesUpdated != null)
                AdditiveParticlesUpdated(additiveParticlePool.AliveList);
            if (AttenuativeParticlesUpdated != null)
                AttenuativeParticlesUpdated(attenuativeParticlePool.AliveList);

            foreach (Effect effect in EffectList)
                effect.Update(deltaTime);
            foreach (Particle particle in additiveParticlePool.AliveList)
                particle.Update(deltaTime);
            foreach (Particle particle in attenuativeParticlePool.AliveList)
                particle.Update(deltaTime);
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            if (Texture != null)
                Texture.Dispose();
        }
        #endregion
    }
}