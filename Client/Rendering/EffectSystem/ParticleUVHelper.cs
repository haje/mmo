﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public enum ParticleCategory
    {
        Circle,
        Ring,
        Glow,
        Ray,
        Side,
        Spiral,
        Fire,
        Fragment,
    }
    public enum ParticleType
    {
        Circle1,
        Circle2,

        Ring1,
        Ring2,
        Ring3,
        Ring4,
        Ring5,
        Ring6,

        Glow1,
        Glow2,

        Ray1_1,
        Ray1_2,
        Ray1_3,
        Ray2_1,
        Ray2_2,
        Ray2_3,

        Side,

        Spiral,
        Star1,
        Fire1,
        Fire2,
        Fire3,
        Fragment1,
        Fire4,
        Fragment2,
    }
    public static class ParticleUVHelper
    {
        private static List<ParticleUVData> ParticleDataList;
        private const int DivisionCount = 8;
        static ParticleUVHelper()
        {
            /*ParticleDataList = new List<ParticleUVData>();

            ParticleDataList.Add(new ParticleUVData(ParticleType.Circle1, new Vector2(0, 1), new Vector2(1, 0)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Circle2, new Vector2(1, 1), new Vector2(2, 0)));

            ParticleDataList.Add(new ParticleUVData(ParticleType.Ring1, new Vector2(2, 1), new Vector2(3, 0)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ring2, new Vector2(3, 1), new Vector2(4, 0)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ring3, new Vector2(4, 1), new Vector2(5, 0)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ring4, new Vector2(5, 1), new Vector2(6, 0)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ring5, new Vector2(6, 1), new Vector2(7, 0)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ring6, new Vector2(7, 1), new Vector2(8, 0)));

            ParticleDataList.Add(new ParticleUVData(ParticleType.Glow1, new Vector2(0, 2), new Vector2(1, 1)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Glow2, new Vector2(1, 2), new Vector2(2, 1)));

            ParticleDataList.Add(new ParticleUVData(ParticleType.Ray1_1, new Vector2(2, 1 + 1f / 3), new Vector2(3, 1 + 0f / 3)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ray1_2, new Vector2(2, 1 + 2f / 3), new Vector2(3, 1 + 1f / 3)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ray1_3, new Vector2(2, 1 + 3f / 3), new Vector2(3, 1 + 2f / 3)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ray2_1, new Vector2(3, 1 + 1f / 3), new Vector2(4, 1 + 0f / 3)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ray2_2, new Vector2(3, 1 + 2f / 3), new Vector2(4, 1 + 1f / 3)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Ray2_3, new Vector2(3, 1 + 3f / 3), new Vector2(4, 1 + 2f / 3)));

            ParticleDataList.Add(new ParticleUVData(ParticleType.Side, new Vector2(4, 2), new Vector2(5, 1)));
            ParticleDataList.Add(new ParticleUVData(ParticleType.Spiral, new Vector2(5, 2), new Vector2(6, 1)));

            XmlSerializer xs = new XmlSerializer(typeof(List<ParticleUVData>));
            using (StreamWriter sw = new StreamWriter("Resource/ParticleSystem/uv.xml"))
                xs.Serialize(sw, ParticleDataList);*/

            XmlSerializer xs = new XmlSerializer(typeof(List<ParticleUVData>));
            using (StreamReader sr = new StreamReader("Resource/ParticleSystem/uv.xml"))
                ParticleDataList = xs.Deserialize(sr) as List<ParticleUVData>;
        }

        public static ParticleType GetRandomParticleType(this Random rd, ParticleCategory cat)
        {
            int randomInt = rd.Next();
            switch (cat)
            {
                case ParticleCategory.Circle:
                    switch (randomInt % 2)
                    {
                        case 0: return ParticleType.Circle1;
                        case 1: return ParticleType.Circle2;
                    }
                    break;

                case ParticleCategory.Ring:
                    switch (randomInt % 6)
                    {
                        case 0: return ParticleType.Ring1;
                        case 1: return ParticleType.Ring2;
                        case 2: return ParticleType.Ring3;
                        case 3: return ParticleType.Ring4;
                        case 4: return ParticleType.Ring5;
                        case 5: return ParticleType.Ring6;
                    }
                    break;

                case ParticleCategory.Glow:
                    switch (randomInt % 2)
                    {
                        case 0: return ParticleType.Glow1;
                        case 1: return ParticleType.Glow2;
                    }
                    break;

                case ParticleCategory.Ray:
                    switch (randomInt % 6)
                    {
                        case 0: return ParticleType.Ray1_1;
                        case 1: return ParticleType.Ray1_2;
                        case 2: return ParticleType.Ray1_3;
                        case 3: return ParticleType.Ray2_1;
                        case 4: return ParticleType.Ray2_2;
                        case 5: return ParticleType.Ray2_3;
                    }
                    break;

                case ParticleCategory.Side:
                    return ParticleType.Side;

                case ParticleCategory.Spiral:
                    return ParticleType.Spiral;
                
            }
            throw new ArgumentException(String.Format("Wrong Particle Category; {0}", cat));
        }

        public static Vector2 GetUVMin(ParticleType type)
        {
            foreach (var tuple in ParticleDataList)
                if (tuple.Type == type)
                    return tuple.MinUV / DivisionCount;

            throw new Exception("Unknown particle type");
        }
        public static Vector2 GetUVMax(ParticleType type)
        {
            foreach (var tuple in ParticleDataList)
                if (tuple.Type == type)
                    return tuple.MaxUV / DivisionCount;

            throw new Exception("Unknown particle type");
        }
    }
}
