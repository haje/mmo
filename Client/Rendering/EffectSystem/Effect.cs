﻿using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public abstract class Effect : ComponentContainer
    {
        protected List<ParticleEmitter> emitterList;
        protected List<EffectLight> lightList;
        protected List<EffectMeshDescriptor> meshList;

        // Need Refactoring
        public IReadOnlyCollection<DeferredPointLight> LightList
        {
            get
            {
                List<DeferredPointLight> lightList = new List<DeferredPointLight>();
                foreach (EffectLight light in this.lightList)
                    lightList.Add(light.Light);

                return lightList.AsReadOnly();
            }
        }
        public IReadOnlyCollection<MeshDescriptor> MeshList
        {
            get
            {
                List<MeshDescriptor> mdList = new List<MeshDescriptor>();
                foreach (EffectMeshDescriptor emd in meshList)
                    mdList.Add(emd.MeshDescriptor);

                return mdList.AsReadOnly();
            }
        }

        public int Id
        {
            get;
            protected set;
        }

        #region .ctor
        public Effect()
            : base()
        {
            emitterList = new List<ParticleEmitter>();
            lightList = new List<EffectLight>();
            meshList = new List<EffectMeshDescriptor>();
        }
        public void Reset(int id, Vector3 initPos)
        {
            Reset();
            this.Id = id;
            this.position = initPos;
        }
        public override void Reset()
        {
            base.Reset();
            emitterList.Clear();
            lightList.Clear();
            meshList.Clear();
        }
        #endregion
        #region abstract
        public override void Update(Second deltaTime)
        {
            base.Update(deltaTime);

            emitterList.RemoveAll(_ => _.IsAlive == false);
            meshList.RemoveAll(_ => _.IsAlive == false);
            lightList.RemoveAll(_ => _.IsAlive == false);

            if (emitterList.Count == 0 && lightList.Count == 0 && meshList.Count == 0)
                Kill();

            elapsedTime += deltaTime;
            if (positionKeyFrame != null)
            {
                Position += positionKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime);
            }
            else if (target != null)
            {
                if (elapsedTime < duration)
                {
                    this.prevPosition = Position;
                    Position += velocity * deltaTime;
                    Position += target.Position - targetPrevPosition;

                    this.targetPrevPosition = target.Position;
                }
            }

            foreach (ParticleEmitter pe in emitterList)
                pe.Update(deltaTime);
            foreach (EffectMeshDescriptor emd in meshList)
                emd.Update(deltaTime);
            foreach (EffectLight efl in lightList)
                efl.Update(deltaTime);
        }
        #endregion
        
        #region Kill / Dispose
        public override void Kill()
        {
            base.Kill();

            foreach (ParticleEmitter em in emitterList)
                em.Kill();
            foreach (EffectMeshDescriptor emd in meshList)
                emd.Kill();
            foreach (EffectLight efl in lightList)
                efl.Kill();
        }
        #endregion
        #region Add
        public void AddEmitterList(IEnumerable<ParticleEmitter> emitterList)
        {
            this.emitterList.AddRange(emitterList);
        }
        public void AddLightList(IEnumerable<EffectLight> lightList)
        {
            this.lightList.AddRange(lightList);
        }
        public void AddMeshList(IEnumerable<EffectMeshDescriptor> meshList)
        {
            this.meshList.AddRange(meshList);
        }
        #endregion
        #region Moving
        protected Second elapsedTime;
        private KeyFrameContainer<Vector3> positionKeyFrame;
        public void Move(KeyFrameContainer<Vector3> positionKeyFrame)
        {
            this.target = null;

            this.elapsedTime = Second.Zero;
            this.positionKeyFrame = positionKeyFrame;
        }
        
        public override Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                Vector3 delta = value - position;

                foreach (ParticleEmitter emitter in emitterList)
                    emitter.Position += delta;
                foreach (EffectLight light in lightList)
                    light.Light.Position += delta;
                foreach (EffectMeshDescriptor mesh in meshList)
                    mesh.MeshDescriptor.Position += delta;

                position = value;
            }
        }
        private Vector3 prevPosition;
        private Vector3 targetPrevPosition;
        private Second duration;

        private Vector3 velocity;
        private WorldSpaceEntity target;
        public void Move(WorldSpaceEntity target, Second duration)
        {
            this.elapsedTime = Second.Zero;
            this.positionKeyFrame = null;

            this.duration = duration;
            this.velocity = (target.Position - this.Position) / duration;
            this.targetPrevPosition = target.Position;
            this.target = target;
        }
        #endregion
    }
}