﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.ParticleComponent
{
    [Obsolete("No More KeyFrame-Based Components. Using this component may cause bugs.", false)]
    public class RotationOverTime : ParticleComponentBase
    {
        private KeyFrameContainer<Radian> rotationKeyFrame;
        private Second elapsedTime;
        public bool IsRelative;

        public void Initialize(KeyFrameContainer<Radian> rotationKeyFrame, bool isRelative)
        {
            StartTime = Second.Zero;
            EndTime = Second.Infinity;

            this.rotationKeyFrame = rotationKeyFrame;
            this.elapsedTime = Second.Zero;

            this.IsRelative = isRelative;
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            elapsedTime += deltaTime;

            if (IsRelative == true)
            {
                particle.Rotation += rotationKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime) * (float)elapsedTime;
            }
            else
            {
                particle.Rotation = rotationKeyFrame.GetValue(elapsedTime) * (float)elapsedTime;
            }
        }
    }
}
