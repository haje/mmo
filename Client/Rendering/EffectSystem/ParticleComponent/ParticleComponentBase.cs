﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;

namespace HAJE.MMO.Client.Rendering.EffectSystem.ParticleComponent
{
    public abstract class ParticleComponentBase : ComponentBase
    {
        protected Particle particle
        {
            get
            {
                return target as Particle;
            }
            set
            {
                if (target != value)
                    target = value;
            }
        }
    }
}
