﻿using OpenTK.Graphics;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.ParticleComponent
{
    [Obsolete("No More KeyFrame-Based Components. Using this component may cause bugs.", false)]
    public class ColorOverTime : ParticleComponentBase
    {
        private KeyFrameContainer<Color4> colorKeyFrame;
        private Second elapsedTime;
        public bool IsRelative;

        public void Initialize(KeyFrameContainer<Color4> colorKeyFrame, bool isRelative)
        {
            StartTime = Second.Zero;
            EndTime = Second.Infinity;

            this.colorKeyFrame = colorKeyFrame;
            this.elapsedTime = Second.Zero;

            this.IsRelative = isRelative;
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            elapsedTime += deltaTime;

            if (IsRelative == true)
            {
                particle.Color.A += colorKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime).A;
                particle.Color.R += colorKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime).R;
                particle.Color.G += colorKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime).G;
                particle.Color.B += colorKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime).B;
            }
            else
            {
                particle.Color = colorKeyFrame.GetValue(elapsedTime);
            }
        }
    }
}
