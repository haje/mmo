﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.ParticleComponent
{
    [Obsolete("No More KeyFrame-Based Components. Using this component may cause bugs.", false)]
    public class SizeOverTime : ParticleComponentBase
    {
        private KeyFrameContainer<Vector2> sizeKeyFrame;
        private Second elapsedTime;
        public bool IsRelative;

        public void Initialize(KeyFrameContainer<Vector2> sizeKeyFrame, bool isRelative)
        {
            StartTime = Second.Zero;
            EndTime = Second.Infinity;

            this.sizeKeyFrame = sizeKeyFrame;
            this.elapsedTime = Second.Zero;

            this.IsRelative = isRelative;
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            elapsedTime += deltaTime;

            if (IsRelative == true)
            {
                particle.BoundingBox += sizeKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime) * elapsedTime;
            }
            else
            {
                particle.BoundingBox = sizeKeyFrame.GetValue(elapsedTime) * elapsedTime;
            }
        }
    }
}
