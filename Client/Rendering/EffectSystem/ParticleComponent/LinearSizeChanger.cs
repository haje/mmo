﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.ParticleComponent
{
    public class LinearSizeChanger : ParticleComponentBase
    {
        public Vector2 SizeVelocity
        {
            get;
            private set;
        }

        private Second elapsedTime;
        private Vector2 initSize;

        public void Initialize(Vector2 initialSize, Vector2 finalSize, Second startTime, Second endTime)
        {
            startTime = Second.Zero;
            endTime = Second.Infinity;

            this.SizeVelocity = (finalSize - initialSize) / (endTime - startTime);
            this.StartTime = startTime;
            this.EndTime = endTime;
            initSize = initialSize;
            elapsedTime = Second.Zero;
        }
        
        protected override void InnerUpdate(Second deltaTime)
        {
            particle.BoundingBox += SizeVelocity * deltaTime;
        }
    }
}
