﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;
using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class ComponentContainer : IPooledObject
    {
        #region Properties
        public bool IsAlive
        {
            get;
            protected set;
        }
        public Second ElapsedTime
        {
            get;
            protected set;
        }

        public Vector3 position;
        public virtual Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                if (position != value)
                    position = value;
            }
        }
        public List<ComponentBase> Components
        {
            get;
            protected set;
        }
        #endregion

        #region .ctor
        public ComponentContainer()
        {
            IsAlive = true;
            ElapsedTime = Second.Zero;
            Components = new List<ComponentBase>();
        }
        #endregion
        #region Component
        public void AddComponent(ComponentBase com)
        {
            com.SetTarget(this);
            Components.Add(com);
        }
        public T[] GetComponent<T>() where T : ComponentBase, new()
        {
            List<T> pcbList = new List<T>();
            foreach (ComponentBase pcb in Components)
                if (pcb.type == TypeHelper<T>.ToType())
                    pcbList.Add(pcb as T);

            return pcbList.ToArray();
        }
        #endregion

        #region Update
        public virtual void Update(Second deltaTime)
        {
            ElapsedTime += deltaTime;

            Components.RemoveAll(_ => _.IsAlive == false);
            foreach (ComponentBase c in Components)
                c.Update(deltaTime);
        }
        #endregion
        #region Reset
        public virtual void Reset()
        {
            IsAlive = true;
            Components.Clear();
        }
        #endregion
        #region Kill
        public virtual void Kill()
        {
            IsAlive = false;
            foreach (ComponentBase cb in Components)
                cb.Kill();
        }
        #endregion
    }
}
