﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;
using OpenTK;
using OpenTK.Graphics;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class EffectMeshDescriptor : ComponentContainer
    {
        public MeshDescriptor MeshDescriptor
        {
            get;
            private set;
        }
        public override Vector3 Position
        {
            get
            {
                return MeshDescriptor.Position;
            }
            set
            {
                if (MeshDescriptor.Position != value)
                    MeshDescriptor.Position = value;
            }
        }
        
        public Vector3 PrevPosition
        {
            get;
            private set;
        }
        public Second DeltaTime
        {
            get;
            private set;
        }

        #region .ctor
        public EffectMeshDescriptor()
        {
            IsAlive = true;
            Components = new List<ComponentBase>();
        }
        public void Initialize(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, Mesh mesh, TextureBlendTypes blendType, Color4? colorTint = null)
        {
            MeshDescriptor = MeshDescriptor.CreateMeshDescriptor(position, lookingAtDirection, upVector, mesh, blendType, colorTint);
        }
        #endregion
        #region update
        public override void Update(Second deltaTime)
        {
            base.Update(deltaTime);

            PrevPosition = MeshDescriptor.Position;
            DeltaTime = deltaTime;
        }
        #endregion
    }
}
