﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.LightComponent
{
    [Obsolete("No More KeyFrame-Based Components. Using this component may cause bugs.", false)]
    public class IntensityOverTime : LightComponentBase
    {
        private Second elapsedTime;
        private KeyFrameContainer<Vector3> intensityKeyFrame;

        public void Initialize(KeyFrameContainer<Vector3> intensityKeyFrame)
        {
            StartTime = Second.Zero;
            EndTime = Second.Infinity;

            this.elapsedTime = Second.Zero;
            this.intensityKeyFrame = intensityKeyFrame;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            light.Light.Intensity = intensityKeyFrame.GetValue(elapsedTime);
            elapsedTime += deltaTime;
        }
    }
}
