﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;

namespace HAJE.MMO.Client.Rendering.EffectSystem.LightComponent
{
    public abstract class LightComponentBase : ComponentBase
    {
        protected EffectLight light
        {
            get
            {
                return target as EffectLight;
            }
            set
            {
                if (target != value)
                    target = value;
            }
        }
    }
}
