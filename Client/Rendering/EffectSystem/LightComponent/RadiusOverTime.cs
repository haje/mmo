﻿using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.LightComponent
{
    [Obsolete("No More KeyFrame-Based Components. Using this component may cause bugs.", false)]
    public class RadiusOverTime : LightComponentBase
    {
        private Second elapsedTime;
        private KeyFrameContainer<float> radiusKeyFrame;

        public void Initialize(KeyFrameContainer<float> radiusKeyFrame)
        {
            StartTime = Second.Zero;
            EndTime = Second.Infinity;

            this.elapsedTime = Second.Zero;
            this.radiusKeyFrame = radiusKeyFrame;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            light.Light.Radius = radiusKeyFrame.GetValue(elapsedTime);
            elapsedTime += deltaTime;
        }
    }
}
