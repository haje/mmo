﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem.LightComponent
{
    public class LinearIntensityChanger : LightComponentBase
    {
        public Vector3 intensityVelocity
        {
            get;
            private set;
        }

        public void Initialize(Vector3 startIntentisy, Vector3 endIntensity, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.intensityVelocity = (endIntensity - startIntentisy) / (endTime - startTime);
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            light.Light.Intensity += intensityVelocity * deltaTime;
        }
    }
}
