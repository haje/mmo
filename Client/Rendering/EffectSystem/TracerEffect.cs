﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class TracerEffect : Effect
    {
        public TracerEffect()
            : base()
        {
        }

        private Vector3 prevCasterPosition;
        private WorldSpaceEntity caster;
        public void Initialize(WorldSpaceEntity target)
        {
            IsAlive = true;
            this.emitterList.Clear();
            this.lightList.Clear();
            this.meshList.Clear();

            this.caster = target;
            this.prevCasterPosition = target.Position;
        }

        public override void Update(Second deltaTime)
        {
            base.Update(deltaTime);
            
            if (caster.Position != prevCasterPosition)
            {
                Position += caster.Position - prevCasterPosition;
                prevCasterPosition = caster.Position;
            }
        }
    }
}
