﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;
using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class ParticleEmitter : ComponentContainer
    {
        public Vector3 PrevPosition
        {
            get;
            private set;
        }
        public Second DeltaTime
        {
            get;
            private set;
        }

        #region Update
        public override void Update(Second deltaTime)
        {
            ElapsedTime += deltaTime;
            PrevPosition = Position;
            DeltaTime = deltaTime;
            foreach (ComponentBase c in Components)
                c.Update(deltaTime);
        }
        public Vector3 GetInterpolatedPosition(Second deltaTime)
        {
            Vector3 pvel = (Position - PrevPosition) / DeltaTime;
            return Position + pvel * deltaTime;
        }
        #endregion
    }
}
