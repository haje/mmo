﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public class StandardEffect : Effect
    {
        public StandardEffect()
            : base()
        {
        }
        public void Initialize()
        {
            IsAlive = true;
            this.emitterList.Clear();
            this.lightList.Clear();
            this.meshList.Clear();
        }
    }
}
