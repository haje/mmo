﻿using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    public enum InterpolationMethod
    {
        Linear,
    }

    public class KeyFrame<T> : IComparable where T : struct
    {
        public Second Time;
        public T Value;
        public InterpolationMethod Method;

        public KeyFrame(Second time, T value, InterpolationMethod method)
        {
            Time = time;
            Value = value;
            Method = method;
        }
        
        public int CompareTo(object obj)
        {
            if ((obj as KeyFrame<T>).Time < this.Time)
                return 1;
            if ((obj as KeyFrame<T>).Time > this.Time)
                return -1;
            return 0;
        }

        public static bool operator <(KeyFrame<T> lhs, KeyFrame<T> rhs)
        {
            return lhs.Time < rhs.Time;
        }
        public static bool operator >(KeyFrame<T> lhs, KeyFrame<T> rhs)
        {
            return lhs.Time > rhs.Time;
        }
    }
}
