﻿using OpenTK;
using System;
using System.Xml.Serialization;

namespace HAJE.MMO.Client.Rendering.EffectSystem
{
    [Serializable]
    public class ParticleUVData
    {
        public ParticleUVData()
        {
        }
        public ParticleUVData(ParticleType type, Vector2 minUV, Vector2 maxUV)
        {
            Type = type;

            minX = minUV.X;
            minY = minUV.Y;

            maxX = maxUV.X;
            maxY = maxUV.Y;
        }

        [XmlAttribute]
        public ParticleType Type;
        [XmlAttribute]
        public float minX, minY, maxX, maxY;

        public Vector2 MinUV
        {
            get
            {
                return new Vector2(minX, minY);
            }
        }
        public Vector2 MaxUV
        {
            get
            {
                return new Vector2(maxX, maxY);
            }
        }
    }
}
