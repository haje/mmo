﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public class LinearMoving : ComponentBase
    {
        public Vector3 velocity
        {
            get;
            private set;
        }

        private Second elapsedTime;

        public void Initialize(Vector3 startPos, Vector3 endPos, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.velocity = (endPos - startPos) / (endTime - startTime);

            this.elapsedTime = Second.Zero;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            target.Position += velocity * deltaTime;
        }
    }
}
