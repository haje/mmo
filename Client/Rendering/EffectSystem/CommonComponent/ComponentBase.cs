﻿using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public enum ComponentState
    {
        Begin,
        Running,
        End,
    }
    public abstract class ComponentBase : IPooledObject
    {
        public Type type
        {
            get;
            protected set;
        }
        public Second ElapsedTime
        {
            get;
            protected set;
        }
        public Second StartTime
        {
            get;
            protected set;
        }
        public Second EndTime
        {
            get;
            protected set;
        }
        public ComponentState ComponentState
        {
            get
            {
                if (IsAlive == false)
                    return ComponentState.End;
                else
                {
                    if (ElapsedTime < StartTime)
                        return ComponentState.Begin;
                    else if (ElapsedTime > EndTime)
                        return ComponentState.End;
                    else
                        return ComponentState.Running;
                }
            }
        }
        public bool IsAlive
        {
            get;
            protected set;
        }
        protected ComponentContainer target;

        public virtual void Reset()
        {
            IsAlive = true;
            ElapsedTime = Second.Zero;
        }
        public virtual void Kill()
        {
            IsAlive = false;
        }

        public virtual void SetTarget(ComponentContainer target)
        {
            this.target = target;
        }

        public void Update(Second deltaTime)
        {
            ElapsedTime += deltaTime;
            Second updateTime = Second.Zero;

            if (ElapsedTime - deltaTime < EndTime)
                updateTime = (Second)Math.Min(deltaTime, Math.Max(0, ElapsedTime - StartTime));
            else
                updateTime = (Second)Math.Min(deltaTime, Math.Max(0, EndTime - ElapsedTime));

            InnerUpdate(updateTime);
        }
        protected abstract void InnerUpdate(Second deltaTime);
    }
}
