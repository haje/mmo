﻿namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public class LifeTime : ComponentBase
    {
        public Second TimeRemain
        {
            get;
            private set;
        }

        public void Initialize(Second aliveTime)
        {
            this.StartTime = Second.Zero;
            this.EndTime = aliveTime;

            this.TimeRemain = aliveTime;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            if (TimeRemain < 0)
                target.Kill();

            TimeRemain -= deltaTime;
        }
    }
}
