﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public class TerrainCompensator : ComponentBase
    {
        public TerrainMeshDescriptor TargetTerrain
        {
            get;
            private set;
        }
        public float CompensateWeight
        {
            get;
            private set;
        }

        private bool initialized;
        private Vector3 prevPosition;

        public void Initialize(TerrainMeshDescriptor terrain, float weight, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;

            this.TargetTerrain = terrain;
            this.CompensateWeight = weight;

            this.initialized = false;
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            if (initialized == false)
                prevPosition = target.Position;

            float prev = target.Position.Y;

            Vector3 pos = target.Position;
            pos.Y -= TargetTerrain.GetHeight(prevPosition.Xz) * CompensateWeight;
            pos.Y += TargetTerrain.GetHeight(target.Position.Xz) * CompensateWeight;

            prevPosition = target.Position;
            target.Position = pos;
        }
    }
}
