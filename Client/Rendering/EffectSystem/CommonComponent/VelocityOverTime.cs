﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    [Obsolete("No More KeyFrame-Based Components. Using this component may cause bugs.", false)]
    public class VelocityOverTime : ComponentBase
    {
        private KeyFrameContainer<Vector3> velocityKeyFrame;
        private Second elapsedTime;
        public bool IsRelative;

        public void Initialize(KeyFrameContainer<Vector3> velocityKeyFrame, bool isRelative)
        {
            StartTime = Second.Zero;
            EndTime = Second.Infinity;

            this.velocityKeyFrame = velocityKeyFrame;
            this.elapsedTime = Second.Zero;

            this.IsRelative = isRelative;
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            elapsedTime += deltaTime;

            if (IsRelative == true)
            {
                target.Position += velocityKeyFrame.GetDelta(elapsedTime - deltaTime, elapsedTime) * elapsedTime;
            }
            else
            {
                target.Position = velocityKeyFrame.GetValue(elapsedTime) * elapsedTime;
            }
        }
    }
}
