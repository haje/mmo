﻿namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public sealed class InfiniteLoop : ComponentBase
    {
        private ComponentBase targetComponent;

        public void Initialize(ComponentBase targetComponent)
        {
            this.StartTime = Second.Zero;
            this.EndTime = Second.Infinity;
            this.targetComponent = targetComponent;
        }
        public sealed override void SetTarget(ComponentContainer target)
        {
            base.SetTarget(target);
            targetComponent.SetTarget(target);
        }
        public sealed override void Reset()
        {
            base.Reset();
            if (targetComponent != null)
                targetComponent.Reset();
        }
        public sealed override void Kill()
        {
            base.Kill();
            if (targetComponent != null)
                targetComponent.Kill();
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            Second timeRemain = deltaTime;
            while (timeRemain > 0)
            {
                targetComponent.Update(timeRemain);
                timeRemain = Second.Zero;

                if (targetComponent.ComponentState == ComponentState.End)
                {
                    timeRemain = targetComponent.ElapsedTime - targetComponent.EndTime;
                    targetComponent.Reset();
                }
            }
        }
    }
}
