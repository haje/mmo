﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public class SphericalMoving : ComponentBase
    {
        public Vector3 Orthogonal
        {
            get;
            private set;
        }
        public float MaxDistance
        {
            get;
            private set;
        }
        public Radian RotationSpeed
        {
            get;
            private set;
        }

        public WorldSpaceEntity TargetEntity
        {
            get;
            private set;
        }
        public Vector3 Offset
        {
            get;
            private set;
        }
        private Vector3 _center;
        public Vector3 Center
        {
            get
            {
                if (TargetEntity != null)
                    return TargetEntity.Position + Offset;
                
                return _center;
            }
            private set
            {
                _center = value;
            }
        }

        private Vector3 prevCenter;
        private float distance;

        private Vector3 u;
        private Vector3 v;

        public void Initialize(Vector3 center, Vector3 orthogonal, Vector3 initPosition, Radian rotationSpeed, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;

            _center = center;
            Orthogonal = Vector3.Normalize(orthogonal);
            RotationSpeed = rotationSpeed;

            float dot = Vector3.Dot(Orthogonal, initPosition - Center);
            MaxDistance = (initPosition - Center).Length;

            Random r = new Random();
            do
            {
                u = Vector3.Cross(initPosition + r.GetUnitVector3(), Orthogonal);
                u.Normalize();
            } while (float.IsNaN(u.X) == true);
            v = Vector3.Cross(Orthogonal, u);

            distance = (float)Math.Sqrt(MaxDistance * MaxDistance - dot * dot);
            prevCenter = Center;
        }
        public void Initialize(WorldSpaceEntity target, Vector3 offset, Vector3 orthogonal, Vector3 initPosition, Radian rotationSpeed, Second startTime, Second endTime)
        {
            this.TargetEntity = target;
            this.Offset = offset;
            Initialize(Center, orthogonal, initPosition, rotationSpeed, startTime, endTime);
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            float dot = Vector3.Dot(Orthogonal, target.Position - Center);
            float cos = 0;
            float sin = 0;

            target.Position += Center - prevCenter;
            prevCenter = Center;
            if (distance > 0)
            {
                Vector3 rotationing = target.Position - Center - dot * Orthogonal;
                cos = Vector3.Dot(rotationing, u) / distance;
                sin = Vector3.Dot(rotationing, v) / distance;
                target.Position -= distance * (cos * u + sin * v);
            }

            distance = (float)Math.Sqrt(MaxDistance * MaxDistance - dot * dot);
            if (distance > 0)
            {
                Radian deltaTheta = RotationSpeed * (float)deltaTime;
                float dcos = Radian.Cos(deltaTheta);
                float dsin = Radian.Sin(deltaTheta);
                target.Position += distance * ((cos * dcos - sin * dsin) * u + (sin * dcos + cos * dsin) * v);
            }
        }
    }
}
