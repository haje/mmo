﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    [Obsolete("Use TracerEffect.", false)]
    public class ObjectTracer : ComponentBase
    {
        public WorldSpaceEntity TargetEntity
        {
            get;
            private set;
        }
        private Vector3 prevTargetPosition;

        public void Initialize(WorldSpaceEntity target)
        {
            this.TargetEntity = target;

            this.prevTargetPosition = target.Position;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            target.Position += TargetEntity.Position - prevTargetPosition;
            prevTargetPosition = TargetEntity.Position;
        }
    }
}
