﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public class ConicMoving : ComponentBase
    {
        public Vector3 Orthogonal
        {
            get;
            private set;
        }
        public Vector3 Center
        {
            get;
            private set;
        }
        public Radian RotationSpeed
        {
            get;
            private set;
        }
        
        private Vector3 u;
        private Vector3 v;

        public void Initialize(Vector3 center, Vector3 orthogonal, Vector3 initPosition, Radian rotationSpeed, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;

            Center = center;
            Orthogonal = Vector3.Normalize(orthogonal);
            RotationSpeed = rotationSpeed;

            u = Vector3.Cross(initPosition + Vector3.UnitX, orthogonal);
            u.Normalize();
            if (float.IsNaN(u.X) == true)
            {
                u = Vector3.Cross(initPosition + Vector3.UnitY, orthogonal);
                u.Normalize();
            }

            v = Vector3.Cross(orthogonal, u);
            v.Normalize();
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            float dot = Vector3.Dot(Orthogonal, target.Position);
            float Distance = (float)Math.Sqrt((target.Position - Center).LengthSquared - dot * dot);
            if (Distance == 0)
                return;

            Vector3 rotationing = target.Position - dot * Orthogonal;
            float cos = Vector3.Dot(rotationing, u) / Distance;
            float sin = Vector3.Dot(rotationing, v) / Distance;
            target.Position -= Distance * (cos * u + sin * v);

            Radian deltaTheta = RotationSpeed * (float)deltaTime;
            float dcos = Radian.Cos(deltaTheta);
            float dsin = Radian.Sin(deltaTheta);
            target.Position += Distance * ((cos * dcos - sin * dsin) * u + (sin * dcos + cos * dsin) * v);
        }
    }
}
