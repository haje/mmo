﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    [Obsolete("Use Object Tracer.", false)]
    public class EmitterTracer : ComponentBase
    {
        public ParticleEmitter TargetEntity
        {
            get;
            private set;
        }
        private Vector3 prevTargetPosition;

        public void Initialize(ParticleEmitter target)
        {
            this.TargetEntity = target;
            this.prevTargetPosition = target.Position;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            target.Position += TargetEntity.Position - prevTargetPosition;
            prevTargetPosition = TargetEntity.Position;
        }
    }
}
