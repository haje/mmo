﻿using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public sealed class Sequence : ComponentBase
    {
        private List<ComponentBase> componentList;
        private int componentIndex;

        public Sequence()
        {
            componentList = new List<ComponentBase>();
        }
        public void Initialize()
        {
            this.StartTime = Second.Zero;
            this.EndTime = Second.Zero;
            componentList.Clear();
            componentIndex = 0;
        }
        public void AddComponent(ComponentBase cb)
        {
            componentList.Add(cb);
            EndTime += cb.EndTime;
        }
        public sealed override void SetTarget(ComponentContainer target)
        {
            base.SetTarget(target);
            foreach (ComponentBase cb in componentList)
                cb.SetTarget(target);
        }
        public sealed override void Reset()
        {
            base.Reset();
            componentIndex = 0;
            foreach (ComponentBase cb in componentList)
                cb.Reset();
        }
        public sealed override void Kill()
        {
            base.Kill();
            foreach (ComponentBase cb in componentList)
                cb.Kill();
        }

        protected sealed override void InnerUpdate(Second deltaTime)
        {
            Second timeRemain = deltaTime;
            while (timeRemain > 0)
            {
                ComponentBase cb = componentList[componentIndex];
                cb.Update(timeRemain);
                timeRemain = Second.Zero;

                if (cb.ComponentState == ComponentState.End)
                {
                    timeRemain = cb.ElapsedTime - cb.EndTime;
                    componentIndex++;

                    if (componentIndex == componentList.Count)
                        break;
                }
            }
        }
    }
}
