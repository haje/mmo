﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public enum ExpansionType
    {
        Relative,
        Absolute,
    }
    public class ExpandFromPoint : ComponentBase
    {
        public Vector3 Center
        {
            get;
            private set;
        }
        public float ExpansionSpeed
        {
            get;
            private set;
        }
        public ExpansionType Expansion
        {
            get;
            private set;
        }

        public void Initialize(Vector3 center, float expansionSpeed, ExpansionType expansionType, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;

            Center = center;
            ExpansionSpeed = expansionSpeed;
            Expansion = expansionType;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            if (Expansion == ExpansionType.Absolute)
            {
                Vector3 delta = (target.Position - Center);
                delta *= (delta.Length + deltaTime * ExpansionSpeed) / delta.Length;
                target.Position = delta;
            }
            else
                target.Position += (target.Position - Center) * ExpansionSpeed * deltaTime + Center;
        }
    }
}
