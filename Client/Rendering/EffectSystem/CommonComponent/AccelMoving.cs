﻿using System;
using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent
{
    public class AccelMoving : ComponentBase
    {
        public Vector3 velocity
        {
            get;
            private set;
        }
        public Vector3 direction
        {
            get;
            private set;
        }
        public float accel
        {
            get;
            private set;
        }
        private Second elapsedTime;

        public void Initialize(Vector3 dir, Second startTime, Second endTime, float acc,float startVelocity)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.direction = Vector3.Normalize(dir);
            this.velocity = this.direction * startVelocity;
            this.accel = acc;
            this.elapsedTime = Second.Zero;
        }

        protected override void InnerUpdate(Second deltaTime)
        {
            target.Position += velocity * deltaTime;
            velocity = velocity * (1f + accel);
        }
    }
}
