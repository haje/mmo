﻿using System;
using OpenTK;

namespace HAJE.MMO.Client.Rendering.EffectSystem.MeshComponent
{
    public class MeshRotationOverTime : MeshComponentBase
    {
        private Vector3 initVector;
        private Vector3 initUp;

        public void Initialize(Vector3 Lookat,float initTime)
        {
            //meshDescriptor.MeshDescriptor.LookingAtDirection = Vector3.Transform(meshDescriptor.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, (Radian)(Degree)90));
            initVector = meshDescriptor.MeshDescriptor.LookingAtDirection;
            initUp = meshDescriptor.MeshDescriptor.UpVector;
            ElapsedTime = (Second)initTime;
            //meshDescriptor.MeshDescriptor.LookingAtDirection = new Vector3(0.2f, 0.8f, 0.4f);
            //target.MeshDescriptor.Rotate(ref a);
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            ElapsedTime += deltaTime;
            meshDescriptor.MeshDescriptor.LookingAtDirection = Vector3.Transform(initVector, Matrix4.CreateFromAxisAngle(Vector3.UnitY, ElapsedTime * 5.5f));
            
            meshDescriptor.MeshDescriptor.UpVector = Vector3.Transform(initUp, Matrix4.CreateFromAxisAngle(Vector3.UnitY, ElapsedTime * 5.5f));
            
            
            //meshDescriptor.MeshDescriptor.LookingAtDirection = Vector3.Transform(meshDescriptor.MeshDescriptor.LookingAtDirection, Matrix4.CreateFromAxisAngle(Vector3.UnitX, 0.1f));
            //  GameSystem.Log.Write("lookingat {0}", meshDescriptor.MeshDescriptor.LookingAtDirection.ToString());
        }
    }
}
