﻿using HAJE.MMO.Client.Rendering.EffectSystem.CommonComponent;

namespace HAJE.MMO.Client.Rendering.EffectSystem.MeshComponent
{
    public abstract class MeshComponentBase : ComponentBase
    {
        protected EffectMeshDescriptor meshDescriptor
        {
            get
            {
                return target as EffectMeshDescriptor;
            }
            set
            {
                if (target != value)
                    target = value;
            }
        }
    }
}
