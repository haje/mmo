﻿using OpenTK;
using OpenTK.Graphics;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.MeshComponent
{
    
    public class ScaleOverTime : MeshComponentBase
    {
        public Vector3 scaleVelocity
        {
            get;
            private set;
        }
        private Vector3 StartScale;
        private Vector3 EndScale;
        private Second elapsedTime;
        public void Initialize(Vector3 startScale, Vector3 endScale, Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.StartScale = startScale;
            this.EndScale = endScale;
            this.elapsedTime = Second.Zero;

            this.scaleVelocity = (endScale - startScale) / (endTime - startTime);
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            elapsedTime += deltaTime;

            if (elapsedTime <= (EndTime - StartTime))
            {
                meshDescriptor.MeshDescriptor.ScaleFactor = StartScale * (((EndTime - StartTime) - elapsedTime) / (EndTime - StartTime)) + EndScale * (elapsedTime / (EndTime - StartTime));
            }
            
        }
    }
    public class MeshColorOverTime : MeshComponentBase
    {
        public Vector4 colorVelocity
        {
            get;
            private set;
        }
        
        private Color4 sColor;
        private Color4 eColor;
        private Second elapsedTime;
        public void Initialize(Color4 startColor, Color4 endColor ,Second startTime, Second endTime)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.elapsedTime =Second.Zero;
            this.sColor = startColor;
            this.eColor = endColor;
            
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            elapsedTime += deltaTime;
            if (elapsedTime <= (EndTime - StartTime))
            {
                meshDescriptor.MeshDescriptor.ColorTint = new Color4(sColor.R * (((EndTime - StartTime) - elapsedTime) / (EndTime - StartTime)) + eColor.R * (elapsedTime / (EndTime - StartTime)), sColor.G * (((EndTime - StartTime) - elapsedTime) / (EndTime - StartTime)) + eColor.G * (elapsedTime / (EndTime - StartTime)), sColor.B * (((EndTime - StartTime) - elapsedTime) / (EndTime - StartTime)) + eColor.B * (elapsedTime / (EndTime - StartTime)), sColor.A * (((EndTime - StartTime) - elapsedTime) / (EndTime - StartTime)) + eColor.A * (elapsedTime / (EndTime - StartTime)));
            }
        }
    }

}
