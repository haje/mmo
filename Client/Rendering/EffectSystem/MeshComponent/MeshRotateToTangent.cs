﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering.EffectSystem.MeshComponent
{
    public class MeshRotateToTangent : MeshComponentBase
    {
        protected override void InnerUpdate(Second deltaTime)
        {
            var tangentVector = meshDescriptor.Position - meshDescriptor.PrevPosition;

            // Tangent Vector가 0일 때 일단 패스
            if (tangentVector == Vector3.Zero)
                return;

            try
            {
                meshDescriptor.MeshDescriptor.LookingAtDirection = tangentVector;
                try
                {
                    meshDescriptor.MeshDescriptor.UpVector = Vector3.Cross
                        (
                        meshDescriptor.MeshDescriptor.LookingAtDirection, 
                        meshDescriptor.MeshDescriptor.InitLookingAtDirection
                        );
                }
                catch (ArgumentOutOfRangeException)
                {
                    // if cross vector is zero, the looking at direction did not change
                    // do not change up vector (gimbal assumption)
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                // if tangent vector is zero, the mesh position did not change
                // do not change looking at direction
            }
        }
    }
}
