﻿using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.Rendering.Renderer
{
    public class ForwardRenderer : IMeshRenderable
    {
        public ForwardRenderer()
        {

        }

        public void Begin(Camera camera)
        {
            Debug.Assert(!isBegan);
            isBegan = true;

            this.Camera = camera;
            descriptors.Clear();
        }

        public void Render(MeshDescriptor mesh)
        {
            Debug.Assert(isBegan);

            descriptors.Add(mesh);
        }

        public void Render(IEnumerable<MeshDescriptor> meshes)
        {
            Debug.Assert(isBegan);

            descriptors.AddRange(meshes);
        }

        public void End()
        {
            Debug.Assert(isBegan);
            isBegan = false;

            GL.Enable(EnableCap.DepthTest);
            ShaderProvider.WorldSpaceForward.Use();
            foreach (var descriptor in descriptors)
            {
                ShaderProvider.WorldSpaceForward.SetUniform
                    (
                    Camera.ProjectionMatrix,
                    Camera.ViewMatrix,
                    descriptor.ModelMatrix
                    );
                descriptor.Draw();
            }
            descriptors.Clear();
            GL.Disable(EnableCap.DepthTest);
        }

        public Camera Camera { get; private set; }

        List<MeshDescriptor> descriptors = new List<MeshDescriptor>();
        bool isBegan = false;
    }
}
