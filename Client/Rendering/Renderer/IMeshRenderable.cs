﻿using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Renderer
{
    public interface IMeshRenderable
    {
        void Render(MeshDescriptor mesh);
        void Render(IEnumerable<MeshDescriptor> meshes);
    }
}
