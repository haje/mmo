﻿using HAJE.MMO.Client.Rendering.Buffer;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.Rendering.Renderer
{
    public class DeferredRenderer : IDisposable, IMeshRenderable
    {
        Framebuffer colorFBO;
        Framebuffer lightFBO;
        Framebuffer additiveFBO;
        Framebuffer attenuatorFBO;
        Framebuffer directionalShadowmapFBO;
        Framebuffer shadowFBO;

        Texture colorTexture;
        Texture normalTexture;
        Texture depthTexture;
        Texture internalDepthTexture;
        Texture lightTexture;
        Texture additiveParticleColorTexture;
        Texture attenuatorAccumulationColorTexture;
        Texture attenuatorRevealageTexture;
        Texture directionalShadowmapTexture;
        Texture directionalShadowmapDepthTexture;
        Texture shadowTexture;

        Texture perlinNoiseTexture;
        bool fogActivated;

        Mesh screenQuad;

        public DeferredRenderer(int width, int height)
        {
            screenQuad = Mesh.CreateScreenQuad();
            initTexture(width, height);
            initFBO();
            this.width = width;
            this.height = height;
        }

        #region private helpers

        private void initTexture(int width, int height)
        {
            if (colorTexture == null) colorTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.Rgba16f, PixelFormat.Rgba, PixelType.Float);
            if (normalTexture == null) normalTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.Rgba16f, PixelFormat.Rgba, PixelType.Float);
            if (depthTexture == null) depthTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.R32f, PixelFormat.Red, PixelType.UnsignedInt);
            if (internalDepthTexture == null) internalDepthTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.DepthComponent32, PixelFormat.DepthComponent, PixelType.UnsignedInt);
            if (lightTexture == null) lightTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.Rgba16f, PixelFormat.Rgba, PixelType.Float, (int)TextureMinFilter.Nearest, (int)TextureMagFilter.Nearest);
            if (additiveParticleColorTexture == null) additiveParticleColorTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.Rgba8, PixelFormat.Rgba, PixelType.UnsignedByte);
            if (attenuatorAccumulationColorTexture == null) attenuatorAccumulationColorTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.Rgba32f, PixelFormat.Rgba, PixelType.Float);
            if (attenuatorRevealageTexture == null) attenuatorRevealageTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.Rgba32f, PixelFormat.Rgba, PixelType.Float);
            if (directionalShadowmapTexture == null) directionalShadowmapTexture = Texture.CreateEmptyBuffer(shadowmapWidth, shadowmapHeight, PixelInternalFormat.R32f, PixelFormat.Red, PixelType.Float);
            if (directionalShadowmapDepthTexture == null) directionalShadowmapDepthTexture = Texture.CreateEmptyBuffer(shadowmapWidth, shadowmapHeight, PixelInternalFormat.DepthComponent32, PixelFormat.DepthComponent, PixelType.UnsignedInt);
            if (shadowTexture == null) shadowTexture = Texture.CreateEmptyBuffer(width, height, PixelInternalFormat.R8, PixelFormat.Red, PixelType.UnsignedByte);
            perlinNoiseTexture = Texture.CreateFromFile("Resource/Image/perlin_noise.png", TextureWrapMode.MirroredRepeat);
        }

        private void initFBO()
        {
            if (colorFBO == null) colorFBO = Framebuffer.Create(colorTexture, normalTexture, depthTexture, internalDepthTexture);
            if (lightFBO == null) lightFBO = Framebuffer.Create(lightTexture);
            if (additiveFBO == null) additiveFBO = Framebuffer.Create(additiveParticleColorTexture, internalDepthTexture);
            if (attenuatorFBO == null) attenuatorFBO = Framebuffer.Create(attenuatorAccumulationColorTexture, attenuatorRevealageTexture, internalDepthTexture);
            if (directionalShadowmapFBO == null) directionalShadowmapFBO = Framebuffer.Create(directionalShadowmapTexture, directionalShadowmapDepthTexture);
            if (shadowFBO == null) shadowFBO = Framebuffer.Create(shadowTexture);
        }

        #endregion

        public void Dispose()
        {
            if (colorFBO != null) colorFBO.Dispose(); colorFBO = null;
            if (lightFBO != null) lightFBO.Dispose(); lightFBO = null;
            if (additiveFBO != null) additiveFBO.Dispose(); additiveFBO = null;
            if (attenuatorFBO != null) attenuatorFBO.Dispose(); attenuatorFBO = null;
            if (directionalShadowmapFBO != null) directionalShadowmapFBO.Dispose(); directionalShadowmapFBO = null;
            if (shadowFBO != null) shadowFBO.Dispose(); shadowFBO = null;
            if (colorTexture != null) colorTexture.Dispose(); colorTexture = null;
            if (normalTexture != null) normalTexture.Dispose(); normalTexture = null;
            if (depthTexture != null) depthTexture.Dispose(); depthTexture = null;
            if (internalDepthTexture != null) internalDepthTexture.Dispose(); internalDepthTexture = null;
            if (lightTexture != null) lightTexture.Dispose(); lightTexture = null;
            if (additiveParticleColorTexture != null) additiveParticleColorTexture.Dispose(); additiveParticleColorTexture = null;
            if (attenuatorAccumulationColorTexture != null) attenuatorAccumulationColorTexture.Dispose(); attenuatorAccumulationColorTexture = null;
            if (attenuatorRevealageTexture != null) attenuatorRevealageTexture.Dispose(); attenuatorRevealageTexture = null;
            if (directionalShadowmapTexture != null) directionalShadowmapTexture.Dispose(); directionalShadowmapTexture = null;
            if (directionalShadowmapDepthTexture != null) directionalShadowmapDepthTexture.Dispose(); directionalShadowmapDepthTexture = null;
            if (shadowTexture != null) shadowTexture.Dispose(); shadowTexture = null;
        }

        public void Begin(Camera camera, ShadowCaster shadowCaster, bool fogActivated, bool crosshairActivated)
        {
            Debug.Assert(!isBegan);
            isBegan = true;

            this.Camera = camera;
            this.ShadowCaster = shadowCaster;
            this.toCharDist = Camera.zAlpha;
            this.fogActivated = fogActivated;
            this.isCrosshairActivated = crosshairActivated;
            Clear();
        }

        void Clear()
        {
            meshList.Clear();
            lightList.Clear();
            particleList.Clear();
            wplList.Clear();
        }

        [Conditional("DEBUG")]
        void CheckBegan()
        {
            Debug.Assert(isBegan, "Render를 호출하기 전에 먼저 Begin을 호출해야 합니다.");
        }

        public void Render(MeshDescriptor mesh)
        {
            CheckBegan();
            meshList.Add(mesh);
        }

        public void Render(IEnumerable<MeshDescriptor> meshes)
        {
            CheckBegan();
            meshList.AddRange(meshes);
        }

        public void Render(DeferredPointLight light)
        {
            CheckBegan();
            lightList.Add(light);
            if (light.AffectedByShadow)
                shadowcastingLightList.Add(light);
        }

        public void Render(IEnumerable<DeferredPointLight> lights)
        {
            CheckBegan();
            lightList.AddRange(lights);
            foreach (var light in lights)
            {
                if (light.AffectedByShadow)
                    shadowcastingLightList.Add(light);
            }
        }

        public void Render(ParticlePointCloudDescriptor particle)
        {
            CheckBegan();
            particleList.Add(particle);
        }

        public void Render(IEnumerable<ParticlePointCloudDescriptor> particles)
        {
            CheckBegan();
            particleList.AddRange(particles);
        }

        public void Render(WorldPositionLabelDescriptor wpl)
        {
            CheckBegan();
            wplList.Add(wpl);
        }

        public void Render(IEnumerable<WorldPositionLabelDescriptor> wpls)
        {
            CheckBegan();
            wplList.AddRange(wpls);
        }

        public void End()
        {
            Debug.Assert(isBegan, "End를 호출하기 전에 짝이 맞는 Begin이 있어야 합니다.");
            isBegan = false;

            GL.Enable(EnableCap.DepthTest);

            // Fill up shadow map here; only for opaque meshes
            if (ShadowCaster != null)
            {
                directionalShadowmapFBO.Bind();
                {
                    GL.Viewport(0, 0, shadowmapWidth, shadowmapHeight);
                    GL.ClearColor(0f, 0f, 0f, 0f);
                    GL.Clear(ClearBufferMask.DepthBufferBit);
                    GL.ClearColor(1, 1, 1, 1);
                    GL.Clear(ClearBufferMask.ColorBufferBit);

                    ShaderProvider.DepthOnly.Use();
                    foreach (var mesh in meshList)
                    {
                        if (mesh.BlendType == TextureBlendTypes.Opaque)
                        {
                            ShaderProvider.DepthOnly.SetUniform
                                (
                                ShadowCaster.ProjectionMatrix,
                                ShadowCaster.ViewMatrix,
                                mesh.ModelMatrix
                                );
                            mesh.Draw();
                        }
                    }
                    GL.Viewport(0, 0, width, height);
                }
            }

            foreach (var mesh in meshList)
            {
                if ((mesh.Position - Camera.Position).Length < toCharDist)
                    mesh.IsCloseEnough = true;
                else
                    mesh.IsCloseEnough = false;
            }

            colorFBO.Bind();
            {
                GL.ClearColor(0f, 0f, 0f, 0f);
                GL.Clear(ClearBufferMask.DepthBufferBit);
                GL.ClearColor(1, 1, 1, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                ShaderProvider.Unshaded.Use();
                foreach (var mesh in meshList)
                {
                    if (mesh.BlendType == TextureBlendTypes.Opaque && !mesh.IsCloseEnough)
                    {
                        Texture texture = mesh.Texture;
                        ShaderProvider.Unshaded.SetUniform
                            (
                            Camera.ProjectionMatrix,
                            Camera.ViewMatrix,
                            mesh.ModelMatrix,
                            mesh.ColorTint,
                            texture
                            );
                        mesh.Draw();
                    }
                }
            }
            colorFBO.Unbind();

            if (ShadowCaster != null)
            {
                shadowFBO.Bind();
                {
                    GL.Disable(EnableCap.DepthTest);
                    GL.ClearColor(1, 1, 1, 1);
                    GL.Clear(ClearBufferMask.ColorBufferBit);

                    ShaderProvider.Shadow.Use();
                    ShaderProvider.Shadow.SetUniform
                        (
                        Camera.ProjectionMatrix,
                        Camera.ViewMatrix,
                        depthTexture,
                        normalTexture,
                        ShadowCaster,
                        directionalShadowmapTexture
                        );
                    screenQuad.Draw();
                    GL.Enable(EnableCap.DepthTest);
                }
            }
            else
            {
                shadowFBO.Bind();
                {
                    GL.ClearColor(1, 1, 1, 1);
                    GL.Clear(ClearBufferMask.ColorBufferBit);
                }
            }

            lightFBO.Bind();
            {
                GL.ClearColor(0f, 0f, 0f, 0f);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                GL.Disable(EnableCap.DepthTest);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);

                ShaderProvider.Light.Use();
                foreach (var light in lightList)
                {
                    if (light.AffectedByShadow)
                    {
                        ShaderProvider.Light.SetUniform
                            (
                            Camera.ProjectionMatrix,
                            Camera.ViewMatrix,
                            depthTexture,
                            normalTexture,
                            light,
                            shadowTexture
                            );
                    }
                    else
                    {
                        ShaderProvider.Light.SetUniform
                            (
                            Camera.ProjectionMatrix,
                            Camera.ViewMatrix,
                            depthTexture,
                            normalTexture,
                            light
                            );
                    }
                    screenQuad.Draw();
                }

                GL.Enable(EnableCap.DepthTest);
                GL.Disable(EnableCap.Blend);
            }
            lightFBO.Unbind();

            additiveFBO.Bind();
            {
                GL.ClearColor(0f, 0f, 0f, 0f);
                GL.Clear(ClearBufferMask.ColorBufferBit);
                if (particleList.Count > 0 || wplList.Count > 0)
                {
                    GL.DepthMask(false);
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.One);
                    ShaderProvider.AdditiveMesh.Use();
                    foreach (var mesh in meshList)
                    {
                        if (mesh.BlendType == TextureBlendTypes.Additive)
                        {
                            ShaderProvider.AdditiveMesh.SetUniform
                                (
                                Camera.ProjectionMatrix,
                                Camera.ViewMatrix,
                                mesh.ModelMatrix,
                                mesh.ColorTint,
                                mesh.Texture
                                );
                            mesh.Draw();
                        }
                    }
                    ShaderProvider.AdditiveParticle.Use();
                    foreach (var particle in particleList)
                    {
                        if (particle.BlendType == TextureBlendTypes.Additive)
                        {
                            ShaderProvider.AdditiveParticle.SetUniform
                                (
                                Camera.ProjectionMatrix,
                                Camera.ViewMatrix,
                                particle.TexAtlas
                                );
                            particle.Draw();
                        }
                    }
                    foreach (var wplDesc in wplList)
                    {
                        if (wplDesc.BlendType == TextureBlendTypes.Additive)
                        {
                            ShaderProvider.AdditiveParticle.SetUniform
                                (
                                Camera.ProjectionMatrix,
                                Camera.ViewMatrix,
                                wplDesc.Texture
                                );
                            wplDesc.Draw();
                        }
                    }
                    GL.Disable(EnableCap.Blend);
                    GL.DepthMask(true);
                }
            }
            additiveFBO.Unbind();

            attenuatorFBO.Bind();
            {
                GL.ClearColor(0f, 0f, 0f, 1f);
                GL.Clear(ClearBufferMask.ColorBufferBit);
                if (particleList.Count > 0 || wplList.Count > 0)
                {
                    GL.DepthMask(false);
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFuncSeparate(BlendingFactorSrc.One, BlendingFactorDest.One,
                        BlendingFactorSrc.Zero, BlendingFactorDest.OneMinusSrcAlpha);

                    ShaderProvider.AttenuativeMesh.Use();
                    foreach (var mesh in meshList)
                    {
                        if (mesh.BlendType == TextureBlendTypes.Attenuative ||
                            (mesh.BlendType == TextureBlendTypes.Opaque && mesh.IsCloseEnough))
                        {
                            ShaderProvider.AttenuativeMesh.SetUniform
                                (
                                Camera.ProjectionMatrix,
                                Camera.ViewMatrix,
                                mesh.ModelMatrix,
                                mesh.ColorTint,
                                mesh.Texture,
                                this.toCharDist,
                                GamePlayConstant.SightRange,
                                GamePlayConstant.CharacterSyncRange
                                );
                            mesh.Draw();
                        }
                    }

                    ShaderProvider.AttenuativeParticle.Use();
                    foreach (var particle in particleList)
                    {
                        if (particle.BlendType == TextureBlendTypes.Attenuative)
                        {
                            ShaderProvider.AttenuativeParticle.SetUniform
                                (
                                Camera.ProjectionMatrix,
                                Camera.ViewMatrix,
                                particle.TexAtlas,
                                GamePlayConstant.SightRange,
                                GamePlayConstant.CharacterSyncRange
                                );
                            particle.Draw();
                        }
                    }
                    foreach (var wplDesc in wplList)
                    {
                        if (wplDesc.BlendType == TextureBlendTypes.Attenuative)
                        {
                            var texture = wplDesc.Texture;
                            ShaderProvider.AttenuativeParticle.Use();
                            ShaderProvider.AttenuativeParticle.SetUniform
                                (
                                Camera.ProjectionMatrix,
                                Camera.ViewMatrix,
                                texture,
                                GamePlayConstant.SightRange,
                                GamePlayConstant.CharacterSyncRange
                                );
                            wplDesc.Draw();
                        }
                    }
                    GL.Disable(EnableCap.Blend);
                    GL.DepthMask(true);
                }
            }
            attenuatorFBO.Unbind();

            ShaderProvider.Composite.Use();
            GL.Disable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            ShaderProvider.Composite.SetUniform(colorTexture, lightTexture, width, height, isCrosshairActivated, fogActivated, new Color4(0.65f, 0.74f, 0.77f, 1f), GamePlayConstant.SightRange, GamePlayConstant.CharacterSyncRange, Camera, depthTexture, perlinNoiseTexture);
            screenQuad.Draw();

            ShaderProvider.AttenuativeComposite.Use();
            GL.BlendFunc(BlendingFactorSrc.OneMinusSrcAlpha, BlendingFactorDest.SrcAlpha);
            ShaderProvider.AttenuativeComposite.SetUniform(attenuatorAccumulationColorTexture, attenuatorRevealageTexture);
            screenQuad.Draw();

            ShaderProvider.WorldSpaceTexture.Use();
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.One);
            ShaderProvider.WorldSpaceTexture.SetUniform(Matrix4.Identity, Matrix4.Identity, Matrix4.Identity, Color4.White, additiveParticleColorTexture);
            screenQuad.Draw();
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.DepthTest);

            Clear();
        }

        public Camera Camera { get; private set; }
        public ShadowCaster ShadowCaster { get; private set; }

        List<MeshDescriptor> meshList = new List<MeshDescriptor>();
        List<DeferredPointLight> lightList = new List<DeferredPointLight>();
        List<DeferredPointLight> shadowcastingLightList = new List<DeferredPointLight>();
        List<ParticlePointCloudDescriptor> particleList = new List<ParticlePointCloudDescriptor>();
        List<WorldPositionLabelDescriptor> wplList = new List<WorldPositionLabelDescriptor>();
        float toCharDist;
        bool isBegan = false;
        bool isCrosshairActivated = true;
        int width;
        int height;
        int shadowmapWidth = 4096;
        int shadowmapHeight = 4096;
    }
}
