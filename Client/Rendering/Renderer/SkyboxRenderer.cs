﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace HAJE.MMO.Client.Rendering.Renderer
{
    public class SkyboxRenderer : IDisposable
    {
        CubemapTexture cubemapTexture;
        MeshDescriptor skybox;
        bool isBegan = false;
        public Camera Camera { get; private set; }

        public SkyboxRenderer(CubemapTexture skymap)
        {
            skybox = MeshDescriptor.CreateMeshDescriptor(Vector3.Zero, Vector3.UnitY, Vector3.UnitX, Mesh.CreateSkycube());
            skybox.ScaleFactor = Vector3.One * 100.0f;
            cubemapTexture = skymap;
        }

        public void Dispose()
        {
            if (cubemapTexture != null) cubemapTexture.Dispose(); cubemapTexture = null;
        }

        public void Begin(Camera camera)
        {
            Debug.Assert(!isBegan);
            isBegan = true;

            this.Camera = camera;
        }

        [Conditional("DEBUG")]
        void CheckBegan()
        {
            Debug.Assert(isBegan, "Render를 호출하기 전에 먼저 Begin을 호출해야 합니다.");
        }

        public void Render()
        {
            skybox.Position = Camera.Position;
            CheckBegan();
        }

        public void End()
        {
            Debug.Assert(isBegan, "End를 호출하기 전에 짝이 맞는 Begin이 있어야 합니다.");
            isBegan = false;

            GL.Disable(EnableCap.DepthTest);
            ShaderProvider.Cubemap.Use();
            GL.Enable(EnableCap.TextureCubeMapSeamless);
            ShaderProvider.Cubemap.SetUniform(Camera.ProjectionMatrix, Camera.ViewMatrix, skybox.ModelMatrix, cubemapTexture);
            skybox.Draw();
            GL.Enable(EnableCap.DepthTest);
        }
    }
}
