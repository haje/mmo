﻿using OpenTK;
using System;
using System.Drawing;
using HAJE.MMO.Terrain;

namespace HAJE.MMO.Client.Rendering
{
    public class TerrainMeshDescriptor : MeshDescriptor
    {
        protected Heightmap heightmap;

        public Heightmap Heightmap
        {
            get
            {
                return heightmap;
            }
        }

        protected TerrainMeshDescriptor(Vector3 position, Heightmap heightmap)
            : base(position, Vector3.UnitZ, Vector3.UnitY, Mesh.FromHeightmap(heightmap.HeightmapRawData))
        {
            this.heightmap = heightmap;
            PickingTarget = new Picking.TerrainPickingTarget(this);
        }

        public static TerrainMeshDescriptor FromIndex(int index, Vector3 position)
        {
            using (Bitmap input = new Bitmap(TerrainPreprocessor.Path(index)))
            {
                Heightmap heightmap = TerrainPreprocessor.Process(input, index);
                float[,] floatArray = heightmap.HeightmapRawData;
                var crevasse = FieldDescriptionFile.ReadCrevasseFromFile(index);
                for (int x = 0; x < floatArray.GetLength(0); x++)
                {
                    for (int y = 0; y < floatArray.GetLength(1); y++)
                    {
                        foreach (var c in crevasse)
                        {
                            if (c.Contains(new Vector2(y, x)))
                            {
                                floatArray[x, y] = -9999;
                                break;
                            }
                        }
                    }
                }
                heightmap.HeightmapRawData = floatArray;
                return TerrainMeshDescriptor.FromHeightmap(heightmap, position);
            }
        }

        public static TerrainMeshDescriptor FromIndex(int index)
        {
            using (Bitmap input = new Bitmap(TerrainPreprocessor.Path(index)))
            {
                Heightmap heightmap = TerrainPreprocessor.Process(input, index);
                float[,] floatArray = heightmap.HeightmapRawData;
                var crevasse = FieldDescriptionFile.ReadCrevasseFromFile(index);
                for (int x = 0; x < floatArray.GetLength(0); x++)
                {
                    for (int y = 0; y < floatArray.GetLength(1); y++)
                    {
                        foreach (var c in crevasse)
                        {
                            if (c.Contains(new Vector2(y, x)))
                            {
                                floatArray[x, y] = -9999;
                                break;
                            }
                        }
                    }
                }
                heightmap.HeightmapRawData = floatArray;
                return TerrainMeshDescriptor.FromHeightmap(heightmap, Vector3.Zero);
            }
        }

        public static TerrainMeshDescriptor FromHeightmap(Heightmap map, Vector3 postion)
        {
            return new TerrainMeshDescriptor(postion, map);
        }

        public float GetHeight(Vector2 xzPos)
        {
            return heightmap.GetHeight(xzPos);
        }

        public readonly Picking.Pickable PickingTarget;
    }
}
