﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Buffer
{
    public class VertexBuffer : IDisposable
    {
        private static Dictionary<BufferUsageHint, VertexBufferType> hintToType =
        new Dictionary<BufferUsageHint, VertexBufferType>()
        {
            { BufferUsageHint.StaticDraw,  VertexBufferType.Static },
            { BufferUsageHint.StaticCopy,  VertexBufferType.Static },
            { BufferUsageHint.StaticRead,  VertexBufferType.Static },
            { BufferUsageHint.DynamicDraw, VertexBufferType.Dynamic },
            { BufferUsageHint.DynamicCopy, VertexBufferType.Dynamic },
            { BufferUsageHint.DynamicRead, VertexBufferType.Dynamic },
            { BufferUsageHint.StreamDraw,  VertexBufferType.Dynamic },
            { BufferUsageHint.StreamCopy,  VertexBufferType.Dynamic },
            { BufferUsageHint.StreamRead,  VertexBufferType.Dynamic }
        };

        private VertexBuffer(int vbo, int count, BufferUsageHint hint)
        {
            this.vbo = vbo;
            this.count = count;
            this.hint = hint;
        }

        public static VertexBuffer Create<T>(T[] vertices, BufferUsageHint bufferUsageHint) where T:struct
        {
            var count = vertices.Length;
            var stride = BlittableValueType.StrideOf(vertices);
            var vbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), vertices, bufferUsageHint);
            
            CheckUploadState(count, stride);
            VertexType.VertexTypes.SetupFunctionOf(vertices)();
            GLHelper.CheckGLError();

            return new VertexBuffer(vbo, count, bufferUsageHint);
        }

        public void UpdateVertex<T>(T[] vertices) where T : struct
        {
            Debug.Assert(hintToType[hint] == VertexBufferType.Dynamic);
            Debug.Assert(vertices.Length <= count);

            var stride = BlittableValueType.StrideOf(vertices);

            Bind();
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * stride), vertices, hint);
        }

        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
        }

        public void Dispose()
        {
            if (vbo != 0) GL.DeleteBuffer(vbo); vbo = 0;
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        #region privates

        [Conditional("DEBUG")]
        static void CheckUploadState(int count, int stride)
        {
            int size;
            GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * stride != size)
                throw new ApplicationException("Vertex data not uploaded correctly");
        }
        
        int vbo;
        int count;
        BufferUsageHint hint;

        #endregion
    }
}
