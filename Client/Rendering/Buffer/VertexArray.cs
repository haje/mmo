﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;

namespace HAJE.MMO.Client.Rendering.Buffer
{
    public class VertexArray : IDisposable
    {
        private VertexArray(int vao, VertexBuffer vertBuffer, ElementBuffer elemBuffer)
        {
            this.vao = vao;
            this.vertBuffer = vertBuffer;
            this.elemBuffer = elemBuffer;
        }

        public static VertexArray CreateArray<T>(T[] vertices, BufferUsageHint bufferUsageHint, int[] elements = null) where T : struct
        {
            var vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            var vertBuffer = VertexBuffer.Create(vertices, bufferUsageHint);
            var elemBuffer = ElementBuffer.Create(elements);

            return new VertexArray(vao, vertBuffer, elemBuffer);
        }

        public void Bind()
        {
            GL.BindVertexArray(vao);
        }

        public void UpdateVertex<T>(T[] vertices) where T : struct
        {
            Bind();
            vertBuffer.UpdateVertex(vertices);
        }

        public void Dispose()
        {
            if (vao != 0) GL.DeleteVertexArray(vao); vao = 0;
            if (vertBuffer != null) vertBuffer.Dispose(); vertBuffer = null;
            if (elemBuffer != null) elemBuffer.Dispose(); elemBuffer = null;
        }

        public int VertCount
        {
            get
            {
                return vertBuffer.Count;
            }
        }

        public int ElemCount
        {
            get
            {
                return elemBuffer.Count;
            }
        }

        #region privates

        int vao;
        VertexBuffer vertBuffer;
        ElementBuffer elemBuffer;

        #endregion
    }
}
