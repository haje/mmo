﻿using System;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Buffer
{
    public class Framebuffer : IDisposable
    {
        int fbo;
        private int cTexCount;

        private Framebuffer(int fbo, int cTexCount)
        {
            this.fbo = fbo;
            this.cTexCount = cTexCount;
        }

        public static Framebuffer Create(params Texture[] buffers)
        {
            int cTexCount = 0;
            var fbo = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);

            foreach (Texture tex in buffers)
            {
                switch(tex.Type)
                {
                    case Texture.Types.ColorType:
                        GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + cTexCount, TextureTarget.Texture2D, tex.TextureHandle, 0);
                        cTexCount++;
                        break;
                    case Texture.Types.DepthType:
                        GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, tex.TextureHandle, 0);
                        break;
                }
            }

            DrawBuffersEnum[] drawBuffers = new DrawBuffersEnum[cTexCount];
            for (int drawBufferIter = 0; drawBufferIter < cTexCount; ++drawBufferIter)
            {
                drawBuffers[drawBufferIter] = (DrawBuffersEnum)FramebufferAttachment.ColorAttachment0 + drawBufferIter;
            }
            GL.DrawBuffers(cTexCount, drawBuffers);

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            GLHelper.CheckGLError();

            return new Framebuffer(fbo, cTexCount);
        }

        public void Bind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbo);
        }

        public void Dispose()
        {
            if (fbo != 0) GL.DeleteFramebuffer(fbo); fbo = 0;
        }

        public void Unbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public int ColorAttachmentCount
        {
            get
            {
                return cTexCount;
            }
        }
    }
}
