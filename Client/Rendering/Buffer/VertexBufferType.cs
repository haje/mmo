﻿namespace HAJE.MMO.Client.Rendering.Buffer
{
    public enum VertexBufferType
    {
        Static,
        Dynamic
    }
}
