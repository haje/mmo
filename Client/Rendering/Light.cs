﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public abstract class Light : WorldSpaceEntity
    {
        Vector3 intensity;

        public Light(Vector3 position, Vector3 intensity)
            : base(position, 
                  // 왠지는 모르지만 원래 아래처럼 되어있길래 임시로 고침
                  //Vector3.Zero - position
                  Vector3.One, Vector3.UnitY)
        {
            this.intensity = intensity;
        }

        public Vector3 Intensity
        {
            get
            {
                return intensity;
            }
            set
            {
                intensity = value;
            }
        }
    }
}
