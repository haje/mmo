﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;

namespace HAJE.MMO.Client.Rendering
{
    public class WorldPositionLabel : Drawable
    {
        protected UI.Component.BMLabel label;

        public UI.Component.BMLabel Label
        {
            get
            {
                return label;
            }
        }
        
        public Vector3 Position;

        public Color4 Color;

        public Texture Texture
        {
            get
            {
                return label.TextTexture;
            }
        }
        
        private int size;

        Rendering.VertexType.VertexPositionRotationUVminmaxBBColor[] vertices = new Rendering.VertexType.VertexPositionRotationUVminmaxBBColor[1];

        public WorldPositionLabel(String text, String fontPath, int size, Vector3 position, Color4? color = null)
        {
            this.label = new UI.Component.BMLabel(text, fontPath);
            this.size = size;
            Position = position;

            vertArray = Buffer.VertexArray.CreateArray(vertices, BufferUsageHint.DynamicDraw, new int[1]);

            Color = color ?? Color4.Black;
        }

        public override void Draw()
        {
            vertices[0].Position = Position;
            vertices[0].Rotation = Radian.Zero;
            vertices[0].BoundingBox = label.TextTexture.Size / size / 4;
            vertices[0].Color = Color;
            vertices[0].UVMin = new Vector2(0, 1);
            vertices[0].UVMax = new Vector2(1, 0);

            vertArray.UpdateVertex(vertices);

            vertArray.Bind();
            GLHelper.DrawPoints(1);
        }
    }
}
