﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering
{
    public class DoodadManager
    {
        public static List<MeshDescriptor> DoodadMeshDescriptorList(int index, TerrainMeshDescriptor terrain, MeshProvider meshProvider)
        {
            List<MeshDescriptor> meshDescriptorList = new List<MeshDescriptor>();
            IReadOnlyList<FieldDoodad> doodadList = FieldDescriptionFile.ReadDoodadFromFile(index);
            DoodadMeshInformationProvider doodadMeshInfoProvider = DoodadMeshInformationProvider.Instance;
            Dictionary<Tuple<int, int, int>, MeshDescriptor> descriptorDic = new Dictionary<Tuple<int, int, int>, MeshDescriptor>();
            
            foreach (FieldDoodad doodad in doodadList)
            {
                var doodadAgent = CreateDoodadMeshDescriptor(doodad, terrain, meshProvider, doodadMeshInfoProvider);
                if(!descriptorDic.ContainsKey(new Tuple<int,int,int>((int)doodadAgent.Position.X,(int)doodadAgent.Position.Y,(int)doodadAgent.Position.Z)))
                {
                    descriptorDic.Add(new Tuple<int, int, int>((int)doodadAgent.Position.X, (int)doodadAgent.Position.Y, (int)doodadAgent.Position.Z),doodadAgent);
                    meshDescriptorList.Add(doodadAgent);
                }
                
            }

            return meshDescriptorList;
        }

        public static MeshDescriptor CreateDoodadMeshDescriptor(FieldDoodad doodad, TerrainMeshDescriptor terrain, MeshProvider meshProvider, DoodadMeshInformationProvider doodadMeshInfoProvider)
        {
            var doodadmesh = meshProvider.GetMesh(doodadMeshInfoProvider[doodad.Type].Path);
            MeshDescriptor doodadAgent = Rendering.MeshDescriptor.CreateMeshDescriptor
                (new Vector3(doodad.Position.X, terrain.GetHeight(doodad.Position) + doodadMeshInfoProvider[doodad.Type].Anchor.Y * doodad.Height, doodad.Position.Y),
                Vector3.UnitX, Vector3.UnitY, doodadmesh);
            doodadAgent.LookingAtDirection = new Vector3(Radian.Cos(doodad.Rotation), 0, Radian.Sin(doodad.Rotation));
            doodadAgent.ScaleFactor =
                new Vector3(doodad.Radius / doodadMeshInfoProvider[doodad.Type].Radius, doodad.Height / doodadMeshInfoProvider[doodad.Type].Height, doodad.Radius / doodadMeshInfoProvider[doodad.Type].Radius);
            return doodadAgent;
        }
    }
}
