﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.VertexType
{
    public struct VertexPositionNormalTintUV
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color4 Tint;
        public Vector2 UV;

        public static void Setup()
        {
            int stride = BlittableValueType<VertexPositionNormalTintUV>.Stride;
            int offset = 0;

            int posAttrLoc = 0;
            int posSize = 3;
            GL.EnableVertexAttribArray(posAttrLoc);
            GL.VertexAttribPointer(posAttrLoc, posSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += posSize * sizeof(float);

            int normalAttrLoc = 1;
            int normalSize = 3;
            GL.EnableVertexAttribArray(normalAttrLoc);
            GL.VertexAttribPointer(normalAttrLoc, normalSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += normalSize * sizeof(float);

            int tintAttrLoc = 2;
            int tintSize = 4;
            GL.EnableVertexAttribArray(tintAttrLoc);
            GL.VertexAttribPointer(tintAttrLoc, tintSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += tintSize * sizeof(float);

            int uvAttrLoc = 3;
            int uvSize = 2;
            GL.EnableVertexAttribArray(uvAttrLoc);
            GL.VertexAttribPointer(uvAttrLoc, uvSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += uvSize * sizeof(float);

            GLHelper.CheckGLError();
        }
    }
}
