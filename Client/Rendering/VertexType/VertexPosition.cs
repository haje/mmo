﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.VertexType
{
    public struct VertexPosition
    {
        public Vector3 Position;

        public static void Setup()
        {
            int stride = BlittableValueType<VertexPosition>.Stride;
            int offset = 0;

            int posAttrLoc = 0;
            int posSize = 3;
            GL.EnableVertexAttribArray(posAttrLoc);
            GL.VertexAttribPointer(posAttrLoc, posSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += posSize * sizeof(float);
        }
    }
}
