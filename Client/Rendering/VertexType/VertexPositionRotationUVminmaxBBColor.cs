﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.VertexType
{
    public struct VertexPositionRotationUVminmaxBBColor
    {
        public Vector3 Position;
        public Color4 Color;
        public float Rotation;
        public Vector2 UVMin;
        public Vector2 UVMax;
        public Vector2 BoundingBox;

        public static void Setup()
        {
            int stride = BlittableValueType<VertexPositionRotationUVminmaxBBColor>.Stride;
            int offset = 0;

            int posAttrLoc = 0;
            int posSize = 3;
            GL.EnableVertexAttribArray(posAttrLoc);
            GL.VertexAttribPointer(posAttrLoc, posSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += posSize * sizeof(float);

            int colorAttrLoc = 2;
            int colorSize = 4;
            GL.EnableVertexAttribArray(colorAttrLoc);
            GL.VertexAttribPointer(colorAttrLoc, colorSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += colorSize * sizeof(float);

            int rotAttrLoc = 5;
            int rotSize = 1;
            GL.EnableVertexAttribArray(rotAttrLoc);
            GL.VertexAttribPointer(rotAttrLoc, rotSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += rotSize * sizeof(float);

            int uvMinAttrLoc = 6;
            int uvMinSize = 2;
            GL.EnableVertexAttribArray(uvMinAttrLoc);
            GL.VertexAttribPointer(uvMinAttrLoc, uvMinSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += uvMinSize * sizeof(float);

            int uvMaxAttrLoc = 7;
            int uvMaxSize = 2;
            GL.EnableVertexAttribArray(uvMaxAttrLoc);
            GL.VertexAttribPointer(uvMaxAttrLoc, uvMaxSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += uvMaxSize * sizeof(float);

            int bbAttrLoc = 8;
            int bbSize = 2;
            GL.EnableVertexAttribArray(bbAttrLoc);
            GL.VertexAttribPointer(bbAttrLoc, bbSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += bbSize * sizeof(float);

            GLHelper.CheckGLError();
        }
    }
}
