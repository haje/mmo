﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.VertexType
{
    public struct VertexWorldPositionNormalColor
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Color4 Color;

        public static void Setup()
        {
            int stride = BlittableValueType<VertexWorldPositionNormalColor>.Stride;
            int offset = 0;

            int posAttrLoc = 0;
            int posSize = 3;
            GL.EnableVertexAttribArray(posAttrLoc);
            GL.VertexAttribPointer(posAttrLoc, posSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += posSize * sizeof(float);

            int normalAttrLoc = 1;
            int normalSize = 3;
            GL.EnableVertexAttribArray(normalAttrLoc);
            GL.VertexAttribPointer(normalAttrLoc, normalSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += normalSize * sizeof(float);

            int colorAttrLoc = 2;
            int colorSize = 4;
            GL.EnableVertexAttribArray(colorAttrLoc);
            GL.VertexAttribPointer(colorAttrLoc, colorSize, VertexAttribPointerType.Float, false, stride, offset);
            offset += colorSize * sizeof(float);

            GLHelper.CheckGLError();
        }
    }
}
