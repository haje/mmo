﻿using OpenTK;
using System;

namespace HAJE.MMO.Client.Rendering
{
    public enum TextureBlendTypes
    {
        Opaque,
        Additive,
        Attenuative,
    }

    public class VisualObjectDescriptor<T> : WorldSpaceEntity, IDisposable where T : Drawable
    {
        public TextureBlendTypes BlendType
        {
            get;
            protected set;
        }

        protected T drawable;

        protected VisualObjectDescriptor(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, T drawable, TextureBlendTypes blendType)
            :base(position, lookingAtDirection, upVector)
        {
            this.drawable = drawable;
            this.BlendType = blendType;
        }

        public void Dispose()
        {
            if (drawable != null) drawable.Dispose(); drawable = null;
        }

        public void Draw()
        {
            drawable.Draw();
        }
    }
}
