﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public class WorldSpaceEntity : ISpaceEntity
    {
        Vector3 position;
        Vector3 lookingAtDirection;
        readonly Vector3 initLookingAtDirection;
        Vector3 upVector;
        readonly Vector3 initUpVector;
        Vector3 scaleFactor;

        protected WorldSpaceEntity(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, Vector3? scaleFactor = null)
        {
            Position = position;
            LookingAtDirection = lookingAtDirection;
            UpVector = upVector;
            ScaleFactor = scaleFactor ?? Vector3.One;
            initLookingAtDirection = LookingAtDirection;
            initUpVector = UpVector;
        }

        /// <summary>
        /// Creates new world-space entity.
        /// </summary>
        /// <param name="position">Position of this entity in world space</param>
        /// <param name="lookingAtDirection">Vector of direction that this entity is facing to</param>
        /// <returns></returns>
        public static WorldSpaceEntity CreateWorldSpaceEntity(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, Vector3? scaleFactor = null)
        {
            return new WorldSpaceEntity(position, lookingAtDirection, upVector, scaleFactor);
        }

        /// <summary>
        /// Transforms the input vector to basis of front-right-worldY.
        /// Right vector is calculated by crossing viewDirection and worldY, 
        /// and Front vector is calculated by crossing worldY and right.
        /// </summary>
        /// <param name="inVector">Vector to be transformed</param>
        /// <returns></returns>
        protected Vector3 ToFrontRightWorldYBasis(Vector3 inVector)
        {
            Vector3 viewDir = lookingAtDirection.Normalized();
            Vector3 worldY = new Vector3(0, 1, 0);
            Vector3 rightDir = Vector3.Cross(viewDir, worldY).Normalized();
            Vector3 frontDir = Vector3.Cross(worldY, rightDir).Normalized();
            return inVector.X * frontDir + inVector.Y * rightDir + inVector.Z * worldY;
        }

        /// <summary>
        /// Rotates the lookingAtDirection in the vector space with basis of front-right-worldY.
        /// The axis is Roll-Pitch-Yaw respectively.
        /// </summary>
        /// <param name="rotMat">Rotation matrix to be applied</param>
        public void Rotate(ref Matrix4 rotMat)
        {
            Vector3 rotAxis;
            float rotAngle;
            rotMat.ExtractRotation().ToAxisAngle(out rotAxis, out rotAngle);
            rotAxis = ToFrontRightWorldYBasis(rotAxis).Normalized();
            Matrix4 newRotMat = Matrix4.CreateFromAxisAngle(rotAxis, rotAngle);

            LookingAtDirection = Vector3.Transform(lookingAtDirection, newRotMat);
            UpVector = Vector3.Transform(upVector, newRotMat);
        }

        /// <summary>
        /// Rotates the up and lookingAtDirection in the local vector space.
        /// </summary>
        /// <param name="rotMat">Rotation matrix to be applied</param>
        public void RotateLocal(ref Matrix4 rotMat)
        {
            UpVector = Vector3.Transform(UpVector, rotMat);
            LookingAtDirection = Vector3.Transform(LookingAtDirection, rotMat);
        }

        /// <summary>
        /// Translates the position in the vector space with basis of front-right-worldY.
        /// </summary>
        /// <param name="delta">Displacement vector in Front-Right-WorldY direction.</param>
        public void Translate(Vector3 delta)
        {
            Vector3 newDelta = ToFrontRightWorldYBasis(delta);
            Position += newDelta;
        }

        public Vector3 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public Vector3 LookingAtDirection
        {
            get
            {
                return lookingAtDirection;
            }
            set
            {
                if (float.IsNaN(value.Normalized().Length))
                    throw new System.ArgumentOutOfRangeException(@"LookingAtDirection은 영벡터가 될 수 없습니다.");
                lookingAtDirection = value.Normalized();
            }
        }

        public Vector3 InitLookingAtDirection
        {
            get
            {
                return initLookingAtDirection;
            }
        }

        public Vector3 InitUpVector
        {
            get
            {
                return initUpVector;
            }
        }

        public Vector3 UpVector
        {
            get
            {
                return upVector;
            }
            set
            {
                if (float.IsNaN(value.Normalized().Length))
                    throw new System.ArgumentOutOfRangeException(@"UpVector는 영벡터가 될 수 없습니다.");
                upVector = value.Normalized();
            }
        }

        public Vector3 ScaleFactor
        {
            get
            {
                return scaleFactor;
            }
            set
            {
                scaleFactor = value;
            }
        }

        public virtual Matrix4 ModelMatrix
        {
            get
            {
                Matrix4 scaleMat, rotMat, translMat;
                scaleMat = Matrix4.CreateScale(ScaleFactor);
                Vector3 rotAxis = Vector3.Cross(InitLookingAtDirection, LookingAtDirection).Normalized();
                if (rotAxis.Length < 1e-10 || float.IsNaN(rotAxis.Length))
                {
                    rotMat = Matrix4.Identity;
                }
                else
                {
                    rotMat = Matrix4.CreateFromAxisAngle(rotAxis, Vector3.CalculateAngle(InitLookingAtDirection, LookingAtDirection));
                }
                
                var transfInitUp = Vector3.Transform(initUpVector, rotMat);
                Vector3 upRotAxis = Vector3.Cross(transfInitUp, UpVector).Normalized();
                if (upRotAxis.Length < 1e-10 || float.IsNaN(upRotAxis.Length))
                {
                }
                else
                {
                    rotMat = Matrix4.Mult(rotMat, Matrix4.CreateFromAxisAngle(upRotAxis, Vector3.CalculateAngle(transfInitUp, UpVector)));
                }
                translMat = Matrix4.CreateTranslation(Position);
                return Matrix4.Mult(Matrix4.Mult(scaleMat, rotMat), translMat);
            }
        }
    }
}
