﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public class ParticlePointCloudDescriptor : VisualObjectDescriptor<ParticlePointCloud>
    {
        protected ParticlePointCloudDescriptor(ParticlePointCloud pCloud, TextureBlendTypes blendType)
            : base(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY, pCloud, blendType)
        {

        }

        public static ParticlePointCloudDescriptor FromEffectSystem(EffectSystem.EffectSystem eSys, TextureBlendTypes blendType)
        {
            var pCloud = ParticlePointCloud.FromEffectSystem(eSys, blendType);
            return new ParticlePointCloudDescriptor(pCloud, blendType);
        }

        public Texture TexAtlas
        {
            get
            {
                return drawable.TexAtlas;
            }
        }
    }
}
