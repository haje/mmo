﻿using OpenTK;
using OpenTK.Graphics;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering
{
    public class MeshDescriptor : VisualObjectDescriptor<Mesh>
    {
        protected MeshDescriptor(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, Mesh mesh, TextureBlendTypes blendType = TextureBlendTypes.Opaque, Color4? colorTint = null)
            : base(position, lookingAtDirection, upVector, mesh, blendType)
        {
            ColorTint = colorTint ?? Color4.White;
        }

        public static MeshDescriptor CreateMeshDescriptor(Vector3 position, Vector3 lookingAtDirection, Vector3 upVector, Mesh mesh, TextureBlendTypes blendType = TextureBlendTypes.Opaque, Color4? colorTint = null)
        {
            return new MeshDescriptor(position, lookingAtDirection, upVector, mesh, blendType, colorTint);
        }

        public Texture Texture
        {
            get
            {
                return drawable.Texture;
            }
        }

        public Vector3[] MeshVertices
        {
            get
            {
                return drawable.Vertices;
            }
        }

        public int[] MeshIndices
        {
            get
            {
                return drawable.Indices;
            }
        }

        public bool IsCloseEnough
        {
            get;
            set;
        }

        public Color4 ColorTint
        {
            get;
            set;
        }

        public Vector3 AABBMax
        {
            get
            {
                if (!isAABBCalculated)
                {
                    calculateMeshAABB();
                    isAABBCalculated = true;
                }
                return Vector3.Transform(meshAABBMax, ModelMatrix);
            }
        }

        public Vector3 AABBMin
        {
            get
            {
                if (!isAABBCalculated)
                {
                    calculateMeshAABB();
                    isAABBCalculated = true;
                }
                return Vector3.Transform(meshAABBMin, ModelMatrix);
            }
        }

        protected void calculateMeshAABB()
        {
            meshAABBMax = Vector3.One * -float.MaxValue;
            meshAABBMin = Vector3.One * float.MaxValue;
            foreach (var vertex in MeshVertices)
            {
                if (meshAABBMax.X < vertex.X) meshAABBMax.X = vertex.X;
                if (meshAABBMax.Y < vertex.Y) meshAABBMax.Y = vertex.Y;
                if (meshAABBMax.Z < vertex.Z) meshAABBMax.Z = vertex.Z;
                if (meshAABBMin.X > vertex.X) meshAABBMin.X = vertex.X;
                if (meshAABBMin.Y > vertex.Y) meshAABBMin.Y = vertex.Y;
                if (meshAABBMin.Z > vertex.Z) meshAABBMin.Z = vertex.Z;
            }
        }

        protected Vector3 meshAABBMax;
        protected Vector3 meshAABBMin;
        protected bool isAABBCalculated;
    }
}