﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using System.Drawing;

namespace HAJE.MMO.Client.Rendering
{
    public static class GLHelper
    {
        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            var error = GL.GetError();
            Debug.Assert(error == ErrorCode.NoError || error == ErrorCode.InvalidEnum);
        }

        public static void DrawTriangles(int count)
        {
            GL.DrawElements(PrimitiveType.Triangles, count, DrawElementsType.UnsignedInt, 0);
        }

        public static void DrawPoints(int count)
        {
            GL.DrawElements(PrimitiveType.Points, count, DrawElementsType.UnsignedInt, 0);
        }
    }
}
