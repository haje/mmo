﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class WorldSpaceForwardShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/WorldSpaceForwardShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/WorldSpaceForwardShaderFragmentSource.glsl";

        public WorldSpaceForwardShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 modelMat)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
    }
}
