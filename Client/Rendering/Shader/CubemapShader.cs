﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class CubemapShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/CubemapShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/CubemapShaderFragmentSource.glsl";

        public CubemapShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "Cubemap");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 modelMat, CubemapTexture texture)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
            if (texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.TextureCubeMap, texture.TextureHandle);
                GL.Uniform1(textureLoc, 0);
            }
            else
            {
                throw new System.ArgumentNullException("텍스처는 null일 수 없습니다.");
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
        int textureLoc;
    }
}
