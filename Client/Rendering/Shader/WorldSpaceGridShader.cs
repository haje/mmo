﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class WorldSpaceGridShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/WorldSpaceGridShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/WorldSpaceGridShaderFragmentSource.glsl";

        public WorldSpaceGridShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            freqLoc = GL.GetUniformLocation(ShaderProgram, "frequency");
            minHeightLoc = GL.GetUniformLocation(ShaderProgram, "minHeight");
            maxHeightLoc = GL.GetUniformLocation(ShaderProgram, "maxHeight");
        }

        public void Use(ref Matrix4 projMat, ref Matrix4 viewMat, ref Matrix4 modelMat, float frequency, float minHeight, float maxHeight)
        {
            base.Use();
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
            GL.Uniform1(freqLoc, frequency);
            GL.Uniform1(minHeightLoc, minHeight);
            GL.Uniform1(maxHeightLoc, maxHeight);
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
        int freqLoc;
        int minHeightLoc;
        int maxHeightLoc;
    }
}
