﻿using HAJE.MMO.Client.Rendering.VertexType;
using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class RectangleShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/RectangleShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/RectangleShaderFragmentSource.glsl";

        public RectangleShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void GetUniformLocation()
        {
            transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
            viewPortPtr = GL.GetUniformLocation(ShaderProgram, "viewPort");
            borderPtr = GL.GetUniformLocation(ShaderProgram, "borderColor");
            fillPtr = GL.GetUniformLocation(ShaderProgram, "fillColor");
        }

        public void Use(ref Matrix3 transform, Vector2 viewPort, Color4 borderColor, Color4 fillColor)
        {
            base.Use();
            GL.UniformMatrix3(transPtr, false, ref transform);
            GL.Uniform2(viewPortPtr, ref viewPort);
            GL.Uniform4(borderPtr, borderColor);
            GL.Uniform4(fillPtr, fillColor);
        }

        int transPtr;
        int viewPortPtr;
        int borderPtr;
        int fillPtr;
    }
}
