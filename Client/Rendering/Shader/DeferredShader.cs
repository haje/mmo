﻿using HAJE.MMO.Client.Rendering.VertexType;
using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class UnshadedShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/DeferredUnshadedVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/DeferredUnshadedFragmentSource.glsl";

        int projLoc;
        int viewLoc;
        int modelLoc;
        int colorTintLoc;

        int isTextureLoadedLoc;
        int textureLoc;

        public UnshadedShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }
        
        protected override void BindFragDataLocation()
        {
            GL.BindFragDataLocation(shaderProgram, 0, "UnshadedOut");
            GL.BindFragDataLocation(shaderProgram, 1, "NormalOut");
            GL.BindFragDataLocation(shaderProgram, 2, "DepthOut");
        }

        protected override void GetUniformLocation()
        {
            projLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            colorTintLoc = GL.GetUniformLocation(ShaderProgram, "ColorTint");

            isTextureLoadedLoc = GL.GetUniformLocation(ShaderProgram, "IsTextureLoaded");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "DiffuseTexture");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 model, Color4 colorTint, Texture texture)
        {
            GL.UniformMatrix4(projLoc, false, ref projMat);
            GL.UniformMatrix4(viewLoc, false, ref viewMat);
            GL.UniformMatrix4(modelLoc, false, ref model);
            GL.Uniform4(colorTintLoc, colorTint);
            if (texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
                GL.Uniform1(isTextureLoadedLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoadedLoc, 0);
            }
        }
    }

    public class LightShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/DeferredLightingVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/DeferredLightingFragmentSource.glsl";

        int projLoc;
        int viewLoc;
        int isShadowEnabledLoc;

        int depthTextureLoc;
        int normalTextureLoc;
        int shadowTextureLoc;

        int lightColorLoc;
        int lightPosLoc;
        int lightRadiusLoc;

        public LightShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void GetUniformLocation()
        {
            projLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            isShadowEnabledLoc = GL.GetUniformLocation(ShaderProgram, "IsShadowEnabled");

            depthTextureLoc = GL.GetUniformLocation(ShaderProgram, "DepthBuffer");
            normalTextureLoc = GL.GetUniformLocation(ShaderProgram, "NormalBuffer");
            shadowTextureLoc = GL.GetUniformLocation(ShaderProgram, "ShadowTexture");

            lightColorLoc = GL.GetUniformLocation(ShaderProgram, "LightColor");
            lightPosLoc = GL.GetUniformLocation(ShaderProgram, "LightPosition");
            lightRadiusLoc = GL.GetUniformLocation(ShaderProgram, "LightRadius");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Texture depthTexture, Texture normalTexture, DeferredPointLight light, Texture shadowTexture = null)
        {
            GL.UniformMatrix4(projLoc, false, ref projMat);
            GL.UniformMatrix4(viewLoc, false, ref viewMat);
            if (shadowTexture != null)
            {
                GL.Uniform1(isShadowEnabledLoc, 1);
                GL.ActiveTexture(TextureUnit.Texture2);
                GL.BindTexture(TextureTarget.Texture2D, shadowTexture.TextureHandle);
                GL.Uniform1(shadowTextureLoc, 2);
            }
            else
            {
                GL.Uniform1(isShadowEnabledLoc, 0);
            }

            // Textures are guaranteed to be generated on DeferredShader Object
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, depthTexture.TextureHandle);
            GL.Uniform1(depthTextureLoc, 0);

            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, normalTexture.TextureHandle);
            GL.Uniform1(normalTextureLoc, 1);

            GL.Uniform3(lightPosLoc, light.Position);
            GL.Uniform3(lightColorLoc, light.Intensity);
            GL.Uniform1(lightRadiusLoc, light.Radius);
        }
    }

    public class CompositeShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/DeferredCompositeVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/DeferredCompositeFragmentSource.glsl";

        int colorTextureLoc;
        int lightTextureLoc;
        int depthTextureLoc;
        int perlinTextureLoc;
        int projMatLoc;
        int viewMatLoc;
        int fogActivLoc;
        int fogColorLoc;
        int fogSightRangeLoc;
        int characterSyncRangeLoc;
        int cameraPosLoc;
        int cameraFarLoc;

        int screenWidth;
        int screenHeight;
        int crosshairSize;
        int isCrosshair;

        public CompositeShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void GetUniformLocation()
        {
            colorTextureLoc = GL.GetUniformLocation(ShaderProgram, "ColorBuffer");
            lightTextureLoc = GL.GetUniformLocation(ShaderProgram, "LightingBuffer");
            depthTextureLoc = GL.GetUniformLocation(ShaderProgram, "DepthBuffer");
            perlinTextureLoc = GL.GetUniformLocation(ShaderProgram, "PerlinNoise");
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            fogActivLoc = GL.GetUniformLocation(ShaderProgram, "FogActivated");
            fogColorLoc = GL.GetUniformLocation(ShaderProgram, "FogColor");
            fogSightRangeLoc = GL.GetUniformLocation(ShaderProgram, "FogSightRange");
            characterSyncRangeLoc = GL.GetUniformLocation(ShaderProgram, "CharacterSyncRange");
            cameraPosLoc = GL.GetUniformLocation(ShaderProgram, "CameraPosition");
            cameraFarLoc = GL.GetUniformLocation(ShaderProgram, "CameraFarP");

            screenWidth = GL.GetUniformLocation(ShaderProgram, "screenWidth");
            screenHeight = GL.GetUniformLocation(ShaderProgram, "screenHeight");
            crosshairSize = GL.GetUniformLocation(ShaderProgram, "crosshairSize");
            isCrosshair = GL.GetUniformLocation(ShaderProgram, "isCrosshair");
        }

        public void SetUniform(Texture colorTexture, Texture lightTexture, int sWidth, int sHeight, bool crosshairActivated, bool fogActivated, Color4? fogColor, float? fogSightRange, float? characterSyncRange, Camera cam = null, Texture depthTexture = null, Texture perlinTexture = null)
        {
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, colorTexture.TextureHandle);
            GL.Uniform1(colorTextureLoc, 0);

            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, lightTexture.TextureHandle);
            GL.Uniform1(lightTextureLoc, 1);

            GL.Uniform1(screenWidth, sWidth);
            GL.Uniform1(screenHeight, sHeight);
            GL.Uniform1(crosshairSize, 10);
            GL.Uniform1(isCrosshair, crosshairActivated ? 1 : 0);

            if (fogActivated)
            {
                if (!fogColor.HasValue || !fogSightRange.HasValue || !characterSyncRange.HasValue || cam == null || depthTexture == null || perlinTexture == null)
                {
                    throw new ArgumentNullException("All of the fog-related arguments should not be null if the fog is activated.");
                }
                GL.Uniform1(fogActivLoc, 1);

                Matrix4 projMat = cam.ProjectionMatrix;
                Matrix4 viewMat = cam.ViewMatrix;
                GL.UniformMatrix4(projMatLoc, false, ref projMat);
                GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
                GL.Uniform3(cameraPosLoc, cam.Position);
                GL.Uniform1(cameraFarLoc, cam.Far);
                GL.Uniform4(fogColorLoc, fogColor.Value);
                GL.Uniform1(fogSightRangeLoc, fogSightRange.Value);
                GL.Uniform1(characterSyncRangeLoc, characterSyncRange.Value);

                GL.ActiveTexture(TextureUnit.Texture2);
                GL.BindTexture(TextureTarget.Texture2D, depthTexture.TextureHandle);
                GL.Uniform1(depthTextureLoc, 2);

                GL.ActiveTexture(TextureUnit.Texture3);
                GL.BindTexture(TextureTarget.Texture2D, perlinTexture.TextureHandle);
                GL.Uniform1(perlinTextureLoc, 3);
            }
            else
            {
                GL.Uniform1(fogActivLoc, 0);
            }
        }
    }

    public class DepthOnlyShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/DeferredDepthOnlyVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/DeferredDepthOnlyFragmentSource.glsl";

        int projLoc;
        int viewLoc;
        int modelLoc;

        public DepthOnlyShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void BindFragDataLocation()
        {
            GL.BindFragDataLocation(shaderProgram, 0, "DepthOut");
        }

        protected override void GetUniformLocation()
        {
            projLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 model)
        {
            GL.UniformMatrix4(projLoc, false, ref projMat);
            GL.UniformMatrix4(viewLoc, false, ref viewMat);
            GL.UniformMatrix4(modelLoc, false, ref model);
        }
    }

    public class ShadowShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/DeferredShadowVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/DeferredShadowFragmentSource.glsl";

        int projLoc;
        int viewLoc;
        int shadowProjLoc;
        int shadowViewLoc;
        int shadowmapSizeLoc;

        int depthTextureLoc;
        int normalTextureLoc;
        int shadowmapLoc;

        public ShadowShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void GetUniformLocation()
        {
            projLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            shadowProjLoc = GL.GetUniformLocation(ShaderProgram, "ShadowProjectionMatrix");
            shadowViewLoc = GL.GetUniformLocation(ShaderProgram, "ShadowViewMatrix");
            shadowmapSizeLoc = GL.GetUniformLocation(ShaderProgram, "ShadowmapSize");

            depthTextureLoc = GL.GetUniformLocation(ShaderProgram, "DepthBuffer");
            normalTextureLoc = GL.GetUniformLocation(ShaderProgram, "NormalBuffer");
            shadowmapLoc = GL.GetUniformLocation(ShaderProgram, "Shadowmap");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Texture depthTexture, Texture normalTexture, ShadowCaster shadowCaster, Texture shadowmap)
        {
            GL.UniformMatrix4(projLoc, false, ref projMat);
            GL.UniformMatrix4(viewLoc, false, ref viewMat);
            var shadowProjMat = shadowCaster.ProjectionMatrix;
            var shadowViewMat = shadowCaster.ViewMatrix;
            GL.UniformMatrix4(shadowProjLoc, false, ref shadowProjMat);
            GL.UniformMatrix4(shadowViewLoc, false, ref shadowViewMat);

            // Assume the square shadowmap
            GL.Uniform1(shadowmapSizeLoc, shadowmap.Size.X);

            // Textures are guaranteed to be generated on DeferredShader Object
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, depthTexture.TextureHandle);
            GL.Uniform1(depthTextureLoc, 0);

            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, normalTexture.TextureHandle);
            GL.Uniform1(normalTextureLoc, 1);

            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture2D, shadowmap.TextureHandle);
            GL.Uniform1(shadowmapLoc, 2);
        }
    }
}
