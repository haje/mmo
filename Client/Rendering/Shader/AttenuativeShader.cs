﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class AttenuativeParticleShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/ParticleShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/AttenuativeShaderFragmentSource.glsl";
        const string geometrySourceFile = "Resource/Shader/ParticleShaderGeometrySource.glsl";

        public AttenuativeParticleShader()
            : base(vertexSourceFile, fragmentSourceFile, geometrySourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            texAtlasLoc = GL.GetUniformLocation(ShaderProgram, "Tex");
            isTextureLoadedLoc = GL.GetUniformLocation(ShaderProgram, "IsTextureLoaded");
            fogSightRangeLoc = GL.GetUniformLocation(ShaderProgram, "FogSightRange");
            characterSyncRangeLoc = GL.GetUniformLocation(ShaderProgram, "CharacterSyncRange");
        }

        protected override void BindFragDataLocation()
        {
            GL.BindFragDataLocation(ShaderProgram, 0, "colorAccum");
            GL.BindFragDataLocation(ShaderProgram, 1, "revealage");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Texture texAtlas, float fogSightRange, float charSyncRange)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.Uniform1(fogSightRangeLoc, fogSightRange);
            GL.Uniform1(characterSyncRangeLoc, charSyncRange);

            if (texAtlas != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texAtlas.TextureHandle);
                GL.Uniform1(texAtlasLoc, 0);
                GL.Uniform1(isTextureLoadedLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoadedLoc, 0);
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int texAtlasLoc;
        int isTextureLoadedLoc;
        int fogSightRangeLoc;
        int characterSyncRangeLoc;
    }

    public class AttenuativeMeshShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/WorldSpaceTextureShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/AttenuativeShaderFragmentSource.glsl";

        public AttenuativeMeshShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            colorTintLoc = GL.GetUniformLocation(ShaderProgram, "ColorTint");
            texLoc = GL.GetUniformLocation(ShaderProgram, "Tex");
            isTextureLoadedLoc = GL.GetUniformLocation(ShaderProgram, "IsTextureLoaded");
            characterDistanceLoc = GL.GetUniformLocation(ShaderProgram, "CharacterDistance");
            fogSightRangeLoc = GL.GetUniformLocation(ShaderProgram, "FogSightRange");
            characterSyncRangeLoc = GL.GetUniformLocation(ShaderProgram, "CharacterSyncRange");
        }

        protected override void BindFragDataLocation()
        {
            GL.BindFragDataLocation(ShaderProgram, 0, "colorAccum");
            GL.BindFragDataLocation(ShaderProgram, 1, "revealage");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 modelMat, Color4 colorTint, Texture tex, float charDist, float fogSightRange, float charSyncRange)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
            GL.Uniform4(colorTintLoc, colorTint);
            GL.Uniform1(characterDistanceLoc, charDist);
            GL.Uniform1(fogSightRangeLoc, fogSightRange);
            GL.Uniform1(characterSyncRangeLoc, charSyncRange);

            if (tex != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, tex.TextureHandle);
                GL.Uniform1(texLoc, 0);
                GL.Uniform1(isTextureLoadedLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoadedLoc, 0);
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
        int texLoc;
        int isTextureLoadedLoc;
        int colorTintLoc;
        int characterDistanceLoc;
        int fogSightRangeLoc;
        int characterSyncRangeLoc;
    }

    public class AttenuativeCompositeShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/AttenuativeCompositeShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/AttenuativeCompositeShaderFragmentSource.glsl";

        public AttenuativeCompositeShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            accumTextureLoc = GL.GetUniformLocation(ShaderProgram, "accumTexture");
            revealageTextureLoc = GL.GetUniformLocation(ShaderProgram, "revealageTexture");
        }

        public void SetUniform(Texture accumTex, Texture revealageTex)
        {
            if (accumTex != null && revealageTex != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, accumTex.TextureHandle);
                GL.Uniform1(accumTextureLoc, 0);

                GL.ActiveTexture(TextureUnit.Texture1);
                GL.BindTexture(TextureTarget.Texture2D, revealageTex.TextureHandle);
                GL.Uniform1(revealageTextureLoc, 1);
            }
            else
            {
                throw new ArgumentNullException("Texture는 null이 될 수 없습니다.");
            }
        }

        int accumTextureLoc;
        int revealageTextureLoc;
    }
}
