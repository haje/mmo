﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class LabelShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/LabelShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/LabelShaderFragmentSource.glsl";

        public LabelShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void GetUniformLocation()
        {
            transfMatLoc = GL.GetUniformLocation(ShaderProgram, "transfMat");
            viewportLoc = GL.GetUniformLocation(ShaderProgram, "viewport");
            colorLoc = GL.GetUniformLocation(ShaderProgram, "color");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "tex");
            isTextureLoc = GL.GetUniformLocation(ShaderProgram, "isTexture");
        }

        public void Use(ref Matrix3 transform, Vector2 viewport, Color4 color, Texture texture)
        {
            base.Use();
            GL.UniformMatrix3(transfMatLoc, false, ref transform);
            GL.Uniform2(viewportLoc, ref viewport);
            GL.Uniform4(colorLoc, color);
            if (texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
                GL.Uniform1(textureLoc, 0);
                GL.Uniform1(isTextureLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoc, 0);
            }
        }

        int transfMatLoc;
        int viewportLoc;
        int colorLoc;
        int textureLoc;
        int isTextureLoc;
    }
}
