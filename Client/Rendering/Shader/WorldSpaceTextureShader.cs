﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class WorldSpaceTextureShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/WorldSpaceTextureShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/WorldSpaceTextureShaderFragmentSource.glsl";

        public WorldSpaceTextureShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            colorTintLoc = GL.GetUniformLocation(ShaderProgram, "ColorTint");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "Tex");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 modelMat, Color4 colorTint, Texture texture)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
            GL.Uniform4(colorTintLoc, colorTint);
            if (texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
                GL.Uniform1(textureLoc, 0);
            }
            else
            {
                throw new System.ArgumentNullException("텍스처는 null일 수 없습니다.");
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
        int textureLoc;
        int colorTintLoc;
    }

    public class PostProcessingBlurShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/WorldSpaceTextureShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/PostProcessingBlurShaderFragmentSource.glsl";

        public PostProcessingBlurShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            colorTintLoc = GL.GetUniformLocation(ShaderProgram, "ColorTint");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "Tex");
            textureSizeLoc = GL.GetUniformLocation(ShaderProgram, "TextureSize");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 modelMat, Color4 colorTint, Texture texture)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
            GL.Uniform4(colorTintLoc, colorTint);
            if (texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
                GL.Uniform1(textureLoc, 0);
                GL.Uniform2(textureSizeLoc, texture.Size);
            }
            else
            {
                throw new System.ArgumentNullException("텍스처는 null일 수 없습니다.");
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
        int textureLoc;
        int colorTintLoc;
        int textureSizeLoc;
    }
}
