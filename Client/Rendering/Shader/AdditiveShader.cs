﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class AdditiveParticleShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/ParticleShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/AdditiveShaderFragmentSource.glsl";
        const string geometrySourceFile = "Resource/Shader/ParticleShaderGeometrySource.glsl";

        public AdditiveParticleShader()
            : base(vertexSourceFile, fragmentSourceFile, geometrySourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            texAtlasLoc = GL.GetUniformLocation(ShaderProgram, "Tex");
            isTextureLoadedLoc = GL.GetUniformLocation(ShaderProgram, "IsTextureLoaded");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Texture texAtlas)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);

            if (texAtlas != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texAtlas.TextureHandle);
                GL.Uniform1(texAtlasLoc, 0);
                GL.Uniform1(isTextureLoadedLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoadedLoc, 0);
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int texAtlasLoc;
        int isTextureLoadedLoc;
    }

    public class AdditiveMeshShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/WorldSpaceTextureShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/AdditiveShaderFragmentSource.glsl";

        public AdditiveMeshShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {

        }

        protected override void GetUniformLocation()
        {
            projMatLoc = GL.GetUniformLocation(ShaderProgram, "ProjectionMatrix");
            viewMatLoc = GL.GetUniformLocation(ShaderProgram, "ViewMatrix");
            modelMatLoc = GL.GetUniformLocation(ShaderProgram, "ModelMatrix");
            colorTintLoc = GL.GetUniformLocation(ShaderProgram, "ColorTint");
            texLoc = GL.GetUniformLocation(ShaderProgram, "Tex");
            isTextureLoadedLoc = GL.GetUniformLocation(ShaderProgram, "IsTextureLoaded");
        }

        public void SetUniform(Matrix4 projMat, Matrix4 viewMat, Matrix4 modelMat, Color4 colorTint, Texture tex)
        {
            GL.UniformMatrix4(projMatLoc, false, ref projMat);
            GL.UniformMatrix4(viewMatLoc, false, ref viewMat);
            GL.UniformMatrix4(modelMatLoc, false, ref modelMat);
            GL.Uniform4(colorTintLoc, colorTint);

            if (tex != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, tex.TextureHandle);
                GL.Uniform1(texLoc, 0);
                GL.Uniform1(isTextureLoadedLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoadedLoc, 0);
            }
        }

        int projMatLoc;
        int viewMatLoc;
        int modelMatLoc;
        int texLoc;
        int isTextureLoadedLoc;
        int colorTintLoc;
    }
}