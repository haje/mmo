﻿using HAJE.MMO.Client.Rendering.VertexType;
using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.Rendering.Shader
{
    public class ScreenSpaceTextureShader : ShaderBase
    {
        const string vertexSourceFile = "Resource/Shader/ScreenSpaceTextureShaderVertexSource.glsl";
        const string fragmentSourceFile = "Resource/Shader/ScreenSpaceTextureShaderFragmentSource.glsl";

        public ScreenSpaceTextureShader()
            : base(vertexSourceFile, fragmentSourceFile)
        {
        }

        protected override void GetUniformLocation()
        {
            transfMatLoc = GL.GetUniformLocation(ShaderProgram, "transfMat");
            viewportLoc = GL.GetUniformLocation(ShaderProgram, "viewport");
            colorLoc = GL.GetUniformLocation(ShaderProgram, "color");
            textureLoc = GL.GetUniformLocation(ShaderProgram, "tex");
            isTextureLoc = GL.GetUniformLocation(ShaderProgram, "isTexture");
            uvMinLoc = GL.GetUniformLocation(ShaderProgram, "uvMin");
            uvMaxLoc = GL.GetUniformLocation(ShaderProgram, "uvMax");
            
            
        }

        public void Use(ref Matrix3 transform, Vector2 viewport, Color4 color, Texture texture, Vector2 uvMin, Vector2 uvMax)
        {
            base.Use();
            GL.UniformMatrix3(transfMatLoc, false, ref transform);
            GL.Uniform2(viewportLoc, ref viewport);
            GL.Uniform4(colorLoc, color);
            if (texture != null)
            {
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
                GL.Uniform1(textureLoc, 0);
                GL.Uniform1(isTextureLoc, 1);
            }
            else
            {
                GL.Uniform1(isTextureLoc, 0);
            }
            GL.Uniform2(uvMinLoc, uvMin);
            GL.Uniform2(uvMaxLoc, uvMax);
        }

        int transfMatLoc;
        int viewportLoc;
        int textureLoc;
        int isTextureLoc;
        int colorLoc;
        int uvMinLoc;
        int uvMaxLoc;
    }
}
