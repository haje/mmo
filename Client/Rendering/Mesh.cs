﻿using FileFormatWavefront;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HAJE.MMO.Client.Rendering
{
    public class Mesh : Drawable
    {
        public Texture Texture
        {
            get;
            protected set;
        }

        public readonly Vector3[] Vertices;
        public readonly int[] Indices;

        public Mesh(VertexType.VertexPosition[] vertices, int[] indices)
        {
            vertArray = Buffer.VertexArray.CreateArray
                (
                vertices,
                BufferUsageHint.StaticDraw,
                indices
                );
            Texture = null;
            Vertices = vertices.Select(v => v.Position).ToArray();
            Indices = indices;
        }

        public Mesh(VertexType.VertexWorldPositionNormalColor[] vertices, int[] indices)
        {
            vertArray = Buffer.VertexArray.CreateArray
                (
                vertices,
                BufferUsageHint.StaticDraw,
                indices
                );
            Texture = null;
            Vertices = vertices.Select(v => v.Position).ToArray();
            Indices = indices;
        }

        public Mesh(VertexType.VertexPositionNormalTintUV[] vertices, int[] indices, Texture texture = null)
        {
            vertArray = Buffer.VertexArray.CreateArray
                (
                vertices,
                BufferUsageHint.StaticDraw,
                indices
                );
            Texture = texture;
            Vertices = vertices.Select(v => v.Position).ToArray();
            Indices = indices;
        }

        public static Mesh CreateScreenQuad(Texture texture = null)
        {
            VertexType.VertexPositionNormalTintUV[] vertices = new VertexType.VertexPositionNormalTintUV[4];
            int[] indices = new int[]
            {
                0,1,2,
                1,3,2
            };

            vertices[0].UV = new Vector2(0, 0);
            vertices[0].Position = new Vector3(-1, -1, 0);
            vertices[0].Normal = new Vector3(0, 0, -1);
            vertices[0].Tint = Color4.White;

            vertices[1].UV = new Vector2(1, 0);
            vertices[1].Position = new Vector3(1, -1, 0);
            vertices[1].Normal = new Vector3(0, 0, -1);
            vertices[1].Tint = Color4.White;

            vertices[2].UV = new Vector2(0, 1);
            vertices[2].Position = new Vector3(-1, 1, 0);
            vertices[2].Normal = new Vector3(0, 0, -1);
            vertices[2].Tint = Color4.White;

            vertices[3].UV = new Vector2(1, 1);
            vertices[3].Position = new Vector3(1, 1, 0);
            vertices[3].Normal = new Vector3(0, 0, -1);
            vertices[3].Tint = Color4.White;

            if (texture != null)
            {
                return new Mesh(vertices, indices, texture);
            }
            else
            {
                return new Mesh(vertices, indices);
            }
        }

        // Create the cube that is facing inside.
        public static Mesh CreateSkycube()
        {
            VertexType.VertexPosition[] vertices = new VertexType.VertexPosition[8];
            int[] indices = new int[]
            {
                0, 2, 1,
                2, 0, 3,
                4, 5, 6,
                4, 6, 7,
                4, 3, 0,
                4, 7, 3,
                5, 1, 2,
                5, 2, 6,
                4, 1, 5,
                4, 0, 1,
                6, 2, 3,
                6, 3, 7,
            };

            vertices[0].Position = new Vector3( 1,  1,  1);
            vertices[1].Position = new Vector3( 1, -1,  1);
            vertices[2].Position = new Vector3( 1, -1, -1);
            vertices[3].Position = new Vector3( 1,  1, -1);
            vertices[4].Position = new Vector3(-1,  1,  1);
            vertices[5].Position = new Vector3(-1, -1,  1);
            vertices[6].Position = new Vector3(-1, -1, -1);
            vertices[7].Position = new Vector3(-1,  1, -1);

            return new Mesh(vertices, indices);
        }

        public static Mesh FromWavefront(string path)
        {
            var modelData = FileFormatObj.Load(path, false).Model;
            List<Vector3> posList = new List<Vector3>(), normalList = new List<Vector3>();
            List<Vector2> uvList = new List<Vector2>();
            List<VertexType.VertexPositionNormalTintUV> vertexList = new List<VertexType.VertexPositionNormalTintUV>();
            List<int> posIndices = new List<int>(), normIndices = new List<int>();

            foreach (var position in modelData.Vertices)
            {
                posList.Add(new Vector3(position.x, position.y, position.z));
            }

            foreach (var normal in modelData.Normals)
            {
                normalList.Add(new Vector3(normal.x, normal.y, normal.z));
            }

            foreach (var uv in modelData.Uvs)
            {
                uvList.Add(new Vector2(uv.u, uv.v));
            }

            // assume only one diffuse texture
            String texturePath;
            try
            {
                texturePath = modelData.Materials[0].TextureMapDiffuse.Path;
                texturePath = Path.Combine(new FileInfo(path).DirectoryName, texturePath);
            }
            catch (ArgumentOutOfRangeException)
            {
                texturePath = null;
            }

            int counter = 0;
            if (modelData.Groups.Count > 0)
            {
                foreach (var group in modelData.Groups)
                {
                    foreach (var face in group.Faces)
                    {
                        foreach (var index in face.Indices)
                        {
                            VertexType.VertexPositionNormalTintUV vertex = new VertexType.VertexPositionNormalTintUV();
                            vertex.Position = posList[index.vertex];
                            vertex.Normal = normalList[(int)index.normal];
                            try
                            {
                                var color = face.Material.Diffuse;
                                vertex.Tint = new Color4(color.r, color.g, color.b, color.a);
                            }
                            catch (NullReferenceException)
                            {
                                vertex.Tint = Color4.White;
                            }
                            if (index.uv.HasValue)
                                vertex.UV = uvList[index.uv.Value];
                            vertexList.Add(vertex);
                            posIndices.Add(counter);
                            counter++;
                        }
                    }
                }
            }
            else
            {
                foreach (var face in modelData.UngroupedFaces)
                {
                    foreach (var index in face.Indices)
                    {
                        VertexType.VertexPositionNormalTintUV vertex = new VertexType.VertexPositionNormalTintUV();
                        vertex.Position = posList[index.vertex];
                        vertex.Normal = normalList[(int)index.normal];
                        try
                        {
                            var color = face.Material.Diffuse;
                            vertex.Tint = new Color4(color.r, color.g, color.b, color.a);
                        }
                        catch (NullReferenceException)
                        {
                            vertex.Tint = Color4.White;
                        }
                        if (index.uv.HasValue)
                            vertex.UV = uvList[index.uv.Value];
                        vertexList.Add(vertex);
                        posIndices.Add(counter);
                        counter++;
                    }
                }
            }

            Texture texture;
            try
            {
                texture = Texture.CreateFromFile(texturePath);
            }
            catch (FileNotFoundException)
            {
                texture = null;
            }

            return new Mesh(
                vertexList.ToArray(),
                posIndices.ToArray(),
                texture
                );
        }

        public static Mesh FromHeightmap(float[,] map)
        {
            List<Vector3> posList = new List<Vector3>(), normalList = new List<Vector3>();
            int[,] colRowToPosListIdx;
            Dictionary<int, List<Vector3>> posListIdxToFaceNormal = new Dictionary<int, List<Vector3>>();
            List<int> posIndices = new List<int>();

            int terrainHeight = map.GetLength(0);
            int terrainWidth  = map.GetLength(1);
            colRowToPosListIdx = new int[terrainWidth, terrainHeight];

            // vertex init
            for (int row = 0; row < terrainHeight; ++row)
            {
                for (int column = 0; column < terrainWidth; ++column)
                {
                    // mesh heightmap will be positioned in the left bottom
                    float x = column;
                    float y = map[row, column];
                    float z = row;
                    colRowToPosListIdx[column, row] = posList.Count;
                    posListIdxToFaceNormal[posList.Count] = new List<Vector3>();
                    posList.Add(new Vector3(x, y, z));
                }
            }

            #region faceInit
            // AB
            // CD
            for (int row = 0; row < terrainHeight - 1; ++row)
            {
                for (int column = 0; column < terrainWidth - 1; ++column)
                {
                    int posAIdx = colRowToPosListIdx[column, row];
                    int posBIdx = colRowToPosListIdx[column + 1, row];
                    int posCIdx = colRowToPosListIdx[column, row + 1];
                    int posDIdx = colRowToPosListIdx[column + 1, row + 1];
                    Vector3 vtxA = posList[posAIdx];
                    Vector3 vtxB = posList[posBIdx];
                    Vector3 vtxC = posList[posCIdx];
                    Vector3 vtxD = posList[posDIdx];

                    if ((vtxA - vtxD).Length < (vtxB - vtxC).Length)
                    {
                        {
                            posIndices.Add(posAIdx); posIndices.Add(posDIdx); posIndices.Add(posBIdx);
                            Vector3 faceNormal = Vector3.Cross((vtxD - vtxA), (vtxB - vtxA)).Normalized();
                            posListIdxToFaceNormal[posAIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posDIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posBIdx].Add(faceNormal);
                        }
                        {
                            posIndices.Add(posAIdx); posIndices.Add(posCIdx); posIndices.Add(posDIdx);
                            Vector3 faceNormal = Vector3.Cross((vtxC - vtxA), (vtxD - vtxA)).Normalized();
                            posListIdxToFaceNormal[posAIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posCIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posDIdx].Add(faceNormal);
                        }
                    }
                    else
                    {
                        {
                            posIndices.Add(posCIdx); posIndices.Add(posBIdx); posIndices.Add(posAIdx);
                            Vector3 faceNormal = Vector3.Cross((vtxB - vtxC), (vtxA - vtxC)).Normalized();
                            posListIdxToFaceNormal[posCIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posBIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posAIdx].Add(faceNormal);
                        }
                        {
                            posIndices.Add(posCIdx); posIndices.Add(posDIdx); posIndices.Add(posBIdx);
                            Vector3 faceNormal = Vector3.Cross((vtxD - vtxC), (vtxB - vtxC)).Normalized();
                            posListIdxToFaceNormal[posCIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posDIdx].Add(faceNormal);
                            posListIdxToFaceNormal[posBIdx].Add(faceNormal);
                        }
                    }
                    // can calculate normal for A
                    Vector3 vtxNormal = Vector3.Zero;
                    foreach (var faceNormal in posListIdxToFaceNormal[posAIdx])
                    {
                        vtxNormal += faceNormal;
                    }
                    vtxNormal /= posListIdxToFaceNormal[posAIdx].Count;
                    normalList.Add(vtxNormal);

                    // last column; calculate it for B too
                    if (column == terrainWidth - 2)
                    {
                        vtxNormal = Vector3.Zero;
                        foreach (var faceNormal in posListIdxToFaceNormal[posBIdx])
                        {
                            vtxNormal += faceNormal;
                        }
                        vtxNormal /= posListIdxToFaceNormal[posBIdx].Count;
                        normalList.Add(vtxNormal);
                    }
                }
            }

            for (int column = 0; column < terrainWidth - 1; ++column)
            {
                // normal calculation for the final row
                int row = terrainHeight - 1;
                int posAIdx = colRowToPosListIdx[column, row];
                int posBIdx = colRowToPosListIdx[column + 1, row];

                // can calculate normal for A
                Vector3 vtxNormal = Vector3.Zero;
                foreach (var faceNormal in posListIdxToFaceNormal[posAIdx])
                {
                    vtxNormal += faceNormal;
                }
                vtxNormal /= posListIdxToFaceNormal[posAIdx].Count;
                normalList.Add(vtxNormal);

                // last column; calculate it for B too
                if (column == terrainWidth - 2)
                {
                    vtxNormal = Vector3.Zero;
                    foreach (var faceNormal in posListIdxToFaceNormal[posBIdx])
                    {
                        vtxNormal += faceNormal;
                    }
                    vtxNormal /= posListIdxToFaceNormal[posBIdx].Count;
                    normalList.Add(vtxNormal);
                }
            }
            #endregion faceInit

            VertexType.VertexWorldPositionNormalColor[] vertexList = new VertexType.VertexWorldPositionNormalColor[posList.Count];
            for (int vertexIter = 0; vertexIter < vertexList.Length; ++vertexIter)
            {
                vertexList[vertexIter].Position = posList[vertexIter];
                vertexList[vertexIter].Normal = normalList[vertexIter];
                vertexList[vertexIter].Color = Color4.Beige;
            }

            return new Mesh(vertexList, posIndices.ToArray());
        }

        public static Mesh FromBMLabel(UI.Component.BMLabel label)
        {
            List<VertexType.VertexPositionNormalTintUV> vertices = new List<VertexType.VertexPositionNormalTintUV>();
            List<int> indices = new List<int>();

            var text = label.Text;
            var font = label.Font;
            var canvasSize = font.MeasureFont(text);
            var texSize = label.Font.TextureSize;

            Vector2 startPt = Vector2.Zero;
            var prevChar = ' ';
            foreach (var character in text)
            {
                var vCnt = vertices.Count;
                indices.Add(vCnt); indices.Add(vCnt + 1); indices.Add(vCnt + 2);
                indices.Add(vCnt + 1); indices.Add(vCnt + 3); indices.Add(vCnt + 2);
                var glyph = font.Characters[character];
                var kerning = font.GetKerning(prevChar, character);

                // emit vtx
                Vector2 offsetPt = startPt + new Vector2(glyph.Offset.X + kerning, glyph.Offset.Y);
                VertexType.VertexPositionNormalTintUV[] quadVtx = new VertexType.VertexPositionNormalTintUV[4];
                quadVtx[0].UV = new Vector2((float)glyph.Bounds.X / texSize.Width, (float)glyph.Bounds.Y / texSize.Height);
                quadVtx[0].Position = new Vector3(offsetPt.X / canvasSize.Width * 2 - 1, offsetPt.Y / canvasSize.Height * 2 - 1, 0);
                quadVtx[0].Normal = new Vector3(0, 0, -1);
                quadVtx[0].Tint = Color4.White;

                quadVtx[1].UV = new Vector2((float)(glyph.Bounds.X + glyph.Bounds.Width) / texSize.Width, (float)glyph.Bounds.Y / texSize.Height);
                quadVtx[1].Position = new Vector3((offsetPt.X + glyph.Bounds.Width) / canvasSize.Width * 2 - 1, offsetPt.Y / canvasSize.Height * 2 - 1, 0);
                quadVtx[1].Normal = new Vector3(0, 0, -1);
                quadVtx[1].Tint = Color4.White;

                quadVtx[2].UV = new Vector2((float)(glyph.Bounds.X) / texSize.Width, (float)(glyph.Bounds.Y + glyph.Bounds.Height) / texSize.Height);
                quadVtx[2].Position = new Vector3(offsetPt.X / canvasSize.Width * 2 - 1, (offsetPt.Y + glyph.Bounds.Height) / canvasSize.Height * 2 - 1, 0);
                quadVtx[2].Normal = new Vector3(0, 0, -1);
                quadVtx[2].Tint = Color4.White;

                quadVtx[3].UV = new Vector2((float)(glyph.Bounds.X + glyph.Bounds.Width) / texSize.Width, (float)(glyph.Bounds.Y + glyph.Bounds.Height) / texSize.Height);
                quadVtx[3].Position = new Vector3((offsetPt.X + glyph.Bounds.Width) / canvasSize.Width * 2 - 1, (offsetPt.Y + glyph.Bounds.Height) / canvasSize.Height * 2 - 1, 0);
                quadVtx[3].Normal = new Vector3(0, 0, -1);
                quadVtx[3].Tint = Color4.White;

                vertices.AddRange(quadVtx);

                startPt.X += glyph.XAdvance + kerning;
                prevChar = character;
            }

            return new Mesh(vertices.ToArray(), indices.ToArray());
        }
    }
}
