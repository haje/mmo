﻿
namespace HAJE.MMO.Client.Rendering.Picking
{
    public enum PickableType
    {
        None,
        Terrain,
        Doodad,
        Player,
    }
}
