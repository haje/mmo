﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public struct PickingResult
    {
        public PickingResult(bool isPicked, Vector3 collidePosition, PickableType type, object owner)
        {
            this.IsPicked = isPicked;
            this.CollidePosition = collidePosition;
            this.Type = type;
            this.Owner = owner;

        }

        public static PickingResult NothingPicked
        {
            get
            {
                return new PickingResult(false, Vector3.Zero, PickableType.None, null);
            }
        }
        
        public readonly bool IsPicked;
        public readonly Vector3 CollidePosition;
        public readonly PickableType Type;
        public readonly object Owner;
    }
}
