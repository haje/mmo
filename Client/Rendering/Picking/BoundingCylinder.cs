﻿using BulletSharp;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public class BoundingCylinder : DynamicPickable, IRecursivePickable, IBulletSharpPickable
    {
        public BoundingCylinder(WorldSpaceEntity boundTarget, float radius, float height, PickableType type, object owner)
            : base(boundTarget, owner, type)
        {
            this.radius = radius;
            this.height = height;
            collisionObject = new CollisionObject();
            collisionObject.CollisionShape = new CylinderShape(radius, height / 2, radius);
            collisionObject.UserObject = this;
            collisionObject.WorldTransform = this.boundTarget.ModelMatrix.ClearScale();
        }

        public CollisionObject CollisionObject
        {
            get
            {
                return collisionObject;
            }
        }

        protected override void updateCollisionObject()
        {
            collisionObject.WorldTransform = this.boundTarget.ModelMatrix.ClearScale();
        }

        public PickingResult Pick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType)
        {
            if (excludeList.Contains(this)) return PickingResult.NothingPicked;
            if (!allowedType.Contains(Type)) return PickingResult.NothingPicked;

            //TODO: @mutal 여기를 구현

            // Check for side
            Vector3 Pa = boundTarget.Position;
            Vector3 Va = new Vector3(0, 1, 0);
            Vector3 DeltaP = origin - Pa;

            float A = (float)Math.Pow((direction - Vector3.Dot(direction, Va) * Va).LengthSquared, 2.0);
            float B = 2 * Vector3.Dot((direction - Vector3.Dot(direction, Va) * Va), (DeltaP - Vector3.Dot(DeltaP, Va) * Va));
            float C = (DeltaP - Vector3.Dot(DeltaP, Va) * Va).LengthSquared - radius * radius;

            float t1 = (-B + (float)Math.Sqrt(B * B - 4 * A * C)) / (2 * A);
            float t2 = (-B - (float)Math.Sqrt(B * B - 4 * A * C)) / (2 * A);

            bool isCollideSide;
            float tSide = float.MaxValue;

            // Can t1 ever be smaller than t2?
            if(t1==float.NaN && t2==float.NaN)
            {
                isCollideSide = false;
            }
            else if (t1 != float.NaN && t2 == float.NaN)
            {
                if (t1 < 0)
                    isCollideSide = false;
                else
                {
                    isCollideSide = true;
                    tSide = t1;
                }
            }
            else if (t1 == float.NaN && t2 != float.NaN)
            {
                if (t2 < 0)
                    isCollideSide = false;
                else
                {
                    isCollideSide = true;
                    tSide = t2;
                }
            }
            else
            {
                if (t1 < 0 && t2 < 0)
                {
                    isCollideSide = false;
                }
                else if (t1 < 0 && t2 >= 0)
                {
                    isCollideSide = true;
                    tSide = t2;
                }
                else if (t2 < 0 && t1 >= 0)
                {
                    isCollideSide = true;
                    tSide = t1;
                }
                else
                {
                    isCollideSide = true;
                    tSide = t1 > t2 ? t2 : t1;
                }
            }

            Vector3 collidePosSide = origin + tSide * direction;
            if (!(collidePosSide.Y >= Pa.Y && collidePosSide.Y <= (Pa.Y + height)))
            {
                isCollideSide = false;
                tSide = float.MaxValue;
            }

            // Check for base
            float tTop = (Pa.Y + height - origin.Y) / direction.Y;
            bool isCollideTop = true;
            Vector3 collidePosTop = origin + tTop * direction;
            if (!(collidePosTop.X * collidePosTop.X + collidePosTop.Z * collidePosTop.Z <= radius * radius))
            {
                tTop = float.MaxValue;
                isCollideTop = false;
            }

            float tBottom = (Pa.Y - origin.Y) / direction.Y;
            bool isCollideBottom = true;
            Vector3 collidePosBottom = origin + tBottom * direction;
            if (!(collidePosBottom.X * collidePosBottom.X + collidePosBottom.Z * collidePosBottom.Z <= radius * radius))
            {
                tBottom = float.MaxValue;
                isCollideBottom = false;
            }

            float t;
            if (!isCollideSide && !isCollideBottom && !isCollideTop)
                return PickingResult.NothingPicked;
            else
            {
                t = tSide > tBottom ? (tBottom > tTop ? tTop : tBottom) : (tSide > tTop ? tTop : tSide);
            }
            Vector3 collidePos = origin + t * direction;
         
            return new PickingResult(true, collidePos, Type, Owner);
        }
        
        readonly float radius;
        readonly float height;
        protected CollisionObject collisionObject;
    }
}
