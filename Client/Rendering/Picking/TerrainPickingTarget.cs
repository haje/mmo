﻿using BulletSharp;
using HAJE.MMO.Terrain;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public class TerrainPickingTarget : Pickable, IRecursivePickable, IBulletSharpPickable
    {
        public TerrainPickingTarget(TerrainMeshDescriptor terrain)
            : base(null, PickableType.Terrain)
        {
            this.heightMap = terrain.Heightmap.HeightmapRawData;
            this.origin = terrain.Position.Xz;
            collisionObject = new CollisionObject();
            var terrainTriangleMesh = new TriangleIndexVertexArray(terrain.MeshIndices, terrain.MeshVertices);
            collisionObject.CollisionShape = new BvhTriangleMeshShape(terrainTriangleMesh, true);
            /*
            var midHeight = (terrain.Heightmap.MaxHeight - terrain.Heightmap.MinHeight) / 2;
            collisionObject.CollisionShape = new HeightfieldTerrainShape(
                terrain.Heightmap.HeightmapRawData.GetLength(0),
                terrain.Heightmap.HeightmapRawData.GetLength(1),
                terrain.Heightmap.HeightmapRawDataStream,
                terrain.Heightmap.GridSize,
                terrain.Heightmap.MinHeight - midHeight,
                terrain.Heightmap.MaxHeight - midHeight,
                1,  // Y-axis
                PhyScalarType.PhyFloat,
                false);
            */
            collisionObject.WorldTransform = terrain.ModelMatrix;
            collisionObject.UserObject = this;
        }

        public CollisionObject CollisionObject
        {
            get
            {
                return collisionObject;
            }
        }

        public PickingResult Pick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType)
        {
            if (excludeList.Contains(this)) return PickingResult.NothingPicked;
            if (!allowedType.Contains(PickableType.Terrain)) return PickingResult.NothingPicked;

            //TODO: @mutal 여기를 구현
            direction.Normalize();
            Vector3 curPos = origin;

            int xLow = (int)Math.Floor(curPos.X);
            int xHigh = (int)Math.Ceiling(curPos.X);
            int zLow = (int)Math.Floor(curPos.Z);
            int zHigh = (int)Math.Ceiling(curPos.Z);

            while (curPos.Y >= 0 && curPos.Y <= 10
                && curPos.X >= 0 && curPos.X < heightMap.GetLength(0)
                && curPos.Z >= 0 && curPos.Z < heightMap.GetLength(1))
            {
                if (xLow < 0 || zLow < 0 || xHigh >= heightMap.GetLength(0) || zHigh >= heightMap.GetLength(1))
                    break;

                float tXLow = (xLow - curPos.X) / direction.X > 0 ? (xLow - curPos.X) / direction.X : float.MaxValue;
                float tXHigh = (xHigh - curPos.X) / direction.X > 0 ? (xHigh - curPos.X) / direction.X : float.MaxValue;
                float tZLow = (zLow - curPos.Z) / direction.Z > 0 ? (zLow - curPos.Z) / direction.Z : float.MaxValue;
                float tZHigh = (zHigh - curPos.Z) / direction.Z > 0 ? (zHigh - curPos.Z) / direction.Z : float.MaxValue;

                float t = Math.Min(tXLow, Math.Min(tXHigh, Math.Min(tZLow, tZHigh)));

                float temp = 0.0f;
                Vector3 tempPos = curPos;

                float zxHeight = heightMap[zLow, xLow];
                float zXHeight = heightMap[zLow, xHigh];
                float ZxHeight = heightMap[zHigh, xLow];
                float ZXHeight = heightMap[zHigh, xHigh];

                float minHeight = Math.Min(zxHeight, Math.Min(zXHeight, Math.Min(ZxHeight, ZXHeight)));
                float maxHeight = Math.Max(zxHeight, Math.Max(zXHeight, Math.Max(ZxHeight, ZXHeight)));

                
                while(temp < t)
                {
                    float xOffset = tempPos.X - xLow;
                    float zOffset = 1 - (tempPos.Z - zLow);
                    Vector2 zHeights = Vector2.Lerp
                        (
                        new Vector2(zxHeight, ZxHeight),
                        new Vector2(zXHeight, ZXHeight),
                        xOffset
                        );
                    float height = zHeights.X * zOffset + zHeights.Y * (1 - zOffset);

                    // TOLERATION MAGIC NUMBER
                    if (Math.Abs(tempPos.Y - height) < 0.01f)
                        return new PickingResult(true, tempPos, PickableType.Terrain, null);
                    
                    temp += 0.01f;
                    tempPos = tempPos + 0.01f * direction;
                }

                curPos = curPos + t * direction;

                if(t==tXLow)
                {
                    xLow--;
                    xHigh--;
                }
                else if(t==tXHigh)
                {
                    xLow++;
                    xHigh++;
                }
                else if(t==tZLow)
                {
                    zLow--;
                    zHigh--;
                }
                else if(t==tZHigh)
                {
                    zLow++;
                    zHigh++;
                }
            }

            return PickingResult.NothingPicked;
        }

        readonly float[,] heightMap;
        readonly Vector2 origin;
        readonly CollisionObject collisionObject;
    }
}
