﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public interface IRecursivePickable
    {
        PickingResult Pick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType);
    }
}
