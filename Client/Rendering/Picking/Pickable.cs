﻿using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public class Pickable
    {
        public readonly object Owner;
        public readonly PickableType Type;

        public Pickable(object owner, PickableType type)
        {
            this.Owner = owner;
            this.Type = type;
        }
    }
}
