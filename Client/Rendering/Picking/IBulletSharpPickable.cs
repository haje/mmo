﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BulletSharp;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public interface IBulletSharpPickable
    {
        // Should assign itself as user object in the collision object.
        CollisionObject CollisionObject { get; }
    }
}
