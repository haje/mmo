﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using BulletSharp;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public class BulletSharpRayPicker : Picker, IDisposable
    {
        DefaultCollisionConfiguration collisionConfiguration;
        CollisionDispatcher dispatcher;
        BroadphaseInterface overlappingPairCache;
        SequentialImpulseConstraintSolver solver;
        DiscreteDynamicsWorld dynamicsWorld;
        float maxDistance;
        List<Pickable> pickableList;

        public BulletSharpRayPicker(float maxDistance)
        {
            collisionConfiguration = new DefaultCollisionConfiguration();
            dispatcher = new CollisionDispatcher(collisionConfiguration);
            overlappingPairCache = new DbvtBroadphase();
            solver = new SequentialImpulseConstraintSolver();
            dynamicsWorld = new DiscreteDynamicsWorld(dispatcher, overlappingPairCache,
                solver, collisionConfiguration);
            this.maxDistance = maxDistance;
            pickableList = new List<Pickable>();
        }

        public void Dispose()
        {
            RemoveTargetAll();
            if (collisionConfiguration != null) collisionConfiguration.Dispose(); collisionConfiguration = null;
            if (dispatcher != null) dispatcher.Dispose(); dispatcher = null;
            if (overlappingPairCache != null) overlappingPairCache.Dispose(); overlappingPairCache = null;
            if (solver != null) solver.Dispose(); solver = null;
            if (dynamicsWorld != null) dynamicsWorld.Dispose(); dynamicsWorld = null;
        }

        public override void AddTarget(Pickable pickable)
        { 
            if (pickable is IBulletSharpPickable)
            {
                var bsPickable = pickable as IBulletSharpPickable;
                dynamicsWorld.AddCollisionObject(bsPickable.CollisionObject);
                pickableList.Add(pickable);
            }
            else
            {
                throw new ArgumentException("BulletSharpRayPicker에 추가될 물체는 IBulletSharpPickable을 구현해야 합니다.");
            }
        }

        public override void RemoveTarget(Pickable pickable)
        {
            if (pickable is IBulletSharpPickable)
            {
                var bsPickable = pickable as IBulletSharpPickable;
                dynamicsWorld.RemoveCollisionObject(bsPickable.CollisionObject);
                pickableList.Remove(pickable);
            }
            else
            {
                throw new ArgumentException("BulletSharpRayPicker에서 제거될 물체는 IBulletSharpPickable을 구현해야 합니다.");
            }
        }

        public void RemoveTargetAll()
        {
            foreach (IBulletSharpPickable pickable in pickableList)
            {
                dynamicsWorld.RemoveCollisionObject(pickable.CollisionObject);
            }
            pickableList.Clear();
        }

        public void UpdateCollisionObjects()
        {
            foreach (IBulletSharpPickable pickable in pickableList)
            {
                if (pickable is DynamicPickable)
                {
                    var dynPickable = pickable as DynamicPickable;
                    bool hasUpdated = dynPickable.UpdateCollisionObject();
                    if (hasUpdated)
                    {
                        dynamicsWorld.RemoveCollisionObject(pickable.CollisionObject);
                        dynamicsWorld.AddCollisionObject(pickable.CollisionObject);
                    }
                }
            }
        }

        protected override PickingResult DoPick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType)
        {
            Vector3 endPt = origin + direction * maxDistance;
            AllHitsRayResultCallback rayCallback = new AllHitsRayResultCallback(origin, endPt);
            dynamicsWorld.RayTest(origin, endPt, rayCallback);
            PickingResult result = PickingResult.NothingPicked;
            if (rayCallback.HasHit)
            {
                var colHitPtTuples = rayCallback.CollisionObjects.Zip(rayCallback.HitPointWorld,
                    (col, hitPt) => Tuple.Create(col, hitPt));
                foreach (var colHitPtTuple in colHitPtTuples)
                {
                    var obj = (Pickable)colHitPtTuple.Item1.UserObject;
                    var hitPt = colHitPtTuple.Item2;
                    if (!excludeList.Contains(obj) && allowedType.Contains(obj.Type))
                    {
                        // Select only the closest
                        if (!result.IsPicked || 
                            ((hitPt - origin).Length < (result.CollidePosition - origin).Length))
                            result = new PickingResult(true, hitPt, obj.Type, obj.Owner);
                    }
                }
            }
            return result;
        }

        // TODO: make use of allowedType
        protected List<PickingResult> DoPickAll(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType)
        {
            Vector3 endPt = origin + direction * maxDistance;
            AllHitsRayResultCallback rayCallback = new AllHitsRayResultCallback(origin, endPt);
            dynamicsWorld.RayTest(origin, endPt, rayCallback);
            List<PickingResult> resultList;
            if (rayCallback.HasHit)
            {
                resultList = rayCallback.CollisionObjects.Zip(rayCallback.HitPointWorld,
                    (obj, hitPt) => new PickingResult(true, hitPt, 
                        ((Pickable)obj.UserObject).Type, 
                        ((Pickable)obj.UserObject).Owner)).ToList();
            }
            else
            {
                resultList = new List<PickingResult>();
            }
            return resultList;
        }
    }
}
