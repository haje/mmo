﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public abstract class Picker
    {
        public abstract void AddTarget(Pickable pickable);
        public abstract void RemoveTarget(Pickable pickable);

        public PickingResult Pick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType)
        {
            return DoPick(origin, direction, excludeList, allowedType);
        }

        protected abstract PickingResult DoPick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType);

        static Picker()
        {
            allPickableTypes = new HashSet<PickableType>();
            var pickables = Enum.GetValues(typeof(PickableType));
            foreach (var p in pickables)
                allPickableTypes.Add((PickableType)p);
        }

        static HashSet<PickableType> allPickableTypes;
        public static ISet<PickableType> AllPickableTypes
        {
            get
            {
                return allPickableTypes;
            }
        }
    }
}

