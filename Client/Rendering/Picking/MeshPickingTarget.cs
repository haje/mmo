﻿using BulletSharp;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public class MeshPickingTarget : DynamicPickable, IBulletSharpPickable
    {
        public MeshPickingTarget(MeshDescriptor boundTarget, PickableType type, object owner)
            : base(boundTarget, owner, type)
        {
            collisionObject = new CollisionObject();
            var meshData = new TriangleIndexVertexArray(boundTarget.MeshIndices, boundTarget.MeshVertices);
            var meshShape = new BvhTriangleMeshShape(meshData, true);
            collisionObject.CollisionShape = new ScaledBvhTriangleMeshShape(meshShape, boundTarget.ScaleFactor);
            collisionObject.WorldTransform = boundTarget.ModelMatrix.ClearScale();
            collisionObject.UserObject = this;
        }

        public CollisionObject CollisionObject
        {
            get
            {
                return collisionObject;
            }
        }

        protected override void updateCollisionObject()
        {
            collisionObject.WorldTransform = this.boundTarget.ModelMatrix.ClearScale();
        }

        protected CollisionObject collisionObject;
    }
}
