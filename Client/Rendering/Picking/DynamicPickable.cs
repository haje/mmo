﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public abstract class DynamicPickable : Pickable
    {
        protected readonly WorldSpaceEntity boundTarget;
        protected bool isCollisionObjectDirty;

        public DynamicPickable(WorldSpaceEntity boundTarget, object owner, PickableType type)
            : base(owner, type)
        {
            this.boundTarget = boundTarget;
        }

        public bool UpdateCollisionObject()
        {
            bool hasUpdated = false;
            if (isCollisionObjectDirty)
            {
                updateCollisionObject();
                hasUpdated = true;
            }
            isCollisionObjectDirty = false;
            return hasUpdated;
        }

        public void InvalidateCollisionObject()
        {
            isCollisionObjectDirty = true;
        }

        protected abstract void updateCollisionObject();
    }
}
