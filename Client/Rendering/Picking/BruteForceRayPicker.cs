﻿using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering.Picking
{
    public class BruteForceRayPicker : Picker
    {
        public override void AddTarget(Pickable pickable)
        {
            if (pickable is IRecursivePickable)
            {
                var recursivePickable = pickable as IRecursivePickable;
                pickingTargetList.Add(recursivePickable);
            }
            else
            {
                throw new ArgumentException("BruteForceRayPicker에 추가될 물체는 IRecursivePickable을 구현해야 합니다.");
            }
        }

        public override void RemoveTarget(Pickable pickable)
        {
            if (pickable is IRecursivePickable)
            {
                var recursivePickable = pickable as IRecursivePickable;
                pickingTargetList.Remove(recursivePickable);
            }
            else
            {
                throw new ArgumentException("BruteForceRayPicker에서 제거될 물체는 IRecursivePickable을 구현해야 합니다.");
            }
        }

        protected override PickingResult DoPick(Vector3 origin, Vector3 direction, ISet<Pickable> excludeList, ISet<PickableType> allowedType)
        {
            PickingResult result = PickingResult.NothingPicked;
            float distanceToResult = float.MaxValue;
            foreach (var p in pickingTargetList)
            {
                var pResult = p.Pick(origin, direction, excludeList, allowedType);
                if (!pResult.IsPicked) continue;

                // 선택된 물체 중 가까운 것을 결과로 선택
                float distanceToP = (origin - pResult.CollidePosition).Length;
                if (distanceToP < distanceToResult)
                {
                    distanceToResult = distanceToP;
                    result = pResult;
                }
            }
            return result;
        }

        List<IRecursivePickable> pickingTargetList = new List<IRecursivePickable>();
    }
}
