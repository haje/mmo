﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Drawing;

namespace HAJE.MMO.Client.Rendering
{
    public class UIRenderer : IDisposable
    {
        VertexType.VertexScreenPositionTexture[] vertices = new VertexType.VertexScreenPositionTexture[4];
        Buffer.VertexArray vertArray;

        public UIRenderer()
        {
            int[] indices = new int[]
            {
               0,1,2,
               1,2,3
            };

            vertices[0].TextureUV = new Vector2(0, 1);
            vertices[0].Position = new Vector2(0, 0);

            vertices[1].TextureUV = new Vector2(1, 1);
            vertices[1].Position = new Vector2(1, 0);

            vertices[2].TextureUV = new Vector2(0, 0);
            vertices[2].Position = new Vector2(0, 1);

            vertices[3].TextureUV = new Vector2(1, 0);
            vertices[3].Position = new Vector2(1, 1);

            vertArray = Buffer.VertexArray.CreateArray
                (
                vertices, 
                BufferUsageHint.StaticDraw,
                indices
                );
        }

        public void Dispose()
        {
            if (vertArray != null) vertArray.Dispose(); vertArray = null;
        }

        public void DrawLabel(RectangleF area, Texture labelTexture, ref Matrix3 transform, Color4 tint, Vector2 uvMin, Vector2 uvMax)
        {
            Matrix3 translMat = Matrix3.Identity;
            translMat.M31 = area.Location.X;
            translMat.M32 = area.Location.Y;
            Matrix3 scaleMat = Matrix3.CreateScale(area.Width, area.Height, 1.0f);
            Matrix3 mat = Matrix3.Mult(scaleMat, translMat);
            mat = Matrix3.Mult(mat, transform);

            ShaderProvider.Label.Use(ref mat, GameSystem.Viewport, tint, labelTexture);
            vertArray.Bind();
            GLHelper.DrawTriangles(vertArray.ElemCount);
        }

        public void DrawTexture(RectangleF area, Texture texture, ref Matrix3 transform, Color4 tint, Vector2 uvMin, Vector2 uvMax)
        {
            Matrix3 translMat = Matrix3.Identity;
            translMat.M31 = area.Location.X;
            translMat.M32 = area.Location.Y;
            Matrix3 scaleMat = Matrix3.CreateScale(area.Width, area.Height, 1.0f);
            Matrix3 mat = Matrix3.Mult(scaleMat, translMat);
            mat = Matrix3.Mult(mat, transform);

            ShaderProvider.ScreenSpaceTexture.Use(ref mat, GameSystem.Viewport, tint, texture, uvMin, uvMax);
            vertArray.Bind();
            GLHelper.DrawTriangles(vertArray.ElemCount);
        }
        
        public void DrawRectangle(RectangleF area, ref Matrix3 transform, Color4 color)
        {
            Matrix3 translMat = Matrix3.Identity;
            translMat.M31 = area.Location.X;
            translMat.M32 = area.Location.Y;
            Matrix3 scaleMat = Matrix3.CreateScale(area.Width, area.Height, 1.0f);
            Matrix3 mat = Matrix3.Mult(scaleMat, translMat);
            mat = Matrix3.Mult(mat, transform);

            ShaderProvider.ScreenSpaceTexture.Use(ref mat, GameSystem.Viewport, color, null, Vector2.Zero, Vector2.One);
            vertArray.Bind();
            GLHelper.DrawTriangles(vertArray.ElemCount);
        }
        
        public void DrawBorderedRectangle(RectangleF area, ref Matrix3 transform, float borderWidth, Color4 borderColor, Color4 fillColor)
        {
            RectangleF lowerArea = new RectangleF(new PointF(area.Left, area.Top), new SizeF(area.Width, borderWidth));
            RectangleF upperArea = new RectangleF(new PointF(area.Left, area.Bottom - borderWidth), new SizeF(area.Width, borderWidth));
            RectangleF leftArea = new RectangleF(new PointF(area.Left, area.Top + borderWidth), new SizeF(borderWidth, area.Height - 2 * borderWidth));
            RectangleF rightArea = new RectangleF(new PointF(area.Right - borderWidth, area.Top + borderWidth), new SizeF(borderWidth, area.Height - 2 * borderWidth));
            RectangleF innerArea = area;
            innerArea.Inflate(-borderWidth, -borderWidth);

            DrawRectangle(upperArea, ref transform, borderColor);
            DrawRectangle(lowerArea, ref transform, borderColor);
            DrawRectangle(leftArea, ref transform, borderColor);
            DrawRectangle(rightArea, ref transform, borderColor);
            DrawRectangle(innerArea, ref transform, fillColor);
        }
    }
}
