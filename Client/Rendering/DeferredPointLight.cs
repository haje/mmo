﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public class DeferredPointLight : Light
    {
        protected float radius;

        public DeferredPointLight(Vector3 position, Vector3 intensity, float radius, bool affectedByShadow = false)
            : base(position, intensity)
        {
            this.radius = radius;
            this.AffectedByShadow = affectedByShadow;
        }

        public float Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        public readonly bool AffectedByShadow;
    }
}
