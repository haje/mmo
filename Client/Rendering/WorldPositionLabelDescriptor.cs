﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public class WorldPositionLabelDescriptor : VisualObjectDescriptor<WorldPositionLabel>
    {
        public Texture Texture
        {
            get
            {
                return drawable.Texture;
            }
        }

        protected WorldPositionLabelDescriptor(WorldPositionLabel wpl, TextureBlendTypes blendType)
            : base(Vector3.Zero, Vector3.UnitZ, Vector3.UnitY, wpl, blendType)
        {

        }

        public static WorldPositionLabelDescriptor FromParticleSystem(WorldPositionLabel wpl, TextureBlendTypes blendType)
        {
            return new WorldPositionLabelDescriptor(wpl, blendType);
        }
    }
}
