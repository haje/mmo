﻿using OpenTK;

namespace HAJE.MMO.Client.Rendering
{
    public class Camera : WorldSpaceEntity
    {
        Radian fovy;
        float zNear;
        float zFar;
        public float zAlpha;

        protected Camera(Vector3 position, Vector3 up, Vector3 lookingAt, Radian fovy, float zNear, float zFar)
            : base (position, lookingAt - position, up)
        {
            this.fovy = fovy;
            this.zNear = zNear;
            this.zFar = zFar;
            this.zAlpha = 0;
        }

        /// <summary>
        /// Create a camera based on the parameters given.
        /// </summary>
        /// <param name="position">Position in the world space of the camera</param>
        /// <param name="lookingAt">Position in the world space of the point that the camera is looking at</param>
        /// <param name="fovy">Field of view in radian</param>
        /// <param name="up">Up vector. Default is (0, 1, 0)</param>
        /// <param name="zNear">Near plane. Default is 0.1</param>
        /// <param name="zFar">Far plane. Default is 100</param>
        /// <returns>Newly created camera based on the parameters</returns>
        public static Camera CreateCamera(Vector3 position, Vector3 lookingAt, Radian fovy, 
            Vector3? up = null, float zNear = 0.1f, float zFar = 100.0f)
        {
            return new Camera(position, up ?? new Vector3(0, 1, 0), lookingAt, fovy, zNear, zFar);
        }
        
        public Matrix4 ProjectionMatrix
        {
            get
            {
                return Matrix4.CreatePerspectiveFieldOfView(fovy, GameSystem.Viewport.X / GameSystem.Viewport.Y, zNear, zFar);
            }
        }

        public Matrix4 ViewMatrix
        {
            get
            {
                return Matrix4.LookAt(Position, Position + LookingAtDirection, UpVector);
            }
        }

        public float Far
        {
            get
            {
                return zFar;
            }
        }

        public float Near
        {
            get
            {
                return zNear;
            }
        }
    }
}
