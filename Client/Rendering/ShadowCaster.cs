﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client.Rendering
{
    public class ShadowCaster
    {
        public ShadowCaster(Vector3 lightVector, Vector3 centerPos)
        {
            this.lightVector = lightVector.Normalized();
            this.CenterPos = centerPos;
        }

        public Matrix4 ProjectionMatrix
        {
            get
            {
                return Matrix4.CreateOrthographic
                    (
                    halfExtent * 2, halfExtent * 2, 
                    -halfExtent * 2, halfExtent * 2
                    );
            }
        }

        public Matrix4 ViewMatrix
        {
            get
            {
                return Matrix4.LookAt(CenterPos - lightVector * halfExtent, CenterPos, Vector3.UnitZ);
            }
        }

        readonly float halfExtent = GamePlayConstant.SightRange;
        public Vector3 CenterPos;
        public Vector3 lightVector;
    }
}
