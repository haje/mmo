﻿using FileFormatWavefront;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.Rendering
{
    public enum MeshType
    {
        TrianglePyramid = 0,
        SquarePyramid = 1,
        Cube = 2,

        Xaxis,
        Yaxis,
        Zaxis
    }

    public class MeshProvider : IDisposable
    {
        public MeshProvider()
        {
        }

        private Mesh trianglePyramidMesh;
        public Mesh TrianglePyramidMesh
        {
            get
            {
                if (trianglePyramidMesh == null)
                {
                    trianglePyramidMesh = CreateTrianglePyramidMesh();
                }

                return trianglePyramidMesh;
            }
        }

        private Mesh sqaurePyramid;
        public Mesh SquarePyramid
        {
            get
            {
                if (sqaurePyramid == null)
                {
                    sqaurePyramid = CreateSquarePyramid();
                }

                return sqaurePyramid;
            }
        }

        private Mesh cube;
        public Mesh Cube
        {
            get
            {
                if(cube == null)
                {
                    cube = CreateCube(Color4.White);
                }

                return cube;
            }
        }

        public Mesh CreateMeshByType(MeshType type)
        {
            switch(type)
            {
                case MeshType.TrianglePyramid:
                    return this.TrianglePyramidMesh;
                case MeshType.SquarePyramid:
                    return this.SquarePyramid;
                case MeshType.Cube:
                    return this.Cube;
                case MeshType.Xaxis:
                    return CreateCube(Color4.Red);
                case MeshType.Yaxis:
                    return CreateCube(Color4.Green);
                case MeshType.Zaxis:
                    return CreateCube(Color4.Blue);
                default:
                    return null;
            }
        }

        private Mesh CreateTrianglePyramidMesh()
        {
            return CreateCorn(3);
        }

        private Mesh CreateCorn(int bottomCount)
        {
            Rendering.VertexType.VertexWorldPositionNormalColor[] vertices = new Rendering.VertexType.VertexWorldPositionNormalColor[bottomCount + bottomCount * 3];
            int[] indices = new int[(bottomCount - 2) * 3 + bottomCount * 3];

            Vector3 top = new Vector3(1, 0, 0);
            Vector3[] bottom = new Vector3[bottomCount];
            int cnt = 0;
            for (int i = 0; i < bottomCount; i++)
            {
                Radian angle = i * Radian.Pi * 2 / bottomCount;
                bottom[i] = new Vector3(0, Radian.Cos(angle), Radian.Sin(angle));
                vertices[cnt++].Position = bottom[i];
            }

            vertices[cnt++].Position = top;
            vertices[cnt++].Position = bottom[bottomCount - 1];
            vertices[cnt++].Position = bottom[0];

            for (int i = 1; i < bottomCount; i++)
            {
                vertices[cnt++].Position = top;
                vertices[cnt++].Position = bottom[i - 1];
                vertices[cnt++].Position = bottom[i];
            }

            for (int i = 2; i < bottomCount; i++)
            {
                var v1 = vertices[i - 2].Position;
                var v2 = vertices[i - 1].Position;
                var v3 = vertices[i].Position;
                var d1 = Vector3.Normalize(v2 - v1);
                var d2 = Vector3.Normalize(v3 - v1);
                var normal = Vector3.Cross(d2, d1);
                normal.Normalize();
                vertices[i - 2].Normal = normal;
                vertices[i - 1].Normal = normal;
                vertices[i].Normal = normal;
            }

            for (int i = 0; i < bottomCount; i++)
            {
                var v1 = vertices[bottomCount + i * 3].Position;
                var v2 = vertices[bottomCount + i * 3 + 1].Position;
                var v3 = vertices[bottomCount + i * 3 + 2].Position;
                var d1 = Vector3.Normalize(v2 - v1);
                var d2 = Vector3.Normalize(v3 - v1);
                var normal = Vector3.Cross(d2, d1);
                normal.Normalize();
                vertices[bottomCount + i * 3].Normal = normal;
                vertices[bottomCount + i * 3 + 1].Normal = normal;
                vertices[bottomCount + i * 3 + 2].Normal = normal;
            }

            for (int i = 0; i < vertices.Length; i++)
                vertices[i].Color = Color4.White;

            cnt = 0;

            if(bottomCount == 3)
            {
                indices[cnt++] = 0;
                indices[cnt++] = 1;
                indices[cnt++] = 2;
            }
            else if (bottomCount == 4)
            {
                indices[cnt++] = 0;
                indices[cnt++] = 1;
                indices[cnt++] = 2;
                indices[cnt++] = 0;
                indices[cnt++] = 2;
                indices[cnt++] = 3;
            }

            cnt = bottomCount;

            for (int i = (bottomCount - 2) * 3; i < indices.Length; i++)
                indices[i] = cnt++;

            return new Mesh(vertices, indices);
        }

        private Mesh CreateSquarePyramid()
        {
            return CreateCorn(4);
        }

        private Mesh CreateCube(Color4 color)
        {
            Rendering.VertexType.VertexWorldPositionNormalColor[] vertices = new Rendering.VertexType.VertexWorldPositionNormalColor[60];

            Vector3[] frontVertices = new Vector3[4]
            {
                new Vector3(0.5f, 0.5f, 0.5f),
                new Vector3(0.5f, -0.5f, 0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
            };

            Vector3[] backVertices = new Vector3[4]
            {
                new Vector3(-0.5f, 0.5f, 0.5f),
                new Vector3(-0.5f, -0.5f, 0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(-0.5f, 0.5f, -0.5f),
            };

            int[] indices = new int[60];

            int cnt = 0;

            //front
            vertices[cnt++].Position = frontVertices[0];
            vertices[cnt++].Position = frontVertices[1];
            vertices[cnt++].Position = frontVertices[2];

            vertices[cnt++].Position = frontVertices[2];
            vertices[cnt++].Position = frontVertices[3];
            vertices[cnt++].Position = frontVertices[0];

            //back
            vertices[cnt++].Position = backVertices[0];
            vertices[cnt++].Position = backVertices[2];
            vertices[cnt++].Position = backVertices[1];

            vertices[cnt++].Position = backVertices[0];
            vertices[cnt++].Position = backVertices[3];
            vertices[cnt++].Position = backVertices[2];

            //right
            vertices[cnt++].Position = backVertices[0];
            vertices[cnt++].Position = frontVertices[0];
            vertices[cnt++].Position = frontVertices[3];

            vertices[cnt++].Position = backVertices[0];
            vertices[cnt++].Position = frontVertices[3];
            vertices[cnt++].Position = backVertices[3];

            //left
            vertices[cnt++].Position = backVertices[1];
            vertices[cnt++].Position = frontVertices[2];
            vertices[cnt++].Position = frontVertices[1];

            vertices[cnt++].Position = backVertices[1];
            vertices[cnt++].Position = backVertices[2];
            vertices[cnt++].Position = frontVertices[2];

            //top
            vertices[cnt++].Position = backVertices[0];
            vertices[cnt++].Position = backVertices[1];
            vertices[cnt++].Position = frontVertices[1];

            vertices[cnt++].Position = backVertices[0];
            vertices[cnt++].Position = frontVertices[1];
            vertices[cnt++].Position = frontVertices[0];

            //bottom
            vertices[cnt++].Position = backVertices[2];
            vertices[cnt++].Position = frontVertices[3];
            vertices[cnt++].Position = frontVertices[2];

            vertices[cnt++].Position = backVertices[2];
            vertices[cnt++].Position = backVertices[3];
            vertices[cnt++].Position = frontVertices[3];

            for (int i = 0; i < vertices.Length / 3; i++)
            {
                var v1 = vertices[i * 3].Position;
                var v2 = vertices[i * 3 + 1].Position;
                var v3 = vertices[i * 3 + 2].Position;
                var normal = Vector3.Cross(v2 - v1, v3 - v1).Normalized();
                vertices[i * 3].Normal = normal;
                vertices[i * 3 + 1].Normal = normal;
                vertices[i * 3 + 2].Normal = normal;
            }

            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Color = color;
                indices[i] = i;
            }

            return new Mesh(vertices, indices);
        }

        public Mesh GetMesh(string path)
        {
            Mesh mesh;
            if(meshCacheFromFile.TryGetValue(path, out mesh))
            {
                return mesh;
            }
            
            mesh = Rendering.Mesh.FromWavefront(path);
            meshCacheFromFile.Add(path, mesh);

            return mesh;
        }

        //Texture가 입혀지면 사용할 함수
        public Mesh GetMesh(string path, string texturePath)
        {
            Mesh mesh;
            if (meshCacheFromFile.TryGetValue(path, out mesh))
            {
                return mesh;
            }
            
            mesh = Rendering.Mesh.FromWavefront(path);
            meshCacheFromFile.Add(path, mesh);

            return mesh;
        }

        public void Dispose()
        {
            foreach(Mesh m in meshCacheFromFile.Values)
            {
                m.Dispose();
            }
            meshCacheFromFile.Clear();
        }

        Dictionary<string, Mesh> meshCacheFromFile = new Dictionary<string, Mesh>();
    }
}
