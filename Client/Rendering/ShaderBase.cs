﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace HAJE.MMO.Client.Rendering
{
    public abstract class ShaderBase : IDisposable
    {
        public ShaderBase(string vSource, string fSource, string gSource = null)
        {
            CreateVertexShader(vSource);
            CreateFragmentShader(fSource);
            if (gSource != null)
                CreateGeometryShader(gSource);
            else
                geometryShader = 0;
            LinkShader();
            GetUniformLocation();
        }

        #region private Helpers

        private int CompileShader(int shader, string file)
        {
            string errorInfo;
            int errorCode;

            using (StreamReader source = new StreamReader(Path.Combine(Environment.CurrentDirectory, file)))
            {
                string data = source.ReadToEnd();
                GL.ShaderSource(shader, data);
            }

            GL.CompileShader(shader);
            GL.GetShaderInfoLog(shader, out errorInfo);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out errorCode);

            if (errorCode != 1)
                throw new ApplicationException(errorInfo);

            return shader;
        }

        void CreateVertexShader(string source)
        {
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            CompileShader(vertexShader, source);
        }

        void CreateGeometryShader(string source)
        {
            geometryShader = GL.CreateShader(ShaderType.GeometryShader);
            CompileShader(geometryShader, source);
        }

        void CreateFragmentShader(string source)
        {
            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            CompileShader(fragmentShader, source);
        }

        void LinkShader()
        {
            string errorInfo;
            int errorCode;

            shaderProgram = GL.CreateProgram();
            GL.AttachShader(shaderProgram, vertexShader);
            GL.AttachShader(shaderProgram, fragmentShader);
            if (geometryShader != 0)
                GL.AttachShader(shaderProgram, geometryShader);
            BindFragDataLocation();
            GL.LinkProgram(shaderProgram);

            GL.GetProgramInfoLog(shaderProgram, out errorInfo);
            GL.GetProgram(shaderProgram, GetProgramParameterName.LinkStatus, out errorCode);
            if (errorCode != 1)
                throw new ApplicationException(errorInfo);
        }

        #endregion

        protected abstract void GetUniformLocation();

        protected virtual void BindFragDataLocation()
        {
            GL.BindFragDataLocation(shaderProgram, 0, "colorOut");
        }

        public void Use()
        {
            GL.UseProgram(shaderProgram);
        }

        public void Dispose()
        {
            if (vertexShader != 0) GL.DeleteShader(vertexShader); vertexShader = 0;
            if (geometryShader != 0) GL.DeleteShader(geometryShader); geometryShader = 0;
            if (fragmentShader != 0) GL.DeleteShader(fragmentShader); fragmentShader = 0;
            if (shaderProgram != 0) GL.DeleteProgram(shaderProgram); shaderProgram = 0;
        }

        public int ShaderProgram
        {
            get
            {
                return shaderProgram;
            }
        }

        int vertexShader;
        int geometryShader;
        int fragmentShader;
        protected int shaderProgram;
    }
}
