﻿using OpenTK;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace HAJE.MMO.Client.Rendering
{
    public class DoodadMeshInformationProvider
    {
        const string path = "Resource/XML/DoodadMesh.xml";

        private static DoodadMeshInformationProvider instance;
        public static DoodadMeshInformationProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DoodadMeshInformationProvider();
                }
                return instance;
            }
        }

        private DoodadMeshInformationProvider()
        {
            Debug.Assert(File.Exists(path));
            XmlSerializer serializer = new XmlSerializer(typeof(DMIXmlType.DoodadMeshInfomation));
            using (FileStream fs = File.OpenRead(path))
            {
                var xml = (DMIXmlType.DoodadMeshInfomation)serializer.Deserialize(fs);
                doodad = new Dictionary<string, DoodadMeshInformation>();
                foreach (var d in xml.SettingList)
                {
                    var info = new DoodadMeshInformation(d);
                    doodad.Add(info.Name, info);
                }
            }
        }

        public DoodadMeshInformation this[string doodadName]
        {
            get
            {
                return doodad[doodadName];
            }
        }

        Dictionary<string, DoodadMeshInformation> doodad;
    }

    public class DoodadMeshInformation
    {
        public DoodadMeshInformation(DMIXmlType.Doodad meshInfo)
        {
            this.Name = meshInfo.name;
            this.Path = "Resource/Model/Terrain/" + Name + ".obj";
            this.Scale = meshInfo.scale;
            this.Radius = meshInfo.radius;
            this.Height = meshInfo.height;
            this.Anchor = new Vector3(meshInfo.Anchor.x, meshInfo.Anchor.y, meshInfo.Anchor.z);
        }

        public readonly string Name;
        public readonly string Path;
        public readonly float Scale;
        public readonly float Radius;
        public readonly float Height;
        public readonly Vector3 Anchor;
    }
}

namespace HAJE.MMO.Client.Rendering.DMIXmlType
{
    public class V3
    {
        [XmlAttribute]
        public float x;

        [XmlAttribute]
        public float y;

        [XmlAttribute]
        public float z;
    }

    public class Doodad
    {
        [XmlAttribute]
        public string name;

        [XmlAttribute]
        public float scale;

        [XmlAttribute]
        public float radius;

        [XmlAttribute]
        public float height;

        public V3 Anchor;
    }

    public class DoodadMeshInfomation
    {
        public List<Doodad> SettingList;
    }
}
