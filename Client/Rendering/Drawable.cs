﻿using System;

namespace HAJE.MMO.Client.Rendering
{
    public abstract class Drawable : IDisposable
    {
        protected Buffer.VertexArray vertArray;

        public virtual void Draw()
        {
            vertArray.Bind();
            GLHelper.DrawTriangles(vertArray.ElemCount);
        }

        public void Dispose()
        {
            if (vertArray != null) vertArray.Dispose(); vertArray = null;
        }
    }
}
