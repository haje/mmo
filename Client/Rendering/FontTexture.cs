﻿using System.Drawing;

namespace HAJE.MMO.Client.Rendering
{
    public static class FontTexture
    {
        public static Texture Create(string text, Font font)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            using (Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle))
            {
                SizeF size = g.MeasureString(text, font);

                using (Bitmap bitmap = new Bitmap((int)size.Width, (int)size.Height))
                {
                    using (Graphics gfx = Graphics.FromImage(bitmap))
                    {
                        PointF pt = new PointF(0, 0);
                        gfx.Clear(System.Drawing.Color.Black);
                        gfx.DrawString(text, font, Brushes.White,
                            new RectangleF(0, 0, size.Width, size.Height));
                    }
                    return Texture.CreateFromBitmap(bitmap);
                }
            }
        }
        public static Texture CreateForWpl(string text, Font font)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            using (Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle))
            {
                SizeF size = g.MeasureString(text, font);

                using (Bitmap bitmap = new Bitmap((int)size.Width, (int)size.Height))
                {
                    using (Graphics gfx = Graphics.FromImage(bitmap))
                    {
                        PointF pt = new PointF(0, 0);
                        gfx.Clear(System.Drawing.Color.Black);
                        gfx.DrawString(text, font, Brushes.White,
                            new RectangleF(0, 0, size.Width, size.Height));
                    }

                    for (int x = 0; x < bitmap.Width; x++)
                        for (int y = 0; y < bitmap.Height; y++)
                        {
                            Color c = bitmap.GetPixel(x, y);
                            int intensity = (c.R + c.G + c.B) / 3;

                            bitmap.SetPixel(x, y, Color.FromArgb(intensity, c.R, c.G, c.B));
                        }

                    return Texture.CreateFromBitmap(bitmap);
                }
            }
        }
    }
}
