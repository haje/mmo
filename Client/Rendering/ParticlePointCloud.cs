﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HAJE.MMO.Client.Rendering
{
    public class ParticlePointCloud : Drawable
    {
        public static int maxVertexNum = 50000;
        protected int vertexToDraw;
        VertexType.VertexPositionRotationUVminmaxBBColor[] vertices;

        public Texture TexAtlas
        {
            get;
            protected set;
        }

        protected ParticlePointCloud(EffectSystem.EffectSystem eSys, TextureBlendTypes blendType)
        {
            if (blendType == TextureBlendTypes.Additive)
                eSys.AdditiveParticlesUpdated += UpdateFromParticleSystem;
            else if (blendType == TextureBlendTypes.Attenuative)
                eSys.AttenuativeParticlesUpdated += UpdateFromParticleSystem;

            vertices = new VertexType.VertexPositionRotationUVminmaxBBColor[maxVertexNum];
            int[] indices = new int[maxVertexNum];
            for(int indexIter = 0; indexIter < maxVertexNum; ++indexIter)
            {
                vertices[indexIter] = new VertexType.VertexPositionRotationUVminmaxBBColor();
                indices[indexIter] = indexIter;
            }
            vertArray = Buffer.VertexArray.CreateArray
                (
                vertices,
                BufferUsageHint.DynamicDraw,
                indices
                );
            TexAtlas = eSys.Texture;
            vertexToDraw = 0;
        }

        private void UpdateFromParticleSystem(ReadOnlyCollection<EffectSystem.Particle> visibleParticles)
        {
            int vertIter = 0;
            foreach (var particle in visibleParticles)
            {
                vertices[vertIter].Position = particle.Position;
                vertices[vertIter].Rotation = particle.Rotation;
                vertices[vertIter].Color = particle.Color;
                vertices[vertIter].UVMax = particle.UVMax;
                vertices[vertIter].UVMin = particle.UVMin;
                // TODO : MUST FIX SHADER!
                vertices[vertIter].BoundingBox = particle.BoundingBox * 3.5f;
                vertIter++;
            }
            vertArray.UpdateVertex(vertices);
            vertexToDraw = visibleParticles.Count;
        }

        public static ParticlePointCloud FromEffectSystem(EffectSystem.EffectSystem eSys, TextureBlendTypes blendType)
        {
            return new ParticlePointCloud(eSys, blendType);
        }

        public override void Draw()
        {
            vertArray.Bind();
            GLHelper.DrawPoints(vertexToDraw);
        }
    }
}
