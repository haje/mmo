﻿using HAJE.MMO.Client.Rendering.Shader;

namespace HAJE.MMO.Client.Rendering
{
    public static class ShaderProvider
    {
        public static void UnloadShader()
        {
            if (screenSpaceTexture != null) screenSpaceTexture.Dispose(); screenSpaceTexture = null;
            if (twoRectangle != null) twoRectangle.Dispose(); twoRectangle = null;
            if (worldSpaceForward != null) worldSpaceForward.Dispose(); worldSpaceForward = null;
            if (worldSpaceGrid != null) worldSpaceGrid.Dispose(); worldSpaceGrid = null;
            if (unshaded != null) unshaded.Dispose(); unshaded = null;
            if (light != null) light.Dispose(); light = null;
            if (composite != null) composite.Dispose(); composite = null;
            if (depthOnly != null) depthOnly.Dispose(); depthOnly = null;
            if (shadow != null) shadow.Dispose(); shadow = null;
            if (label != null) label.Dispose(); label = null;
            if (additiveParticle != null) additiveParticle.Dispose(); additiveParticle = null;
            if (additiveMesh != null) additiveMesh.Dispose(); additiveMesh = null;
            if (attenuativeParticle != null) attenuativeParticle.Dispose(); attenuativeParticle = null;
            if (attenuativeMesh != null) attenuativeMesh.Dispose(); attenuativeMesh = null;
            if (attenuativeComposite != null) attenuativeComposite.Dispose(); attenuativeComposite = null;
            if (worldSpaceTexture != null) worldSpaceTexture.Dispose(); worldSpaceTexture = null;
            if (cubemap != null) cubemap.Dispose(); cubemap = null;
            if (postProcessingBlur != null) postProcessingBlur.Dispose(); postProcessingBlur = null;
        }

        static ScreenSpaceTextureShader screenSpaceTexture;
        static RectangleShader twoRectangle;
        static WorldSpaceForwardShader worldSpaceForward;
        static WorldSpaceGridShader worldSpaceGrid;
        static UnshadedShader unshaded;
        static LightShader light;
        static CompositeShader composite;
        static DepthOnlyShader depthOnly;
        static ShadowShader shadow;
        static LabelShader label;
        static AdditiveParticleShader additiveParticle;
        static AdditiveMeshShader additiveMesh;
        static AttenuativeParticleShader attenuativeParticle;
        static AttenuativeMeshShader attenuativeMesh;
        static AttenuativeCompositeShader attenuativeComposite;
        static WorldSpaceTextureShader worldSpaceTexture;
        static CubemapShader cubemap;
        static PostProcessingBlurShader postProcessingBlur;

        public static ScreenSpaceTextureShader ScreenSpaceTexture
        {
            get
            {
                if (screenSpaceTexture == null)
                {
                    screenSpaceTexture = new ScreenSpaceTextureShader();
                }
                return screenSpaceTexture;
            }
        }

        public static RectangleShader TwoRectangle
        {
            get
            {
                if (twoRectangle == null)
                {
                    twoRectangle = new RectangleShader();
                }
                return twoRectangle;
            }
        }

        public static WorldSpaceForwardShader WorldSpaceForward
        {
            get
            {
                if (worldSpaceForward == null)
                {
                    worldSpaceForward = new WorldSpaceForwardShader();
                }
                return worldSpaceForward;
            }
        }

        public static WorldSpaceGridShader WorldSpaceGrid
        {
            get
            {
                if (worldSpaceGrid == null)
                {
                    worldSpaceGrid = new WorldSpaceGridShader();
                }
                return worldSpaceGrid;
            }
        }

        public static UnshadedShader Unshaded
        {
            get
            {
                if (unshaded == null)
                {
                    unshaded = new UnshadedShader();
                }
                return unshaded;
            }
        }

        public static LightShader Light
        {
            get
            {
                if (light == null)
                {
                    light = new LightShader();
                }
                return light;
            }
        }

        public static CompositeShader Composite
        {
            get
            {
                if (composite == null)
                {
                    composite = new CompositeShader();
                }
                return composite;
            }
        }

        public static DepthOnlyShader DepthOnly
        {
            get
            {
                if (depthOnly == null)
                {
                    depthOnly = new DepthOnlyShader();
                }
                return depthOnly;
            }
        }

        public static ShadowShader Shadow
        {
            get
            {
                if (shadow == null)
                {
                    shadow = new ShadowShader();
                }
                return shadow;
            }
        }

        public static LabelShader Label
        {
            get
            {
                if (label == null)
                {
                    label = new LabelShader();
                }
                return label;
            }
        }

        public static AdditiveParticleShader AdditiveParticle
        {
            get
            {
                if (additiveParticle == null)
                {
                    additiveParticle = new AdditiveParticleShader();
                }
                return additiveParticle;
            }
        }

        public static AdditiveMeshShader AdditiveMesh
        {
            get
            {
                if (additiveMesh == null)
                {
                    additiveMesh = new AdditiveMeshShader();
                }
                return additiveMesh;
            }
        }

        public static AttenuativeParticleShader AttenuativeParticle
        {
            get
            {
                if (attenuativeParticle == null)
                {
                    attenuativeParticle = new AttenuativeParticleShader();
                }
                return attenuativeParticle;
            }
        }

        public static AttenuativeMeshShader AttenuativeMesh
        {
            get
            {
                if (attenuativeMesh == null)
                {
                    attenuativeMesh = new AttenuativeMeshShader();
                }
                return attenuativeMesh;
            }
        }

        public static AttenuativeCompositeShader AttenuativeComposite
        {
            get
            {
                if (attenuativeComposite == null)
                {
                    attenuativeComposite = new AttenuativeCompositeShader();
                }
                return attenuativeComposite;
            }
        }

        public static WorldSpaceTextureShader WorldSpaceTexture
        {
            get
            {
                if (worldSpaceTexture == null)
                {
                    worldSpaceTexture = new WorldSpaceTextureShader();
                }
                return worldSpaceTexture;
            }
        }

        public static CubemapShader Cubemap
        {
            get
            {
                if (cubemap == null)
                {
                    cubemap = new CubemapShader();
                }
                return cubemap;
            }
        }

        public static PostProcessingBlurShader PostProcessingBlur
        {
            get
            {
                if (postProcessingBlur == null)
                {
                    postProcessingBlur = new PostProcessingBlurShader();
                }
                return postProcessingBlur;
            }
        }
    }
}
