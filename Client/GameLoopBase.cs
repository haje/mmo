﻿using System;
using System.Diagnostics;

namespace HAJE.MMO.Client
{
    public abstract class GameLoopBase : IDisposable
    {
        public GameLoopBase(string debugName)
        {
            Status = GameLoopStatus.Created;
            DebugName = debugName;
            CommandList = new CommandList();
        }

        public void Initialize()
        {
            OnInitialize();
            Debug.Assert(Status == GameLoopStatus.Created);
            Status = GameLoopStatus.Initialized;
        }

        protected virtual void OnInitialize()
        {
        }

        public void Run()
        {
            if (Status == GameLoopStatus.Created)
                Initialize();
            OnRun();
            Debug.Assert(Status == GameLoopStatus.Initialized);
            Status = GameLoopStatus.Running;
        }

        protected virtual void OnRun()
        {
        }

        public abstract void Update(Second deltaTime, Input.InputState input);

        public abstract void RenderFrame();

        public void Stop()
        {
            Debug.Assert(Status == GameLoopStatus.Running);
            OnStop();
            Status = GameLoopStatus.Exited;
        }

        protected virtual void OnStop()
        {

        }

        public void Dispose()
        {
            if (Status != GameLoopStatus.Disposed)
                Dispose(true);

            Status = GameLoopStatus.Disposed;
        }

        public virtual void Dispose(bool manual)
        {
            Status = GameLoopStatus.Disposed;
        }

        public CommandList CommandList { get; private set; }
        public GameLoopStatus Status { get; private set; }
        public readonly string DebugName;
    }

    public enum GameLoopStatus
    {
        Created,
        Initialized,
        Running,
        Exited,
        Disposed
    }
}
