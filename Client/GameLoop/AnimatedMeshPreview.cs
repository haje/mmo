﻿using HAJE.MMO.Client.Animation;
using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.GamePlay;
using OpenTK;
using OpenTK.Input;

namespace HAJE.MMO.Client.GameLoop
{
    public class AnimatedMeshPreview : GameLoopBase
    {
        public AnimatedMeshPreview()
            :base("AnimatedMeshTest")
        {

        }

        Camera camera;
        Vector3 camTarget = Vector3.Zero;
        Rendering.Renderer.ForwardRenderer renderer;
        MeshProvider meshProvider = new MeshProvider();
        AnimatedObjectDescriptor descriptor;
        ControlProcessor control;
        MouseCursorLock cursorLock;

        protected override void OnInitialize()
        {
            control = new ControlProcessor();

            camera = Camera.CreateCamera(new Vector3(2, 4, 6), new Vector3(0, 0, 0), (Degree)45.0f);
            renderer = new Rendering.Renderer.ForwardRenderer();
            camTarget = camera.Position;
            cursorLock = new MouseCursorLock();

            descriptor = AnimatedObjectDescriptor.CreateHuman(meshProvider);

            GameSystem.MainForm.MouseWheel += MainForm_MouseWheel;
        }
        
        private void MainForm_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            distance += -e.Delta * 0.1f;
        }

        private void Reload()
        {

        }

        bool isRotating = false;

        public override void Update(Second deltaTime, Input.InputState input)
        {
            if(!input.Keyboard.InputHandled)
            {
                var kbd = input.Keyboard;

                if(kbd.IsDown(Key.F5))
                {
                    descriptor.ReloadMeshInfo(meshProvider);
                }

                if (kbd.IsPressing(Key.AltLeft))
                {
                    isRotating = true;
                }
                else
                {
                    isRotating = false;
                }

                float cameraSpeed = 3.0f;

                var forward = camera.LookingAtDirection;
                forward.Y = 0;
                forward.Normalize();

                if (kbd.IsPressing(Key.W))
                {
                    camTarget += forward * deltaTime * cameraSpeed;
                }

                if (kbd.IsPressing(Key.S))
                {
                    camTarget += -forward * deltaTime * cameraSpeed;
                }

                if (kbd.IsPressing(Key.A))
                {
                    var right = Vector3.Cross(Vector3.UnitY, camera.LookingAtDirection);
                    camTarget += right * deltaTime * cameraSpeed;
                }

                if (kbd.IsPressing(Key.D))
                {
                    var right = Vector3.Cross(Vector3.UnitY, camera.LookingAtDirection);
                    camTarget += -right * deltaTime * cameraSpeed;
                }
                
                camera.Position = camTarget - camera.LookingAtDirection * distance;
                CheckRotate();
            }
        }

        float distance = 1.0f;
        Vector2 sensitivity = new Vector2(0.001f, 0.001f);

        private void CheckRotate()
        {
            var form = GameSystem.MainForm;

            if(isRotating)
            {
                Vector2 deltaVec = -1 * cursorLock.GetMovedDistance(false);
                
                var rotMat = Matrix4.CreateRotationZ(deltaVec.X);
                rotMat = Matrix4.Mult(rotMat, Matrix4.CreateRotationY(deltaVec.Y));

                camera.Rotate(ref rotMat);
            }
        }

        public override void RenderFrame()
        {
            renderer.Begin(camera);
            renderer.Render(descriptor.GetMeshDescriptor());
            renderer.End();
        }

        public override void Dispose(bool manual)
        {
            base.Dispose(manual);
        }
    }
}
