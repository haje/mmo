﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.EffectSystem;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Input;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Client.GameLoop
{
    public class ParticleTest : GameLoopBase
    {
        private Camera cam;
        private Rendering.Renderer.DeferredRenderer renderer;
        private ParticlePointCloudDescriptor ppcDesc;

        private ControlProcessor control;
        private EffectSystem effectSystem;

        private List<Effect> effectList;

        private Vector3 camTarget;
        private float distance;
        private float timeScale;
        private MeshProvider provider;
        private List<MeshDescriptor> meshList;
        private DeferredPointLight light;

        #region .ctor
        public ParticleTest()
            : base("ParticleTest")
        {
            effectSystem = new EffectSystem();
            EffectFactory.Initialize(effectSystem);
            timeScale = 1;

            cam = Rendering.Camera.CreateCamera(new Vector3(2, 4, 6), new Vector3(0, 0, 0), (Degree)(45.0f));
            camTarget = Vector3.Zero;   
            renderer = new Rendering.Renderer.DeferredRenderer((int)GameSystem.Viewport.X, (int)GameSystem.Viewport.Y);
            ppcDesc = Rendering.ParticlePointCloudDescriptor.FromEffectSystem(effectSystem, TextureBlendTypes.Additive);

            control = new ControlProcessor();
            control.ProxyChanged += Control_ProxyChanged;
            distance = 6;
            
            CommandList.AddCommand("timescale", "파티클 시스템의 타임스케일을 변경합니다.", TimeScale);

            #region Drawing Grid
            provider = new MeshProvider();
            Mesh xmesh = provider.CreateMeshByType(MeshType.Xaxis);
            Mesh ymesh = provider.CreateMeshByType(MeshType.Yaxis);
            Mesh zmesh = provider.CreateMeshByType(MeshType.Zaxis);

            meshList = new List<MeshDescriptor>();
            MeshDescriptor md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitX * 15, Vector3.UnitY, Vector3.UnitX, xmesh);
            md.ScaleFactor = new Vector3(30, 0.1f, 0.1f);
            meshList.Add(md);

            md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitY * 15, Vector3.UnitY, Vector3.UnitX, ymesh);
            md.ScaleFactor = new Vector3(0.1f, 30, 0.1f);
            meshList.Add(md);

            md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitZ * 15, Vector3.UnitY, Vector3.UnitX, zmesh);
            md.ScaleFactor = new Vector3(0.1f, 0.1f, 30);
            meshList.Add(md);
            for (int x = 1; x <= 5; x += 1)
            {
                md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitX * 2.5f + Vector3.UnitZ * x, Vector3.UnitX, Vector3.UnitY, xmesh);
                md.ScaleFactor = new Vector3(5f, 0.05f, 0.05f);
                meshList.Add(md);
                
                md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitZ * 2.5f + Vector3.UnitX * x, Vector3.UnitX, Vector3.UnitY, zmesh);
                md.ScaleFactor = new Vector3(0.05f, 0.05f, 5f);
                meshList.Add(md);
            }
            for (int x = 5; x <= 30; x += 5)
            {
                md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitX * 15 + Vector3.UnitZ * x, Vector3.UnitX, Vector3.UnitY, xmesh);
                md.ScaleFactor = new Vector3(30, 0.1f, 0.1f);
                meshList.Add(md);

                md = MeshDescriptor.CreateMeshDescriptor(Vector3.UnitZ * 15 + Vector3.UnitX * x, Vector3.UnitX, Vector3.UnitY, zmesh);
                md.ScaleFactor = new Vector3(0.1f, 0.1f, 30);
                meshList.Add(md);
            }

            light = new DeferredPointLight(new Vector3(10, 10, 10), Vector3.One, 100);
            #endregion
            
            GameSystem.MainForm.UpdateFrame += control.UpdateFrame;
            GameSystem.MainForm.KeyDown += MainForm_KeyDown;
            GameSystem.MainForm.MouseDown += MainForm_MouseDown;
            GameSystem.MainForm.MouseWheel += MainForm_MouseWheel;
        }
        #endregion

        #region Commands
        private void TimeScale(DebugConsole console, String[] args)
        {
            Action error = () => { console.Write("timescale time"); };

            if (args.Length != 2)
                error();
            else
            {
                try
                {
                    timeScale = float.Parse(args[1]);
                }
                catch (Exception)
                {
                    error();
                }
            }
        }
        #endregion
        #region Event Handler
        private void Control_ProxyChanged(Rendering.WorldSpaceEntity proxy)
        {
            var xzVec = proxy.LookingAtDirection.Xz.Normalized();
            cam.LookingAtDirection = proxy.LookingAtDirection;
            cam.Position = camTarget - cam.LookingAtDirection * distance;

            light.Position = cam.Position;
        }

        private WorldSpaceEntity wse;
        private void MainForm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (effectList == null)
                effectList = new List<Effect>();

            if (e.Button == MouseButton.Left)
            {
                wse = MeshDescriptor.CreateMeshDescriptor(Vector3.Zero, Vector3.One, Vector3.UnitY, (new MeshProvider()).Cube);
                effectList.Add(EffectFactory.CreateCastLoop(0, wse));
                effectList.Add(EffectFactory.CreateComboPoint(1, wse));
            }
            else
            {
                foreach (Effect effect in effectList)
                    effect.Kill();
                effectList.Clear();
            }
        }
        private void MainForm_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            distance += -e.Delta * 1f;
        }
        private void MainForm_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                GameSystem.MainForm.Exit();

            if (wse != null)
            {
                if (e.Key == Key.W)
                    wse.Position = new Vector3(wse.Position.X + 1, wse.Position.Y, wse.Position.Z);
                if (e.Key == Key.S)
                    wse.Position = new Vector3(wse.Position.X - 1, wse.Position.Y, wse.Position.Z);
                if (e.Key == Key.A)
                    wse.Position = new Vector3(wse.Position.X, wse.Position.Y, wse.Position.Z - 1);
                if (e.Key == Key.D)
                    wse.Position = new Vector3(wse.Position.X, wse.Position.Y, wse.Position.Z + 1);
            }
        }
        #endregion

        #region overrides
        protected override void OnInitialize()
        {
            base.OnInitialize();
        }

        public override void Update(Second deltaTime, Input.InputState input)
        {
            if (input.Keyboard.IsDown(Key.AltLeft))
            {
                GameSystem.MainForm.MouseMove += control.MouseMove;
                GameSystem.MainForm.CursorVisible = false;
            }
            else if (input.Keyboard.IsUp(Key.AltLeft))
            {
                GameSystem.MainForm.MouseMove -= control.MouseMove;
                GameSystem.MainForm.CursorVisible = true;
            }
            
            effectSystem.Update(deltaTime * timeScale);
        }

        public override void RenderFrame()
        {
            renderer.Begin(cam, null, false, true);
            renderer.Render(meshList);
            renderer.Render(light);
            renderer.Render(ppcDesc);
            renderer.End();
        }
        protected override void OnStop()
        {
            if (effectSystem != null)
                effectSystem.Dispose();
            effectSystem = null;
            
            GameSystem.MainForm.MouseMove -= control.MouseMove;
            GameSystem.MainForm.UpdateFrame -= control.UpdateFrame;
            GameSystem.MainForm.KeyDown -= MainForm_KeyDown;
            GameSystem.MainForm.MouseDown -= MainForm_MouseDown;
            GameSystem.MainForm.MouseWheel -= MainForm_MouseWheel;
            GameSystem.MainForm.CursorVisible = true;
        }
        #endregion
    }
}
