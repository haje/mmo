﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace HAJE.MMO.Client.GameLoop
{
    public class TerrainPreview : GameLoopBase
    {
        enum CameraMode
        {
            FreeCam,
            FpsCam
        }

        int tileIndex;
        List<MeshDescriptor> doodads = new List<MeshDescriptor>();
        MeshProvider meshProvider;
        DeferredPointLight[] lights;
        TerrainMeshDescriptor terrain;
        Camera camera;
        CameraMode cameraMode = CameraMode.FreeCam;
        DeferredRenderer deferredRenderer;

        public TerrainPreview(int tileIndex)
            : base("TerrainPreview")
        {
            this.tileIndex = tileIndex;

            CommandList.AddCommand("reload", "지형을 다시 불러 옵니다.", Reload);
            CommandList.AddCommand("tileindex", "불러온 타일 인덱스를 변경합니다.", TileIndex);
        }

        void Reload(DebugConsole console, string[] args)
        {
            Reload();
        }

        void TileIndex(DebugConsole console, string[] args)
        {
            if (args.Length == 3 && int.TryParse(args[2], out tileIndex))
            {
                Reload();
            }
            else
            {
                console.Write("tileindex = [index]");
            }
        }

        protected override void OnInitialize()
        {
            meshProvider = new MeshProvider();

            Reload();
            camera = Rendering.Camera.CreateCamera(new Vector3(250.0f, 4, 250.0f + 4.0f), new Vector3(250.0f, 0, 250.0f), (Degree)(45.0f), null, 0.1f, 10000.0f);
            deferredRenderer = new Rendering.Renderer.DeferredRenderer((int)GameSystem.Viewport.X, (int)GameSystem.Viewport.Y);

            lights = new DeferredPointLight[1]
            {
                new DeferredPointLight(new Vector3(0, 3, 0), new Vector3(0.1f), 5)
            };
        }

        void Reload()
        {
            try
            {
                List<MeshDescriptor> meshList = new List<MeshDescriptor>();
                var loadedTerrain = Rendering.TerrainMeshDescriptor.FromIndex(tileIndex);
                doodads = DoodadManager.DoodadMeshDescriptorList(tileIndex, loadedTerrain, meshProvider);

                terrain = loadedTerrain;
            }
            catch
            {
                MessageBox.Show("없는 인덱스를 불러오려 했습니다. index=" + tileIndex);
            }
        }

        public override void Update(Second deltaTime, Input.InputState input)
        {
            var keyboard = input.Keyboard;
            if (!keyboard.InputHandled)
            {
                if (cameraMode == CameraMode.FreeCam)
                {
                    var pos = camera.Position;
                    float cameraSpeed = 5.0f * camera.Position.Y;
                    if (keyboard.IsPressing(Key.Z))
                    {
                        pos = pos + camera.LookingAtDirection * deltaTime * cameraSpeed;
                    }
                    if (keyboard.IsPressing(Key.C))
                    {
                        pos = pos - camera.LookingAtDirection * deltaTime * cameraSpeed;
                    }

                    var forward = camera.LookingAtDirection;
                    forward.Y = 0;
                    forward.Normalize();
                    if (keyboard.IsPressing(Key.W))
                    {
                        pos = pos + forward * deltaTime * cameraSpeed;
                    }
                    if (keyboard.IsPressing(Key.S))
                    {
                        pos = pos - forward * deltaTime * cameraSpeed;
                    }
                    var right = Vector3.Cross(Vector3.UnitY, camera.LookingAtDirection);
                    if (keyboard.IsPressing(Key.A))
                    {
                        pos = pos + right * deltaTime * cameraSpeed;
                    }
                    if (keyboard.IsPressing(Key.D))
                    {
                        pos = pos + -right * deltaTime * cameraSpeed;
                    }

                    pos.Y = Math.Max(pos.Y, 11.0f);
                    pos.Y = Math.Min(pos.Y, 500.0f);
                    camera.Position = pos;
                }
                if (keyboard.IsDown(Key.F5))
                {
                    Reload();
                }

                GameSystem.DebugOutput.SetWatch("camera",
                    string.Format("{0:F2}, {1:F2}, {2:F2}", camera.Position.X, camera.Position.Y, camera.Position.Z));
            }

            float camY = camera.Position.Y;
            lights[0].Position = camera.Position;
            lights[0].Radius = 3000.0f;
            var intensity = camY * camY * 0.005f + camY * 0.016f - 0.10f;
            lights[0].Intensity = new Vector3(intensity);
        }

        public override void RenderFrame()
        {
            deferredRenderer.Begin(camera, null, false, true);
            deferredRenderer.Render(terrain);
            deferredRenderer.Render(doodads);
            deferredRenderer.Render(lights);
            deferredRenderer.End();
        }

        public override void Dispose(bool manual)
        {
            if (meshProvider != null)
                meshProvider.Dispose();
            meshProvider = null;

            base.Dispose(manual);
        }
    }
}
