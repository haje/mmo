﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Scenes;
using HAJE.MMO.Client.UI.Tool;
using System;

namespace HAJE.MMO.Client.GameLoop
{
    public class Login : GameLoopBase
    {
        public Login()
            : base("Login")
        {
            CommandList.AddCommand("pass", "로그인 과정을 테스트 계정으로 통과합니다.", Pass);
        }

        void Pass(DebugConsole console, string[] args)
        {
            if (args.Length == 2)
            {
                try
                {
                    var request = GameSystem.NetworkContext.GetLoginContext().CreateLoginRequest();
                    request.OnLoginSuccess += (response) =>
                        {
                            GameSystem.SetGameLoop(new GameLoop.ConnectToField(
                                response.FieldIp, response.FieldPort,
                                response.PlayerId, response.FieldToken));
                            GameSystem.NetworkContext.CloseLoginContext();
                        };
                    request.SendSuper(args[1]);
                }
                catch (InvalidOperationException e)
                {
                    GameSystem.UISystem.ShowError(e.Message, ErrorType.Ignore);
                }
            }
            else
            {
                console.Write("login [test_id]");
            }
        }

        protected override void OnInitialize()
        {
            Scene scene = new LoginScene();
            GameSystem.UISystem.ReplaceScene(scene);
        }

        public override void Update(Second deltaTime, Input.InputState input)
        {
        }

        public override void RenderFrame()
        {
        }

        protected override void OnStop()
        {
            
        }
    }
}

