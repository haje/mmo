﻿using OpenTK;
using OpenTK.Input;

namespace HAJE.MMO.Client.GameLoop
{
    public class ControlProcessor
    {
        public Vector2 initMousePos;
        private Vector2 mousePos;
        private Vector2 sensitivity = new Vector2(0.001f, 0.001f);
        public Rendering.WorldSpaceEntity proxy;
        private bool focused = false;

        public ControlProcessor()
        {
            initMousePos = new Vector2(GameSystem.MainForm.Width / 2, GameSystem.MainForm.Height / 2);
            Mouse.SetPosition(initMousePos.X, initMousePos.Y);

            proxy = Rendering.WorldSpaceEntity.CreateWorldSpaceEntity(Vector3.Zero, new Vector3(1, -0.2f, 0), Vector3.UnitY);

            Vector2 deltaVec = sensitivity * (new Vector2(Mouse.GetState().X, Mouse.GetState().Y) - initMousePos - mousePos);
            RotateProxy(deltaVec);

            mousePos = new Vector2(0, 0);
        }

        private void RotateProxy(Vector2 deltaVec)
        {
            Vector2 rotAngle = Vector2.Clamp(deltaVec, Vector2.One * (float)(Degree)(-90.0f), Vector2.One * (float)(Degree)(90.0f));
            var rotMat = Matrix4.CreateRotationZ(rotAngle.X);
            rotMat = Matrix4.Mult(rotMat, Matrix4.CreateRotationY(rotAngle.Y));
            proxy.Rotate(ref rotMat);
        }

        public void MouseMove(object sender, MouseMoveEventArgs e)
        {
            var form = GameSystem.MainForm;
            if (form.Focused)
            {
                if (!focused)
                {
                    Mouse.SetPosition(initMousePos.X, initMousePos.Y);
                    focused = true;
                }
                else if (focused)
                {
                    Vector2 deltaVec = -sensitivity * (new Vector2(Mouse.GetState().X, Mouse.GetState().Y) - initMousePos - mousePos);
                    RotateProxy(deltaVec);

                    mousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y) - initMousePos;
                    Mouse.SetPosition(initMousePos.X, initMousePos.Y);

                    if (ProxyChanged != null)
                    {
                        ProxyChanged(proxy);
                    }
                }
            }
            else
            {
                focused = false;
            }
        }

        public void UpdateFrame(object sender, FrameEventArgs e)
        {
            var kbd = GameSystem.MainForm.Keyboard;

            Vector3 deltaPos = Vector3.Zero;
            float movSpeed = 8.0f * (float)e.Time;

            if (kbd[Key.W])
            {
                deltaPos.X += 1;
            }
            if (kbd[Key.A])
            {
                deltaPos.Y -= 1;
            }
            if (kbd[Key.D])
            {
                deltaPos.Y += 1;
            }
            if (kbd[Key.S])
            {
                deltaPos.X -= 1;
            }
            if (deltaPos.Length > 0)
                deltaPos = deltaPos.Normalized() * movSpeed;

            proxy.Translate(deltaPos);

            if (ProxyChanged != null)
            {
                ProxyChanged(proxy);
            }
        }

        public delegate void GetProxyEventHandler(Rendering.WorldSpaceEntity proxy);

        public event GetProxyEventHandler ProxyChanged;
    }
}
