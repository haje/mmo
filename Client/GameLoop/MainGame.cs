﻿using HAJE.MMO.Client.GamePlay;
using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.EffectSystem;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Input;
using System;

namespace HAJE.MMO.Client.GameLoop
{
    public class MainGame : GameLoopBase
    {
        public MainGame(GamePlay.PlayContext gamePlay, EffectSystem particleSystem, EffectSystem effectSystem)
            : base("MainGame")
        {
            this.gamePlay = gamePlay;
            this.effectSystem = effectSystem;

            camera = Rendering.Camera.CreateCamera(new Vector3(250.0f, 4, 250.0f + 100.0f), new Vector3(250.0f, 0, 250.0f), (Degree)(45.0f), null, 0.1f, 1000.0f);
            shadowCaster = new ShadowCaster(-Vector3.One, camera.Position);
            deferredRenderer = new Rendering.Renderer.DeferredRenderer((int)GameSystem.Viewport.X, (int)GameSystem.Viewport.Y);
            cubemapTex = CubemapTexture.CreateDefaultCubemap();
            skyboxRenderer = new SkyboxRenderer(cubemapTex);
            cursorLock = new MouseCursorLock();
            moveInput = new MoveInput();
            cursor = new CursorParticle();
            ui = new GamePlayUI(gamePlay);
            gamePlay.SetGamePlayUI(ui);
            CommandList.AddCommand("mouse", "마우스 잠금 상태를 변경합니다.", HandleMouseCommand, "lock", "unlock");
            CommandList.AddCommand("reload", "스크립트를 다시 로드합니다.", ReloadCommand, "presentation", "chara");
            CommandList.AddCommand("dead", "죽었을때 Scene을 로드합니다.", DeadCommand);
            CommandList.AddCommand("server", "서버 명령을 입력합니다.", ServerCommand);
            CommandList.AddCommand("find", "특정 플레이어 혹은 현재 플레이어의 위치를 출력합니다", CharacterCommand);
            CommandList.AddCommand("kill", "특정 플레이어를 죽이거나 자살합니다", CharacterCommand);
            CommandList.AddCommand("teleport", "특정 장소로 이동합니다", TeleportCommand);
            CommandList.AddCommand("crosshair", "크로스헤어를 켜고 끕니다", Crosshair);
            CommandList.AddCommand("bgm", "배경음을 켜고 끕니다", BGMCommand);
            CommandList.AddCommand("sound", "효과음을 켜고 끕니다", EffectSoundCommand);
            CommandList.AddCommand("objectpool", "Object Pool의 Watch를 켜고 끕니다", ObjectPool.TurnOnOffHandler);
            CommandList.AddCommand("shadow", "그림자를 켜고 끕니다", ShadowCommand);

            loadScript("GamePlay/Presentation.cs", ref presentation);
        }

        protected override void OnInitialize()
        {
            additivePpcDesc = ParticlePointCloudDescriptor.FromEffectSystem(effectSystem, TextureBlendTypes.Additive);
            attenuativePpcDesc = ParticlePointCloudDescriptor.FromEffectSystem(effectSystem, TextureBlendTypes.Attenuative);
        }

        void loadScript<T>(string path, ref T script) where T : class
        {
            try
            {
                CSScriptLibrary.CSScript.Evaluator.Reset();
                var loaded = CSScriptLibrary.CSScript.Evaluator.LoadFile<T>(path);
                script = loaded;
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
                CSScriptLibrary.CSScript.Evaluator.Reset();
            }
        }

        void HandleMouseCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("mouse | mouse lock | mouse unlock"); };
            if (args.Length == 2)
            {
                string param = args[1];
                if (param == "lock")
                    cursorLock.Enabled = true;
                else if (param == "unlock")
                    cursorLock.Enabled = false;
                else
                    error();
            }
            else if (args.Length == 1)
            {
                cursorLock.Enabled = !cursorLock.Enabled;
                console.Write("mouse " + (cursorLock.Enabled ? "locked" : "unlocked"));
            }
            else
            {
                error();
            }
        }

        void ReloadCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("reload presentation | reload chara"); };
            if (args.Length == 2)
            {
                string module = args[1];
                if (module == "presentation")
                {
                    loadScript("GamePlay/Presentation.cs", ref presentation);
                }
                else if (module == "chara")
                {
                    var myCharacter = gamePlay.HostingCharacter;
                    if(myCharacter != null)
                    {
                        myCharacter.ReloadCharaMesh();
                    }
                }
            }
            else
            {
                error();
            }
        }

        void DeadCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("reload presentation | reload chara"); };
            if (args.Length == 1)
            {
                string module = args[0];
                if(module == "dead")
                {
                    UI.Scenes.DeadScene deadScene = new UI.Scenes.DeadScene();
                    GameSystem.UISystem.ReplaceScene(deadScene);
                    cursorLock.Enabled = false;
                }
                else
                {
                    error();
                }
            }
            else
            {
                error();
            }
        }

        void ServerCommand(DebugConsole console, string[] args)
        {
            var trimmed = new string[args.Length - 1];
            Array.Copy(args, 1, trimmed, 0, trimmed.Length);
            GameSystem.NetworkContext.GetFieldContext().SendSuperCommand(string.Join(" ", trimmed));
        }

        void CharacterCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("{0} | {0} id", args[0]); };
            if (args.Length == 2)
            {
                ServerCommand(console, args);
            }
            else if (args.Length == 1)
            {
                ServerCommand(console, 
                    new string[] { args[0], gamePlay.HostingCharacter.CharacterId.ToString() });
            }
            else
            {
                error();
            }
        }

        void TeleportCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("teleport pos_x pos_y"); };
            if (args.Length == 3)
            {
                ServerCommand(console,
                    new string[] { args[0], gamePlay.HostingCharacter.CharacterId.ToString(), 
                        args[1], args[2] });
            }
            else
            {
                error();
            }
        }

        void BGMCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("bgm on | bgm off || bgm pause"); };

            if(args.Length == 2)
            {
                string module = args[1];

                //if (module == "on")
                //{
                //    GameSystem.SoundManager.Mute(false);
                //}
                //else if (module == "off")
                //{
                //    GameSystem.SoundManager.Mute(true);
                //}
                //else if (module == "pause")
                //{

                //}
            }
            else
            {
                error();
            }
        }

        void EffectSoundCommand(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("sound on | sound off"); };

            if(args.Length == 2)
            {
                string module = args[1];

                if (module == "on")
                {
                    GameSystem.SoundManager.Mute(false);
                }
                else if (module == "off")
                {
                    GameSystem.SoundManager.Mute(true);
                }
                else if (module == "pause")
                {

                }
            }
            else
            {
                error();
            }
        }

        void Crosshair(DebugConsole console, string[] args)
        {
            crosshairActivated = !crosshairActivated;
        }

        void ShadowCommand(DebugConsole console, string[] args)
        {
            shadowActivated = !shadowActivated;
        }

        protected override void OnRun()
        {
            ui.Show();
        }

        public override void Update(Second deltaTime, Input.InputState input)
        {
            var myCharacter = gamePlay.HostingCharacter;
            if (myCharacter != null)
            {
                if (!myCharacter.IsAlive)
                    cursorLock.Enabled = false;

                ui.SetStatusBar(
                    myCharacter.HitPoint,
                    myCharacter.MaxHitPoint,
                    myCharacter.ManaPoint,
                    myCharacter.MaxManaPoint);

                var cursorDelta = cursorLock.GetMovedDistance(true);
                myCharacter.ChangeDirection(cursorDelta * sensitivity);

                var keyboard = input.Keyboard;
                if (!keyboard.InputHandled)
                {
                    moveInput.Update(deltaTime, input.Keyboard, myCharacter);
                    myCharacter.SetMoveState(moveInput.State, moveInput.Direction);

                    var spellCount = myCharacter.ActiveSpellList.Count;
                    for (int i = 0; i < spellCount; i++)
                    {
                        if (keyboard.IsDown(Key.Number1 + i))
                        {
                            myCharacter.SelectedSpellIndex = i;
                            ui.SelectedSpellIndex = i;
                        }
                    }

                    if (keyboard.IsUp(Key.P))
                    {
                        cursorLock.Enabled = !ui.SetSpellInventoryVisible();
                    }
                }

                var mouse = input.Mouse;
                if (!mouse.InputHandled)
                {
                    if(cursorLock.Enabled)
                    {
                        if (myCharacter.SpellEnable &&
                        mouse.IsDown(MouseButton.Left))
                        {
                            if (myCharacter.SpellEnable)
                            {
                                ui.StartCast();
                                myCharacter.BeginCast();
                                gamePlay.SendBeginCast();
                            }
                        }
                        if (myCharacter.IsCasting
                            && mouse.IsUp(MouseButton.Left))
                        {
                            if (myCharacter.CastingTimeRatio == 1.0f)
                            {
                                ui.CastCancel();
                                UpdateCamera();
                                gamePlay.SendFinishCast(camera);
                            }
                            else
                            {
                                ui.CastCancel();
                                gamePlay.SendCancelCast();
                            }
                        }
                        if (myCharacter.IsCasting
                            && mouse.IsPressing(MouseButton.Left))
                        {
                            if (ui.MousePressingCloseTime())
                            {
                                UpdateCamera();
                                gamePlay.SendFinishCast(camera);
                                myCharacter.FinishCast();
                            }
                        }
                    }
                    
                }

                if (myCharacter.CheckSpellEnableState())
                {
                    ui.SetSpellEnabled(myCharacter.SpellEnableList);
                }
            }

            cursor.Update(deltaTime);
            gamePlay.Update(deltaTime);
            effectSystem.Update(deltaTime);
            GameSystem.SoundManager.Update(deltaTime);
            shadowCaster.CenterPos = camera.Position;
        }

        void UpdateCamera()
        {
            var myCharacter = gamePlay.HostingCharacter;
            if (myCharacter != null)
            { 
                var charPos = myCharacter.Position;
                var cdir = myCharacter.Direction;
                var tlv = gamePlay.GetTerrainLevel(myCharacter.FieldId, myCharacter.RelativePosition);
                presentation.SetCharacterCamera(charPos, cdir, tlv, camera, gamePlay.Picker);
            }
            else
            {
                camera.zAlpha = 0;
            }
        }

        public override void RenderFrame()
        {
            skyboxRenderer.Begin(camera);
            skyboxRenderer.Render();
            skyboxRenderer.End();

            var myCharacter = gamePlay.HostingCharacter;
            if (myCharacter != null)
            {
                var charPos = myCharacter.Position;
                var cdir = myCharacter.Direction;
                var tlv = gamePlay.GetTerrainLevel(myCharacter.FieldId, myCharacter.RelativePosition);
                presentation.SetCharacterCamera(charPos, cdir, tlv, camera, gamePlay.Picker);
                if (shadowActivated)
                {
                    deferredRenderer.Begin(camera, shadowCaster, true, crosshairActivated);
                }
                else
                {
                    deferredRenderer.Begin(camera, null, true, crosshairActivated);
                }

                presentation.DrawCharacterLight(charPos, cdir, tlv, deferredRenderer);

                var camPos = camera.Position;
                presentation.DrawCameraLight(camPos, deferredRenderer);
                presentation.DrawMoonLight(camPos, deferredRenderer);

                cursor.Draw(camera, gamePlay.Picker, gamePlay.HostingCharacter, deferredRenderer);
            }
            else
            {
                camera.zAlpha = 0;
                deferredRenderer.Begin(camera, shadowCaster, true, crosshairActivated);
            }

            gamePlay.UpdateCull(myCharacter.Position);
            gamePlay.Draw(deferredRenderer);

            foreach (var effect in effectSystem.EffectList)
            {
                deferredRenderer.Render(effect.MeshList);
                deferredRenderer.Render(effect.LightList);
            }
            deferredRenderer.Render(additivePpcDesc);
            deferredRenderer.Render(attenuativePpcDesc);
            deferredRenderer.End();
        }

        public override void Dispose(bool manual)
        {
            OnStop();
        }

        protected override void OnStop()
        {
            if (gamePlay != null)
                gamePlay.Dispose();
            gamePlay = null;
            
            effectSystem.Dispose();
        }

        float sensitivity = 1.0f;
        bool crosshairActivated = true;
        bool shadowActivated = true;
        ShadowCaster shadowCaster;
        PlayContext gamePlay;
        MouseCursorLock cursorLock;
        MoveInput moveInput;
        DeferredRenderer deferredRenderer;
        SkyboxRenderer skyboxRenderer;
        Camera camera;
        CubemapTexture cubemapTex;
        IPresentation presentation;
        CursorParticle cursor;
        GamePlayUI ui;
        EffectSystem effectSystem;
        ParticlePointCloudDescriptor additivePpcDesc;
        ParticlePointCloudDescriptor attenuativePpcDesc;
    }
}
