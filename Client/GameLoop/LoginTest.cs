﻿using System;
using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Client.GameLoop
{
    public class LoginTest : GameLoopBase
    {
        public LoginTest()
            : base("LoginTest")
        {

        }

        protected override void OnInitialize()
        {
            SslStreamClient sslStreamClient = new SslStreamClient("LoginTest", "127.0.0.1", 25253, "CARoot");
            sslStreamClient.ConnectServer();
            if (sslStreamClient.IsConnected)
            {
                LoginRequest Request = new LoginRequest("donghy91", "1234");
                sslStreamClient.SetReceiveHandler(PacketType.LoginResponse, OnLoginFinished);
                sslStreamClient.SendAsync(Request);
            }
        }

        public void OnLoginFinished(PacketReader reader)
        {
            //Console.WriteLine("YAY");
            using (LoginResponse result = new LoginResponse())
            {
                result.Deserialize(reader);
                //Console.WriteLine("LoginResult {0}",result.ResponseCode.ToString());
            }
            
        }

        public override void Update(Second deltaTime, Input.InputState input)
        {
            
        }

        public override void RenderFrame()
        {
            
        }

        protected override void OnStop()
        {
            
        }
    }
}
