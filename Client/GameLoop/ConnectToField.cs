﻿using HAJE.MMO.Client.Rendering.EffectSystem;
using HAJE.MMO.Client.UI.Scenes;
using OpenTK;

namespace HAJE.MMO.Client.GameLoop
{
    public class ConnectToField : GameLoopBase
    {
        Network.Field.FieldContext fieldContext;
        GamePlay.PlayContext gamePlay = null;
        int playerId;
        string token;

        public ConnectToField(string ip, int port, int playerId, string token)
            : base("ConnectToField")
        {
            EffectFactory.Initialize(effectSystem);
            fieldContext = GameSystem.NetworkContext.CreateFieldContext(ip, port);
            this.playerId = playerId;
            this.token = token;
        }

        protected override void OnInitialize()
        {
            GameSystem.UISystem.ReplaceScene(new LoadingScene());
            fieldContext.SendAuthToken(playerId, token);
        }

        Second timeAcc = (Second)0;
        Second delayTime = (Second)0.25f;
        bool newCharacterSent = false;
        public override void Update(Second deltaTime, Input.InputState input)
        {
            timeAcc += deltaTime;
            if (timeAcc > (Second)0.25f && !newCharacterSent)
            {
                newCharacterSent = true;
                var newChar = fieldContext.NewCharacterRequest();
                newChar.OnNewCharacter += OnNewCharacterResponse;
                newChar.Send(GameSystem.NewCharacterName);
            }
            if (gamePlay != null && gamePlay.IsLoadFinished)
            {
                GameSystem.SetGameLoop(new MainGame(gamePlay, particleSystem, effectSystem));
            }

            //gamePlay.Picker.Pick(new Vector3(0, 0, 0), new Vector3(1, 1, 1), null, Rendering.Picking.PickableType.Player);
        }

        void OnNewCharacterResponse(int characterId)
        {
            gamePlay = new GamePlay.PlayContext(characterId, fieldContext);
            fieldContext.SetPlayContext(gamePlay);
        }

        public override void RenderFrame()
        {
        }

        EffectSystem particleSystem = new EffectSystem();
        EffectSystem effectSystem = new EffectSystem();
    }
}
