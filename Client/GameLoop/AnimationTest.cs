﻿using HAJE.MMO.Client.Animation;
using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Renderer;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Input;
using System;


namespace HAJE.MMO.Client.GameLoop
{
    public class AnimationTest : GameLoopBase
    {
        public AnimationTest()
            : base("AnimationTest")
        {
            CommandList.AddCommand("aimup", "손을 올립니다.", aimup);
            CommandList.AddCommand("stand", "동작을 멈춥니다.", stand);
            CommandList.AddCommand("walk", "걷기 애니메이션을 재생합니다.", walk);
            CommandList.AddCommand("run", "달리기 애니메이션을 재생합니다.", run);
            CommandList.AddCommand("jump", "점프 애니메이션을 재생합니다.", jump);
            CommandList.AddCommand("runleft", "왼쪽으로 게걸음", runLeft);
        }

        void aimup(DebugConsole console, string[] args)
        {
            Action error = () => { console.Write("aimup true | aimup false | aimup"); };
            if (args.Length == 2)
            {
                if (args[1] == "true")
                {
                    corn.AimUp = true;
                }
                else if (args[1] == "false")
                {
                    corn.AimUp = false;
                }
                else
                {
                    error();
                }
            }
            else if (args.Length == 1)
            {
                corn.AimUp = !corn.AimUp;
            }
            else
            {
                error();
            }
        }

        void stand(DebugConsole console, string[] args)
        {
            corn.Stand();
        }

        void walk(DebugConsole console, string[] args)
        {
            corn.Walk();
        }

        void run(DebugConsole console, string[] args)
        {
            corn.Run();
        }

        void runLeft(DebugConsole console, string[] args)
        {
            corn.RunLeft();
        }

        void jump(DebugConsole console, string[] args)
        {
            corn.Jump();
        }
        Camera camera;
        ForwardRenderer renderer;
        Corn corn;
        MeshProvider meshProvider = new MeshProvider();

        protected override void OnInitialize()
        {
            camera = Camera.CreateCamera(new Vector3(0, 2, 6), new Vector3(0, 0, 0), (Degree)(45.0f));
            renderer = new ForwardRenderer();

            corn = new Corn(meshProvider);
        }

        float angle = 0.0f;
        public override void Update(Second deltaTime, Input.InputState input)
        {
            if (!input.Keyboard.InputHandled)
            {
                Vector3 deltaPos = Vector3.Zero;
                float movSpeed = 0.5f;
                var kbd = input.Keyboard;

                if (kbd.IsPressing(Key.W))
                {
                    deltaPos.X += 1;
                }
                if (kbd.IsPressing(Key.A))
                {
                    deltaPos.Y -= 1;
                }
                if (kbd.IsPressing(Key.D))
                {
                    deltaPos.Y += 1;
                }
                if (kbd.IsPressing(Key.S))
                {
                    deltaPos.X -= 1;
                }

                if (deltaPos.Length > 0)
                    deltaPos = deltaPos.Normalized() * movSpeed;

                angle += 1.0f;
                camera.Translate(deltaPos);
            }

            corn.Update(deltaTime);
        }

        public override void RenderFrame()
        {
            renderer.Begin(camera);
            corn.Draw(renderer);
            renderer.End();
        }

        protected override void OnStop()
        {

        }
    }
}
