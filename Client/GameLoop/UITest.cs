﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Scenes;
using HAJE.MMO.Client.UI.Tool;
using System;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Client.GameLoop
{
    public class UITest : GameLoopBase
    {
        public UITest() : base("UITest")
        {
            CommandList.AddCommand("resource", "캐릭터의 자원 상태를 변경합니다.", SetResourceCommand, "hpdamage", "mpdamage", "maxhp", "maxmp", "currenthp", "currentmp");
            CommandList.AddCommand("cast", "캐릭터의 캐스트 바 관련 커맨드입니다.", SetCastCommand, "time");
        }

        protected override void OnInitialize()
        {
            GameSystem.UISystem.ReplaceScene(UIScene);
        }

        void SetResourceCommand(DebugConsole console, string[] args)
        {
            Action error = () =>
            {
                console.Write("resource | resource hpdamage [amount] | resource mpdamage [amount]");
                console.Write("resource maxhp [amount] | resource maxmp [amount]");
                console.Write("resource currenthp [amount] | resource currentmp [amount]");
            };
            if (args.Length == 3)
            {
                string module = args[1];
                switch (args[1])
                {
                    case ("hpdamage"):
                        UIScene.HPBar.ConsumeResource(Convert.ToInt32(args[2]));
                        break;
                    case ("mpdamage"):
                        UIScene.MPBar.ConsumeResource(Convert.ToInt32(args[2]));
                        break;
                    case ("maxhp"):
                        UIScene.HPBar.SetMaxResource(Convert.ToInt32(args[2]));
                        break;
                    case ("maxmp"):
                        UIScene.MPBar.SetMaxResource(Convert.ToInt32(args[2]));
                        break;
                    case ("currenthp"):
                        UIScene.HPBar.SetCurrentResource(Convert.ToInt32(args[2]));
                        break;
                    case ("currentmp"):
                        UIScene.MPBar.SetCurrentResource(Convert.ToInt32(args[2]));
                        break;
                    default:
                        error();
                        break;
                }
            }
            else
            {
                error();
            }
        }

        void SetCastCommand(DebugConsole console, string[] args)
        {
            Action error = () =>
            {
                console.Write("cast | cast time [amount]");
            };
            if (args.Length == 3)
            {
                string module = args[1];
                if (module == "time")
                {
                    UIScene.CastingBar.SetCastTime(float.Parse(args[2]));
                }
                else
                {
                    error();
                }
            }
            else
            {
                error();
            }
        }

        public override void Update(Second deltaTime, Input.InputState input)
        {
            GameSystem.DebugOutput.SetWatch("Mouse Position", GameSystem.InputState.Mouse.Position);
        }

        public override void RenderFrame()
        {
            GL.ClearColor(Color4.White);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        protected override void OnStop()
        {
        }

        MainGameUIScene UIScene = new MainGameUIScene(null);
    }
}

