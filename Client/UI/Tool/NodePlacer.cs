﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI.Component;
using OpenTK;
using OpenTK.Input;

namespace HAJE.MMO.Client.UI.Tool
{
    public class NodePlacer
    {
        private Node trackingNode;

        public NodePlacer()
        {
        }

        public void SetTrackingNode(Node node)
        {
            trackingNode = node;
        }
        // How to input:
        // F1 + Arrow Key : 트래킹하고 있는 노드를 움직입니다.
        // F1 + Enter Key : 트래킹하고 있는 노드의 위치를 디버그 콘솔에 출력합니다.
        public void HandleInput(Input.InputState input)
        {
            if (trackingNode == null)
                return;

            Vector2 move = new Vector2();
            if (input.Keyboard.IsPressing(Key.Right))
                move.X += 1;
            if (input.Keyboard.IsPressing(Key.Left))
                move.X -= 1;
            if (input.Keyboard.IsPressing(Key.Up))
                move.Y += 1;
            if (input.Keyboard.IsPressing(Key.Down))
                move.Y -= 1;

            if (input.Keyboard.IsDown(Key.Enter) || input.Keyboard.IsDown(Key.KeypadEnter))
                GameSystem.Log.Write("Position of Node {0} : {1}", trackingNode.DebugName, trackingNode.Position);

            trackingNode.Position += move;
        }
    }
}
