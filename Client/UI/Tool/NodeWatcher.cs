﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI.Component;
using System.Collections.Generic;
using System.Drawing;
using System;
using OpenTK;

namespace HAJE.MMO.Client.UI.Tool
{
    public class NodeWatcher
    {
        public void WatchNode(Node node)
        {
            //색상 지정하지 않으면 랜덤배정
            Random randomGen = new Random();
            KnownColor[] colorNames = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor randomColorName = colorNames[randomGen.Next(colorNames.Length)];
            Color randomColor = Color.FromKnownColor(randomColorName);

            WatchNode(node, randomColor);
        }

        public void WatchNode(Node node, Color color)
        {
            WatchedObject watched = new WatchedObject(node, color, node.DebugName);
            watchList.Add(watched);
            layer.AddChild(watched.label);
        }

        public void UnwatchNode(Node node)
        {
            //layer 에 그려진 label 을 지운다.
            foreach (WatchedObject watched in watchList)
            {
                if (watched.node.Equals(node))
                {
                    layer.RemoveChild(watched.label);
                }
            }
            //watchList 에서 watchedObject를 지운다.
            watchList.RemoveAll(x => x.node.Equals(node));
        }

        public void Render(UIRenderer renderer)
        {

            foreach (WatchedObject watched in watchList)
            {
                watched.node.RecursiveEnsureTransform();
                renderer.DrawBorderedRectangle(
                    new RectangleF(0, 0, watched.node.ContentSizeWidth, watched.node.ContentSizeHeight),
                    ref watched.node.ThisToWorldTransform, 
                    2, 
                    watched.color, 
                    Color.Empty);

                //화면밖의 노드의 레이블을 화면 안에 띄운다.

                Vector2 labelPosition = watched.node.WorldPosition;
                labelPosition.X = 0 < labelPosition.X ? (labelPosition.X < 1024 - 100 ? labelPosition.X : 1024 - 100) : 0;
                labelPosition.Y = 0 < labelPosition.Y ? (labelPosition.Y < 768 - 50 ? labelPosition.Y : 768 - 50) : 0;

                watched.label.Position = labelPosition;
                layer.VisitRender(renderer);
            }
        }

        private Vector2 getLabelPosition(Vector2 nodePosition)
        {

            return new Vector2(1, 1);
        }

        private List<WatchedObject> watchList = new List<WatchedObject>();
        //text 가 그려질 layer 멤버
        Layer layer = new Layer();
    }

    public class WatchedObject
    {
        public WatchedObject(Node node, Color color, String nodeText)
        {
            this.node = node;
            this.color = color;
            this.label = new Label(node.DebugName, "Resource/Font/NanumBarunGothic.ttf", 12);
        }
        public Node node { get; set; }
        public Color color { get; set; }
        public Label label { get; set; }

    }
}
