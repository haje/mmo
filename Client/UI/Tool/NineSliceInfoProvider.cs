﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace HAJE.MMO.Client.UI.Tool
{
    public class NineSliceInfoProvider
    {
        const string path = "Resource/XML/NineSlices.xml";

        private static NineSliceInfoProvider instance;
        public static NineSliceInfoProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NineSliceInfoProvider();
                }
                return instance;
            }
        }

        private NineSliceInfoProvider()
        {
            Debug.Assert(File.Exists(path));

            XmlSerializer serializer = new XmlSerializer(typeof(SliceXmlType.NineSliceSetting));
            using (FileStream fs = File.OpenRead(path))
            {
                var xml = (SliceXmlType.NineSliceSetting)serializer.Deserialize(fs);
                nineSlices = new Dictionary<string,NineSlice>();
                foreach (var n in xml.NineSliceList)
                {
                    var info = new NineSlice(n);
                    nineSlices.Add(info.Name, info);
                }
            }
        }

        public NineSlice this[string nineSliceName]
        {
            get
            {
                return nineSlices[nineSliceName];
            }
        }

        Dictionary<string, NineSlice> nineSlices;
    }

    public class NineSlice
    {
        public NineSlice(SliceXmlType.NineSlice info)
        {
            this.Folder = info.folder;
            this.Name = info.name;
            this.BorderLength = info.borderLength;
        }

        public readonly string Name;
        public readonly string Folder;
        public readonly float BorderLength;
    }
}

namespace HAJE.MMO.Client.UI.Tool.SliceXmlType
{
    public class NineSlice
    {
        [XmlAttribute]
        public string folder;

        [XmlAttribute]
        public string name;

        [XmlAttribute]
        public float borderLength;

    }
    public class NineSliceSetting
    {
        public List<NineSlice> NineSliceList;
    }
}
