﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK.Input;

namespace HAJE.MMO.Client.UI.Tool
{
    public class DebugConsole
    {
        public DebugConsole()
        {
            layer = new Layer();
            layer.Visible = false;

            colorLayer = new ColorLayer(new Color4(0.1f, 0.1f, 0.1f, 0.9f));
            colorLayer.ContentSize = new Vector2(750, 500);
            layer.AddChild(colorLayer);

            dockLayout = new Layout.DockLayout(colorLayer, layer, new Vector2(0, 1), new Vector2(25, -25));
            colorLayer.Anchor = new Vector2(0, 1);

            textInput = new TextInput("> ", ResourcePath.Font, 12);
            textInput.OnConfirmKey += (t) => { ExecuteCommand(t.Text); };
            textInput.OnNextFocusKey += (t) => { AutoComplete(t.Text); };
            textInput.Focused = true;
            colorLayer.AddChild(textInput);

            logNode = new Node();
            colorLayer.AddChild(logNode);
            for (int i = 0; i < logShowLimit; i++)
            {
                UI.Component.Label logLabel = new UI.Component.Label(" ", ResourcePath.Font, lineHeight);
                logLabel.X = 0;
                logLabel.Y = colorLayer.ContentSizeHeight - lineHeight * 2 * (i + 1);
                logLabelList.Add(logLabel);
                logNode.AddChild(logLabel);
            }
        }

        public void ExecuteCommand(string command)
        {
            if (string.IsNullOrWhiteSpace(command)) return;

            Write("> {0}", command);
            lastCommand = command;
            textInput.Text = "";

            string[] args = command.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < args.Length; i++)
            {
                args[i] = args[i].ToLower();
            }
            if (GameSystem.RunningGameLoop != null)
            {
                if (GameSystem.RunningGameLoop.CommandList.Execute(this, args))
                    return;
            }
            if (!GameSystem.CommandList.Execute(this, args))
            {
                Write("{0}: 알 수 없는 명령입니다.", args[0]);
                Write("    지원하는 명령어 목록을 보려면 help를 입력하세요.");
            }
        }

        public void AutoComplete(string command)
        {
            if (string.IsNullOrWhiteSpace(command)) return;

            textInput.Text = GameSystem.CommandList.AutoCompleteCommand(command);
        }
        
        /*
        public override void HandleInput(Input.InputState args)
        {
            layer.HandleInput(args);
            foreach (var k in args.Keyboard.Keys)
            {
                if (args.Keyboard.IsUp(k))
                    if (k == Key.Up)
                        if (lastCommand != String.Empty)
                            textInput.Text = lastCommand;
            }
        }
        */
        

        public void Write(string format, params object[] args)
        {
            string log;
            log = string.Format(format, args);

            if (logOffset == logShowLimit)
            {
                for (int i = 0; i < logShowLimit - 1; i++)
                {
                    logLabelList[i].Text = logLabelList[i + 1].Text;
                }
                logLabelList[logShowLimit - 1].Text = log;
            }
            else
            {
                logLabelList[logOffset].Text = log;
                logOffset++;
            }
        }

        public void Clear()
        {
            textInput.Text = "";
        }

        public void ToggleMinimize()
        {
            if (isMinimized)
            {
                MinimizeOff();
            }
            else
            {
                MinimizeOn();
            }
        }

        public void MinimizeOn()
        {
            logNode.Visible = false;
            colorLayer.ContentSize = new Vector2(750, 20);
            isMinimized = true;
        }

        public void MinimizeOff()
        {
            logNode.Visible = true;
            colorLayer.ContentSize = new Vector2(750, 500);
            isMinimized = false;
        }

        public void DockTopLeft()
        {
            dockLayout.DockPosition = new Vector2(0, 1);
            dockLayout.Offset = new Vector2(25, -25);
            colorLayer.Anchor = new Vector2(0, 1);
        }

        public void DockTopRight()
        {
            dockLayout.DockPosition = new Vector2(1, 1);
            dockLayout.Offset = new Vector2(-25, -25);
            colorLayer.Anchor = new Vector2(1, 1);
        }
        public void DockBottomLeft()
        {
            dockLayout.DockPosition = new Vector2(0, 0);
            dockLayout.Offset = new Vector2(25, 25);
            colorLayer.Anchor = new Vector2(0, 0);
        }
        public void DockBottomRight()
        {
            dockLayout.DockPosition = new Vector2(1, 0);
            dockLayout.Offset = new Vector2(-25, 25);
            colorLayer.Anchor = new Vector2(1, 0);
        }

        public Layer ConsoleLayer
        {
            get
            {
                return layer;
            }
            set
            {

            }
        }

        int logOffset = 0;
        const int logShowLimit = 20;
        const int lineHeight = 10;
        bool isMinimized = false;

        TextInput textInput;
        String lastCommand;
        Node logNode;
        Layout.DockLayout dockLayout;
        List<Label> logLabelList = new List<Label>();
        ColorLayer colorLayer;
        Layer layer;
        

        
    }
}
