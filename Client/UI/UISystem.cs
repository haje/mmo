﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Tool;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using System;
using Scene = HAJE.MMO.Client.UI.Component.Scene;

namespace HAJE.MMO.Client.UI
{
    public class UISystem : IDisposable
    {
        public UISystem()
        {
            visitor = new NodeInputVisitor();
            Watcher = new NodeWatcher();
            Placer = new NodePlacer();
            Renderer = new UIRenderer();
            Console = new DebugConsole();
        }

        public void ReplaceScene(Scene inputScene)
        {
            scene = inputScene;
        }

        public void Render()
        {
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.DepthTest);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            if (scene != null)
            {
                scene.VisitRender(Renderer);
            }
            if (popup != null)
            {
                popup.VisitRender(Renderer);
            }
            Watcher.Render(Renderer);
            GameSystem.DebugOutput.Draw(Renderer);
            if (Console.ConsoleLayer.Visible)
                Console.ConsoleLayer.VisitRender(Renderer);
        }

        public void Update(Second deltaTime)
        {
            var input = GameSystem.InputState;

            visitor.BeginHandleInput(input);

            if (!BuildInfo.DeployBuild)
            {
                if (input.Keyboard.IsDown(Key.Grave) && !input.Keyboard.InputHandled)
                {
                    Console.ConsoleLayer.Visible ^= true;
                    if (!Console.ConsoleLayer.Visible)
                    {
                        Console.Clear();
                    }
                    input.Keyboard.MarkAsHandled();

                }
                visitor.HandleInput(Console.ConsoleLayer, input);
                if (input.Keyboard.IsPressing(Key.F1) && !input.Keyboard.InputHandled)
                {
                    Placer.HandleInput(input);
                    return;
                }
                if (Console.ConsoleLayer.Visible)
                    Console.ConsoleLayer.VisitUpdate(deltaTime);
            }
            if (popup != null)
            {
                visitor.HandleInput(popup, input);
                popup.VisitUpdate(deltaTime);
            }
            if (scene != null)
            {
                visitor.HandleInput(scene, input);
                scene.VisitUpdate(deltaTime);
            }
            visitor.EndHandleInput(input);
        }

        public void ShowError(string message, ErrorType errorType)
        {
            var popup = ConfirmPopupLayer.CreateFail(message);
            if (errorType == ErrorType.ReturnToLogin)
                popup.AddConfirmHandler(delegate { GameSystem.SetGameLoop(new GameLoop.Login()); });
            else if (errorType == ErrorType.ExitGame)
                popup.AddConfirmHandler(delegate { GameSystem.Exit(); });
            this.popup = popup;
            popup.AddConfirmHandler(delegate {
                if (this.popup == popup)
                    this.popup = null;
            });
        }

        public void Dispose()
        {
            if (Renderer != null)
                Renderer.Dispose();
            Renderer = null;
        }

        public readonly NodePlacer Placer;
        public readonly NodeWatcher Watcher;
        NodeInputVisitor visitor;
        Scene scene;
        Layer popup;
        public UIRenderer Renderer { get; private set; }
        public DebugConsole Console { get; private set; }
    }
}
