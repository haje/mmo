﻿using System.Collections.Generic;

namespace HAJE.MMO.Client.UI.Component
{
    public class TextInputGroup
    {
        public TextInputGroup(params TextInput[] textInputs)
        {
            group = new List<TextInput>();
            if (textInputs != null)
            {
                foreach (var t in textInputs)
                {
                    Add(t);
                }
            }
        }

        public void Add(TextInput textInput)
        {
            group.Add(textInput);
            textInput.OnFocused += HandleFocused;
            textInput.OnNextFocusKey += HandleNextFocus;
            textInput.OnPrevFocusKey += HandlePrevFocus;
            if (textInput.Focused)
                HandleFocused(textInput);
        }

        public void Remove(TextInput textInput)
        {
            group.Remove(textInput);
            textInput.OnFocused -= HandleFocused;
            textInput.OnNextFocusKey -= HandleNextFocus;
            textInput.OnPrevFocusKey -= HandlePrevFocus;
        }

        void HandleFocused(TextInput focused)
        {
            foreach (var t in group)
            {
                if (t != focused)
                {
                    t.Focused = false;
                }
            }
        }

        void HandleNextFocus(TextInput nextFocus)
        {
            NextFocus();
        }

        void HandlePrevFocus(TextInput prevFocus)
        {
            PrevFocus();
        }

        public void NextFocus()
        {
            for (int i = 0; i < group.Count; i++)
            {
                if (group[i].Focused)
                {
                    group[i].Focused = false;
                    int next = (i + 1) % group.Count;
                    group[next].Focused = true;
                    return;
                }
            }

            if (group.Count > 0)
                group[0].Focused = true;
        }

        public void PrevFocus()
        {
            for (int i = group.Count - 1; i >= 0; i--)
            {
                if (group[i].Focused)
                {
                    group[i].Focused = false;
                    int next = (i - 1 + group.Count) % group.Count;
                    group[next].Focused = true;
                    return;
                }
            }

            if (group.Count > 0)
                group[0].Focused = true;
        }

        List<TextInput> group;
    }
}
