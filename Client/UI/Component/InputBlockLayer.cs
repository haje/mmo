﻿
namespace HAJE.MMO.Client.UI.Component
{
    public class InputBlockLayer : Node
    {
        //  들어오는 입력을 모두 먹어치운다.
        //  이 레이어 뒤의 버튼이 입력되거나, 텍스트가 입력되거나 하지 않아야 한다.
        //  NodeVisitor.VisitHandleInput랑 TextInput.HandleInput을 잘 살펴보자

        public override void HandleInput(Input.InputState args)
        {
            args.Keyboard.MarkAsHandled();
            args.Mouse.MarkAsHandled();
        }
    }
}
