﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.UI.Component
{
    public class CastingBar : Node
    {
        public CastingBar()
        {
            castTime = 0;
            closeTIme = 0;
            chargingTime = 0;

            castSpellImage = new Sprite("Resource/Image/UI/NineSliceImage/spell_button_frame.png");
            castSpellImage.ContentSize = new Vector2(25, 25);
            castSpellImage.Visible = false;
            AddChild(castSpellImage);

            Frame = new NineSliceSprite("resource_bar.png");
            Frame.ContentSize = new Vector2(GameSystem.Viewport.X / 3, 25);
            Frame.PositionAnchor = new Vector2(0, 0.5f);
            Frame.Visible = false;
            ContentSize = Frame.ContentSize;
            Frame.Position = new Vector2(castSpellImage.ContentSizeWidth + 5, castSpellImage.ContentSizeHeight/2);
            AddChild(Frame);

            FailFrame = new NineSliceSprite("resource_bar_fail.png");
            FailFrame.ContentSize = Frame.ContentSize;
            FailFrame.PositionAnchor = new Vector2(0, 0.5f);
            FailFrame.Visible = false;
            FailFrame.Position = Frame.Position;
            AddChild(FailFrame);

            FillRectangle = new Sprite("Resource/Image/UI/casting_normal.png");
            FillRectangle.ContentSize = Frame.ContentSize - new Vector2(4);
            FillRectangle.Position = new Vector2(2);
            Frame.AddChild(FillRectangle);

            FailRectangle = new Sprite("Resource/Image/UI/casting_fail.png");
            FailRectangle.ContentSize = FillRectangle.ContentSize;
            FailRectangle.Position = new Vector2(2);
            FailFrame.AddChild(FailRectangle);

            SucessRectangle = new Sprite("Resource/Image/UI/casting_success.png");
            SucessRectangle.ContentSize = FillRectangle.ContentSize;
            SucessRectangle.Position = new Vector2(2);
            Frame.AddChild(SucessRectangle);

            contentSize = new Vector2(castSpellImage.ContentSizeWidth * 2 + Frame.ContentSizeWidth + 10, Frame.ContentSizeHeight);

            onCastTime = false;
            onCloseTime = false;
            onChargingTime = false;

            interrupt = false;
        }

        void CastBarAnimation(Second deltaTime)
        {
            if(!interrupt)
            {
                if (onCastTime)
                {
                    castSpellImage.Visible = true;
                    currentCastTime += deltaTime;
                    if (currentCastTime > castTime)
                    {
                        onCastTime = false;
                        onChargingTime = true;
                        FillRectangle.Visible = false;
                        SucessRectangle.Visible = true;
                    }
                    else
                    {
                        FillRectangle.ContentSizeWidth = currentCastTime * (Frame.ContentSizeWidth - 4) / castTime;
                        FillRectangle.UV = new Vector2(currentCastTime / castTime , 1);
                    }
                        
                }
                if (onChargingTime)
                    SuccessWait(deltaTime);
                if (onCloseTime)
                    CastingBarClose(deltaTime);
            }
            else
            {
                if(onCastTime)
                {
                    onCloseTime = true;
                    onCastTime = false;
                    Frame.Visible = false;
                    FailFrame.Visible = true;
                    FailRectangle.Visible = true;
                    FailRectangle.ContentSizeWidth = currentCastTime * (Frame.ContentSizeWidth - 4) / castTime;
                    FailRectangle.UV = new Vector2(currentCastTime / castTime, 1);
                }
                CastingBarClose(deltaTime);
            }
        }

        void SuccessWait(Second deltaTime)
        {
            chargingTime += deltaTime;
            if (chargingTime > GamePlayConstant.MaxChargeTime)
            {
                onChargingTime = false;
                onCloseTime = true;
            }
        }

        void CastingBarClose(Second deltaTime)
        {
            closeTIme += deltaTime;
            Frame.Color.A -= 1.5f * deltaTime;
            FailFrame.Color.A -= 1.5f * deltaTime;
            FailRectangle.Color.A -= 1.5f * deltaTime;
            SucessRectangle.Color.A -= 1.5f * deltaTime;
            castSpellImage.Color.A -= 1.5f * deltaTime;

            if (closeTIme > 0.6f)
                ResetCastBar();                
        }

        public void ResetCastBar()
        {
            onCastTime = false;
            onCloseTime = false;
            onChargingTime = false;
            Frame.Visible = false;
            FailFrame.Visible = false;
            FailRectangle.Visible = false;
            SucessRectangle.Visible = false;
            castSpellImage.Visible = false;
            FillRectangle.Visible = true;
            FillRectangle.UV = Vector2.One;
            FailRectangle.UV = Vector2.One;
            castTime = 0;
            currentCastTime = 0;
            closeTIme = 0;
            chargingTime = 0;
            FillRectangle.ContentSizeWidth = 0;
            Frame.Color.A = 1;
            FailFrame.Color.A = 1;
            FailRectangle.Color.A = 1;
            castSpellImage.Color.A = 1;
            SucessRectangle.Color.A = 1;
            interrupt = false;
        }

        public void SetCastTime(float time)
        {
            ResetCastBar();
            castTime = time;
            Frame.Visible = true;
            onCastTime = true;
        }

        public override void Update(Second deltaTime)
        {
            CastBarAnimation(deltaTime);
        }

        float castTime;
        float currentCastTime;
        float chargingTime;
        float closeTIme;
        bool onCastTime;
        bool onChargingTime;
        bool onCloseTime;
        bool interrupt;        
        Sprite castSpellImage;
        NineSliceSprite Frame;
        NineSliceSprite FailFrame;
        Sprite FillRectangle;
        Sprite FailRectangle;
        Sprite SucessRectangle;


        public bool OnCastTime
        {
            get
            {
                return onCastTime;
            }
        }

        public bool OnCloseTime
        {
            get
            {
                return onCloseTime;
            }
        }

        public bool OnChargingTime
        {
            get
            {
                return OnChargingTime;
            }
        }

        public bool Interrupt
        {
            get
            {
                return interrupt;
            }
            set
            {
                interrupt = value;
            }
        }

        public Sprite CastSpellImage
        {
            get
            {
                return castSpellImage;
            }
        }
    }
}
