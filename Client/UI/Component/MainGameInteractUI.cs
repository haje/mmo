﻿using OpenTK;
using HAJE.MMO.Client.UI.Scenes;
using HAJE.MMO.Client.GamePlay;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Client.UI.Component
{
    public class MainGameInteractUI : Node
    {

        public MainGameInteractUI(PlayContext playContext)
        {
            this.playContext = playContext;
            toolTip = new SpellToolTip(SpellInfo.GetInfo(SpellId.None));
            AddChild(toolTip);
            toolTip.ZOrder = 10;
            toolTip.Visible = false;
        }

        public override void HandleInput(Input.InputState args)
        {
            toolTip.Visible = false;
            mainScene = (MainGameUIScene)Parent;
            Vector2 mouseNodePosition = args.Mouse.Position;

            if (args.Mouse.IsUp(OpenTK.Input.MouseButton.Left))
            {
                if(draggedSpell != null)
                {
                    DragCancel(args.Mouse);
                    RemoveChild(draggedSpell);
                    draggedSpell = null;
                }
            }
            isLeftDown = args.Mouse.IsDown(OpenTK.Input.MouseButton.Left);

            if (!args.Mouse.IsPressing(OpenTK.Input.MouseButton.Left))
                toolTip.MousePressed = false;
            else
            {
                if (draggedSpell != null)
                {
                    draggedSpell.WorldPosition = args.Mouse.Position;
                }
            }

            if (mainScene.SpellInventory.Visible)
            {
                foreach (SpellShortcutFrame s in mainScene.SpellBar.SpellList)
                {
                    if (s.Contains(mouseNodePosition))
                    {
                        if (args.Mouse.IsDown(OpenTK.Input.MouseButton.Left))
                        {
                            s.IsOwnByPlayer = SpellClicked(s.Spell);
                            break;
                        }

                        if (!s.IsPressed)
                        {
                            toolTip.Visible = true;
                            toolTip.WorldPosition = s.WorldPosition - s.ContentSize * s.PositionAnchor + s.ContentSize / 2;
                            toolTip.SpellChange(s.Spell);
                            toolTip.AnchorSetter();
                        }
                        if (draggedSpell == null && args.Mouse.IsPressing(OpenTK.Input.MouseButton.Left))
                        {
                            draggedSpell = new GeneralSpell(s.ContentSize);
                            draggedSpell.Spell = s.Spell;
                            draggedSpell.Index = s.Index;
                            draggedSpell.Owner = s.Owner;
                            draggedSpell.Enabled = s.Enabled;
                            draggedSpell.SetSpellCount(s.Count);
                            draggedSpell.WorldPosition = args.Mouse.Position;
                            draggedSpell.ZOrder = 10;
                            draggedSpell.IsOwnByPlayer = s.IsOwnByPlayer;
                            AddChild(draggedSpell);
                            toolTip.MousePressed = true;
                        }
                        break;
                    }
                }

                foreach (GeneralSpell s in mainScene.SpellInventory.AllSpellSingList)
                {
                    if (s.Contains(mouseNodePosition))
                    {
                        if (args.Mouse.IsDown(OpenTK.Input.MouseButton.Left))
                        {
                            s.IsOwnByPlayer = SpellClicked(s.Spell);
                            break;
                        }

                        if (!s.IsPressed)
                        {
                            toolTip.Visible = true;
                            toolTip.WorldPosition = s.WorldPosition - s.ContentSize * s.PositionAnchor + s.ContentSize / 2;
                            toolTip.SpellChange(s.Spell);
                            toolTip.AnchorSetter();
                        }
                        if (draggedSpell == null && args.Mouse.IsPressing(OpenTK.Input.MouseButton.Left))
                        {
                            draggedSpell = new GeneralSpell(s.ContentSize);
                            draggedSpell.Spell = s.Spell;
                            draggedSpell.Index = s.Index;
                            draggedSpell.Owner = s.Owner;
                            draggedSpell.Enabled = s.Enabled;
                            draggedSpell.SetSpellCount(s.Count);
                            draggedSpell.WorldPosition = args.Mouse.Position;
                            draggedSpell.ZOrder = 10;
                            draggedSpell.IsOwnByPlayer = s.IsOwnByPlayer;
                            AddChild(draggedSpell);
                            toolTip.MousePressed = true;
                        }
                            
                        break;
                    }
                }
            }

            if (toolTip.MousePressed)
                toolTip.Visible = false;

            if (!toolTip.Visible)
            {
                toolTip.Position = Vector2.Zero;
            }
        }

        private bool SpellClicked(SpellInfo spell)
        {
            if (spell.GainType == GainType.Purchase)
            {
                GameSystem.NetworkContext.GetFieldContext().SendPurchaseSpell(spell.Id);
                return true;
            }
            else if (spell.GainType == GainType.Basic)
                return true;

            return false;
        }

        private void DragCancel(Input.MouseInput mouse)
        {
            Vector2 mouseNodePosition = mouse.Position;

            if (mainScene.SpellBar.Contains(mouseNodePosition))
            {
                foreach (SpellShortcutFrame s in mainScene.SpellBar.SpellList)
                {
                    if (s.Contains(mouseNodePosition))
                    {
                        if(draggedSpell.Owner == OwnerType.SpellInventory)
                        {
                            playContext.HostingCharacter.EquipSpell(s.Index, draggedSpell.Spell);
                            mainScene.SpellInventory.SpellReplace(draggedSpell.Index, s.Spell, s.Enabled);
                            mainScene.SpellBar.ReplaceSpell(s.Index, draggedSpell.Spell, draggedSpell.Enabled);                           
                        }
                        else if(draggedSpell.Owner == OwnerType.SpellBar)
                        {
                            playContext.HostingCharacter.EquipSpell(draggedSpell.Index, s.Spell);
                            playContext.HostingCharacter.EquipSpell(s.Index, draggedSpell.Spell);
                            mainScene.SpellBar.ReplaceSpell(draggedSpell.Index, s.Spell, s.Enabled);
                            mainScene.SpellBar.ReplaceSpell(s.Index, draggedSpell.Spell, draggedSpell.Enabled); 
                        }
                    }
                        
                }
            }                    

            if (mainScene.SpellInventory.Visible)
            {
                if (mainScene.SpellInventory.Contains(mouseNodePosition))
                {
                    foreach (GeneralSpell s in mainScene.SpellInventory.AllSpellSingList)
                    {
                        if(s.Contains(mouseNodePosition))
                        {
                            if (draggedSpell.Owner == OwnerType.SpellInventory)
                            {
                                mainScene.SpellInventory.SpellReplace(draggedSpell.Index, s.Spell, s.Enabled);
                                mainScene.SpellInventory.SpellReplace(s.Index, draggedSpell.Spell, draggedSpell.Enabled);
                            }
                            else if (draggedSpell.Owner == OwnerType.SpellBar)
                            {
                                playContext.HostingCharacter.EquipSpell(draggedSpell.Index, s.Spell);
                                mainScene.SpellBar.ReplaceSpell(draggedSpell.Index, s.Spell, s.Enabled);
                                mainScene.SpellInventory.SpellReplace(s.Index, draggedSpell.Spell, draggedSpell.Enabled);
                            }
                        }
                    }
                }
            }            
        }

        bool isLeftDown = false;

        PlayContext playContext;
        MainGameUIScene mainScene;

        SpellToolTip toolTip;
        GeneralSpell draggedSpell;
    }
}
