﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;

namespace HAJE.MMO.Client.UI.Component
{
    public class NineSliceButton : GeneralButton
    {
        protected NineSliceButton()
        {

        }
        public NineSliceButton(string enableName,Vector2 size)
        {
            DebugName = "[NineSliceButton]";
            instance = NineSliceInfoProvider.Instance;
            enable = new NineSliceSprite(instance[enableName].Name);
            disable = new NineSliceSprite(instance[enableName].Name);
            over = new NineSliceSprite(instance[enableName].Name);
            press = new NineSliceSprite(instance[enableName].Name);
            AddInnerChild(enable);
            AddInnerChild(disable);
            AddInnerChild(over);
            AddInnerChild(press);
            contentSize = size;
            enable.ContentSize = size;
            disable.ContentSize = size;
            over.ContentSize = size;
            press.ContentSize = size;
        }

        public void ButtonImageSetting(string enableName, string disableName, string overName, string pressName)
        {
            if (enable != null)
                enable.UpdatePath(enableName);
            else
                enable = new NineSliceSprite(enableName);

            if (disable != null)
                disable.UpdatePath(disableName);
            else
                disable = new NineSliceSprite(disableName);

            if (over != null)
                over.UpdatePath(overName);
            else
                over = new NineSliceSprite(overName);

            if (pressName != null)
                press.UpdatePath(pressName);
            else
                press = new NineSliceSprite(pressName);
        }
        
        public override void ShowEnabled()
        {
            enable.Visible = true;
            disable.Visible = false;
            over.Visible = false;
            press.Visible = false;
        }

        public override void ShowDisabled()
        {
            enable.Visible = false;
            disable.Visible = true;
            over.Visible = false;
            press.Visible = false;
        }

        public override void ShowOver()
        {
            enable.Visible = false;
            disable.Visible = false;
            over.Visible = true;
            press.Visible = false;
        }

        public override void ShowPressed()
        {
            enable.Visible = false;
            disable.Visible = false;
            over.Visible = false;
            press.Visible = true;
        }

        public void SetWidth(float width)
        {
            ContentSizeWidth = width;
            enable.ContentSizeWidth = width;
            disable.ContentSizeWidth = width;
            over.ContentSizeWidth = width;
            press.ContentSizeWidth = width;
        }

        public void SetHeight(float height)
        {
            ContentSizeHeight = height;
            enable.ContentSizeHeight = height;
            disable.ContentSizeHeight = height;
            over.ContentSizeHeight = height;
            press.ContentSizeHeight = height;
        }

        public override void Render(UIRenderer renderer)
        {
            this.RenderInnerChild(renderer);
        }

        public NineSliceSprite enable, disable, over, press;

        NineSliceInfoProvider instance;
    }
}
