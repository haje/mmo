﻿namespace HAJE.MMO.Client.UI.Component
{
    public class Layer : Node
    {
        public Layer()
        {
            this.ContentSize = GameSystem.Viewport;
            this.DebugName = "[Layer]";
        }
    }
}
