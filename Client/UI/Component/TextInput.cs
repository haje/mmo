﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using System.Diagnostics;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class TextInput : Node
    {
        public TextInput(string defaultText, string fontPath, int size)
        {
            DebugName = "[TextInput] " + defaultText;
            Text = "";
            DefaultText = defaultText;
            font = GameSystem.FontCache.GetFont(fontPath, size);
            RefreshDefaultTexture();

            var nodeSize = defaultTexture.Size;
            nodeSize.X = nodeSize.X * 2;
            ContentSize = nodeSize;
        }


        Font font;
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                if (font == value) return;
                font = value;
                textureNeedUpdate = true;
                cursorTextureNeedUpdate = true;
                defaultTextureNeedUpdate = true;
            }
        }
        string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                if (string.IsNullOrEmpty(value)) value = "";
                if (text == value) return;
                if ( value.Length > maxCharacter  && maxCharacter  != 0  )
                {
                    text = value.Substring(0, maxCharacter);
                }
                else
                {
                    text = value;
                }
                textureNeedUpdate = true;
                cursorTextureNeedUpdate = true;
            }
        }

        string defaultText;
        public string DefaultText
        {
            get
            {
                return defaultText;
            }
            set
            {
                if (string.IsNullOrEmpty(value)) value = "";
                if (text == value) return;
                defaultText = value;
                defaultTextureNeedUpdate = true;
            }
        }

        int maxCharacter = 0;
        public int MaxCharacter
        {
            get
            {
                return maxCharacter;
            }
            set
            {
                maxCharacter = value;
            }
        }

        public Color4 FocusColor = Color4.White;
        public Color4 EnableColor = Color4.White;
        public Color4 DisableColor = Color4.Gray;
        public Color4 DefaultTextColor = Color4.Gray;

        
        public override void Render(Rendering.UIRenderer renderer)
        {
            Texture textureToRender = texture;
            Color4 colorToRender = FocusColor;


            if (string.IsNullOrEmpty(text) && !Focused)
            {
                RefreshDefaultTexture();
                textureToRender = defaultTexture;
                colorToRender = DefaultTextColor;
            }
            else
            {
                if (drawingCursor && Focused)
                {
                    RefreshCursorTexture();
                    textureToRender = cursorTexture;
                }
                else
                {
                    RefreshTexture();
                    textureToRender = texture;
                }

                if (Focused)
                    colorToRender = FocusColor;
                else if (Enabled)
                    colorToRender = EnableColor;
                else
                    colorToRender = DisableColor;
            }

            if (textureToRender != null)
            {
                var textureSize = textureToRender.Size;
                RectangleF area = new RectangleF(new PointF(0, 0), new SizeF(textureSize.X, textureSize.Y));
                renderer.DrawLabel(area, textureToRender, ref ThisToWorldTransform, colorToRender, Vector2.Zero, Vector2.One);
            }
        }

        public override void Update(Second deltaTime)
        {
            cursorTime += deltaTime;
            while (cursorTime > cursorInterval)
            {
                cursorTime -= cursorInterval;
                drawingCursor = !drawingCursor;
            }
            if ( backSpaced == true )
            {
                backSpaceDelay += deltaTime;
                if ( backSpaceDelay > backSpaceDelayInterval )
                {
                    backSpaceTime += deltaTime;
                    if (text.Length > 0 && backSpaceTime > backSpaceInterval )
                    {
                        backSpaceTime -= backSpaceInterval;
                        Text = text.Substring(0, text.Length - 1);
                    }
                }
            }
        }

        public override void HandleInput(Input.InputState args)
        {
            if (Focused)
            {
                var keyboard = args.Keyboard;
                if (!keyboard.InputHandled)
                {
                    HandleKeyboardInput(keyboard);
                    keyboard.MarkAsHandled();
                }
            }
            else if (Enabled)
            {
                var mouse = args.Mouse;
                if (!mouse.InputHandled)
                {
                    HandleMouseInput(mouse);
                }
            }
        }

        void HandleKeyboardInput(Input.KeyboardInput keyboard)
        {
            var shift = keyboard.IsPressing(Key.LShift) || keyboard.IsPressing(Key.RShift);
            foreach (var k in keyboard.Keys)
            {
                if (keyboard.IsDown(k))
                {
                    if (k == Key.BackSpace)
                    {
                        if (!string.IsNullOrEmpty(text))
                        {
                            backSpaced = true;
                            if ( text.Length > 0 )
                            {
                                Text = text.Substring(0, text.Length - 1);
                            }
                        }
                    }
                    else if (k == Key.Enter || k == Key.KeypadEnter)
                    {
                        OnConfirmKey(this);
                    }
                    else if (k == Key.Tab)
                    {
                        if (shift)
                            OnPrevFocusKey(this);
                        else
                            OnNextFocusKey(this);
                    }
                    else
                    {
                        if (shift)
                        {
                            if (keyboard.ShiftKeyToCharacter.ContainsKey(k))
                            {
                                Text = text + keyboard.ShiftKeyToCharacter[k];
                            }
                        }
                        else
                        {
                            if (keyboard.KeyToCharacter.ContainsKey(k))
                            {
                                Text = text + keyboard.KeyToCharacter[k];
                            }
                        }
                    }
                }
                if ( keyboard.IsUp(k))
                {
                    if ( k == Key.BackSpace )
                    {
                        backSpaced = false;
                        backSpaceTime = 0.0f;
                        backSpaceDelay = 0.0f;
                    }
                }
            }
        }

        void HandleMouseInput(Input.MouseInput mouse)
        {
            if ( mouse.IsPressing( MouseButton.Left ))
            {
                if ( this.Contains ( mouse.Position ) )
                {
                    Focused = true;
                    mouse.MarkAsHandled();
                    mouse.MarkAsTracked(this);
                }
                else
                {
                    Focused = false;
                }
            }
        }

        #region texture mgmt

        bool textureNeedUpdate;
        bool cursorTextureNeedUpdate;
        bool defaultTextureNeedUpdate;
        Texture texture;
        Texture cursorTexture;
        Texture defaultTexture;

        void RefreshTexture()
        {
            if (!textureNeedUpdate) return;

            if (texture != null)
            {
                texture.Dispose();
                texture = null;
            }
            string temptext = text;
            if ( password == true )
            {
                text = "";
                for ( int i = 0; i < temptext.Length; i ++ )
                {
                    text = text + "*";
                }
            }
            texture = FontTexture.Create(text, font);
            text = temptext;
            textureNeedUpdate = false;
        }

        void RefreshCursorTexture()
        {
            if (!cursorTextureNeedUpdate) return;

            if (cursorTexture != null)
            {
                cursorTexture.Dispose();
                cursorTexture = null;
            }
            string temptext = text;
            if (password == true)
            {
                text = "";
                for (int i = 0; i < temptext.Length; i++)
                {
                    text = text + "*";
                }
            }
            cursorTexture = FontTexture.Create(text + cursorCharacter, font);
            text = temptext;
            cursorTextureNeedUpdate = false;
        }

        void RefreshDefaultTexture()
        {
            if (!defaultTextureNeedUpdate) return;

            if (defaultTexture != null)
            {
                defaultTexture.Dispose();
                defaultTexture = null;
            }

            defaultTexture = FontTexture.Create(defaultText, font);
            defaultTextureNeedUpdate = false;
        }

        #endregion

        #region cursor

        bool drawingCursor = false;
        const float cursorInterval = 0.5f;
        float cursorTime = 0.0f;

        char cursorCharacter = '_';
        public char CursorCharacter
        {
            get
            {
                return cursorCharacter;
            }
            set
            {
                if (cursorCharacter == value) return;
                Debug.Assert(!char.IsControl(value));
                cursorCharacter = value;
                cursorTextureNeedUpdate = true;
            }
        }

        #endregion

        #region backSpace;

        bool backSpaced = false;
        float backSpaceTime = 0.0f;
        const float backSpaceInterval = 0.075f; // 문자 하나씩 지워질 때 걸리는 텀
        float backSpaceDelay = 0.0f;
        const float backSpaceDelayInterval = 0.3f; // 처음 문자를 지울 때 발생하는 Delay

        #endregion

        bool password = false;
        public bool Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        bool enabled = false;
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                if (enabled && !value && focused)
                    Focused = false;
                enabled = value;
            }
        }

        bool focused = false;
        public bool Focused
        {
            get
            {
                return focused;
            }
            set
            {
                if (!focused && value)
                {
                    focused = value;
                    OnFocused(this);
                }
                focused = value;
            }
        }

        public event TextInputHandler OnFocused = delegate { };
        public event TextInputHandler OnConfirmKey = delegate { };
        public event TextInputHandler OnNextFocusKey = delegate { };
        public event TextInputHandler OnPrevFocusKey = delegate { };
    }

    public delegate void TextInputHandler(TextInput input);
}
