﻿using System;
using System.Collections.Generic;
using OpenTK;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class CompositeButton : GeneralButton
    {
        public void AddButton(GeneralButton button)
        {
            AddInnerChild(button);
        }

        public void RemoveButton(GeneralButton button)
        {
            RemoveInnerChild(button);
        }

        public override void ShowDisabled()
        {
            foreach (GeneralButton c in InnerChildren)
            {
                c.ShowDisabled();
            }
        }

        public override void ShowEnabled()
        {
            foreach (GeneralButton c in InnerChildren)
            {
                c.ShowEnabled();
            }
        }

        public override void ShowOver()
        {
            foreach (GeneralButton c in InnerChildren)
            {
                c.ShowOver();
            }
        }

        public override void ShowPressed()
        {
            foreach (GeneralButton c in InnerChildren)
            {
                c.ShowPressed();
            }
        }

        public override void Render(Rendering.UIRenderer render)
        {
            this.RenderInnerChild(render);
        }
    }
}
