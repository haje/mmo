﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class ColorLayer : Node
    {
        public ColorLayer(Color4 color)
        {
            thisColor = color;
            ContentSizeWidth = GameSystem.Viewport.X;
            ContentSizeHeight = GameSystem.Viewport.Y;
        }

        public override void Render(UIRenderer renderer)
        {
            renderer.DrawBorderedRectangle(
                    new RectangleF(0, 0, ContentSizeWidth, ContentSizeHeight),
                    ref ThisToWorldTransform,
                    0, Color.Empty, thisColor);
        }

        public Color4 thisColor;
    }
}
