﻿using Cyotek.Drawing.BitmapFont;
using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.Rendering.Buffer;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Client.UI.Component
{
    public class BMLabel : Node
    {
        private BitmapFont font;
        public BitmapFont Font
        {
            get
            {
                return font;
            }
            set
            {
                if (value == font)
                    return;

                font = value;
                if (pageTexture != null)
                    pageTexture.Dispose();
                pageTexture = Texture.CreateFromBitmap(new Bitmap(Bitmap.FromFile(font.Pages[0].FileName)));

                textureNeedUpdate = true;
            }
        }
        private String text;
        public String Text
        {
            get
            {
                return text;
            }
            set
            {
                if (value == text)
                    return;
                
                foreach (var character in value)
                {
                    if (!font.Characters.ContainsKey(character))
                    {
                        throw new Exception("이 BMFont에서 지원하지 않는 문자(" + character + ")를 포함하고 있습니다.");
                    }
                }

                text = value;

                textureNeedUpdate = true;
            }
        }

        public Color4 Color = Color4.White;

        bool textureNeedUpdate;
        private Texture texture;
        public Texture TextTexture
        {
            get
            {
                UpdateTexture();
                return texture;
            }
        }
        private Texture pageTexture;

        public BMLabel(String text, String fontPath)
        {
            DebugName = "[BMLabel]" + text;
            this.Font = BitmapFontLoader.LoadFontFromFile(fontPath);
            this.Text = text;
            this.texture = null;    // updated in UpdateTexture();
        }

        public override void Render(UIRenderer renderer)
        {
            UpdateTexture();
            RectangleF area = new RectangleF(new PointF(0, 0), new SizeF(ContentSizeWidth, ContentSizeHeight));
            renderer.DrawLabel(area, texture, ref ThisToWorldTransform, Color, Vector2.Zero, Vector2.One);
        }

        private void UpdateTexture()
        {
            if (textureNeedUpdate)
            {
                Size lastSize = new Size((int)GameSystem.Viewport.X, (int)GameSystem.Viewport.Y);
                var size = font.MeasureFont(text);
                contentSize = new Vector2(size.Width, size.Height);
                if (texture != null)
                {
                    texture.Dispose();
                    texture = null;
                }
                texture = Texture.CreateEmptyBuffer(size.Width, size.Height, PixelInternalFormat.Rgba8, PixelFormat.Rgba, PixelType.UnsignedByte);

                using (var texFBO = Framebuffer.Create(texture))
                {
                    using (var textMesh = Mesh.FromBMLabel(this))
                    {
                        texFBO.Bind();
                        GL.ClearColor(0, 0, 0, 0);
                        GL.Clear(ClearBufferMask.ColorBufferBit);
                        GL.Disable(EnableCap.DepthTest);
                        GL.Enable(EnableCap.Blend);
                        GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.One);
                        ShaderProvider.WorldSpaceTexture.Use();
                        // Assume only one page atm
                        ShaderProvider.WorldSpaceTexture.SetUniform(Matrix4.Identity, Matrix4.Identity, Matrix4.Identity, Color4.White, pageTexture);
                        GL.Viewport(0, 0, size.Width, size.Height);
                        textMesh.Draw();
                        texFBO.Unbind();
                        GL.Viewport(0, 0, lastSize.Width, lastSize.Height);
                        GL.Disable(EnableCap.Blend);
                        GL.Enable(EnableCap.DepthTest);
                    }
                }
                textureNeedUpdate = false;
            }
        }
    }
}
