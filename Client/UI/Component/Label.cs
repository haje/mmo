﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class Label : Node
    {
        public Label(string text, string fontPath, int size)
        {
            DebugName = "[Label]" + text;
            this.text = text;
            this.font = GameSystem.FontCache.GetFont(fontPath, size);
            textureNeedUpdate = true;
            UpdateTexture();
        }

        Font font;
        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                if (font == value) return;
                font = value;
                textureNeedUpdate = true;
            }
        }

        string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                if (string.IsNullOrEmpty(value)) value = "";
                if (text == value) return;
                text = value;
                textureNeedUpdate = true;
            }
        }

        public Color4 Color = Color4.White;

        bool textureNeedUpdate;
        Texture texture;

        public override void Render(UIRenderer renderer)
        {
            UpdateTexture();
            RectangleF area = new RectangleF(new PointF(0, 0), new SizeF(ContentSizeWidth, ContentSizeHeight));
            renderer.DrawLabel(area, texture, ref ThisToWorldTransform, Color, Vector2.Zero, Vector2.One);
        }

        void UpdateTexture()
        {
            if (!textureNeedUpdate) return;

            if (texture != null)
            {
                texture.Dispose();
                ContentSize = new Vector2(0, 0);
                texture = null;
            }

            texture = FontTexture.Create(text, font);
            if (texture != null)
            {
                ContentSize = texture.Size;
            }

            textureNeedUpdate = false;
        }
    }
}
