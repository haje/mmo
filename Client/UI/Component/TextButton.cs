﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class TextButton : GeneralButton
    {
        public TextButton(string text, string fontPath, int size)
        {
            DebugName = "[TextButton]" + text;
            label = new Label(text, fontPath, size);
            this.AddInnerChild(label);
            this.contentSize = label.ContentSize;
        }

        public void ButtonColorSetting(Color4 enabledColor, Color4 disabledColor, Color4 overColor, Color4 pressColor)
        {
            EnabledColor = enabledColor;
            DisabledColor = disabledColor;
            OverColor = overColor;
            PressColor = pressColor;
        }

        public Color4 EnabledColor;
        public Color4 DisabledColor;
        public Color4 OverColor;
        public Color4 PressColor;

        public override void ShowEnabled()
        {
            label.Color = EnabledColor;
        }

        public override void ShowDisabled()
        {
            label.Color = DisabledColor;
        }

        public override void ShowOver()
        {
            label.Color = OverColor;
        }

        public override void ShowPressed()
        {
            label.Color = PressColor;
        }

        public override void Render(UIRenderer renderer)
        {
            this.RenderInnerChild(renderer);
        }

        public Label label;

    }
}
