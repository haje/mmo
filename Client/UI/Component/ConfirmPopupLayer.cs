﻿using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.UI.Component
{
    public class ConfirmPopupLayer : Layer
    {
        private ConfirmPopupLayer(string inputImagePath, string inputText, float scale)
        {
            AddChild(new InputBlockLayer());

            backgroundColorLayer = new ColorLayer(Color4.Black);
            backgroundColorLayer.thisColor.A = 0.4f;
            AddChild(backgroundColorLayer);

            popupImage = new NineSliceSprite("login_button.png");
            popupImage.Anchor = new Vector2(0.5f, 0.5f);
            popupImage.Position = GameSystem.Viewport / 2;
            popupImage.ContentSize = new Vector2(350, 250);
            AddChild(popupImage);

            explainText = new Label(inputText, "Resource/Font/NanumBarunGothic.ttf", 20);
            explainText.Anchor = new Vector2(0.5f, 0.5f);
            explainText.Color = Color4.Black;
            if (explainText.ContentSizeWidth > popupImage.ContentSizeWidth)
                popupImage.ContentSize = new Vector2(explainText.ContentSizeWidth + 50, 250);
            explainText.Position = popupImage.ContentSize / 2;
            popupImage.AddChild(explainText);

            inputImage = new Sprite(inputImagePath);
            inputImage.Scale = scale;
            inputImage.Anchor = new Vector2(0.5f, 0.5f);
            inputImage.Position = new Vector2(popupImage.ContentSizeWidth/2, popupImage.ContentSize.Y - inputImage.ContentSizeHeight * inputImage.Scale + 5);
            popupImage.AddChild(inputImage);

            confirmButton = new CompositeButton();

            confirmButton.Anchor = new Vector2(0.5f, 0.5f);
            confirmButton.Position = new Vector2(popupImage.ContentSizeWidth / 2, confirmButton.ContentSize.Y/2 + 30);
            confirmButton.ContentSize = new Vector2(130, 50);
            
            popupImage.AddChild(confirmButton);
            confirmFrame = new NineSliceButton("login_button.png", confirmButton.ContentSize);
            confirmFrame.ButtonImageSetting("login_button.png", "login_button_disable.png", "login_button_over.png", "login_button_press.png");
            confirmButton.AddButton(confirmFrame);

            confirmText = new TextButton("OK", "Resource/Font/NanumBarunGothic.ttf", 20);
            confirmText.ButtonColorSetting(Color4.Black, Color4.Gray, Color4.White, Color4.Black);
            confirmText.Anchor = new Vector2(0.5f, 0.5f);
            confirmText.Position = confirmFrame.ContentSize / 2;
            confirmButton.AddButton(confirmText);
        }

        public void AddConfirmHandler(ButtonEvent inputEvent)
        {
            confirmButton.OnButton += inputEvent;
        }

        public void SetCloseAsConfirmed()
        {
            confirmButton.OnButton += OnConfirmClose;
        }

        void OnConfirmClose(GeneralButton button)
        {
            RemoveFromParent();
        }

        public static ConfirmPopupLayer CreateSucceess(string inputText)
        {
            return new ConfirmPopupLayer("Resource/Image/UI/register_success.png", inputText, 0.7f);
        }

        public static ConfirmPopupLayer CreateFail(string inputText)
        {
            return new ConfirmPopupLayer("Resource/Image/UI/register_fail.png", inputText, 0.4f);
        }

        Sprite inputImage;
        TextButton confirmText;
        NineSliceButton confirmFrame;
        CompositeButton confirmButton;
        NineSliceSprite popupImage;
        ColorLayer backgroundColorLayer;
        Label explainText;
    }
}
