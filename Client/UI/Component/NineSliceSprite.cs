﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class NineSliceSprite : Node
    {
        protected NineSliceSprite(){}

        public NineSliceSprite(string fileName)
        {
            DebugName = "[NineSliceSprite]";
            index = 0;
            instance = NineSliceInfoProvider.Instance[fileName];
            texturePath = "Resource/Image/" + instance.Folder + "/" + instance.Name;
            texture = GameSystem.TextureCache.GetTexture(texturePath);
            ContentSizeWidth = (int)Math.Ceiling(texture.Size.X);
            ContentSizeHeight = (int)Math.Ceiling(texture.Size.Y);
            borderwidth = instance.BorderLength;
            FileName = fileName;
            xmlNeedUpdate = false;
        }

        public override void Render(UIRenderer renderer)
        {
            if (xmlNeedUpdate)
            {
                instance = NineSliceInfoProvider.Instance[FileName];
                texture = GameSystem.TextureCache.GetTexture("Resource/Image/" + instance.Folder +"/" + instance.Name);
                xmlNeedUpdate = false;
            }

            NineSpriteRendering(renderer);
        }

        private void NineSpriteRendering(UIRenderer renderer)
        {
            renderer.DrawTexture(new RectangleF(new PointF(0, ContentSizeHeight - borderwidth), new SizeF(borderwidth, borderwidth)), texture, ref ThisToWorldTransform, Color,
                Vector2.Zero, new Vector2(borderwidth / texture.Size.X, borderwidth / texture.Size.Y)); // Left Top
            renderer.DrawTexture(new RectangleF(new PointF(0, borderwidth), new SizeF(borderwidth, ContentSizeHeight - 2 * borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(0, borderwidth / texture.Size.Y), new Vector2(borderwidth / texture.Size.X, 1 - borderwidth / texture.Size.Y)); // Left
            renderer.DrawTexture(new RectangleF(new Point(0, 0), new SizeF(borderwidth, borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(0, 1 - borderwidth / texture.Size.Y), new Vector2(borderwidth / texture.Size.X, 1)); // Left Bottom;
            renderer.DrawTexture(new RectangleF(new PointF(borderwidth, 0), new SizeF(ContentSizeWidth - 2 * borderwidth, borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(borderwidth / texture.Size.X, 1 - borderwidth / texture.Size.Y), new Vector2(1 - borderwidth / texture.Size.X, 1)); //Bottom;
            renderer.DrawTexture(new RectangleF(new PointF(ContentSizeWidth - borderwidth, 0), new SizeF(borderwidth, borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(1 - borderwidth / texture.Size.X, 1 - borderwidth / texture.Size.Y), Vector2.One); // Right Bottom;
            renderer.DrawTexture(new RectangleF(new PointF(ContentSizeWidth - borderwidth, borderwidth), new SizeF(borderwidth, ContentSizeHeight - 2 * borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(1 - borderwidth / texture.Size.X, borderwidth / texture.Size.Y), new Vector2(1, 1 - borderwidth / texture.Size.Y)); //Right
            renderer.DrawTexture(new RectangleF(new PointF(ContentSizeWidth - borderwidth, ContentSizeHeight - borderwidth), new SizeF(borderwidth, borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(1 - borderwidth / texture.Size.X, 0), new Vector2(1, borderwidth / texture.Size.Y));
            renderer.DrawTexture(new RectangleF(new PointF(borderwidth, ContentSizeHeight - borderwidth), new SizeF(ContentSizeWidth - 2 * borderwidth, borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(borderwidth / texture.Size.X, 0), new Vector2(1 - borderwidth / texture.Size.X, borderwidth / texture.Size.Y)); // Top

            renderer.DrawTexture(new RectangleF(new PointF(borderwidth, borderwidth), new SizeF(ContentSizeWidth - 2 * borderwidth, ContentSizeHeight - 2 * borderwidth)), texture, ref ThisToWorldTransform, Color,
                new Vector2(borderwidth / texture.Size.X, borderwidth / texture.Size.Y), new Vector2(1 - borderwidth / texture.Size.X, 1 - borderwidth / texture.Size.Y)); // fill
        }

        public void UpdatePath(string fileName)
        {
            FileName = NineSliceInfoProvider.Instance[fileName].Name;
            xmlNeedUpdate = true;
        }

        Texture texture;
        int index;
        float borderwidth;
        string texturePath;
        string FileName;
        bool xmlNeedUpdate;
        public Color4 Color = Color4.White;
        NineSlice instance;
    }
}
