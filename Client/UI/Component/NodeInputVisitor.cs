﻿using HAJE.MMO.Client.Input;

namespace HAJE.MMO.Client.UI.Component
{
    public class NodeInputVisitor
    {
        public void BeginHandleInput(InputState input)
        {
            input.Mouse.SetTrackedHandler(OnMouseTracked);

            trackedOnLastFrame = trackedOnThisFrame;
            trackedOnThisFrame = null;
            if (trackedOnLastFrame != null)
            {
                trackedOnLastFrame.HandleInput(input);
            }
        }

        public void EndHandleInput(InputState input)
        {
            input.Mouse.SetTrackedHandler(null);
        }

        void OnMouseTracked(Node node)
        {
            if (trackedOnThisFrame == null)
                trackedOnThisFrame = node;
        }

        public void HandleInput(Node root, InputState input)
        {
            if (root.Visible == false) return;

            var childrens = root.Children;
            int i = childrens.Count - 1;

            while (i >= 0)
            {
                Node c = childrens[i];
                if (c.ZOrder >= 0)
                {
                    HandleInput(c, input);
                    i--;
                }
                else
                {
                    break;
                }
            }
            if (root != trackedOnLastFrame)
            {
                root.HandleInput(input);
            }
            while (i >= 0)
            {
                Node c = childrens[i];
                if (c.ZOrder < 0)
                {
                    HandleInput(c, input);
                }
                i--;
            }
        }

        private Node trackedOnLastFrame;
        private Node trackedOnThisFrame;
    }
}
