﻿using HAJE.MMO.Client.Rendering;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.UI.Component
{
    public class SpriteButton : GeneralButton
    {
        protected SpriteButton()
        {

        }
        public SpriteButton(string imagePath,Vector2 size)
        {
            DebugName = "[SpriteButton]";

            enable = new CompositeButton();
            enable.ContentSize = size;
            AddInnerChild(enable);

            enableImage = new Sprite(imagePath);
            enableImage.ContentSize = size;
            enable.AddChild(enableImage);

            disable = new CompositeButton();
            disable.ContentSize = size;
            AddInnerChild(disable);

            disableImage = new Sprite(imagePath);
            disableImage.ContentSize = size;
            disable.AddChild(disableImage);

            disableRectangle = new Rectangle(Color4.Black, Color4.Black, 0, size);
            disableRectangle.fillColor.A = 0.5f;
            disable.AddChild(disableRectangle);

            over = new CompositeButton();
            over.ContentSize = size;
            AddInnerChild(over);

            overImage = new Sprite(imagePath);
            overImage.ContentSize = size;
            over.AddChild(overImage);

            overRectangle = new Rectangle(Color4.White, Color4.White, 0, size);
            overRectangle.fillColor.A = 0.5f;
            over.AddChild(overRectangle);

            press = new CompositeButton();
            press.ContentSize = size;
            AddInnerChild(press);

            pressImage = new Sprite(imagePath);
            pressImage.ContentSize = size;
            press.AddChild(pressImage);

            pressRectangle = new Rectangle(Color4.Black, Color4.Black, 0, size);
            pressRectangle.fillColor.A = 0.3f;
            press.AddChild(pressRectangle);
        }

        public override void ShowEnabled()
        {
            enable.Visible = true;
            disable.Visible = false;
            over.Visible = false;
            press.Visible = false;
        }

        public override void ShowDisabled()
        {
            enable.Visible = false;
            disable.Visible = true;
            over.Visible = false;
            press.Visible = false;
        }

        public override void ShowOver()
        {
            enable.Visible = false;
            disable.Visible = false;
            over.Visible = true;
            press.Visible = false;
        }

        public override void ShowPressed()
        {
            enable.Visible = false;
            disable.Visible = false;
            over.Visible = false;
            press.Visible = true;
        }

        public void SetWidth(float width)
        {
            ContentSizeWidth = width;
            enable.ContentSizeWidth = width;
            disable.ContentSizeWidth = width;
            over.ContentSizeWidth = width;
            press.ContentSizeWidth = width;
        }

        public void SetHeight(float height)
        {
            ContentSizeHeight = height;
            enable.ContentSizeHeight = height;
            disable.ContentSizeHeight = height;
            over.ContentSizeHeight = height;
            press.ContentSizeHeight = height;
        }

        public override void Render(UIRenderer renderer)
        {
            this.RenderInnerChild(renderer);
        }

        public void UpdateImage(string imagePath)
        {
            enableImage.UpdatePath(imagePath);
            disableImage.UpdatePath(imagePath);
            overImage.UpdatePath(imagePath);
            pressImage.UpdatePath(imagePath);
        }

        public CompositeButton enable, disable, over, press;
        Sprite enableImage, disableImage, overImage, pressImage;
        Rectangle disableRectangle, overRectangle, pressRectangle;
    }
}
