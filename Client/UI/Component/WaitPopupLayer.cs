﻿using OpenTK;
using OpenTK.Graphics;
namespace HAJE.MMO.Client.UI.Component
{
    public class WaitPopupLayer : Node
    {
        public WaitPopupLayer(string inputText)
        {
            AddChild(new InputBlockLayer());

            backgroundColorLayer = new ColorLayer(Color4.Black);
            backgroundColorLayer.thisColor.A = 0.4f;
            AddChild(backgroundColorLayer);

            popupImage = new NineSliceSprite("login_button.png");
            popupImage.Anchor = new Vector2(0.5f, 0.5f);
            popupImage.Position = GameSystem.Viewport / 2;
            popupImage.ContentSize = new Vector2(350, 250);
            AddChild(popupImage);

            explainText = new Label(inputText, "Resource/Font/NanumBarunGothic.ttf", 20);
            explainText.Anchor = new Vector2(0.5f, 0.5f);
            explainText.Color = Color4.Black;
            explainText.Position = popupImage.ContentSize / 2;
            popupImage.AddChild(explainText);
        }

        NineSliceSprite popupImage;
        ColorLayer backgroundColorLayer;
        Label explainText;
    }
}
