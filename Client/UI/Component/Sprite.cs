﻿using HAJE.MMO.Client.Rendering;
using OpenTK.Graphics;
using System;
using System.IO;
using OpenTK;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class Sprite : Node
    {
        public Sprite(string path)
        {
            DebugName = "[Sprite]";
            texture = GameSystem.TextureCache.GetTexture(path);
            ContentSizeWidth = (int)Math.Ceiling(texture.Size.X);
            ContentSizeHeight = (int)Math.Ceiling(texture.Size.Y);
            texturePath = path;
            textureNeedUpdate = false;
            uv = Vector2.One;
        }

        public override void Render(UIRenderer renderer)
        {
            if (textureNeedUpdate)
            {
                texture = GameSystem.TextureCache.GetTexture(texturePath);
                textureNeedUpdate = false;
            }

            RectangleF area = new RectangleF(new PointF(0, 0), new SizeF(ContentSizeWidth, ContentSizeHeight));
            renderer.DrawTexture(area, texture, ref ThisToWorldTransform, Color, Vector2.Zero, uv);
        }

        public void UpdatePath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("파일을 찾을 수 없습니다.", path);
            }
            texturePath = path;
            textureNeedUpdate = true;
        }

        bool textureNeedUpdate;
        string texturePath;
        Vector2 uv;
        Texture texture;
        public Color4 Color = Color4.White;

        public Vector2 UV
        {
            get
            {
                return uv;
            }
            set
            {
                uv = value;
            }
        }
    }
}
