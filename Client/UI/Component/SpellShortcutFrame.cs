﻿using OpenTK;
using HAJE.MMO.Client.UI.Tool;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Client.UI.Component
{
    public class SpellShortcutFrame : CompositeButton
    {
        public SpellShortcutFrame(Vector2 size)
        {
            DebugName = "[SpellShortcutFrame]";
            contentSize = size;
            instance = NineSliceInfoProvider.Instance;

            spellIcon = new GeneralSpell(size);
            AddButton(spellIcon);

            selectedframe = new NineSliceSprite(instance["spell_button_frame_selected.png"].Name);
            selectedframe.ZOrder = 4;
            AddChild(selectedframe);
            
            selectedframe.ContentSize = size;
            isselected = false;
        }

        public override void HandleInput(Input.InputState args)
        {
            base.HandleInput(args);
            if(isselected)
            {
                spellIcon.normalframe.Visible = false;
                selectedframe.Visible = true;
            }
            else
            {
                spellIcon.normalframe.Visible = true;
                selectedframe.Visible = false;
            }
        }

        public NineSliceSprite selectedframe;
        GeneralSpell spellIcon;

        NineSliceInfoProvider instance;
        bool isselected;

        public SpellInfo Spell
        {
            get
            {
                return spellIcon.Spell;
            }
            set
            {
                spellIcon.Spell = value;
            }
        }

        public GeneralSpell SpellIcon
        {
            get
            {
                return spellIcon;
            }
        }
   
        public bool IsSelected
        {
            get
            {
                return isselected;
            }
            set
            {
                isselected = value;
            }
        }

        public bool IsPressed
        {
            get
            {
                return spellIcon.IsPressed;
            }
        }

        public int Index
        {
            get
            {
                return spellIcon.Index;
            }
            set
            {
                spellIcon.Index = value;
            }
        }

        public OwnerType Owner
        {
            get
            {
                return spellIcon.Owner;
            }
            set
            {
                spellIcon.Owner = value;
            }
        }

        public int Count
        {
            get
            {
                return spellIcon.Count;
            }
        }

        public bool IsOwnByPlayer
        {
            get
            {
                return spellIcon.IsOwnByPlayer;
            }
            set
            {
                spellIcon.IsOwnByPlayer = value;
            }
        }
    }
}
