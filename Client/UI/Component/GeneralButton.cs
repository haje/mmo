﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public abstract class GeneralButton : Node
    {
        public override void HandleInput(Input.InputState args)
        {
            if(Visible)
            {
                if (enabled)
                    ShowEnabled();

                if (!args.Mouse.InputHandled)
                {
                    StillClickedState(args.Mouse);
                    if (this.Contains(args.Mouse.Position))
                    {
                        ShowOver();
                        ContainDoPress(args.Mouse);
                    }
                    else
                    {
                        if (!mousePressState)
                            NotContainDoPress(args.Mouse);
                    }
                    DoButtonEvent(args.Mouse);
                }

                if (!enabled)
                    ShowDisabled();

                StateChecker();
            }
            
        }

        #region Helper
        private void ContainDoPress(Input.MouseInput mouse)
        {
            if (mouse.IsPressing(MouseButton.Left))
            {
                if (!prevClicked)
                {
                    ShowPressed();
                    mousePressState = true;
                    isPressed = true;
                }
            }
            else
                prevClicked = false;
        }
        private void NotContainDoPress(Input.MouseInput mouse)
        {
            if (mouse.IsPressing(MouseButton.Left))
                prevClicked = true;
            else
                prevClicked = false;
        }
        private void StillClickedState(Input.MouseInput mouse)
        {
            if (mouse.IsPressing(MouseButton.Left))
            {
                if (mousePressState)
                {
                    mouse.MarkAsHandled();
                    mouse.MarkAsTracked(this);
                }
            }
            else
                mousePressState = false;

        }
        private void DoButtonEvent(Input.MouseInput mouse)
        {
            if (enabled && isPressed && !mouse.IsPressing(MouseButton.Left) && this.Contains(mouse.Position))
            {
                OnButton(this);
                isPressed = false;
            }
            else if (!this.Contains(mouse.Position))
            {
                isPressed = false;
            }
        }
        private void StateChecker()
        {
            if (!enabled)
            {
                isPressed = false;
            }            
        }
        #endregion

        bool enabled = true;
        bool movable = false;

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        public bool Movable
        {
            get
            {
                return movable;
            }
            set
            {
                movable = value;
            }
        }

        public abstract void ShowDisabled();
        public abstract void ShowEnabled();
        public abstract void ShowOver();
        public abstract void ShowPressed();

        private bool mousePressState = false;
        private bool isPressed = false;
        private bool prevClicked = false;

        protected Matrix3 parentTransform = Matrix3.Identity;

        public event ButtonEvent OnButton = delegate { };
    }

    public delegate void ButtonEvent(GeneralButton button);

}
