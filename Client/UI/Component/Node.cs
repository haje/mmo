﻿using HAJE.MMO.Client.Input;
using HAJE.MMO.Client.Rendering;
using OpenTK;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.UI.Component
{
    public class Node
    {
        #region Add/Remove Child, ZOrder

        public void AddChild(Node child)
        {
            children.Add(child);
            child.parent = this;
            OnChildZOrderChanged();

            OnChildListChanged(this);
        }

        public void AddChild(Node child, int zOrder)
        {
            children.Add(child);
            child.ZOrder = zOrder;
            child.parent = this;
            OnChildZOrderChanged();

            OnChildListChanged(this);
        }

        public void RemoveChild(Node child)
        {
            children.Remove(child);
            OnChildZOrderChanged();

            OnChildListChanged(this);
        }

        public void RemoveAllChild()
        {
            children = new List<Node>();
        }
        public event NodeHandler OnChildListChanged = delegate { };

        private Node parent = null;
        public Node Parent
        {
            get
            {
                return parent;
            }
        }

        private int zOrder = 0;
        public int ZOrder
        {
            get
            {
                return zOrder;
            }
            set
            {
                zOrder = value;
                if (parent != null)
                {
                    parent.OnChildZOrderChanged();
                }
            }
        }

        private void OnChildZOrderChanged()
        {
            childrenNeedSort = true;
        }

        private List<Node> children = new List<Node>();

        public IReadOnlyList<Node> Children
        {
            get
            {
                EnsureChildrenSorted();
                return children.AsReadOnly();
            }
        }

        /// <summary>
        /// 최근 정렬 이후 자식 목록이나 자식의 zOrder가 변경되었으면 true
        /// </summary>
        private bool childrenNeedSort = false;

        /// <summary>
        /// 자식 목록이 정렬되어 있지 않으면 정렬한다.
        /// 자식 목록의 정렬이 보장되어 있어야 하는 코드를 실행하기 전에 이 함수를 호출
        /// </summary>
        private void EnsureChildrenSorted()
        {
            if (childrenNeedSort)
            {
                children.Sort((c1, c2) =>
                    {
                        return c1.zOrder - c2.zOrder;
                    });
                innerChildren.Sort((c1, c2) =>
                    {
                        return c1.zOrder - c2.zOrder;
                    });
                childrenNeedSort = false;
            }
        }

        protected void AddInnerChild(Node node)
        {
            innerChildren.Add(node);
            node.parent = this;
            OnChildZOrderChanged();
        }

        protected void RemoveInnerChild(Node node)
        {
            innerChildren.Remove(node);
            OnChildZOrderChanged();
        }

        protected void RenderInnerChild(UIRenderer renderer)
        {
            EnsureTransform();
            foreach (Node n in InnerChildren)
                n.VisitRender(renderer);
        }

        List<Node> innerChildren = new List<Node>();
        protected IReadOnlyList<Node> InnerChildren
        {
            get
            {
                EnsureChildrenSorted();
                return innerChildren.AsReadOnly();
            }
        }

        public void RemoveFromParent()
        {
            Debug.Assert(this.parent != null);
            this.parent.RemoveChild(this);
        }

        #endregion

        #region Transform = { Position, Rotation, Scale, Size }

        private bool thisToParentTransformNeedUpdate = false;
        private bool thisToWorldTransformNeedUpdate = false;
        private void MarkTransformNeedUpdate()
        {
            thisToParentTransformNeedUpdate = true;
            thisToWorldTransformNeedUpdate = true;
            OnTransformChanged(this);
        }

        private void OnParentTransformUpdated()
        {
            thisToWorldTransformNeedUpdate = true;
        }

        public Matrix3 ThisToWorldTransform = Matrix3.Identity;

        private Matrix3 thisToParentTransform = Matrix3.Identity;
        private void CalculateTransform()
        {
            thisToParentTransform = Matrix3.Identity;
            thisToParentTransform = thisToParentTransform * new Matrix3(1, 0, 0, 0, 1, 0, -(contentSize.X * rotationAnchor.X) , -(contentSize.Y * rotationAnchor.Y), 1);
            thisToParentTransform = thisToParentTransform * new Matrix3(Radian.Cos(rotation), Radian.Sin(rotation), 0, -Radian.Sin(rotation), Radian.Cos(rotation), 0, 0, 0, 1);
            thisToParentTransform = thisToParentTransform * new Matrix3(ScaleX, 0, 0, 0, ScaleY, 0, 0, 0, 1);
            thisToParentTransform = thisToParentTransform * new Matrix3(1, 0, 0, 0, 1, 0,
                position.X + (contentSize.X * rotationAnchor.X) * ScaleX - (contentSize.X * positionAnchor.X) * ScaleX, position.Y + (contentSize.Y * rotationAnchor.Y) * ScaleY - (contentSize.Y * positionAnchor.Y) * ScaleY, 1);
        }

        public void RecursiveEnsureTransform()
        {
            if (parent != null)
                parent.RecursiveEnsureTransform();
            EnsureTransform();
        }

        public void EnsureTransform()
        {
            if (thisToParentTransformNeedUpdate)
            {
                CalculateTransform();
                thisToParentTransformNeedUpdate = false;
                thisToWorldTransformNeedUpdate = true;
            }

            if (thisToWorldTransformNeedUpdate)
            {
                if (parent != null)
                    ThisToWorldTransform = thisToParentTransform * parent.ThisToWorldTransform;
                else
                    ThisToWorldTransform = thisToParentTransform;

                foreach (Node n in children)
                {
                    n.OnParentTransformUpdated();
                }
                foreach (Node n in innerChildren)
                {
                    n.OnParentTransformUpdated();
                }
                thisToWorldTransformNeedUpdate = false;
            }
        }

        public Vector2 WorldPosition
        {
            get
            {
                return NodeToWorld(Vector2.Zero);
            }
            set
            {
                RecursiveEnsureTransform();
                if (parent != null)
                    position = new Vector2(((value.X - parent.ThisToWorldTransform.M31) / parent.ScaleX), ((value.Y - parent.ThisToWorldTransform.M32) / parent.ScaleY));
                else
                    position = value;
                
                MarkTransformNeedUpdate();
            }
        }

        public bool Contains(Vector2 worldPosition)
        {
            RectangleF area = new RectangleF(0,0,ContentSizeWidth,ContentSizeHeight);
            Vector2 nodeposition = WorldToNode(worldPosition);

            return area.Contains(new PointF(nodeposition.X, nodeposition.Y));
        }

        public Vector2 WorldToNode(Vector2 worldPosition)
        {
            RecursiveEnsureTransform();
            List<float> MatHelper = MatrixConvert(ThisToWorldTransform.Inverted());
            return new Vector2(MatHelper[0] * worldPosition.X + MatHelper[1] * worldPosition.Y + MatHelper[2], MatHelper[3] * worldPosition.X + MatHelper[4] * worldPosition.Y + MatHelper[5]);
        }

        private List<float> MatrixConvert(Matrix3 input)
        {
            List<float> temp = new List<float>();
            temp.Add(input.M11);
            temp.Add(input.M21);
            temp.Add(input.M31);
            temp.Add(input.M12);
            temp.Add(input.M22);
            temp.Add(input.M32);
            temp.Add(input.M13);
            temp.Add(input.M23);
            temp.Add(input.M33);

            return temp;
        }

        public Vector2 NodeToWorld(Vector2 nodePosition)
        {
            RecursiveEnsureTransform();
            Vector2 parentWorldPosition = Vector2.Zero;

            return new Vector2(ThisToWorldTransform.M31 + nodePosition.X, ThisToWorldTransform.M32 + nodePosition.Y) + ContentSize * PositionAnchor * scale;
        }

        private Vector2 position;
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                MarkTransformNeedUpdate();
            }
        }

        public float X
        {
            get
            {
                return position.X;
            }
            set
            {
                position.X = value;
                MarkTransformNeedUpdate();
            }
        }

        public float Y
        {
            get
            {
                return position.Y;
            }
            set
            {
                position.Y = value;
                MarkTransformNeedUpdate();
            }
        }

        private Vector2 scale = Vector2.One;
        public Vector2 ScaleXY
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
                MarkTransformNeedUpdate();
            }
        }

        public float Scale
        {
            get
            {
                Debug.Assert(scale.X == scale.Y, "Scale의 대표값을 정할 수 없습니다. ScaleX와 ScaleY를 따로 얻어서 사용하세요.");
                return scale.X;
            }
            set
            {
                scale = new Vector2(value, value);
                MarkTransformNeedUpdate();
            }
        }

        public float ScaleX
        {
            get
            {
                return scale.X;
            }
            set
            {
                scale.X = value;
                MarkTransformNeedUpdate();
            }
        }
        public float ScaleY
        {
            get
            {
                return scale.Y;
            }
            set
            {
                scale.Y = value;
                MarkTransformNeedUpdate();
            }
        }

        protected Vector2 contentSize;
        public Vector2 ContentSize
        {
            get
            {
                return contentSize;
            }
            set
            {
                contentSize = value;
                MarkTransformNeedUpdate();

                OnContentSizeChanged(this);
            }
        }

        public float ContentSizeWidth
        {
            get
            {
                return contentSize.X;
            }
            set
            {
                contentSize.X = value;
                MarkTransformNeedUpdate();

                OnContentSizeChanged(this);
            }
        }
        public float ContentSizeHeight
        {
            get
            {
                return contentSize.Y;
            }
            set
            {
                contentSize.Y = value;
                MarkTransformNeedUpdate();

                OnContentSizeChanged(this);
            }
        }

        private Radian rotation = new Radian(0);
        public Radian Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                MarkTransformNeedUpdate();
            }
        }
        
        private Vector2 rotationAnchor = Vector2.Zero;
        public Vector2 RotationAnchor
        {
            get
            {
                return rotationAnchor;
            }
            set
            {
                rotationAnchor = value;
                MarkTransformNeedUpdate();
            }
        }
        private Vector2 positionAnchor = Vector2.Zero;
        public Vector2 PositionAnchor
        {
            get
            {
                return positionAnchor;
            }
            set
            {
                positionAnchor = value;
                MarkTransformNeedUpdate();
            }
        }
        private Vector2 anchor = Vector2.Zero;
        public Vector2 Anchor
        {
            get
            {
                Debug.Assert(positionAnchor == rotationAnchor);
                return anchor;
            }
            set
            {
                anchor = value;
                positionAnchor = value;
                rotationAnchor = value;
                MarkTransformNeedUpdate();
            }
        }

        public event NodeHandler OnContentSizeChanged = delegate { };
        public event NodeHandler OnTransformChanged = delegate { };
        #endregion

        public bool Visible = true;

        public bool IsRecrsiveVisible()
        {
            if (parent != null)
                if (!parent.IsRecrsiveVisible()) return false;

            return Visible;
        }

        public string DebugName = "[Node]";

        public void VisitRender(UIRenderer renderer)
        {
            if (Visible == false) return;

            EnsureTransform();
            foreach (Node c in Children)
            {
                if (c.ZOrder < 0)
                    c.VisitRender(renderer);
            }
            Render(renderer);
            foreach (Node c in Children)
            {
                if (c.ZOrder >= 0)
                    c.VisitRender(renderer);
            }
        }

        public virtual void Render(UIRenderer renderer)
        {

        }

        public virtual void Render(UIRenderer renderer, Matrix3 parentMatrix)
        {

        }

        public void VisitUpdate(Second deltaTime)
        {
            var childrens = Children;
            int i = childrens.Count - 1;
            while (i >= 0)
            {
                Node c = childrens[i];
                if (c.ZOrder >= 0)
                {
                    c.VisitUpdate(deltaTime);
                }
                else
                {
                    break;
                }
                i--;
            }
            Update(deltaTime);
            while (i >= 0)
            {
                childrens[i--].VisitUpdate(deltaTime);
            }
        }

        public virtual void Update(Second deltaTime)
        {

        }

        public virtual void HandleInput(InputState args)
        {

        }
    }

    public delegate void NodeHandler(Node node);
}
