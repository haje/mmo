﻿using HAJE.MMO.Client.Rendering;
using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace HAJE.MMO.Client.UI.Component
{
    public class Rectangle : Node
    {
        public Rectangle(Color4 fillColor, Color4 borderColor, float borderLength, Vector2 contentSize)
        {
            this.fillColor = fillColor;
            this.borderColor = borderColor;
            this.borderLength = borderLength;
            this.contentSize = contentSize;
        }
        public override void Render(UIRenderer renderer)
        {
            renderer.DrawBorderedRectangle(
                    new RectangleF(0, 0, ContentSizeWidth, ContentSizeHeight),
                    ref ThisToWorldTransform,
                    borderLength, borderColor, fillColor);
        }

        public float borderLength;
        public Color4 fillColor;
        public Color4 borderColor;
    }

    
}
