﻿using HAJE.MMO.Spell;
using System.Collections.Generic;
using System;
using OpenTK;

namespace HAJE.MMO.Client.UI.Component
{
    public class SpellBar : Layer
    {
        const int maxSpellNumber = 5;

        public SpellBar()
        {
            spellList = new List<SpellShortcutFrame>();
            nowSelected = false;
        }

        public void AddSpell(SpellInfo Spell)
        {
            if(Children.Count < maxSpellNumber)
            {
                SpellShortcutFrame child = new SpellShortcutFrame(new Vector2(50, 50));
                child.Spell = Spell;
                child.Position = new Vector2(Children.Count * child.ContentSizeWidth, 0);
                child.Index = spellList.Count;
                child.Owner = OwnerType.SpellBar;
                child.IsOwnByPlayer = true;
                AddChild(child);
                spellList.Add(child);
                ContentSizeWidth = child.ContentSizeWidth * Children.Count;
                ContentSizeHeight = child.ContentSizeHeight;
            }
            else
            {
                throw new Exception("스킬바의 최대 스킬 갯수를 넘겼습니다.");
            }
                
        }

        public void ReplaceSpell(int index, SpellInfo Spell, bool isEnable)
        {
            if(Children[index] != null)
            {
                spellList[index].Spell = Spell;
                spellList[index].Enabled = isEnable;
                Renewal();
            }
            else
            {
                throw new Exception("그 index에 spell이 없습니다.");
            }
        }

        void Renewal()
        {
            RemoveAllChild();
            for (int i = 0; i < spellList.Count; i++)
            {
                spellList[i].Position = new Vector2(i * 50, 0);
                spellList[i].Owner = OwnerType.SpellBar;
                spellList[i].Index = i;
                AddChild(spellList[i]);
            }

            contentSize = new Vector2(50, 50) * Children.Count;
        }

        public void RemoveSpell(int index)
        {
            if(Children[index] != null)
            {
                RemoveChild(Children[index]);
                spellList.RemoveAt(index);
                Renewal();
            }
            else
            {
                throw new Exception("그 index에 spell이 없습니다.");
            }            
        }

        void ShortcutAllSelectedFalse()
        {
            nowSelected = false;
            foreach (SpellShortcutFrame s in Children)
            {
                s.IsSelected = false;
                if (s.Enabled)
                    s.ShowEnabled();
            }
        }

        public void SetSpellFirst()
        {
            SetSelectedIndex(0);
        }

        bool nowSelected;

        public bool NowSelected
        {
            get
            {
                return nowSelected;
            }
        }
        SpellShortcutFrame nowSpell;

        public SpellShortcutFrame NowSpell
        {
            get
            {
                return nowSpell;
            }
        }

        public void SetSelectedIndex(int index)
        {
            if(Children[index] != null)
            {
                nowSelected = true;
                ShortcutAllSelectedFalse();
                nowSpell = (SpellShortcutFrame)Children[index];
                nowSpell.IsSelected = true;
            }
        }

        List<SpellShortcutFrame> spellList;

        public IReadOnlyList<SpellShortcutFrame> SpellList
        {
            get
            {
                return spellList;
            }
        }
    }
}
