﻿using OpenTK;
using HAJE.MMO.Client.UI.Tool;
using HAJE.MMO.Spell;
using HAJE.MMO.Client.UI.Layout;

namespace HAJE.MMO.Client.UI.Component
{
    public class GeneralSpell : CompositeButton
    {
        public GeneralSpell(Vector2 size)
        {
            contentSize = size;
            instance = NineSliceInfoProvider.Instance;

            normalframe = new NineSliceSprite(instance["spell_button_frame.png"].Name);
            AddChild(normalframe);
            normalframe.ContentSize = size;

            spellImage = new SpriteButton("Resource/Image/Icon/Empty.png", size - new Vector2(4));
            spellImage.ZOrder = 4;
            AddButton(spellImage);
            spellImage.Position = new Vector2(2);

            count = 0;
            countText = new Label(count.ToString(), ResourcePath.Font, 8);
            countText.Visible = false;
            AddChild(countText);

            countText.Anchor = new Vector2(1, 0);

            var countTextLayout = new DockLayout(countText);
            countTextLayout.DockTarget = this;
            countTextLayout.DockPosition = new Vector2(1, 0);
            countTextLayout.Offset = new Vector2(-2, 2);

            ispressed = false;
            isGet = true;

            owner = OwnerType.None;

            isOwnByPlayer = true;
        }

        public SpellInfo Spell
        {
            get
            {
                return spell;
            }
            set
            {
                spell = value;
                SpellSetter(spell);
            }
        }

        private void SpellSetter(SpellInfo input)
        {
            spellImage.UpdateImage(input.IconPath);
            RefreshSpellCount();

            if (input.GainType != GainType.Basic)
                countText.Visible = true;
            else
                countText.Visible = false;
        }

        public override void HandleInput(Input.InputState args)
        {
            base.HandleInput(args);
            if (!IsOwnByPlayer)
                ShowDisabled();
        }

        public override void ShowEnabled()
        {
            base.ShowEnabled();
            ispressed = false;
        }

        public override void ShowOver()
        {
            base.ShowOver();
            ispressed = false;
        }

        public override void ShowDisabled()
        {
            base.ShowDisabled();
            ispressed = false;
        }

        public override void ShowPressed()
        {
            base.ShowPressed();
            ispressed = true;
        }

        public bool IsPressed
        {
            get
            {
                return ispressed;
            }
        }

        public bool IsGet
        {
            get
            {
                return isGet;
            }
            set
            {
                isGet = value;
                if (isGet)
                    Enabled = true;
                else
                    Enabled = false;
            }
        }

        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }

        public bool IsOwnByPlayer
        {
            get
            {
                return isOwnByPlayer;
            }
            set
            {
                isOwnByPlayer = value;
            }
        }

        /// <summary>
        /// value 만큼의 값을 더한다
        /// </summary>
        /// <param name="value"></param>
        public void ChangeSpellCount(int value)
        {
            count += value;
            if (count < 0)
                count = 0;

            RefreshSpellCount();
        }

        public void SetSpellCount(int set)
        {
            count = set;
            if (set < 0)
                count = 0;

            RefreshSpellCount();
        }
        
        private void RefreshSpellCount()
        {
            countText.Text = count.ToString();
        }

        public OwnerType Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        public NineSliceSprite normalframe;
        SpriteButton spellImage;
        NineSliceInfoProvider instance;
        bool ispressed;
        bool isGet;
        SpellInfo spell;
        int count;
        int index;
        Label countText;
        OwnerType owner;
        bool isOwnByPlayer;
    }

    public enum OwnerType
    {
        None,
        SpellBar,
        SpellInventory
    }
}
