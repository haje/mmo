﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Client.UI.Component
{
    public class ResourceBar : CompositeButton
    {
        public ResourceBar(int maxResource, ResourceBarType type)
        {
            MaxResource = maxResource;
            CurrentResource = maxResource;
            CurrentDamagedResource = 0;
            Type = type;
            Frame = new NineSliceButton("resource_bar.png", new Vector2(GameSystem.Viewport.X / 3 * 1.2f, 25));
            contentSize = Frame.ContentSize;
            this.AddButton(Frame);
            BarSetting(type);
            currentResourceDirty = false;
            maxResourceDirty = false;
            damagedResourceDirty = false;
        }

        public ResourceBar(ResourceBarType type)
            : this(100, type)
        {
            MaxResource = 100;
            CurrentResource = 100;
            CurrentDamagedResource = 0;
            Type = type;
            Frame = new NineSliceButton("resource_bar.png", new Vector2(GameSystem.Viewport.X / 3 * 1.2f, 25));
            contentSize = Frame.ContentSize;
            this.AddButton(Frame);
            BarSetting(type);
            currentResourceDirty = false;
            maxResourceDirty = false;
            damagedResourceDirty = false;
        }

        void BarSetting(ResourceBarType type)
        {
            switch (type)
            {
                case ResourceBarType.HP:
                    FillRectangle = new Sprite("Resource/Image/UI/hp_bar.png");
                    FillRectangle.ContentSize = new Vector2(0, 21);
                    FillRectangle.Position = new Vector2(2);
                    Frame.AddChild(FillRectangle);
                    DamagedRectangle = new Rectangle(new Color4(1, 0, 0, 0.3f), Color4.Red, 0, new Vector2(0, Frame.ContentSizeHeight - 4));
                    DamagedRectangle.Position = new Vector2(Frame.ContentSizeWidth - 2, 2);
                    Frame.AddChild(DamagedRectangle);
                    break;
                case ResourceBarType.MP:
                    FillRectangle = new Sprite("Resource/Image/UI/mp_bar.png");
                    FillRectangle.ContentSize = new Vector2(0, 21);
                    FillRectangle.Position = new Vector2(2);
                    Frame.AddChild(FillRectangle);
                    DamagedRectangle = new Rectangle(new Color4(0, 0, 1, 0.3f), Color4.Blue, 0, new Vector2(0, Frame.ContentSizeHeight - 4));
                    DamagedRectangle.Position = new Vector2(Frame.ContentSizeWidth - 2, 2);
                    Frame.AddChild(DamagedRectangle);
                    break;
                default:
                    Debug.Assert(false, "그런 이름의 Resource Bar는 없습니다.");
                    break;
            }
        }

        void resourceChanged(Second deltaTime)
        {
            if (maxResourceDirty)
            {
                FillRectangle.ContentSizeWidth = (CurrentResource * (Frame.ContentSizeWidth - 4)) / MaxResource;
                FillRectangle.UV = new Vector2((float)CurrentResource/MaxResource, 1);
                DamagedRectangle.ContentSizeWidth = (CurrentDamagedResource * (Frame.ContentSizeWidth - 4)) / MaxResource;
                maxResourceDirty = false;
            }
            if (currentResourceDirty)
            {
                FillRectangle.ContentSizeWidth = (CurrentResource * (Frame.ContentSizeWidth - 4)) / MaxResource;
                FillRectangle.UV = new Vector2((float)CurrentResource/MaxResource, 1);
                DamagedRectangle.Position = new Vector2(FillRectangle.ContentSizeWidth, 2);
                DamagedRectangle.ContentSizeWidth = (CurrentDamagedResource * (Frame.ContentSizeWidth - 4)) / MaxResource;

                currentResourceDirty = false;
            }
            if (damagedResourceDirty)
            {
                if (CurrentDamagedResource > 0)
                {
                    CurrentDamagedResource -= 100 * deltaTime;
                    if (CurrentResource < 0)
                    {
                        CurrentResource = 0;
                        DamagedRectangle.Position = new Vector2(FillRectangle.ContentSizeWidth , 2);
                        DamagedRectangle.ContentSizeWidth = 0;
                        damagedResourceDirty = false;
                    }
                    else
                    {
                        DamagedRectangle.ContentSizeWidth = (CurrentDamagedResource * (Frame.ContentSizeWidth - 4)) / MaxResource;
                    }
                }
                else
                {
                    CurrentDamagedResource = 0;
                    DamagedRectangle.Position = new Vector2(FillRectangle.ContentSizeWidth, 2);
                    DamagedRectangle.ContentSizeWidth = 0;
                    damagedResourceDirty = false;
                }
            }
        }

        public void InitializeResource(int resource)
        {
            CurrentResource = resource;
            MaxResource = resource;
            CurrentDamagedResource = 0;
            currentResourceDirty = true;
            maxResourceDirty = true;
            damagedResourceDirty = true;
        }

        public void SetCurrentResource(int resource)
        {
            if(resource > 0)
            {
                CurrentDamagedResource += CurrentResource - resource;
                CurrentResource = resource;
            }
            else
            {
                CurrentDamagedResource += CurrentResource;
                CurrentResource = 0;
            }
            
            currentResourceDirty = true;
            damagedResourceDirty = true;
        }

        public void SetMaxResource(int resource)
        {
            if (resource < CurrentResource)
                CurrentResource = resource;
            MaxResource = resource;
            maxResourceDirty = true;
        }

        public void ConsumeResource(int resource)
        {
            if (CurrentResource - resource >= 0)
            {
                CurrentDamagedResource += resource;
                CurrentResource -= resource;
            }
            else
            {
                CurrentDamagedResource += CurrentResource;
                CurrentResource = 0;
            }

            currentResourceDirty = true;
            damagedResourceDirty = true;
        }

        public override void Update(Second deltaTime)
        {
            resourceChanged(deltaTime);
            base.Update(deltaTime);
        }

        public void SetWidthbyScale(float scale)
        {
            ContentSizeWidth *= scale;
            Frame.SetWidth(ContentSizeWidth);
            FillRectangle.ContentSizeWidth = Frame.ContentSizeWidth - 4;
            DamagedRectangle.Position = new Vector2(Frame.ContentSizeWidth - 2, 2);
        }

        public void SetHeightbyScale(float scale)
        {
            ContentSizeHeight *= scale;
            Frame.SetHeight(ContentSizeHeight);
            FillRectangle.ContentSizeHeight = Frame.ContentSizeHeight - 4;
            DamagedRectangle.ContentSizeHeight = FillRectangle.ContentSizeHeight;
            DamagedRectangle.Position = new Vector2(Frame.ContentSizeWidth - 2, 2);
        }

        NineSliceButton Frame;
        Sprite FillRectangle;
        Rectangle DamagedRectangle;
        bool currentResourceDirty;
        bool maxResourceDirty;
        bool damagedResourceDirty;
        ResourceBarType Type;
        int CurrentResource;
        int MaxResource;
        float CurrentDamagedResource;
        private ResourceBarType resourceBarType;
    }

    public enum ResourceBarType
    {
        HP,
        MP,
    }
}
