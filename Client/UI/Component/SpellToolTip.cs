﻿using HAJE.MMO.Client.UI.Layout;
using HAJE.MMO.Client.UI.Tool;
using HAJE.MMO.Spell;
using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.UI.Component
{
    public class SpellToolTip : Layer
    {
        public SpellToolTip(SpellInfo spell)
        {
            instance = NineSliceInfoProvider.Instance;
            background = new NineSliceSprite(instance["login_button.png"].Name);
            background.ContentSize = new Vector2(200, 70);
            ContentSize = background.ContentSize;
            AddChild(background);

            SpellName = new Label(spell.Name, ResourcePath.Font, 15);
            SpellName.Color = Color4.Black;
            SpellName.Anchor = Anchors.TopLeft;
            AddChild(SpellName);

            var SpellNameLayout = new DockLayout(SpellName);
            SpellNameLayout.DockTarget = this;
            SpellNameLayout.DockPosition = DockPositions.TopLeft;
            SpellNameLayout.Offset = new Vector2(7, -7);

            SpellExplanation = new Label(spell.SpellTooltip, ResourcePath.Font, 10);
            SpellExplanation.Anchor = Anchors.TopLeft;
            SpellExplanation.Color = Color4.Black;
            AddChild(SpellExplanation);

            var SpellExplanationLayout = new DockLayout(SpellExplanation);
            SpellExplanationLayout.DockTarget = SpellName;
            SpellExplanationLayout.DockPosition = DockPositions.BottomLeft;
            SpellExplanationLayout.Offset = new Vector2(0, -40);

            mousePressed = false;
        }

        public void AnchorSetter()
        {
            int AnchorX = 0;
            int AnchorY = 0;

            if (WorldPosition.X > GameSystem.Viewport.X / 2)
                AnchorX = 1;

            if (WorldPosition.Y > GameSystem.Viewport.Y / 2)
                AnchorY = 1;

            Anchor = new Vector2(AnchorX, AnchorY);
        }

        public void SpellChange(SpellInfo spell)
        {
            SpellName.Text = spell.Name;
            SpellExplanation.Text = spell.SpellTooltip;
        }

        public bool MousePressed
        {
            get
            {
                return mousePressed;
            }
            set
            {
                mousePressed = value;
            }
        }

        NineSliceSprite background;
        NineSliceInfoProvider instance;
        Label SpellName;
        Label SpellExplanation;
        bool mousePressed;
    }
}
