﻿namespace HAJE.MMO.Client.UI.Component
{
    public class Scene : Node
    {
        public Scene()
        {
            this.ContentSize = GameSystem.Viewport;
            this.DebugName = "[Scene]";
        }
    }
}
