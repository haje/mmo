﻿using OpenTK;
using HAJE.MMO.Client.UI.Layout;

namespace HAJE.MMO.Client.UI.Component
{
    public class SpellCountBar : Layer
    {
        public SpellCountBar(Vector2 size)
        {
            spellCount = 0;
            ContentSize = size;
            spellCountBackground = new Sprite("Resource/Image/UI/spell_count_box.png");
            spellCountBackground.ContentSize = size;
            AddChild(spellCountBackground);

            spellCountText = new Label("SpellCount : " + spellCount.ToString(), ResourcePath.Font, 14);
            spellCountText.Color = OpenTK.Graphics.Color4.Black;
            spellCountText.Anchor = new Vector2(0, 0.5f);
            AddChild(spellCountText);

            var SpellCountTextLayout = new DockLayout(spellCountText);
            SpellCountTextLayout.DockTarget = this;
            SpellCountTextLayout.DockPosition = DockPositions.LeftCenter;
        }

        public int SpellCount
        {
            get
            {
                return spellCount;
            }
        }

        public void ChangeSpellCount(int value)
        {
            spellCount += value;
            if (spellCount < 0)
                spellCount = 0;
            SpellTextRefresh();
        }

        public void SetSpellCount(int set)
        {
            spellCount = set;
            if (spellCount < 0)
                spellCount = 0;
            SpellTextRefresh();
        }

        private void SpellTextRefresh()
        {
            spellCountText.Text = "SpellCount : " + spellCount.ToString();
        }

        Sprite spellCountBackground;
        Label spellCountText;
        int spellCount;
    }
}
