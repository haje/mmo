﻿using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;
using HAJE.MMO.Client.Input;

namespace HAJE.MMO.Client.UI.Component
{
    public class SpellInventory : Layer
    {
        public SpellInventory(IReadOnlyList<SpellInfo> spells)
        {
            ContentSize = new Vector2(300,400);
            widthCount = 5;
            spellCount = 0;
            Visible = false;

            spellInvetoryBackground = new Sprite("Resource/Image/UI/spell_inventory.png");
            spellInvetoryBackground.ContentSize = ContentSize;
            AddChild(spellInvetoryBackground);
            
            childLayer = new Layer();
            childLayer.ContentSize = ContentSize;
            AddChild(childLayer);

            allSpellDoubleList = new List<List<GeneralSpell>>();
            allSpellSingleList = new List<GeneralSpell>();

            foreach (SpellInfo s in spells)
                SpellAdd(s);

            spellInventoryCountBar = new SpellCountBar(new Vector2(ContentSizeWidth, 50));
            spellInventoryCountBar.Anchor = new Vector2(0, 1);
            AddChild(spellInventoryCountBar);
        }

        public void SpellAdd(SpellInfo spell)
        {
            if (spellCount % widthCount == 0)
                allSpellDoubleList.Add(new List<GeneralSpell>());

            GeneralSpell childSpell = new GeneralSpell(new Vector2(50, 50));
            childSpell.Spell = spell;
            childSpell.Anchor = new Vector2(0,1);
            childSpell.Position = new Vector2(10 + (spellCount % widthCount) * (childSpell.ContentSizeWidth + 5),
                ContentSizeHeight - 10 - ((allSpellDoubleList.Count - 1) * (childSpell.ContentSizeHeight + 5)));

            childSpell.Index = allSpellSingleList.Count;
            childSpell.Owner = OwnerType.SpellInventory;

            if (spell.GainType != GainType.Basic)
                childSpell.IsOwnByPlayer = false;
            
            allSpellDoubleList[allSpellDoubleList.Count - 1].Add(childSpell);
            allSpellSingleList.Add(childSpell);
            childLayer.AddChild(childSpell);
            spellCount += 1;
        }

        public void SpellDelete(int row, int col)
        {
            try
            {
                allSpellSingleList.RemoveAt(row * widthCount + col);
                SpellInventorySort();
            }
            catch 
            {
                throw new Exception("그 index에 spell이 없습니다.");
            }
        }

        public void SpellReplace(int index, SpellInfo inputSpell, bool isEnable)
        {
            if (AllSpellSingList[index] != null)
            {
                AllSpellSingList[index].Spell = inputSpell;
                AllSpellSingList[index].Enabled = isEnable;
                SpellInventorySort();
            }
            else
            {
                throw new Exception("그 index에 spell이 없습니다.");
            }
        }

        public SpellInfo GetSpell(int row, int col)
        {
            try
            {
                return allSpellDoubleList[row][col].Spell;
            }
            catch
            {
                throw new Exception("그 index에 spell이 없습니다.");
            }
        }

        private void SpellInventorySort()
        {
            spellCount = 0;
            childLayer.RemoveAllChild();
            allSpellDoubleList = new List<List<GeneralSpell>>();
            for(int i = 0; i < allSpellSingleList.Count; i++)
            {
                if(i % widthCount == 0)
                {
                    allSpellDoubleList.Add(new List<GeneralSpell>());
                }

                allSpellSingleList[i].Position = new Vector2(10 + (spellCount % widthCount) * (allSpellSingleList[i].ContentSizeWidth + 5),
                ContentSizeHeight - 10 - ((allSpellDoubleList.Count - 1) * (allSpellSingleList[i].ContentSizeHeight + 5)));
                allSpellSingleList[i].Index = spellCount;
                allSpellSingleList[i].Owner = OwnerType.SpellInventory;
                allSpellDoubleList[i / widthCount].Add(allSpellSingleList[i]);
                childLayer.AddChild(allSpellSingleList[i]);
                spellCount += 1;
            }
        }

        public IReadOnlyList<GeneralSpell> AllSpellSingList
        {
            get
            {
                return allSpellSingleList;
            }
        }
        
        int widthCount;
        int spellCount;
        Layer childLayer;
        List<List<GeneralSpell>> allSpellDoubleList;
        List<GeneralSpell> allSpellSingleList;
        Sprite spellInvetoryBackground;
        SpellCountBar spellInventoryCountBar;
    }
}
