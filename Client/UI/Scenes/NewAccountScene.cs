﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Network.Packet;
using OpenTK;
using OpenTK.Graphics;
using System;

namespace HAJE.MMO.Client.UI.Scenes
{
    public class NewAccountScene : Scene
    {
        public NewAccountScene()
        {
            backgroundLayer = new ColorLayer(Color4.Gray);
            AddChild(backgroundLayer);

            //Page title 가운데정렬
            registrationLabel = new Label("REGISTRATION" ,fontfolderPath + "NanumBarunGothic.ttf", 40);
            registrationLabel.Anchor = new Vector2(0.5f, 0.5f);
            registrationLabel.Position = new Vector2(GameSystem.Viewport.X / 2, GameSystem.Viewport.Y - GameSystem.Viewport.Y/9);
            registrationLabel.Color = Color4.Black;
            AddChild(registrationLabel);

            //Username 
            usernameLabel = new Label("Username", fontfolderPath + "NanumBarunGothic.ttf", 20);
            usernameLabel.Position = new Vector2(GameSystem.Viewport.X / 2 - boxWidth / 2, registrationLabel.Position.Y - 130);
            usernameLabel.Color = Color4.Black;
            AddChild(usernameLabel);

            usernameInputCover = new Rectangle(Color4.White, Color4.Black, 1.5f, new Vector2(boxWidth, boxHeight));
            usernameInputCover.Position = new Vector2(GameSystem.Viewport.X / 2 - boxWidth / 2, usernameLabel.Position.Y - boxHeight);
            AddChild(usernameInputCover);

            usernameInput = new TextInput("Username", fontfolderPath + "NanumBarunGothic.ttf", 20);
            usernameInput.Enabled = true;
            usernameInput.Focused = true;
            usernameInput.FocusColor = Color4.Black;
            usernameInput.EnableColor = Color4.Black;
            usernameInput.MaxCharacter = 24;
            usernameInputCover.AddChild(usernameInput);

            //Email Address
            emailLabel = new Label("Email Address", fontfolderPath + "NanumBarunGothic.ttf", 20);
            emailLabel.Position = new Vector2(usernameLabel.Position.X, usernameLabel.Position.Y - boxHeight * 2.5f);
            emailLabel.Color = Color4.Black;
            AddChild(emailLabel);

            emailInputCover = new Rectangle(Color4.White, Color4.Black, 1.5f, new Vector2(boxWidth, boxHeight));            
            emailInputCover.Position = new Vector2(usernameInputCover.Position.X, emailLabel.Position.Y - boxHeight);
            AddChild(emailInputCover);

            emailInput = new TextInput("Email Address", fontfolderPath + "NanumBarunGothic.ttf", 20);
            emailInput.Enabled = true;
            emailInput.FocusColor = Color4.Black;
            emailInput.EnableColor = Color4.Black;
            emailInput.MaxCharacter = 24;
            emailInputCover.AddChild(emailInput);

            //Password
            passwordLabel = new Label("Password (more than 4 letters)", fontfolderPath + "NanumBarunGothic.ttf", 20);
            passwordLabel.Position = new Vector2(emailLabel.Position.X, emailLabel.Position.Y - boxHeight * 2.5f);
            passwordLabel.Color = Color4.Black;
            AddChild(passwordLabel);

            passwordInputCover = new Rectangle(Color4.White, Color4.Black, 1.5f, new Vector2(590, 40));
            passwordInputCover.Position = new Vector2(emailInputCover.Position.X, passwordLabel.Position.Y - boxHeight);
            AddChild(passwordInputCover);

            passwordInput = new TextInput("Password", fontfolderPath + "NanumBarunGothic.ttf", 20);
            passwordInput.Enabled = true;
            passwordInput.FocusColor = Color4.Black;
            passwordInput.EnableColor = Color4.Black;
            passwordInput.MaxCharacter = 24;
            passwordInput.Password = true;
            passwordInputCover.AddChild(passwordInput);

            //PasswordRetype
            passwordRetypeLabel = new Label("Repeat Password", fontfolderPath + "NanumBarunGothic.ttf", 20);
            passwordRetypeLabel.Position = new Vector2(passwordLabel.Position.X, passwordLabel.Position.Y - boxHeight * 2.5f);
            passwordRetypeLabel.Color = Color4.Black;
            AddChild(passwordRetypeLabel);

            passwordRetypeInputCover = new Rectangle(Color4.White, Color4.Black, 1.5f, new Vector2(590, 40));
            passwordRetypeInputCover.Position = new Vector2(passwordInputCover.Position.X, passwordRetypeLabel.Position.Y - boxHeight);
            AddChild(passwordRetypeInputCover);

            passwordRetypeInput = new TextInput("Password", fontfolderPath + "NanumBarunGothic.ttf", 20);
            passwordRetypeInput.Enabled = true;
            passwordRetypeInput.FocusColor = Color4.Black;
            passwordRetypeInput.EnableColor = Color4.Black;
            passwordRetypeInput.MaxCharacter = 24;
            passwordRetypeInput.Password = true;
            passwordRetypeInputCover.AddChild(passwordRetypeInput);

            //RegisterButton
            registerButton = new CompositeButton();
            registerButton.ContentSize = new Vector2(200, 50);
            registerButton.Position = new Vector2(GameSystem.Viewport.X / 2 - 250, passwordRetypeInputCover.Position.Y - 100);
            registerButton.OnButton += registerButton_OnButton;
            AddChild(registerButton);

            registerBackgroundButton = new NineSliceButton("login_button.png", registerButton.ContentSize);
            registerBackgroundButton.ButtonImageSetting("login_button.png", "login_button_disable.png", "login_button_over.png", "login_button_press.png");
            registerButton.AddButton(registerBackgroundButton);

            registerTextButton = new TextButton("Register", fontfolderPath + "NanumBarunGothic.ttf", 20);
            registerTextButton.Anchor = new Vector2(0.5f, 0.5f);
            registerTextButton.Position = registerButton.ContentSize / 2;
            registerTextButton.OverColor = Color4.White;
            registerButton.AddButton(registerTextButton);

            //CancelButton
            cancelButton = new CompositeButton();
            cancelButton.ContentSize = new Vector2(200, 50);
            cancelButton.Position = new Vector2(GameSystem.Viewport.X / 2 + 250, passwordRetypeInputCover.Position.Y - 100);
            cancelButton.OnButton += cancelButton_OnButton;
            cancelButton.Anchor = new Vector2(1.0f, 0f);
            AddChild(cancelButton);

            cancelBackgroundButton = new NineSliceButton("login_button.png", cancelButton.ContentSize);
            cancelBackgroundButton.ButtonImageSetting("login_button.png", "login_button_disable.png", "login_button_over.png", "login_button_press.png");
            cancelButton.AddButton(cancelBackgroundButton);

            cancelTextButton = new TextButton("Back", fontfolderPath + "NanumBarunGothic.ttf", 20);
            cancelTextButton.Anchor = new Vector2(0.5f, 0.5f);
            cancelTextButton.Position = registerButton.ContentSize / 2;
            cancelTextButton.OverColor = Color4.White;
            cancelButton.AddButton(cancelTextButton);

            waitpopup = new UI.Component.WaitPopupLayer("Wait please");

            inputGroup = new TextInputGroup(usernameInput, emailInput, passwordInput,passwordRetypeInput);
        }

        void registerButton_OnButton(GeneralButton button)
        {
            if (usernameInput.Text == "")
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("You need a username");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if (emailInput.Text == "")
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("You need an email address");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if (passwordInput.Text == "")
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("You need a password");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if (passwordRetypeInput.Text == "")
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("You need to confirm\nyour password");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if (passwordInput.Text != passwordRetypeInput.Text)
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("Your passwords must\nmatch each other");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else
            {
                AddChild(waitpopup);
                try
                {
                    var request = GameSystem.NetworkContext.GetLoginContext().NewAccountRequest();
                    request.OnNewAccountSuccess += newAccountService_OnNewAccountSuccess;
                    request.OnNewAccountFailed += newAccountService_OnNewAccountFailed;
                    request.Send(emailInput.Text, passwordInput.Text, usernameInput.Text);
                }
                catch (InvalidOperationException e)
                {
                    GameSystem.UISystem.ShowError(e.Message, ErrorType.Ignore);
                    RemoveChild(waitpopup);
                }
            }
        }

        void cancelButton_OnButton(GeneralButton button)
        {
            LoginScene loginScene = new LoginScene();
            GameSystem.UISystem.ReplaceScene(loginScene);
        }

        void newAccountService_OnNewAccountSuccess(NewAccountResponseCode responseCode)
        {
            RemoveChild(waitpopup);
            ConfirmPopupLayer successLayer = ConfirmPopupLayer.CreateSucceess("Sucessfully Registered");
            AddChild(successLayer);
            successLayer.SetCloseAsConfirmed();
            successLayer.AddConfirmHandler(confirmButton_OnButton);
        }

        void confirmButton_OnButton(GeneralButton button)
        {
            LoginScene loginScene = new LoginScene();
            GameSystem.UISystem.ReplaceScene(loginScene);
        }

        void newAccountService_OnNewAccountFailed(NewAccountResponseCode responseCode)
        {
            RemoveChild(waitpopup);
            if(responseCode == MMO.Network.Packet.NewAccountResponseCode.EmailAlreadyExists)
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("Account already exists");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if (responseCode == MMO.Network.Packet.NewAccountResponseCode.UserNameAlreadyExists)
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("User name already exists");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if (responseCode == MMO.Network.Packet.NewAccountResponseCode.WebServerNotFound)
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("Web Server Not Found");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            else if(responseCode == MMO.Network.Packet.NewAccountResponseCode.Unknown)
            {
                ConfirmPopupLayer failLayer = ConfirmPopupLayer.CreateFail("Invalid email or password");
                AddChild(failLayer);
                failLayer.SetCloseAsConfirmed();
            }
            
        }

        const int boxWidth = 600;
        const int boxHeight = 40;

        string fontfolderPath = "Resource/Font/";
        string xmlfolderPath = "Resource/XML/";
        
        ColorLayer backgroundLayer;
        Label registrationLabel;
        Label usernameLabel;
        Rectangle usernameInputCover;
        TextInput usernameInput;
        Label emailLabel;
        Rectangle emailInputCover;
        TextInput emailInput;
        Label passwordLabel;
        Rectangle passwordInputCover;
        TextInput passwordInput;
        Label passwordRetypeLabel;
        Rectangle passwordRetypeInputCover;
        TextInput passwordRetypeInput;
        CompositeButton registerButton;
        NineSliceButton registerBackgroundButton;
        TextButton registerTextButton;
        CompositeButton cancelButton;
        NineSliceButton cancelBackgroundButton;
        TextButton cancelTextButton;
        WaitPopupLayer waitpopup;


        TextInputGroup inputGroup;
    }
}
