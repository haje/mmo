﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Layout;
using HAJE.MMO.Client.GamePlay;
using HAJE.MMO.Spell;
using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.UI.Scenes
{
    public class MainGameUIScene : Scene
    {
        public MainGameUIScene(PlayContext playContext)
        {
            this.playContext = playContext;
            var resourceBarSize = new Vector2(
                ContentSizeWidth * 0.45f,
                25
            );

            hpBar = new ResourceBar(ResourceBarType.HP);
            hpBar.Anchor = Anchors.TopLeft;
            AddChild(hpBar);

            var hpBarLayout = new DockLayout(hpBar);
            hpBarLayout.DockTarget = this;
            hpBarLayout.DockPosition = DockPositions.TopLeft;
            hpBarLayout.Offset = new Vector2(15, -15);

            mpBar = new ResourceBar(ResourceBarType.MP);
            mpBar.Anchor = Anchors.TopLeft;
            AddChild(mpBar);

            var mpBarLayout = new DockLayout(mpBar);
            mpBarLayout.DockTarget = hpBar;
            mpBarLayout.DockPosition = DockPositions.BottomLeft;
            mpBarLayout.Offset = new Vector2(0, -30);


            spellbar = new SpellBar();
            spellbar.Anchor = Anchors.BottomCenter;
            foreach (SpellInfo s in playContext.HostingCharacter.ActiveSpellList)
            {
                spellbar.AddSpell(s);
            }
            spellbar.SetSpellFirst();
            AddChild(spellbar);

            var spellBarLayout = new DockLayout(spellbar);
            spellBarLayout.DockTarget = this;
            spellBarLayout.DockPosition = DockPositions.BottomCenter;
            spellBarLayout.Offset = new Vector2(0, 15);
            
            castingBar = new CastingBar();
            castingBar.Anchor = Anchors.BottomCenter;
            AddChild(castingBar);

            var castingBarLayout = new DockLayout(castingBar);
            castingBarLayout.DockTarget = spellbar;
            castingBarLayout.DockPosition = DockPositions.TopCenter;
            castingBarLayout.Offset = new Vector2(0, 20);
            
            spellInventory = new SpellInventory(playContext.HostingCharacter.SpellBookList);
            spellInventory.Anchor = Anchors.RightCenter;
            AddChild(spellInventory);

            var spellInvetoryLayout = new DockLayout(spellInventory);
            spellInvetoryLayout.DockTarget = this;
            spellInvetoryLayout.DockPosition = Anchors.RightCenter;
            spellInvetoryLayout.Offset = new Vector2(-30,0);

            interactUI = new MainGameInteractUI(playContext);
            interactUI.ZOrder = 10;
            AddChild(interactUI);

            if (Configuration.Instance.ConnectionTarget == ConnectionTarget.Local)
            {
                var versionLabel = new Label("Local Server. " + BuildInfo.BuildStamp, ResourcePath.Font, 10);
                versionLabel.Anchor = new Vector2(1, 0);
                versionLabel.Position = new Vector2(ContentSizeWidth, 0);
                versionLabel.Color = Color4.Red;
                AddChild(versionLabel);
            }
        }

        PlayContext playContext;
        ResourceBar hpBar;
        ResourceBar mpBar;
        CastingBar castingBar;
        SpellInventory spellInventory;
        SpellBar spellbar;
        MainGameInteractUI interactUI;

        public ResourceBar HPBar
        {
            get
            {
                return hpBar;
            }
        }

        public ResourceBar MPBar
        {
            get
            {
                return mpBar;
            }
        }

        public CastingBar CastingBar
        {
            get
            {
                return castingBar;
            }
        }

        public SpellBar SpellBar
        {
            get
            {
                return spellbar;
            }
        }

        public SpellInventory SpellInventory
        {
            get
            {
                return spellInventory;
            }
        }

        public MainGameInteractUI InteractUI
        {
            get { return interactUI; }
        }
    }
}
