﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Layout;
using HAJE.MMO.Client.UI.Tool;
using OpenTK;
using OpenTK.Graphics;
using System;

namespace HAJE.MMO.Client.UI.Scenes
{
    public class LoginScene : Scene
    {
        public LoginScene()
        {
            var backgroundLayer = new ColorLayer(Color4.Gray);
            AddChild(backgroundLayer);

            var logoImage = new Sprite(resourcefolderPath + "logo_haje.png");
            logoImage.ContentSize = new Vector2(GameSystem.Viewport.X / 1.5f, GameSystem.Viewport.Y / 2.5f);
            logoImage.Scale = 0.7f;
            logoImage.Anchor = new Vector2(0.5f, 0.5f);
            AddChild(logoImage);
            new RelativePositionLayout(logoImage, this, new Vector2(0.5f, 0.75f), Vector2.Zero);

            var inputCover = new Rectangle(Color4.White, Color4.Black, 1.5f, new Vector2(logoImage.ContentSizeWidth * 0.7f, 40));
            inputCover.Anchor = Anchors.Center;
            AddChild(inputCover);
            new RelativePositionLayout(inputCover, this, new Vector2(0.5f, 0.35f), Vector2.Zero);

            userNameInput = new TextInput("플레이어 이름", ResourcePath.Font, 20);
            userNameInput.Enabled = true;
            userNameInput.Focused = true;
            userNameInput.FocusColor = Color4.Black;
            userNameInput.EnableColor = Color4.Black;
            userNameInput.MaxCharacter = 10;
            userNameInput.OnConfirmKey += delegate { Login(); };
            inputCover.AddChild(userNameInput);

            var userNameLabel = new Label("플레이어 이름을 입력하세요", ResourcePath.Font, 15);
            userNameLabel.Color = Color4.Black;
            userNameLabel.Anchor = Anchors.BottomLeft;
            AddChild(userNameLabel);
            new RelativePositionLayout(userNameLabel, userNameInput, DockPositions.TopLeft, new Vector2(0, 10));

            startButton = new CompositeButton();
            startButton.ContentSize = new Vector2(logoImage.ContentSizeWidth * 0.7f / 2 - 20, 50);
            startButton.X = ContentSizeWidth / 2;
            startButton.Y = inputCover.Y - 50;
            startButton.Anchor = Anchors.TopCenter;
            startButton.OnButton += delegate { Login(); };
            AddChild(startButton);

            var loginBackgroundButton = new NineSliceButton("login_button.png", startButton.ContentSize);
            loginBackgroundButton.ButtonImageSetting("login_button.png", "login_button_disable.png", "login_button_over.png", "login_button_press.png");
            startButton.AddButton(loginBackgroundButton);

            var loginTextButton = new TextButton("게임 시작", ResourcePath.Font, 20);
            loginTextButton.Anchor = Anchors.Center;
            loginTextButton.Position = startButton.ContentSize / 2;
            loginTextButton.OverColor = Color4.White;
            startButton.AddButton(loginTextButton);


            var versionInfo = new Label(BuildInfo.BuildStamp, ResourcePath.Font, 10);
            versionInfo.Color = Color4.DarkRed;
            versionInfo.Position = new Vector2(ContentSizeWidth, 0);
            versionInfo.Anchor = new Vector2(1, 0);
            AddChild(versionInfo);

            waitpopup = new UI.Component.WaitPopupLayer("서버 통신 중");
        }

        void Login()
        {
            if (string.IsNullOrWhiteSpace(userNameInput.Text))
            {
                GameSystem.UISystem.ShowError("이름을 입력해 주세요.", ErrorType.Ignore);
            }
            else
            {
                AddChild(waitpopup);
                try
                {
                    var request = GameSystem.NetworkContext.GetLoginContext().CreateLoginRequest();
                    request.OnLoginSuccess += (response) =>
                    {
                        GameSystem.SetGameLoop(new GameLoop.ConnectToField(
                            response.FieldIp, response.FieldPort,
                            response.PlayerId, response.FieldToken));
                        GameSystem.NetworkContext.CloseLoginContext();
                    };
                    request.SendSuper(userNameInput.Text);
                }
                catch (InvalidOperationException e)
                {
                    GameSystem.UISystem.ShowError(e.Message, ErrorType.Ignore);
                    RemoveChild(waitpopup);
                }
            }
        }

        string resourcefolderPath = "Resource/Image/UI/";

        TextInput userNameInput;
        CompositeButton startButton;
        WaitPopupLayer waitpopup;
    }
}
