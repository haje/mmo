﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Layout;
using OpenTK;
using OpenTK.Graphics;

namespace HAJE.MMO.Client.UI.Scenes
{
    public class LoadingScene : Scene
    {
        public LoadingScene()
        {
            Sprite sprite = new Sprite("Resource/Image/UI/loading.png");
            AddChild(sprite);
            FillParentLayout layout = new FillParentLayout(sprite, this);

            if (!BuildInfo.DeployBuild)
            {
                var serverInfo = new Label("Connection Target: " + Configuration.Instance.ConnectionTarget, ResourcePath.Font, 10);
                serverInfo.Color = Color4.DarkRed;
                serverInfo.Position = new Vector2(0, 0);
                serverInfo.Anchor = new Vector2(0, 0);
                AddChild(serverInfo);

                var versionInfo = new Label(BuildInfo.BuildStamp, ResourcePath.Font, 10);
                versionInfo.Color = Color4.DarkRed;
                versionInfo.Position = new Vector2(ContentSizeWidth, 0);
                versionInfo.Anchor = new Vector2(1, 0);
                AddChild(versionInfo);
            }
        }
    }
}
