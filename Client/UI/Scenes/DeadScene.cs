﻿using HAJE.MMO.Client.UI.Component;
using HAJE.MMO.Client.UI.Layout;
using System;
using OpenTK;
using OpenTK.Graphics;
using HAJE.MMO.Client.GamePlay;

namespace HAJE.MMO.Client.UI.Scenes
{
    public class DeadScene : Scene
    {
        public DeadScene()
        {
            blockLayer = new InputBlockLayer();
            AddChild(blockLayer);

            backgroundLayer = new ColorLayer(Color4.Red);
            backgroundLayer.thisColor.A = 0.4f;
            AddChild(backgroundLayer);

            deadMessage = new Label("당신은 죽었습니다", "Resource/Font/NanumBarunGothic.ttf", 80);
            deadMessage.Anchor = new Vector2(0.5f, 0.5f);
            AddChild(deadMessage);

            deadMessagePositionLayout = new RelativePositionLayout(deadMessage, this, new Vector2(0.5f, 0.77f), Vector2.Zero);

            okCompositeButton = new CompositeButton();
            okCompositeButton.ContentSize = new Vector2(300, 100);
            okCompositeButton.Position = new Vector2(300, 300);
            okCompositeButton.Anchor = new Vector2(0.5f, 0.5f);
            okCompositeButton.OnButton += okCompositeButton_OnButton;
            AddChild(okCompositeButton);

            okButtonPositionLayout = new RelativePositionLayout(okCompositeButton, deadMessage, new Vector2(-0.02f, -3f), Vector2.Zero);

            okNineSliceButton = new NineSliceButton("login_button.png", okCompositeButton.ContentSize);
            okNineSliceButton.ButtonImageSetting("login_button.png", "login_button_disable.png", "login_button_over.png", "login_button_press.png");
            okCompositeButton.AddButton(okNineSliceButton);

            okTextButton = new TextButton("다시 시작","Resource/Font/NanumBarunGothic.ttf",20);
            okTextButton.ButtonColorSetting(Color4.Black, Color4.Gray, Color4.White, Color4.Black);
            okTextButton.Anchor = new Vector2(0.5f, 0.5f);
            okTextButton.Position = okCompositeButton.ContentSize / 2;
            okCompositeButton.AddButton(okTextButton);            
        }

        void okCompositeButton_OnButton(GeneralButton button)
        {
            GameSystem.NetworkContext.CloseFieldContext();
            GameSystem.SetGameLoop(new GameLoop.Login());
        }

        InputBlockLayer blockLayer;
        ColorLayer backgroundLayer;
        Label deadMessage;
        CompositeButton okCompositeButton;
        NineSliceButton okNineSliceButton;
        TextButton okTextButton;

        public RelativePositionLayout deadMessagePositionLayout { get; set; }
        public RelativePositionLayout okButtonPositionLayout { get; set; }
    }
}
