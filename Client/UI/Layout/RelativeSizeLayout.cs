﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;

namespace HAJE.MMO.Client.UI.Layout
{
    public class RelativeSizeLayout
    {
        private readonly Node owner;
        private readonly Node target;
        private Vector2 relativeSize;
        public Vector2 RelativeSize
        {
            get
            {
                return relativeSize;
            }
            set
            {
                if (value == relativeSize)
                    return;

                relativeSize = value;
                UpdateRelativeSize();
            }
        }

        public RelativeSizeLayout(Node owner, Node target, Vector2 relativeSize)
        {
            this.owner = owner;
            owner.OnContentSizeChanged += (node) => UpdateRelativeSize();

            this.target = target;
            this.relativeSize = relativeSize;

            UpdateRelativeSize();
        }

        private void UpdateRelativeSize()
        {
            owner.ScaleX = target.ContentSizeWidth / owner.ContentSizeWidth * relativeSize.X;
            owner.ScaleY = target.ContentSizeHeight / owner.ContentSizeHeight * relativeSize.Y;
        }
    }
}
