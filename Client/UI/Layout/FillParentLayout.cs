﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;

namespace HAJE.MMO.Client.UI.Layout
{
    public class FillParentLayout : RelativeSizeLayout
    {
        public FillParentLayout(Node owner, Node target) : base(owner, target, Vector2.One)
        {
        }
    }
}
