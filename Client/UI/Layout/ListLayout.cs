﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;
using System.Linq;

namespace HAJE.MMO.Client.UI.Layout
{
    public class ListLayout
    {
        public enum ListDirection
        {
            Up,
            Down,
            Left,
            Right
        }

        private readonly Node owner;
        private float padding;
        public float Padding
        {
            get
            {
                return padding;
            }
            set
            {
                if (value == padding)
                    return;

                padding = value;
                UpdateList();
            }
        }
        private ListDirection direction;
        public ListDirection Direction
        {
            get
            {
                return direction;
            }
            set
            {
                if (value == direction)
                    return;

                direction = value;
                UpdateList();
            }
        }

        public ListLayout(Node owner, float padding, ListDirection direction)
        {
            this.owner = owner;
            owner.OnChildListChanged += (node) => UpdateList();
            owner.OnContentSizeChanged += (node) => UpdateList();
            owner.OnTransformChanged += (node) => UpdateList();

            this.padding = padding;
            this.direction = direction;

            UpdateList();
        }

        private void UpdateList()
        {
            Vector2 pos = owner.Position;
            switch (direction)
            {
                case ListDirection.Down:
                    foreach (Node child in owner.Children)
                    {
                        child.Position = pos;
                        pos.Y -= child.ContentSizeHeight * child.ScaleY + padding;
                    }
                    break;

                case ListDirection.Up:
                    foreach (Node child in owner.Children.Reverse())
                    {
                        child.Position = pos;
                        pos.Y -= child.ContentSizeHeight * child.ScaleY + padding;
                    }
                    break;

                case ListDirection.Right:
                    foreach (Node child in owner.Children)
                    {
                        child.Position = pos;
                        pos.X += child.ContentSizeWidth * child.ScaleX + padding;
                    }
                    break;

                case ListDirection.Left:
                    foreach (Node child in owner.Children.Reverse())
                    {
                        child.Position = pos;
                        pos.X += child.ContentSizeWidth * child.ScaleX + padding;
                    }
                    break;

            }
        }
    }
}
