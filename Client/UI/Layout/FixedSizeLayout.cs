﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;

namespace HAJE.MMO.Client.UI.Layout
{
    public class FixedSizeLayout
    {
        private readonly Node owner;
        private float width;
        public float Width
        {
            get
            {
                return width;
            }
            set
            {
                if (value == width)
                    return;

                width = value;
                UpdateScale();
            }
        }
        private float height;
        public float Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value == height)
                    return;

                height = value;
                UpdateScale();
            }
        }

        public FixedSizeLayout(Node owner, float width, float height)
        {
            this.owner = owner;
            owner.OnContentSizeChanged += (node) => UpdateScale();

            this.width = width;
            this.height = height;

            UpdateScale();
        }

        private void UpdateScale()
        {
            Vector2 contentSize = owner.ContentSize;

            owner.ScaleX = Width / contentSize.X;
            owner.ScaleY = Height / contentSize.Y;
        }
    }
}
