﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;

namespace HAJE.MMO.Client.UI.Layout
{
    public class DockLayout
    {
        private readonly Node owner;
        private Node dockTarget;
        public Node DockTarget
        {
            get
            {
                return dockTarget;
            }
            set
            {
                dockTarget = value;
                UpdatePosition();
            }
        }

        private Vector2 dockPosition;
        public Vector2 DockPosition
        {
            get
            {
                return dockPosition;
            }
            set
            {
                if (value == dockPosition)
                    return;

                dockPosition = value;
                UpdatePosition();
            }
        }
        private Vector2 offset;
        public Vector2 Offset
        {
            get
            {
                return offset;
            }
            set
            {
                if (value == offset)
                    return;

                offset = value;
                UpdatePosition();
            }
        }

        public DockLayout(Node owner, Node dockTarget, Vector2 dockPosition, Vector2 offset)
        {
            this.owner = owner;
            owner.OnContentSizeChanged += (node) => UpdatePosition();

            this.dockTarget = dockTarget;
            dockTarget.OnContentSizeChanged += (node) => UpdatePosition();
            dockTarget.OnTransformChanged += (node) => UpdatePosition();

            this.dockPosition = dockPosition;
            this.offset = offset;

            UpdatePosition();
        }

        public DockLayout(Node owner)
        {
            this.owner = owner;
            this.offset = Vector2.Zero;
            this.dockPosition = Vector2.Zero;
        }

        private void UpdatePosition()
        {
            if (dockTarget == null || owner == null) return;

            Vector2 targetPos;
            targetPos = new Vector2(dockTarget.ScaleX * dockTarget.ContentSizeWidth * dockPosition.X, dockTarget.ScaleY * dockTarget.ContentSizeHeight * dockPosition.Y);
            owner.WorldPosition = dockTarget.NodeToWorld(targetPos) + offset;
        }
    }

    public static class DockPositions
    {
        public static readonly Vector2 TopLeft = new Vector2(0, 1);
        public static readonly Vector2 TopCenter = new Vector2(0.5f, 1);
        public static readonly Vector2 BottomLeft = new Vector2(0, 0);
        public static readonly Vector2 BottomCenter = new Vector2(0.5f, 0);
        public static readonly Vector2 LeftCenter = new Vector2(0, 0.5f);
        public static readonly Vector2 Center = new Vector2(0.5f, 0.5f);
    }

    public static class Anchors
    {
        public static readonly Vector2 TopLeft = new Vector2(0, 1);
        public static readonly Vector2 TopCenter = new Vector2(0.5f, 1);
        public static readonly Vector2 BottomLeft = new Vector2(0, 0);
        public static readonly Vector2 BottomCenter = new Vector2(0.5f, 0);
        public static readonly Vector2 BottomRight = new Vector2(1, 0);
        public static readonly Vector2 RightCenter = new Vector2(1, 0.5f);
        public static readonly Vector2 Center = new Vector2(0.5f, 0.5f);
    }
}
