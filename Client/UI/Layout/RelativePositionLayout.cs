﻿using HAJE.MMO.Client.UI.Component;
using OpenTK;

namespace HAJE.MMO.Client.UI.Layout
{
    public class RelativePositionLayout
    {
        private readonly Node owner;
        private readonly Node target;
        private Vector2 relativePosition;
        public Vector2 RelativePosition
        {
            get
            {
                return relativePosition;
            }
            set
            {
                if (value == relativePosition)
                    return;

                relativePosition = value;
                UpdateRelativePosition();
            }
        }
        private Vector2 offset;
        public Vector2 Offset
        {
            get
            {
                return offset;
            }
            set
            {
                if (value == offset)
                    return;

                offset = value;
                UpdateRelativePosition();
            }
        }

        public RelativePositionLayout(Node owner, Node target, Vector2 relativePosition, Vector2 offset)
        {
            this.owner = owner;
            owner.OnContentSizeChanged += (node) => UpdateRelativePosition();

            this.target = target;

            this.relativePosition = relativePosition;
            this.offset = offset;

            UpdateRelativePosition();
        }

        private void UpdateRelativePosition()
        {
            Vector2 targetPos;
            targetPos = target.Scale * target.ContentSize * relativePosition;
            targetPos += offset;
            owner.WorldPosition = target.NodeToWorld(targetPos);
        }
    }
}
