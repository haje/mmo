﻿using System;

namespace HAJE.MMO.Client
{
    static class Entry
    {
        [STAThread]
        static void Main(String[] args)
        {
            Configuration.Load();
            GameSystem.Initialize(true);
            GameSystem.Unload();            
        }
    }
}
