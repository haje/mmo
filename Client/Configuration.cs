﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HAJE.MMO.Client
{
    public class Configuration
    {
        public static Configuration Instance { get; private set; }
        public static void Load()
        {
            Instance = new Configuration(
                ConfigurationReader.Read<XmlType.Configuration>(
                    "ClientConfig.xml"
                )
            );
        }

        public Configuration(XmlType.Configuration config)
        {
            this.ClientMode = config.clientMode;
            this.ConnectionTarget = config.connectionTarget;
            this.UnitTestEnabled = config.unitTestEnabled;
            if (config.DevServer != null)
            {
                this.IP = config.DevServer.ip;
                this.LoginPort = config.DevServer.loginPort;
            }

            if (!string.IsNullOrEmpty(config.DefaultCommand))
            {
                string[] cmds = config.DefaultCommand.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                DefaultCommand = cmds;
            }
            else
            {
                DefaultCommand = new string[0];
            }
        }

        public readonly ClientMode ClientMode;
        public readonly ConnectionTarget ConnectionTarget;
        public readonly bool UnitTestEnabled;
        public readonly string IP;
        public readonly int LoginPort;
        public readonly string[] DefaultCommand;
    }

    public enum ClientMode
    {
        StandardGame,
        UITest,
        FieldTest,
        AnimationTest,
        LoginTest,
        ParticleTest,
    }

    public enum ConnectionTarget
    {
        Local,
        DevServer,
    }
}

namespace HAJE.MMO.Client.XmlType
{
    public class ServerInfo
    {
        [XmlAttribute]
        public string ip;

        [XmlAttribute]
        public int loginPort;
    }

    public class Configuration
    {
        [XmlAttribute]
        public ClientMode clientMode;

        [XmlAttribute]
        public ConnectionTarget connectionTarget;

        [XmlAttribute]
        public bool unitTestEnabled;

        public ServerInfo DevServer;

        public string DefaultCommand;
    }
}
