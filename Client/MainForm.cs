﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using HAJE.MMO.Client.UI.Component;
using HAJE.MMO;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.MMO.Client
{
    public class MainForm : GameWindow
    {
        //public const int ScreenWidth = 1024;
        //public const int ScreenHeight = 768;
        public const int ScreenWidth = 1024;
        public const int ScreenHeight = 768;

        public MainForm()
            : base(ScreenWidth, ScreenHeight,
                GraphicsMode.Default, "MMO Client", GameWindowFlags.FixedWindow,
                DisplayDevice.Default, 3, 2,
                GraphicsContextFlags.ForwardCompatible | GraphicsContextFlags.Debug)
        {
            X = 100; // TODO: 작업표시줄 가리는 문제 때문에 임시 포지션 넣음. @doss
            Y = 50;
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            var config = Configuration.Instance;
            GameLoopBase runningLoop = null;
            switch (config.ClientMode)
            {
                case ClientMode.AnimationTest:
                    runningLoop = new GameLoop.AnimationTest();
                    break;
                case ClientMode.LoginTest:
                    runningLoop = new GameLoop.LoginTest();
                    break;
                case ClientMode.UITest:
                    runningLoop = new GameLoop.UITest();
                    break;
                case ClientMode.StandardGame:
                    runningLoop = new GameLoop.Login();
                    break;
                case ClientMode.ParticleTest:
                    runningLoop = new GameLoop.ParticleTest();
                    break;
                default:
                    throw new System.ArgumentOutOfRangeException("지원하지 않는 ClientMode입니다.");
            }
            GameSystem.SetGameLoop(runningLoop);

            if (config.DefaultCommand.Length > 0)
            {
                GameSystem.UISystem.Console.ConsoleLayer.Visible = true;
                for (int i = 0; i < config.DefaultCommand.Length; i++)
                {
                    var cmd = config.DefaultCommand[i];
                    GameSystem.UISystem.Console.ExecuteCommand(cmd);
                }
            }
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            GameSystem.Update((Second)e.Time);

            var keyboard = GameSystem.InputState.Keyboard;
            var altPressed = keyboard.IsPressing(Key.AltLeft) || keyboard.IsPressing(Key.AltRight);
            if (altPressed && keyboard.IsPressing(Key.F4))
                GameSystem.Exit();

            base.OnUpdateFrame(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.ClearColor(0f, 0f, 0f, 0f);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GameSystem.Render();
            SwapBuffers();
        }
    }
}
