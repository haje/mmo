﻿using System.Collections.Generic;
using OpenTK;
using HAJE.MMO.Server.Field.Base;
using HAJE.MMO.Server.Field.Object;

namespace HAJE.MMO.Client
{
    [UnitTestAttribute]
    public static class ServerFieldTest
    {
        public static bool RunUnitTest()
        {
            UnitTest.Log.Write("ServerFieldTest Start");

            var field = new Field(1, 1);
            List<FieldObject> objects = new List<FieldObject>();
            for(int nodeIdx = 0; nodeIdx < 16; nodeIdx++)
            {
                FieldObject newObj = new FieldObject(new Vector2(100*((nodeIdx/4)+1), 100 * ((nodeIdx % 4) + 1)), 0, field, FieldObjectType.Player);
                objects.Add(newObj);
            }

            foreach(var obj in objects)
            {
                field.AddObject(obj);
                field.WriteLog(UnitTest.Log);
            }

            List<FieldObject> searchResults = new List<FieldObject>();
            int searchradius = 0;

            #region Test 1 - Position, Round Search
            UnitTest.Log.Write("Test 1 - Position, Round Query");
            Vector2 somePoint = new Vector2(200, 500);
            searchradius = 100;
            UnitTest.Log.Write("Serach point : {0}, radius : {1}", somePoint, searchradius);
            field.Query(somePoint, searchradius, FieldObjectType.All,searchResults);

            int finalCount = 0;
            foreach (var result in searchResults)
            {
                //result.WriteLog(GameSystem.Log);
                if ((somePoint - result.Position).Length < searchradius)
                    finalCount++;
            }
            UnitTest.Log.Write("Test 1 - Final Result Count : {0}", finalCount);
            #endregion

            #region Test 2 - Object, Round Search
            UnitTest.Log.Write("Test 2 - Object, Round Query");
            searchResults.Clear();
            searchradius = 101;
            UnitTest.Log.Write("Serach point : {0}, radius : {1}", objects[4].Position, searchradius);
            field.Query(objects[4], searchradius, FieldObjectType.All, searchResults);

            finalCount = 0;
            foreach (var result in searchResults)
            {
                //result.WriteLog(GameSystem.Log);
                if (result != objects[4] &&
                    (objects[4].Position - result.Position).Length < searchradius)
                    finalCount++;
            }
            UnitTest.Log.Write("Test 2 - Final Result Count : {0}", finalCount);

            /*
            Bitmap bitmap = new Bitmap(1000, 1000);
            foreach(FieldObject obj in objects)
            {
                bitmap.SetPixel((int)obj.Position.X, (int)obj.Position.Y, Color.White);
            }
            bitmap.Save("ServerFieldUnitTest.bmp");
            */
            #endregion


            UnitTest.Log.Write("ServerFieldTest End");

            // TODO : 상황에 맞게 true/false를 리턴하기.
            return true;
        }

    }
}
