﻿using HAJE.MMO.Client.UI.Component;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.MMO.Client
{
    public class DebugOutput
    {
        public DebugOutput(LogDispatcher log)
        {
            InitDefault(log);

#if DEBUG
            layer.Visible = true;
#else
            layer.Visible = false;
#endif
        }

        public void FpsToggle()
        {
            fpsLabel.Visible ^= true;
            frameLabel.Visible = fpsLabel.Visible;
        }
        public void WatchToggle()
        {
            watchNode.Visible ^= true;
        }
        public void LogToggle()
        {
            logNode.Visible ^= true;
        }
        public void DOutToggle()
        {
            layer.Visible ^= true;
        }


        public void SetWatch(string label, object text)
        {
            SetWatch(label, text, Color.Yellow);
        }
        public void SetWatch(string label, object text, Color color)
        {
            SetWatch(label, "Uncategorized", text, color);
        }
        public void SetWatch(string label, string category, object text)
        {
            SetWatch(label, category, text, Color.Yellow);
        }
        public void SetWatch(string label, string category, object text, Color color)
        {
            Tuple<String, String> tuple = new Tuple<String, String>(label, category);
            if (watchLabelList.ContainsKey(tuple))
            {
                watchLabelList[tuple].Text = label + ": " + text;
                watchLabelList[tuple].Color = color;
            }
            else
            {
                leftTop -= lineHeight * 2;
                UI.Component.Label watchLabel = new UI.Component.Label(label + ": " + text, DOFontPath, lineHeight);
                watchLabel.X = left;
                watchLabel.Y = leftTop;
                watchLabel.Color = color;
                watchLabelList.Add(tuple, watchLabel);
                watchNode.AddChild(watchLabel);
            }
        }

        public void RemoveWatch(String category)
        {
            List<Tuple<String, String>> removeCandidate = new List<Tuple<string, string>>();
            foreach (Tuple<String, String> key in watchLabelList.Keys)
                if (key.Item2 == category)
                    removeCandidate.Add(key);

            foreach (Tuple<String, String> key in removeCandidate)
            {
                // 누군가가 여기서 렌더링 업데이트를 하도록 만들어줘야 함.
                watchLabelList.Remove(key);
            }
        }


        public void PrintWarning(string msg)
        {
            for (int i = maxWarnings - 1; i > 0; i--)
            {
                warningLabelList[i].label.Text = warningLabelList[i - 1].label.Text;
                //warningLabelList[i].lifetime = warningLabelList[i - 1].lifetime;
                //아니이게왜안되지
            }
            warningLabelList[0].label.Text = msg;
            //warningLabelList[0].lifetime = 3.0f;

        }

        public void Draw(Rendering.UIRenderer renderer)
        {
            warningLayer.VisitRender(renderer);
            DrawWarnings();

            if (fpsLabel.Visible)
            {
                DrawFPS();
                DrawFrames();
            }

            layer.VisitRender(renderer);
        }

        void DrawFPS()
        {
            var fps = GameSystem.Profiler.Fps.Average;

            var fpsMsg = string.Format("fps: {0,-6}  ({1,-6}-{2,6})",
                fps.ToString("F1"),
                GameSystem.Profiler.Fps.Minimum.ToString("F1"),
                GameSystem.Profiler.Fps.Maximum.ToString("F1"));
            fpsLabel.Text = fpsMsg;
        }

        void DrawFrames()
        {
            float frameInterval = 1000.0f / GameSystem.Profiler.Fps.Average;
            var frameMsg = "frame interval: " + frameInterval.ToString("F1") + " ms";
            frameLabel.Text = frameMsg;
        }

        void DebugDrawLogs( string log, LogDispatcher dispatcher )
        {
            if (logNum == maxLogs)
            {
                for (int i = 0; i < maxLogs - 1; i++)
                {
                    logLabelList[i].Text = logLabelList[i + 1].Text;
                }
                logLabelList[maxLogs - 1].Text = log;
            }
            else
            {
                logLabelList[logNum].Text = log;
                logNum++;
            }
        }

        void DrawWarnings()
        {
            // TODO : Somebody should implement this
            foreach (var w in warningLabelList.Values)
            {
                
            }
        }

        void InitDefault(LogDispatcher log)
        {
            thisLog = log;

            thisLog.OnWriteLog += DebugDrawLogs;

            layer = new Layer();

            fpsLabel = new UI.Component.Label("Default FPS", DOFontPath ,lineHeight );
            fpsLabel.X = left;
            fpsLabel.Y = leftTop;
            fpsLabel.Color = Color.Red;
            layer.AddChild(fpsLabel);
            
            leftTop -= lineHeight * 3;
            
            frameLabel = new UI.Component.Label("Default Frame", DOFontPath, lineHeight);
            frameLabel.X = left;
            frameLabel.Y = leftTop;
            frameLabel.Color = Color.DarkRed;
            layer.AddChild(frameLabel);

            leftTop -= lineHeight * 3;

            watchNode = new Node();
            layer.AddChild(watchNode);

            logNode = new Node();
            layer.AddChild(logNode);
            for (int i = 0; i < maxLogs; i++)
            {
                UI.Component.Label logLabel = new UI.Component.Label(" ", DOFontPath, lineHeight);
                logLabel.X = logPosX;
                logLabel.Y = rightTop;
                rightTop -= lineHeight * 2;

                logLabel.Color = Color.White;
                logLabelList.Add(i, logLabel);

                logNode.AddChild(logLabel);
            }

            warningLayer = new Layer();

            for (int i = 0; i < maxWarnings; i++)
            {
                UI.Component.Label warningLabel = new UI.Component.Label(" ", DOFontPath, lineHeight);
                warningLabel.X = warningPosX;
                warningLabel.Y = bottom + i*lineHeight*2;

                warningLabel.Color = Color.OrangeRed;
                var x = new LabelTimePair()
                {
                    label = warningLabel,
                    lifetime = 0.0f
                };

                warningLabelList.Add(i, x);
                warningLayer.AddChild(x.label);
            }
        }
        
        const int lineHeight = 10;
        const string DOFontPath = @"Resource/Font/NanumBarunGothic.ttf";

        Dictionary<Tuple<String, String>, UI.Component.Label> watchLabelList = new Dictionary<Tuple<string, string>, Label>();
        Dictionary<int, UI.Component.Label> logLabelList = new Dictionary<int, UI.Component.Label>();
        Dictionary<int, LabelTimePair> warningLabelList = new Dictionary<int, LabelTimePair>();

        struct LabelTimePair
        {
            public Label label;
            public float lifetime;
        }

        UI.Component.Layer layer;
        UI.Component.Layer warningLayer;

        UI.Component.Label fpsLabel;
        UI.Component.Label frameLabel;

        UI.Component.Node watchNode;

        UI.Component.Node logNode;

        LogDispatcher thisLog;

        private float leftTop = GameSystem.Viewport.Y - 20;
        private float rightTop = GameSystem.Viewport.Y - 20 ;
        private float bottom = 20;
        private float left = 5;
        private float logPosX = 410;
        private const int maxLogs = 20;
        private int logNum = 0;

        private float warningPosX = 200;
        private const int maxWarnings = 10;

    }
}
