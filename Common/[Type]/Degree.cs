﻿using OpenTK;
using System;

namespace HAJE.MMO
{
    public struct Degree
    {
        private float value;

        #region .ctor
        public Degree(float degree)
        {
            value = degree;
        }
        public Degree(Radian radian)
        {
            value = radian / Radian.Pi * 180.0f;
        }
        #endregion

        #region trigonometric
        public static float Cos(Degree r)
        {
            return Radian.Cos(r);
        }
        public static float Sin(Radian r)
        {
            return Radian.Sin(r);
        }
        public static float Tan(Radian r)
        {
            return Radian.Tan(r);
        }
        #endregion
        #region trigonometric advanced
        public Vector2 Rotate(Vector2 v)
        {
            return new Vector2(
                (float)(v.X * Degree.Cos(this) - v.Y * Degree.Sin(this)),
                (float)(v.X * Degree.Sin(this) + v.Y * Degree.Cos(this))
            );
        }
        public static Vector2 ToDirection(Degree r)
        {
            return new Vector2(Cos(r), Sin(r));
        }
        #endregion

        #region casting
        public static explicit operator Degree(float radian)
        {
            return new Degree(radian);
        }

        public static implicit operator Degree(Radian radian)
        {
            return new Degree(radian);
        }

        public static explicit operator float(Degree degree)
        {
            return degree.value;
        }
        #endregion
        #region operator
        public static Degree operator +(Degree a, Degree b)
        {
            return new Degree(a.value + b.value);
        }

        public static Degree operator +(Degree a, float b)
        {
            return new Degree(a.value + b);
        }

        public static Degree operator +(float a, Degree b)
        {
            return new Degree(a + b.value);
        }

        public static Degree operator -(Degree a, Degree b)
        {
            return new Degree(a.value - b.value);
        }

        public static Degree operator -(Degree a, float b)
        {
            return new Degree(a.value - b);
        }

        public static Degree operator -(float a, Degree b)
        {
            return new Degree(a - b.value);
        }

        public static Degree operator /(Degree a, Degree b)
        {
            return new Degree(a.value / b.value);
        }

        public static Degree operator /(Degree a, float b)
        {
            return new Degree(a.value / b);
        }

        public static Degree operator /(float a, Degree b)
        {
            return new Degree(a / b.value);
        }

        public static Degree operator *(Degree a, Degree b)
        {
            return new Degree(a.value * b.value);
        }

        public static Degree operator *(Degree a, float b)
        {
            return new Degree(a.value * b);
        }

        public static Degree operator *(float a, Degree b)
        {
            return new Degree(a * b.value);
        }
        #endregion
    }
}
