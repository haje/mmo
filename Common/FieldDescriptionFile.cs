﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace HAJE.MMO
{
    public class FieldDescriptionFile
    {
        static string Path(int index)
        {
            return String.Format("{0}/Terrain/{1}.xml", PathHelper.ResourcePath, index);
        }

        public static IReadOnlyList<FieldDoodad> ReadDoodadFromFile(int index)
        {
            List<FieldDoodad> doodads = new List<FieldDoodad>();

            XmlSerializer serializer = new XmlSerializer(typeof(FieldDescriptionXml.FieldDescription));
            int id = 0;
            using (FileStream fs = File.OpenRead(Path(index)))
            {
                var xml = (FieldDescriptionXml.FieldDescription)serializer.Deserialize(fs);
                foreach (var d in xml.DoodadStatusList)
                {
                    var info = new FieldDoodad(d, id++);
                    doodads.Add(info);
                }
            }

            return doodads.AsReadOnly();
        }

        public static IReadOnlyList<Crevasse> ReadCrevasseFromFile(int index)
        {
            List<Crevasse> crevasse = new List<Crevasse>();

            XmlSerializer serializer = new XmlSerializer(typeof(FieldDescriptionXml.FieldDescription));
            using (FileStream fs = File.OpenRead(Path(index)))
            {
                var xml = (FieldDescriptionXml.FieldDescription)serializer.Deserialize(fs);
                foreach (var c in xml.CrevasseList)
                {
                    var info = new Crevasse(c);
                    crevasse.Add(info);
                }
            }

            return crevasse.AsReadOnly();
        }
    }

    public class FieldDoodad
    {
        public FieldDoodad(FieldDescriptionXml.Doodad statusInfo, int id)
        {
            Type = statusInfo.type;
            Id = id;
            Radius = statusInfo.radius;
            Height = statusInfo.height;
            Rotation = (Degree)statusInfo.rotation;
            Position = new Vector2(statusInfo.Position.x, statusInfo.Position.y);
            IsTexture = statusInfo.isTexture;
        }

        public readonly string Type;
        public readonly int Id;
        public readonly float Radius;
        public readonly float Height;
        public readonly Radian Rotation;
        public readonly Vector2 Position;
        public readonly bool IsTexture;
    }

    public class Crevasse
    {
        public Crevasse(FieldDescriptionXml.Crevasse info)
        {
            Rotation = (Radian)((Degree)info.rotation);
            Axis = new Vector2(info.Axis.x, info.Axis.y);
            Position = new Vector2(info.Position.x, info.Position.y);
        }

        public bool Contains(Vector2 position)
        {
            Radian invRot = this.Rotation;
            var vecToPos = (position - this.Position);
            var axis = invRot.Rotate(vecToPos);
            axis.X = axis.X / this.Axis.X;
            axis.Y = axis.Y / this.Axis.Y;
            return (axis.X * axis.X) + (axis.Y * axis.Y) <= 1;
        }

        public readonly Radian Rotation;
        public readonly Vector2 Axis;
        public readonly Vector2 Position;
    }
}

namespace HAJE.MMO.FieldDescriptionXml
{
    public class V2
    {
        [XmlAttribute]
        public float x;

        [XmlAttribute]
        public float y;
    }
    public class Doodad
    {
        [XmlAttribute]
        public string type;

        [XmlAttribute]
        public float radius;

        [XmlAttribute]
        public float height;

        [XmlAttribute]
        public float rotation;

        [XmlAttribute]
        public bool isTexture;

        public V2 Position;
    }

    public class Crevasse
    {
        [XmlAttribute]
        public float rotation;
        public V2 Axis;
        public V2 Position;
    }

    public class FieldDescription
    {
        public List<Doodad> DoodadStatusList;
        public List<Crevasse> CrevasseList;
    }
}
