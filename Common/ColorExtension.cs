﻿using System.Drawing;

namespace HAJE.MMO.Rendering
{
    public static class ColorExtension
    {
        public static bool IsGray(this Color color)
        {
            return color.R == color.G && color.R == color.B;
        }

        public static int GrayIntensity(this Color color)
        {
            return color.R;
        }
    }
}
