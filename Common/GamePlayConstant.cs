﻿namespace HAJE.MMO
{
    public static class GamePlayConstant
    {
        public static readonly float RunSpeed = 6.0f;
        public static readonly float WalkSpeed = 2.0f;
        public static readonly float MaxChargeTime = 5.0f;

        public const float CharacterSyncRange = 150.0f;
        public static readonly float SightRange = 120.0f;

        public static readonly int MaxHitPoint = 1000;
        public static readonly int MaxManaPoint = 100;
        public static readonly int MaxComboPoint = 5;
        public static readonly int MaxElementPoint = 100;

        public static readonly int HitPointRegen = 30;
        public static readonly int ManaPointRegen = 10;
        public static readonly Second RegenInterval = (Second)5;
        public static readonly Second SyncInterval = (Second)2;

        public static readonly int MaxSpellEquip = 10;

        public static readonly int DefaultFieldSize = 500;
    }
}
