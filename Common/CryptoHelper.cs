﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Collections;
using System.Linq;
using System.Net;

namespace HAJE.MMO
{
    public static class CryptoHelper
    {
        public static IReadOnlyList<byte> HashToBytes(string plaintext)
        {
            using (SHA256 sha2 = SHA256Managed.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(plaintext);
                var hash = sha2.ComputeHash(bytes);
                return new List<byte>(hash).AsReadOnly();
            }
        }
        
        public static IReadOnlyList<byte> HashServerBinaries()
        {
            string[] files = {
                "Common.dll",
                "Server.dll",
            };

            List<byte[]> byteArrayList = new List<byte[]>();
            try
            {
                using (SHA256 sha2 = SHA256Managed.Create())
                {
                    foreach (string filename in files)
                    {
                        //각 파일의 해쉬값을 구하고 이들을 하나의 리스트에 넣는다.
                        var path = PathHelper.BinaryDirectory + filename;
                        using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                        {
                            var hashValue = sha2.ComputeHash(fs);
                            byteArrayList.Add(hashValue);
                        }
                    }
                    //해쉬값을 모아놓은 byteArray 의 해쉬를 구한다.
                    byte[] byteArray = byteArrayList.SelectMany(bytes => bytes).ToArray();
                    var hash = sha2.ComputeHash(byteArray);
                    return new List<byte>(hash).AsReadOnly();
                }
            }
            catch (IOException)
            {
                //파일을 못열었을때
                return null;
            }
        }

        public static string IListToString(IReadOnlyList<byte> list, Encoding e)
        {
            string retVal = null;
            if(e.IsSingleByte)
            {
                retVal = string.Join("", list.Select(b=>e.GetString(new byte[]{b})));
            }
            else
            {
                StringBuilder sb = new StringBuilder(list.Count() / 2);
                var enumerator = list.GetEnumerator();
                var buffer = new byte[2];
                while(enumerator.MoveNext())
                {
                    buffer[0] = enumerator.Current;
                    if (enumerator.MoveNext())
                        buffer[1] = enumerator.Current;
                    else
                        buffer[1] = 0;
                    sb.Append(e.GetString(buffer));
                }
                retVal = sb.ToString();
            }
            return retVal;
        }

        public static string GetUserHashedPassword(string email, string password)
        {
            try
            {
                HttpWebRequest passReq =(HttpWebRequest)WebRequest.Create("http://143.248.233.58:5000/passwd/" + email+"/"+password);
                passReq.Method = "GET";
          
                using (HttpWebResponse passRes = (HttpWebResponse)passReq.GetResponse())
                {
                    Stream passResStream = passRes.GetResponseStream();
                    StreamReader reader = new StreamReader(passResStream, Encoding.GetEncoding("EUC-KR"), true);

                    string hashedPassword = reader.ReadToEnd();
                    return hashedPassword;
                }
            }
            catch(WebException e)
            {
                System.Console.WriteLine("GetUserHashedPassword: {0}", e.Message);
                return null;
            }


            /* Web Server에서 Hashing된 비밀번호를 받아오도록 구현을 변경합니다. */
            /*
            using (SHA256 sha2 = SHA256Managed.Create())
            {
                var password_bytes = Encoding.UTF8.GetBytes(password);
                var password_hash = sha2.ComputeHash(password_bytes);
                var email_bytes = Encoding.UTF8.GetBytes(email);
                var bytes = email_bytes.Concat(password_hash).ToArray();
                var hash = sha2.ComputeHash(bytes);
                return Convert.ToBase64String(hash);
            }
            */
        }

        public static string GetFieldToken(string email)
        {
            using (SHA256 sha2 = SHA256Managed.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(email + DateTime.Now.ToLongTimeString());
                var hash = sha2.ComputeHash(bytes);
                return Convert.ToBase64String(hash);
            }
        }

        public static string GetAuthHashedUrl(string id, string name)
        {
            try
            {
                HttpWebRequest tokenReq = (HttpWebRequest)WebRequest.Create("http://143.248.233.58:5000/token/" + id + "/" + name);
                tokenReq.Method = "GET";

                using (HttpWebResponse tokenRes = (HttpWebResponse)tokenReq.GetResponse())
                {
                    Stream tokenResStream = tokenRes.GetResponseStream();
                    StreamReader reader = new StreamReader(tokenResStream, Encoding.GetEncoding("EUC-KR"), true);

                    string token = reader.ReadToEnd();
                    return token;
                }
            }
            catch (WebException e)
            {
                System.Console.WriteLine("GetAuthHashedUrl: {0}", e.Message);
                return null;
            }


            /* Web Server에서 token을 받아오도록 구현을 변경합니다. */
            /*
            using (SHA256 sha2 = SHA256Managed.Create())
            {
                var id_bytes = Encoding.UTF8.GetBytes(id);
                var id_hash = sha2.ComputeHash(id_bytes);
                var name_bytes = Encoding.UTF8.GetBytes(name);
                var time_stamp = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds.ToString();
                var time_bytes = Encoding.UTF8.GetBytes(time_stamp);
                var time_hash = sha2.ComputeHash(time_bytes);
                var bytes = name_bytes.Concat(id_hash).Concat(time_hash).ToArray();
                var hash = sha2.ComputeHash(bytes);
                return Convert.ToBase64String(hash);
            }
            */
        }
    }
}
