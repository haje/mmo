﻿using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace HAJE.MMO
{
    public class ConfigurationReader
    {
        public static T Read<T>(string fileName)
        {
            string[] pathCandidates = new string[2]
            {
                PathHelper.BinaryDirectory + fileName,
                PathHelper.ProjectDirectory + fileName
            };

            for (int i = 0; i < pathCandidates.Length; i++)
            {
                string path = pathCandidates[i];
                if (File.Exists(pathCandidates[i]))
                {
                    XmlSerializer s = new XmlSerializer(typeof(T));
                    using (FileStream fs = File.OpenRead(path))
                    {
                        return (T)s.Deserialize(fs);
                    }
                }
            }

            throw new FileNotFoundException("설정 파일을 찾을 수 없습니다.");
        }
    }
}
