﻿namespace HAJE.MMO
{
    public enum EffectType
    {
        None,
        DropElement,
        ArcaneMissile,
        ArcaneExplosion,
        ArcaneHit
    }
}
