﻿namespace HAJE.MMO
{
    public enum TargetType
    {
        None,
        Air,
        Ground,
        Doodad,
        Player,
    }
}
