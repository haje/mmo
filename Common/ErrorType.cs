﻿namespace HAJE.MMO
{
    public enum ErrorType
    {
        Ignore,
        ReturnToLogin,
        ExitGame
    }
}
