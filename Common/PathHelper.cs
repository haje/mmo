﻿using System.IO;
using System.Reflection;

namespace HAJE.MMO
{
    public static class PathHelper
    {
        public static readonly string BinaryPath;
        public static readonly string BinaryDirectory;
        public static readonly string ProjectDirectory;
        public static readonly string ResourcePath;

        static PathHelper()
        {
            BinaryPath = Assembly.GetEntryAssembly().Location;

            if (BinaryPath.EndsWith("UnitTest.exe"))
            {
                BinaryDirectory = Path.Combine(Path.GetDirectoryName(BinaryPath), "..\\..\\..\\Client\\bin\\Debug");
            }
            else
            {
                BinaryDirectory = Path.GetDirectoryName(BinaryPath) + "\\";
            }
            ProjectDirectory = Path.GetFullPath(BinaryDirectory + "/../../");

            if (BuildInfo.DeployBuild)
                ResourcePath = Path.GetFullPath(BinaryDirectory + "/Resource/");
            else
                ResourcePath = Path.GetFullPath(ProjectDirectory + "/../Client/Resource");
        }

        public static void EnsureDirectory(string dirPath)
        {
            var dir = Path.GetDirectoryName(dirPath);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
    }
}
