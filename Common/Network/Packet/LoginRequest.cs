﻿
namespace HAJE.MMO.Network.Packet
{
    public class LoginRequest : PacketBase
    {
        public LoginRequest() : base(PacketType.LoginRequest) { }

        public LoginRequest(string email, string password) 
            : base( PacketType.LoginRequest)
        {
            this.Email = email;
            this.Password = password;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Email);
            writer.Write(this.Password);
        }

        public override void Deserialize(PacketReader reader)
        {
            Email = reader.ReadString();
            Password = reader.ReadString();
        }

        public string Email;
        public string Password;
    }
}
