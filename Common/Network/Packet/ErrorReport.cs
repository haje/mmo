﻿namespace HAJE.MMO.Network.Packet
{
    public class ErrorReport : PacketBase
    {
        public ErrorReport() : base(PacketType.ErrorReport) { }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(Content);
            writer.Write((short)ErrorType);
        }

        public override void Deserialize(PacketReader reader)
        {
            Content = reader.ReadString();
            ErrorType = (ErrorType)reader.ReadInt16();
        }

        public string Content;
        public ErrorType ErrorType;
    }
}
