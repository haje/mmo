﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Network.Packet
{
    public class NewAccountResponse : PacketBase
    {
        public NewAccountResponse() : base(PacketType.NewAccountResponse) { }

        public NewAccountResponse(NewAccountResponseCode responseCode)
            : base(PacketType.NewAccountResponse)
        {
            this.ResponseCode = responseCode;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write((Int16)ResponseCode);
        }

        public override void Deserialize(PacketReader reader)
        {
            ResponseCode = reader.ReadEnum(NewAccountResponseCode.Unknown);
        }

        public NewAccountResponseCode ResponseCode;
    }

    public enum NewAccountResponseCode
    {
        Unknown = 0,
        Success,
        UserNameAlreadyExists,
        EmailAlreadyExists,
        AddressNotExists,
        InvalidUserId,
        WebServerNotFound,
    };
}
