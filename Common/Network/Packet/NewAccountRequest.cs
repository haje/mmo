﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Network.Packet
{
    public class NewAccountRequest : PacketBase
    {
        public NewAccountRequest() : base(PacketType.NewAccountRequest) { }

        public NewAccountRequest(string email, string userName, string password)
            : base(PacketType.NewAccountRequest)
        {
            this.Email = email;
            this.UserName = userName;
            this.Password = password;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Email);
            writer.Write(this.UserName);
            writer.Write(this.Password);
        }

        public override void Deserialize(PacketReader reader)
        {
            Email = reader.ReadString();
            UserName = reader.ReadString();
            Password = reader.ReadString();
        }

        public string Email;
        public string UserName;
        public string Password;
    }
}
