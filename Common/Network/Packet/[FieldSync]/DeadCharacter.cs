﻿
namespace HAJE.MMO.Network.Packet
{
    public class DeadCharacter : PacketBase
    {
        public DeadCharacter()
            : base(PacketType.DeadCharacter)
        {
        }

        public DeadCharacter(int characterId)
            : base(PacketType.DeadCharacter)
        {
            this.CharacterId = characterId;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.CharacterId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
        }

        public int CharacterId;
    }
}
