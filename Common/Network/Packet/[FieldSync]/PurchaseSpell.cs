﻿using System;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Network.Packet
{
    public class PurchaseSpell : PacketBase
    {
        public PurchaseSpell()
            : base(PacketType.PurchaseSpell)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterId);
            writer.Write((Int16)SpellId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            SpellId = reader.ReadEnum(SpellId.None);
        }

        public int CharacterId;
        public SpellId SpellId;
    }
}
