﻿using System;

namespace HAJE.MMO.Network.Packet
{
    public class MoveEffect : PacketBase
    {
        public MoveEffect()
            : base(PacketType.MoveEffect)
        {
        }

        public MoveEffect(int id)
            : base(PacketType.MoveEffect)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Id);
            writer.Write((Int16)TargetType);
            switch (TargetType)
            {
                case TargetType.Air:
                case TargetType.Ground:
                case TargetType.Doodad:
                    writer.Write(TargetPosition);
                    break;
                case TargetType.Player:
                    writer.Write(TargetId);
                    writer.Write((Int16)AttachmentPoint);
                    break;
                default:
                    break;
            }
            writer.Write(Duration);
        }

        public override void Deserialize(PacketReader reader)
        {
            Id = reader.ReadInt32();
            TargetType = reader.ReadEnum(TargetType.None);
            switch (TargetType)
            {
                case TargetType.Air:
                case TargetType.Ground:
                case TargetType.Doodad:
                    reader.ReadPositionInField(out TargetPosition);
                    break;
                case TargetType.Player:
                    TargetId = reader.ReadInt32();
                    AttachmentPoint = reader.ReadEnum(AttachmentPoint.Origin);
                    break;
                default:
                    break;
            }
            Duration = (Second)reader.ReadFloat();
        }

        public int Id;
        public TargetType TargetType;
        public PositionInField TargetPosition;
        public int TargetId;
        public AttachmentPoint AttachmentPoint;
        public Second Duration;
    }
}
