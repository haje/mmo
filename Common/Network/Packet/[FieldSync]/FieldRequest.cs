﻿
namespace HAJE.MMO.Network.Packet
{
    public class FieldRequest : PacketBase
    {
        public FieldRequest()
            : base(PacketType.FieldRequest)
        {
        }

        public FieldRequest(int fieldId)
            : base(PacketType.FieldRequest)
        {
            FieldId = fieldId;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.FieldId);
        }

        public override void Deserialize(PacketReader reader)
        {
            FieldId = reader.ReadInt32();
        }

        public int FieldId;
    }
}
