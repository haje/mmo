﻿using OpenTK;

namespace HAJE.MMO.Network.Packet
{
    public class RequestCharacter : PacketBase
    {
        public RequestCharacter()
            : base(PacketType.RequestCharacter)
        {
        }

        public RequestCharacter(int characterId, int fieldId)
            : base(PacketType.RequestCharacter)
        {
            this.CharacterId = characterId;
            this.FieldId = fieldId;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.CharacterId);
            writer.Write(this.FieldId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            FieldId = reader.ReadInt32();
        }

        public int CharacterId;
        public int FieldId;
    }
}
