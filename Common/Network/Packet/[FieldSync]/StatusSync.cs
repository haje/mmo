﻿using System;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Network.Packet
{
    public class StatusSync : PacketBase
    {
        public StatusSync()
            : base(PacketType.StatusSync)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.CharacterId);
            writer.Write((Int16)StatusType);
            if (StatusType == StatusType.ComboPoint || StatusType == StatusType.ElementPoint)
                writer.Write((Int16)ElementType);
            else if (StatusType == StatusType.SpellCount)
                writer.Write((Int16)SpellId);
            writer.Write(this.Value);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            StatusType = reader.ReadEnum(StatusType.None);
            if (StatusType == StatusType.ComboPoint || StatusType == StatusType.ElementPoint)
                ElementType = reader.ReadEnum(ElementType.None);
            else if (StatusType == StatusType.SpellCount)
                SpellId = reader.ReadEnum(SpellId.None);
            Value = reader.ReadInt32();
        }

        public int CharacterId;
        public StatusType StatusType;
        public ElementType ElementType;
        public SpellId SpellId;
        public int Value;
    }

    public enum StatusType
    {
        None,
        HitPoint,
        ManaPoint,
        ComboPoint,
        ElementPoint,
        SpellCount
        //and so on
    }
}
