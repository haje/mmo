﻿using System;
using OpenTK;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Network.Packet
{
    public class DropItem : PacketBase
    {
        public DropItem()
            : base(PacketType.DropItem)
        {
        }

        public DropItem(int fieldId, Vector2 position, ElementType elementType, int point)
            : base(PacketType.DropItem)
        {
            FieldId = fieldId;
            Position = position;
            ElementType = elementType;
            Point = point;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(FieldId);
            writer.Write(Position);
            writer.Write((Int16)ElementType);
            writer.Write(Point);
        }

        public override void Deserialize(PacketReader reader)
        {
            FieldId = reader.ReadInt32();
            Position = reader.ReadVector2();
            ElementType = reader.ReadEnum(ElementType.None);
            Point = reader.ReadInt32();
        }

        public int FieldId;
        public Vector2 Position;
        public ElementType ElementType;
        public int Point;
    }
}
