﻿using System;
using OpenTK;

namespace HAJE.MMO.Network.Packet
{
    public class CreateEffect : PacketBase
    {
        public CreateEffect()
            : base(PacketType.CreateEffect)
        {
        }

        public CreateEffect(int id, EffectType effectType, TargetType targetType)
            : base(PacketType.CreateEffect)
        {
            this.Id = id;
            this.EffectType = effectType;
            this.TargetType = targetType;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Id);
            writer.Write((Int16)EffectType);
            writer.Write((Int16)TargetType);
            switch (TargetType)
            {
                case TargetType.Air:
                case TargetType.Ground:
                case TargetType.Doodad:
                    writer.Write(TargetPosition);
                    break;
                case TargetType.Player:
                    writer.Write(TargetId);
                    writer.Write((Int16)AttachmentPoint);
                    break;
                default:
                    break;
            }
        }

        public override void Deserialize(PacketReader reader)
        {
            Id = reader.ReadInt32();
            EffectType = reader.ReadEnum(EffectType.None);
            TargetType = reader.ReadEnum(TargetType.None);
            switch (TargetType)
            {
                case TargetType.Air:
                case TargetType.Ground:
                case TargetType.Doodad:
                    reader.ReadPositionInField(out TargetPosition);
                    break;
                case TargetType.Player:
                    TargetId = reader.ReadInt32();
                    AttachmentPoint = reader.ReadEnum(AttachmentPoint.Origin);
                    break;
                default:
                    break;
            }
        }

        public int Id;
        public EffectType EffectType;
        public TargetType TargetType;
        public PositionInField TargetPosition;
        public int TargetId;
        public AttachmentPoint AttachmentPoint;
    }
}
