﻿
namespace HAJE.MMO.Network.Packet 
{
    public class NewCharacterRequest : PacketBase
    {
        public NewCharacterRequest() 
            : base(PacketType.NewCharacterRequest)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterName);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterName = reader.ReadString();
        }

        public string CharacterName = "Unknown";
    }

}
