﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Network.Packet
{
    public class CancelCastSync : PacketBase
    {
        public CancelCastSync()
            : base(PacketType.CancelCastSync)
        { }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
        }

        public int CharacterId;
    }
}
