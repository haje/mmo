﻿using HAJE.MMO.Spell;
using OpenTK;
using System;

namespace HAJE.MMO.Network.Packet
{
    public class EndCastSync : PacketBase
    {
        public EndCastSync()
            : base(PacketType.EndCastSync)
        {
        }

        public EndCastSync(int characterId, SpellId spellId, TargetType targetType)
            : base(PacketType.EndCastSync)
        {
            this.CharacterId = characterId;
            this.SpellId = spellId;
            this.TargetType = targetType;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterId);
            writer.Write((Int16)SpellId);
            writer.Write((Int16)TargetType);
            writer.Write(TargetId);
            writer.Write(TargetFieldId);
            writer.Write(TargetPosition);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            SpellId = reader.ReadEnum(SpellId.None);
            TargetType = reader.ReadEnum(TargetType.None);
            TargetId = reader.ReadInt32();
            TargetFieldId = reader.ReadInt32();
            TargetPosition = reader.ReadVector3();
        }

        public int CharacterId;
        public SpellId SpellId;
        public TargetType TargetType;
        public int TargetId;
        public int TargetFieldId;
        public Vector3 TargetPosition;
    }
}
