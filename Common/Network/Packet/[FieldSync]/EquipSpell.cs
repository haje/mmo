﻿using System;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Network.Packet
{
    public class EquipSpell : PacketBase
    {
        public EquipSpell()
            : base(PacketType.EquipSpell)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterId);
            writer.Write(Index);
            writer.Write((Int16)SpellId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            Index = reader.ReadInt32();
            SpellId = reader.ReadEnum(SpellId.None);
        }

        public int CharacterId;
        public int Index;
        public SpellId SpellId;
    }
}
