﻿using System;

namespace HAJE.MMO.Network.Packet
{
    public class MoveSync : PacketBase
    {
        public MoveSync()
            : base(PacketType.MoveSync)
        {
            this.positionInfo = new PositionInfo();
        }

        public MoveSync(PositionInfo positionInfo)
            : base(PacketType.MoveSync)
        {
            this.positionInfo = positionInfo;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterId);
            writer.Write(positionInfo);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            reader.ReadPositionInfo(positionInfo);
        }

        public void CopyFrom(PositionInfo positionInfo)
        {
            this.positionInfo.CopyFrom(positionInfo);
        }

        public int CharacterId;
        private PositionInfo positionInfo; // 외부에서 할당된 객체의 참조를 이용해서 계속 재활용
    }
}
