﻿
namespace HAJE.MMO.Network.Packet
{
    public class RemoveEffect : PacketBase
    {
        public RemoveEffect()
            : base(PacketType.RemoveEffect)
        {
        }

        public RemoveEffect(int id)
            : base(PacketType.RemoveEffect)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Id);
        }

        public override void Deserialize(PacketReader reader)
        {
            Id = reader.ReadInt32();
        }

        public int Id;
    }
}
