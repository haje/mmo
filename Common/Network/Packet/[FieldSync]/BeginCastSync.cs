﻿using HAJE.MMO.Spell;
using System;

namespace HAJE.MMO.Network.Packet
{
    public class BeginCastSync : PacketBase
    {
        public BeginCastSync()
            : base(PacketType.BeginCastSync)
        {
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(CharacterId);
            writer.Write((Int16)SpellId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
            SpellId = reader.ReadEnum(SpellId.None);
        }

        public int CharacterId;
        public SpellId SpellId;
    }
}
