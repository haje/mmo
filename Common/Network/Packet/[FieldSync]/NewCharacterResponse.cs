﻿using OpenTK;
using System.Collections.Generic;

namespace HAJE.MMO.Network.Packet
{
    public class NewCharacterRepsonse : PacketBase
    {
        public NewCharacterRepsonse() : base(PacketType.NewCharacterResponse) { }
        public NewCharacterRepsonse(int characterId)
            : base(PacketType.NewCharacterResponse)
        {
            this.CharacterId = characterId;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.CharacterId);
        }

        public override void Deserialize(PacketReader reader)
        {
            CharacterId = reader.ReadInt32();
        }

        public int CharacterId;
        public int FieldId;
    }


    //TODO : Move These to Other File and CHANGE NAME!
    public class PositionInfo
    {
        public PositionInfo()
        {
            MotionState = MotionState.Stop;
            RelativeOfs = Vector2.Zero;
            Direction = new Vector3(1, 0, 0);
        }

        public void CopyFrom(PositionInfo sourceInfo)
        {
            this.MotionState = sourceInfo.MotionState;
            this.FieldId = sourceInfo.FieldId;
            this.Position = sourceInfo.Position;
            this.RelativeOfs = sourceInfo.RelativeOfs;
            this.Velocity = sourceInfo.Velocity;
            this.Direction = sourceInfo.Direction;
        }

        public PositionInField GetPositionInField()
        {
            return new PositionInField(FieldId, Position);
        }

        public void AdjustInFieldPosition(Dictionary<FieldConnectDirection, int> connection)
        {
            var FieldSize = GamePlayConstant.DefaultFieldSize;
            var relativePos = Position + RelativeOfs;
            if (relativePos.X < 0 || relativePos.Y < 0 ||
                relativePos.X >= FieldSize || relativePos.Y >= FieldSize)
            {
                FieldConnectDirection offDir = GetAreaOffDirection();
                if(offDir != FieldConnectDirection.MAX)
                {
                    int newId = -1; 
                    connection.TryGetValue(offDir, out newId);
                    if(newId > 0)
                    {
                        FieldId = connection[offDir];
                        RelativeOfs += GetRelativeOfs(offDir);
                        //Position = GetRelativePosition(offDir);
                    }
                }
            }

        }

        public Vector2 GetRelativeOfs(FieldConnectDirection direction)
        {
            var FieldSize = GamePlayConstant.DefaultFieldSize;
            switch (direction)
            {
                case FieldConnectDirection.UpperLeft:
                    return new Vector2(FieldSize / 2, -FieldSize);
                case FieldConnectDirection.UpperRight:
                    return new Vector2(-FieldSize / 2, -FieldSize);
                case FieldConnectDirection.Right:
                    return new Vector2(-FieldSize, 0);
                case FieldConnectDirection.LowerRight:
                    return new Vector2(-FieldSize / 2, FieldSize);
                case FieldConnectDirection.LowerLeft:
                    return new Vector2(FieldSize / 2, FieldSize);
                case FieldConnectDirection.Left:
                    return new Vector2(FieldSize, 0);
            }
            return Vector2.Zero;
        }

        public FieldConnectDirection GetAreaOffDirection()
        {
            FieldConnectDirection OffDirection = FieldConnectDirection.MAX;
            var size = GamePlayConstant.DefaultFieldSize;
            var relativePos = Position + RelativeOfs;
            if (relativePos.X < size / 2)
            {
                if (relativePos.Y >= size)
                    OffDirection = FieldConnectDirection.UpperLeft;
                else if (relativePos.Y < 0)
                    OffDirection = FieldConnectDirection.LowerLeft;
                else
                    OffDirection = FieldConnectDirection.Left;
            }
            else
            {
                if (relativePos.Y >= size)
                    OffDirection = FieldConnectDirection.UpperRight;
                else if (relativePos.Y < 0)
                    OffDirection = FieldConnectDirection.LowerRight;
                else
                    OffDirection = FieldConnectDirection.Right;
            }

            return OffDirection;
        }

        public MotionState MotionState;
        public int FieldId;
        public Vector2 Position;
        public Vector2 RelativeOfs;
        public Vector2 Velocity;
        public Vector3 Direction;

    }

    public enum MotionState
    {
        Stop,
        Walk,
        Run,
        Jump
    }
}
