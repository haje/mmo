﻿using System;

namespace HAJE.MMO.Network.Packet
{
    public class EncountCharacter : PacketBase
    {
        public EncountCharacter()
            : base(PacketType.EncountCharacter)
        {
        }


        public EncountCharacter(TargetType targetType, int characterId, string characterName, PositionInfo positionInfo)
            : base(PacketType.EncountCharacter)
        {
            this.TargetType = targetType;
            this.CharacterId = characterId;
            this.CharacterName = characterName;
            this.PositionInfo = positionInfo;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write((Int16)TargetType);
            writer.Write(CharacterId);
            writer.Write(CharacterName);
            writer.Write(PositionInfo);
        }

        public override void Deserialize(PacketReader reader)
        {
            TargetType = reader.ReadEnum(TargetType.None);
            CharacterId = reader.ReadInt32();
            CharacterName = reader.ReadString();
            reader.ReadPositionInfo(PositionInfo);
        }

        public TargetType TargetType;
        public int CharacterId;
        public string CharacterName;
        private PositionInfo PositionInfo;
    }
}
