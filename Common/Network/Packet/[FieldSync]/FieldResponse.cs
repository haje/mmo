﻿using System.Diagnostics;

namespace HAJE.MMO.Network.Packet
{
    public class FieldResponse : PacketBase
    {
        public FieldResponse()
            : base(PacketType.FieldResponse)
        {
            NeighborFieldId = new int[(int)FieldConnectDirection.MAX];
        }

        public FieldResponse(int fieldId, int fieldType, int[] neighborFieldId)
            : base(PacketType.FieldResponse)
        {
            FieldId = fieldId;
            FieldType = fieldType;

            Debug.Assert(neighborFieldId.Length == (int)FieldConnectDirection.MAX);
            NeighborFieldId = neighborFieldId;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.FieldId);
            writer.Write(this.FieldType);
            foreach (int fieldId in NeighborFieldId)
            {
                writer.Write(fieldId);
            }
        }

        public override void Deserialize(PacketReader reader)
        {
            FieldId = reader.ReadInt32();
            FieldType = reader.ReadInt32();
            for (int direction = 0; direction < NeighborFieldId.Length; ++direction)
                NeighborFieldId[direction] = reader.ReadInt32();
        }

        public int FieldId;
        public int FieldType;
        public int[] NeighborFieldId;
    }

    public enum FieldConnectDirection
    {
        UpperLeft,
        UpperRight,
        Right,
        LowerRight,
        LowerLeft,
        Left,

        MAX
    }
}
