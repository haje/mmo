﻿using System;
using System.Collections.Generic;

namespace HAJE.MMO.Network.Packet
{
    public abstract class PacketBase : IDisposable
    {
        public static PacketType ReadPacketType(PacketReader reader)
        {
            return reader.ReadEnum(PacketType.Unknown);
        }

        protected PacketBase(PacketType packetType)
        {
            this.PacketType = packetType;
        }

        public void Serialize(PacketWriter writer)
        { 
            writer.Clear();
            writer.Write((Int16)this.PacketType);
            DoSerialize(writer);
        }

        protected abstract void DoSerialize(PacketWriter writer);
        public abstract void Deserialize(PacketReader reader);

        public void Release()
        {
            if (PoolQueue != null && RefCount > 0)
            {
                if (--RefCount <= 0)
                    PoolQueue.Enqueue(this);
            }
        }

        public int RefCount { get; set; }
        public Queue<PacketBase> PoolQueue { get; set; }
        public PacketType PacketType { get; private set; }
        
        public void Dispose() {  }
    }   
}
