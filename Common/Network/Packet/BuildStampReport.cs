﻿namespace HAJE.MMO.Network.Packet
{
    public class BuildStampReport : PacketBase
    {
        public BuildStampReport() : base(PacketType.BuildStampReport) { }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(buildStamp);
        }

        public override void Deserialize(PacketReader reader)
        {
            buildStamp = reader.ReadString();
        }

        public string buildStamp = BuildInfo.BuildStamp;
    }
}
