﻿
namespace HAJE.MMO.Network.Packet
{
    public class SuperLoginRequest : PacketBase
    {
        public SuperLoginRequest() : base(PacketType.SuperLoginRequest) { }

        public SuperLoginRequest(string name)
            : base(PacketType.SuperLoginRequest)
        {
            this.Name = name;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Name);
        }

        public override void Deserialize(PacketReader reader)
        {
            Name = reader.ReadString();
        }

        public string Name;
    }
}
