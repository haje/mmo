﻿
namespace HAJE.MMO.Network.Packet
{
    public class SuperCommandResponse : PacketBase
    {
        public SuperCommandResponse() : base(PacketType.SuperCommandResponse) { }

        public SuperCommandResponse(string result)
            : base(PacketType.SuperCommandResponse)
        {
            this.Result = result;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Result);
        }

        public override void Deserialize(PacketReader reader)
        {
            Result = reader.ReadString();
        }

        public string Result;
    }
}
