﻿
namespace HAJE.MMO.Network.Packet
{
    public class SuperCommandRequest : PacketBase
    {
        public SuperCommandRequest() : base(PacketType.SuperCommandRequest) { }

        public SuperCommandRequest(string cmd)
            : base(PacketType.SuperCommandRequest)
        {
            this.Cmd = cmd;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Cmd);
        }

        public override void Deserialize(PacketReader reader)
        {
            Cmd = reader.ReadString();
        }

        public string Cmd;
    }
}
