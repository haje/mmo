﻿namespace HAJE.MMO.Network.Packet
{
    public enum PacketType
    {
        Unknown = -1,
        ServerRegisterRequest = 101,
        ServerRegisterResponse,
        SuperCommandRequest,
        SuperCommandResponse,
        SuperLoginRequest,
        LoginRequest,
        LoginResponse,
        AchieveRequest,
        AchieveResponse,
        NewAccountRequest,
        NewAccountResponse,
        AuthWithToken,
        BuildStampReport,
        ErrorReport,

        // Field
        NewCharacterRequest,
        NewCharacterResponse,
        FieldRequest,
        FieldResponse,
        RequestCharacter,
        RespondCharacter,
        DeadCharacter,
        StatusSync,
        MoveSync,
        BeginCastSync,
        CancelCastSync,
        EndCastSync,
        CreateEffect,
        MoveEffect,
        RemoveEffect,
        PurchaseSpell,
        EquipSpell,
        DropItem,

        EncountCharacter,//legacy
    }
}
