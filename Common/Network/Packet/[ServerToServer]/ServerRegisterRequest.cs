﻿using System;

namespace HAJE.MMO.Network.Packet
{
    public class ServerRegisterRequest : PacketBase
    {
        public ServerRegisterRequest() : base(PacketType.ServerRegisterRequest) { }
        public ServerRegisterRequest(string serverKey, string binaryKey, ServerType serverType, int serverPort) 
            : base(PacketType.ServerRegisterRequest)
        {
            this.ServerKey = serverKey;
            this.BinaryKey = binaryKey;
            this.ServerType = serverType;
            this.ServerPort = serverPort;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(ServerKey);
            writer.Write(BinaryKey);
            writer.Write((Int16)ServerType);
            writer.Write((Int16)ServerPort);
        }

        public override void Deserialize(PacketReader reader)
        {
            ServerKey = reader.ReadString();
            BinaryKey = reader.ReadString();
            ServerType = reader.ReadEnum(ServerType.Unknown);
            ServerPort = reader.ReadInt16();
        }
        
        public string ServerKey;
        public string BinaryKey;
        public ServerType ServerType;
        public int ServerPort;
    }

    public enum ServerType
    {
        Unknown = 0,
        Master,
        Login,
        Allocator,
        Field,
        Achieve,
    }
}
