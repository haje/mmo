﻿using System;

namespace HAJE.MMO.Network.Packet
{
    public class ServerRegisterResponse : PacketBase
    {
        public ServerRegisterResponse() : base(PacketType.ServerRegisterResponse) { }

        public ServerRegisterResponse(ResponseCode responseCode)
            : base(PacketType.ServerRegisterResponse)
        {
            this.ResponseCode = responseCode;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write((Int16)ResponseCode);
            writer.Write((byte)(isServerInfoExist ? 1 : 0));
            if (isServerInfoExist)
            {
                writer.Write((Int16)ServerType);
                writer.Write(ServerIp);
                writer.Write(ServerPort);
            }
        }

        public override void Deserialize(PacketReader reader)
        {
            ResponseCode = reader.ReadEnum(ResponseCode.Error);
            isServerInfoExist = reader.ReadByte() == 1;
            if (isServerInfoExist)
            {
                ServerType = reader.ReadEnum(ServerType.Unknown);
                ServerIp = reader.ReadString();
                ServerPort = reader.ReadInt32();
            }
        }

        public ResponseCode ResponseCode;
        public bool isServerInfoExist = false;
        public ServerType ServerType;
        public string ServerIp;
        public int ServerPort;
    }

    public enum ResponseCode
    {
        Success = 1,
        Failed,
        Error
    }
}
