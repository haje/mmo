﻿
namespace HAJE.MMO.Network.Packet
{
    public class AuthWithToken : PacketBase
    {
        public AuthWithToken() : base(PacketType.AuthWithToken) { }

        public AuthWithToken(int id, string token)
            : base(PacketType.AuthWithToken)
        {
            this.Id = id;
            this.Token = token;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write(this.Id);
            writer.Write(this.Token);
        }

        public override void Deserialize(PacketReader reader)
        {
            Id = reader.ReadInt32();
            Token = reader.ReadString();
        }

        public int Id;
        public string Token;
    }
}
