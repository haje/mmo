﻿using System;

namespace HAJE.MMO.Network.Packet
{
    public class LoginResponse : PacketBase
    {
        public LoginResponse() : base(PacketType.LoginResponse) { }

        public LoginResponse(LoginResponseCode responseCode, int playerId, string fieldToken, string fieldIp, int fieldPort) 
            : base(PacketType.LoginResponse)
        {
            this.ResponseCode = responseCode;
            this.PlayerId = playerId;
            this.FieldToken = fieldToken;
            this.FieldIp = fieldIp;
            this.FieldPort = fieldPort;
        }

        protected override void DoSerialize(PacketWriter writer)
        {
            writer.Write((Int16)ResponseCode);
            writer.Write(PlayerId);
            writer.Write(FieldToken);
            writer.Write(FieldIp);
            writer.Write((Int16)FieldPort);
        }

        public override void Deserialize(PacketReader reader)
        {
            ResponseCode = reader.ReadEnum(LoginResponseCode.Unknown);
            PlayerId = reader.ReadInt32();
            FieldToken = reader.ReadString();
            FieldIp = reader.ReadString();
            FieldPort = reader.ReadInt16();
        }

        public LoginResponseCode ResponseCode;
        public int PlayerId;
        public string FieldToken;
        public string FieldIp;
        public int FieldPort;

    }

    public enum LoginResponseCode
    {
        Unknown = 0,
        Success,
        UserNotFound,
        PasswordNotMatch,
        WebServerNotFound,
    };
}
