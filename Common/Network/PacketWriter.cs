﻿using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;
using System.Text;

using OpenTK;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network
{
    public class PacketWriter
    {
        public PacketWriter()
        {
            buffer = new byte[PacketLayout.BufferSize];
            Clear();
        }

        public void Clear()
        {
            offset = PacketLayout.PacketSizeOffsetInBytes;
        }

        public void Write(IList<byte> byteArray)
        {
            for (int i = 0; i < byteArray.Count; i++)
            {
                buffer[offset++] = byteArray[i];
            }
        }

        public void Write(byte source)
        {
            buffer[offset++] = (byte)(source & 0xFF);
        }

        public void Write(bool source)
        {
            Write(source ? (byte)1 : (byte)0);
        }

        public void Write(Int16 source)
        {
            buffer[offset++] = (byte)(source & 0xFF);
            buffer[offset++] = (byte)((source >> 8) & 0xFF);
        }

        public void Write(Int32 i)
        {
            buffer[offset++] = (byte)(i & 0xFF);
            buffer[offset++] = (byte)((i >> 8) & 0xFF);
            buffer[offset++] = (byte)((i >> 16) & 0xFF);
            buffer[offset++] = (byte)((i >> 24) & 0xFF);
        }

        public void Write(uint u)
        {
            buffer[offset++] = (byte)(u & 0xFF);
            buffer[offset++] = (byte)((u >> 8) & 0xFF);
            buffer[offset++] = (byte)((u >> 16) & 0xFF);
            buffer[offset++] = (byte)((u >> 24) & 0xFF);
        }

        public void Write(float source)
        {
            // Use union to avoid BitConverter.GetBytes() which allocates memory on the heap
            SingleUIntUnion su;
            su.UIntValue = 0; // must initialize every member of the union to avoid warning
            su.SingleValue = source;
            Write(su.UIntValue);
        }

        public void Write(double source)
        {
            DoubleUIntUnion du;
            du.UIntValue1 = 0; // must initialize every member of the union to avoid warning
            du.UIntValue2 = 0;
            du.DoubleValue = source;
            Write(du.UIntValue1);
            Write(du.UIntValue2);
        }

        public void Write(string source)
        {
            if(source == null)
            {
                Write(0);
                return;
            }

            byte[] bytes = Encoding.UTF8.GetBytes(source);
            if (bytes.Length > buffer.Length - 1)
            {
                //should not exceed buffer size
                return;
            }
            Write(bytes.Length);
            Write(bytes);
        }

        public void Write(Vector2 vector)
        {
            Write(vector.X);
            Write(vector.Y);
        }

        public void Write(Vector3 vector)
        {
            Write(vector.X);
            Write(vector.Y);
            Write(vector.Z);
        }

        public void Write(PositionInfo positionInfo)
        {
            Write((Int16)positionInfo.MotionState);
            Write(positionInfo.FieldId);
            Write(positionInfo.Position + positionInfo.RelativeOfs);
            Write(positionInfo.Velocity);
            Write(positionInfo.Direction);
        }

        public void Write(PositionInField positionInField)
        {
            Write(positionInField.FieldId);
            Write(positionInField.Position);
            Write(positionInField.IsGroundRelative);
        }

        /// <summary>
        /// 버퍼를 반환.
        /// SocketSender외에는 쓰지 마세요
        /// </summary>
        public byte[] Buffer
        {
            get
            {
                return buffer;
            }
        }

        public int Length
        {
            get
            {
                return offset;
            }
        }

        byte[] buffer;
        int offset;

        [StructLayout(LayoutKind.Explicit)]
        public struct SingleUIntUnion
        {
            [FieldOffset(0)]
            public float SingleValue;
            [FieldOffset(0)]
            public uint UIntValue;
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct DoubleUIntUnion
        {
            [FieldOffset(0)]
            public double DoubleValue;
            [FieldOffset(0)]
            public uint UIntValue1;
            [FieldOffset(4)]
            public uint UIntValue2;
        }

    }
}
