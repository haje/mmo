﻿using HAJE.MMO.Network.Packet;
using System.Diagnostics;

namespace HAJE.MMO.Network
{
    public abstract class Sender
    {
        public Sender(LogDispatcher logDispatcher)
        {
            this.logDispatcher = logDispatcher;
        }
        public void Log(string format, params object[] args)
        {
            if (logDispatcher != null)
                logDispatcher.Write(format, args);
        }
        public int WriteTotalSize(PacketBase packet)
        {
            Debug.Assert(Writer != null);

            int totalSize = Writer.Length;

            Debug.Assert(totalSize < byte.MaxValue);
            Debug.Assert(totalSize + PacketLayout.PaddingLengthInBytes
                < Writer.Buffer.Length);

            for (int i = 0; i < PacketLayout.PaddingLengthInBytes; i++)
                Writer.Buffer[totalSize++] = PacketLayout.PaddingByte;
            Writer.Buffer[0] = (byte)totalSize;

            return totalSize;
        }

        public abstract void Send(PacketBase packet);
        public abstract void SendAsync(PacketBase packet);

        public PacketWriter Writer { get; protected set; }
        protected LogDispatcher logDispatcher;
    }
}
