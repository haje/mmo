﻿using System.Net.Security;

namespace HAJE.MMO.Network
{
    public delegate void DisconnectHandler();
    public abstract class Receiver
    {
        public Receiver(string debugName, LogDispatcher logDispatcher)
        {
            this.logDispatcher = logDispatcher;
            this.debugName = debugName;
        }
        public void Log(string format, params object[] args)
        {
            if (logDispatcher != null)
                logDispatcher.Write(format, args);
        }
        public abstract void Receive();
        public abstract void ReceiveAsync();
        protected LogDispatcher logDispatcher;

        protected readonly string debugName;
    }
}
