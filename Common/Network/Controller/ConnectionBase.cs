﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public abstract class ConnectionBase
    {
        public ConnectionBase(string debugName)
        {
            this.DebugName = debugName;
        }

        private readonly string DebugName;

        protected abstract void OnPacketReceive(PacketReader reader);
        protected abstract void OnDisconnect();
        public abstract IPEndPoint RemoteIP();
        public string RemoteIPAddress()
        {
            var address = RemoteIP().Address.ToString();
            if (address.CompareTo("127.0.0.1") == 0)
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
            }
            return address;
        }

        public bool DoReceiveHook(PacketReader reader)
        {
            if (ReceiveHook != null)
            {
                ReceiveHook(reader, this);
                return true;
            }
            return false;
        }

        public void SendMessage(PacketBase packet)
        {
            sender.SendAsync(packet);
        }

        public virtual void ForceDisconnect()
        {
            OnDisconnect();
        }

        public event ReceiveConnectionHandler ReceiveHook = null;

        protected Sender sender;
        protected Receiver receiver;

        public Dictionary<PacketType, ReceiveConnectionHandler> PacketHandlers = new Dictionary<PacketType, ReceiveConnectionHandler>();
    }
}
