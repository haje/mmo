﻿using System;
using System.Net;
using System.Net.Sockets;

using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public class SocketClient : ClientBase
    {
        public SocketClient(string debugName, string hostname, int port, LogDispatcher logDispatcher = null)
            : base(debugName, logDispatcher)
        {
            this.hostname = hostname;
            this.port = port;

            IsConnected = false;
        }

        public override void ConnectServer()
        {
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(hostname, port);
                receiver = new SocketReceiver(
                    debugName,
                    socket,
                    ResponsePacketHandler,
                    OnDisconnect,logDispatcher);
                sender = new SocketSender(socket, logDispatcher);

                IsConnected = true;
                ReceiveAsync();
            }
            catch (Exception ex)
            {
                IsConnected = false;
                if (socket != null) socket.Close();
                Console.WriteLine("ConnectServer Exception: {0}", ex.Message);
                return;
            }
        }

        public override void Send(PacketBase packet)
        {
            sender.Send(packet);
        }

        public override void SendAsync(PacketBase packet)
        {
            sender.SendAsync(packet);
        }

        protected override void Receive()
        {
            receiver.Receive();
        }

        protected override void ReceiveAsync()
        {
            receiver.ReceiveAsync();
        }

        protected override void OnDisconnect()
        {
            //TODO : OnDisconnect
            if (socket != null)
                socket.Close();
        }

        Socket socket = null;
        SocketSender sender = null;
        SocketReceiver receiver = null;
    }
}
