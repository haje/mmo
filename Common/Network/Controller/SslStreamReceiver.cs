﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Security;
using System.Text;
using HAJE.MMO.Network.Packet;
using System.IO;

namespace HAJE.MMO.Network.Controller
{
    public class SslStreamReceiver : Receiver
    {
        public SslStreamReceiver
            (string debugName, 
            SslStream stream, 
            ReceiveHandler handler, 
            DisconnectHandler disconnectHandler,
            LogDispatcher logDispatcher = null)
            : base(debugName, logDispatcher)
        {
            this.stream = stream;
            this.buffer = new byte[PacketLayout.BufferSize];
            reader = new PacketReader();

            ReceivedPacketEvent += handler;
            DisconnectedStreamEvent += disconnectHandler;
        }
       
        public override void Receive() //Don't use this!
        {
            stream.Read(this.buffer, 0, PacketLayout.PacketSizeOffsetInBytes);

            int recvSize = -1;
            int size = 0;
            do
            {
                size = buffer[0];
                recvSize = stream.Read(this.buffer, 0, size);

                if (recvSize < size)
                {
                    continue;
                }
                else
                {
                    for (int i = 0; i < PacketLayout.PaddingLengthInBytes; i++)
                    {
                        byte padding = buffer[--size];
                        Debug.Assert(padding == PacketLayout.PaddingByte);
                    }
                    reader.SetBuffer(buffer, size);
                    Console.WriteLine(Encoding.UTF8.GetString(buffer));
                    break;
                }
            }
            while (recvSize != 0);
        }

        public override void ReceiveAsync()
        {
            recvSize = 0;
            stream.BeginRead(this.buffer, 0, PacketLayout.PacketSizeOffsetInBytes, ReadCallback, null);
        }

        void ReadCallback(IAsyncResult ar)
        {
            //넘겨야할 object가 있을때는 (someobject)ar.AsyncState; 사용
            try
            {
                int size = 0;
                recvSize += stream.EndRead(ar);
                size = buffer[0];
                if (recvSize < size)
                {
                    stream.BeginRead(this.buffer, recvSize, size, ReadCallback, null);
                }
                else
                {
                    for (int i = 0; i < PacketLayout.PaddingLengthInBytes; i++)
                    {
                        byte padding = buffer[--size];
                        Debug.Assert(padding == PacketLayout.PaddingByte);
                    }
                    reader.SetBuffer(buffer, size);
                    HandleResponsePacket(reader);

                    ReceiveAsync();
                }
            }
            catch (ObjectDisposedException e)
            {
                Log("SslStreamReceiver::ReadCallback Exception: {0}", e.Message);
                if (DisconnectedStreamEvent != null) DisconnectedStreamEvent();
                return;
            }
            catch (IOException e)
            {
                Log("SslStreamReceiver::ReadCallback Exception: {0}", e.Message);
                if (DisconnectedStreamEvent != null) DisconnectedStreamEvent();
                return;
            }
        }

        protected void HandleResponsePacket(PacketReader reader)
        {
            if (ReceivedPacketEvent != null)
                ReceivedPacketEvent(reader);
        }

        protected event ReceiveHandler ReceivedPacketEvent = delegate { };
        protected event DisconnectHandler DisconnectedStreamEvent = delegate { };

        int recvSize;
        PacketReader reader; 
        SslStream stream;
        byte[] buffer;
    }
}
