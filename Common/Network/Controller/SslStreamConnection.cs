﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;

namespace HAJE.MMO.Network.Controller
{
    //Serverside에서 클라이언트들을 관리하기 위한 객체
    public class SslStreamConnection : ConnectionBase, IDisposable
    {
        public delegate void disconnectEventHandler();

        public SslStreamConnection
            (string debugName,
            TcpClient client, 
            SslStream stream, 
            SslStreamResponseHandler handler,
            SslStreamDisconnectHandler disconnectHandler,
            LogDispatcher logDispatcher = null)
            : base(debugName)
        {
            this.client = client;
            this.stream = stream;

            receiver = new SslStreamReceiver(debugName, this.stream, OnPacketReceive, OnDisconnect,logDispatcher);
            sender = new SslStreamSender(this.stream,logDispatcher);

            outerHandler = handler;
            this.disconnectHandler = disconnectHandler;

            receiver.ReceiveAsync();
            IsConnected = true;
        }

        public void Dispose()
        {
            if (stream != null)
            {
                stream.Close();
                stream = null;
            }
            if (client != null)
            {
                client.Close();
                client = null;
            }
            IsConnected = false;
        }

        protected override void OnPacketReceive(PacketReader reader)
        {
            if (DoReceiveHook(reader) == false)
            {
                outerHandler(reader, this);
            }
        }

        protected override void OnDisconnect()
        {
            disconnectHandler(this);
            if (DisconnectEvent != null)
                DisconnectEvent();
        }

        public override IPEndPoint RemoteIP()
        {
            return client.Client.RemoteEndPoint as IPEndPoint;
        }

        public void SetPacketReceiveCallback(SslStreamResponseHandler handler)
        {
            outerHandler = handler;
        }

        TcpClient client;
        SslStream stream;
        SslStreamResponseHandler outerHandler;
        SslStreamDisconnectHandler disconnectHandler;

        public bool IsConnected { get; private set; }
        public event disconnectEventHandler DisconnectEvent;
    }
}
