﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public class SslStreamSender : Sender
    {
        public SslStreamSender(SslStream stream, LogDispatcher logDispatcher = null)
            :base(logDispatcher)
        {
            this.stream = stream;
            this.Writer = new PacketWriter();
            packetQueue = new Queue<PacketBase>();
            isWritable = true;
        }

        //Send와 AsyncSend로 들어오는 패킷은 SendQueue로 관리
        public override void Send(PacketBase packet)
        {
            packet.Serialize(this.Writer);
            int totalSize = WriteTotalSize(packet);
            stream.Write(Writer.Buffer, 0, totalSize);
        }

        public override void SendAsync(PacketBase packet)
        {
            lock(packetQueue)
            {
                if (!stream.CanWrite || !isWritable)
                {
                    packetQueue.Enqueue(packet);
                    return;
                }
                isWritable = false;
                packet.Serialize(this.Writer);
                packet.Release();
                int totalSize = WriteTotalSize(packet);
                stream.BeginWrite(Writer.Buffer, 0, totalSize, WriteCallback, null);
            }
        }
        
        void WriteCallback(IAsyncResult ar)
        {
            try
            {
                lock(packetQueue)
                {
                    stream.EndWrite(ar);
                    isWritable = true;
                    if (packetQueue.Count > 0)
                        SendAsync(packetQueue.Dequeue());
                }
            }
            catch (Exception ex)
            {
                Log("SslStreamSender Exception: {0}", ex.Message);
            }
        }

        bool isWritable = true;
        SslStream stream;
        Queue<PacketBase> packetQueue;
    }
}
