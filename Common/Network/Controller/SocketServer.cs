﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace HAJE.MMO.Network.Controller
{
    public delegate void SocketResponseHandler(PacketReader reader, SocketConnection connection);
    public delegate void SocketDisconnectHandler(SocketConnection connection);

    public class SocketServer : ServerBase
    {
        public SocketServer(string debugName, int port, SocketResponseHandler receiveCallback, LogDispatcher logDispatcher = null)
            :base(debugName, logDispatcher)
        {
            this.port = port;
            this.receiveCallback = receiveCallback;
        }

        public override void StartServer()
        {
            try
            {
                listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    listener.Bind(new IPEndPoint(IPAddress.Any, port));
                }
                catch
                {
                    listener.Bind(new IPEndPoint(IPAddress.Any, 0));
                }
                port = ((IPEndPoint)listener.LocalEndPoint).Port;
                listener.Listen(256);
                listener.BeginAccept(AcceptCallback, listener);
            }
            catch (Exception ex)
            {
                Log("StartServer Exception: {0}", ex.Message);
            }
        }

        public override void AcceptCallback(IAsyncResult ar)
        {
            Socket socket = null;
            try
            {
                socket = listener.EndAccept(ar);
                listener.BeginAccept(AcceptCallback, listener);

                SocketConnection connection = new SocketConnection(debugName + "ToClient", socket, receiveCallback, DisconnectCallback);
                connections.Add(connection);
            }
            catch (Exception ex)
            {
                if (socket != null) socket.Close();
                Log("AcceptCallback Exception: {0}", ex.Message);
            }
        }

        public void DisconnectCallback(SocketConnection connection)
        {
            lock(connections)
            {
                connections.Remove(connection);
                connection.Dispose();
            }
        }


        Socket listener = null;
        List<SocketConnection> connections = new List<SocketConnection>();
        SocketResponseHandler receiveCallback;
    }
}
