﻿using System;
using System.Net.Sockets;
using System.Collections.Generic;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public class SocketSender : Sender
    {
        public SocketSender(Socket socket, LogDispatcher logDispatcher = null)
            : base(logDispatcher)
        {
            this.socket = socket;
            this.Writer = new PacketWriter();
            packetQueue = new Queue<PacketBase>();
        }

        public override void Send(PacketBase packet)
        {
            packet.Serialize(this.Writer);
            int totalSize = WriteTotalSize(packet);
            socket.Send(Writer.Buffer, 0, totalSize, SocketFlags.None);
        }

        public override void SendAsync(PacketBase packet)
        {
            lock (packetQueue)
            {
                if (!socket.Connected)
                {
                    packetQueue.Enqueue(packet);
                    return;
                }
                packet.Serialize(this.Writer);
                int totalSize = WriteTotalSize(packet);
                socket.BeginSend(Writer.Buffer, 0, totalSize, SocketFlags.None, WriteCallback, null);
            }
        }

        void WriteCallback(IAsyncResult ar)
        {
            try
            {
                lock (packetQueue)
                {
                    socket.EndSend(ar);
                    if (packetQueue.Count > 0)
                        SendAsync(packetQueue.Dequeue());
                }
            }
            catch (Exception ex)
            {
                Log("SocketSender Exception: {0}", ex.Message);
                //TODO: Exception 처리
            }
        }

        Socket socket;
        Queue<PacketBase> packetQueue;
    }
}
