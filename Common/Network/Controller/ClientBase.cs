﻿using System.Collections.Generic;
using HAJE.MMO.Network.Packet;
using System;

namespace HAJE.MMO.Network.Controller
{
    public delegate void ReceiveHandler(PacketReader reader);
    public delegate void ReceiveConnectionHandler(PacketReader reader, ConnectionBase connection);

    public abstract class ClientBase : IDisposable
    {
        public ClientBase(string debugName, LogDispatcher logDispatcher)
        {
            this.debugName = debugName;
            this.logDispatcher = logDispatcher;
        }
        public abstract void ConnectServer();
        public abstract void Send(PacketBase packet);
        public abstract void SendAsync(PacketBase packet);
        protected abstract void Receive();
        protected abstract void ReceiveAsync();
        protected abstract void OnDisconnect();

        public void Dispose()
        {
            OnDisconnect();
        }

        protected void ResponsePacketHandler(PacketReader reader)
        {
            if (ReceiveHook != null)
            {
                ReceiveHook(reader);
                return;
            }

            lock (packetHandlerDic)
            {
                PacketType packetType = PacketBase.ReadPacketType(reader);
                ReceiveHandler handler = null;
                packetHandlerDic.TryGetValue(packetType, out handler);
                if (handler != null)
                    packetHandlerDic[packetType](reader);
            }
        }

        public void SetReceiveHandler(PacketType packetType, ReceiveHandler handler)
        {
            lock (packetHandlerDic)
            {
                if (packetHandlerDic.ContainsKey(packetType))
                    packetHandlerDic.Remove(packetType);
                packetHandlerDic.Add(packetType, handler);
            }
        }

        public void Log(string format, params object[] args)
        {
            if(logDispatcher != null)
                logDispatcher.Write(format, args);
        }

        public event ReceiveHandler ReceiveHook = null;

        public bool IsConnected { get; set; }

        protected readonly string debugName;
        protected string hostname;
        protected int port;
        
        protected Dictionary<PacketType, ReceiveHandler> packetHandlerDic = new Dictionary<PacketType, ReceiveHandler>();

        protected LogDispatcher logDispatcher;
    }

}
