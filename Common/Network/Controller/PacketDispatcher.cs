﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using System.Collections.Generic;

namespace HAJE.MMO.Network.Controller
{
    /// <summary>
    /// 네트워크쪽에서 마구잡이로 던진 패킷을 버퍼해 뒀다가 메인쓰레드로 크로스 스레드.
    /// 메인쓰레드에서 연결된 핸들러를 호출해 준다.
    /// 
    /// 이 함수의 모든 public 메서드들은 메인쓰레드에서만 호출되어야 한다.
    /// </summary>
    public class PacketDispatcher
    {
        public PacketDispatcher()
        {
            this.reader = new PacketReader();
        }

        public void AttachToClient(ClientBase client)
        {
            client.ReceiveHook += OnReceived;
        }

        public void AttachToConnection(ConnectionBase connection)
        {
            connection.ReceiveHook += OnReceived;
        }

        #region 네트워크 쓰레드에서 호출 될 수 있는 핸들러 및 헬퍼

        void OnReceived(PacketReader reader)
        {
            Buffer buffer = GetBuffer();
            buffer.Length = reader.Length;
            reader.CopyTo(buffer.ByteArray);
            lock (packetQueue)
            {
                packetQueue.Enqueue(buffer);
            }
        }

        void OnReceived(PacketReader reader, ConnectionBase connection)
        {
            Buffer buffer = GetBuffer();
            buffer.Length = reader.Length;
            reader.CopyTo(buffer.ByteArray);
            buffer.connection = connection;
            lock (packetQueue)
            {
                packetQueue.Enqueue(buffer);
            }
        }

        Buffer GetBuffer()
        {
            lock (garbageQueue)
            {
                if (garbageQueue.Count > 0)
                    return garbageQueue.Dequeue();
            }
            return new Buffer();
        }

        #endregion

        public void Dispatch()
        {
            while (packetQueue.Count > 0)
            {
                Buffer buffer = null;
                lock (packetQueue)
                {
                    buffer = packetQueue.Dequeue();
                }
                HandlePacket(buffer);
                lock (garbageQueue)
                {
                    garbageQueue.Enqueue(buffer);
                }
            }
        }

        void HandlePacket(Buffer buffer)
        {
            reader.SetBuffer(buffer.ByteArray, buffer.Length);
            var packetType = PacketBase.ReadPacketType(reader);
            if (buffer.connection == null)
            {
                var handler = packetHandlers[packetType];
                handler(reader);
            }
            else if (packetConnectionHandlers.ContainsKey(packetType))
            {
                var handler = packetConnectionHandlers[packetType];
                handler(reader, buffer.connection);
            }
            else
            {
                var handler = buffer.connection.PacketHandlers[packetType];
                handler(reader, buffer.connection);
            }
        }

        public void SetReceiveHandler(PacketType packetType, ReceiveHandler handler)
        {
            if (packetHandlers.ContainsKey(packetType))
                packetHandlers.Remove(packetType);

            if (handler != null)
                packetHandlers.Add(packetType, handler);
        }

        public void SetReceiveConnectionHandler(PacketType packetType, ReceiveConnectionHandler handler)
        {
            if (packetConnectionHandlers.ContainsKey(packetType))
                packetConnectionHandlers.Remove(packetType);

            if (handler != null)
                packetConnectionHandlers.Add(packetType, handler);
        }

        public void SetReceiveConnectionHandler(PacketType packetType, ReceiveConnectionHandler handler, ConnectionBase connection)
        {
            if (connection.PacketHandlers.ContainsKey(packetType))
                connection.PacketHandlers.Remove(packetType);

            if (handler != null)
                connection.PacketHandlers.Add(packetType, handler);
        }

        class Buffer
        {
            public byte[] ByteArray = new byte[PacketLayout.BufferSize];
            public int Length = 0;
            public ConnectionBase connection = null;
        }

        PacketReader reader;
        Queue<Buffer> packetQueue = new Queue<Buffer>();
        Queue<Buffer> garbageQueue = new Queue<Buffer>();
        Dictionary<PacketType, ReceiveHandler> packetHandlers = new Dictionary<PacketType, ReceiveHandler>();
        Dictionary<PacketType, ReceiveConnectionHandler> packetConnectionHandlers = new Dictionary<PacketType, ReceiveConnectionHandler>();
    }
}
