﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public class SslStreamClient : ClientBase
    {
        public SslStreamClient(string debugName, string hostname, int port, string serverAuthenticationName, LogDispatcher logDispatcher = null)
            : base(debugName, logDispatcher)
        {
            this.hostname = hostname;
            this.port = port;
            this.serverAuthenticationName = serverAuthenticationName; // AuthenticateAsClient를 위해 필요한 내용, 서버 인증서 이름이 들어간다.

            client = null;
            sender = null;
            receiver = null;

            IsConnected = false;
        }

        public override void ConnectServer()
        {
            try
            {
                client = new TcpClient(hostname, port);
                stream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);
            }
            catch (Exception ex)
            {
                IsConnected = false;
                Log(ex.ToString());
                
                return;
                //TODO : 각 Exception에 대해 다른 처리 
            }

            try
            {
                stream.AuthenticateAsClient(serverAuthenticationName);
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
                //TODO : 각 Exception에 대해 다른 처리 
                //AuthenticationException
                if (stream != null)
                {
                    stream.Close();
                    stream = null;
                }
                if (client != null)
                {
                    client.Close();
                    client = null;
                }

                IsConnected = false;
                return;
            }

            receiver = new SslStreamReceiver(debugName, stream, ResponsePacketHandler, OnDisconnect);
            sender = new SslStreamSender(stream);

            IsConnected = true;
            ReceiveAsync(); //항상 Receive를 열어놓는다.
        }

        public override void Send(PacketBase packet)
        {
            sender.Send(packet);
        }

        public override void SendAsync(PacketBase packet)
        {
            sender.SendAsync(packet);
        }

        protected override void Receive()
        {
            receiver.Receive();
        }

        protected override void ReceiveAsync()
        {
            receiver.ReceiveAsync();
        }

        protected override void OnDisconnect()
        {
            //TODO : OnDisconnect
            if (client != null)
                client.Close();
        }

        public bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            return true;
            //신뢰할 수 있는 인증서 체크는 서버에서만 하도록
            //TODO : 제대로 인증서 체크를 할 수 있도록 수정
        }


        string serverAuthenticationName;

        TcpClient client;
        SslStream stream;

        SslStreamReceiver receiver;
        SslStreamSender sender;
    }
}
