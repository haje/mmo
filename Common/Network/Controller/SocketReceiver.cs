﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public class SocketReceiver : Receiver
    {
        public SocketReceiver
            (
            string debugName,
            Socket socket, 
            ReceiveHandler handler,
            DisconnectHandler disconnectHandler, 
            LogDispatcher logDispatcher = null)
            : base(debugName, logDispatcher)
        {
            this.socket = socket;
            this.buffer = new byte[PacketLayout.BufferSize];
            reader = new PacketReader();

            ReceivedPacketEvent += handler;
            DisconnectedSocketEvent += disconnectHandler;
        }

        public override void Receive() // TODO: remove or unuse
        {
            int recvSize = 0;
            while (recvSize < PacketLayout.PacketSizeOffsetInBytes)
            {
                recvSize = socket.Receive(
                    buffer,
                    recvSize,
                    PacketLayout.PacketSizeOffsetInBytes - recvSize,
                    SocketFlags.None
                );
            }
            int size = buffer[0];

            while (recvSize < size)
            {
                recvSize += socket.Receive(
                    buffer,
                    recvSize,
                    size - recvSize,
                    SocketFlags.None);
            }

            for (int i = 0; i < PacketLayout.PaddingLengthInBytes; i++)
            {
                byte padding = buffer[--size];
                Debug.Assert(padding == PacketLayout.PaddingByte);
            }
        }

        public override void ReceiveAsync()
        {
            recvSize = 0;
            socket.BeginReceive(buffer, 0, PacketLayout.PacketSizeOffsetInBytes, SocketFlags.None, ReadCallback, null);
        }

        void ReadCallback(IAsyncResult ar)
        {
            try
            {
                recvSize += socket.EndReceive(ar);
            }
            catch (SocketException)
            {
                if (!isDisconnected)
                    DisconnectedSocketEvent();
                isDisconnected = true;
                socket.Close();
                return;
            }
            catch (ObjectDisposedException)
            {
                if (!isDisconnected)
                    DisconnectedSocketEvent();
                isDisconnected = true;
                return;
            }

            int size = buffer[0];

            if (recvSize < size)
            {
                socket.BeginReceive(buffer, recvSize, size, SocketFlags.None, ReadCallback, null);
            }
            else
            {
                for (int i = 0; i < PacketLayout.PaddingLengthInBytes; i++)
                {
                    byte padding = buffer[--size];
                    Debug.Assert(padding == PacketLayout.PaddingByte);
                }
                reader.SetBuffer(buffer, size);
                HandleResponsePacket(reader);

                ReceiveAsync();
            }
         }

        protected void HandleResponsePacket(PacketReader reader)
        {
            if (ReceivedPacketEvent != null)
                ReceivedPacketEvent(reader);
        }

        protected event ReceiveHandler ReceivedPacketEvent = delegate { };
        protected event DisconnectHandler DisconnectedSocketEvent = delegate { };
        bool isDisconnected = false;

        int recvSize;
        Socket socket;
        byte[] buffer;
        PacketReader reader;
    }
}
