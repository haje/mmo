﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;

using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Network.Controller
{
    public delegate void SslStreamResponseHandler(PacketReader reader, SslStreamConnection connection);
    public delegate void SslStreamDisconnectHandler(SslStreamConnection connection);

    public class SslStreamServer : ServerBase
    {
        readonly string PFXFilePath = PathHelper.BinaryDirectory + @"CARoot.pfx"; //TODO : Read from config file
        readonly string PFXFilePassword = "82538253";  //TODO : Read from config file

        public SslStreamServer(string debugName, int port, SslStreamResponseHandler receiveCallback, LogDispatcher logDispatcher = null)
            :base(debugName, logDispatcher)
        {
            this.port = port;
            this.receiveCallback = receiveCallback;
        }

        public override void StartServer()
        {
            try
            {
                serverCertificate = new X509Certificate(PFXFilePath, PFXFilePassword);
                listener = new TcpListener(IPAddress.Any, port);
            
                listener.Start(256);
                listener.BeginAcceptTcpClient(AcceptCallback, serverCertificate);
                //Console.WriteLine("BeginAcceptTcpClient");
            }
            catch (Exception ex)
            {
                Log("SslStreamServer::StartServer Exception : {0}", ex.ToString());
                //Console.WriteLine("StartServer Exception : {0}",ex.Message);
                //Console.WriteLine("{0}", ex.StackTrace);
                //Console.WriteLine("{0}", ex.HelpLink);
                //TODO : 다양한 Exception에 대해서 다르게 처리
            }
        }

        public override void AcceptCallback(IAsyncResult ar)
        {
            TcpClient client = null;
            try
            {
                client = listener.EndAcceptTcpClient(ar);
                listener.BeginAcceptTcpClient(AcceptCallback, serverCertificate);
            }
            catch (Exception ex)
            {
                Log("SslStreamServer::AcceptCallback Exception: {0}", ex.Message);
                //Console.WriteLine("AcceptTcpClientCallback Exception : {0}", ex.Message);
                //TODO : 다양한 Exception에 대해서 다르게 처리
                //SocketException
                //ObjectDisposedException
                return;
            }

            SslStream sslStream = new SslStream(client.GetStream(), false);

            try
            {
                sslStream.AuthenticateAsServer(serverCertificate, false, SslProtocols.Tls, true);
                SslStreamConnection newClient = new SslStreamConnection(
                    debugName, 
                    client, 
                    sslStream, 
                    receiveCallback, 
                    DisconnectCallback, 
                    logDispatcher
                );
                connections.Add(newClient);
            }
            catch (AuthenticationException ex)
            {
                Log("SslStreamServer::AcceptCallback AuthenticationException: {0}", ex.Message);
                //Console.WriteLine("AcceptTcpClientCallback AuthenticationException : {0}", ex.Message);
                //TODO : 다양한 Exception에 대해서 다르게 처리
                sslStream.Close();
                client.Close();
                return;
            }
            catch (Exception ex)
            {
                Log("SslStreamServer::AcceptCallback Unknown Exception: {0}", ex.Message);
                sslStream.Close();
                client.Close();
                return;
            }
        }

        //TODO : Base Class로
        public void DisconnectCallback(SslStreamConnection connection)
        {
            lock (connections)
            {
                connections.Remove(connection);
                connection.Dispose();
            }
        }

        X509Certificate serverCertificate = null;
        TcpListener listener = null;
        List<SslStreamConnection> connections = new List<SslStreamConnection>(); //이후 Dictionary로 바뀔 여지 있음

        SslStreamResponseHandler receiveCallback;
    }

}
