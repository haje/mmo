﻿using System;
using System.Net;
using System.Net.Sockets;

namespace HAJE.MMO.Network.Controller
{
    public class SocketConnection : ConnectionBase, IDisposable
    {
        public SocketConnection
            (string debugName,
            Socket socket, 
            SocketResponseHandler handler, 
            SocketDisconnectHandler disconnectHandler, 
            LogDispatcher logDispatcher = null)
            : base(debugName)
        {
            this.socket = socket;

            receiver = new SocketReceiver(
                debugName,
                this.socket,
                OnPacketReceive,
                OnDisconnect,logDispatcher);
            sender = new SocketSender(this.socket, logDispatcher);

            outerHandler = handler;
            this.disconnectHandler = disconnectHandler;
            receiver.ReceiveAsync();
        }

        protected override void OnPacketReceive(PacketReader reader)
        {
            if (DoReceiveHook(reader) == false)
            {
                outerHandler(reader, this);
            }
        }

        protected override void OnDisconnect()
        {
            disconnectHandler(this);
        }

        public override IPEndPoint RemoteIP()
        {
            return socket.RemoteEndPoint as IPEndPoint;
        }

        public void Dispose()
        {
            socket.Close();
        }

        Socket socket;
        SocketResponseHandler outerHandler;
        SocketDisconnectHandler disconnectHandler;
    }
}
