﻿using System;

namespace HAJE.MMO.Network.Controller
{
    public abstract class ServerBase
    {
        public ServerBase(string debugName, LogDispatcher logDispatcher)
        {
            this.logDispatcher = logDispatcher;
            this.debugName = debugName;
        }

        protected readonly string debugName;

        public abstract void StartServer();
        public abstract void AcceptCallback(IAsyncResult ar);
        //public abstract void Send(Writer writer);
        //public abstract void SendAsync(Writer writer);
        //public abstract void Receive(Reader writer);
        //public abstract void ReceiveAsync(Reader writer);
        public void Log(string format, params object[] args)
        {
            if (logDispatcher != null)
                logDispatcher.Write(format, args);
        }

        protected int port;
        public int Port
        {
            get
            {
                return port;
            }
        }

        protected LogDispatcher logDispatcher;
    }
}
