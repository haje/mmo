﻿using HAJE.MMO.Network.Packet;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace HAJE.MMO.Network
{
    public class PacketReader
    {
        public void SetBuffer(IList<byte> buffer, int length)
        {
            offset = PacketLayout.PacketSizeOffsetInBytes;
            this.buffer = buffer;
            this.length = length;
        }

        public void ReadBytes(byte[] bytes)
        {
            ReadBytes(bytes, 0, bytes.Length);
        }

        public void ReadBytes(byte[] bytes, int length)
        {
            ReadBytes(bytes, 0, length);
        }

        public void ReadBytes(byte[] bytes, int offset, int length)
        {
            Debug.Assert(offset + length < buffer.Count);
            for (int i = 0; i < length; i++)
                bytes[offset + i] = buffer[this.offset + i];
            this.offset += length;
        }

        public byte ReadByte()
        {
            byte b = (byte)buffer[offset];
            this.offset += 1;
            return b;
        }

        public bool ReadBool()
        {
            return ReadByte() == 1;
        }

        public Int16 ReadInt16()
        {
            uint byte1 = (uint)ReadByte();
            uint byte2 = (uint)ReadByte() << 8;
            uint retval = byte1 | byte2;
            return (Int16)retval;
        }

        public Int32 ReadInt32()
        {
            uint byte1 = (uint)ReadByte();
            uint byte2 = (uint)ReadByte() << 8;
            uint byte3 = (uint)ReadByte() << 16;
            uint byte4 = (uint)ReadByte() << 24;
            uint retval = byte1 | byte2 | byte3 | byte4;
            return (Int32)retval;
        }

        public float ReadFloat()
        {
            uint byte1 = (uint)ReadByte();
            uint byte2 = (uint)ReadByte() << 8;
            uint byte3 = (uint)ReadByte() << 16;
            uint byte4 = (uint)ReadByte() << 24;
            uint retval = byte1 | byte2 | byte3 | byte4;
            SingleUIntUnion su;
            su.SingleValue = 0;
            su.UIntValue = retval;
            return su.SingleValue;
        }

        public T ReadEnum<T>(T defValue)
        {
            int ptype = ReadInt16();
            if (Enum.IsDefined(typeof(T), ptype))
                return (T)(object)ptype;
            else
                return defValue;
        }

        public Vector2 ReadVector2()
        {
            float X = ReadFloat();
            float Y = ReadFloat();
            return new Vector2(X, Y);
        }

        public Vector3 ReadVector3()
        {
            float X = ReadFloat();
            float Y = ReadFloat();
            float Z = ReadFloat();
            return new Vector3(X, Y, Z);
        }

        public void ReadPositionInfo(PositionInfo info)
        {
            int ptype = ReadInt16();
            if (Enum.IsDefined(typeof(MotionState), ptype))
                info.MotionState = (MotionState)ptype;
            else
                info.MotionState = MotionState.Stop;
            info.FieldId = ReadInt32();
            info.Position = ReadVector2();
            info.Velocity = ReadVector2();
            info.Direction = ReadVector3();
        }

        public void ReadPositionInField(out PositionInField positionInField)
        {
            positionInField = new PositionInField(ReadInt32(), ReadVector3(), ReadBool());
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct SingleUIntUnion
        {
            [FieldOffset(0)]
            public float SingleValue;
            [FieldOffset(0)]
            public uint UIntValue;
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct DoubleUIntUnion
        {
            [FieldOffset(0)]
            public double DoubleValue;
            [FieldOffset(0)]
            public uint UIntValue1;
            [FieldOffset(4)]
            public uint UIntValue2;
        }

        public double ReadDouble()
        {
            uint byte1 = (uint)ReadByte();
            uint byte2 = (uint)ReadByte() << 8;
            uint byte3 = (uint)ReadByte() << 16;
            uint byte4 = (uint)ReadByte() << 24;
            uint byte5 = (uint)ReadByte();
            uint byte6 = (uint)ReadByte() << 8;
            uint byte7 = (uint)ReadByte() << 16;
            uint byte8 = (uint)ReadByte() << 24;
            uint retval1 = byte1 | byte2 | byte3 | byte4;
            uint retval2 = byte5 | byte6 | byte7 | byte8;
            DoubleUIntUnion du;
            du.DoubleValue = 0;
            du.UIntValue1 = retval1;
            du.UIntValue2 = retval2;
            return du.DoubleValue;

        }

        public string ReadString()
        {
            int lengthInBytes = ReadInt32(); //앞으로 올 스트링 길이를 읽어들임
            if (lengthInBytes == 0)
                return null;

            byte[] byteArray = new byte[lengthInBytes];
            ReadBytes(byteArray);
            string result = System.Text.Encoding.UTF8.GetString(byteArray);
            return result;
        }

        public int Length
        {
            get
            {
                return length;
            }
        }

        public void CopyTo(byte[] buffer)
        {
            Debug.Assert(buffer.Length >= length);
            int i = 0;
            for (i = 0; i < length; i++)
            {
                buffer[i] = this.buffer[i];
            }
            for (; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }
        }

        IList<byte> buffer;
        int offset;
        int length;
    }
}
