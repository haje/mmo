﻿
namespace HAJE.MMO.Network
{
    /// <summary>
    /// 패킷 레이아웃 관련 상수
    /// </summary>
    public class PacketLayout
    {
        public const int BufferSize = 256;
        public const int PacketSizeOffsetInBytes = 1;
        public const byte PaddingByte = 0x42;
        public const int PaddingLengthInBytes = 1;
    }
}
