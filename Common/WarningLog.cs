﻿using System;

namespace HAJE.MMO
{
    public class WarningLog : LogDispatcher
    {
        private static WarningLog instance;
        public static WarningLog Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WarningLog("Warning Log", 10);
                    instance.DebugOutputEnable = true;
                    instance.ConsoleOutputEnable = true;
                }

                return instance;
            }
        }

        private WarningLog(String ownerName, int bufferSize)
            : base(ownerName, bufferSize)
        {
        }
    }
}
