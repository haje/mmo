﻿namespace HAJE.MMO
{
    /// <summary>
    /// 애니메이션에 이펙트가 붙을 수 있는 지점의 목록
    /// </summary>
    public enum AttachmentPoint
    {
        Origin,
        MagicWand,
        Foot,
        ComboPoint1,
        ComboPoint2,
        ComboPoint3,
        ComboPoint4,
        ComboPoint5,
    }
}
