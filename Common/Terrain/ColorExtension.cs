﻿using System.Drawing;

namespace HAJE.MMO.Terrain
{
    public static class ColorExtension
    {
        public static bool IsGray(this Color color)
        {
            return color.R == color.G && color.R == color.B;
        }
        public static int GetIntensity(this Color color)
        {
            return (color.R + color.G + color.B) / 3;
        }
    }
}
