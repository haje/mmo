﻿using OpenTK;
using System;
using System.IO;
using System.Linq;

namespace HAJE.MMO.Terrain
{
    public class Heightmap
    {
        protected float[,] heightmap;
        protected float gridSize;
        protected float minHeight;
        protected float maxHeight;

        public float GridSize
        {
            get
            {
                return gridSize;
            }
        }

        public Stream HeightmapRawDataStream
        {
            get
            {
                return new MemoryStream(heightmap.Cast<float>().Select(h => (byte)h).ToArray());
            }
        }

        public float[,] HeightmapRawData
        {
            get
            {
                return heightmap;
            }
            set
            {
                heightmap = value;
                var flattened = value.Cast<float>();
                var newMax = flattened.Max();
                var newMin = flattened.Min();
                if (newMax > maxHeight)
                {
                    maxHeight = newMax;
                }
                if (newMin < minHeight)
                {
                    minHeight = newMin;
                }
            }
        }

        public float MinHeight
        {
            get
            {
                return minHeight;
            }
        }

        public float MaxHeight
        {
            get
            {
                return maxHeight;
            }
        }

        public Heightmap(float[,] heightmap, float gridSize, float minHeight, float maxHeight)
        {
            this.heightmap = heightmap;
            this.gridSize = gridSize;
            this.minHeight = minHeight;
            this.maxHeight = maxHeight;
        }

        public float GetHeight(Vector2 xzPos)
        {
            int xLow = (int)Math.Floor(xzPos.X);
            int xHigh = (int)Math.Ceiling(xzPos.X);
            int zLow = (int)Math.Floor(xzPos.Y);
            int zHigh = (int)Math.Ceiling(xzPos.Y);
            float height = 0;
            if (0 <= zLow && zLow < heightmap.GetLength(0)
                && 0 <= zHigh && zHigh < heightmap.GetLength(0)
                && 0 <= xLow && xLow < heightmap.GetLength(1)
                && 0 <= xHigh && xHigh < heightmap.GetLength(1))
            {
                float zxHeight = heightmap[zLow, xLow];
                float zXHeight = heightmap[zLow, xHigh];
                float ZxHeight = heightmap[zHigh, xLow];
                float ZXHeight = heightmap[zHigh, xHigh];
                float xOffset = xzPos.X - xLow;
                float zOffset = 1 - (xzPos.Y - zLow);
                Vector2 zHeights = Vector2.Lerp
                    (
                    new Vector2(zxHeight, ZxHeight),
                    new Vector2(zXHeight, ZXHeight),
                    xOffset
                    );
                height = zHeights.X * zOffset + zHeights.Y * (1 - zOffset);
            }

            return height;
        }
    }
}
