﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace HAJE.MMO.Terrain
{
    public static class TerrainPreprocessor
    {
        private const float maxHeight = 10;
        private const int expansionRate = 5;
        private const int seaPixelValue = 0x7f;
        private const int pruningRadius = 40;
        private const float maxNoise = 0.05f;
        private const float gridSize = 1.0f;
        private static Dictionary<int, Heightmap> heightMapCache = new Dictionary<int, Heightmap>();
        
        public static string Path(int index)
        {
            return String.Format("{0}/Terrain/{1}.bmp", PathHelper.ResourcePath, index);
        }

        /// <summary>
        /// Grayscale Bitmap 파일을 입력받아서 적당히 확대된 Height map을 리턴함
        /// </summary>
        /// <param name="input">Bitmap 파일</param>
        /// <returns>Height map</returns>
        public static Heightmap Process(Bitmap input, int index)
        {
            if (heightMapCache.ContainsKey(index))
                return heightMapCache[index];
            // 실행 시간 측정 시작
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            // 잡다한 상수 들
            int inputWidth = input.Width;
            int inputHeight = input.Height;
            int heightMapWidth = inputWidth * expansionRate;
            int heightMapHeight = inputHeight * expansionRate;
            float[,] heightMapData = new float[heightMapWidth, heightMapHeight];

            int inputMaxHeight = 0x00;
            int inputMinHeight = 0xFF;

            // Peak point들을 리스트에 넣음
            // 0x80이 아닌 값을 가진 픽셀들을 Peak point라고 정의함
            List<Vector3> peakPointList = new List<Vector3>();
            for (int x = 0; x < inputWidth; x++)
                for (int y = 0; y < inputHeight; y++)
                {
                    int intensity = input.GetPixel(x, y).GetIntensity();
                    inputMaxHeight = Math.Max(inputMaxHeight, intensity);
                    inputMinHeight = Math.Min(inputMinHeight, intensity);

                    if (intensity != seaPixelValue)
                        peakPointList.Add(new Vector3(
                            expansionRate * y + expansionRate / 2,
                            expansionRate * x + expansionRate / 2, 
                            (float)intensity / seaPixelValue - 1));
                }
            
            // Peak point로 부터 가우시안을 써서 적당히 주변 값들을 보정함
            // Pruning 기준은 euclidean distance가 pruningRadius보다 길 경우 제외
            foreach (Vector3 v in peakPointList)
            {
                int xstart = Math.Max(0, (int)v.X - pruningRadius + 1);
                int xend = Math.Min((int)v.X + pruningRadius, heightMapWidth);
                for (int x = xstart; x < xend; x++)
                {
                    int ystart = Math.Max(0, (int)v.Y - pruningRadius + 1);
                    int yend = Math.Min((int)v.Y + pruningRadius, heightMapHeight);
                    for (int y = ystart; y < yend; y++)
                    {
                        int dx = (int)v.X - x;
                        int dy = (int)v.Y - y;
                        if (dx * dx + dy * dy > pruningRadius * pruningRadius) continue;

                        heightMapData[x, y] += GaussianHelper(dx, dy) * v.Z;
                    }
                }
            }

            // Height map의 min, max값 탐색
            float lowMax = 0;
            float highMax = 0;
            for (int x = 0; x < heightMapWidth; x++)
                for (int y = 0; y < heightMapHeight; y++)
                {
                    lowMax = Math.Min(lowMax, heightMapData[x, y]);
                    highMax = Math.Max(highMax, heightMapData[x, y]);
                }

            // Height map의 각 height들이 0에서 10 사이의 값을 갖도록 normalize한 뒤 Perlin noise를 더함.
            PerlinNoise pn = new PerlinNoise(heightMapWidth, heightMapHeight, DateTime.Now.Millisecond);
            float highIntensityFactor = (inputMaxHeight - seaPixelValue) / seaPixelValue;
            float lowIntensityFactor = (seaPixelValue - inputMinHeight) / seaPixelValue;
            for (int x = 0; x < heightMapWidth; x++)
                for (int y = 0; y < heightMapHeight; y++)
                {
                    float r = pn.GetRandomHeight(x, y, maxNoise * 2, 0.5f, 5f, 0.5f, 5) - maxNoise;

                    if (heightMapData[x, y] > 0)
                        heightMapData[x, y] = (heightMapData[x, y] / highMax / 2 + 0.5f) / highIntensityFactor * maxHeight + r;
                    else if (heightMapData[x, y] < 0)
                        heightMapData[x, y] = (heightMapData[x, y] / -lowMax / 2 + 0.5f) / lowIntensityFactor * maxHeight + r;
                    else
                        heightMapData[x, y] = 5f + r;
                }

            // Debug용 출력
            // ArrayToBitmap(heightMap, heightMapWidth, heightMapHeight).Save("Resource/Terrain/Debug/test.bmp");

            // 실행 시간 출력
            sw.Stop();
            Console.WriteLine("Time Elapsed: {0}", sw.ElapsedMilliseconds);

            // Height map 리턴
            Heightmap heightmap = new Heightmap(heightMapData, gridSize, 0, maxHeight);
            heightMapCache.Add(index, heightmap);
            return heightmap;
        }

        private static float[,] gaussianCache = new float[pruningRadius, pruningRadius];
        private static float GaussianHelper(int dx, int dy)
        {
            float sigma2 = 200;
            int adx = Math.Abs(dx);
            int ady = Math.Abs(dy);

            if (gaussianCache[adx, ady] == 0)
            {
                float value = (float)Math.Exp(-(dx * dx + dy * dy) / (2 * sigma2));
                gaussianCache[adx, ady] = value;
            }

            return gaussianCache[adx, ady];
        }

        public static float[,] BitmapToArray(Bitmap bitmap)
        {
            float[,] floatArray = new float[bitmap.Width, bitmap.Height];

            for (int x = 0; x < bitmap.Width; x++)
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color pixel = bitmap.GetPixel(x, y);

                    if (pixel.IsGray())
                        floatArray[x, y] = pixel.GetIntensity() * maxHeight / 0xFF;
                }

            return floatArray;
        }
        public static Bitmap ArrayToBitmap(float[,] array, int width, int height)
        {
            Bitmap b = new Bitmap(width, height);
            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                {
                    int intensity = Math.Max(0, Math.Min(0xFF, (int)(array[x, y] / maxHeight * 0xFF)));
                    b.SetPixel(x, y, Color.FromArgb(intensity, intensity, intensity));
                }

            return b;
        }
    }
}
