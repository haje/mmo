﻿using System;
using System.Linq;

namespace HAJE.MMO
{
    public class UnitTestAttribute : Attribute { }
    
    public static class UnitTest
    {
        public static void Run(LogDispatcher log)
        {
            UnitTest.Log = log;
            log.Write("UnitTest 시작");
            RunAllTest();
            log.Write("UnitTest 끝");
        }

        private static void RunAllTest()
        {
            Log.Write("전체 자동 테스트 시작");

            var typesWithMyAttribute =
                from a in AppDomain.CurrentDomain.GetAssemblies().AsParallel()
                from t in a.GetTypes()
                let attributes = t.GetCustomAttributes(typeof(UnitTestAttribute), true)
                where attributes != null && attributes.Length > 0
                select new { Type = t, Attributes = attributes.Cast<UnitTestAttribute>() };

            foreach (var t in typesWithMyAttribute)
            {
                var method = t.Type.GetMethod("RunUnitTest");
                if (method == null)
                    throw new Exception("UnitTestAttribute가 정의되어 있지만 RunUnitTest 메서드를 정의하지 않는 타입이 있습니다. 타입 이름:" + t.Type.Name);
                if ((bool)method.Invoke(null, null) == false)
                    throw new Exception(String.Format("UnitTest Failed at {0}", t.Type.Name));
            }

            Log.Write("전체 자동 테스트 종료");
        }

        public static void Begin(string name)
        {
            if (!string.IsNullOrWhiteSpace(testModuleName))
                throw new Exception("이전 테스트 코드의 UnitTest.End() 호출 없이 다음 테스트가 시작되었습니다. 이전 모듈: " + testModuleName);
            Log.Write( "UnitTest : "+ name + " 테스트 시작");

            testModuleName = name;
            testCount = 0;
            testFailed = false;
        }

        public static void Test(bool testValue)
        {
            Test(testValue, (++testCount).ToString());
        }

        public static void Test(bool testValue, string description)
        {
            if (testValue == false)
            {
                testFailed = true;
                throw new Exception(testModuleName + " 유닛 테스트 실패");
            }
        }

        public static void End()
        {
            Log.Write("UnitTest : " + string.Format("{0} 테스트 {1}", testModuleName, !testFailed ? "성공" : "실패"));

            testModuleName = null;
        }

        public static LogDispatcher Log { get; private set; }
        static string testModuleName;
        static int testCount;
        static bool testFailed;
    }
}
