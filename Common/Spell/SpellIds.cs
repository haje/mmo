﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Spell
{
    public static class SpellIds
    {
        static SpellIds()
        {
            var valueList = Enum.GetValues(typeof(SpellId));
            foreach (var v in valueList)
            {
                all.Add((SpellId)v);
            }
        }

        static List<SpellId> all = new List<SpellId>();
        public static IReadOnlyList<SpellId> All
        {
            get
            {
                return all;
            }
        }
    }
}
