﻿namespace HAJE.MMO.Spell
{
    public enum GainType
    {
        Basic,
        Purchase,
        FieldDrop
    }
}
