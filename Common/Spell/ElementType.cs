﻿
namespace HAJE.MMO.Spell
{
    public enum ElementType
    {
        None,
        Fire,
        Frost,
        Arcane,
        Lightning,
        Wind,
        Shadow,
        Max,
    }
}
