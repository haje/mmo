﻿
namespace HAJE.MMO.Spell
{
    public enum SpellId
    {
        None,
        ArcaneMissile,
        ArcaneSlowMissile,
        ArcaneExplosion,
        IceArrow,
        IceLance,
        DeepFreeze,
        FrozenOrb,
        IceCloud,
        CurseOfFrost,
        IceSlide,
        ConeOfCold,
        Icicles,
        IceSpike,
    }
}
