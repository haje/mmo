﻿
using System.Diagnostics;

namespace HAJE.MMO.Spell
{
    public partial class SpellInfo
    {
        static SpellInfo()
        {
            Initialize();

            var none = new SpellInfo(
                SpellId.None,
                ElementType.None,
                "Null Spell"
            );
            none.SpellTooltip = "";
            none.GainType = GainType.Basic;
            infoMap.Add(SpellId.None, none);

            var arcaneMissile = new SpellInfo(
                SpellId.ArcaneMissile,
                ElementType.Arcane,
                "Arcane Missile"
            );
            arcaneMissile.GainType = GainType.Basic;
            arcaneMissile.AimType = AimType.Object;
            arcaneMissile.ManaCost = 5;
            arcaneMissile.Damage = 150;
            arcaneMissile.Range = 45.0f;
            arcaneMissile.Speed = 130.0f;
            arcaneMissile.CastingTime = (Second)0.5f;
            arcaneMissile.IconPath = "Resource/Image/Icon/Arcane Missile.png";
            arcaneMissile.SpellTooltip = "아케인 미사일이시다";
            infoMap.Add(SpellId.ArcaneMissile, arcaneMissile);

            var arcaneSlowMissile = new SpellInfo(
                SpellId.ArcaneSlowMissile,
                ElementType.Arcane,
                "Arcane Slow Missile"
            );
            arcaneSlowMissile.GainType = GainType.Purchase;
            arcaneSlowMissile.AimType = AimType.Direction;
            arcaneSlowMissile.PurchaseCost = 0;
            arcaneSlowMissile.PurchaseCount = 3;
            arcaneSlowMissile.ManaCost = 5;
            arcaneSlowMissile.Damage = 300;
            arcaneSlowMissile.Range = 50.0f;
            arcaneSlowMissile.Radius = 2.0f;
            arcaneSlowMissile.Speed = 10.0f;
            arcaneSlowMissile.CastingTime = (Second)0.5f;
            arcaneSlowMissile.UpdateTime = (Second)0.5f;
            arcaneSlowMissile.IconPath = "Resource/Image/Icon/Arcane Slow Missile.png";
            arcaneSlowMissile.SpellTooltip = "아케인 슬로우 미사일이시다.";
            infoMap.Add(SpellId.ArcaneSlowMissile, arcaneSlowMissile);

            var arcaneExplosion = new SpellInfo(
                SpellId.ArcaneExplosion,
                ElementType.Arcane,
                "Arcane Explosion"
            );
            arcaneExplosion.GainType = GainType.Basic;
            arcaneExplosion.AimType = AimType.Ground;
            arcaneExplosion.ManaCost = 0;
            arcaneExplosion.ComboPointCost = 3;
            arcaneExplosion.Damage = 500;
            arcaneExplosion.Range = 45.0f;
            arcaneExplosion.Radius = 3.0f;
            arcaneExplosion.CastingTime = (Second)2.5f;
            arcaneExplosion.DelayTime = (Second)0.7f;
            arcaneExplosion.IconPath = "Resource/Image/Icon/Arcane Explosion.png";
            arcaneExplosion.SpellTooltip = "아케인 익스플로전이시다.";
            infoMap.Add(SpellId.ArcaneExplosion, arcaneExplosion);

            ////////////////////////////////////////////////////////////////
            /* 냉기 주문 */
            ////////////////////////////////////////////////////////////////

            /* 얼음 화살 Ice Arrow */
            var iceArrow = new SpellInfo(SpellId.IceArrow, ElementType.Frost, "Ice Arrow");
            iceArrow.AimType = AimType.Direction;
            iceArrow.IconPath = "Resource/Image/Icon/IceArrow.png";
            infoMap.Add(SpellId.IceArrow, iceArrow);

            /* 얼음 창 Ice Lance */
            var iceLance = new SpellInfo(SpellId.IceLance, ElementType.Frost, "Ice Arrow");
            iceLance.AimType = AimType.Direction;
            iceLance.IconPath = "Resource/Image/Icon/IceLance.png";
            infoMap.Add(SpellId.IceLance, iceLance);

            /* 동결 Deep Freeze */
            var deepFreeze = new SpellInfo(SpellId.DeepFreeze, ElementType.Frost, "Deep Freeze");
            deepFreeze.AimType = AimType.Direction;
            deepFreeze.IconPath = "Resource/Image/Icon/DeepFreeze.png";
            infoMap.Add(SpellId.DeepFreeze, deepFreeze);

            /* 얼음구슬 Frozen Orb */
            var frozenOrb = new SpellInfo(SpellId.FrozenOrb, ElementType.Frost, "Frozen Orb");
            frozenOrb.AimType = AimType.Direction;
            frozenOrb.IconPath = "Resource/Image/Icon/FrozenOrb.png";
            infoMap.Add(SpellId.FrozenOrb, frozenOrb);

            /* 냉기 구름 Ice Cloud */
            var iceCloud = new SpellInfo(SpellId.IceCloud, ElementType.Frost, "Ice Cloud");
            iceCloud.AimType = AimType.Direction;
            iceCloud.IconPath = "Resource/Image/Icon/IceCloud.png";
            infoMap.Add(SpellId.IceCloud, iceCloud);

            /* 서리 저주 Curse Of Frost */
            var curseOfFrost = new SpellInfo(SpellId.CurseOfFrost, ElementType.Frost, "Curse Of Frost");
            curseOfFrost.AimType = AimType.Direction;
            curseOfFrost.IconPath = "Resource/Image/Icon/CurseOfFrost.png";
            infoMap.Add(SpellId.CurseOfFrost, curseOfFrost);

            /* 아이스 슬라이드 Ice Slide */
            var iceSlide = new SpellInfo(SpellId.IceSlide, ElementType.Frost, "Ice Slide");
            iceSlide.AimType = AimType.Direction;
            iceSlide.IconPath = "Resource/Image/Icon/IceSlide.png";
            infoMap.Add(SpellId.IceSlide, iceSlide);

            /* 차가운 바람 Cone Of Cold */
            var coneOfCold = new SpellInfo(SpellId.ConeOfCold, ElementType.Frost, "Cone Of Cold");
            coneOfCold.AimType = AimType.Direction;
            coneOfCold.IconPath = "Resource/Image/Icon/ConeOfCold.png";
            infoMap.Add(SpellId.ConeOfCold, coneOfCold);

            /* 고드름 Icicles */
            var icicles = new SpellInfo(SpellId.Icicles, ElementType.Frost, "Icicles");
            icicles.AimType = AimType.Direction;
            icicles.IconPath = "Resource/Image/Icon/Icicles.png";
            infoMap.Add(SpellId.Icicles, icicles);

            /* 얼음 가시 Ice Spike */
            var iceSpike = new SpellInfo(SpellId.IceSpike, ElementType.Frost, "Ice Spike");
            iceSpike.AimType = AimType.Direction;
            iceSpike.IconPath = "Resource/Image/Icon/IceSpike.png";
            infoMap.Add(SpellId.IceSpike, iceSpike);




            ////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////

            Debug.Assert(infoMap.Count == SpellIds.All.Count,
                "모든 주문 id에 대해, 설령 더미 객체를 넣게 되라도, spellInfo가 정의 되어있어야 합니다.");
        }
    }
}
