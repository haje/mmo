﻿
using System;
using System.Collections.Generic;
namespace HAJE.MMO.Spell
{
    public partial class SpellInfo
    {
        public SpellInfo(SpellId id, ElementType elementType, string name)
        {
            this.Id = id;
            this.Name = name;
            this.ElementType = elementType;
            this.AimType = AimType.Object;
            this.CastingTime = (Second)1.0f;
            this.Range = 20.0f;

            this.IconPath = "Resource/Image/Icon/ArcaneMissile.png";
        }

        public readonly SpellId Id;
        public readonly ElementType ElementType;
        public readonly string Name;
        public GainType GainType { get; private set; }
        public AimType AimType { get; private set; }
        public int PurchaseCost { get; private set; }
        public int PurchaseCount { get; private set; } 
        public int ManaCost { get; private set; }
        public int ComboPointCost { get; private set; }
        public int Damage { get; private set; }
        public float Range { get; private set; }
        public float Radius { get; private set; }
        public float Speed { get; private set; }
        public Second Cooldown { get; private set; }
        public Second CastingTime { get; private set; }
        public Second DelayTime { get; private set; }
        public Second UpdateTime { get; private set; }
        public string IconPath { get; private set; }
        public string SpellTooltip { get; private set; }

        private static Dictionary<SpellId, SpellInfo> infoMap = new Dictionary<SpellId, SpellInfo>();
        public static SpellInfo GetInfo(SpellId id)
        {
            return infoMap[id];
        }

        private static List<SpellId> basicSpellIdList = new List<SpellId>();
        public static IReadOnlyList<SpellId> BasicSpellIdList
        {
            get { return basicSpellIdList; }
        }

        private static ElementType[] elements;
        public static IReadOnlyList<ElementType> ElementTypes
        {
            get
            {
                return elements;
            }
        }

        static void Initialize()
        {
            var elements = Enum.GetValues(typeof(ElementType));
            SpellInfo.elements = new ElementType[elements.Length];
            int count = 0;
            foreach (var e in elements)
            {
                SpellInfo.elements[count++] = (ElementType)e;
            }
        }
    }
}
