﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace HAJE.MMO
{
    public delegate void LogHandler(string log, LogDispatcher dispatcher);

    public class LogDispatcher
    {
        int logline = 0;
        List<string> loglines;

        int bufferSize = 0;

        public bool DebugOutputEnable = false;
        public bool ConsoleOutputEnable = false;
        public string OwnerName;
        
        public LogDispatcher(string ownerName, int bufferSize)
        {
            this.OwnerName = ownerName;
            this.bufferSize = bufferSize;

            int count = bufferSize;
            loglines = new List<string>();
            while ( count > 0)
            {
                loglines.Add("");
                count--;
            }
        }

        public void Clear()
        {
            logline = 0;
            for (int i = 0; i < bufferSize; i++)
            {
                if (loglines[i].Length > 0)
                    loglines[i] = "";
            }
        }

        public void Write(Object obj)
        {
            Write(obj.ToString());
        }
        public void Write(string format, params object[] args)
        {
            string log;
            lock (this.loglines)
            {
                logline++;
                log = String.Format(format, args);
                if (logline == bufferSize)
                {
                    logline = 0;
                }
                loglines[logline] = log;

                if (DebugOutputEnable == true && ConsoleOutputEnable != true )
                {
                    Debug.WriteLine(log);
                }
                if (ConsoleOutputEnable == true)
                {
                    System.Console.WriteLine(log);
                }
                
                this.OnWriteLog(log, this);
            }

        }

        public int BufferSize
        {
            get
            {
                return bufferSize;
            }
            set
            {
                lock (this.loglines)
                {
                    //TODO: @whitecrow 저장 가능한 최대 버퍼 크기 변경
                    // loglines의 내용 변경
                    if (value == bufferSize)
                    {
                        return;
                    }
                    else if (value < bufferSize)
                    {
                        loglines = GetRecentLog(value);
                        loglines.Reverse();
                        logline = value-1;
                        bufferSize = value;
                    }
                    else
                    {
                        int count = value - bufferSize;
                        loglines = GetRecentLog(bufferSize);
                       
                        loglines.Reverse();

                        while (count > 0)
                        {
                            count--;
                            loglines.Add("");
                        }
                        logline = bufferSize-1;
                        bufferSize = value;
                    }
                }
            }
        }

        public List<string> GetRecentLog(int count)
        {
            lock (this.loglines)
            {
                if (count > bufferSize)
                {
                    count = bufferSize;
                }

                List<string> recentList = new List<string>();
                int copyingline = logline;
                while (count > 0)
                {
                    recentList.Add(loglines[copyingline]);
                    copyingline--;
                    count--;
                    if (copyingline < 0)
                    {
                        copyingline = bufferSize - 1;
                    }
                }
                return recentList;
            }
        }

        public event LogHandler OnWriteLog = delegate { };
    }
}
