﻿using System;
using System.IO;

namespace HAJE.MMO
{
    public class LogFileWriter : IDisposable
    {

        public LogFileWriter(string path, LogDispatcher dispatcher)
        {
            this.filepath = path;
            this.logdispatcher = dispatcher;
        }

        public void Start()
        {
            logdispatcher.OnWriteLog += HandleLog;
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(WhenProgramEnd);
            file = File.AppendText(filepath);
        }

        void HandleLog(string log, LogDispatcher dispatcher)
        {
            file.WriteLine( log );
        }

        public void End()
        {
            logdispatcher.OnWriteLog -= HandleLog;
            AppDomain.CurrentDomain.ProcessExit -= new EventHandler(WhenProgramEnd);
            file.Close();
        }

        public void WhenProgramEnd(object sender, EventArgs e )
        {
            End();
        }

        public void Dispose()
        {
            if (file != null)
            {
                End();
                file = null;
            }
        }
        string filepath;
        StreamWriter file;
        LogDispatcher logdispatcher;
    }
}
