﻿using OpenTK;

namespace HAJE.MMO
{
    /// <summary>
    /// 위치 정보를 (필드, 필드 안에서의 로컬 위치) 쌍으로 표현하는 객체
    /// FieldManager를 거치치 않고 계산하는 일이 없도록 불변객체로 설계 되었다.
    /// 필드 사이를 오가는 연산을 올바르게 하려면 반드시 FieldManager를 거쳐서 연산해야 한다.
    /// </summary>
    public struct PositionInField
    {
        public PositionInField(int fieldId, Vector2 position)
        {
            FieldId = fieldId;
            Position = new Vector3(position.X, 0, position.Y);
            IsGroundRelative = true;
        }

        public PositionInField(int fieldId, Vector3 position, bool isGroundRelative = false)
        {
            FieldId = fieldId;
            Position = position;
            IsGroundRelative = isGroundRelative;
        }

        public readonly int FieldId;
        public readonly Vector3 Position;
        public readonly bool IsGroundRelative;
    }
}
