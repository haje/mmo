﻿
namespace HAJE.MMO
{
    public class BuildInfo
    {
        public static readonly bool DeployBuild = false;

        public static readonly int Timestamp = 151119;
        public static readonly int Revision = 1989;

        public static readonly string BuildStamp;

        static BuildInfo()
        {
            BuildStamp = "Development";
            if (DeployBuild)
                BuildStamp = "Deploy";
            BuildStamp += string.Format(" (Rev. {0}  Build Date. {1})", Revision, Timestamp);
        }
    }
}
