﻿using OpenTK;
using OpenTK.Graphics;
using System;

namespace HAJE.MMO
{
    public static class RandomExtension
    {
        public static float NextFloat(this Random rd)
        {
            return (float)rd.NextDouble();
        }
        public static float NextFloat(this Random rd, float min, float max)
        {
            return min + (float)rd.NextDouble() * (max - min);
        }

        public static Second NextSecond(this Random rd)
        {
            return (Second)rd.NextFloat();
        }
        public static Second NextSecond(this Random rd, float min, float max)
        {
            return (Second)rd.NextFloat(min, max);
        }

        public static Radian NextRadian(this Random rd)
        {
            return new Radian(rd.NextFloat() * (float)Math.PI * 2);
        }
        public static Radian NextRadian(this Random rd, Radian min, Radian max)
        {
            return min + rd.NextRadian() * (max - min);
        }
        public static Degree NextDegree(this Random rd)
        {
            return new Degree(rd.NextFloat() * 360);
        }
        public static Degree NextDegree(this Random rd, Degree min, Degree max)
        {
            return min + rd.NextDegree() * (max - min);
        }
        
        public static Color4 RandomColor(this Random rd, Color4 center, float Rdiff, float Gdiff, float Bdiff, float Adiff)
        {
            float r = rd.NextFloat(-Rdiff, Rdiff);
            float g = rd.NextFloat(-Gdiff, Gdiff);
            float b = rd.NextFloat(-Bdiff, Bdiff);
            float a = rd.NextFloat(-Adiff, Adiff);

            r = Math.Min(1, Math.Max(0, r + center.R));
            g = Math.Min(1, Math.Max(0, g + center.G));
            b = Math.Min(1, Math.Max(0, b + center.B));
            a = Math.Min(1, Math.Max(0, a + center.A));

            return new Color4(r, g, b, a);
        }
        public static Vector3 GetPointInSphere(this Random rd, float maxRadius)
        {
            Radian theta = rd.NextRadian();
            Radian phi = rd.NextRadian();
            float radius = rd.NextFloat(0, maxRadius);

            return new Vector3(Radian.Sin(theta) * Radian.Cos(phi), Radian.Sin(theta) * Radian.Sin(phi), Radian.Cos(theta)) * radius;
        }
        public static Vector3 GetPointInSphericalShell(this Random rd, float minRadius, float maxRadius)
        {
            Radian theta = rd.NextRadian();
            Radian phi = rd.NextRadian();
            float radius = rd.NextFloat(minRadius, maxRadius);
            
            return new Vector3(Radian.Sin(theta) * Radian.Cos(phi), Radian.Sin(theta) * Radian.Sin(phi), Radian.Cos(theta)) * radius;
        }
        public static Vector2 GetUnitVector2(this Random rd)
        {
            Radian theta = rd.NextRadian();
            return new Vector2(Radian.Sin(theta), Radian.Cos(theta));
        }
        public static Vector3 GetUnitVector3(this Random rd)
        {
            Radian theta = rd.NextRadian();
            Radian phi = rd.NextRadian();
            return new Vector3(Radian.Sin(theta) * Radian.Cos(phi), Radian.Sin(theta) * Radian.Sin(phi), Radian.Cos(theta));
        }
        public static Vector3 GetPointInCone(this Random rd, Vector3 direction, Radian maxAngle)
        {
            Vector3 u = Vector3.Cross(direction, direction + Vector3.UnitX);
            if (u == Vector3.Zero)
                u = Vector3.Cross(direction, direction + Vector3.UnitY);
            Vector3 v = Vector3.Cross(direction, u);
            u.Normalize();
            v.Normalize();
            Radian theta = rd.NextRadian(new Radian(0), maxAngle);
            Radian phi = rd.NextRadian();

            return Radian.Sin(theta) * (Radian.Cos(phi) * u + Radian.Sin(phi) * v) + Radian.Cos(theta) * direction;
        }
        public static Vector3 GetPointInCone(this Random rd, Vector3 direction, Degree maxAngle)
        {
            return rd.GetPointInCone(direction, new Radian(maxAngle));
        }

        public static Vector2 GetPointInCircle(this Random rd, float maxRadius)
        {
            Radian theta = rd.NextRadian();
            float radius = (float)Math.Sqrt(rd.NextFloat()) * maxRadius; // uniform distribution
            return new Vector2(Radian.Cos(theta), Radian.Sin(theta)) * radius;
        }

    }
}
