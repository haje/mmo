﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HAJE.MMO.Server
{
    public class Configuration
    {
        public static Configuration Instance { get; private set; }
        public static void Load()
        {
            Instance = new Configuration(
                ConfigurationReader.Read<XmlType.Configuration>(
                    "ServerConfig.xml"
                )
            );
        }
        public static void LoadLocalServerConfig()
        {
            Instance = new Configuration(ServerMode.LocalServer);
        }

        private Configuration(ServerMode mode)
        {
            if (mode == Server.ServerMode.LocalServer)
            {
                ServerMode = Server.ServerMode.LocalServer;
                ServerKey = CryptoHelper.HashToBytes("localServerKey");
                BinaryKey = CryptoHelper.HashServerBinaries();
                MasterPort = 25252;
                LoginPort = 25253;
                AllocatorPort = 25254;
                AchievePort = 25255;
                FieldPort = 25256;
            }
        }

        public Configuration(XmlType.Configuration config)
        {
            this.ServerMode = config.serverMode;
            this.ServerKey = CryptoHelper.HashToBytes(config.serverKey);
            this.BinaryKey = CryptoHelper.HashServerBinaries();
            this.UnitTestEnabled = config.unitTestEnabled;
            this.MasterPort = config.MasterServer.defaultPort;
            this.LoginPort = config.LoginServer.defaultPort;
            this.LoginDB = new DBConfiguration(config.LoginServer.DataBase);
            this.AllocatorPort = config.AllocatorServer.defaultPort;
            this.AchievePort = config.AchieveServer.defaultPort;
            this.FieldPort = config.FieldServer.defaultPort;
        }

        public readonly ServerMode ServerMode;
        public readonly IReadOnlyList<byte> ServerKey;
        public readonly IReadOnlyList<byte> BinaryKey;
        public readonly bool UnitTestEnabled;
        public readonly int MasterPort;
        public readonly int LoginPort;
        public readonly int AllocatorPort;
        public readonly int AchievePort;
        public readonly int FieldPort;
        public readonly DBConfiguration LoginDB;
    }

    public class DBConfiguration
    {
        public DBConfiguration(XmlType.DBConfiguration config)
        {
            this.Ip = config.ip;
            this.Name = config.dbName;
            this.Password = config.password;
            this.Uid = config.uid;
        }

        public readonly string Ip;
        public readonly string Name;
        public readonly string Uid;
        public readonly string Password;
    }

    public enum ServerMode
    {
        Console,
        LocalServer,
        DevServer,
        LiveMaster,
        LiveLogin,
    }
}

namespace HAJE.MMO.Server.XmlType
{
    public class Configuration
    {
        [XmlAttribute]
        public ServerMode serverMode;

        [XmlAttribute]
        public string serverKey;

        [XmlAttribute]
        public bool unitTestEnabled;

        public MasterServerConfiguration MasterServer = new MasterServerConfiguration();
        public LoginServerConfiguration LoginServer = new LoginServerConfiguration();
        public AllocatorServerConfiguration AllocatorServer = new AllocatorServerConfiguration();
        public AchieveServerConfiguration AchieveServer = new AchieveServerConfiguration();
        public FieldServerConfiguration FieldServer = new FieldServerConfiguration();
    }

    public class DBConfiguration
    {
        [XmlAttribute]
        public string ip;

        [XmlAttribute]
        public string dbName;

        [XmlAttribute]
        public string uid;

        [XmlAttribute]
        public string password;
    };

    public class MasterServerConfiguration
    {
        [XmlAttribute]
        public int defaultPort = 25252;
    }

    public class LoginServerConfiguration
    {
        [XmlAttribute]
        public int defaultPort = 25253;

        public DBConfiguration DataBase;
    };

    public class AllocatorServerConfiguration
    {
        [XmlAttribute]
        public int defaultPort = 25254;

        public DBConfiguration DataBase;
    }

    public class AchieveServerConfiguration
    {
        [XmlAttribute]
        public int defaultPort = 25255;
    };

    public class FieldServerConfiguration
    {
        [XmlAttribute]
        public int defaultPort = 25256;
    };
}