﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Server.Console
{
    public static class PromptChangeHelper
    {
        public static void ToServer(Prompt rootPrompt)
        {
            if (rootPrompt.Child != null)
                rootPrompt.Child.Stop();
        }

        public static void ToMaster(Prompt rootPrompt, Master.MasterServer server)
        {
            var factory = new Master.MasterConsoleFactory(rootPrompt, server);
            var master = factory.Create();
        }

        public static void ToLogin(Prompt rootPrompt, Login.LoginServer server)
        {
            var factory = new Login.LoginConsoleFactory(rootPrompt, server);
            var login = factory.Create();
        }

        public static void ToAchieve(Prompt rootPrompt, Achieve.AchieveServer server)
        {
            var factory = new Achieve.AchieveConsoleFactory(rootPrompt, server);
            var achieve = factory.Create();
        }

        public static void ToField(Prompt rootPrompt, Field.FieldServer server)
        {
            var factory = new Field.FieldConsoleFactory(rootPrompt, server);
            var field = factory.Create();
        }
    }
}
