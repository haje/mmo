﻿
namespace HAJE.MMO.Server.Console
{
    public abstract class Command
    {
        protected Command(string keyword, string description)
        {
            this.Keyword = keyword.ToLower();
            this.Description = description;
        }

        public bool Execute(string[] commandTokens)
        {
            if (commandTokens[0] != Keyword)
                return false;

            somethingWritten = false;
            bool stopPropagation = ExecuteInternal(commandTokens);
            if (stopPropagation && somethingWritten)
            {
                System.Console.WriteLine();
            }
            return stopPropagation;
        }

        protected void WriteConsole(string format, params object[] args)
        {
            format.Replace("\n", "\n  ");
            System.Console.WriteLine("{0}: {1}", Keyword, string.Format(format, args));
            somethingWritten = true;
        }

        protected abstract bool ExecuteInternal(string[] commandTokens);

        private bool somethingWritten = false;

        public readonly string Keyword;
        public readonly string Description;
    }
}
