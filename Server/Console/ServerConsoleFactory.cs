﻿using HAJE.MMO.Server.Console.CommandImpl;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Console
{
    public class ServerConsoleFactory : ConsoleFactory
    {
        public ServerConsoleFactory()
            : base("server", null)
        {
        }

        protected override void BuildCommand(Prompt prompt, List<Command> cmds)
        {
            var ctx = ServerContext.Instance;
            cmds.Add(new Start(prompt, ctx));
            cmds.Add(new CommandImpl.Server(prompt));
            cmds.Add(new CommandImpl.Master(ctx));
            cmds.Add(new CommandImpl.Login(ctx));
            cmds.Add(new CommandImpl.Achieve(ctx));
            cmds.Add(new CommandImpl.Field(ctx));
            cmds.Add(new Help(prompt));
            cmds.Add(new Exit());
            base.BuildCommand(prompt, cmds);
        }
    }
}
