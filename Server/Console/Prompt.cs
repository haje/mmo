﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Server.Console
{
    public class Prompt
    {
        public Prompt(string name, Prompt parent)
        {
            this.Name = name;
            this.parent = parent;
            if (this.parent != null)
                this.parent.child = this;
            this.IsRunning = false;
        }

        public void InitCommandList(List<Command> commandList)
        {
            Debug.Assert(this.commandList == null);
            this.commandList = commandList;
        }

        public void Run()
        {
            if (parent != null)
                parent.child = this;

            while (!isStopped)
            {
                IsRunning = true;
                RunSingleCommand();
            }
            IsRunning = false;
        }

        void RunSingleCommand()
        {
            if (child != null)
                child.RunSingleCommand();
            else
            {
                System.Console.Write(Name);
                System.Console.Write("> ");
                string line = System.Console.ReadLine();
                ExecuteCommand(line);
            }
        }

        public void ExecuteCommand(string line)
        {
            string[] tokens = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            tokens[0] = tokens[0].ToLower();
            if (tokens.Length >= 1)
            {
                ExecuteCommand(tokens);
            }
        }

        private void ExecuteCommand(string[] commandTokens)
        {
            foreach (var cmd in commandList)
            {
                if (cmd.Execute(commandTokens))
                {
                    return;
                }
            }

            if (parent != null)
                parent.ExecuteCommand(commandTokens);
            else
            {
                System.Console.WriteLine("{0}: '{1}' 알 수 없는 명령입니다.", Name, commandTokens[0]);
                System.Console.WriteLine("help를 통해 사용 가능한 명령을 볼 수 있습니다.");
                System.Console.WriteLine();
            }
        }

        public void Stop()
        {
            isStopped = true;
            parent.child = null;
        }

        public readonly string Name;

        public IReadOnlyList<Command> CommandList
        {
            get
            {
                return commandList.AsReadOnly();
            }
        }

        public bool IsRootPrompt
        {
            get
            {
                return parent == null;
            }
        }

        public Prompt Parent
        {
            get
            {
                return parent;
            }
        }

        public Prompt Child
        {
            get
            {
                return child;
            }
        }

        public bool IsRunning { private set; get; }

        private Prompt parent = null;
        private Prompt child = null;
        private bool isStopped = false;
        private List<Command> commandList = null;
    }
}
