﻿
namespace HAJE.MMO.Server.Console
{
    public interface ILogHolder
    {
        LogDispatcher Log
        {
            get;
        }
    }
}
