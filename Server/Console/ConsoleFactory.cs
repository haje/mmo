﻿using HAJE.MMO.Server.Console.CommandImpl;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Console
{
    public class ConsoleFactory
    {
        protected ConsoleFactory(string consoleName, Prompt parent)
        {
            this.name = consoleName;
            this.parent = parent;
        }

        public Prompt Create()
        {
            Prompt prompt = new Prompt(name, parent);
            List<Command> cmds = new List<Command>();
            BuildCommand(prompt, cmds);
            prompt.InitCommandList(cmds);
            return prompt;
        }

        protected virtual void BuildCommand(Prompt prompt, List<Command> cmds)
        {
        }

        private readonly string name;
        private readonly Prompt parent;
    }
}
