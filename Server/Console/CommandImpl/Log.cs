﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Log : Command
    {
        public Log(ServerContext ctx, ILogHolder logHolder)
            : base("log", "로그를 콘솔 화면에 출력합니다.")
        {
            this.ctx = ctx;
            this.logHolder = logHolder;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                if (commandTokens[1] == "fetch")
                {
                    int count = logHolder.Log.BufferSize;
                    if (commandTokens.Length == 3)
                    {
                        int.TryParse(commandTokens[2], out count);
                    }
                    WriteLogToConsole(count);
                }
                else
                {
                    WriteConsole("잘못된 명령 형식입니다.");
                }
            }
            else
            {
                WriteConsole("로그 출력을 시작합니다. 중단하려면 아무 키나 누르세요.");
                logHolder.Log.ConsoleOutputEnable = true;
                System.Console.ReadKey();
                logHolder.Log.ConsoleOutputEnable = false;
            }
            return true;
        }

        void WriteLogToConsole(int count)
        {
            var logs = logHolder.Log.GetRecentLog(count);
            WriteConsole("최근 " + count + "개의 로그 출력");
        }

        ILogHolder logHolder;
        ServerContext ctx;
    }
}
