﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Login : Command
    {
        public Login(ServerContext ctx)
            : base("login", "로그인 서버의 콘솔로 이동합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (ctx.LoginServer == null)
            {
                WriteConsole("구동중인 로그인 서버가 없습니다.");
            }
            else
            {
                PromptChangeHelper.ToLogin(ctx.RootPrompt, ctx.LoginServer);
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
