﻿using System;

namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Exit : Command
    {
        public Exit()
            : base("exit", "서버를 종료합니다.")
        {
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            ServerContext.Instance.Dispose();
            Environment.Exit(0);
            return true;
        }
    }
}
