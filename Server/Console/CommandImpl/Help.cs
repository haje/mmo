﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Help : Command
    {
        public Help(Prompt owner)
            : base("help", "현재 콘솔에서 사용 가능한 명령어를 출력합니다.")
        {
            this.owner = owner;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            WriteHelpList(owner);
            return true;
        }

        void WriteHelpList(Prompt prompt)
        {
            var cmdList = prompt.CommandList;
            WriteConsole("{0}의 명령어 목록", prompt.Name);
            foreach (var cmd in cmdList)
            {
                System.Console.WriteLine("  {0}: {1}", cmd.Keyword, cmd.Description);
            }

            if (prompt.Child != null)
            {
                System.Console.WriteLine();
                WriteHelpList(prompt.Child);
            }
        }

        private readonly Prompt owner;
    }
}
