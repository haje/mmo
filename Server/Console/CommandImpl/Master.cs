﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Master : Command
    {
        public Master(ServerContext ctx)
            : base("master", "마스터 서버의 콘솔로 이동합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (ctx.MasterServer == null)
            {
                WriteConsole("구동중인 마스터 서버가 없습니다.");
            }
            else
            {
                PromptChangeHelper.ToMaster(ctx.RootPrompt, ctx.MasterServer);
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
