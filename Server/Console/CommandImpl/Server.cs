﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Server : Command
    {
        public Server(Prompt prompt)
            : base("server", "서버 기본 콘솔로 이동합니다.")
        {
            this.prompt = prompt;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            PromptChangeHelper.ToServer(prompt);
            return true;
        }

        Prompt prompt;
    }
}
