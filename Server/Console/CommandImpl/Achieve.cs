﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Achieve : Command
    {
        public Achieve(ServerContext ctx)
            : base("achieve", "업적 서버의 콘솔로 이동합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (ctx.AchieveServer == null)
            {
                WriteConsole("구동중인 업적 서버가 없습니다.");
            }
            else
            {
                PromptChangeHelper.ToAchieve(ctx.RootPrompt, ctx.AchieveServer);
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
