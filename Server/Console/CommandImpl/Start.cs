﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    class Start : Command
    {
        public Start(Prompt owner, ServerContext ctx)
            : base("start", "기능 서버를 새로 시작합니다.")
        {
            this.owner = owner;
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                string serverType = commandTokens[1];
                if (serverType == "master")
                {
                    if (ctx.MasterServer != null)
                    {
                        WriteConsole("이미 마스터 서버가 로컬에 실행중입니다.");
                    }
                    else
                    {
                        var master = new MMO.Server.Master.MasterServer();
                        master.Run();
                        ctx.MasterServer = master;

                        PromptChangeHelper.ToMaster(owner, ctx.MasterServer);
                        WriteConsole("마스터 서버를 시작했습니다.");
                    }
                }
                else if (serverType == "login")
                {
                    if (ctx.LoginServer != null)
                    {
                        WriteConsole("이미 로그인 서버가 로컬에 실행중입니다.");
                    }
                    else if (ctx.MasterServer == null)
                    {
                        WriteConsole("마스터 서버를 찾을 수 없습니다.");
                    }
                    else
                    {
                        var login = new MMO.Server.Login.LoginServer("localhost",ctx.MasterServer.Port);
                        login.Run();
                        ctx.LoginServer = login;

                        PromptChangeHelper.ToLogin(owner, ctx.LoginServer);
                        WriteConsole("로그인 서버를 시작했습니다.");
                    }
                }
                else if (serverType == "achieve")
                {
                    if (ctx.AchieveServer != null)
                    {
                        WriteConsole("이미 업적 서버가 로컬에 실행중입니다.");
                    }
                    else if (ctx.MasterServer == null)
                    {
                        WriteConsole("마스터 서버를 찾을 수 없습니다.");
                    }
                    else
                    {
                        var achieve = new MMO.Server.Achieve.AchieveServer("localhost",ctx.MasterServer.Port);
                        achieve.Run();
                        ctx.AchieveServer = achieve;

                        PromptChangeHelper.ToAchieve(owner, ctx.AchieveServer);
                        WriteConsole("업적 서버를 시작했습니다.");
                    }
                }
                else if (serverType == "field")
                {
                    if (ctx.FieldServer != null)
                    {
                        WriteConsole("이미 필드 서버가 로컬에 실행중입니다.");
                    }
                    else if (ctx.MasterServer == null)
                    {
                        WriteConsole("마스터 서버를 찾을 수 없습니다.");
                    }
                    else
                    {
                        var field = new MMO.Server.Field.FieldServer("localhost", ctx.MasterServer.Port);
                        field.Run();
                        ctx.FieldServer = field;

                        PromptChangeHelper.ToField(owner, ctx.FieldServer);
                        WriteConsole("필드 서버를 시작했습니다.");
                    }
                }
                else
                {
                    WriteConsole("잘못된 인자입니다.\n{0}", availableArgs);
                }
            }
            else
            {
                WriteConsole("최소 하나의 인자를 넘겨야 합니다.\n{0}", availableArgs);
            }
            return true;
        }

        private readonly string availableArgs = "  사용 가능한 인자: master";

        private readonly Prompt owner;
        private readonly ServerContext ctx;
    }
}
