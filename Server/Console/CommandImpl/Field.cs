﻿
namespace HAJE.MMO.Server.Console.CommandImpl
{
    public class Field : Command
    {
        public Field(ServerContext ctx)
            : base("field", "필드 서버의 콘솔로 이동합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (ctx.FieldServer == null)
            {
                WriteConsole("구동중인 필드 서버가 없습니다.");
            }
            else
            {
                PromptChangeHelper.ToField(ctx.RootPrompt, ctx.FieldServer);
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
