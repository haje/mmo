﻿using System;

namespace HAJE.MMO.Server
{
    public class ServerContext : IDisposable
    {
        public ServerContext()
        {
            ErrorLog = new LogDispatcher("Error", 10);
            ErrorLog.ConsoleOutputEnable = true;
            ErrorLog.DebugOutputEnable = true;
            CommandLog = new LogDispatcher("Error", 5);
            CommandLog.ConsoleOutputEnable = true;
            CommandLog.DebugOutputEnable = true;
        }

        public static ServerContext Instance = new ServerContext();

        public Console.Prompt RootPrompt = null;
        public Master.MasterServer MasterServer = null;
        public Login.LoginServer LoginServer = null;
        public Achieve.AchieveServer AchieveServer = null;
        public Field.FieldServer FieldServer = null;

        public LogDispatcher ErrorLog;
        public LogDispatcher CommandLog;

        public void Dispose()
        {
            if (MasterServer != null) MasterServer.Dispose();
            if (LoginServer != null) LoginServer.Dispose();
            if (AchieveServer != null) AchieveServer.Dispose();
            if (FieldServer != null) FieldServer.Dispose();
        }
    }
}
