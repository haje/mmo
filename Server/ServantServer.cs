﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server
{
    public abstract class ServantServer : Console.ILogHolder, System.IDisposable
    {
        readonly int LogDispatcherBufferLength = 256;
        public ServantServer(string logName, string masterIp, int masterPort)
        {
            this.logName = logName;
            this.masterIp = masterIp;
            this.masterPort = masterPort;
            Log = new LogDispatcher(logName, LogDispatcherBufferLength);
        }

        public abstract void Run();
        public abstract void Stop();
        public void Dispose() 
        {
            Stop();
        }

        protected void ConnectMaster()
        {
            if (masterSocketClinet != null) masterSocketClinet.Dispose();
            masterSocketClinet = new SocketClient(logName, masterIp, masterPort, Log);
            masterSocketClinet.ConnectServer();
            masterSocketClinet.SetReceiveHandler(PacketType.ServerRegisterResponse, MasterRegisterCallback);
        }
        protected abstract void MasterRegisterCallback(PacketReader reader);
            
        public LogDispatcher Log { get; private set; }

        protected string logName;
        protected string masterIp;
        protected int masterPort;
        protected SocketClient masterSocketClinet = null;
    }
}
