﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server
{
    class SocketHelper
    {
        public static Socket CreateAcceptor(int port, int backlog, AsyncCallback handler)
        {
            Socket sock = new Socket(
                AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp
            );
            try
            {
                sock.Bind(new IPEndPoint(IPAddress.Any, port));
            }
            catch
            {
                sock.Bind(new IPEndPoint(IPAddress.Any, 0));
            }
            sock.Listen(backlog);
            sock.BeginAccept(handler, sock);
            return sock;
        }

        public static ServerRegisterRequest CreateMasterRegisterPacket(ServerType serverType, int port)
        {
            ServerRegisterRequest packet = new ServerRegisterRequest(
                    CryptoHelper.IListToString(Configuration.Instance.ServerKey, Encoding.UTF8),
                    CryptoHelper.IListToString(Configuration.Instance.BinaryKey, Encoding.UTF8),
                    serverType, port);
            return packet;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1";
        }
    }
}
