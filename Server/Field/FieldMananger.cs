﻿using System.Collections.Generic;
using System.Diagnostics;
using HAJE.MMO.Server.Field.Base;

namespace HAJE.MMO.Server.Field
{
    public class FieldMananger //새 필드의 할당, 관리
    {
        public FieldMananger()
        {
            fieldDict = new Dictionary<int, Base.Field>();
        }

        public void AllocNewField()
        {

        }

        public Base.Field GetField(int fieldId)
        {
            if (fieldDict.ContainsKey(fieldId))
                return fieldDict[fieldId];
            return null;
        }

        Dictionary<int, Base.Field> fieldDict = null;
    }
}
