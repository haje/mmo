﻿using HAJE.MMO.Server.Console;
using System.Collections.Generic;
using HAJE.MMO.Server.Field.CommandImpl;

namespace HAJE.MMO.Server.Field
{
    public class FieldConsoleFactory : Console.ConsoleFactory
    {
        public FieldConsoleFactory(Prompt root, FieldServer server)
            : base("field", root)
        {
            this.server = server;
        }

        protected override void BuildCommand(Prompt prompt, List<Command> cmds)
        {
            var ctx = ServerContext.Instance;
            cmds.Add(new Create(ctx));
            cmds.Add(new Find(ctx));
            cmds.Add(new Kill(ctx));
            cmds.Add(new Teleport(ctx));
            cmds.Add(new NewField(ctx));
            cmds.Add(new Console.CommandImpl.Log(ctx, ctx.FieldServer));
            base.BuildCommand(prompt, cmds);
        }

        private readonly FieldServer server;
    }
}
