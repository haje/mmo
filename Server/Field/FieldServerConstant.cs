﻿
namespace HAJE.MMO.Server.Field
{
    public static class FieldServerConstant
    {
        public static readonly int MaxObjectPerFieldNode = 20;
        public static readonly int MinObjectPerFieldNode = 10;
    }
}