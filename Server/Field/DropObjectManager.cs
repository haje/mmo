﻿using System.Collections.Generic;
using OpenTK;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Server.Field.Base;

namespace HAJE.MMO.Server.Field
{
    public class DropObjectManager
    {
        public DropObjectManager(FieldLogic owner)
        {
            this.owner = owner;
            networkContext = owner.NetworkContext;
            scheduler = owner.FieldScheduler;
            effectManager = owner.EffectManager;
        }

        public void Update(Second deltaTime)
        {
            currentDeltaTime += deltaTime;
            if (currentDeltaTime > UpdateInterval)
            {
                lock (dropObjectDict)
                {
                    foreach (var obj in removeObjectList)
                    {
                        obj.FieldObject.RemoveFromField();
                        dropObjectDict.Remove(obj.Id);
                    }
                    removeObjectList.Clear();

                    foreach (KeyValuePair<int, DropObject> pair in dropObjectDict)
                    {
                        DropObject dropObject = pair.Value;
                        if (dropObject.FieldObject == null) continue;
                        dropObject.Update(deltaTime, owner.LogDispatcher);
                    }
                }

                currentDeltaTime = Second.Zero;
            }
        }

        public void CreateDropObject(FieldBase field, Vector2 position)
        {
            var dropFieldObject = new FieldObject(position, 0.0f, field, FieldObjectType.Drop);
            var dropObject = new DropObject(++lastDropObjectId, dropFieldObject);
            dropObject.DropGainEvent += GainDropObject;
            field.AddObject(dropFieldObject);

            dropObject.Effect = effectManager.CreateEffect(EffectType.DropElement, 
                new PositionInField(field.Id, dropFieldObject.GetPos3()));
            dropObjectDict.Add(dropObject.Id, dropObject);
        }

        public void GainDropObject(Player player, DropObject dropObject)
        {
            dropObject.DropGainEvent -= GainDropObject;

            Second duration = (Second)1.0f;
            effectManager.MoveEffect(dropObject.Effect.Id, player, AttachmentPoint.MagicWand, duration);
            
            scheduler.ScheduleOnce(delegate() {
                removeObjectList.Add(dropObject);

                effectManager.RemoveEffect(dropObject.Effect.Id, true);
                dropObject.Effect = null;

                var statusSync = new StatusSync();
                statusSync.CharacterId = player.Id;
                statusSync.StatusType = StatusType.ElementPoint;
                statusSync.ElementType = dropObject.ElementType;
                statusSync.Value = dropObject.Point;
                player.SendMessage(statusSync);
            }, duration);
        }

        int lastDropObjectId = 0;

        FieldLogic owner;
        FieldNetworkContext networkContext;
        Scheduler scheduler;
        EffectManager effectManager;

        Dictionary<int, DropObject> dropObjectDict = new Dictionary<int, DropObject>();
        List<DropObject> removeObjectList = new List<DropObject>();

        Second currentDeltaTime = Second.Zero;
        readonly Second UpdateInterval = (Second)0.5f;
    }
}
