﻿using System;
using System.Collections.Generic;
using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Field
{
    public class FieldServer : ServantServer
    {
        public FieldServer(string masterIp, int masterPort)
            : base("FieldServer", masterIp, masterPort)
        {
            FieldServerLogic = new FieldLogic(this.Log);
        }

        public override void Run()
        {
            try
            {
                PlayerAcceptServer = new SslStreamServer(logName,
                    Configuration.Instance.FieldPort, SslReceiveCallback, Log);
                PlayerAcceptServer.StartServer();

                ConnectMaster();
                masterSocketClinet.SendAsync(SocketHelper.CreateMasterRegisterPacket(ServerType.Field, Port));
            }
            catch (Exception e)
            {
                Log.Write("failed to run server");
                Log.Write(e.ToString());
                Stop();
            }

            FieldServerLogic.Begin();
        }

        public override void Stop()
        {
            PlayerAcceptServer = null;
            if (masterSocketClinet != null) masterSocketClinet.Dispose();

            FieldServerLogic.End();
        }

        protected override void MasterRegisterCallback(PacketReader reader)
        {
            using (ServerRegisterResponse response = new ServerRegisterResponse())
            {
                response.Deserialize(reader);
                if (response.ResponseCode == ResponseCode.Success)
                {
                    if (response.isServerInfoExist)
                    {
                        loginSocketClient = new SocketClient("LoginFieldClient",
                            response.ServerIp, response.ServerPort, Log);
                        loginSocketClient.ConnectServer();
                        loginSocketClient.SetReceiveHandler(PacketType.AuthWithToken, FieldTokenCallback);
                        loginSocketClient.Send(SocketHelper.CreateMasterRegisterPacket(ServerType.Field, Port));
                    }
                }
            }
        }

        private void FieldTokenCallback(PacketReader reader)
        {
            using (var packet = new AuthWithToken())
            {
                packet.Deserialize(reader);
                if (AuthenticatedTokenDic.ContainsKey(packet.Token))
                {
                    AuthenticatedTokenDic[packet.Token] = packet.Id;
                    //TODO : dic to redis?
                }
                else
                {
                    AuthenticatedTokenDic.Add(packet.Token, packet.Id);
                }
               
            }
        }

        private void SslReceiveCallback(PacketReader reader, SslStreamConnection connection)
        {
            PacketType packetType = PacketBase.ReadPacketType(reader);
            switch (packetType)
            {
                case PacketType.AuthWithToken:
                    using (var packet = new AuthWithToken())
                    {
                        packet.Deserialize(reader);
                        if (AuthenticatedTokenDic.ContainsKey(packet.Token))
                        {
                            //Authenticate Success!
                            FieldServerLogic.PlayerManager.CreatePlayer(connection, AuthenticatedTokenDic[packet.Token], packet.Token);
                        }
                        else
                        {
                            //Authenticate Failed!
                            Log.Write("Clinet Authenticate Failed! : Wrong Token - ({0},{1})",packet.Id,packet.Token);
                            connection.Dispose();
                        }
                    }
                    break;
                default:
                    Log.Write("Clinet Must Authenticate First Before Send OtherPacket!");
                    connection.Dispose();
                    break;
            }
        }

        public int Port { get { return PlayerAcceptServer.Port; } }

        SocketClient loginSocketClient = null;
        SslStreamServer PlayerAcceptServer = null;

        Dictionary<string, int> AuthenticatedTokenDic = new Dictionary<string, int>();
        public FieldLogic FieldServerLogic { get; private set; }

    }
}
