﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Base;
using HAJE.MMO.Server.Field.Object;
using OpenTK;

namespace HAJE.MMO.Server.Field
{
    public class FieldManager
    {
        public FieldManager(FieldLogic owner)
        {
            this.owner = owner;
            fieldDict = new Dictionary<int, Base.Field>();
            AllocInitialField(); //
        }

        int GetRandomTerrainType()
        {
            return 1; // TODO : terrianType 갯수를 읽고 너무 중복되지 않도록 랜덤 셀렉
        }
        
        void AllocInitialField()
        {
            if (rootField != null)
                return;

            rootField = new Base.Field(FieldIdStart, GetRandomTerrainType());
            fieldDict.Add(FieldIdStart, rootField);
            FieldIdStart++; //필드 아이디는 1씩 늘리면서 생성
        }

        public void AllocNewField(FieldBase field, FieldConnectDirection direction)
        {
            if (field.Neighbor.ContainsKey(direction))
                return; //이미 해당 방향에 필드가 존재함

            Base.Field newField = new Base.Field(FieldIdStart, GetRandomTerrainType());
            field.SetNeighbor(newField, direction); //이웃 필드 등록
            fieldDict.Add(FieldIdStart, newField);
            FieldIdStart++; //필드 아이디는 1씩 늘리면서 생성

            //유저에게 변경사항 알림
            foreach (var player in owner.PlayerManager.PlayerDict)
            {
                if (player.Value.IsDummy)
                    continue;

                foreach (var neighborField in newField.Neighbor)
                {
                    owner.SendFieldInfoToPlayer(neighborField.Value, player.Value.NetConnection);
                }
                owner.SendFieldInfoToPlayer(newField, player.Value.NetConnection);
            }
        }

        public void AllocNewField(int fieldId, FieldConnectDirection direction)
        {
            if (fieldDict.ContainsKey(fieldId))
                AllocNewField(fieldDict[fieldId], direction);
            else
                throw new Exception("존재하지 않는 Field Id 입니다.");
        }

        public Base.Field FindField(int id)
        {
            if (fieldDict.ContainsKey(id))
                return fieldDict[id];
            else
                return null;
        }

        public void AllocPlayerToField(Player player, Vector2 pos)
        {
            //TODO : Player를 시작하기 좋은 위치로 ~
            Base.Field targetField = null;
            foreach(var field in fieldDict.Values)
            {
                if (targetField == null || targetField.PlayerCount > field.PlayerCount)
                    targetField = field;
                //현재 가장 플레이어 수가 적은 필드에 넣는다.
            }
        
            Debug.Assert(targetField != null);
            targetField.AllocPlayerFieldObject(player, pos);
        }

        public PositionInField MovePosition(PositionInField source, Vector3 direction, float length)
        {
            Vector3 dest = source.Position + direction * length;

            FieldConnectDirection newFieldDirection = FieldConnectDirection.MAX;
            float xOfs = 0;
            float yOfs = 0;
            if (dest.Y > FieldBase.FieldSize)
            {
                if (dest.X < FieldBase.FieldSize / 2)
                {
                    newFieldDirection = FieldConnectDirection.UpperLeft;
                    xOfs = -FieldBase.FieldSize / 2;
                    yOfs = FieldBase.FieldSize;
                }
                else
                {
                    newFieldDirection = FieldConnectDirection.UpperRight;
                    xOfs = FieldBase.FieldSize / 2;
                    yOfs = FieldBase.FieldSize;
                }
            }
            else if (dest.Y < 0)
            {
                if (dest.X < FieldBase.FieldSize / 2)
                {
                    newFieldDirection = FieldConnectDirection.LowerLeft;
                    xOfs = -FieldBase.FieldSize / 2;
                    yOfs = -FieldBase.FieldSize;
                }
                else
                {
                    newFieldDirection = FieldConnectDirection.LowerRight;
                    xOfs = FieldBase.FieldSize / 2;
                    yOfs = -FieldBase.FieldSize;
                }
            }
            else
            {
                if (dest.X < 0)
                {
                    newFieldDirection = FieldConnectDirection.Left;
                    xOfs = -FieldBase.FieldSize;
                }
                else if (dest.X > FieldBase.FieldSize)
                {
                    newFieldDirection = FieldConnectDirection.Right;
                    xOfs = FieldBase.FieldSize;
                }
            }


            Debug.Assert(fieldDict.ContainsKey(source.FieldId));
            FieldBase currentField = fieldDict[source.FieldId];
            int newFieldId = source.FieldId;
            if (newFieldDirection != FieldConnectDirection.MAX) //dest가 현재 필드를 벗어남
            {
                currentField = currentField.GetNeighbor(newFieldDirection);
                if (currentField != null)
                {
                    newFieldId = currentField.Id;
                    dest += new Vector3(xOfs, 0, yOfs);
                }
                else
                {
                    //  단순 clamp가 아니라 field 위 적당한 위치가 되도록 length를 줄여야
                    dest = Vector3.Clamp(dest,
                        new Vector3(0, float.MinValue, 0),
                        new Vector3(FieldBase.FieldSize, float.MaxValue, FieldBase.FieldSize));
                }
            }
            
            return new PositionInField(newFieldId, dest, source.IsGroundRelative);
        }

        FieldLogic owner;

        Base.Field rootField = null;
        Dictionary<int, Base.Field> fieldDict = null;
        int FieldIdStart = 1;
    }
}
