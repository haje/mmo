﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using HAJE.MMO.Terrain;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;

namespace HAJE.MMO.Server.Field.Base
{
    public abstract class FieldBase
    {
        public const int FieldSize = 500;

        public FieldBase(int id, int terrainType)
        {
            this.Id = id;
            this.TerrainType = terrainType;
            
            using (Bitmap input = new Bitmap(TerrainPreprocessor.Path(terrainType)))
            {
                HeightMap = TerrainPreprocessor.Process(input, terrainType);
            }

            Neighbor = new Dictionary<FieldConnectDirection, FieldBase>();
            Initialize(terrainType); //C#은 derived class 멤버 초기화 이후 base constructor를 부른다.

            random = new Random(DateTime.Now.Millisecond);
        }

        protected abstract void Initialize(int terrainType);

        public abstract void SetNeighbor(FieldBase newNeighbor, FieldConnectDirection direction);
        public abstract FieldBase GetNeighbor(FieldConnectDirection direction);
        public abstract void AddObject(FieldObject newObj);

        public virtual void AddObject(FieldObject[] newObj)
        {
            for (int iter = 0; iter < newObj.Length; iter++)
            {
                AddObject(newObj[iter]);
            }
        }

        public virtual void AddObject(List<FieldObject> newObj)
        {
            for (int iter = 0; iter < newObj.Count; iter++)
            {
                AddObject(newObj[iter]);
            }
        }

        public abstract void RemoveObject(FieldObject newObj);
        public abstract void Query(Vector2 position, float radius, FieldObjectType objType, List<FieldObject> outputList); //Insert graspsed object to outputList
        public abstract void Query(Vector2 position, Rect area, FieldObjectType objType, List<FieldObject> outputList);
        public abstract void Query(FieldObject obj, float radius, FieldObjectType objType, List<FieldObject> outputList);
        public abstract void Query(FieldObject obj, Rect area, FieldObjectType objType, List<FieldObject> outputList);
        protected abstract void Query(FieldNode node, Vector2 position, float radius, FieldObjectType objType, List<FieldObject> outputList);

        protected abstract FieldNode SearchNode(Vector2 position);
        protected virtual FieldNode SearchNode(FieldObject tarObj)
        {
            return SearchNode(tarObj.Position);
        }

        public void WriteLog(LogDispatcher logDispatcher)
        {
            logDispatcher.Write("Field Id: {0}, TerrainType: {1}", Id, TerrainType);
            rootNode.WriteLog(logDispatcher);
        }

        public int TerrainType { get; private set; }
        public int Id { get; private set; }
        public Heightmap HeightMap { get; private set; }

        public Dictionary<FieldConnectDirection, FieldBase> Neighbor; 
        protected FieldNode rootNode;

        protected Random random;
    }
}
