﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Field.Base
{

    public class FieldNode : IDisposable
    {
        public FieldNode(FieldBase ownerField ,FieldNode parent, Rect area)
        {
            this.parent = parent; //parent가 null일 경우 root
            this.Area = area;
            this.children = null;
            ObjectCount = 0;

            OwnerField = ownerField;
            ownedObject = new List<FieldObject>(); //TODO : FIX THIS - Get Ref from manager 
                                                   //그냥 FieldNode 자체를 Recycle하는 방법도?
        }

        public FieldNode Search(Vector2 position)
        {
            if (children == null)
                return this;

            return children[(int)GetDivisionDirection(position)].Search(position);
        }

        public void AddInitialObjects(IReadOnlyList<FieldObject> objects)
        {
            //Initialize 시에 한꺼번에 많은 오브젝트를 받고 후에 Divide한다.
            ownedObject.AddRange(objects);
            ObjectCount = ownedObject.Count;
            DivideCascade();
        }

        private void DivideCascade()
        {
            //Initialize때만 불려야 함
            if (children == null &&
                ObjectCount > MaxObjectCount)
            {
                Divide();
                foreach (var Child in children)
                {
                    Child.DivideCascade();
                }
            }
        }

        public void AddObject(FieldObject newObj) //TODO : 구조 다시 생각해보기
        {
            Debug.Assert(children == null); //leaf node가 아닌 node에 대해서 list에 넣는 동작을 해서는 안된다.
            ownedObject.Add(newObj);
            newObj.TerminalNode = this;
            ChangeObjectCount(1);
            Debug.Assert(parent.ObjectCount >= ObjectCount);
        }

        public void RemoveObject(FieldObject tarObj)
        {
            Debug.Assert(ownedObject.Count == ObjectCount);
            Debug.Assert(children == null); //leaf node가 아닌 node에 대해서 list에 넣는 동작을 해서는 안된다.
            ownedObject.Remove(tarObj);
            tarObj.TerminalNode = null;
            ChangeObjectCount(-1);
            Debug.Assert(parent.ObjectCount >= ObjectCount);
        }

        public bool IsInFieldNode(FieldObject tarObj)
        {
            if ((tarObj.Position.X < Area.X)
                || (tarObj.Position.X >= Area.X + Area.Width)
                || (tarObj.Position.Y < Area.Y)
                || (tarObj.Position.Y >= Area.Y + Area.Height))
                return false;
            return true;
        }

        private void Divide()
        {
            Debug.Assert(children == null || children.Length == 0);
            this.divPos = CalculateDivisionPoint();
            children = new FieldNode[(int)FieldDivisonDirection.MAX];
            children[(int)FieldDivisonDirection.UpperLeft] = new FieldNode(OwnerField, this, 
                new Rect(Area.X, 
                        divPos.Y, 
                        divPos.X - Area.X,
                        Area.Y + Area.Height - divPos.Y));
            children[(int)FieldDivisonDirection.UpperRight] = new FieldNode(OwnerField, this, 
                new Rect(divPos.X,
                        divPos.Y,
                        Area.X + Area.Width - divPos.X,
                        Area.Y + Area.Height - divPos.Y));
            children[(int)FieldDivisonDirection.LowerLeft] = new FieldNode(OwnerField, this,
                new Rect(Area.X,
                        Area.Y,
                        divPos.X - Area.X,
                        divPos.Y - Area.Y));
            children[(int)FieldDivisonDirection.LowerRight] = new FieldNode(OwnerField, this,
                new Rect(divPos.X,
                        Area.Y,
                        Area.X + Area.Width - divPos.X,
                        divPos.Y - Area.Y));

            foreach (var obj in ownedObject)
            {
                //owned object child에게 배분
                children[(int)GetDivisionDirection(obj.Position)].ownedObject.Add(obj);
                children[(int)GetDivisionDirection(obj.Position)].ObjectCount++;
                obj.TerminalNode = children[(int)GetDivisionDirection(obj.Position)];
            }

            int childcount = 0;
            foreach(var c in children)
            {
                Debug.Assert(c.ObjectCount == c.ownedObject.Count);
                childcount += c.ObjectCount;
            } 
            Debug.Assert(ObjectCount == childcount);
            ownedObject.Clear();
        }

        private void Merge()
        {
            Debug.Assert(children != null);

            for (int nodeIdx = 0; nodeIdx < children.Length; nodeIdx++)
            {
                children[nodeIdx].MergeOwnedObject(ownedObject);
                children[nodeIdx].Dispose();
            }

            Debug.Assert(ObjectCount == ownedObject.Count);

            children = null;
        }

        private void MergeOwnedObject(List<FieldObject> parentObjectList)
        {
            //Debug.Assert(parent.ObjectCount >= ObjectCount);
            Debug.Assert(children == null);

            foreach (var obj in ownedObject)
            {
                obj.TerminalNode = parent;
                parentObjectList.Add(obj);
            }
            ownedObject.Clear();
            ObjectCount = 0;
        }

        private FieldDivisonDirection GetDivisionDirection(Vector2 position)
        {
            int direction = 0;
            if (position.X >= divPos.X)
                direction += 1;
            if (position.Y <= divPos.Y)
                direction += 2;

            return (FieldDivisonDirection)direction;
        }

        private void ChangeObjectCount(int ofs)
        {
            ObjectCount += ofs;

            if (children != null && ObjectCount < MinObjectCount)
            {
                Merge();
            }

            if (children == null && ObjectCount > MaxObjectCount)
            {
                Divide();
            }

            if (parent != null)
                parent.ChangeObjectCount(ofs);
        }

        private Vector2 CalculateDivisionPoint()
        {
            //TODO : Calculate Division Point
            float averageX = 0;
            float averageY = 0;
            foreach (FieldObject obj in ownedObject)
            {
                averageX += obj.Position.X;
                averageY += obj.Position.Y;
            }

            averageX /= ownedObject.Count;
            averageY /= ownedObject.Count;

            return new Vector2(averageX, averageY);
            //return new Vector2(Area.X + Area.Width / 2, Area.Y + Area.Height / 2);
        }

        public void PassOwnedObjectInfo(Vector2 position, float radius, FieldObjectType objType, List<FieldObject> list) //TODO : Method 이름 수정
        {
            if (parent != null && !IsRangeIncludeAll(position, radius)) //현재 area 범위에서 지역이 벗어남
            {
                parent.PassOwnedObjectInfo(position, radius, objType, list);
                return;
                //parent가 null이면 이미 Query를 실행하기 전에 다른 필드에서 Query 요청을 보낸 상태
            }
            //더 이상 parent로 올라갈 이유가 없음
            PassOwnedObjectInfoCascade(position, radius, objType, list);
        }

        private void PassOwnedObjectInfoCascade(Vector2 position, float radius, FieldObjectType objType, List<FieldObject> list)
        {
            if (children != null)
            {
                //중점을 기준으로 나뉜 4개의 영역 안에 영역 원이 충돌하는지 확인하고
                //충돌한 child node에게만 fieldobject 정보 요구!
                foreach (var child in children)
                {
                    if (child.IsRangeIntersect(position, radius))
                    {
                        child.PassOwnedObjectInfoCascade(position, radius, objType, list);
                    }
                }
            }
            else
            {
                foreach (var obj in ownedObject)
                {
                    if (objType == FieldObjectType.All ||
                        obj.OwnerType == objType)
                    {
                        if((obj.Position - position).Length <= radius)
                            list.Add(obj);
                    }
                }
                
                //leaf에 도착했으면 걍 정보를 넘기면 된다.
            }
        }

        #region FieldNode CollisionCheck
        private int GetCollisionCheckSectorIndex(Vector2 position)
        {
            /*
            사각형과 원의 충돌 체크를 위해 
            area 부분을 9 sector로 나누어 위치한 곳에 따라 다르게 판정
            area는 4 에 위치
                0    1    2
            ---------------
            2 | 6    7    8
              |
            1 | 3    4    5
              |
            0 | 0    1    2
              |
            Sector number = row * 3 + column 
            */

            int row = (position.Y < Area.Y) ? 0 :
                        (position.Y >= Area.Y + Area.Height) ? 2 : 1;
            int column = (position.X < Area.X) ? 0 :
                        (position.X >= Area.X + Area.Width) ? 2 : 1;

            return row * 3 + column;
        }


        private bool IsRangeIncludeAll(Vector2 position, float radius)
        {
            //중앙을 anchor로 잡고 너비, 높이에서 원의 반지름만큼 뺀 내부 사각형 안에 
            //원의 중점이 들어가 있으면 원이 사각형에 포함되어 있다.
            Rect smallRect = new Rect(Area.X + radius, 
                                      Area.Y + radius, 
                                      Area.Width - 2*radius, 
                                      Area.Height - 2*radius);
            return (smallRect.IsPointIncluded(position)) ? true : false;
        }

        public bool IsRangeIntersect(Vector2 position, float radius)
        {
            //원과 사각형의 충돌 판정
            int sector = GetCollisionCheckSectorIndex(position);
            switch (sector)
            {
                case 4:
                    return true;
                case 3:
                    return (position.X >= Area.X - radius);
                case 5:
                    return (position.X < Area.X + Area.Width + radius);
                case 1:
                    return (position.Y >= Area.Y - radius);
                case 7:
                    return (position.X < Area.Y + Area.Height + radius);
                case 0:
                    return IsCircleIncludePoint(position, radius, 
                                                new Vector2(Area.X, 
                                                            Area.Y));
                case 2:
                    return IsCircleIncludePoint(position, radius, 
                                                new Vector2(Area.X+Area.Width, 
                                                            Area.Y));
                case 6:
                    return IsCircleIncludePoint(position, radius, 
                                                new Vector2(Area.X, 
                                                            Area.Y+Area.Height));
                case 8:
                    return IsCircleIncludePoint(position, radius, 
                                                new Vector2(Area.X+Area.Width, 
                                                            Area.Y+Area.Height));
            }
            return false;
        }

        private bool IsCircleIncludePoint(Vector2 center, float radius, Vector2 position)
        {
            //점이 원에 포함되는가?
            if ((center - position).Length <= radius)
                return true;
            return false;
        }

        //TODO : Rect형 Area에 대해서도 적용
        private bool IsInsideAreaRange(Rect area)
        {
            return true;
        }
        #endregion

        #region FieldNode Debug
        public void WriteLog(LogDispatcher logDispatcher)
        {
            logDispatcher.Write("{0}: ObjectCount: {1}", children == null ? "LeafNode" : "Node", ObjectCount);
            if (children != null)
            {
                foreach (var child in children)
                {
                    child.WriteLog(logDispatcher);
                }
            }
        }
        #endregion

        public void Dispose() { isDisposed = true; }

        FieldNode parent;
        FieldNode[] children;
        Vector2 divPos; //zero point : LowerLeft
        List<FieldObject> ownedObject;
        public Rect Area { get; private set; }
        public int ObjectCount{ get; private set; }

        public FieldBase OwnerField { get; private set; }

        public bool isDisposed = false;

        //TODO : Read These from Config File or else;
        int MaxObjectCount = FieldServerConstant.MaxObjectPerFieldNode; // from end-child
        int MinObjectCount = FieldServerConstant.MinObjectPerFieldNode; // from end-parent
    }


    public enum FieldDivisonDirection 
    {
        //division point에 의해 나눠진 child들의 위치
        UpperLeft = 0,
        UpperRight,
        LowerLeft,
        LowerRight,

        MAX
    }

    public struct Rect
    {
        public Rect(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public float X, Y, Width, Height;

        public bool IsPointIncluded(Vector2 ptr)
        {
            if (ptr.X < X || ptr.X >= X + Width || ptr.Y < Y || ptr.Y >= Y + Height)
                return false;
            return true;
        }

        public override string ToString()
        {
            return string.Format("Rect : ({0},{1},{2},{3})", X, Y, X + Width, Y + Height);
        }

    }
}
