﻿using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Field.Base
{
    public delegate void AllocPlayerHandler(Player newPlayer, Vector2 pos);

    public class Field : FieldBase
    {
        public Field(int id, int terrainType) : base(id, terrainType) { }

        protected override void Initialize(int terrainType)
        {
            rootNode = new FieldNode(this, null, new Rect(0, 0, FieldSize, FieldSize));
            //terrainType을 읽고 해당 XML 파일을 부른 뒤 초기 오브젝트들을 생성한 뒤 rootNode에 초기화
            
            IReadOnlyList<FieldObject> tempList = GetFieldObjectInfo(terrainType);
            rootNode.AddInitialObjects(tempList);

            PlayerCount = 0;
            MonsterCount = 0;
        }

        private IReadOnlyList<FieldObject> GetFieldObjectInfo(int fieldType)
        {
            var objectList = new List<FieldObject>();
            var doodadList = FieldDescriptionFile.ReadDoodadFromFile(fieldType);
            foreach(var info in doodadList)
            {
                var newObj = new FieldObject(info.Position, info.Radius, this, FieldObjectType.Obstacle);
                objectList.Add(newObj);
            }

            return objectList.AsReadOnly();
        }

        public void AllocPlayerFieldObject(Player player, Vector2 pos)
        {
            //TEST POSITION : 플레이어 마다 조금씩 다른 위치
            //Vector2 pos = new Vector2(FieldSize / 2 + playerCount * 2, FieldSize / 2);

            //TEST POSITION : 필드 경계
            //Vector2 pos = new Vector2(10, 490);

            //TEST POSITION : 필드 중앙
            //Vector2 position = pos + new Vector2(FieldSize / 2, FieldSize / 2);

            //필드 중앙으로 부터 랜덤 위치
            Vector2 position = new Vector2(FieldSize / 2, FieldSize / 2) + random.GetPointInCircle(FieldSize / 2);

            FieldObject obj = new FieldObject(position, 0, this, FieldObjectType.Player, player);
            AddObject(obj);

            PlayerCount++;
            player.SetInitialFieldObject(obj);
            //player.FieldObject = obj;
        }
        
        /*
        public void AllocMonsterObject(Monster monster)
        {
            Vector2 pos = new Vector2(15, 490);
            FieldObject obj = new FieldObject(pos, 0, this, FieldObjectType.Monster, monster);
            AddObject(obj);
            monster.SetInitialFieldObject(obj);
     
            MonsterCount++;
        }
        */

        public override void SetNeighbor(FieldBase newNeighbor, FieldConnectDirection direction)
        {
            if (Neighbor.ContainsKey(direction))
                return;

            Neighbor.Add(direction, newNeighbor);


            FieldConnectDirection clockwise = (FieldConnectDirection)(((int)direction + 6 + 1) % 6);
            FieldConnectDirection counterclockwise = (FieldConnectDirection)(((int)direction + 6 - 1) % 6);
            FieldConnectDirection reverse = (FieldConnectDirection)(((int)direction + 3) % 6);

            newNeighbor.SetNeighbor(this, reverse);

            FieldBase existingNeighbor = null;

            Neighbor.TryGetValue(clockwise, out existingNeighbor);
            if (existingNeighbor != null
                && existingNeighbor.GetNeighbor(counterclockwise) == null)
            {
                    existingNeighbor.SetNeighbor(newNeighbor, counterclockwise);
            }

            existingNeighbor = null;
            Neighbor.TryGetValue(counterclockwise, out existingNeighbor);
            if (existingNeighbor != null
                && existingNeighbor.GetNeighbor(counterclockwise) == null)
            {
                existingNeighbor.SetNeighbor(newNeighbor, clockwise);
            }
        }

        public override FieldBase GetNeighbor(FieldConnectDirection direction)
        {
            if (Neighbor.ContainsKey(direction))
            {
                return Neighbor[direction];
            }
            return null;
        }

        public override void AddObject(FieldObject newObj)
        {
            Debug.Assert(rootNode.IsInFieldNode(newObj));
            SearchNode(newObj.Position).AddObject(newObj);
        }

        public override void RemoveObject(FieldObject newObj)
        {
            Debug.Assert(newObj.TerminalNode != null);
            SearchNode(newObj.Position).RemoveObject(newObj);

            if (newObj.OwnerType == FieldObjectType.Player)
            {
                PlayerCount--;
            }
            /*
            else if (newObj.OwnerType == FieldObjectType.Monster)
            {
                MonsterCount--;
            }
            */

        }

        protected override FieldNode SearchNode(Vector2 position) //TODO : Change Method Name
        {
            return rootNode.Search(position);
        }
        
        //TODO : 코드 중복 많이 생길것이므로 합치기 작업
        public override void Query
            (Vector2 position, 
            float radius,
            FieldObjectType objType,
            List<FieldObject> outputList) //TODO : Change Method Name
        {
            if (!IsPointInsideField(position))
            {
                rootNode.PassOwnedObjectInfo(position, radius, objType, outputList);
            }
            else
                Query(rootNode.Search(position), position, radius, objType, outputList);
        }

        public override void Query
            (FieldObject obj, 
            float radius, 
            FieldObjectType objType,
            List<FieldObject> outputList) //TODO : Change Method Name
        {
            Query(obj.TerminalNode, obj.Position, radius, objType, outputList);
        }

        public override void Query
            (FieldObject obj, 
            Rect area,
            FieldObjectType objType,
            List<FieldObject> outputList) //TODO : Change Method Name
        {
            //TODO : Insert included object to outputList according to Rect search range
        }

        public override void Query
            (Vector2 position, 
            Rect area,
            FieldObjectType objType,
            List<FieldObject> outputList)
        {
            //TODO : Insert included object to outputList according to Rect search range
        }

        protected override void Query
            (FieldNode node, 
            Vector2 position, 
            float radius,
            FieldObjectType objType,
            List<FieldObject> outputList)
        {
            outputList.Clear();

            if (!IsRangeIncludeAll(position, radius))
                QueryToNeigbor(position, radius, objType, outputList);
            node.PassOwnedObjectInfo(position, radius, objType, outputList);
        }

        private void QueryToNeigbor(Vector2 position,
            float radius,
            FieldObjectType objType,
            List<FieldObject> outputList)
        {
            Vector2 newPos = position;
            foreach (var neighbor in Neighbor)
            {
                switch(neighbor.Key)
                {
                    case FieldConnectDirection.UpperLeft:
                        newPos = position + new Vector2(FieldSize / 2, -FieldSize);
                        break;
                    case FieldConnectDirection.UpperRight:
                        newPos = position + new Vector2(-FieldSize / 2, -FieldSize);
                        break;
                    case FieldConnectDirection.Right:
                        newPos = position + new Vector2(-FieldSize, 0);
                        break;
                    case FieldConnectDirection.LowerRight:
                        newPos = position + new Vector2(-FieldSize / 2, FieldSize);
                        break;
                    case FieldConnectDirection.LowerLeft:
                        newPos = position + new Vector2(FieldSize / 2, FieldSize);
                        break;
                    case FieldConnectDirection.Left:
                        newPos = position + new Vector2(FieldSize, 0);
                        break;
                    default:
                        Debug.Assert(false);
                        break;
                }

                if(IsRangeIntersects(newPos, radius))
                    neighbor.Value.Query(newPos, radius, objType, outputList);
            }
        }

        private bool IsRangeIncludeAll(Vector2 position, float radius)
        {
            //중앙을 anchor로 잡고 너비, 높이에서 원의 반지름만큼 뺀 내부 사각형 안에 
            //원의 중점이 들어가 있으면 원이 사각형에 포함되어 있다.
            Rect smallRect = new Rect(radius,
                                      radius,
                                      FieldSize - 2 * radius,
                                      FieldSize - 2 * radius);
            return (smallRect.IsPointIncluded(position)) ? true : false;
        }

        private bool IsPointInsideField(Vector2 position)
        {
            if (position.X < 0 || position.Y < 0 || position.X > FieldSize || position.Y > FieldSize)
                return false;
            return true;
        }

        private bool IsRangeIntersects(Vector2 position, float radius)
        {
            return rootNode.IsRangeIntersect(position, radius);
        }

        public int PlayerCount { get; private set; }
        public int MonsterCount { get; private set; }
        
    }
}
