﻿using System;
using System.Collections.Generic;
using OpenTK;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;

namespace HAJE.MMO.Server.Field
{
    public class FieldNetworkContext
    {
        public FieldNetworkContext(LogDispatcher logDispatcher)
        {
            MoveSyncPos = new PositionInfo();
            MoveSync = new MoveSync(MoveSyncPos);
            this.logDispatcher = logDispatcher;
        }

        public void StreamToPlayer(Base.FieldBase field, Vector2 position, PacketBase packet, float rangeRadius = GamePlayConstant.CharacterSyncRange)
        {
            List<FieldObject> objList = bufferList;
            field.Query(position, rangeRadius, FieldObjectType.Player, objList);
            packet.RefCount = objList.Count;
            foreach (var obj in objList)
            {
                var remotePlayer = obj.Owner as Player;
                try
                {
                    remotePlayer.SendMessage(packet);
                }
                catch (Exception e)
                {
                    logDispatcher.Write("StreamToPlayer SendMessage failed: {0}", e.Message);
                }
            }
        }

        public void StreamToPlayer(Player player, PacketBase packet, float rangeRadius = GamePlayConstant.CharacterSyncRange)
        {
            List<FieldObject> objList = bufferList;
            player.Query(rangeRadius, FieldObjectType.Player, objList);
            packet.RefCount = objList.Count;
            foreach (var obj in objList)
            {
                var remotePlayer = obj.Owner as Player;
                try
                {
                    remotePlayer.SendMessage(packet);
                }
                catch (Exception e)
                {
                    logDispatcher.Write("StreamToPlayer SendMessage failed: {0}", e.Message);
                }
            }
        }

        public T GetPacket<T>() where T : PacketBase, new()
        {
            Type type = typeof(T);
            if (packetReadyPool.ContainsKey(type) == false)
                packetReadyPool.Add(type, new Queue<PacketBase>());
            var queue = packetReadyPool[type];

            T packet;
            if (queue.Count == 0)
                packet = new T();
            else
                packet = queue.Dequeue() as T;
            packet.RefCount = 1;
            packet.PoolQueue = queue;
            return packet;
        }

        public PositionInfo MoveSyncPos { get; private set; }
        public MoveSync MoveSync { get; private set; } // OnMoveSync에서만 예외로 쓰기 위함

        LogDispatcher logDispatcher;
        List<FieldObject> bufferList = new List<FieldObject>();
        Dictionary<Type, Queue<PacketBase>> packetReadyPool = new Dictionary<Type, Queue<PacketBase>>();
    }
}
