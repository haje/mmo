﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.AI;
using HAJE.MMO.Server.Field.Base;
using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Field.Object
{
    public class Player
    {
        public delegate void DeadPlayerHandler(int id, string token);

        public Player()
        {
            MaxHP = GamePlayConstant.MaxHitPoint;
            MaxMP = GamePlayConstant.MaxManaPoint;
            HPRegen = GamePlayConstant.HitPointRegen;
            MPRegen = GamePlayConstant.ManaPointRegen;
            HP = MaxHP;
            MP = MaxMP;
            IsDead = false;
            IsDummy = false;
            PositionInfo = new PositionInfo();
            FieldObject = null;

            comboPoint = new Dictionary<ElementType, int>();
            foreach (var e in SpellInfo.ElementTypes)
                comboPoint.Add(e, 0);
            elementPoint = new Dictionary<ElementType, int>();
            foreach (var e in SpellInfo.ElementTypes)
                elementPoint.Add(e, 0);
            spellEquip = new List<SpellId>();
            foreach (var id in SpellInfo.BasicSpellIdList)
                spellEquip.Add(id);
            for (int i = spellEquip.Count; i < GamePlayConstant.MaxSpellEquip; i++)
                spellEquip.Add(0);
            spellCount = new Dictionary<SpellId, int>();

            RegenInterval = GamePlayConstant.RegenInterval;
            SyncInterval = GamePlayConstant.SyncInterval;
        }

        public Player(int id, FieldNetworkContext networkContext, List<FieldObject> bufferList, AIBase dummyAI)
            : this()
        {
            IsDummy = true;
            Name = dummyAI.GetRandomName();
            NetConnection = null;
            this.AI = dummyAI;
            this.networkContext = networkContext;
            this.Id = id;
            this.bufferList = bufferList;

            dummyAI.SetPlayer(this);
        }
        
        public Player(int id, string token, SslStreamConnection connection, FieldNetworkContext networkContext, AllocPlayerHandler allocHandler)
            : this()
        {
            this.Id = id;
            this.Token = token;
            NetConnection = connection;
            this.networkContext = networkContext;
            this.allocHandler = allocHandler;

            NetConnection.DisconnectEvent += OnPlayerDead;
        }

        public void SetInitialFieldObject(FieldObject fieldObject)
        {
            this.FieldObject = fieldObject;
            PositionInfo.FieldId = fieldObject.Field.Id;
            PositionInfo.Position = FieldObject.Position;
            PositionInfo.MotionState = MotionState.Stop;
        }

        private void OnPlayerDead()
        {
            IsDead = true;
            if (DeadPlayerEvent != null)
                DeadPlayerEvent(this.Id, this.Token); 
        }

        public void SendMessage(PacketBase packet)
        {
            if (IsDummy) { packet.Release(); return; }
            if (!NetConnection.IsConnected) return;
            NetConnection.SendMessage(packet);
        }

        public void Kill()
        {
            OnPlayerDead();
            if (NetConnection != null)
            {
                NetConnection.DisconnectEvent -= OnPlayerDead;
                NetConnection.ForceDisconnect();
            }
        }

        public void RemoveFromField()
        {
            if (FieldObject == null)
                return; //Arleady Removed or Not have been allocated
            FieldObject.RemoveFromField();
        }

        public void OnNewCharacterRequest(PacketReader reader, ConnectionBase connection)
        {
            var request = networkContext.GetPacket<NewCharacterRequest>();
            request.Deserialize(reader);
            Name = request.CharacterName;
            allocHandler(this, new Vector2(0, 0));
            request.Release();
        }

        public void ApplyPostionInfo(PositionInfo source)
        {
            //행동 입력에 의한 변환시에 syncInterval은 초기화
            SyncInterval = GamePlayConstant.SyncInterval;

            //Debug.WriteLine(string.Format("before  {0},{1}", PositionInfo.Position, PositionInfo.FieldId));
            //Debug.WriteLine(string.Format("source  {0},{1}", source.Position, source.FieldId));
            if (source.MotionState == MotionState.Stop
                && (PositionInfo.MotionState == MotionState.Run
                || PositionInfo.MotionState == MotionState.Walk))
            {
                //Run이나 Walk중의 Stop의 경우 Streaming 받는 Client 입장에서는 어쩔 수 없이 
                //시간차가 발생하게 되므로 *임시로* 위치 보정을 서버쪽에서도 해주도록 변경

                //TODO : TimeStamp
                source.FieldId = PositionInfo.FieldId;
                source.Position = PositionInfo.Position;//temporary value 
            }
            
           
            if (CurrentField.Id != source.FieldId)
            {
                foreach(var field in CurrentField.Neighbor)
                {
                    if(field.Value.Id == source.FieldId)
                    {
                        PositionInfo.CopyFrom(source);
                        FieldObject.Position = source.Position - source.GetRelativeOfs(field.Key);
                        PositionInfo.Position = FieldObject.Position;
                        PositionInfo.FieldId = CurrentField.Id;
                    }
                }
            }
            else
            {
                PositionInfo.CopyFrom(source);
                FieldObject.Position = PositionInfo.Position;
            }


            //Debug.WriteLine(string.Format("after  {0},{1}", PositionInfo.Position, PositionInfo.FieldId));
            //Node 이동이 있는지 체크 
            //TODO : 여기서 해줘야 할까...?
            //FieldObject.RelocateFieldNode();
        }

        public void Query(float radius, FieldObjectType objType, List<FieldObject> outputList)
        {
            if (FieldObject == null)
                return;

            FieldObject.Query(radius, objType, outputList);
        }
        
        public void Damage(Player caster, int damage)
        {
            if (IsDead) return;
            if (this.AI != null)
                this.AI.DamagedBy(caster);

            HP -= damage;
            if (HP <= 0)
            {
                HP = 0;
                IsDead = true;
            }

            SendStatusSync(StatusType.HitPoint, (int)HP);
            if (IsDead)
            {
                Kill();
            }
        }

        public void ConsumeMana(float cost)
        {
            MP -= cost;
            MP = Math.Max(MP, 0);

            SendStatusSync(StatusType.ManaPoint, (int)MP);
        }

        public int ComboPoint(ElementType element)
        {
            return comboPoint[element];
        }

        public void GainComboPoint(ElementType element, int point)
        {
            comboPoint[element] = Math.Min(comboPoint[element] + point, GamePlayConstant.MaxComboPoint);
            SendStatusSync(StatusType.ComboPoint, element, comboPoint[element]);
        }

        public void ConsumeComboPoint(ElementType element, int cost)
        {
            comboPoint[element] = Math.Max(comboPoint[element] - cost, 0);
            SendStatusSync(StatusType.ComboPoint, element, comboPoint[element]);
        }

        public int ElementPoint(ElementType element)
        {
            return elementPoint[element];
        }

        public void GainElementPoint(ElementType element, int point)
        {
            elementPoint[element] = Math.Min(elementPoint[element] + point, GamePlayConstant.MaxElementPoint);
            SendStatusSync(StatusType.ElementPoint, element, elementPoint[element]);
        }

        public void ConsumeElementPoint(ElementType element, int cost)
        {
            elementPoint[element] = Math.Max(elementPoint[element] - cost, 0);
            SendStatusSync(StatusType.ElementPoint, element, elementPoint[element]);
        }

        public int SpellCount(SpellId spellId)
        {
            return spellCount.ContainsKey(spellId) ? spellCount[spellId] : 0;
        }

        public void GainSpellCount(SpellId spellId, int count)
        {
            if (spellCount.ContainsKey(spellId))
                spellCount[spellId] += count;
            else
                spellCount[spellId] = count;
            SendStatusSync(StatusType.SpellCount, spellId, spellCount[spellId]);
        }

        public void ConsumeSpellCount(SpellId spellId)
        {
            if (spellCount.ContainsKey(spellId) && spellCount[spellId] > 0)
            {
                spellCount[spellId]--;
                SendStatusSync(StatusType.SpellCount, spellId, spellCount[spellId]);
            }
        }

        public void EquipSpell(int index, SpellId spellId)
        {
            spellEquip[index] = spellId;
        }

        public void Update(Second deltaTime, LogDispatcher log = null)
        {
            if (FieldObject == null) //Not Initialized;
                return;

            if (IsDummy)
                AI.Update(deltaTime);

            switch (PositionInfo.MotionState)
            {
                case MotionState.Stop:
                    break;

                case MotionState.Jump:
                case MotionState.Run:
                case MotionState.Walk:
                    PositionInfo.Position += PositionInfo.Velocity * deltaTime;
                    FieldObject.Position += PositionInfo.Velocity * deltaTime;
                    
                    if (FieldObject.OnProperPosition(PositionInfo))
                    {
                        FieldObject.RelocateFieldNode(PositionInfo);
                    }
                    else
                    {
                        PositionInfo.Position -= PositionInfo.Velocity * deltaTime;
                        FieldObject.Position -= PositionInfo.Velocity * deltaTime;
                        PositionInfo.Velocity = Vector2.Zero;
                    }
                    break;
            }

            SyncInterval -= deltaTime;
            if (SyncInterval < 0)
            {
                //Beacon Sync !
                SyncInterval = GamePlayConstant.SyncInterval;
                StreamToOtherPlayer();
            }

            RegenInterval -= deltaTime;
            if (RegenInterval < 0)
            {
                RegenInterval = GamePlayConstant.RegenInterval;
                if (HP != MaxHP)
                {
                    HP = Math.Min(HP + HPRegen, MaxHP);
                    SendStatusSync(StatusType.HitPoint, (int)HP);
                }
                if (MP != MaxMP)
                {
                    MP = Math.Min(MP + MPRegen, MaxMP);
                    SendStatusSync(StatusType.ManaPoint, (int)MP);
                }
            }

        }

        StatusSync GetStatusSync(StatusType statusType, int value)
        {
            var statusSync = networkContext.GetPacket<StatusSync>();
            statusSync.CharacterId = Id;
            statusSync.StatusType = statusType;
            statusSync.Value = value;
            return statusSync;
        }

        void SendStatusSync(StatusType statusType, int value)
        {
            if (IsDummy) return;
            var statusSync = GetStatusSync(statusType, value);
            SendMessage(statusSync);
        }

        void SendStatusSync(StatusType statusType, ElementType elementType, int value)
        {
            if (IsDummy) return;
            var statusSync = GetStatusSync(statusType, value);
            statusSync.ElementType = elementType;
            SendMessage(statusSync);
        }

        void SendStatusSync(StatusType statusType, SpellId spellId, int value)
        {
            if (IsDummy) return;
            var statusSync = GetStatusSync(statusType, value);
            statusSync.SpellId = spellId;
            SendMessage(statusSync);
        }

        void StreamToOtherPlayer()
        {
            //Is it OK using movesyncpos?
            var moveSync = networkContext.GetPacket<MoveSync>();
            moveSync.CopyFrom(PositionInfo);
            moveSync.CharacterId = Id;
            networkContext.StreamToPlayer(this, moveSync);
        }

        public float MaxHP { get; private set; }
        public float MaxMP { get; private set; }
        public float HP { get; private set; }
        public float MP { get; private set; }
        public float HPRegen { get; private set; }
        public float MPRegen { get; private set; }
        public bool IsDead { get; private set; }
        public bool IsDummy { get; private set; }
        
        public PositionInfo PositionInfo { get; set; }
        public FieldObject FieldObject { get; private set; }

        public FieldBase CurrentField
        {
            get
            {
                return FieldObject.Field;
            }
        }

        public SslStreamConnection NetConnection { get; set; }
        public int Id { get; private set; }

        // AI 디버깅 용으로 잠시 인터페이스를 열어 둠
        public string Name { get; set; }
        public string Token { get; set; }

        FieldNetworkContext networkContext;
        AllocPlayerHandler allocHandler;

        public event DeadPlayerHandler DeadPlayerEvent = delegate {};

        Dictionary<ElementType, int> comboPoint;
        Dictionary<ElementType, int> elementPoint;
        List<SpellId> spellEquip;
        Dictionary<SpellId, int> spellCount;

        Second RegenInterval = Second.Zero;
        
        public Second SyncInterval = Second.Zero;
        public List<FieldObject> bufferList;

        public AIBase AI { get; private set; }
    }
}
