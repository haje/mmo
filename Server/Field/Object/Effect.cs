﻿namespace HAJE.MMO.Server.Field.Object
{
    public class Effect
    {
        public Effect(int id, EffectType effectType, PositionInField position)
        {
            this.Id = id;
            this.Type = effectType;
            this.Position = position;
        }
        
        public int Id { get; private set; }
        public EffectType Type { get; private set; }
        public PositionInField Position { get; set; }
    }
}
