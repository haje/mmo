﻿using System;
using System.Collections.Generic;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Server.Field.Object
{
    public class DropObject
    {
        public delegate void GainDropHandler(Player player, DropObject dropObject);
        public static Random random = new Random();
        public static Array values = Enum.GetValues(typeof(ElementType));

        public DropObject(int id, FieldObject fieldObject)
        {
            Id = id;
            FieldObject = fieldObject;
            ElementType = (ElementType)values.GetValue(random.Next(1, (int)ElementType.Max - 1));
            Point = random.Next(3, 9);
        }

        public void Update(Second deltaTime, LogDispatcher log = null)
        {
            FieldObject.Field.Query(FieldObject, 10.0f, FieldObjectType.Player, buffer);
            foreach (var obj in buffer)
            {
                Player player = obj.Owner as Player;
                if (DropGainEvent != null)
                    DropGainEvent(player, this);
                break;
            }
        }

        public event GainDropHandler DropGainEvent = delegate {};

        public int Id { get; private set; }
        public FieldObject FieldObject { get; private set; }
        public ElementType ElementType { get; private set; }
        public int Point { get; private set; }
        public Effect Effect { get; set; }

        List<FieldObject> buffer = new List<FieldObject>();
    }
}
