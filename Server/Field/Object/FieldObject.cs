﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;
using HAJE.MMO.Server.Field.Base;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Field.Object
{
    public class FieldObject
    {
        public FieldObject(Vector2 position, float radius, FieldBase ownedField, FieldObjectType ownerType) 
        {
            Position = position;
            this.Field = ownedField;
            this.OwnerType = ownerType;
            this.Radius = radius;
        }

        public FieldObject(Vector2 position, float radius, FieldBase ownedField, FieldObjectType ownerType, object owner)
            : this(position, radius, ownedField, ownerType)
        {
            this.Owner = owner;
        }

        public void RelocateFieldNode(PositionInfo positionInfo, LogDispatcher log = null)
        {
            if (log != null)
            {
                WriteLog(log);
                log.Write(TerminalNode.Area.ToString());
            }

            if (!TerminalNode.IsInFieldNode(this))
            {
                if (log != null)
                {
                    log.Write("Relocate Start!");
                }

                FieldNode currentNode = TerminalNode;
                TerminalNode.RemoveObject(this);
                try
                {
                    if (Position.X < 0 || Position.X >= FieldBase.FieldSize
                        || Position.Y < 0 || Position.Y >= FieldBase.FieldSize)
                    {
                        MoveToOtherField(positionInfo, log);
                    }
                    else
                    {
                        Field.AddObject(this);
                    }


                    if (log != null)
                    {
                        log.Write("Relocate Object To Field {0}", Field.Id);
                        log.Write("New Position : {0}", positionInfo.Position);
                    }
                }
                catch(Exception ex)
                {
                    currentNode.AddObject(this);
                    if (log != null)
                        log.Write("RelocateFieldNode EXCEPTION!!!");
                    throw ex;
                }
            }
        }

        public void Relocate(FieldBase newField, PositionInfo positionInfo, LogDispatcher log = null)
        {
            if (newField.Id != positionInfo.FieldId)
                return;

            FieldNode currentNode = TerminalNode;
            TerminalNode.RemoveObject(this);

            try
            {
                newField.AddObject(this);
            }
            catch(Exception ex)
            {
                currentNode.AddObject(this);
                if (log != null)
                    log.Write("Relocate EXCEPTION!!!");
            }

        }
        

        public void Query(float radius,  FieldObjectType objType, List<FieldObject> outputList)
        {
            if (Field == null)
                return;

            Field.Query(this, radius, objType, outputList);
        }

        public void RemoveFromField()
        {
            Debug.Assert(Field != null);
            Field.RemoveObject(this);
        }

        public bool OnProperPosition(PositionInfo positionInfo)
        {
            if (0 <= positionInfo.Position.X && positionInfo.Position.X < FieldBase.FieldSize)
                if (0 <= positionInfo.Position.Y && positionInfo.Position.Y < FieldBase.FieldSize)
                    return true;

            FieldConnectDirection newNeighborDirection = positionInfo.GetAreaOffDirection();
            return Field.Neighbor.ContainsKey(newNeighborDirection);
        }
        void MoveToOtherField(PositionInfo positionInfo, LogDispatcher log = null)
        {
            if (log != null)
                log.Write("movetoother : {0} {1} {2}",positionInfo.FieldId,Position,positionInfo.Position);
            FieldConnectDirection newNeighborDirection = positionInfo.GetAreaOffDirection();

            if (!Field.Neighbor.ContainsKey(newNeighborDirection))
                 throw new Exception(string.Format("{0}, No Neighbor Field in {1}!!", Field.Id,newNeighborDirection));
            else
            {
                int FieldSize = FieldBase.FieldSize;
                switch (newNeighborDirection)
                {
                    case FieldConnectDirection.UpperLeft:
                        Position += new Vector2(FieldSize / 2, -FieldSize);
                        break;
                    case FieldConnectDirection.UpperRight:
                        Position += new Vector2(-FieldSize / 2, -FieldSize);
                        break;
                    case FieldConnectDirection.Right:
                        Position += new Vector2(-FieldSize, 0);
                        break;
                    case FieldConnectDirection.LowerRight:
                        Position += new Vector2(-FieldSize / 2, FieldSize);
                        break;
                    case FieldConnectDirection.LowerLeft:
                        Position += new Vector2(FieldSize / 2, FieldSize);
                        break;
                    case FieldConnectDirection.Left:
                        Position += new Vector2(FieldSize, 0);
                        break;
                    default:
                        Debug.Assert(false);
                        break;
                }
                Field = Field.Neighbor[newNeighborDirection];
                Field.AddObject(this);
                positionInfo.Position = Position;
                positionInfo.FieldId = Field.Id;
            }
        }

        public PositionInField GetPosInField(bool isGroundRelative = false)
        {
            return new PositionInField(Field.Id, GetPos3(isGroundRelative), isGroundRelative);
        }

        public Vector3 GetPos3(bool isGroundRelative = false)
        {
            float height = isGroundRelative ? 0 : Height;
            if (OwnerType == FieldObjectType.Player || OwnerType == FieldObjectType.Drop)
                height += 1.0f;
            return new Vector3(Position.X, height, Position.Y);
        }

        public float Height
        {
            get { return Field.HeightMap.GetHeight(Position); }
        }

        public Vector2 Position { get; set; }
        public float Radius { get; set; }
        public FieldBase Field { get; private set; }
        public FieldObjectType OwnerType { get; private set; }
        public object Owner { get; private set; }

        public FieldNode TerminalNode { get; set; }

        #region FieldObject Debug
        public void WriteLog(LogDispatcher logDispatcher)
        {
            logDispatcher.Write("FieldObject - ( field : {0}, position : {1} , OwnerType: {2} )",Field.Id,Position, OwnerType);
        }
        #endregion

    }

    public enum FieldObjectType
    {
        Player = 0,
        Obstacle,
        Crebass,
        Drop,

        All
    }
}
