﻿using System.Collections.Generic;

namespace HAJE.MMO.Server.Field.Object
{
    public class SpellObject
    {
        public SpellObject(int id, int casterId, PositionInField position)
        {
            OnUpdate = true;
            Id = id;
            CasterId = casterId;
            Position = position;
            EffectList = new List<Effect>();
        }

        public bool OnUpdate { get; set; }
        public int Id { get; private set; }
        public int CasterId { get; private set; }
        public PositionInField Position { get; set; }
        public List<Effect> EffectList { get; private set; }
    }
}
