﻿using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Server.Field.Spell;
using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.MMO.Server.Field.AI
{
    public abstract class AIBase
    {
        #region Static
        public static Dictionary<Type, List<String>> NamePool;
        static AIBase()
        {
            NamePool = new Dictionary<Type, List<String>>();
        }
        #endregion
        #region variable
        protected Random random;
        public Player Player
        {
            get;
            protected set;
        }

        protected SpellHandler spellHandler;

        protected String innerName
        {
            get;
            set;
        }
        private String _debugName;
        protected String debugName
        {
            get
            {
                return _debugName;
            }
#if DEBUG
            set
            {
                if (value == _debugName)
                    return;

                _debugName = value;
                if (Player != null)
                {
                    if (String.IsNullOrWhiteSpace(_debugName) == true)
                        Player.Name = innerName;
                    else
                        Player.Name = String.Format("[{0}]{1}", _debugName, innerName);
                }
            }
#else
            set
            {
                _debugName = value;
            }
#endif
        }

        private bool needSync;
        private MotionState _motionState;
        private Vector2 _velocity;
        private Vector3 _direction;
        #region Interface of motionState/velocity/direction
        protected MotionState motionState
        {
            get
            {
                return _motionState;
            }
            set
            {
                if (_motionState == value)
                    return;

                _motionState = value;
                needSync = true;
            }
        }
        protected Vector2 velocity
        {
            get
            {
                return _velocity;
            }
            set
            {
                if (_velocity == value)
                    return;

                _velocity = value;
                needSync = true;
            }
        }
        protected Vector3 direction
        {
            get
            {
                return _direction;
            }
            set
            {
                if (_direction == value)
                    return;

                _direction = value;
                needSync = true;
            }
        }
        #endregion
        #endregion

        #region .ctor
        protected AIBase()
        {
            random = new Random(DateTime.Now.Millisecond);
        }
        #endregion

        #region Set Player 
        public void SetPlayer(Player player)
        {
            this.Player = player;
            player.DeadPlayerEvent += (lhs, rhs) =>
            {
                ServerContext.Instance.FieldServer.FieldServerLogic.AIManager.RemoveAI(this);
            };
        }

        public void SetSpellHandler(SpellHandler spellHandler)
        {
            this.spellHandler = spellHandler;
        }
        public String GetRandomName()
        {
            String name = NamePool[GetType()][random.Next(NamePool[GetType()].Count)];
            innerName = name;
            return name;
        }
        #endregion
        #region Update
        public void Update(Second deltaTime)
        {
            InnerUpdate(deltaTime);
            if (needSync == true)
                Sync();
        }
        #endregion
        
        #region Helper Functions
        protected void RunToDirection(Vector2 dir)
        {
            if (float.IsNaN(dir.X) || float.IsNaN(dir.Y))
                throw new Exception();

            velocity = Vector2.Normalize(dir) * GamePlayConstant.RunSpeed * 0.8f;
            motionState = MotionState.Run;
            direction = Vector3.Normalize(new Vector3(dir.X, 0, dir.Y));
        }
        protected void Stop()
        {
            velocity = Vector2.Zero;
            motionState = MotionState.Stop;
        }
        protected void UseSpell(SpellId spellId, Player target, float accuracy)
        {
            bool isAccurate = random.NextFloat() < accuracy;
            var spell = SpellInfo.GetInfo(spellId);

            TargetType targetType = TargetType.Air;
            var playerPos = Player.FieldObject.GetPos3();
            var targetPos = target.FieldObject.GetPos3();
            if (spell.AimType == AimType.Object)
            {
                if (isAccurate)
                    targetType = TargetType.Player;
                else
                    targetPos += random.NextFloat() * 3 * new Vector3(1, 0, 1);
            }
            else
            {
                if (!isAccurate)
                    targetPos += random.NextFloat() * 3 * new Vector3(1, 0, 1);
            }
            Player.PositionInfo.Direction = (targetPos-playerPos).Normalized();

            SpellTarget spellTarget = new SpellTarget(targetType, 
                new PositionInField(target.FieldObject.Field.Id, targetPos), target.Id);
            spellHandler.HandleSpellCastEnd(Player, spellId, spellTarget);
        }
        #endregion
        
        #region Sync!
        private void Sync()
        {
            Player.PositionInfo.Velocity = this.velocity;
            Player.PositionInfo.MotionState = this.motionState;
            Player.PositionInfo.Direction = this.direction;

            needSync = false;
            Player.SyncInterval = Second.Zero;
        }
        #endregion

        #region Abstract
        abstract protected void SetDebugName();

        public abstract void DamagedBy(Player player);
        protected abstract void InnerUpdate(Second deltaTime);
        #endregion
    }
}
