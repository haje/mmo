﻿using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Field.AI
{
    public sealed class SimpleAI : AIBase
    {
        private bool betrayed;
        private Player trackTarget;

        private Player _aggro;
        private Player aggro
        {
            get
            {
                if (betrayed == true)
                    return trackTarget;
                return _aggro;
            }
            set
            {
                _aggro = value;
            }
        }
        private List<Player> enemyList;

        static SimpleAI()
        {
            Type t = typeof(SimpleAI);
            NamePool.Add(t, new List<string>());
            NamePool[t].Add("YandereChan");
        }
        public SimpleAI()
            : base()
        {
            betrayed = false;
            enemyList = new List<Player>();

            remainAttackTime = attackCoolTime;
        }

        // 어그로 관리
        public override void DamagedBy(Player player)
        {
            if (player.Id == trackTarget.Id)
                betrayed = true;

            enemyList.Add(player);
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            Player.FieldObject.Field.Query(Player.FieldObject, 25.0f, FieldObjectType.Player, Player.bufferList);

            if (trackTarget == null)
            {
                foreach (var obj in Player.bufferList)
                {
                    var dir = obj.Position - Player.FieldObject.Position;
                    if (dir.Length > 15.0f)
                    {
                        Player pl = obj.Owner as Player;
                        if (pl.IsDummy == false || pl.AI is SimpleAI == false)
                        {
                            trackTarget = pl;
                            break;
                        }
                    }
                }
            }
            
            bool tracking = false;
            if (trackTarget != null)
            {
                Vector2 dir = trackTarget.PositionInfo.Position - Player.FieldObject.Position;
                
                // 일정 거리 이상 길면 trackTarget을 따라감.
                if (dir.Length > 7.0f)
                {
                    tracking = true;
                    RunToDirection(trackTarget.PositionInfo.Position - Player.FieldObject.Position);
                }

                // 캐스팅! 발싸!
                if (aggro != null)
                {
                    if (remainAttackTime <= 0.0f)
                    {
                        if (dir.Length < 14.0f)
                        {
                            UseSpell(SpellId.ArcaneSlowMissile, aggro, 1.0f);
                            remainAttackTime = attackCoolTime;
                        }
                    }
                    else
                        remainAttackTime -= deltaTime;
                }

                // 어그로 관리
                if (aggro == null)
                {
                    Player.FieldObject.Field.Query(Player.FieldObject, 25.0f, FieldObjectType.Player, Player.bufferList);
                    foreach (FieldObject obj in Player.bufferList)
                        if (enemyList.Contains(obj.Owner as Player))
                        {
                            aggro = obj.Owner as Player;
                            break;
                        }
                }
            }
            if (tracking == false)
                Stop();
        }

        protected override void SetDebugName()
        {
        }

        Second remainAttackTime;
        readonly Second attackCoolTime = (Second)5.0f;
    }
}
