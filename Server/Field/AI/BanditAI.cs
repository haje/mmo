﻿using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Spell;
using OpenTK;
using System;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Field.AI
{
    public class BanditAI : AIBase
    {
        private bool isLeader;
        public bool IsLeader
        {
            get
            {
                return isLeader;
            }
            set
            {
                if (isLeader == value)
                    return;

                isLeader = value;
                SetDebugName();
            }
        }
        private Player leader;
        public Player Leader
        {
            get
            {
                return leader;
            }
            set
            {
                if (leader == value)
                    return;

                leader = value;
                SetDebugName();
            }
        }
        public List<Player> EnemyList;
        public List<Player> AlliList;

        static BanditAI()
        {
            Type t = typeof(BanditAI);
            NamePool.Add(t, new List<string>());
            NamePool[t].Add("Bandit");
        }

        public BanditAI()
            : base()
        {
            EnemyList = new List<Player>();
            AlliList = new List<Player>();

            IsLeader = false;
            Leader = null;
            remainAttackTime = attackCoolTime;
            movingTimer = Second.Zero;
        }

        private void SetAttackTarget(Player target)
        {
            if (EnemyList.Contains(Player))
                return;

            EnemyList.Add(target);
        }

        public override void DamagedBy(Player player)
        {
            if (EnemyList.Contains(player))
                return;
            if (AlliList.Contains(player))
                return;

            foreach (Player alli in AlliList)
                (alli.AI as BanditAI).SetAttackTarget(player);
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            if (AlliList.Contains(Player) == false)
                AlliList.Add(Player);

            Player.FieldObject.Field.Query(Player.FieldObject, 25.0f, FieldObjectType.Player, Player.bufferList);
            
            // 죽은 사람은 빼야지
            AlliList.RemoveAll(_ => _.IsDead == true);
            EnemyList.RemoveAll(_ => _.IsDead == true);

            // 너무 멀어도 빼자
            EnemyList.RemoveAll(_ => (_.PositionInfo.Position - Player.PositionInfo.Position).Length > 35f);

            // Leader 관리. 선착순.
            if (AlliList.Exists(_ => (_.AI as BanditAI).IsLeader == true) == false)
            {
                this.IsLeader = true;

                foreach (Player alli in AlliList)
                    (alli.AI as BanditAI).Leader = Player;
            }
            // 리더가 너무 많으면 망함
            if (this.IsLeader == true)
                if (AlliList.Exists(_ => _ != Player && (_.AI as BanditAI).IsLeader == true) == true)
                {
                    this.IsLeader = false;
                    
                    Player newLeader = AlliList.Find(_ => (_.AI as BanditAI).IsLeader == true);
                    foreach (Player alli in AlliList)
                        (alli.AI as BanditAI).Leader = newLeader;
                }

            foreach (FieldObject obj in Player.bufferList)
            {
                Player pl = obj.Owner as Player;
                if (AlliList.Contains(pl))
                    continue;
                else if (EnemyList.Contains(pl))
                    continue;
                else if (pl.AI != null && pl.AI is BanditAI)
                {
                    if (AlliList.Count < 4 && (pl.AI as BanditAI).AlliList.Count < 3)
                        AlliList.Add(pl);
                }
                else
                    EnemyList.Add(pl);
            }

            if (EnemyList.Count != 0)
            {
                if (remainAttackTime <= 0.0f)
                {
                    UseSpell(SpellId.ArcaneSlowMissile, EnemyList[random.Next(EnemyList.Count)], 1.0f);
                    remainAttackTime = attackCoolTime;
                }
                else
                    remainAttackTime -= deltaTime;
            }

            if (movingTimer < 0f)
            {
                if (EnemyList.Count != 0)
                {
                    Vector2 dir = EnemyList[random.Next(EnemyList.Count)].PositionInfo.Position - Player.PositionInfo.Position;
                    if (dir != Vector2.Zero)
                        dir.Normalize();

                    RunToDirection(dir + random.GetUnitVector2());
                    movingTimer = movingTimerConst;
                }
                else
                {
                    if (IsLeader == true)
                    {
                        RunToDirection(random.GetUnitVector2());
                        movingTimer = movingTimerConst * 5;
                    }
                    else if (Leader != null)
                    {
                        Vector2 dir = Leader.PositionInfo.Position - Player.PositionInfo.Position;
                        RunToDirection(dir + random.GetUnitVector2());
                        movingTimer = movingTimerConst;
                    }
                }
            }
            else
                movingTimer -= deltaTime;
        }

        protected override void SetDebugName()
        {
            if (Leader == null)
                debugName = (IsLeader == true ? "L_" : "S_");
            else
                debugName = (IsLeader == true ? "L_" : "S_") + (Int32.MaxValue - Leader.Id);
        }

        Second movingTimer;
        readonly Second movingTimerConst = (Second)2.0f;

        Second remainAttackTime;
        readonly Second attackCoolTime = (Second)5.0f;
    }
}
