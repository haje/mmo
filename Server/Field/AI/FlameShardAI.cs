﻿using System;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Server.Field.AI.BT;
using System.Collections.Generic;
using HAJE.MMO.Spell;
using OpenTK;

namespace HAJE.MMO.Server.Field.AI
{
    // Warning: Broken AI
    public class FlameShardAI : AIBase
    {
        private static BehaviourTree behaviourTree;
        static FlameShardAI()
        {
            Type t = typeof(FlameShardAI);
            NamePool.Add(t, new List<string>());
            NamePool[t].Add("Shard");

            behaviourTree = new BehaviourTree();
            behaviourTree.SetRoot(new SequenceBTNode())
                .AddChild(new ActionBTNode((_) => (_ as FlameShardAI).Initialize()))
                .AddChild(new SelectorBTNode())
                    .AddChild(new ActionBTNode((_) => (_ as FlameShardAI).CheckAggro()))
                    .AddChild(new ActionBTNode((_) => (_ as FlameShardAI).AttackTarget()))
                    .Parent
                .AddChild(new ActionBTNode((_) => (_ as FlameShardAI).RunToRandom()));
        }

        public FlameShardAI()
            : base()
        {
            // TODO: MaxHP, 공격력 세팅
        }
        public bool Initialize()
        {
            if (Aggro != null)
            {
                if (Aggro.IsDead == true)
                    Aggro = null;
                else if ((Aggro.PositionInfo.Position - this.Player.PositionInfo.Position).Length > 20)
                    Aggro = null;
            }

            return true;
        }
        public bool CheckAggro()
        {
            return this.Aggro == null;
        }
        public bool AttackTarget()
        {
            attackTimer -= deltaTime;
            if (attackTimer <= Second.Zero)
            {
                attackTimer = attackTimerConst;
                UseSpell(SpellId.ArcaneSlowMissile, Aggro, 1.0f);
            }

            return true;
        }
        public bool RunToRandom()
        {
            randomTimer -= deltaTime;
            if (Aggro == null)
            {
                if (randomTimer <= Second.Zero)
                {
                    dir = random.GetUnitVector2();
                    randomTimer = randomTimerConst + random.NextSecond(-randomTimerError, randomTimerError);
                }
            }
            else
            {
                dir = Aggro.PositionInfo.Position - this.Player.PositionInfo.Position;
                dir = (Vector2.Normalize(dir) + random.GetUnitVector2());
            }
            
            RunToDirection(dir);
            return true;
        }

        private Vector2 dir;
        private Second deltaTime = Second.Zero;
        private Second randomTimer = Second.Zero;
        private Second attackTimer = Second.Zero;
        private readonly Second randomTimerConst = (Second)3.0f;
        private readonly Second randomTimerError = (Second)0.5f;
        private readonly Second attackTimerConst = (Second)3.0f;
        public Player Aggro
        {
            get;
            private set;
        }

        public override void DamagedBy(Player player)
        {
            if (Aggro == null)
                Aggro = player;
        }
        protected override void InnerUpdate(Second deltaTime)
        {
            this.deltaTime = deltaTime;
            behaviourTree.Traverse(this);
        }
        protected override void SetDebugName()
        {
        }
    }
}
