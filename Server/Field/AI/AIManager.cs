﻿using HAJE.MMO.Server.Field.Object;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HAJE.MMO.Server.Field.AI
{
    public class AIManager
    {
        private ServerContext serverContext;
        private List<Player> playerList;
        private Dictionary<Type, List<AIBase>> aiList;
        private Dictionary<Type, ConstructorInfo> ctorList;
        private Dictionary<Type, float> densityList;

        private const int FieldSize = 100;

        private Random random;

        public AIManager(ServerContext ctx)
        {
            serverContext = ctx;
            playerList = new List<Player>();
            aiList = new Dictionary<Type, List<AIBase>>();
            ctorList = new Dictionary<Type, ConstructorInfo>();
            densityList = new Dictionary<Type, float>();

            random = new Random(DateTime.Now.Millisecond);

            // Need Serialization?
            densityList.Add(typeof(FlameShardAI), 0.005f);

            foreach (Type t in densityList.Keys)
            {
                aiList.Add(t, new List<AIBase>());
                ctorList.Add(t, t.GetConstructor(new Type[] { }));
            }
        }

        // Todo :
        // 적당히 보이지 않는곳에서 생성되도록 해야 함.
        public void GenerateAI(Type aiType, Vector2 pos)
        {
            AIBase ai = ctorList[aiType].Invoke(new System.Object[] { }) as AIBase;
            aiList[aiType].Add(ai);
            
            serverContext.FieldServer.FieldServerLogic.PlayerManager.CreateDummyPlayer(ai, pos);
        }
        public void RemoveAI(AIBase ai)
        {
            aiList[ai.GetType()].Remove(ai);
        }
        
        public void Update(Second deltaTime)
        {
            foreach (Type t in densityList.Keys)
                if (aiList[t].Count < FieldSize * FieldSize * densityList[t])
                    GenerateAI(t, new Vector2(random.Next(-FieldSize / 2, FieldSize / 2), random.Next(-FieldSize / 2, FieldSize / 2)));
        }
    }
}
