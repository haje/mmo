﻿using System;

namespace HAJE.MMO.Server.Field.AI.BT
{
    public class SequenceBTNode : BehaviourTreeNode
    {
        public SequenceBTNode()
            : base()
        {
        }

        public override bool Traverse(AIBase ai)
        {
            if (Childlen.Count == 0)
                throw new Exception("Sequence BT Node should not be leaf node");

            foreach (BehaviourTreeNode child in Childlen)
            {
                bool result = child.Traverse(ai);
                if (result == false)
                    return false;
            }
            return true;
        }
    }
}
