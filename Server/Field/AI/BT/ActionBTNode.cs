﻿using System;

namespace HAJE.MMO.Server.Field.AI.BT
{
    public delegate bool Actor(AIBase ai);
    public class ActionBTNode : BehaviourTreeNode
    {
        private Actor actor;
        public ActionBTNode(Actor actor)
            : base()
        {
            this.actor = actor;
        }
        public override bool Traverse(AIBase ai)
        {
            if (Childlen.Count != 0)
                throw new Exception("Actor should be leaf node");

            return actor.Invoke(ai);
        }      
    }
}
