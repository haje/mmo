﻿using System;

namespace HAJE.MMO.Server.Field.AI.BT
{
    public class BehaviourTree
    {
        public BehaviourTreeNode RootNode
        {
            get;
            private set;
        }      

        public BehaviourTree()
        {
        }

        public BehaviourTreeNode SetRoot(BehaviourTreeNode node)
        {
            RootNode = node;
            return RootNode;
        }
        public void Traverse(AIBase ai)
        {
            if (RootNode == null)
                throw new Exception("Root node is empty");

            RootNode.Traverse(ai);
        }
    }
}
