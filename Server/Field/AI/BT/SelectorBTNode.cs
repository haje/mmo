﻿using System;

namespace HAJE.MMO.Server.Field.AI.BT
{
    public class SelectorBTNode : BehaviourTreeNode
    {
        public SelectorBTNode()
            : base()
        {
        }

        public override bool Traverse(AIBase ai)
        {
            if (Childlen.Count == 0)
                throw new Exception("Selector BT Node should not be leaf node");

            foreach (BehaviourTreeNode child in Childlen)
            {
                bool result = child.Traverse(ai);
                if (result == true)
                    return true;
            }
            return false;
        }
    }
}
