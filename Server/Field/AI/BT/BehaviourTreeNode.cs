﻿using System.Collections.Generic;

namespace HAJE.MMO.Server.Field.AI.BT
{
    public abstract class BehaviourTreeNode
    {
        public BehaviourTreeNode Parent;
        public List<BehaviourTreeNode> Childlen;
        
        protected BehaviourTreeNode()
        {
            Parent = null;
            Childlen = new List<BehaviourTreeNode>();
        }
        public BehaviourTreeNode AddChild(BehaviourTreeNode node)
        {
            Childlen.Add(node);
            node.Parent = this;

            if (node is SelectorBTNode || node is SequenceBTNode)
                return node;
            else
                return this;
        }

        public abstract bool Traverse(AIBase ai);
    }
}
