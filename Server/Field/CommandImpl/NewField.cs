﻿using System;
using HAJE.MMO.Server.Console;

using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Field.CommandImpl
{
    public class NewField : Command
    {
        public NewField(ServerContext ctx)
            : base("newfield", "새 필드를 생성합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 2)
            {
                try
                {
                    int fieldId = int.Parse(commandTokens[1]);
                    int neighnborDirectionId = int.Parse(commandTokens[2]);
                    if(neighnborDirectionId > 5 || neighnborDirectionId < 0)
                    {
                        WriteConsole("방향값은 0과 5사이의 값이어야 합니다.");
                        return true;
                    }
                    ctx.FieldServer.FieldServerLogic.FieldManager.AllocNewField(fieldId, (FieldConnectDirection)neighnborDirectionId);
                    WriteConsole("Create Field");
                }
                catch (Exception ex)
                {
                    WriteConsole(ex.Message);
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: 현재 필드 번호, 방향 (좌상단부터 시작해서 시계방향으로 0~5)");
            }
            return true;
        }
        
        private readonly ServerContext ctx;
    }
}
