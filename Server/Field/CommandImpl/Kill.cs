﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Field.CommandImpl
{
    public class Kill : Command
    {
        public Kill(ServerContext ctx)
            : base("kill", "해당 id를 가진 유저를 강제로 죽입니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                string userId = commandTokens[1];
                try
                {
                    WriteConsole("KILL Player {0} ", userId);
                    ctx.FieldServer.FieldServerLogic.PlayerManager.KillPlayer(int.Parse(userId));
                }
                catch(Exception ex)
                {
                    WriteConsole(ex.Message);
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: PlayerId");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
