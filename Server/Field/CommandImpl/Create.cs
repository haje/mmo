﻿using System;
using HAJE.MMO.Server.Console;
using HAJE.MMO.Server.Field.AI;
using OpenTK;

namespace HAJE.MMO.Server.Field.CommandImpl
{
    public class Create : Command
    {
        public Create(ServerContext ctx)
            : base("create", "Dummy 유저를 생성합니다")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 0)
            {
                try
                {
                    var aiType = "bandit";
                    if (commandTokens.Length > 1)
                        aiType = commandTokens[1];
                    
                    AIBase ai = null;
                    if (aiType == "bandit")
                        ai = new BanditAI();
                    else if (aiType == "simple")
                        ai = new SimpleAI();
                    else
                    {
                        WriteConsole("올바른 ai패턴을 입력하세요");
                    }

                    var player = ctx.FieldServer.FieldServerLogic.PlayerManager.CreateDummyPlayer(ai, Vector2.Zero);
                    WriteConsole("Create {0}: {1}", aiType, player.Id);
                }
                catch (Exception ex)
                {
                    WriteConsole(ex.Message);
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: ");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
