﻿using System;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Field.CommandImpl
{
    public class Teleport : Command
    {
        public Teleport(ServerContext ctx)
            : base("teleport", "Player를 특정 위치로 전송시킵니다")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 3)
            {
                string playerId = commandTokens[1];
                try
                {
                    WriteConsole("Move Player {0} ", playerId);
                    var networkContext = ctx.FieldServer.FieldServerLogic.NetworkContext;
                    var playerManager = ctx.FieldServer.FieldServerLogic.PlayerManager;
                    var player = playerManager.FindPlayer(int.Parse(playerId));
                    player.PositionInfo.Position = 
                        new OpenTK.Vector2(float.Parse(commandTokens[2]), float.Parse(commandTokens[3]));
                    // TODO: 미완성: 없어지는 지점과 나타나는 지점에 대해서 다 sync 맞춰야한다
                    using (var moveSync = new MoveSync(player.PositionInfo))
                    {
                        moveSync.CharacterId = player.Id;
                        networkContext.StreamToPlayer(player, moveSync, GamePlayConstant.CharacterSyncRange);
                    }
                }
                catch (Exception ex)
                {
                    WriteConsole(ex.Message);
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: PlayerId, pos_x, pos_y");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
