﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Field.CommandImpl
{
    public class Find : Command
    {
        public Find(ServerContext ctx)
            : base("find", "해당 id를 가진 Player의 위치를 출력합니다")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                string userId = commandTokens[1];
                try
                {
                    WriteConsole("Find Player {0} ", userId);
                    var player = ctx.FieldServer.FieldServerLogic.PlayerManager.FindPlayer(int.Parse(userId));
                    var position = player.PositionInfo.Position;
                    ctx.CommandLog.Write("Field: {0}, Pos: ({1}, {2})",
                        player.FieldObject.Field.Id, position.X, position.Y);
                }
                catch (Exception ex)
                {
                    WriteConsole(ex.Message);
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: PlayerId");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
