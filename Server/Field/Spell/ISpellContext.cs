﻿using System.Collections.Generic;
using OpenTK;
using HAJE.MMO.Spell;

namespace HAJE.MMO.Server.Field.Spell
{
    /// <summary>
    /// 주문 로직이 의존하는 인터페이스
    /// 주문 로직은 여기서 제공되는 인터페이스를 통해서만 구현된다.
    /// 
    /// 인터페이스가 비대해지면 여러 인터페이스들의 집합으로 쪼갠다.
    /// </summary>
    public interface ISpellContext
    {
        void Log(string format, params object[] args);
        void Warning(string format, params object[] args);

        void CancelCast(PlayerHandle caster);
        void EndCast(PlayerHandle caster, SpellId spellId, SpellTarget target);

        bool QueryPlayerIsDummy(PlayerHandle player);
        bool QueryPlayerIsAlive(PlayerHandle player);
        int QueryMana(PlayerHandle player);
        int QueryComboPoint(PlayerHandle player, ElementType elementType);
        int QuerySpellCount(PlayerHandle player, SpellId spellId);
        Vector3 QueryPlayerDirection(PlayerHandle player);
        PositionInField QueryPlayerPosition(PlayerHandle player, AttachmentPoint attachmentPoint);
        PositionInField QueryPredictedPosition(PlayerHandle player, float updateTime);
        List<PlayerHandle> QueryAroundPlayer(PositionInField position, float radius);
        PositionInField QuerySpellPosition(SpellObjectHandle handle);
        List<PlayerHandle> QuerySpellExpectCollision(SpellObjectHandle handle, Vector3 velocity, float radius, Second updateTime);

        void ConsumeMana(PlayerHandle player, int cost);
        void ConsumeComboPoint(PlayerHandle player, ElementType elementType, int cost);
        void GainComboPoint(PlayerHandle player, ElementType elementType, int point);
        void ConsumeSpellCount(PlayerHandle player, SpellId spellId);
        void Damage(PlayerHandle caster, PlayerHandle target, int damage);

        EffectHandle CreateEffect(EffectType effectType, SpellTarget target);
        EffectHandle CreateEffect(EffectType effectType, PositionInField position);
        EffectHandle CreateEffect(EffectType effectType, SpellObjectHandle spellObject);
        EffectHandle CreateEffect(EffectType effectType, PlayerHandle player, AttachmentPoint attachmentPoint);
        void MoveEffect(EffectHandle effect, SpellTarget target, Second duration);
        void MoveEffect(EffectHandle effect, PositionInField position, Second duration);
        void MoveEffect(EffectHandle effect, PlayerHandle player, AttachmentPoint attachmentPoint, Second duration);
        void AutoDeleteEffect(EffectHandle effect, Second delay);
        void RemoveEffect(EffectHandle effect);

        void CallDelayed(DelayedSpellHandler handler, Second time);

        SpellObjectHandle CreateSpellObject(PlayerHandle playerHandle, PositionInField position);
        void UpdateSpellObject(SpellObjectHandle handle, SpellObjectHandler handler, Second updateTime);
        void StopUpdateSpellObject(SpellObjectHandle handle);
        void MoveSpellObject(SpellObjectHandle spellObject, SpellTarget target, Second duration);
        void MoveSpellObject(SpellObjectHandle spellObject, PositionInField position, Second duration);
        void RemoveSpellObject(SpellObjectHandle spellObject);

        float DistanceBetween(PositionInField pos1, PositionInField pos2);
        float DistanceBetween(PositionInField position, PlayerHandle target);
        float DistanceBetween(PlayerHandle player1, PlayerHandle player2);
        float DistanceBetween(PlayerHandle caster, SpellTarget target);
    }

    public delegate void DelayedSpellHandler(Second deltaTime);
    public delegate void SpellObjectHandler(Second deltaTime);
}