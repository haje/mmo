﻿namespace HAJE.MMO.Server.Field.Spell
{
    // 로직 안에서는 특정 객체를 직접 참조하지 않는다.
    // 핸들을 들고 핸들을 통해 실제 객체를 조회하고 변경한다.

    public struct PlayerHandle
    {
        public PlayerHandle(int id)
        {
            Id = id;
        }

        public readonly int Id;
    }

    public struct EffectHandle
    {
        public EffectHandle(int id)
        {
            Id = id;
        }

        public readonly int Id;
    }

    public struct SpellObjectHandle
    {
        public SpellObjectHandle(int id)
        {
            Id = id;
        }

        public readonly int Id;
    }
}
