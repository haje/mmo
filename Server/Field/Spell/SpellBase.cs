﻿using HAJE.MMO.Spell;

namespace HAJE.MMO.Server.Field.Spell
{
    public abstract class SpellBase
    {
        public SpellBase(SpellId spellId)
        {
            SpellData = SpellInfo.GetInfo(spellId);
        }

        public virtual void HandleCastEnd(ISpellContext context, PlayerHandle caster, SpellTarget target)
        {
            bool isNeedCheck = !context.QueryPlayerIsDummy(caster);
            if (isNeedCheck)
            {
                if (!CheckSpellEnable(context, caster))
                {
                    context.CancelCast(caster);
                    return;
                }
                ConsumeSpellCost(context, caster);
            }
            CastSpell(context, caster, target);
            context.EndCast(caster, SpellData.Id, target);
        }

        protected virtual bool CheckSpellEnable(ISpellContext context, PlayerHandle caster)
        {
            if (context.QueryMana(caster) < SpellData.ManaCost) return false;
            if (context.QueryComboPoint(caster, SpellData.ElementType) < SpellData.ComboPointCost) return false;
            if (SpellData.GainType != GainType.Basic && context.QuerySpellCount(caster, SpellData.Id) <= 0) return false;
            // 모든 주문에 공용으로 적용될 사용 조건이 있다면 이곳에 추가
            // 모든 주문에 공용으로 적용되지 않는다면 이 메서드를 재정의
            return true;
        }

        protected virtual void ConsumeSpellCost(ISpellContext context, PlayerHandle caster)
        {
            context.ConsumeMana(caster, SpellData.ManaCost);
            context.ConsumeComboPoint(caster, SpellData.ElementType, SpellData.ComboPointCost);
            if (SpellData.GainType != GainType.Basic) context.ConsumeSpellCount(caster, SpellData.Id);
            // 모든 주문이 공용으로 소모할 자원 처리가 있다면 이곳에 추가
            // 모든 주문에 공용으로 적용되지 않는다면 이 메서드를 재정의
        }

        protected abstract void CastSpell(ISpellContext context, PlayerHandle caster, SpellTarget target);

        protected readonly SpellInfo SpellData;
        public SpellId Id
        {
            get
            {
                return SpellData.Id;
            }
        }
    }
}
