﻿namespace HAJE.MMO.Server.Field.Spell.Impl
{
    public class ArcaneMissile : SpellBase
    {
        public ArcaneMissile()
            : base(MMO.Spell.SpellId.ArcaneMissile)
        { }

        protected override void CastSpell(ISpellContext context, PlayerHandle caster, SpellTarget target)
        {
            var projectile = context.CreateEffect(EffectType.ArcaneMissile, caster, AttachmentPoint.MagicWand);
            var distance = context.DistanceBetween(caster, target);
            // 임시 땜빵!
            // 아무래도 서버 들어가면서 타게팅하면 생기는듯하다
            if (float.IsNaN(distance))
                return;

            var duration = (Second)(distance / SpellData.Speed);
            context.MoveEffect(projectile, target, duration);

            context.CallDelayed(delegate {
                if (target.TargetType == TargetType.Player)
                {
                    var targetPlayer = target.TargetPlayer;
                    if (context.QueryPlayerIsAlive(targetPlayer))
                        context.Damage(caster, targetPlayer, SpellData.Damage);
                }
                context.RemoveEffect(projectile);

                if (target.TargetType != TargetType.Air)
                {
                    var hitEffect = context.CreateEffect(EffectType.ArcaneHit, target);
                    context.AutoDeleteEffect(hitEffect, (Second)1.0f); // TODO: 이거 effect delay를 어디서 가져오지?
                }
            }, duration);

            context.GainComboPoint(caster, SpellData.ElementType, 1);
        }
    }
}
