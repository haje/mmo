﻿namespace HAJE.MMO.Server.Field.Spell.Impl
{
    public class ArcaneExplosion : SpellBase
    {
        public ArcaneExplosion()
            : base(MMO.Spell.SpellId.ArcaneExplosion)
        { }

        protected override void CastSpell(ISpellContext context, PlayerHandle caster, SpellTarget target)
        {
            var effectHandle = context.CreateEffect(EffectType.ArcaneExplosion, target);
            context.CallDelayed(delegate {
                var playerList = context.QueryAroundPlayer(target.TargetPosition, SpellData.Radius);
                foreach (var player in playerList)
                {
                    context.Damage(caster, player, SpellData.Damage);
                    var hitEffect = context.CreateEffect(EffectType.ArcaneHit, player, AttachmentPoint.Foot);
                    context.AutoDeleteEffect(hitEffect, (Second)1.0f);
                }
            }, SpellData.DelayTime);
        }
    }
}
