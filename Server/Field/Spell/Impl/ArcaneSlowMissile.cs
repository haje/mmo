﻿namespace HAJE.MMO.Server.Field.Spell.Impl
{
    public class ArcaneSlowMissile : SpellBase
    {
        public ArcaneSlowMissile()
            : base(MMO.Spell.SpellId.ArcaneSlowMissile)
        { }

        protected override void CastSpell(ISpellContext context, PlayerHandle caster, SpellTarget target)
        {
            Second updateTime = SpellData.UpdateTime;
            var castDir = context.QueryPlayerDirection(caster);
            var castPos = context.QueryPlayerPosition(caster, AttachmentPoint.MagicWand);
            var spellObject = context.CreateSpellObject(caster, castPos);
            context.CreateEffect(EffectType.ArcaneMissile, spellObject);
            
            context.GainComboPoint(caster, SpellData.ElementType, 1);
            context.UpdateSpellObject(spellObject, delegate {
                var spellPos = context.QuerySpellPosition(spellObject);
                if (SpellData.Range < (spellPos.Position - castPos.Position).Length)
                {
                    context.RemoveSpellObject(spellObject);
                    return;
                }
                
                PlayerHandle nearPlayer = new PlayerHandle(-1);
                var moveVec = castDir * (SpellData.Speed * updateTime);
                var nextPos = new PositionInField(spellPos.FieldId, spellPos.Position + moveVec);

                var targetList = context.QuerySpellExpectCollision(spellObject,
                    castDir * SpellData.Speed, SpellData.Radius, updateTime);
                foreach (var player in targetList)
                {
                    nearPlayer = player; // 일단 한명만 맞도록
                    break;
                }
                
                if (nearPlayer.Id > 0)
                {
                    var targetPos = context.QueryPlayerPosition(nearPlayer, AttachmentPoint.Origin);
                    var remainTime = (Second)((targetPos.Position - spellPos.Position).Length / SpellData.Speed);
                    context.StopUpdateSpellObject(spellObject);
                    context.MoveSpellObject(spellObject, 
                        new SpellTarget(TargetType.Player, targetPos, nearPlayer.Id),
                        remainTime);
                    context.CallDelayed(delegate {
                        context.Damage(caster, nearPlayer, SpellData.Damage);
                        context.RemoveSpellObject(spellObject);
                        var hitEffect = context.CreateEffect(EffectType.ArcaneHit, nearPlayer, AttachmentPoint.Origin);
                        context.AutoDeleteEffect(hitEffect, (Second)1.0f);
                    }, remainTime);
                }
                else
                    context.MoveSpellObject(spellObject, nextPos, updateTime);
            }, updateTime);
        }
    }
}
