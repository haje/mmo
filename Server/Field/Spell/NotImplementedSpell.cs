﻿using HAJE.MMO.Spell;

namespace HAJE.MMO.Server.Field.Spell
{
    public class NotImplementedSpell : SpellBase
    {
        public NotImplementedSpell(SpellId spellId)
            : base(spellId)
        {
        }

        protected override void CastSpell(ISpellContext context, PlayerHandle caster, SpellTarget target)
        {
            context.Log("{0}: 구현 되지 않은 주문을 시전하려 했습니다.", SpellData.Name);
        }
    }
}
