﻿using System;
using System.Collections.Generic;
using OpenTK;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Spell;
using HAJE.MMO.Server.Field.Object;

namespace HAJE.MMO.Server.Field.Spell
{
    public class SpellContext : ISpellContext
    {
        public SpellContext(FieldLogic owner)
        {
            this.logDispatcher = owner.LogDispatcher;
            this.fieldScheduler = owner.FieldScheduler;
            this.networkContext = owner.NetworkContext;
            this.fieldManager = owner.FieldManager;
            this.effectManager = owner.EffectManager;
            this.playerManager = owner.PlayerManager;
        }

        public void Log(string format, params object[] args)
        {
            logDispatcher.Write(format, args);
        }

        public void Warning(string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void CancelCast(PlayerHandle casterHandler)
        {
            var caster = playerManager.FindPlayer(casterHandler.Id);
            if (caster == null) return;

            var cancelCastSync = networkContext.GetPacket<CancelCastSync>();
            cancelCastSync.CharacterId = caster.Id;

            networkContext.StreamToPlayer(caster, cancelCastSync, GamePlayConstant.CharacterSyncRange);
        }

        public void EndCast(PlayerHandle casterHandler, SpellId spellId, SpellTarget target)
        {
            var caster = playerManager.FindPlayer(casterHandler.Id);
            if (caster == null) return;

            var endCastSync = networkContext.GetPacket<EndCastSync>();
            endCastSync.CharacterId = caster.Id;
            endCastSync.SpellId = spellId;
            endCastSync.TargetType = target.TargetType;
            endCastSync.TargetId = target.TargetId;
            endCastSync.TargetFieldId = target.TargetPosition.FieldId;
            endCastSync.TargetPosition = target.TargetPosition.Position;

            networkContext.StreamToPlayer(caster, endCastSync, GamePlayConstant.CharacterSyncRange);
        }

        public bool QueryPlayerIsDummy(PlayerHandle playerHandle)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            return player == null ? false : player.IsDummy;
        }

        public bool QueryPlayerIsAlive(PlayerHandle playerHandle)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            return player == null ? false : !player.IsDead;
        }

        public int QueryMana(PlayerHandle playerHandle)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            return player == null ? 0 : (int)player.MP;
        }

        public int QueryComboPoint(PlayerHandle playerHandle, ElementType elementType)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            return player == null ? 0 : player.ComboPoint(elementType);
        }

        public int QuerySpellCount(PlayerHandle playerHandle, SpellId spellId)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            return player == null ? 0 : player.SpellCount(spellId);
        }

        public Vector3 QueryPlayerDirection(PlayerHandle playerHandle)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            return player == null ? Vector3.Zero : player.PositionInfo.Direction;
        }

        public PositionInField QueryPlayerPosition(PlayerHandle playerHandle, AttachmentPoint attachmentPoint)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null)
                return new PositionInField(-1, new Vector2(0, 0));
            
            var vec3 = player.FieldObject.GetPos3();
            if (attachmentPoint == AttachmentPoint.MagicWand)
            {
                var dir = player.PositionInfo.Direction;
                dir.Y = 0;
                dir.Normalize();
                vec3 += dir * 1.0f;
                vec3.Y += 0.4f;
                // TODO: attachmentPoint Position 코드고 공용코드가 있어야...?
            }
            var playerPos = new PositionInField(player.FieldObject.Field.Id, vec3);
            return playerPos;
        }

        public PositionInField QueryPredictedPosition(PlayerHandle playerHandle, float updateTime)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null)
                return new PositionInField(-1, new Vector2(0, 0));

            var dir = player.PositionInfo.Direction.Xz.Normalized();
            var vec2 = player.PositionInfo.Position + player.PositionInfo.Velocity * dir;
            var terrainY = player.FieldObject.Field.HeightMap.GetHeight(vec2);
            var vec3 = new Vector3(vec2.X, terrainY, vec2.Y);
            var playerPos = new PositionInField(player.FieldObject.Field.Id, vec3);
            return playerPos;
        }

        public List<PlayerHandle> QueryAroundPlayer(PositionInField position, float radius)
        {
            var list = new List<PlayerHandle>();
            var field = fieldManager.FindField(position.FieldId);
            if (field == null) return list;

            var objList = new List<FieldObject>();
            field.Query(position.Position.Xz, radius, FieldObjectType.Player, objList);
            foreach (var obj in objList)
            {
                var player = obj.Owner as Player;
                list.Add(new PlayerHandle(player.Id));
            }

            return list;
        }

        public PositionInField QuerySpellPosition(SpellObjectHandle handle)
        {
            if (spellDict.ContainsKey(handle.Id) == false)
                return new PositionInField(-1, new Vector2(0, 0));

            var spellObject = spellDict[handle.Id];
            return spellObject.Position;
        }

        public List<PlayerHandle> QuerySpellExpectCollision(
            SpellObjectHandle handle, Vector3 velocity, float radius, Second updateTime)
        {
            var list = returnPlayerList;
            list.Clear();
            if (spellDict.ContainsKey(handle.Id) == false)
                return list;

            var spellObject = spellDict[handle.Id];
            Vector3 spellStartPos = spellObject.Position.Position;
            Vector3 spellEndPos = spellStartPos + velocity * updateTime;
            Vector3 spellMidPos = (spellStartPos + spellEndPos) / 2;
            var field = fieldManager.FindField(spellObject.Position.FieldId);
            if (field == null) return list;

            // y = ax + b, b = y - ax, ax - y + b = 0
            var vel2 = velocity.Xz;
            var spellStartPos2 = spellStartPos.Xz;
            var spellEndPos2 = spellEndPos.Xz;
            float a = (spellEndPos2.Y - spellStartPos2.Y) / (spellEndPos2.X - spellStartPos2.X);
            float a2 = 1 / a;
            float b = spellStartPos2.Y - a * spellStartPos2.X;
            float sqVal = (float)Math.Sqrt(a * a + 1);

            float searchRange = (spellMidPos.Xz - spellStartPos.Xz).Length + radius + GamePlayConstant.RunSpeed * updateTime;
            field.Query(spellMidPos.Xz, searchRange, FieldObjectType.Player, fieldObjectList);
            foreach (var obj in fieldObjectList)
            {
                var player = obj.Owner as Player;
                if (player.Id == spellObject.CasterId) continue;
                var start = obj.Position;
                var end = start + player.PositionInfo.Velocity * updateTime;
                if ((end - spellEndPos2).Length <= radius) // next spell 도착 범위에 있는 경우
                {
                    var terrainY = player.FieldObject.Field.HeightMap.GetHeight(end) + 0.8f; // TODO: fix Player height 보정...
                    float collsitionDistance = (spellEndPos - new Vector3(end.X, terrainY, end.Y)).Length;
                    if (collsitionDistance <= radius)
                    {
                        list.Add(new PlayerHandle(player.Id));
                    }
                }
                else
                {
                    var mid = (start + end) / 2;
                    float distance = Math.Abs(a * mid.X - mid.Y + b) / sqVal;
                    if (distance <= radius) // next spell 경로에 있는 경우
                    {
                        float b2 = mid.Y - a2 * mid.X;
                        var spx = (-b + b2) / (a - a2);
                        var spy = a * spx + b;
                        var terrainY = player.FieldObject.Field.HeightMap.GetHeight(new Vector2(spx, spy)) + 0.8f;

                        float collsitionDistance = (spellMidPos - new Vector3(spx, terrainY, spy)).Length;
                        if (collsitionDistance <= radius)
                        {
                            list.Add(new PlayerHandle(player.Id));
                        }
                    }
                }
            }
            
            return list;
        }

        public void ConsumeMana(PlayerHandle playerHandle, int cost)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null) return;
            player.ConsumeMana(cost);
        }

        public void ConsumeComboPoint(PlayerHandle playerHandle, ElementType elementType, int cost)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null) return;
            player.ConsumeComboPoint(elementType, cost);
        }

        public void GainComboPoint(PlayerHandle playerHandle, ElementType elementType, int point)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null) return;
            player.GainComboPoint(elementType, point);
        }

        public void ConsumeSpellCount(PlayerHandle playerHandle, SpellId spellId)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null) return;
            player.ConsumeSpellCount(spellId);
        }

        public void Damage(PlayerHandle casterHandle, PlayerHandle targetHandle, int damage)
        {
            var caster = playerManager.FindPlayer(casterHandle.Id);
            var target = playerManager.FindPlayer(targetHandle.Id);
            if (caster == null || target == null) return;
            target.Damage(caster, damage);
        }

        public EffectHandle CreateEffect(EffectType effectType, SpellTarget target)
        {
            var createEffect = networkContext.GetPacket<CreateEffect>();
            createEffect.TargetType = target.TargetType;
            if (target.TargetType == TargetType.Player)
            {
                createEffect.TargetId = target.TargetPlayer.Id;
                createEffect.AttachmentPoint = AttachmentPoint.Origin;
            }

            var effect = effectManager.CreateEffect(effectType, GetTargetPosition(target), createEffect);
            return new EffectHandle(effect == null ? -1 : effect.Id);
        }

        public EffectHandle CreateEffect(EffectType effectType, PositionInField position)
        {
            var field = fieldManager.FindField(position.FieldId);
            if (field == null) return new EffectHandle(-1);

            var effect = effectManager.CreateEffect(effectType, position);
            return new EffectHandle(effect == null ? -1 : effect.Id);
        }

        public EffectHandle CreateEffect(EffectType effectType, SpellObjectHandle spellObjectHandle)
        {
            if (spellDict.ContainsKey(spellObjectHandle.Id) == false) return new EffectHandle(-1);
            var spellObject = spellDict[spellObjectHandle.Id];
            var effectHandle = CreateEffect(effectType, spellObject.Position);
            spellObject.EffectList.Add(effectManager.FindEffect(effectHandle.Id));
            return effectHandle;
        }

        public EffectHandle CreateEffect(EffectType effectType, PlayerHandle playerHandle, AttachmentPoint attachmentPoint)
        {
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null) return new EffectHandle(-1);
            
            var createEffect = networkContext.GetPacket<CreateEffect>();
            createEffect.TargetType = TargetType.Player;
            createEffect.TargetId = player.Id;
            createEffect.AttachmentPoint = attachmentPoint;

            var position = new PositionInField(player.FieldObject.Field.Id, player.FieldObject.Position);
            var effect = effectManager.CreateEffect(effectType, position, createEffect);
            return new EffectHandle(effect.Id);
        }

        public void MoveEffect(EffectHandle effectHandle, SpellTarget target, Second duration)
        {
            var effect = effectManager.FindEffect(effectHandle.Id);
            if (effect == null) return;

            var moveEffect = networkContext.GetPacket<MoveEffect>();
            moveEffect.TargetType = target.TargetType;
            if (target.TargetType == TargetType.Player)
                moveEffect.TargetId = target.TargetPlayer.Id;

            effectManager.MoveEffect(effect.Id, GetTargetPosition(target), duration, moveEffect);
        }

        public void MoveEffect(EffectHandle effectHandle, PositionInField position, Second duration)
        {
            var effect = effectManager.FindEffect(effectHandle.Id);
            if (effect == null) return;
            
            effectManager.MoveEffect(effect.Id, position, duration);
        }

        public void MoveEffect(EffectHandle effectHandle, PlayerHandle playerHandle,
            AttachmentPoint attachmentPoint, Second duration)
        {
            var effect = effectManager.FindEffect(effectHandle.Id);
            if (effect == null) return;
            var player = playerManager.FindPlayer(playerHandle.Id);
            if (player == null) return;
            
            effectManager.MoveEffect(effect.Id, player, attachmentPoint, duration);
        }

        public void AutoDeleteEffect(EffectHandle effectHandle, Second delay)
        {
            CallDelayed(delegate {
                effectManager.RemoveEffect(effectHandle.Id, false);
            }, delay);
        }

        public void RemoveEffect(EffectHandle effectHandle)
        {
            effectManager.RemoveEffect(effectHandle.Id, true);
        }

        public void CallDelayed(DelayedSpellHandler handler, Second time)
        {
            fieldScheduler.ScheduleOnce(delegate() { handler(time); }, time);
        }

        public SpellObjectHandle CreateSpellObject(PlayerHandle playerHandle, PositionInField position)
        {
            var field = fieldManager.FindField(position.FieldId);
            if (field == null) return new SpellObjectHandle(-1);

            var spellObject = new SpellObject(++lastSpellObjectId, playerHandle.Id, position);
            spellDict.Add(spellObject.Id, spellObject);
            var handle = new SpellObjectHandle(spellObject.Id);
            return handle;
        }

        public void UpdateSpellObject(SpellObjectHandle handle, SpellObjectHandler handler, Second updateTime)
        {
            if (spellDict.ContainsKey(handle.Id) == false) return;
            if (spellDict[handle.Id].OnUpdate == false) return;
            handler(updateTime);
            CallDelayed(delegate { UpdateSpellObject(handle, handler, updateTime); }, updateTime);
        }

        public void StopUpdateSpellObject(SpellObjectHandle handle)
        {
            if (spellDict.ContainsKey(handle.Id) == false) return;
            spellDict[handle.Id].OnUpdate = false;
        }

        public void MoveSpellObject(SpellObjectHandle handle, SpellTarget target, Second duration)
        {
            if ((spellDict.ContainsKey(handle.Id)) == false) return;
            var spellObject = spellDict[handle.Id];
            foreach (var effect in spellObject.EffectList)
            {
                MoveEffect(new EffectHandle(effect.Id), target, duration);
            }
        }

        public void MoveSpellObject(SpellObjectHandle handle, PositionInField position, Second duration)
        {
            if ((spellDict.ContainsKey(handle.Id)) == false) return;
            var spellObject = spellDict[handle.Id];
            spellObject.Position = position;
            foreach (var effect in spellObject.EffectList)
            {
                MoveEffect(new EffectHandle(effect.Id), position, duration);
            }
        }

        public void RemoveSpellObject(SpellObjectHandle handle)
        {
            if (spellDict.ContainsKey(handle.Id))
            {
                var spellObject = spellDict[handle.Id];
                foreach (var effect in spellObject.EffectList)
                {
                    RemoveEffect(new EffectHandle(effect.Id));
                }
                spellObject.EffectList.Clear();
                spellDict.Remove(handle.Id);
            }
        }

        public float DistanceBetween(PositionInField pos1, PositionInField pos2)
        {
            return (pos1.Position - pos2.Position).Length;
        }

        public float DistanceBetween(PositionInField position, PlayerHandle target)
        {
            var player = playerManager.FindPlayer(target.Id);
            if (player == null) return float.MaxValue;

            var playerPos = new PositionInField(player.FieldObject.Field.Id, player.PositionInfo.Position);
            return DistanceBetween(position, playerPos);
        }

        public float DistanceBetween(PlayerHandle playerHandle1, PlayerHandle playerHandle2)
        {
            var player1 = playerManager.FindPlayer(playerHandle1.Id);
            var player2 = playerManager.FindPlayer(playerHandle1.Id);
            if (player1 == null || player2 == null) return float.MaxValue;

            var playerPos1 = new PositionInField(player1.FieldObject.Field.Id, player1.FieldObject.Position);
            var playerPos2 = new PositionInField(player2.FieldObject.Field.Id, player2.FieldObject.Position);
            return DistanceBetween(playerPos1, playerPos2);
        }

        public float DistanceBetween(PlayerHandle casterHandler, SpellTarget target)
        {
            var caster = playerManager.FindPlayer(casterHandler.Id);
            if (caster == null) return 0;

            var casterPos = new PositionInField(caster.FieldObject.Field.Id, caster.FieldObject.Position);
            var targetPos = target.TargetPosition;
            if (target.TargetType == TargetType.Player)
            {
                var targetPlayer = playerManager.FindPlayer(target.TargetPlayer.Id);
                if (targetPlayer == null)
                    return 0.0f;
                targetPos = new PositionInField(targetPlayer.FieldObject.Field.Id, targetPlayer.PositionInfo.Position);
            }

            return DistanceBetween(casterPos, targetPos);
        }

        private void GetTargetVar(SpellTarget target, out Base.Field field, out PositionInField position, out Vector2 pos2)
        {
            field = null;
            position = target.TargetPosition;
            pos2 = Vector2.Zero;
            switch (target.TargetType)
            {
                case TargetType.Air:
                case TargetType.Ground:
                case TargetType.Doodad:
                    field = fieldManager.FindField(position.FieldId);
                    pos2 = position.Position.Xz;
                    break;
                case TargetType.Player:
                    {
                        var player = playerManager.FindPlayer(target.TargetPlayer.Id);
                        if (player == null) break;
                        field = fieldManager.FindField(player.FieldObject.Field.Id);
                        pos2 = player.FieldObject.Position;
                        position = new PositionInField(field.Id, pos2);
                    }
                    break;
                default:
                    break;
            }
        }

        private PositionInField GetTargetPosition(SpellTarget target)
        {
            PositionInField position = target.TargetPosition;
            if (target.TargetType == TargetType.Player)
            {
                var player = playerManager.FindPlayer(target.TargetPlayer.Id);
                if (player != null)
                    position = player.FieldObject.GetPosInField();
            }
            return position;
        }

        LogDispatcher logDispatcher;
        Scheduler fieldScheduler;
        FieldNetworkContext networkContext;
        FieldManager fieldManager;
        EffectManager effectManager;
        PlayerManager playerManager;
        
        int lastSpellObjectId = 0;
        Dictionary<int, SpellObject> spellDict = new Dictionary<int, SpellObject>();

        List<FieldObject> fieldObjectList = new List<FieldObject>();
        List<PlayerHandle> returnPlayerList = new List<PlayerHandle>();
    }
}
