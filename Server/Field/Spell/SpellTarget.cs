﻿using System.Diagnostics;

namespace HAJE.MMO.Server.Field.Spell
{
    public struct SpellTarget
    {
        public SpellTarget(TargetType type, PositionInField position)
            : this()
        {
            TargetType = type;
            TargetPosition = position;
            TargetId = 0;

            Debug.Assert(type == TargetType.Air|| type == TargetType.Ground);
        }

        public SpellTarget(TargetType type, PositionInField position, int id)
            : this()
        {
            TargetType = type;
            TargetPosition = position;
            TargetId = id;
        }

        public PositionInField TargetPosition { get; private set; }
        public TargetType TargetType { get; private set; }
        public int TargetId;

        public PlayerHandle TargetPlayer
        {
            get
            {
                Debug.Assert(TargetType == TargetType.Player);
                return new PlayerHandle(TargetId);
            }
        } 
    }
}
