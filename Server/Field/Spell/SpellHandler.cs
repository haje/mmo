﻿using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Spell;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace HAJE.MMO.Server.Field.Spell
{
    /// <summary>
    /// 필드 서버 기반 코드에서 주문 처리 로직으로의 진입점
    /// 기반 코드에서 사용하는 형식들을 인자로 받아
    /// 주문 처리 로직에서 처리 할 수 있는 형태로 가공 적절하게 주문 처리 로직과 이어주자.
    /// </summary>
    public class SpellHandler
    {
        public SpellHandler(ISpellContext spellContext)
        {
            context = spellContext;

            var handlerTypes = FindAllHandlerTypes();
            RegisterHandlers(handlerMap, handlerTypes);
            FillNullHandlers(handlerMap, spellContext);
        }

        #region initialize helpers

        static IEnumerable<Type> FindAllHandlerTypes()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var handlerTypes = assembly.GetTypes()
                .Where(t => t.BaseType == typeof(SpellBase)
                    && t != typeof(NotImplementedSpell));
            return handlerTypes;
        }

        static void RegisterHandlers(Dictionary<SpellId, SpellBase> handlerMap, IEnumerable<Type> handlerTypes)
        {
            foreach (var t in handlerTypes)
            {
                var ctor = t.GetConstructor(Type.EmptyTypes);
                var obj = ctor.Invoke(new object[0]);
                var handler = obj as SpellBase;
                handlerMap.Add(handler.Id, handler);

                Debug.Assert(handler.Id.ToString() == t.Name);
            }
        }

        static void FillNullHandlers(Dictionary<SpellId, SpellBase> handlerMap, ISpellContext context)
        {
            foreach (var id in SpellIds.All)
            {
                if (!handlerMap.ContainsKey(id))
                {
                    var nullHandler = new NotImplementedSpell(id);
                    handlerMap.Add(id, nullHandler);

                    context.Log("주문 {0}에 대한 핸들러를 찾을 수 없어서 임시 객체가 생성되었습니다.",
                        SpellInfo.GetInfo(id).Name);
                }
            }
        }

        #endregion

        public void HandleSpellCastEnd(Player caster, SpellId spellId, SpellTarget spellTarget)
        {
            var casterHandle = new PlayerHandle(caster.Id);
            handlerMap[spellId].HandleCastEnd(context, casterHandle, spellTarget);
        }

        public void HandleSpellCastEnd(Player caster, EndCastSync castingInfo)
        {
            var targetPosition = TargetPosition(castingInfo);
            var targetId = castingInfo.TargetId;
            var targetBeforeCorrection = new SpellTarget(castingInfo.TargetType, targetPosition, targetId);
            var target = CorrectSpellTarget(caster, targetBeforeCorrection);
            HandleSpellCastEnd(caster, castingInfo.SpellId, target);
        }

        #region target helpers

        PositionInField TargetPosition(EndCastSync castingInfo)
        {
            return new PositionInField(
                castingInfo.TargetFieldId,
                castingInfo.TargetPosition
            );
        }

        SpellTarget CorrectSpellTarget(Player caster, SpellTarget target)
        {
            //TODO: 사거리 체크, 없는 플레이어 체크 등 서버쪽에서도 한번 더 유효성 검증
            return target;
        }

        #endregion

        ISpellContext context;
        Dictionary<SpellId, SpellBase> handlerMap = new Dictionary<SpellId, SpellBase>();
    }
}
