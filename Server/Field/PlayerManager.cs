﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;

using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Server.Field.AI;

namespace HAJE.MMO.Server.Field
{
    public class PlayerManager
    {
        public PlayerManager(FieldLogic owner)
        {
            this.owner = owner;
            packetDispatcher = owner.PacketDispatcher;
            networkContext = owner.NetworkContext;
            PlayerDict = new Dictionary<int, Player>();
        }
        
        public void Update(Second deltaTime)
        {
            lock (PlayerDict)
            {
                foreach (KeyValuePair<int, Player> pair in PlayerDict)
                {
                    Player player = pair.Value;
                    if (player.FieldObject == null)
                        continue;
                    player.Update(deltaTime, owner.LogDispatcher);
                }
            }
        }

        public void AllocPlayerCallback(Player newPlayer, Vector2 pos)
        {
            owner.FieldManager.AllocPlayerToField(newPlayer, pos);

            PositionInfo positionInfo = new PositionInfo();
            positionInfo.FieldId = newPlayer.FieldObject.Field.Id;
            positionInfo.Position = newPlayer.FieldObject.Position;
            positionInfo.Velocity = new Vector2(0, 0);
            positionInfo.Direction = new Vector3(1, 0, 0);

            PositionInfo remotePosInfo = new PositionInfo();
            remotePosInfo.FieldId = newPlayer.FieldObject.Field.Id;
            remotePosInfo.Velocity = new Vector2(0, 0);
            remotePosInfo.Direction = new Vector3(1, 0, 0);

            var response = new NewCharacterRepsonse(newPlayer.Id);
            newPlayer.SendMessage(response);
            var hostEncount = new RespondCharacter(TargetType.Player, newPlayer.Id, newPlayer.Name, positionInfo);


            List<FieldObject> objList = new List<FieldObject>();
            newPlayer.Query(GamePlayConstant.CharacterSyncRange, FieldObjectType.Player, objList);// TODO: query with FieldObjectType?
            foreach (var obj in objList)
            {
                Debug.Assert(obj.OwnerType == FieldObjectType.Player); //자신포함
                var remotePlayer = obj.Owner as Player;
                remotePlayer.SendMessage(hostEncount);

                if (newPlayer != remotePlayer) // TODO: 생략?
                {
                    remotePosInfo.Position = remotePlayer.FieldObject.Position;
                    var remoteEncount = new RespondCharacter(TargetType.Player, remotePlayer.Id, remotePlayer.Name, remotePosInfo);
                    newPlayer.SendMessage(remoteEncount);

                }
            }
        }

        public void CreatePlayer(SslStreamConnection connection, int id, string token)
        {
            Player newPlayer = null;
            lock (PlayerDict)
            {
                if (PlayerDict.ContainsKey(id))
                {
                    var oldPlayer = PlayerDict[id];
                    if (oldPlayer.NetConnection != null)
                        PlayerDict[id].NetConnection.Dispose();
                    oldPlayer.RemoveFromField();
                }

                newPlayer = new Player(id, token, connection, networkContext, AllocPlayerCallback);
                newPlayer.DeadPlayerEvent += DeletePlayer;
                PlayerDict[id] = newPlayer;
            }

            packetDispatcher.SetReceiveConnectionHandler(PacketType.NewCharacterRequest, newPlayer.OnNewCharacterRequest, connection);
            packetDispatcher.AttachToConnection(connection);
        }

        public Player CreateDummyPlayer(AIBase ai, Vector2 pos)
        {
            ai.SetSpellHandler(owner.SpellHandler);
            Player newPlayer = new Player(DummyIdStart--, networkContext, bufferList, ai);

            Debug.Assert(PlayerDict.ContainsKey(newPlayer.Id) == false);
            lock (PlayerDict)
            {
                newPlayer.DeadPlayerEvent += DeletePlayer;
                PlayerDict[newPlayer.Id] = newPlayer;
            }
            AllocPlayerCallback(newPlayer, pos);
            return newPlayer;
        }

        public Player FindPlayer(int id)
        {
            if (PlayerDict.ContainsKey(id))
                return PlayerDict[id];
            else
                return null;
        }

        public void KillPlayer(int id)
        {
            var targetPlayer = FindPlayer(id);
            if (targetPlayer != null)
            {
                targetPlayer.Kill();
            }
            else
            {
                throw new Exception("No Such User");
            }
        }

        public void DeletePlayer(int id, string token)
        {
            lock (PlayerDict)
            {
                var deletedPlayer = FindPlayer(id);
                if (deletedPlayer != null)
                {
                    Base.FieldBase field = deletedPlayer.FieldObject.Field;
                    Vector2 position = deletedPlayer.FieldObject.Position;

                    var deadCharacter = networkContext.GetPacket<DeadCharacter>();
                    deadCharacter.CharacterId = deletedPlayer.Id;
                    networkContext.StreamToPlayer(deletedPlayer, deadCharacter);

                    deletedPlayer.RemoveFromField();
                    PlayerDict.Remove(id);
                    owner.DropObjectManager.CreateDropObject(field, position);
                }
            }

        }

        public Dictionary<int, Player> PlayerDict { get; private set; }
        
        FieldLogic owner;
        PacketDispatcher packetDispatcher;
        FieldNetworkContext networkContext;
        
        int DummyIdStart = int.MaxValue;
        List<FieldObject> bufferList = new List<FieldObject>();
    }
}
