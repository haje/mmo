﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Spell;
using HAJE.MMO.Server.Field.Base;
using HAJE.MMO.Server.Field.Object;
using HAJE.MMO.Spell;
using System.Threading;
using HAJE.MMO.Server.Field.AI;

namespace HAJE.MMO.Server.Field
{
    public class FieldLogic
    {
        public FieldLogic(LogDispatcher logDispatcher)
        {
            this.LogDispatcher = logDispatcher;

            PacketDispatcher = new PacketDispatcher();
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.SuperCommandRequest, OnSuperCommandRequest);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.FieldRequest, OnFieldRequest);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.RequestCharacter, OnRequestCharacter);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.MoveSync, OnMoveSync);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.BeginCastSync, OnBeginCastSync);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.CancelCastSync, OnCancelCastSync);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.EndCastSync, OnEndCastSync);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.PurchaseSpell, OnPurchaseSpell);
            PacketDispatcher.SetReceiveConnectionHandler(PacketType.EquipSpell, OnEquipSpell);

            NetworkContext = new FieldNetworkContext(logDispatcher);
            FieldScheduler = new Scheduler("FieldLogicScheduler");
            FieldManager = new FieldManager(this);
            EffectManager = new EffectManager(this);
            PlayerManager = new PlayerManager(this);
            DropObjectManager = new DropObjectManager(this);
        }

        #region Threading
        public void Begin()
        {
            var spellContext = new SpellContext(this);
            SpellHandler = new SpellHandler(spellContext);
            AIManager = new AIManager(ServerContext.Instance);
            
            if (loopThread != null) return;
            loopThread = new Thread(Loop);
            loopThread.Start();
        }

        public void End()
        {
            if (loopThread == null) return;
            loopThread.Abort();
            loopThread = null;
        }

        Thread loopThread = null;
        GameTime gameTime = new GameTime();

        void Loop()
        {
            gameTime.Refresh();
            while (true)
            {
                gameTime.Refresh();
                Update(gameTime.DeltaTime);
                Thread.Yield();
            }
        }
        #endregion
        
        public void Update(Second deltaTime)
        {
            //debugTime += deltaTime;
            currentDeltaTime += deltaTime;
            if (currentDeltaTime > UpdateInterval)
            {
                PlayerManager.Update(currentDeltaTime);
                currentDeltaTime = Second.Zero;
            }
            DropObjectManager.Update(deltaTime);

            if (PacketDispatcher != null)
            {
                PacketDispatcher.Dispatch();
            }
            if (FieldScheduler != null)
            {
                FieldScheduler.Update(deltaTime);
            }
            if (AIManager != null)
            {
                AIManager.Update(deltaTime);
            }
        }

        #region PacketLogic
        //TODO: Logic 내 exception 처리...?
        public void OnSuperCommandRequest(PacketReader reader, ConnectionBase connection)
        {
            using (var request = new SuperCommandRequest())
            {
                request.Deserialize(reader);
                var ctx = ServerContext.Instance;
                Console.PromptChangeHelper.ToField(ctx.RootPrompt, ctx.FieldServer);

                ctx.CommandLog.Clear();
                ctx.RootPrompt.Child.ExecuteCommand(request.Cmd);

                var log = ctx.CommandLog.GetRecentLog(1)[0];
                if (log.Length > 0)
                {
                    connection.SendMessage(new SuperCommandResponse(log));
                }
            }
        }

        public void OnFieldRequest(PacketReader reader, ConnectionBase connection)
        {
            using (var request = new FieldRequest())
            {
                request.Deserialize(reader);
                FieldBase field = FieldManager.FindField(request.FieldId);
                if (field == null)
                {
                    Log("Client Request Not Existing Field {0}!!!", request.FieldId);
                    return;
                }

                SendFieldInfoToPlayer(field, connection);
            }
        }

        public void SendFieldInfoToPlayer(FieldBase field, ConnectionBase connection)
        {
            int[] neighbors = new int[6];
            for (FieldConnectDirection direction = FieldConnectDirection.UpperLeft;
                direction != FieldConnectDirection.MAX;
                direction = (FieldConnectDirection)((int)direction + 1))
            {
                if (field.Neighbor.ContainsKey(direction))
                    neighbors[(int)direction] = field.Neighbor[direction].Id;
                else
                    neighbors[(int)direction] = -1;
            }

            var response = new FieldResponse(field.Id, field.TerrainType, neighbors);
            connection.SendMessage(response);
        }

        public void OnRequestCharacter(PacketReader reader, ConnectionBase connection)
        {
            var requestCharacter = NetworkContext.GetPacket<RequestCharacter>();
            requestCharacter.Deserialize(reader);

            Player player = PlayerManager.FindPlayer(requestCharacter.CharacterId);
            if (player == null) { requestCharacter.Release(); return; }

            //더 많은 정보를 담을 수 있도록 수정
            var respondCharacter = new RespondCharacter(TargetType.Player,
                                                        player.Id,
                                                        player.Name,
                                                        player.PositionInfo);
            connection.SendMessage(respondCharacter);
        }

        public void OnMoveSync(PacketReader reader, ConnectionBase connection)
        {
            var moveSync = NetworkContext.MoveSync;
            moveSync.Deserialize(reader);
            Player player = PlayerManager.FindPlayer(moveSync.CharacterId);
            if (player == null) return;

            player.ApplyPostionInfo(NetworkContext.MoveSyncPos);
            NetworkContext.StreamToPlayer(player, moveSync);
        }

        public void OnBeginCastSync(PacketReader reader, ConnectionBase connection)
        {
            var beginCastSync = NetworkContext.GetPacket<BeginCastSync>();
            beginCastSync.Deserialize(reader);
            Player player = PlayerManager.FindPlayer(beginCastSync.CharacterId);
            if (player == null) { beginCastSync.Release(); return; }

            NetworkContext.StreamToPlayer(player, beginCastSync);
        }

        public void OnCancelCastSync(PacketReader reader, ConnectionBase connection)
        {
            var cancelCastSync = NetworkContext.GetPacket<CancelCastSync>();
            cancelCastSync.Deserialize(reader);
            Player player = PlayerManager.FindPlayer(cancelCastSync.CharacterId);
            if (player == null) { cancelCastSync.Release(); return; }

            NetworkContext.StreamToPlayer(player, cancelCastSync);
        }

        public void OnEndCastSync(PacketReader reader, ConnectionBase connection)
        {
            var endCastSync = NetworkContext.GetPacket<EndCastSync>();
            endCastSync.Deserialize(reader);
            Player player = PlayerManager.FindPlayer(endCastSync.CharacterId);
            if (player == null) { endCastSync.Release(); return; }

            SpellHandler.HandleSpellCastEnd(player, endCastSync);
            NetworkContext.StreamToPlayer(player, endCastSync);
        }

        public void OnPurchaseSpell(PacketReader reader, ConnectionBase connection)
        {
            var purchaseSpell = NetworkContext.GetPacket<PurchaseSpell>();
            purchaseSpell.Deserialize(reader);

            Player player = PlayerManager.FindPlayer(purchaseSpell.CharacterId);
            purchaseSpell.Release();
            if (player == null) return;

            var spell = SpellInfo.GetInfo(purchaseSpell.SpellId);
            if (player.ElementPoint(spell.ElementType) >= spell.PurchaseCost)
            {
                player.ConsumeElementPoint(spell.ElementType, spell.PurchaseCost);
                player.GainSpellCount(spell.Id, spell.PurchaseCount);
            }
        }

        public void OnEquipSpell(PacketReader reader, ConnectionBase connection)
        {
            var equipSpell = NetworkContext.GetPacket<EquipSpell>();
            equipSpell.Deserialize(reader);

            Player player = PlayerManager.FindPlayer(equipSpell.CharacterId);
            if (player == null) { equipSpell.Release(); return; }

            player.EquipSpell(equipSpell.Index, equipSpell.SpellId);
            NetworkContext.StreamToPlayer(player, equipSpell);
        }

        #endregion

        void Log(string format,params object[] args)
        {
            if (LogDispatcher != null)
                LogDispatcher.Write(format, args);
        }

        public LogDispatcher LogDispatcher { get; private set; }
        public PacketDispatcher PacketDispatcher { get; private set; }
        public FieldNetworkContext NetworkContext { get; private set; }

        public Scheduler FieldScheduler { get; private set; }
        public FieldManager FieldManager { get; private set; }
        public EffectManager EffectManager { get; private set; }
        public PlayerManager PlayerManager { get; private set; }
        public DropObjectManager DropObjectManager { get; private set; }
        public SpellHandler SpellHandler { get; private set; }
        public AIManager AIManager { get; private set;  }

        Second currentDeltaTime = Second.Zero;
        readonly Second UpdateInterval = (Second)0.02f;
    }
}
