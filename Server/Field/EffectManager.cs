﻿using System.Collections.Generic;
using HAJE.MMO.Network.Packet;
using HAJE.MMO.Server.Field.Object;

namespace HAJE.MMO.Server.Field
{
    public class EffectManager
    {
        public EffectManager(FieldLogic owner)
        {
            this.owner = owner;
            networkContext = owner.NetworkContext;
            fieldManager = owner.FieldManager;
        }

        public Effect FindEffect(int id)
        {
            if (effectDict.ContainsKey(id))
                return effectDict[id];
            else
                return null;
        }

        public Effect CreateEffect(EffectType effectType, PositionInField position, CreateEffect createEffect = null)
        {
            var field = fieldManager.FindField(position.FieldId);
            if (field == null) return null;

            var effect = new Effect(++lastEffectId, effectType, position);
            effectDict.Add(effect.Id, effect);

            if (createEffect == null)
            {
                createEffect = networkContext.GetPacket<CreateEffect>();
                createEffect.TargetType = TargetType.Air;
            }
            createEffect.Id = effect.Id;
            createEffect.EffectType = effectType;
            createEffect.TargetPosition = position;

            networkContext.StreamToPlayer(field, position.Position.Xz,
                createEffect, GamePlayConstant.CharacterSyncRange);
            return effect;
        }

        public void MoveEffect(int id, PositionInField position, Second duration, MoveEffect moveEffect = null)
        {
            var field = fieldManager.FindField(position.FieldId);
            if (field == null) return;
            var effect = FindEffect(id);
            if (effect == null) return;
            effect.Position = position;

            if (moveEffect == null)
            {
                moveEffect = networkContext.GetPacket<MoveEffect>();
                moveEffect.TargetType = TargetType.Air;
            }
            moveEffect.Id = effect.Id;
            moveEffect.TargetPosition = position;
            moveEffect.Duration = duration;

            networkContext.StreamToPlayer(field, position.Position.Xz,
                moveEffect, GamePlayConstant.CharacterSyncRange);
        }

        public void MoveEffect(int id, Player player, AttachmentPoint attachmentPoint, Second duration)
        {
            PositionInField position = player.FieldObject.GetPosInField();
            var field = fieldManager.FindField(position.FieldId);
            if (field == null) return;
            var effect = FindEffect(id);
            if (effect == null) return;
            effect.Position = position;

            var moveEffect = networkContext.GetPacket<MoveEffect>();
            moveEffect.Id = effect.Id;
            moveEffect.TargetType = TargetType.Player;
            moveEffect.TargetPosition = position;
            moveEffect.TargetId = player.Id;
            moveEffect.Duration = duration;

            networkContext.StreamToPlayer(field, position.Position.Xz,
                moveEffect, GamePlayConstant.CharacterSyncRange);
        }

        public void RemoveEffect(int id, bool isStream)
        {
            var effect = FindEffect(id);
            if (effect == null) return;

            var field = fieldManager.FindField(effect.Position.FieldId);
            if (isStream && field != null)
            {
                var removeEffect = networkContext.GetPacket<RemoveEffect>();
                removeEffect.Id = effect.Id;
                networkContext.StreamToPlayer(field, effect.Position.Position.Xz,
                    removeEffect, GamePlayConstant.CharacterSyncRange);
            }
            effectDict.Remove(id);
        }

        int lastEffectId = 0;

        FieldLogic owner;
        FieldNetworkContext networkContext;
        FieldManager fieldManager;
        Dictionary<int, Effect> effectDict = new Dictionary<int, Effect>();
    }
}
