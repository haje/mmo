﻿using HAJE.MMO.Server.Console;
using System.Collections.Generic;
using HAJE.MMO.Server.Achieve.CommandImpl;

namespace HAJE.MMO.Server.Achieve
{
    public class AchieveConsoleFactory : Console.ConsoleFactory
    {
        public AchieveConsoleFactory(Prompt root, AchieveServer server)
            : base("achieve", root)
        {
            this.server = server;
        }

        protected override void BuildCommand(Prompt prompt, List<Command> cmds)
        {
            var ctx = ServerContext.Instance;
            cmds.Add(new Write(ctx));
            cmds.Add(new Read(ctx));
            cmds.Add(new Update(ctx));
            cmds.Add(new Console.CommandImpl.Log(ctx, ctx.AchieveServer));
            base.BuildCommand(prompt, cmds);
        }

        private readonly AchieveServer server;
    }
}
