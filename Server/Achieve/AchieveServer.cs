﻿using System;
using System.Diagnostics;
using System.Text;
using System.Net.Security;
using System.Net.Sockets;
using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Achieve
{
    public class AchieveServer : ServantServer
    {
        public AchieveServer(string masterIp, int masterPort)
            : base("AchieveServer", masterIp, masterPort)
        {
        }

        public override void Run()
        {
            try
            {
                //TODO: local or remote
                achieveDB = new RemoteAchieveDB();
                socketServer = new SocketServer(logName, Configuration.Instance.AchievePort, SocketReceiveCallback, Log);

                ConnectMaster();
                masterSocketClinet.SendAsync(SocketHelper.CreateMasterRegisterPacket(ServerType.Achieve, socketServer.Port));
            }
            catch (Exception e)
            {
                Log.Write("failed to run server");
                Log.Write(e.ToString());
                Stop();
            }
        }

        public override void Stop()
        {
            if (masterSocketClinet != null)
            {
                masterSocketClinet.Dispose();
                masterSocketClinet = null;
            }
            achieveDB = null;
        }

        protected override void MasterRegisterCallback(PacketReader reader)
        {
        }

        private void SocketReceiveCallback(PacketReader reader, SocketConnection connection)
        {
        }

        /*
        public void SslReceiveCallback(SslStream stream, Reader reader)
        {
            PacketType packetType = BasePacket.ReadPacketType(reader);
            switch(packetType)
            {
                case PacketType.AchieveRequest:
                {
                    List<string> listAchievement;

                    string id = reader.ReadString();
                    string achievement = reader.ReadString();

                    AchieveResult achieveResult = achieveDB.ReadAchieve(id, out listAchievement);
                    AchieveResonseCode responseCode = AchieveResonseCode.Unknown;
                    switch (achieveResult)
                    {
                        case AchieveResult.Success:
                            responseCode = AchieveResonseCode.Success;
                            break;
                        case AchieveResult.UserNotFound:
                            responseCode = AchieveResonseCode.UserNotFound;
                            break;
                        case AchieveResult.DataNotFound:
                            responseCode = AchieveResonseCode.DataNotFound;
                            break;
                        default:
                            Debug.Assert(false);
                            break;
                    }
                     
                    AchieveResonse responsePacket = new AchieveResonse(responseCode);
                    sslStreamServer.SendMessage(stream, responsePacket);
                    break;
                }
                default:
                    Debug.Assert(false);
                    return;
            }
        }
        */

        public IAchieveDB achieveDB = null;
        SocketServer socketServer = null;
    }
}
