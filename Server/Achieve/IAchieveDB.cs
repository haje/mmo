﻿using System.Collections.Generic;

namespace HAJE.MMO.Server.Achieve
{
    public enum AchieveResult
    {
        Success,
        UserNotFound,
        DataNotFound,
        DataAlreadyExist
    };

    public interface IAchieveDB
    {
        AchieveResult WriteAchieve(string userId, string achievement);
        AchieveResult ReadAchieve(string userId, out List<string> listAchievement);
        AchieveResult UpdateAchieve(string userId, string oldAchievement, string newAchievement);
    }
}
