﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Achieve
{
    public class RemoteAchieveDB : IAchieveDB
    {
        public AchieveResult WriteAchieve(string userName, string achievement)
        {
            // DB에 userName, achievement 추가
            dbConn.Open();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = dbConn;

                if (IsDataExist(cmd, userName, achievement) == false)
                {
                    cmd.CommandText = string.Format("INSERT INTO achievements (user_name, achievement) VALUES ('{0}', '{1}')", userName, achievement);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    dbConn.Close();
                    return AchieveResult.DataAlreadyExist; 
                }
            }
            dbConn.Close();
            return AchieveResult.Success;
        }


        public AchieveResult ReadAchieve(string userName, out List<string> listAchievement)
        {
            // DB에서 id, hashedPassword 읽어서 반환
            listAchievement = new List<string>();
            dbConn.Open();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = dbConn;
                if (IsUserExist(cmd, userName) == false)
                {
                    dbConn.Close();
                    return AchieveResult.UserNotFound;
                }

                cmd.CommandText = string.Format("SELECT achievement from achievements where user_name='{0}'", userName);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    listAchievement.Add(reader["achievement"].ToString());
                }
                reader.Close();
            }
            dbConn.Close();
            return AchieveResult.Success;
        }

        public AchieveResult UpdateAchieve(string userName, string oldAchievement, string newAchievement)
        {
            dbConn.Open();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = dbConn;
                if (IsUserExist(cmd, userName) == false)
                {
                    dbConn.Close();
                    return AchieveResult.UserNotFound;
                }
                if (IsDataExist(cmd, userName, oldAchievement) == false)
                {
                    dbConn.Close();
                    return AchieveResult.DataNotFound;
                }
                cmd.CommandText = string.Format("UPDATE achievements SET achievement='{0}' where user_name='{1}' and achievement='{2}'", newAchievement, userName, oldAchievement);
                cmd.ExecuteNonQuery();
            }
            dbConn.Close();
            return AchieveResult.Success;
        }

        public RemoteAchieveDB()
        {
            connStr = "Server=143.248.233.53;Database=mmo_achievement;Uid=root;Pwd=mmo;";
            dbConn = new MySqlConnection(connStr);
        }

        private bool IsUserExist(MySqlCommand cmd, string userName)
        {
            cmd.CommandText = string.Format("SELECT user_name from achievements where user_name='{0}'", userName);
            MySqlDataReader reader = cmd.ExecuteReader();
            bool isExist = reader.Read();
            reader.Close();
            return isExist;
        }

        private bool IsDataExist(MySqlCommand cmd, string userName, string achievement)
        {
            cmd.CommandText = string.Format("SELECT user_name from achievements where user_name='{0}' and achievement='{1}'", userName, achievement);
            MySqlDataReader reader = cmd.ExecuteReader();
            bool isExist = reader.Read();
            reader.Close();
            return isExist;
        }

        string connStr;
        MySqlConnection dbConn;
    }
}
