using System.Collections.Generic;
using System.IO;

namespace HAJE.MMO.Server.Achieve
{
    class LocalAchieveDB : IAchieveDB
    {
        //임시 PATH. 수정되어야 합니다.
        readonly string LocalAchieveDBPath = @"LocalAchieveDB.csv";
        readonly string LocalAchieveDBUserInfo = "{0},{1},{2},{3}";
        List<string> Achievements = new List<string>();

        public enum LocalDBAchievementDetailInfo
        {
            userName = 0,
            achievement
        }

        public LocalAchieveDB()
        {
            InitializeLocalAchieveDB();
        }

        public void InitializeLocalAchieveDB()
        {
            //파일이 없으면 미리 생성
            if (!File.Exists(LocalAchieveDBPath))
            {
                File.Create(LocalAchieveDBPath).Close();
            }
            else
            {
                using (StreamReader sr = File.OpenText(LocalAchieveDBPath))
                {
                    string data;
                    while ((data = sr.ReadLine()) != null)
                        Achievements.Add(data);
                }
            }
        }


        public AchieveResult WriteAchieve(string userName, string achievement)
        {
            string[] userDetailedData;
            foreach (string datainfo in Achievements)
            {
                userDetailedData = datainfo.Split(',');
                if (userDetailedData[(int)LocalDBAchievementDetailInfo.userName].Equals(userName) &&
                    userDetailedData[(int)LocalDBAchievementDetailInfo.achievement].Equals(achievement))
                {
                    //중복 Data 지원 안함
                    return AchieveResult.DataAlreadyExist;
                }
            }

            if (File.Exists(LocalAchieveDBPath)) 
            {
                using (StreamWriter sw = File.AppendText(LocalAchieveDBPath))
                {
                    string newData = string.Format(LocalAchieveDBUserInfo, userName, achievement);
                    sw.WriteLine(newData);
                    Achievements.Add(newData);
                }
            }
            return AchieveResult.Success;
        }

        public AchieveResult ReadAchieve(string userName, out List<string> listAchievement)
        {
            listAchievement = new List<string>();
            string[] userDetailedData;
            foreach (string datainfo in Achievements)
            {
                userDetailedData = datainfo.Split(',');
                if (userDetailedData[(int)LocalDBAchievementDetailInfo.userName].Equals(userName))
                {
                    listAchievement.Add(userDetailedData[(int)LocalDBAchievementDetailInfo.achievement]);
                }
            }
            // 파일 시스템에서 achievement 읽어서 반환
            if(listAchievement.Count == 0)
                return AchieveResult.Success;
            else
                return AchieveResult.UserNotFound;
        }

        public void RenewLocalAchieveDB()
        {
            //현재 List<string> userInfo에 있는 내용을 파일에 반영
            if (File.Exists(LocalAchieveDBPath))
                File.Delete(LocalAchieveDBPath);

            using (StreamWriter sw = File.CreateText(LocalAchieveDBPath))
            {
                foreach (string datainfo in Achievements)
                    sw.WriteLine(datainfo);
            }
        }


        public AchieveResult UpdateAchieve(string userName, string oldAchievement, string newAchievement)
        {
            string[] userDetailedData;
            bool isSuccess = false;
            for (int i = 0; i < Achievements.Count; i++)
            {
                userDetailedData = Achievements[i].Split(',');
                if (userDetailedData[(int)LocalDBAchievementDetailInfo.userName].Equals(userName) &&
                    userDetailedData[(int)LocalDBAchievementDetailInfo.achievement].Equals(oldAchievement))
                {
                    isSuccess = true;
                    Achievements[i] = string.Format(LocalAchieveDBUserInfo, userDetailedData[(int)LocalDBAchievementDetailInfo.userName],
                                                                   newAchievement);
                    break;
                }
            }

            if (isSuccess)
            {
                RenewLocalAchieveDB();
                return AchieveResult.Success;
            }
            return AchieveResult.DataNotFound;
        }
    }
}
