﻿using System;
using HAJE.MMO.Server.Console;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Achieve.CommandImpl
{
    public class Read : Command
    {
        public Read(ServerContext ctx)
            : base("read", "Achievement DB의 업적 정보를 불러옵니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                List<string> listAchievement;
                AchieveResult dbRet = ctx.AchieveServer.achieveDB.ReadAchieve(commandTokens[1], out listAchievement);
                switch (dbRet)
                {
                    case AchieveResult.Success:
                        WriteConsole("userName: {0}", commandTokens[1]);
                        for (int i = 0; i < listAchievement.Count; i++ )
                            WriteConsole(i.ToString() + ": "+listAchievement[i]);
                        break;
                    case AchieveResult.UserNotFound:
                        WriteConsole("User not exist!");
                        break;
                    default:
                        throw new System.Exception("Unexpected enum case");
                }
               
            }
            else
            {
                WriteConsole("필요 인자 목록: userId");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
