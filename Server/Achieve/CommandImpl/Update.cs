﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Achieve.CommandImpl
{
    public class Update : Command
    {
        public Update(ServerContext ctx)
            : base("update", "Achievement DB의 업적 정보를 변경합니다")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 3)
            {
                AchieveResult dbRet = ctx.AchieveServer.achieveDB.UpdateAchieve(commandTokens[1], commandTokens[2], commandTokens[3]);
                switch (dbRet)
                {
                    case AchieveResult.Success:
                        WriteConsole("Achievement updated successfully");
                        break;
                    case AchieveResult.UserNotFound:
                        WriteConsole("User not exist!");
                        break;
                    case AchieveResult.DataNotFound:
                        WriteConsole("Achievement data not exist!");
                        break;
                    default: 
                        throw new System.Exception("Unexpected enum case");
                }               
            }
            else
            {
                WriteConsole("필요 인자 목록: userName, oldAchievement, newAchievement");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
