﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Achieve.CommandImpl
{
    public class Write : Command
    {
        public Write(ServerContext ctx)
            : base("write", "Achievement DB에 업적을 생성합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 2)
            {
                AchieveResult dbRet = ctx.AchieveServer.achieveDB.WriteAchieve(commandTokens[1], commandTokens[2]);

                switch (dbRet)
                {
                    case AchieveResult.Success:
                        WriteConsole("Achievement added successfully");
                        break;
                    case AchieveResult.DataAlreadyExist:
                        WriteConsole("Achievement data already exist!");
                        break;
                    default:
                        throw new System.Exception("Unexpected enum case");
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: userName, achievement");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
