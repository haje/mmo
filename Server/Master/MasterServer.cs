﻿using System;
using System.Net;
using System.Diagnostics;
using System.Text;
using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Master
{
    public class MasterServer : IDisposable, Console.ILogHolder
    {
        public void Run()
        {
            socketServer = new SocketServer(
                "MasterServer",
                Configuration.Instance.MasterPort,
                ReceiveCallback);
            socketServer.StartServer();
            Log = new LogDispatcher("Master", 128);
        }

        public void Stop()
        {
        }

        public void Dispose()
        {
            Stop();
        }

        private void ReceiveCallback(PacketReader reader, SocketConnection connection)
        {
            PacketType packetType = PacketBase.ReadPacketType(reader);
            switch(packetType)
            {
                case PacketType.ServerRegisterRequest:
                    using (var request = new ServerRegisterRequest())
                    {
                        request.Deserialize(reader);
                        string serverKey = CryptoHelper.IListToString(Configuration.Instance.ServerKey, Encoding.UTF8);
                        string binaryKey = CryptoHelper.IListToString(Configuration.Instance.BinaryKey, Encoding.UTF8);
                        bool isValid = serverKey.CompareTo(request.ServerKey) == 0 &&
                                       binaryKey.CompareTo(request.BinaryKey) == 0;

                        ResponseCode responseCode = isValid ? ResponseCode.Success : ResponseCode.Failed;
                        var responsePacket = new ServerRegisterResponse(responseCode);

                        if (isValid)
                        {
                            var acceptServerInfo = new AcceptServerInfo(request.ServerType,
                                connection.RemoteIPAddress(),
                                request.ServerPort);
                            switch (request.ServerType)
                            {
                                case ServerType.Login:
                                    loginServerInfo = acceptServerInfo;
                                    break;
                                case ServerType.Allocator:
                                    allocatorServerInfo = acceptServerInfo;
                                    break;
                                case ServerType.Field:
                                    fieldServerInfo = acceptServerInfo;
                                    responsePacket.isServerInfoExist = true;
                                    responsePacket.ServerType = ServerType.Login;
                                    responsePacket.ServerIp = allocatorServerInfo.ServiceIp;
                                    responsePacket.ServerPort = allocatorServerInfo.ServerPort;
                                    break;
                                case ServerType.Achieve:
                                    achieveServerInfo = acceptServerInfo;
                                    break;
                                default:
                                    Debug.Assert(false);
                                    break;
                            }
                        }

                        connection.SendMessage(responsePacket);
                        break;
                    }
                default:
                    Debug.Assert(true);
                    break;
            }
        }

        public int Port
        {
            get { return socketServer.Port; }
        }

        SocketServer socketServer = null;
        AcceptServerInfo loginServerInfo = null;
        AcceptServerInfo allocatorServerInfo = null;
        AcceptServerInfo fieldServerInfo = null;
        AcceptServerInfo achieveServerInfo = null;

        public LogDispatcher Log { get; private set; }
    }
}
