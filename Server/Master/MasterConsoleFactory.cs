﻿using HAJE.MMO.Server.Console;
using System.Collections.Generic;

namespace HAJE.MMO.Server.Master
{
    public class MasterConsoleFactory : Console.ConsoleFactory
    {
        public MasterConsoleFactory(Prompt root, MasterServer server)
            : base("master", root)
        {
            this.server = server;
        }

        protected override void BuildCommand(Prompt prompt, List<Command> cmds)
        {
            var ctx = ServerContext.Instance;
            cmds.Add(new Console.CommandImpl.Log(ctx, server));
            base.BuildCommand(prompt, cmds);
        }

        private readonly MasterServer server;
    }
}
