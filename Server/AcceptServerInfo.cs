﻿using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server
{
    public class AcceptServerInfo
    {
        public AcceptServerInfo(ServerType serverType, string serviceIp, int serverPort)
        {
            this.ServerType = serverType;
            this.ServiceIp = serviceIp;
            this.ServerPort = serverPort;
        }

        public ServerType ServerType;
        public string ServiceIp;
        public int ServerPort;
    }
}
