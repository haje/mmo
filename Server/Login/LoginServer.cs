﻿using HAJE.MMO.Network;
using HAJE.MMO.Network.Controller;
using HAJE.MMO.Network.Packet;
using System;
using System.Net;
using System.Diagnostics;

namespace HAJE.MMO.Server.Login
{ 
    public class LoginServer : ServantServer
    {
        public LoginServer(string masterIp, int masterPort)
            : base("LoginServer", masterIp, masterPort)
        {
        }

        public override void Run()
        {
            try
            {
                if (Configuration.Instance.ServerMode == ServerMode.LocalServer)
                    loginDB = new LocalLoginDB();
                else
                    loginDB = new RemoteLoginDB();

                socketServer = new SocketServer("LoginSocketServer",
                    Configuration.Instance.AllocatorPort, SocketReceiveCallback, Log);
                socketServer.StartServer();

                sslStreamServer = new SslStreamServer("LoginSslServer",
                    Configuration.Instance.LoginPort, SslReceiveCallback, Log);
                sslStreamServer.StartServer();

                ConnectMaster();
                //masterSocketClinet.SendAsync(SocketHelper.CreateMasterRegisterPacket(ServerType.Login, sslStreamServer.Port));
                masterSocketClinet.SendAsync(SocketHelper.CreateMasterRegisterPacket(ServerType.Allocator, socketServer.Port));
            }
            catch (Exception e)
            {
                Log.Write("LoginServer: failed to run server");
                Log.Write(e.ToString());
                Stop();
            }
        }

        public override void Stop()
        {
            loginDB = null;
            masterSocketClinet = null;
            sslStreamServer = null;
        }

        protected override void MasterRegisterCallback(PacketReader reader)
        {
            using (ServerRegisterResponse response = new ServerRegisterResponse())
            {
                response.Deserialize(reader);
                if (response.ResponseCode == ResponseCode.Success)
                {
                }
            }
        }

        public void SocketReceiveCallback(PacketReader reader, SocketConnection connection)
        {
            PacketType packetType = PacketBase.ReadPacketType(reader);
            switch (packetType)
            {
                case PacketType.ServerRegisterRequest:
                    using (ServerRegisterRequest request = new ServerRegisterRequest())
                    {
                        request.Deserialize(reader);
                        if (request.ServerType == ServerType.Field)
                        {
                            fieldServerInfo = new AcceptServerInfo(request.ServerType,
                                connection.RemoteIPAddress(),
                                request.ServerPort);
                            fieldConnection = connection;
                            ServerRegisterResponse response = new ServerRegisterResponse(ResponseCode.Success);
                            connection.SendMessage(response);
                        }
                    }
                    break;
                default:
                    Debug.Assert(false);
                    return;
            }
        }

        int dummyPlayerId = 1;

        public void SslReceiveCallback(PacketReader reader, SslStreamConnection connection)
        {
            PacketType packetType = PacketBase.ReadPacketType(reader);
            switch(packetType)
            {
                case PacketType.SuperLoginRequest:
                    using (SuperLoginRequest superLogionRequest = new SuperLoginRequest())
                    {
                        superLogionRequest.Deserialize(reader);
                        int playerId;
                        var loginResponseCode = ProcessSuperLogin(superLogionRequest.Name, out playerId);

                        //XXX: 임시로 중복 로그인 허용.
                        // 모든 로그인은 다른 플레이어로 간주되도록 한다. @atonal
                        playerId = dummyPlayerId++;

                        string token = CryptoHelper.GetFieldToken(superLogionRequest.Name);
                        var loginResponsePacket = new LoginResponse(loginResponseCode,
                                playerId, token,
                                fieldServerInfo.ServiceIp, (Int16)fieldServerInfo.ServerPort);
                        var fieldTokenPacket = new AuthWithToken(playerId, token);
                        fieldConnection.SendMessage(fieldTokenPacket);
                        connection.SendMessage(loginResponsePacket);
                    }
                    break;
                case PacketType.LoginRequest:
                    using (LoginRequest loginRequest = new LoginRequest())
                    {
                        loginRequest.Deserialize(reader);
                        int playerId;
                        var loginResponseCode = ProcessLogin(loginRequest.Email, loginRequest.Password, out playerId);

                        LoginResponse loginResponsePacket;
                        if (loginResponseCode == LoginResponseCode.Success)
                        {
                            string token = CryptoHelper.GetFieldToken(loginRequest.Email);
                            loginResponsePacket = new LoginResponse(loginResponseCode,
                                playerId, token,
                                fieldServerInfo.ServiceIp, (Int16)fieldServerInfo.ServerPort);
                            var fieldTokenPacket = new AuthWithToken(playerId, token);
                            fieldConnection.SendMessage(fieldTokenPacket);
                        }
                        else
                            loginResponsePacket = new LoginResponse(loginResponseCode, -1, "None", "None", -1);

                        connection.SendMessage(loginResponsePacket);
                    }
                    break;
                case PacketType.NewAccountRequest:
                    using (NewAccountRequest accountRequest = new NewAccountRequest())
                    {
                        accountRequest.Deserialize(reader);
                        NewAccountResponse accountResponse = new NewAccountResponse(ProcessAccountRegistration(accountRequest.Email, accountRequest.UserName, accountRequest.Password));
                        connection.SendMessage(accountResponse);
                    }
                    break;
                case PacketType.BuildStampReport:
                    using (BuildStampReport buildStampRequest = new BuildStampReport())
                    {
                        buildStampRequest.Deserialize(reader);
                        if (buildStampRequest.buildStamp != BuildInfo.BuildStamp)
                        {
                            ErrorReport errorReport = new ErrorReport();
                            errorReport.Content = "서버와 호환되지 않는 클라이언트 버전입니다.";
                            errorReport.ErrorType = ErrorType.ExitGame;
                            connection.SendMessage(errorReport);
                        }
                    }
                    break;
                default:
                    throw new InvalidOperationException("아직 처리되지 않은 PacketType 입니다.");
            }
        }

        public LoginResponseCode ProcessSuperLogin(string name, out int id)
        {
            string email, userName, hashedPassword, createDate, accessDate;
            int internalKey, mailAuthorized;

            LoginResult loginResult = loginDB.ReadLogin(UserColumn.UserName, name,
                out email, out userName, out internalKey,
                out hashedPassword, out mailAuthorized, out createDate, out accessDate);
            switch (loginResult)
            {
                case LoginResult.Success:
                    break;
                case LoginResult.UserNotFound:
                    try
                    {
                        loginDB.WriteLogin(name, name, name);
                    }
                    catch
                    {
                        loginDB.WriteLogin(name, name+"_",name);
                    }
                    loginResult = loginDB.ReadLogin(UserColumn.UserName, name,
                        out email, out userName, out internalKey,
                        out hashedPassword, out mailAuthorized, out createDate, out accessDate);
                    Debug.Assert(loginResult == LoginResult.Success);
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }

            id = internalKey;
            return LoginResponseCode.Success;
        }

        public LoginResponseCode ProcessLogin(string _email, string _password, out int id)
        {
            //System.Console.WriteLine("Receive from Client : {0} {1}", _id, _password);
            string email, userName, hashedPassword, createDate, accessDate;
            int internalKey, mailAuthorized;

            LoginResult loginResult = loginDB.ReadLogin(UserColumn.Email, _email,
                out email, out userName, out internalKey, 
                out hashedPassword, out mailAuthorized, out createDate, out accessDate);
            LoginResponseCode responseCode = LoginResponseCode.Unknown;
            switch (loginResult)
            {
                case LoginResult.Success:
                {
                    var _hashedPassword = CryptoHelper.GetUserHashedPassword(_email, _password);
                    if(_hashedPassword == null)
                        responseCode = LoginResponseCode.WebServerNotFound;
                    else if (hashedPassword.CompareTo(_hashedPassword) == 0)
                        responseCode = LoginResponseCode.Success;
                    else
                        responseCode = LoginResponseCode.PasswordNotMatch;
                    break;
                }
                case LoginResult.UserNotFound:
                    responseCode = LoginResponseCode.UserNotFound;
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }
            id = internalKey;
            return responseCode;
        }

        public NewAccountResponseCode ProcessAccountRegistration(string _email, string _userName, string _password)
        {
            //System.Console.WriteLine("Receive from Client : {0} {1}", _id, _password);
            string token = null;
            

            NewAccountResponseCode responseCode = NewAccountResponseCode.Unknown;

            if (_email.IndexOf('@') < 1)
            {
                responseCode = NewAccountResponseCode.InvalidUserId;
                return responseCode;
            }
            
            /* 메일 기능 켜고 싶을때 ON */
            /*
            if (mailRegister == null) mailRegister = new MailRegister();
            token = mailRegister.SendAuthorizeMail(_email, _userName);
            if (token == null)
            {
                responseCode = NewAccountResponseCode.AddressNotExists;
                return responseCode;
            }
            */

            var _hashedPassword = CryptoHelper.GetUserHashedPassword(_email, _password);
            if(_hashedPassword == null)
                return NewAccountResponseCode.WebServerNotFound;

            LoginResult loginResult = loginDB.WriteLogin(_email, _userName, _hashedPassword);
            
            loginDB.WriteToken(_email, token);

            switch (loginResult)
            {
                case LoginResult.Success:
                    responseCode = NewAccountResponseCode.Success;
                    break;
                case LoginResult.EmailAlreadyExist:
                    responseCode = NewAccountResponseCode.EmailAlreadyExists;
                    break;
                case LoginResult.UserAlreadyExist:
                    responseCode = NewAccountResponseCode.UserNameAlreadyExists;
                    break;
                default:
                    Debug.Assert(false);
                    break;
            }
            return responseCode;
        }

        public int Port
        {
            get
            {
                return sslStreamServer.Port;
            }
        }

        public ILoginDB loginDB = null;

        SocketServer socketServer = null;
        SslStreamServer sslStreamServer = null;
        MailRegister mailRegister = null;
        AcceptServerInfo fieldServerInfo = null;
        SocketConnection fieldConnection = null;
    }
}
