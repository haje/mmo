﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HAJE.MMO.Server.Login
{
    class LocalLoginDB : ILoginDB
    {
        readonly string LocalLoginDBPath = PathHelper.BinaryDirectory + "LocalDB\\LocalLoginDB.csv";
        readonly string LocalLoginDBUserInfo = "{0},{1},{2},{3},{4},{5},{6}";
        List<string> Users = new List<string>();
        
        public LocalLoginDB()
        {
            InitializeLocalLoginDB();
        }

        public void InitializeLocalLoginDB()
        {
            PathHelper.EnsureDirectory(LocalLoginDBPath);

            //파일이 없으면 미리 생성
            if (!File.Exists(LocalLoginDBPath))
            {
                File.Create(LocalLoginDBPath).Close();
            }
            else
            {
                using (StreamReader sr = File.OpenText(LocalLoginDBPath))
                {
                    string data;
                    while ((data = sr.ReadLine()) != null)
                        Users.Add(data);
                }
            }
        }

        void RenewLocalLoginDB()
        {
            //현재 List<string> userInfo에 있는 내용을 파일에 반영
            if (File.Exists(LocalLoginDBPath))
                File.Delete(LocalLoginDBPath);

            using (StreamWriter sw = File.CreateText(LocalLoginDBPath))
            {
                foreach (string userInfo in Users)
                    sw.WriteLine(userInfo);
            }
        }

        public LoginResult WriteLogin(string email, string userName, string hashedPassword)
        {
            try
            {
                string[] userDetailedData;
                foreach (string userInfo in Users)
                {
                    userDetailedData = userInfo.Split(',');
                    if (userDetailedData[(int)UserColumn.Email].Equals(email))
                    {
                        //중복 ID 지원 안함
                        return LoginResult.UserAlreadyExist;
                    }
                }

                if (File.Exists(LocalLoginDBPath))
                {
                    using (StreamWriter sw = File.AppendText(LocalLoginDBPath))
                    {
                        string newData = string.Format(LocalLoginDBUserInfo, 
                            email, userName, Users.Count + 1, hashedPassword, 1, DateTime.Now, DateTime.Now);
                        sw.WriteLine(newData);
                        Users.Add(newData);
                    }
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("WriteLogin: {0}", e.Message);
                return LoginResult.Exception;
            }
            return LoginResult.Success;
        }

        public LoginResult ReadLogin(UserColumn column, string colValue, out string email, out string userName, out int internalKey,
            out string hashedPassword, out int mailAuthorized, out string createDate, out string accessDate)
        {
            email = "";
            userName = "";
            internalKey = -1;
            hashedPassword = "";
            mailAuthorized = 0;
            createDate = "";
            accessDate = "";

            try
            {
                string[] userDetailedData;
                foreach (string userInfo in Users)
                {
                    userDetailedData = userInfo.Split(',');
                    if (userDetailedData[(int)column].Equals(colValue))
                    {
                        email = userDetailedData[(int)UserColumn.Email];
                        userName = userDetailedData[(int)UserColumn.UserName];
                        internalKey = Int32.Parse(userDetailedData[(int)UserColumn.InternalKey]);
                        hashedPassword = userDetailedData[(int)UserColumn.PasswordHash];
                        mailAuthorized = Int32.Parse(userDetailedData[(int)UserColumn.MailAuthorized]);
                        createDate = userDetailedData[(int)UserColumn.CreateDate];
                        accessDate = userDetailedData[(int)UserColumn.AccessDate];
                        return LoginResult.Success;
                    }
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("ReadLogin: {0}", e.Message);
                return LoginResult.Exception;
            }
            return LoginResult.UserNotFound;
        }

        public LoginResult UpdateHashedPassword(string email, string hashedPassword)
        {
            try
            {
                string[] userDetailedData;
                for (int i = 0; i < Users.Count; i++)
                {
                    userDetailedData = Users[i].Split(',');
                    if (userDetailedData[(int)UserColumn.Email].Equals(email))
                    {
                        Users[i] = string.Format(LocalLoginDBUserInfo, 
                            userDetailedData[(int)UserColumn.Email],
                            userDetailedData[(int)UserColumn.UserName],
                            userDetailedData[(int)UserColumn.InternalKey],
                            hashedPassword,
                            userDetailedData[(int)UserColumn.MailAuthorized],
                            userDetailedData[(int)UserColumn.CreateDate],
                            userDetailedData[(int)UserColumn.AccessDate]);
                        RenewLocalLoginDB();
                        return LoginResult.Success;
                    }
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("ReadLogin: {0}", e.Message);
                return LoginResult.Exception;
            }
            return LoginResult.UserNotFound;
        }

        public LoginResult DeleteUser(UserColumn column, string colValue)
        {
            //Do nothing in local server
            return LoginResult.Success;
        }

        public void FindUser(UserColumn column, string colValue, out List<Dictionary<string, string>> userList)
        {
            //Do nothing in local server
            userList = null;
        }

        public bool WriteToken(string email, string token)
        {
            //Do nothing in local server
            return true;
        }

        public bool AuthorizeUser(string token)
        {
            //Do nothing in local server
            return true;
        }

        public void ResetTable()
        {
            try
            {
                Users.Clear();
                RenewLocalLoginDB();
            }
            catch (Exception e)
            {
                System.Console.WriteLine("ResetTables: {0}", e.Message);
            }
        }

        public int CountTable()
        {
            return Users.Count;
        }
    }
}
