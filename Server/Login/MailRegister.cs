﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.Mail;
using MySql.Data.MySqlClient;

namespace HAJE.MMO.Server.Login
{
    class MailRegister
    {
        public string SendAuthorizeMail(string _email, string _userName)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();

                string[] parsedEmail = System.Text.RegularExpressions.Regex.Split(_email, "@");

                mail.From = new MailAddress("HajeMMO@haje.org");
                mail.To.Add(_email);
                mail.Subject = "[Haje MMO 가입 인증]";
                string token = CryptoHelper.GetAuthHashedUrl(_email, _userName);

                string url = GetLocalIPAddress() + ":5000" + token;
                mail.Body = _email + " 님 안녕하세요. 이메일 가입 인증을 하시려면 아래의 링크로 접속해주세요. <br /><br />" 
                    + "<a href=\"http://"+url+"\" target=\"_blank\"> " + url + " </a>";
                mail.IsBodyHtml = true;
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                string smtp = parsedEmail[1];
                client.Host = "smtp." + smtp;
                client.UseDefaultCredentials = false;
                // TODO: 메일 인증서가 필요하면 추가
                //client.Credentials = new System.Net.NetworkCredential("My ID", "My Password");
                //client.EnableSsl = true;

                client.Send(mail);
                return token;
            }
            catch (Exception e)
            {
                System.Console.WriteLine("SMTP Mail Send ERROR : {0}",e.Message);
                return null;
            }
        }

        public string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public MailRegister()
        {
            var config = Configuration.Instance.LoginDB;
            //TODO: @server 위 config에 따라 세팅 후 테스트.
            string connStr = string.Format("Server={0};Database={1};Uid={2};Pwd={3};",config.Ip,config.Name,config.Uid,config.Password);
            dbConn = new MySqlConnection(connStr);
        }

        MySqlConnection dbConn;
    }
}
