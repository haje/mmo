﻿using System.Collections.Generic;

namespace HAJE.MMO.Server.Login
{
    public enum LoginResult
    {
        Success,
        Exception,
        UserNotFound,
        EmailAlreadyExist,
        UserAlreadyExist,
    }

    public enum UserColumn
    {
        Email = 0,
        UserName,
        InternalKey,
        PasswordHash,
        MailAuthorized,
        CreateDate,
        AccessDate,

        MAX
    }

    public interface ILoginDB
    {
        LoginResult WriteLogin(string email, string userName, string hashedPassword);
        LoginResult ReadLogin(UserColumn column, string colValue, out string email, out string userName, out int internalKey,
            out string hashedPassword, out int mailAuthorized, out string createDate, out string accessDate);
        LoginResult UpdateHashedPassword(string email, string hashedPassword);
        LoginResult DeleteUser(UserColumn column, string colValue);
        void FindUser(UserColumn column, string colValue, out List<Dictionary<string, string>> userList);

        bool WriteToken(string email, string token);
        bool AuthorizeUser(string token);

        void ResetTable();
        int CountTable();
    }
}
