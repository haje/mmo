﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace HAJE.MMO.Server.Login
{
    public class RemoteLoginDB : ILoginDB
    {
        public static string[] userColumnNames = { "email", "userName", "internalKey",
                        "passwordHash", "mailAuthorized", "createDate", "accessDate" };

        public LoginResult WriteLogin(string email, string userName, string hashedPassword)
        {
            LoginResult result = LoginResult.Success;
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    if (IsUserExist(cmd, "email", email) == true)
                    {
                        result = LoginResult.EmailAlreadyExist;
                    }
                    else if (IsUserExist(cmd, "userName", userName) == true)
                    {
                        result = LoginResult.UserAlreadyExist;
                    }
                    else
                    {
                        var cmdFormat = "INSERT INTO users (email, userName, passwordHash, mailAuthorized) VALUES ('{0}', '{1}', '{2}', {3})";
                        cmd.CommandText = string.Format(cmdFormat, email, userName, hashedPassword, 0);
                        cmd.ExecuteNonQuery();
                    }                        
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("WriteLogin: {0}", e.Message);
                result = LoginResult.Exception;
            }
            dbConn.Close();
            return result;
        }

        public LoginResult ReadLogin(UserColumn column, string colValue, out string email, out string userName, out int internalKey,
            out string hashedPassword, out int mailAuthorized, out string createDate, out string accessDate)
        {
            LoginResult result = LoginResult.Success;
            email = "";
            userName = "";
            internalKey = -1;
            hashedPassword = "";
            mailAuthorized = 0;
            createDate = "";
            accessDate = "";

            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    string colName = userColumnNames[(int)column];
                    cmd.Connection = dbConn;
                    if (IsUserExist(cmd, colName, colValue) == false)
                    {
                        dbConn.Close();
                        return LoginResult.UserNotFound;
                    }

                    cmd.CommandText = string.Format("SELECT * FROM users WHERE {0}='{1}'", colName, colValue);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        email = reader["email"].ToString();
                        userName = reader["userName"].ToString();
                        internalKey = reader.GetInt32("internalKey");
                        hashedPassword = reader["passwordHash"].ToString();
                        mailAuthorized = reader.GetInt32("mailAuthorized");
                        createDate = reader["createDate"].ToString();
                        accessDate = reader["accessDate"].ToString();
                    }
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("ReadLogin: {0}", e.Message);
                result = LoginResult.Exception;
            }
            dbConn.Close();
            return result;
        }

        public LoginResult UpdateHashedPassword(string email, string hashedPassword)
        {
            LoginResult result = LoginResult.Success;
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    if (IsUserExist(cmd, "email", email))
                    {
                        cmd.CommandText = string.Format("UPDATE users SET passwordHash='{0}' WHERE email='{1}'", hashedPassword, email);
                        cmd.ExecuteNonQuery();
                    }
                    else
                        result = LoginResult.UserNotFound;
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("UpdateHashedPassword: {0}", e.Message);
                result = LoginResult.Exception;
            }
            dbConn.Close();
            return result;
        }

        public LoginResult DeleteUser(UserColumn column, string colValue)
        {
            LoginResult result = LoginResult.Success;
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    string colName = userColumnNames[(int)column];
                    cmd.Connection = dbConn;
                    if (IsUserExist(cmd, colName, colValue))
                    {
                        cmd.CommandText = string.Format("DELETE FROM users WHERE {0}='{1}'", colName, colValue);
                        cmd.ExecuteNonQuery();
                    }
                    else
                        result = LoginResult.UserNotFound;
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("DeleteUser: {0}", e.Message);
                result = LoginResult.Exception;
            }
            dbConn.Close();
            return result;
        }

        public void FindUser(UserColumn column, string colValue, out List<Dictionary<string, string>> userList)
        {
            userList = null;
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    userList = new List<Dictionary<string, string>>();
                    string colName = userColumnNames[(int)column];

                    cmd.Connection = dbConn;
                    cmd.CommandText = string.Format("SELECT * FROM users WHERE {0} LIKE '{1}%'", colName, colValue);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var dict = new Dictionary<string, string>();
                        foreach (var col in userColumnNames)
                        {
                            dict.Add(col, reader[col].ToString());
                        }
                        userList.Add(dict);
                    }
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("FindUser: {0}", e.Message);
                userList = null;
            }
            dbConn.Close();
        }

        public bool WriteToken(string email, string token)
        {
            string type = "Register";
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    // 토큰이 이미 존재하면 지우고 다시 쓴다. (덮어쓰기)
                    cmd.CommandText = string.Format("SELECT token FROM token WHERE email='{0}'", email);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    bool isExist = reader.Read();
                    reader.Close();
                    if(isExist == true)
                    {
                        cmd.CommandText = string.Format("DELETE FROM token WHERE email='{0}'", email);
                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = string.Format("INSERT INTO token (email, token, type) VALUES ('{0}', '{1}', '{2}')", email, token, type);
                    cmd.ExecuteNonQuery();
                }
                dbConn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                System.Console.WriteLine("MailRegister: Token Table Insert Error! : {0}",ex.Message);
                dbConn.Close();
                return false;
            }
        }

        public bool AuthorizeUser(string token)
        {
            string email;
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    cmd.CommandText = string.Format("SELECT email FROM token WHERE token = '{0}'", token);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        email = reader["email"].ToString();
                        reader.Close();
                        cmd.CommandText = string.Format("UPDATE users SET mailAuthorized=1 WHERE email='{0}'", email);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        reader.Close();
                        dbConn.Close();
                        return false;
                    }
                }
                dbConn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                System.Console.WriteLine("MailRegister: Token Table Insert Error! : {0}",ex.Message);
                dbConn.Close();
                return false;
            }
        }

        public void ResetTable()
        {
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand("DELETE FROM users", dbConn))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("ResetTable: {0}", e.Message);
            }
            dbConn.Close();
        }

        public int CountTable()
        {
            int count = 0;
            dbConn.Open();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT COUNT(*) FROM users WHERE mailAuthorized=1", dbConn))
                {
                    count = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("CountTable: {0}", e.Message);
                count = - 1;
            }
            dbConn.Close();
            return count;
        }

        public RemoteLoginDB()
        {
            var config = Configuration.Instance.LoginDB;
            var connStr = string.Format("Server={0};Database={1};Uid={2};Pwd={3};",config.Ip,config.Name,config.Uid,config.Password);
            dbConn = new MySqlConnection(connStr);
        }

        private bool IsUserExist(MySqlCommand cmd, string colName, string colValue)
        {
            cmd.CommandText = string.Format("SELECT email from users WHERE {0}='{1}'", colName, colValue);
            MySqlDataReader reader = cmd.ExecuteReader();
            bool isExist = reader.Read();
            reader.Close();
            return isExist;
        }

        MySqlConnection dbConn;
    }
}
