﻿using System;
using System.Collections.Generic;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Find : Command
    {
        public Find(ServerContext ctx)
            : base("find", "Login DB의 유저를 검색합니다 (userName)")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                List<Dictionary<string, string>> userList = null;
                ctx.LoginServer.loginDB.FindUser(UserColumn.UserName, commandTokens[1], out userList);
                if (userList == null)
                {
                    WriteConsole("Find query failed");
                    return true;
                }

                foreach(var dict in userList)
                    WriteConsole("{0}\t{1}\t{2}\t", 
                        dict["internalKey"], dict["email"], dict["userName"]);
            }
            else
            {
                WriteConsole("필요 인자 목록: userName");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}

