﻿using System;
using HAJE.MMO.Server.Console;
using HAJE.MMO.Network.Packet;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Write : Command
    {
        public Write(ServerContext ctx)
            : base("write", "Login DB에 유저를 생성합니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 3)
            {
                NewAccountResponseCode dbRet = ctx.LoginServer.ProcessAccountRegistration(commandTokens[1], commandTokens[2], commandTokens[3]);

                switch (dbRet)
                {
                    case NewAccountResponseCode.Success:
                        WriteConsole("User added successfully");
                        break;
                    case NewAccountResponseCode.EmailAlreadyExists:
                        WriteConsole("Email already exist!");
                        break;
                    case NewAccountResponseCode.UserNameAlreadyExists:
                        WriteConsole("User name already exist!");
                        break;
                    case NewAccountResponseCode.AddressNotExists:
                        WriteConsole("Email address not exist!");
                        break;                    
                    case NewAccountResponseCode.InvalidUserId:
                        WriteConsole("User ID is not valid address!");
                        break;
                    case NewAccountResponseCode.WebServerNotFound:
                        WriteConsole("Web server not found");
                        break;
                    default:
                        throw new System.Exception("Unexpected enum case");
                }
            }
            else
            {
                WriteConsole("필요 인자 목록: email, userName, password");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
