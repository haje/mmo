﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Count : Command
    {
        public Count(ServerContext ctx)
            : base("count", "Login DB의 유저 수를 출력합니다 (mailAuthorized=1)")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            int count = ctx.LoginServer.loginDB.CountTable();
            WriteConsole("Total Users: {0}", count);
            return true;
        }

        private readonly ServerContext ctx;
    }
}
