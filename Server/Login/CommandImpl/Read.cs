﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Read : Command
    {
        public Read(ServerContext ctx)
            : base("read", "Login DB의 유저 정보를 불러옵니다.")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                string email, userName, hashedPassword, createDate, accessDate;
                int internalKey, mailAuthorized;

                LoginResult dbRet = ctx.LoginServer.loginDB.ReadLogin(UserColumn.UserName, commandTokens[1],
                    out email, out userName, out internalKey,
                    out hashedPassword, out mailAuthorized, out createDate, out accessDate);
                switch (dbRet)
                {
                    case LoginResult.Success:
                        WriteConsole("\n email: {0},\n userName: {1},\n internalKey: {2},\n passwordHash: {3},\n mailAuthorized: {4},\n createDate: {5},\n accessDate: {6}",
                         email, userName, internalKey, hashedPassword, mailAuthorized, createDate, accessDate);
                        break;
                    case LoginResult.UserNotFound:
                        WriteConsole("User not exist!");
                        break;
                    default:
                        throw new System.Exception("Unexpected enum case");
                }
               
            }
            else
            {
                WriteConsole("필요 인자 목록: userName");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
