﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Delete : Command
    {
        public Delete(ServerContext ctx)
            : base("delete", "Login DB의 유저를 삭제합니다 (userName)")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 1)
            {
                LoginResult dbRet = ctx.LoginServer.loginDB.DeleteUser(UserColumn.UserName, commandTokens[1]);
                switch (dbRet)
                {
                    case LoginResult.Success:
                        WriteConsole("Delete {0} success", commandTokens[1]);
                        break;
                    case LoginResult.UserNotFound:
                        WriteConsole("User not exist!");
                        break;
                    default:
                        throw new System.Exception("Unexpected enum case");
                }

            }
            else
            {
                WriteConsole("필요 인자 목록: userName");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
