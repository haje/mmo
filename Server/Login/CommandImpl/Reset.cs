﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Reset : Command
    {
        public Reset(ServerContext ctx)
            : base("reset", "Login DB의 유저를 모두 삭제합니다")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            ctx.LoginServer.loginDB.ResetTable();
            return true;
        }

        private readonly ServerContext ctx;
    }
}
