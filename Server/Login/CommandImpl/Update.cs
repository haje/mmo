﻿using System;
using HAJE.MMO.Server.Console;

namespace HAJE.MMO.Server.Login.CommandImpl
{
    public class Update : Command
    {
        public Update(ServerContext ctx)
            : base("update", "Login DB의 유저 정보를 변경합니다")
        {
            this.ctx = ctx;
        }

        protected override bool ExecuteInternal(string[] commandTokens)
        {
            if (commandTokens.Length > 2)
            {
                LoginResult dbRet = ctx.LoginServer.loginDB.UpdateHashedPassword(commandTokens[1], commandTokens[2]);
                switch (dbRet)
                {
                    case LoginResult.Success:
                        WriteConsole("User password updated successfully");
                        break;
                    case LoginResult.UserNotFound:
                        WriteConsole("User not exist!");
                        break;
                    default: 
                        throw new System.Exception("Unexpected enum case");
                }               
            }
            else
            {
                WriteConsole("필요 인자 목록: email, password(new)");
            }
            return true;
        }

        private readonly ServerContext ctx;
    }
}
