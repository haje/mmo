﻿using HAJE.MMO.Server.Console;
using System.Collections.Generic;
using HAJE.MMO.Server.Login.CommandImpl;

namespace HAJE.MMO.Server.Login
{
    public class LoginConsoleFactory : Console.ConsoleFactory
    {
        public LoginConsoleFactory(Prompt root, LoginServer server)
            : base("login", root)
        {
            this.server = server;
        }

        protected override void BuildCommand(Prompt prompt, List<Command> cmds)
        {
            var ctx = ServerContext.Instance;
            cmds.Add(new Write(ctx));
            cmds.Add(new Read(ctx));
            cmds.Add(new Update(ctx));
            cmds.Add(new Delete(ctx));
            cmds.Add(new Find(ctx));
            cmds.Add(new Count(ctx));
            cmds.Add(new Reset(ctx));
            cmds.Add(new Console.CommandImpl.Log(ctx, server));
            base.BuildCommand(prompt, cmds);
        }

        private readonly LoginServer server;
    }
}
