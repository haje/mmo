﻿using System.Collections.Generic;
using System.Xml;
using System;

namespace HAJE.MMO.DoodadGenerator
{
    public class RandomMaker
    {
        public enum doodadType
        {
            tree,
            rock
        }

        public RandomMaker()
        {
            doodadTypeList = new List<string>()
            {
                "IceRock2",
                "IceRock4",
                "IceRock5",
                "Pine2_Snow",
                "Pine2_Snow2",
                "Pine3_Snow",
                "Pine3_Snow2"
            };
            doodadSizeDic = new Dictionary<string, Tuple<float, float, float>>()
            {
                {"IceRock2" , new Tuple<float,float,float>(2.0f, 3.0f, 0)},
                {"IceRock4" , new Tuple<float,float,float>(2.0f, 3.0f, 0)},
                {"IceRock5" , new Tuple<float,float,float>(2.0f, 3.0f, 0)},
                {"Pine2_Snow" , new Tuple<float,float,float>(2.0f, 15.0f, 0)},
                {"Pine2_Snow2" , new Tuple<float,float,float>(2.0f, 15.0f, 0)},
                {"Pine3_Snow" , new Tuple<float,float,float>(2.0f, 15.0f, 0)},
                {"Pine3_Snow2" , new Tuple<float,float,float>(2.0f, 15.0f, 0)}
            };
        }

        public static void Run(string filename, int doodadnumber)
        {
            RandomMaker rndMaker = new RandomMaker();
            XmlDocument terrainxml = new XmlDocument();
            XmlNode fieldDescription = terrainxml.CreateElement(string.Empty, "FieldDescription", string.Empty);
            terrainxml.AppendChild(fieldDescription);

            XmlNode doodadStatusList = terrainxml.CreateElement("", "DoodadStatusList", "");
            fieldDescription.AppendChild(doodadStatusList);

            Random random = new Random();

            
            for (int i = 0; i < doodadnumber; i++)
            {
                XmlElement doodad = terrainxml.CreateElement("Doodad");
                XmlElement position = terrainxml.CreateElement("Position");
                doodad.SetAttribute("type", rndMaker.doodadTypeList[random.Next(0, rndMaker.doodadTypeList.Count)]);
                doodad.SetAttribute("radius", rndMaker.doodadSizeDic[doodad.GetAttribute("type")].Item1.ToString());
                doodad.SetAttribute("height", rndMaker.doodadSizeDic[doodad.GetAttribute("type")].Item2.ToString());
                doodad.SetAttribute("rotation", rndMaker.doodadSizeDic[doodad.GetAttribute("type")].Item3.ToString());

                position.SetAttribute("x", random.Next(1, 500).ToString());
                position.SetAttribute("y", random.Next(1, 500).ToString());
                doodad.AppendChild(position);
                doodadStatusList.AppendChild(doodad);
            }

            terrainxml.Save("../../../../" + filename + ".xml");
        }

        List<string> doodadTypeList;
        Dictionary<string, Tuple<float, float, float>> doodadSizeDic; 
    }
}
