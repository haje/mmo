﻿using System;
using System.Text.RegularExpressions;

namespace Common
{
    [Serializable]
    public class BuildResult
    {
        public enum Result
        {
            Success,
            Fail
        }

        public DateTime TimeStamp;
        public String BuildLog;
        public String ChangeLog;

        public String PushedUser;
        public Result Status;

        public BuildResult()
        {
        }
        public BuildResult(String buildLog, String changeLog)
        {
            this.BuildLog = buildLog;
            this.ChangeLog = changeLog;
            this.TimeStamp = DateTime.Now;

            if (BuildLog.Contains("Finished: SUCCESS"))
                Status = Result.Success;
            else
                Status = Result.Fail;

            Regex rg = new Regex("author ([a-zA-Z0-9]+)");
            Match m = rg.Match(ChangeLog);

            if (m.Groups.Count >= 1)
                PushedUser = rg.Match(changeLog).Groups[1].ToString();
            else
                PushedUser = String.Empty;
        }
    }
}
