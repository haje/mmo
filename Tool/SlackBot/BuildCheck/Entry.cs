﻿using Common;
using System;
using System.IO;
using System.IO.Pipes;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace BuildCheck
{
    class Entry
    {
        [STAThread]
        static void Main(string[] args)
        {
            String logPath = @"C:\Users\Commentator\.jenkins\jobs\Project MMO";
            int currentBuildNumber = -1;

            String buildLog = null;
            String commitLog = null;

            using (StreamReader sr = new StreamReader(Path.Combine(logPath, "nextBuildNumber")))
                currentBuildNumber = Int32.Parse(sr.ReadToEnd()) - 1;
            logPath = Path.Combine(logPath, "builds", currentBuildNumber.ToString());

            using (StreamReader sr = new StreamReader(Path.Combine(logPath, "log"), Encoding.Default))
                buildLog = sr.ReadToEnd();
            using (StreamReader sr = new StreamReader(Path.Combine(logPath, "changelog.xml"), Encoding.Default))
                commitLog = sr.ReadToEnd();
                
            BuildResult br = new BuildResult(buildLog, commitLog);
            using (NamedPipeClientStream npcs = new NamedPipeClientStream(".", "SlackBot", PipeDirection.InOut, PipeOptions.None, System.Security.Principal.TokenImpersonationLevel.Impersonation))
            {
                npcs.Connect();

                Console.WriteLine("Connected");
                using (StreamWriter sw = new StreamWriter(npcs))
                {
                    sw.WriteLine("BuildResult");
                    sw.Flush();

                    BinaryFormatter binFormatter = new BinaryFormatter();
                    binFormatter.Serialize(sw.BaseStream, br);

                    sw.Flush();
                    npcs.WaitForPipeDrain();
                }

                Console.WriteLine("Connection closed");
            }
        }
    }
}
