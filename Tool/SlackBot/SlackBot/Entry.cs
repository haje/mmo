﻿using Common;
using HAJE.SlackBot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace HAJE
{
    class Entry
    {
        #region Bot Setting
#if DEBUG
        private static String targetChannel = "test_bot";
#else
        private static String targetChannel = "project_mmo_bot";
#endif

        [STAThread]
        static void Main(string[] args)
        {
            HAJE.SlackBot.SlackBot sb = new SlackBot.SlackBot("xoxb-13643017205-v7JrUUcg4juvCBhP0tn3GLdR");
            AppDomain.CurrentDomain.ProcessExit += (s, e) =>
            {
                if (sb != null)
                    sb.Dispose();
            };

            Listener listener = new Listener(sb.SocketClient.Channels.Find(_ => _.name == targetChannel));
            listener.ActionList.Add(new BotAction(
                new Regex($"<@{sb.SocketClient.MyData.id}> .+? help", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace),
                (msg) =>
                {
                    sb.SendMessage(listener.TargetChannel, @"사용법: @mmo_slackbot help|ping|build|server_on|exit
더 자세한 설명은 https://docs.google.com/document/d/1COEp7YQgq_HY6S3Rjt9sHDdJuhsfadYy5XsA2wasoSk/edit 를 참조.");
                }));
            listener.ActionList.Add(new BotAction(
                new Regex($"<@{sb.SocketClient.MyData.id}> .+? ping", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace),
                (msg) =>
                {
                    sb.SendMessage(listener.TargetChannel, $"@{sb.SocketClient.Users.Find(_ => _.id == msg.user).name} pong");
                }));
            listener.ActionList.Add(new BotAction(
                new Regex($"<@{sb.SocketClient.MyData.id}> .+? build", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace),
                (msg) =>
                {
                    sb.SendMessage(listener.TargetChannel, $"@{sb.SocketClient.Users.Find(_ => _.id == msg.user).name} 빌드를 요청했습니다.");
                    SendBuildRequest(sb);
                }));
            listener.ActionList.Add(new BotAction(
                new Regex($"<@{sb.SocketClient.MyData.id}> .+? server_on", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace),
                (msg) =>
                {
                    sb.SendMessage(listener.TargetChannel, $"서버 시동중...");
                    foreach (Process process in Process.GetProcessesByName("ServerEntry"))
                        process.Kill();

                    String srcDirectory = @"C:\Users\Commentator\.jenkins\jobs\Project MMO\workspace\ServerEntry\bin\Debug";
                    String dstDirectory = @"C:\Users\Commentator\.jenkins\jobs\Project MMO\workspace\ServerEntry\bin\Real";

                    bool isCopied = true;
                    DirectoryInfo dirInfo = new DirectoryInfo(srcDirectory);
                    do
                    {
                        isCopied = true;
                        sb.WriteLog(LogLevel.Information, "Trying to copy....");
                        foreach (FileInfo file in dirInfo.GetFiles())
                        {
                            try
                            {
                                file.CopyTo(Path.Combine(dstDirectory, file.Name), true);
                            }
                            catch (IOException)
                            {
                                sb.WriteLog(LogLevel.Information, $"Failed to clone {file.Name}");
                                isCopied = false;
                                Thread.Sleep(10);
                                break;
                            }
                        }
                    } while (isCopied == false);

                    Process.Start(@"C:\Users\Commentator\.jenkins\jobs\Project MMO\workspace\ServerEntry\bin\Real\ServerEntry.exe");
                    sb.SendMessage(listener.TargetChannel, $"서버 시동 완료");
                }));
            listener.ActionList.Add(new BotAction(
                new Regex($"<@{sb.SocketClient.MyData.id}> .+? exit", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace),
                (msg) =>
                {
                    sb.Dispose();
                    sb = null;
                }));
            sb.AddListener(listener);
            #endregion


            while (true)
                try
                {
                    bool isConnectionRequired = false;
                    NamedPipeServerStream npss = null;
                    StreamReader sr = null;

                    while (sb != null)
                    {
                        if (npss != null && npss.IsConnected == true)
                        {
                            String firstLine = sr.ReadLine();
                            sb.WriteLog(LogLevel.Information, "From NPSS: " + firstLine);
                            if (firstLine == "BuildResult")
                                BuildCheck(sb, sr);
                            else if (firstLine == "ServerException")
                            {
                                sb.SendMessage(listener.TargetChannel, "Warning: 서버가 예외를 던졌습니다");
                                sb.SendMessage(listener.TargetChannel, $"```{sr.ReadToEnd()}```");

                                sb.SendMessage(listener.TargetChannel, $"서버 재시작 중...");
                                foreach (Process process in Process.GetProcessesByName("ServerEntry"))
                                    process.Kill();

                                Process.Start(@"C:\Users\Commentator\.jenkins\jobs\Project MMO\workspace\ServerEntry\bin\Real\ServerEntry.exe");
                                sb.SendMessage(listener.TargetChannel, $"서버 재시작 완료");
                            }

                            isConnectionRequired = false;
                        }
                        else if (isConnectionRequired == false)
                        {
                            Task.Run(() =>
                            {
                                sb.WriteLog(LogLevel.Information, "NPSS Connection Waiting");
                                isConnectionRequired = true;

                                if (npss != null)
                                    npss.Close();
                                npss = new NamedPipeServerStream("SlackBot", PipeDirection.InOut, 10);
                                npss.WaitForConnection();

                                sb.WriteLog(LogLevel.Information, "NPSS Connected");
                                if (sr != null)
                                    sr.Close();
                                sr = new StreamReader(npss);
                            });
                        }

                        sb.Update();
                        Thread.Sleep(100);
                    }

                    sr.Close();
                    npss.Close();
                }
                catch (ObjectDisposedException e)
                {
                    sb.WriteLog(LogLevel.Warning, e.ToString());
                    sb.SendMessage(targetChannel, "Warning: Pipe has crashed");
                }
        }

        public static void SendBuildRequest(SlackBot.SlackBot sb)
        {
            sb.WriteLog(LogLevel.Information, "Build Request Sent!");
            HttpWebRequest hreq = WebRequest.Create(@"http://143.248.233.58:8080/job/Project%20MMO/build?delay=0sec") as HttpWebRequest;
            HttpWebResponse hres = hreq.GetResponse() as HttpWebResponse;
        }
        public static void BuildCheck(SlackBot.SlackBot sb, StreamReader sr)
        {
            // bitbucket -> slack
            Dictionary<String, String> userDictionary = new Dictionary<string, string>();
            userDictionary.Add("KeV", "kev");
            userDictionary.Add("jinsung23", "sineras");
            userDictionary.Add("Atonal", "atonal");
            userDictionary.Add("namkyu2", "namkyu");

            BinaryFormatter binFormatter = new BinaryFormatter();
            BuildResult br = binFormatter.Deserialize(sr.BaseStream) as BuildResult;

            if (br.Status == BuildResult.Result.Success)
            {
                sb.WriteLog(LogLevel.Information, "Build Success");
                sb.SendMessage(targetChannel, "Build Success");
            }
            else
            {
                sb.WriteLog(LogLevel.Information, "Build Failed");
                if (userDictionary.ContainsKey(br.PushedUser) == true)
                    sb.SendMessage(targetChannel, $"@{userDictionary[br.PushedUser]} 빌드 실패 기념 :seolho:");
                else
                {
                    sb.SendMessage(targetChannel, $"{br.PushedUser} 빌드 실패!!");
                }
            }
        }
    }
}
