﻿using SlackAPI;
using System;
using System.Text.RegularExpressions;

namespace HAJE.SlackBot
{
    public struct BotAction
    {
        public Regex RegularExpression
        {
            get;
            private set;
        }
        public Action<Message> Action
        {
            get;
            private set;
        }

        public BotAction(Regex regularExpression, Action<Message> action)
        {
            this.RegularExpression = regularExpression;
            this.Action = action;
        }
    }
}
