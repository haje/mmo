﻿using System;
using SlackAPI;
using System.Collections.Generic;

namespace HAJE.SlackBot
{
    public class Listener
    {
        private readonly DateTime startTime;
        private readonly List<Message> readList;

        public SlackBot Owner
        {
            get;
            private set;
        }
        public readonly Channel TargetChannel;
        public readonly List<BotAction> ActionList;

        public Listener(Channel targetChannel)
        {
            startTime = DateTime.Now;
            readList = new List<Message>();

            TargetChannel = targetChannel;
            ActionList = new List<BotAction>();
        }
        public void SetOwner(SlackBot owner)
        {
            Owner = owner;
        }

        public void Update(SlackSocketClient SocketClient)
        {
            SocketClient.GetChannelHistory((hist) =>
            {
                List<Message> msgList = new List<Message>(hist.messages);
                msgList.Reverse();

                lock (readList)
                {
                    foreach (Message msg in msgList)
                        if (startTime.Ticks < msg.ts.Ticks)
                            if (readList.Find(_ => _.ts == msg.ts) == null)
                            {
                                readList.Add(msg);

                                if (msg.user != null)
                                    Owner.WriteLog(LogLevel.Information, $"{SocketClient.UserLookup[msg.user].name} said {msg.text}, time:{msg.ts}");

                                foreach (var botAction in ActionList)
                                    if (botAction.RegularExpression.IsMatch(msg.text))
                                        botAction.Action.Invoke(msg);
                            }

                    while (readList.Count > 100)
                        readList.RemoveAt(0);
                }
            }, TargetChannel, null, null, 100);
        }
    }
}