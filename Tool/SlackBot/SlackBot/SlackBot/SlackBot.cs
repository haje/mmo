﻿using SlackAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace HAJE.SlackBot
{
    public enum LogLevel
    {
        Information,
        Warning,
        Bug,
    }
    public class SlackBot : IDisposable
    {
        #region Special Variables
#if DEBUG
        public const String DefaultChannelName = "test_bot";
#else
        public const String DefaultChannelName = "project_mmo_bot";
#endif
        #endregion

        #region variables
        public StreamWriter LogStream
        {
            get;
            private set;
        }
        private readonly List<Message> readList;

        public readonly SlackSocketClient SocketClient;
        public readonly List<Listener> ListenerList;
#endregion
#region .ctor
        public SlackBot(String token)
        {
            LogStream = new StreamWriter(String.Format("slackbot_log_{0,4}{1,0:D2}{2,0:D2}_{3,0:D2}{4,0:D2}{5,0:D2}.{6,0:D3}.txt",
                DateTime.Now.Year,
                DateTime.Now.Month,
                DateTime.Now.Day,
                DateTime.Now.Hour,
                DateTime.Now.Minute,
                DateTime.Now.Second,
                DateTime.Now.Millisecond));
            readList = new List<Message>();
            ListenerList = new List<Listener>();

            SocketClient = new SlackSocketClient(token);
            SocketClient.Connect((connected) =>
            {
                this.WriteLog(LogLevel.Information, "SlackBot Online");
            });

            while (SocketClient.IsConnected == false)
                Thread.Sleep(10);
            SendMessage(DefaultChannelName, "SlackBot Online");
            
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                WriteLog(LogLevel.Bug, e.ExceptionObject.ToString());
            };
        }
#endregion

#region add Action
        public void AddListener(Listener listener)
        {
            listener.SetOwner(this);
            ListenerList.Add(listener);
        }
#endregion
#region Update
        public void Update()
        {
            foreach (Listener listener in ListenerList)
                listener.Update(SocketClient);
        }
#endregion

#region Write Log
        public void WriteLog(LogLevel logLevel, String log, params object[] arg)
        {
            String logLevelString = String.Empty;
            switch (logLevel)
            {
                case LogLevel.Information:
                    logLevelString = "Info: ";
                    break;

                case LogLevel.Warning:
                    logLevelString = "Warn: ";
                    break;

                case LogLevel.Bug:
                    logLevelString = "Bug : ";
                    break;
            }

            Console.WriteLine(logLevelString + log, arg);
            if (LogStream != null)
            {
                LogStream.WriteLine(logLevelString + log, arg);
                LogStream.Flush();
            }
        }
#endregion

#region Send Message
        private bool isMessageSent;
        public void SendMessage(Channel channel, String text)
        {
            SendMessage(channel.name, text);
        }
        public void SendMessage(String channelName, String text)
        {
            if (SocketClient.IsConnected == false)
                throw new Exception("Client socket not connected");
            
            isMessageSent = false;
            SocketClient.SendMessage((msgReceived) =>
            {
                isMessageSent = true;
            }, SocketClient.Channels.Find(_ => _.name == channelName).id, text);

            while (isMessageSent == false)
                Thread.Sleep(100);
            WriteLog(LogLevel.Information, "Message Sent; {0}", text);
        }
#endregion
        public void Dispose()
        {
            ListenerList.Clear();

            SendMessage(DefaultChannelName, "SlackBot Offline");
            WriteLog(LogLevel.Information, "SlackBot Offline");

            SocketClient.CloseSocket();
            LogStream.Close();
        }
    }
}
