# -*- coding: utf8-*-
import mysql.connector as mariadb
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from contextlib import closing
import hashlib, base64, time
import smtplib, socket
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText

DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin@asdf'
PASSWORD = 'default'

app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def show_entries():   
    return render_template('index.html')



@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        passwd = request.form['passwd']
        passwordHash = get_hashed_password(email, passwd)

        g.cursor.execute("SELECT internalKey FROM users WHERE email=%s", (email,))
        internalKey = g.cursor.fetchone()
        if (internalKey is None):
            return 'User Not Exist'

        g.cursor.execute("SELECT internalKey FROM users WHERE email=%s AND passwordHash=%s", (email, passwordHash,))
        internalKey = g.cursor.fetchone()
        if (internalKey is None):
            return 'Wrong Password'

        session['logged_in'] = True
        return render_template('AuthSuccess.html')

    return abort(400)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        email = request.form['email']
        userName = request.form['userName']
        passwd = request.form['passwd']
        passwordHash = get_hashed_password(email, passwd)        

        g.cursor.execute("SELECT internalKey FROM users WHERE email=%s", (email,))
        internalKey = g.cursor.fetchone()
        if (internalKey is not None):
            refresh_token(email, userName)
            return 'Email address already exist'

        g.cursor.execute("SELECT internalKey FROM users WHERE userName=%s", (userName,))
        internalKey = g.cursor.fetchone()
        if (internalKey is not None):
            return 'Username already exist'

        g.cursor.execute("INSERT INTO users(email, userName, passwordHash) VALUES (%s, %s, %s)", (email, userName, passwordHash,))
        g.mariadb_connection.commit()
        refresh_token(email, userName)

        return "User added successfully"
    else:
        return render_template('register.html')


def refresh_token(email, userName):
    g.cursor.execute("SELECT token FROM token WHERE email=%s", (email,))
    token = g.cursor.fetchone()
    if (token is not None):
        g.cursor.execute("DELETE FROM token WHERE email=%s", (email,))
    token = get_token(email, userName)
    token_type = 'Register'
    g.cursor.execute("INSERT INTO token(email, token, type) VALUES (%s, %s, %s)", (email, token, token_type,))
    g.mariadb_connection.commit()
    myIP = socket.gethostbyname(socket.gethostname())
    url = myIP + ":5000" + token
    print "smtp." + email.split('@')[1]
    mailserver = smtplib.SMTP("smtp." + email.split('@')[1], 25)
    
    fromaddr = "HajeMMO@haje.org"
    toaddr = email
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Haje MMO Mail Auth"
    body = "Hi " + email + ", Please enter below URL to authorize your account <br /><br />"
    body = body + "<a href=\"http://"+url+"\" target=\"_blank\"> " + url + " </a>"
    msg.attach(MIMEText(body, 'html'))
    mailserver.sendmail(fromaddr, toaddr, msg.as_string())


@app.before_request
def connect_db():
    g.mariadb_connection = mariadb.connect(host='143.248.233.58', user='root', password='mmo', database='mmo_login')
    g.cursor = g.mariadb_connection.cursor()


@app.route('/passwd/<email>/<passwd>')
def show_hashed_password(email, passwd):
    if request.remote_addr != '143.248.233.58':
        return abort(403)  # Forbidden
    return get_hashed_password(email, passwd)


def get_hashed_password(email, passwd):    
    passwd_hash = hashlib.sha256(passwd).hexdigest()
    bytes = email+passwd_hash
    hash_val = hashlib.sha256(bytes).hexdigest()
    passwordHash = base64.encodestring(hash_val).split('\n')[0]
    print "hash: " + passwordHash
    return passwordHash

@app.route('/token/<email>/<userName>')
def show_token(email, userName):
    if request.remote_addr != '127.0.0.1':
        return abort(403)  # Forbidden
    return get_token(email, userName)

def get_token(email, userName):    
    time_str = str(int(time.time()))
    time_hash = hashlib.sha256(time_str).hexdigest()
    userName_hash = hashlib.sha256(userName).hexdigest()
    bytes = email+time_hash+userName_hash
    hash_val = hashlib.sha256(bytes).hexdigest()
    token = base64.encodestring(hash_val).split('\n')[0]
    print "token: " + token
    return "/auth/"+email+"/"+token


@app.route('/auth/<email>/<token>')
def mail_auth(email, token):    
    g.cursor.execute("SELECT token FROM token WHERE email=%s AND token=%s", (email, request.path, ))
    db_token = g.cursor.fetchone()
    if (db_token is None):
        return render_template('AuthFail.html')

    g.cursor.execute("UPDATE users SET mailAuthorized=1 WHERE email=%s", (email, ))
    g.mariadb_connection.commit()

    return 'Mail Authorization Success'


def close_db():
    g.mariadb_connection.close()


if __name__ == '__main__':
    #app.debug = True
    app.run(host='0.0.0.0')
